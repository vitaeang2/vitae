@ECHO OFF
SET ARTNEW_FRAMEWORK_PATH=D:\work\vitae\arquitetura
SET SUITE_ADM_PATH=D:\work\vitae\b3-bus
@ECHO Framework: %ARTNEW_FRAMEWORK_PATH%
@ECHO SuiteAdm: %SUITE_ADM_PATH%
SET /P VALUE=Os caminhos acima estao corretos (S/N)?:  
IF /I %VALUE% EQU S (

cd /D %SUITE_ADM_PATH%
call mvn clean
@ECHO=======================================================
@ECHO executado 'mvn clean' em 'SUITE_ADM'
@ECHO=======================================================
PAUSE

cd /D %SUITE_ADM_PATH%
call mvn install
@ECHO O processo foi concluido.
@ECHO Verifique se o arquivo esperado esta em: %SUITE_ADM_PATH%\target
@ECHO=======================================================
@ECHO executado 'mvn install' em 'SUITE_ADM'
@ECHO=======================================================

PAUSE

) ELSE (
@ECHO O processo sera encerrado.
PAUSE
)