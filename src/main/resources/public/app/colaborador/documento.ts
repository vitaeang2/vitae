export class Documento {

	id: number;

	nomeArquivo : string;

	nomeArquivoExt : string;
	
	tipoArquivo : string;

	caminhoArquivo : string;
	
	temArquivo : boolean;

	historico : string;

	dataEvento : string;

	dataCadastro : string;
	
	ativo : boolean;

	tipoDocumento : number;

	nomeDocumento : string;

	idColaborador : number;

	idUsuarioAdicionou : number;

	valorRecebido : string;

}