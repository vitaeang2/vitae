import {Component} from '@angular/core';
import {Routes, ROUTER_DIRECTIVES, Router} from '@angular/router';
import {ColaboradorListComponent} from './colaborador-list.component';
import {ColaboradorDetailComponent} from "./colaborador-detail.component";

@Component({
	selector: 'colaborador',
	templateUrl: 'app/colaborador/colaborador.html',
	directives: [ROUTER_DIRECTIVES]
})
@Routes([
	{ path: "/", component: ColaboradorListComponent},
	{ path: ":id", component: ColaboradorDetailComponent}
	])
export class ColaboradorComponent  {
	constructor(private _router : Router) {
		this._router.navigate(['/colaborador'])
	}
}
