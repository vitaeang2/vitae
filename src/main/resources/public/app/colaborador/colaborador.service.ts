import {Injectable} from '@angular/core';
import {Http, HTTP_PROVIDERS, Headers} from '@angular/http';

import {UrlService} from '../model/url.service';
import {HeadersService} from '../aplicacao/headers.service'
import {LoginService} from '../login/login.service';
import {PaginacaoParams} from '../model/paginacao_params';
import {Colaborador} from '../colaborador/colaborador';
import {Documento} from './documento';

@Injectable()
export class ColaboradorService  extends UrlService {

	constructor(_http: Http, _headerService: HeadersService, _loginService: LoginService) {
		super(_http, _headerService, _loginService);
	}

	public listar(params: PaginacaoParams, idEmpresa: number) {
		return super.callHeader(`colaborador/listar?params=${JSON.stringify(params)}&idEmpresa=${idEmpresa}`);
	}

	public listarPorNucleo(params: PaginacaoParams, idEmpresa: number, idNucleo?: number) {
		return super.callHeader(`colaborador/listar?params=${JSON.stringify(params)}&idEmpresa=${idEmpresa}&idNucleo=${idNucleo}`);
	}
	
	public listarCombo() {
		return super.callHeader(`colaborador/listarCombo`);
	}

	public consultarId(id: number) {
		return super.callHeader(`colaborador/consultarId?id=${id}`);
	}

	public possuiEmail(email: string) {
		return super.callHeader(`colaborador/possuiEmail?email=${email}`);
	}

	public possuiCnj(cnj: string) {
		return super.callHeader(`colaborador/possuiCnj?cnj=${cnj}`);
	}

	public salvar(colaborador: Colaborador, id: number) {
		return super.callHeaderBody(`colaborador/salvar?idPerfil=${id}`, JSON.stringify(colaborador));
	}

	public excluirId(id: number) {

		return super.callHeader(`colaborador/excluirId?id=${id}`);
	}

	public removerFoto(id) {

		return super.callHeader(`colaborador/removerFoto?id=${id}`);
	}

	public listarDocumentos(idColaborador: number) {
		return super.callHeader(`colaborador/listarDocumentos?idColaborador=${idColaborador}`);
	}

	public consultarDocumento(idDocumento : number) {
		return super.callHeader(`colaborador/consultarDocumento?id=${idDocumento}`);
	}

	public removerDocumento(idDocumento : number, idColaborador : number) {
		return super.callHeader(`colaborador/removerDocumento?id=${idDocumento}&idColaborador=${idColaborador}`);
	}

	public removerArquivo(idDocumento : number, idColaborador : number) {
		return super.callHeader(`colaborador/removerArquivo?id=${idDocumento}&idColaborador=${idColaborador}`);
	}

	public salvarDocumento(documento: Documento) {
		return super.callHeaderBody(`colaborador/salvarDocumento`, JSON.stringify(documento));
	}
	
	

	/*public listarDocumentos(params: PaginacaoParams, idColaborador: number) {
		return super.callHeader(`colaborador/listarDocumentos?params=${JSON.stringify(params)}&idColaborador=${idColaborador}`);
	}*/
}