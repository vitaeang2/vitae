import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {DataTable, Column, LazyLoadEvent, Dropdown, SelectItem, Dialog} from 'primeng/primeng';
import {MaterialDesignDirective} from "../aplicacao/material";
import {Colaborador} from "./colaborador";
import {ColaboradorService} from "./colaborador.service"
import {Nucleo} from "../nucleo/nucleo";
import {NucleoService} from '../nucleo/nucleo.service';
import {MensagemService} from "../aplicacao/mensagem.service"
import {LoginService} from "../login/login.service"
import {PaginacaoParams} from '../model/paginacao_params';
import {Paginacao} from '../model/paginacao';
import {Response} from '../model/response';
import {VitaeLazy} from '../aplicacao/vitae-lazy.service';

@Component({
    selector: 'colaborador',
    templateUrl: './app/colaborador/colaborador-list.html',
    directives: [DataTable, Column, Dropdown, Dialog],
    providers: [ColaboradorService, NucleoService]
})
export class ColaboradorListComponent extends VitaeLazy  implements OnInit {

    params : PaginacaoParams;
    paginacao : Paginacao = new Paginacao();
    listaNucleo : SelectItem[];
    idNucleoSelecionado : number;
    mostarFiltroNucleo :boolean = false;
    idNucleo : number = 0;

    private idColaboradorSelecionado: number = 0;

    public displayDialogExcluir: boolean = false;

    constructor(private _nucleoService: NucleoService, private _colaboradorService : ColaboradorService, private _loginService : LoginService, private _mensagemService : MensagemService, private router: Router){
        super(router);
    }

    ngOnInit(){
        let nucleo: Nucleo = this._loginService.getUser().colaborador.nucleo;

        if (nucleo && nucleo.id) {
            this.idNucleo = nucleo.id;
            this.mostarFiltroNucleo = false;

        } else {

            this.idNucleo = 0;
            this.mostarFiltroNucleo = true;
            this.carregarListaNucleo();
        }

        this.doListar();
    }

    novo () {
        super.novo('/colaborador/');
    }

    editar(id: number){

        super.editar(id, '/colaborador/');
    }

    doListar () {
        if(this.params){
            if(this.mostarFiltroNucleo){

                if(this.idNucleoSelecionado && this.idNucleoSelecionado != 0){

                    this.listarPorNucleo(this.idNucleoSelecionado);

                }else{

                    this.listarPorNucleo(this.idNucleo);
                }

            }else{

                this.listar();
            }
        }
    }

    listarPorNucleo(idNucleo: number){

        this._colaboradorService.listarPorNucleo(this.params, this._loginService.getUser().colaborador.empresa.id, idNucleo).subscribe((resultado) => {

            this.setListaColaborador(resultado)
        });
    }

    private listar(){
        this._colaboradorService.listar(this.params, this._loginService.getUser().colaborador.empresa.id).subscribe((resultado) => {

            this.setListaColaborador(resultado)
        });
    }

    private setListaColaborador(resultado : Response){

        if(!resultado.sucesso){

            this._mensagemService.showAtencao(resultado.mensagem);
            return;
        }

        this.paginacao = resultado.objetoRetorno;
    }

    private carregarListaNucleo(){

        this._nucleoService.listarComboPorIdEmpresa(this._loginService.getUser().colaborador.empresa.id).subscribe((resultado) => {

            this.setListaNucleo(resultado)
        });
    }

    private setListaNucleo(resultado : Response){

        if(!resultado.sucesso){

            this._mensagemService.showAtencao(resultado.mensagem);
            return;
        }
        
        this.listaNucleo = [];
        let lista : Array<Nucleo> = resultado.objetoRetorno;
        this.listaNucleo.push({label: "-- TODOS --", value: 0});
        lista.forEach(nuc => this.listaNucleo.push({label: nuc.nome, value: nuc.id}));
    }

    confirmDiolgExcluir(id:number) {

        this.idColaboradorSelecionado = id;
        this.displayDialogExcluir = true;
    }

    excluir() { 

        this._colaboradorService.excluirId(this.idColaboradorSelecionado).subscribe((resultado) => {

            this._mensagemService.showInfo("Colaborador excluído com sucesso!");
            this.displayDialogExcluir = false;
            this.doListar();
        });

    }
}
