import {Pessoa} from '../pessoa/pessoa';
import {Cargo} from '../cargo/cargo';
import {Empresa} from '../empresa/empresa';
import {Nucleo} from '../nucleo/nucleo';

export class Colaborador {

	id: number;

	nomeArquivo : string;
	
	tipoArquivo : string;

	caminhoArquivo : string;
	
	temFoto : boolean;

	pessoa : Pessoa;
	
	empresa : Empresa;

	nucleo : Nucleo;

	cargo : Cargo;

	admissao : string;

	demissao : string;
	
	ativo : boolean;

}