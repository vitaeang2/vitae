import {Component} from '@angular/core';
import {FORM_BINDINGS, ControlGroup, FormBuilder, Validators} from '@angular/common';
import {FileUploader, FILE_UPLOAD_DIRECTIVES} from 'ng2-file-upload/ng2-file-upload';
import {OnActivate, RouteSegment, Router} from '@angular/router';
import {SelectItem, Dialog, TabView, TabPanel, Dropdown} from 'primeng/primeng';

import {LoginService} from '../login/login.service';
import {Documento} from './documento';
import {EmpresaService} from '../empresa/empresa.service';
import {Empresa} from '../empresa/empresa';
import {Nucleo} from '../nucleo/nucleo';
import {NucleoService} from '../nucleo/nucleo.service';
import {ColaboradorService} from './colaborador.service'
import {Colaborador} from '../colaborador/colaborador'
import {MensagemService} from '../aplicacao/mensagem.service';
import {Response} from '../model/response'
import {NumeroBanco} from '../numero_banco/numero_banco';
import {PaginacaoParams} from '../model/paginacao_params';
import {NumeroBancoService} from '../numero_banco/numero_banco.service'
import {Pessoa} from '../pessoa/pessoa';
import {Cargo} from '../cargo/cargo';
import {Banco} from '../banco/banco';
import {Usuario} from '../usuario/usuario';
import {CargoService} from '../cargo/cargo.service';
import {CustomDatePickerComponent} from '../componentes/custom-datepicker.component';
import {MeuPanel} from '../aplicacao/panel';
import {CustomInput} from '../componentes/custom-input.component';
import {InputNumber} from '../componentes/input-number';
import {EnumEstadosBrasileiros} from '../enum/enum-estados-brasileiros';
import {EnumTipoDocumento} from '../enum/enum-tipo-documento';
import {EnumSexo} from '../enum/enum-sexo';
import {EnumEstadoCivil} from '../enum/enum-estado-civil';
import {EnumTipoPessoa} from '../enum/enum-tipo-pessoas';
import {PerfilAcesso} from '../perfil_acesso/perfil_acesso';
import {PerfilAcessoService} from '../perfil_acesso/perfil_acesso.service';
import {UsuarioService} from '../usuario/usuario.service';
import { NotEqualValidator } from '../aplicacao/not-equal-validator.directive';
import { DataValidator } from '../aplicacao/data-validator.directive';
import {TimerWrapper} from '@angular/core/src/facade/async';
import {UrlService} from '../model/url.service';
import {MoneyPipe} from '../componentes/money_pipe';

//const BASE  = "http://localhost:8080/colaborador";
const BASE : string = window.location.href;
const PATH : string = `${BASE}colaborador/foto`;;
const PATH_DOC : string = `${BASE}colaborador/documento`;;
@Component({
	selector: 'colaborador-detail',
	templateUrl: './app/colaborador/colaborador-detail.html',
	providers: [PerfilAcessoService, ColaboradorService, NucleoService, EmpresaService, CargoService, NumeroBancoService],
	directives: [MeuPanel, CustomDatePickerComponent, CustomInput, InputNumber, Dialog, FILE_UPLOAD_DIRECTIVES, NotEqualValidator, TabView, TabPanel, DataValidator, Dropdown],
	viewBindings: [FORM_BINDINGS],
	pipes: [MoneyPipe]
})
export class ColaboradorDetailComponent implements OnActivate {

	paramsDocumentos : PaginacaoParams;

	public uploader:FileUploader;
	
	public uploaderDocumento:FileUploader;

	mostrarDialog : boolean;

	isPodeSalvar: boolean = true;

	displayDialogExcluir: boolean = false;

	numeroBanco: NumeroBanco = new NumeroBanco();

	sexo: SelectItem[];
	
	estadoCivil: SelectItem[];

	colaborador : Colaborador = new Colaborador();

	usuario : Usuario = new Usuario();

	listaNumeroBanco : Array<NumeroBanco>;

	listaEstado = EnumEstadosBrasileiros;

	listaEmpresa : SelectItem[];
	
	listaNucleo : SelectItem[];
	
	listaCargo : SelectItem[];

	listaPerfis : SelectItem[];
	
	listaTipoDocumentos : SelectItem[];

	idPerfil : number = 0;

	idEmpresa : number = 0;
	
	idNucleo : number = 0;
	
	idCargo : number = 0;

	possuiUsuario : boolean = false;
	
	repetirSenha : string = '';

	pt_BR: any;

	nomeBanco : string = '';
	
	documento : Documento;
	
	idDocumentoRemover : number = 0; 
	
	listaDocumentos : Array<Documento> = new Array<Documento>();
	
	constructor(private _usuarioService: UsuarioService, private _perfilAcessoService: PerfilAcessoService, 
		private _numeroBancoService: NumeroBancoService, private _cargoService : CargoService, 
		private _nucleoService : NucleoService, private _empresaService: EmpresaService, 
		private _colaboradorService: ColaboradorService, private _loginService: LoginService, 
		private _mensagemService : MensagemService, private _router: Router, builder: FormBuilder){
		this.limpar();
	}

	routerOnActivate(curr: RouteSegment) {
		let id = <number> +curr.getParam('id');
		
		this.listarComboSexo();
		//this.listarComboEstadoCivil();
		//this.doListarCombo();
		//this.doListarComboCargo();
		//this.listarNumerosBancos();

		this.limpar();

		if(id != -1){
			this.colaborador = new Colaborador();
			this.colaborador.id = id;
			this.documento = new Documento();
			this.listaTipoDocumentos = EnumTipoDocumento.toSelectItem();
			//this.recuperarColaborador(id);
		}
	}

	habilitarBanco(){
		this.mostrarDialog = true;
		this.numeroBanco = new NumeroBanco();
		this.listarNumerosBancos();
		this.nomeBanco = '';
	}

	listarNumerosBancos(){
		this.doListarNumerosBancos();
	}

	filtrarBanco() {
		this.doListarNumerosBancos(this.nomeBanco);
	}

	listarComboSexo(){
		this.sexo = [];
		for(var n in EnumSexo) {
			var isValueProperty = parseInt(n, 10) >= 0 
			if (isValueProperty) {
				this.sexo.push({label: EnumSexo[n], value: n});
			}
		}
		this.listarComboEstadoCivil();
	}

	listarComboEstadoCivil(){
		this.estadoCivil = [];
		for(var n in EnumEstadoCivil) {
			var isValueProperty = parseInt(n, 10) >= 0 
			if (isValueProperty) {
				this.estadoCivil.push({label: EnumEstadoCivil[n], value: n});
			}
		}

		this.doListarCombo();
	}

	salvar(){
		this.doSalvar();
	}

	limpar(){
		this.colaborador = new Colaborador();
		this.colaborador.pessoa = new Pessoa();
		this.colaborador.cargo = new Cargo();
		this.colaborador.empresa = new Empresa();
		this.colaborador.nucleo = new Nucleo();
		this.numeroBanco = new NumeroBanco();
		this.usuario = new Usuario();
		this.nomeBanco = '';
		this.mostrarDialog = false;
		this.idPerfil = 0;
		this.idEmpresa = 0;
		this.idNucleo = 0;
		this.idCargo = 0;
		this.possuiUsuario = false;
	}

	voltar(){

		this._router.navigate(['/colaborador']);
	}

	listarComboPorIdEmpresa(){

		if (this.idEmpresa > 0) {

			this._nucleoService.listarComboPorIdEmpresa(this.idEmpresa).subscribe((resultado) => {
				
				this.setListaComboNucleo(resultado)
				
			},(error) => {
				
				console.log(error)
			});

		} else {
			
			this.listaNucleo = [];
		}
	}

	selecionarBanco(nb : NumeroBanco) {
		
		this.numeroBanco = nb;
		this.mostrarDialog = false;
	}

	carregarListaPerfil(cb) {

		this.possuiUsuario = cb;
		this.doCarregarListaPerfil();
	}

	public isSalvandoDocumento : boolean = false;

	salvarDocumento(){
		this.isSalvandoDocumento = true;
		let salvar = true;
		//this.iniciarDocumento();
		this.montarDocumento();
		if(this.uploaderDocumento.getNotUploadedItems().length && this.uploaderDocumento.getNotUploadedItems().length > 0){
			this.uploaderDocumento.queue.forEach(p => { 
				if(!p.file.type.includes("image") && !p.file.type.includes("pdf")){
					salvar = false;
					return;
				}
				this.documento.nomeArquivo = p.file.name; 
				this.documento.tipoArquivo = p.file.type;
				p.url = PATH_DOC+"?documento="+encodeURIComponent(JSON.stringify(this.documento));
				
			});
			if(!salvar){
				this.uploaderDocumento.clearQueue();
				this._mensagemService.showInfo("Não foi possivel salvar. O arquivo deve ser uma imagem ou um PDF!");
				return;
			}
			this.uploaderDocumento.uploadAll();

			TimerWrapper.setTimeout(() => {  
				this.isSalvandoDocumento = false;
				this._mensagemService.showInfo("Documento enviado com sucesso!");
				this.documento = new Documento();
				this.listarDocumentos();
			}, 2000);
			
		} else {
			this._colaboradorService.salvarDocumento(this.documento).subscribe((retorno) => {
				this.isSalvandoDocumento = false;
				this._mensagemService.showInfo("Documento enviado com sucesso!");
				this.documento = new Documento();
				this.listarDocumentos();
			});
		}
	}

	salvarFoto(){
		let salvar = true;
		if(this.uploader.getNotUploadedItems().length && this.uploader.getNotUploadedItems().length > 0){
			this.uploader.queue.forEach(p => { 
				if(!p.file.type.includes("image")){
					salvar = false;
					return;
				}
				this.colaborador.nomeArquivo = p.file.name; 
				this.colaborador.tipoArquivo = p.file.type;
			});
			if(!salvar){
				this.uploader.clearQueue();
				this._mensagemService.showInfo("Não foi possivel salvar. O arquivo deve ser uma imagem!");
				return;
			}
			this.uploader.uploadAll();

			TimerWrapper.setTimeout(() => {  
				this._mensagemService.showInfo("Foto enviada com sucesso!");

				this._colaboradorService.consultarId(this.colaborador.id).subscribe((resultado) => {

					this.setColaborador(resultado);
				})
			}, 1500);

			
		}
	}

	removerFoto(){
		this._colaboradorService.removerFoto(this.colaborador.id).subscribe((resultado) => {
			this.setRemoveuFoto(resultado);
		})
	}

	iniciarDocumento(){
		this.uploaderDocumento = new FileUploader({url:PATH_DOC+"?documento="+encodeURIComponent(JSON.stringify(this.documento))});
	}
	
	consultarDocumento(id : number){
		this._colaboradorService.consultarDocumento(id).subscribe((resultado) => {
			this.documento = resultado.objetoRetorno;
		});
	}
	
	confirmDiolgExcluir(id:number) {

        this.idDocumentoRemover = id;
        this.displayDialogExcluir = true;
    }

	excluirDocumento(){
		this._colaboradorService.removerDocumento(this.idDocumentoRemover, this.colaborador.id).subscribe((resultado) => {
			this._mensagemService.showInfo("Documento removido com sucesso!");
			this.documento = new Documento();
			this.listarDocumentos();
			this.idDocumentoRemover = 0;
			this.displayDialogExcluir = false;
		});
	}
	
	novoDocumento(){
		this.documento = new Documento();
	}

	removerArquivo(id :number){
		this._colaboradorService.removerArquivo(id, this.colaborador.id).subscribe((resultado) => {
			this._mensagemService.showInfo("Removido com sucesso!");
			this.documento = resultado.objetoRetorno;
		});
	}
	
	private montarDocumento(){
		this.documento.idUsuarioAdicionou = this._loginService.getUser().id;
		this.documento.idColaborador = this.colaborador.id;
	}

	private setRemoveuFoto(resultado: Response){
		if(!resultado.sucesso){
			this._mensagemService.showInfo(resultado.objetoRetorno.mensagem);
			return;
		}

		this._mensagemService.showInfo("Removida com sucesso!");
		this.colaborador.temFoto = false;
		this.colaborador.caminhoArquivo = null;

	}

	private doCarregarListaPerfil() {
		
		if (this.possuiUsuario) {

			this.doListarPerfis();
			
			if(!this.usuario){

				this.usuario = new Usuario();
			}
		}
		else{

			this.idPerfil = 0;
		}
	}

	caminhoArquivo

	private doListarCombo(){

		this._empresaService.listarCombo().subscribe((resultado) => {
			
			this.setListaCombo(resultado)
		});
	}

	private doListarComboCargo(){
		
		this._cargoService.listarCombo().subscribe((resultado) => {
			
			this.setListaComboCargo(resultado)
		});
	}

	private recuperarColaborador(id:number){
		
		this._colaboradorService.consultarId(id).subscribe((resultado) => {
			
			this.setColaborador(resultado)
			
		},(error) => {
			
			console.log(error)
		});
	}


	private listarDocumentos(){

		/*this.paramsDocumentos = new PaginacaoParams();
		let v : FilterMetadata = event.filters;
		if(v){
			this.params.filter = v;
		}

		//Calcula a página atual
		let page = 0;
		if (event.first > 0) {
			page = event.first / event.rows;
		}

		this.paramsDocumentos.page = page;
		this.paramsDocumentos.size = event.rows;*/

		this._colaboradorService.listarDocumentos(this.colaborador.id).subscribe((resultado) => {
			this.setListaDocumentos(resultado);
		})
	}

	private setListaDocumentos(resultado: Response){
		if(!resultado.sucesso){
			this._mensagemService.showErro(resultado.mensagem);
			return;
		}

		this.listaDocumentos = <Array<Documento>>resultado.objetoRetorno;
	}

	private setColaborador(resultado : Response){

		this.colaborador =  <Colaborador>resultado.objetoRetorno;
		this.uploader = new FileUploader({url:PATH+"?id="+this.colaborador.id});
		this.iniciarDocumento()
		this.listarDocumentos();
		this.recuperarUsuario(this.colaborador.id);
		this.idEmpresa = this.colaborador.empresa.id;
		let nucleo = this.colaborador.nucleo;

		if(nucleo){
			this.idNucleo = nucleo.id;
		}

		this.idCargo = this.colaborador.cargo.id;
		if(this.colaborador.pessoa.banco){
			this.numeroBanco = this.colaborador.pessoa.banco;
		}else {
			this.numeroBanco = new NumeroBanco();
		}
		this.listarComboPorIdEmpresa();

	}

	private recuperarUsuario(idColaborador:number){

		this._usuarioService.consultarPorColaborador(idColaborador).subscribe((resultado) => {
			
			this.setUsuario(resultado)
			
		},(error) => {
			
			console.log(error)
		});
	}

	private setUsuario(resultado : Response){

		if(resultado.objetoRetorno.id){

			this.usuario = resultado.objetoRetorno;
			this.possuiUsuario = true;
			this.doCarregarListaPerfil();

		}else{

			this.possuiUsuario = false;
		}
	}

	private doMontarColaborador(){
		this.colaborador.empresa = new Empresa();
		this.colaborador.empresa.id = this.idEmpresa;
		
		if(this.idNucleo !== 0){

			this.colaborador.nucleo = new Nucleo();
			this.colaborador.nucleo.id =  this.idNucleo;
		}

		this.colaborador.cargo = new Cargo();
		this.colaborador.cargo.id = this.idCargo;
		
		this.colaborador.pessoa.tipoPessoa = EnumTipoPessoa.PF;

		this.colaborador.pessoa.banco = new Banco();
		this.colaborador.pessoa.banco.id = this.numeroBanco.id;
		this.colaborador.pessoa.banco.numero = this.numeroBanco.numero;
		this.colaborador.pessoa.banco.nome = this.numeroBanco.nome;
	}

	private doSalvar(){

		this.doMontarColaborador();
		this.isPodeSalvar = false;
		this._colaboradorService.salvar(this.colaborador, this.idPerfil).subscribe((resultado) => {
			if (resultado) {

				if(resultado.sucesso){

					this._mensagemService.showInfo("Colaborador cadastrado com sucesso!");

					this.voltar();
				} else {

					this._mensagemService.showAtencao(resultado.mensagem);
				}
			}

			this.isPodeSalvar = true;

			console.log(resultado);
		});
	}

	private setSalvou(){
		this.voltar();
		this._mensagemService.showInfo("Colaborador cadastrado com sucesso!");
	}

	private setListaCombo(resultado : Response){

		if(!resultado.sucesso){
			this._mensagemService.showAtencao("Falha de conexão, não foi possivel carregar combo!");
			return;
		}
		this.listaEmpresa = [];
		this.listaEmpresa.push({label: '-- Selecione --', value: 0})
		let listaRetorno : Array<Empresa> = resultado.objetoRetorno;
		listaRetorno.forEach(emp => this.listaEmpresa.push({label: emp.nome, value: emp.id}))

		this.doListarComboCargo();
	}

	private setListaComboCargo(resultado : Response){

		if(!resultado.sucesso){

			this._mensagemService.showAtencao("Falha de conexão, não foi possivel carregar combo!");
			return;
		}

		this.listaCargo = [];
		this.listaCargo.push({label: '-- Selecione --', value: 0})
		let listaRetorno : Array<Empresa> = resultado.objetoRetorno;
		listaRetorno.forEach(emp => this.listaCargo.push({label: emp.nome, value: emp.id}));

		this.listarNumerosBancos();
	}

	private setListaComboNucleo(resultado : Response){

		if(!resultado.sucesso){

			this._mensagemService.showAtencao("Falha de conexão, não foi possivel carregar combo!");
			return;
		}

		this.listaNucleo = [];
		this.listaNucleo.push({label: '-- Selecione --', value: 0})
		let listaRetorno : Array<Nucleo> = resultado.objetoRetorno;
		listaRetorno.forEach(nuc => this.listaNucleo.push({label: nuc.nome, value: nuc.id}))
		this.idNucleo = this.colaborador.nucleo.id;
	}

	private doListarNumerosBancos(nome?){

		this._numeroBancoService.listar(nome).subscribe((resultado) => {

			this.setListaNumerosBancos(resultado);
		});
	}

	private setListaNumerosBancos(resultado : Response){

		if(!resultado.sucesso){

			this._mensagemService.showAtencao("Falha de conexão, não foi possivel números dos bancos!");
			return;
		}

		this.listaNumeroBanco = resultado.objetoRetorno;

		if(this.colaborador && this.colaborador.id){

			this.recuperarColaborador(this.colaborador.id);
		}
	}

	private doListarPerfis(){

		this._perfilAcessoService.listarPerfis().subscribe((resultado) => {

			this.setListaPerfis(resultado)

		}, (error) => {
			console.log(error)
		})
	}

	private setListaPerfis(resultado : Response){

		if(!resultado.sucesso){

			this._mensagemService.showErro(resultado.mensagem);
			return;
		}

		if(this.usuario && this.usuario.id){

			this.idPerfil = this.usuario.idPerfil;
		}

		let listaRetorno : Array<PerfilAcesso> = resultado.objetoRetorno;
		this.listaPerfis = [];
		listaRetorno.forEach(p => this.listaPerfis.push({label: p.nome, value: p.id}) );
	}
}
