export class PaginacaoParams {
	public page : number;
	public size : number;
	public sortField : string;
	public sortOrder : string;
	public filter: any;

} 