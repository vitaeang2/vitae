export class Paginacao {
	paginaCorrente : number;
	totalConsulta : number;
	totalEntradas : number;
	entradas : Array<any>;

	constructor(){
		this.entradas = new Array<any>();
	}
}