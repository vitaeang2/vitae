import {Injectable} from '@angular/core'
import {Http, HTTP_PROVIDERS, Headers} from '@angular/http'
import {Usuario} from '../usuario/usuario'
import {HeadersService} from '../aplicacao/headers.service'
import {LoginService} from '../login/login.service'
import 'rxjs/add/operator/map'

export class UrlService {

    //url: string = "http://localhost:8080/vitae/";
	public static url: string;

    public _http : Http;
    public _headerService: HeadersService;
    public _loginService: LoginService;

    constructor(http: Http, headerService: HeadersService, loginService: LoginService) {
        this._http = http;
        this._headerService = headerService;
        this._loginService = loginService;
    }

    public call(params: string) {
        return this._http.post(UrlService.url + params, "")
        .map(res => res.json())
    }

    public callBody(sufix: string, params: string) {
        return this._http.post(UrlService.url + sufix, params)
        .map(res => res.json())
    }
    public callHeader(params: string) {
        return this._http.post(UrlService.url + params, "", this._headerService.
            getJsonHeaders(this._loginService.getToken()))
        .map(res => <any>res.json())
    }

    public callHeaderBody(sufix:string, params: any) {
        return this._http.post(UrlService.url + sufix, params, this._headerService.
            getJsonHeaders(this._loginService.getToken()))
        .map(res => <any>res.json())
    }

    public callHeaderParams(urlService: string, params: string) {
        return this._http.post(UrlService.url + urlService, params, this._headerService.
            getJsonHeaders(this._loginService.getToken()))
        .map(res => <any>res.json())
    }

}