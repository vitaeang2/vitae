import {Component} from '@angular/core';
import {Routes, ROUTER_DIRECTIVES, Router} from '@angular/router';
import {QualidadeListComponent} from './qualidade-list.component';
import {QualidadeDetailComponent} from "./qualidade-detail.component";

@Component({
	selector: 'qualidade',
	templateUrl: 'app/qualidade/qualidade.html',
	directives: [ROUTER_DIRECTIVES]
})
@Routes([
	{ path: "/", component: QualidadeListComponent},
	{ path: ":id", component: QualidadeDetailComponent}
	])
export class QualidadeComponent  {
	constructor(private _router : Router) {
		this._router.navigate(['/qualidade'])
	}
}
