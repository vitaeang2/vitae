import {Component} from '@angular/core';
import {FORM_BINDINGS, ControlGroup, FormBuilder, Validators} from '@angular/common';
import {OnActivate, RouteSegment, Router} from '@angular/router';
import {SelectItem, Dropdown, Dialog, TabView, TabPanel} from 'primeng/primeng';
import {EmpresaService} from '../empresa/empresa.service';
import {Empresa} from '../empresa/empresa';
import {Nucleo} from '../nucleo/nucleo';
import {NucleoService} from '../nucleo/nucleo.service';
import {QualidadeService} from './qualidade.service'
import {Qualidade} from '../qualidade/qualidade'
import {MensagemService} from '../aplicacao/mensagem.service';
import {Response} from '../model/response'
import {NumeroBanco} from '../numero_banco/numero_banco';
import {NumeroBancoService} from '../numero_banco/numero_banco.service'
import {Pessoa} from '../pessoa/pessoa';
import {Banco} from '../banco/banco';
import {Usuario} from '../usuario/usuario';
import {UsuarioService} from '../usuario/usuario.service';
import {CustomDatePickerComponent} from '../componentes/custom-datepicker.component';
import {MeuPanel} from '../aplicacao/panel';
import {CustomInput} from '../componentes/custom-input.component';
import {InputNumber} from '../componentes/input-number';
import {EnumEstadosBrasileiros} from '../enum/enum-estados-brasileiros';
import {EnumSexo} from '../enum/enum-sexo';
import {EnumEstadoCivil} from '../enum/enum-estado-civil';
import {EnumTipoPessoa} from '../enum/enum-tipo-pessoas';
import {PerfilAcesso} from '../perfil_acesso/perfil_acesso';
import {LoginService} from "../login/login.service";
import {PerfilAcessoService} from '../perfil_acesso/perfil_acesso.service';
import { NotEqualValidator } from '../aplicacao/not-equal-validator.directive';
import { DataValidator } from '../aplicacao/data-validator.directive';
import {TimerWrapper} from '@angular/core/src/facade/async';


@Component({
	selector: 'qualidade-detail',
	templateUrl: './app/qualidade/qualidade-detail.html',
	providers: [PerfilAcessoService, QualidadeService, NucleoService, EmpresaService, UsuarioService, NumeroBancoService],
	directives: [MeuPanel, Dropdown, CustomDatePickerComponent, CustomInput, InputNumber, Dialog, NotEqualValidator, TabView, TabPanel, DataValidator],
	viewBindings: [FORM_BINDINGS]
})
export class QualidadeDetailComponent implements OnActivate {

	mostrarDialog : boolean;

	isPodeSalvar: boolean = true;

	qualidade : Qualidade = new Qualidade();

	usuario : Usuario = new Usuario();

	listaEmpresa : SelectItem[];
	
	listaNucleo : SelectItem[];
	
	listaAutor : SelectItem[];

	idEmpresa : number = 0;
	
	idNucleo : number = 0;
	
	idAutor : number = 0;

	possuiUsuario : boolean = false;

	pt_BR: any;
	
	constructor(private _usuarioService: UsuarioService, private _autorService : UsuarioService, private _nucleoService : NucleoService, 
		private _empresaService: EmpresaService, private _qualidadeService: QualidadeService, private _loginService : LoginService,
		private _mensagemService : MensagemService, private _router: Router, builder: FormBuilder){
		this.limpar();
	}

	routerOnActivate(curr: RouteSegment) {
		let id = <number> +curr.getParam('id');
		
		this.doListarCombo();

		this.limpar();

		if(id != -1){
			this.qualidade = new Qualidade();
			this.qualidade.id = id;
			this.recuperarQualidade(id);
		}
	}

	salvar(){
		this.doSalvar();
	}

	limpar(){
		this.qualidade = new Qualidade();
		this.qualidade.autor = new Usuario();
		this.qualidade.empresa = new Empresa();
		this.qualidade.nucleo = new Nucleo();
		this.qualidade.criacao = new Date().toLocaleDateString();
		this.usuario = new Usuario();
		this.mostrarDialog = false;
		this.idEmpresa = 0;
		this.idNucleo = 0;	
		this.idAutor = this._loginService.getUser().id;
		this.possuiUsuario = false;
	}

	voltar(){
		this._router.navigate(['/qualidade']);
	}

	listarComboPorIdEmpresa(){

		if (this.idEmpresa > 0) {

			this._nucleoService.listarComboPorIdEmpresa(this.idEmpresa).subscribe((resultado) => {
				
				this.setListaComboNucleo(resultado)
				
			},(error) => {
				
				console.log(error)
			});

		} else {
			
			this.listaNucleo = [];
		}
	}

	private setListaComboNucleo(resultado : Response){

		if(!resultado.sucesso){

			this._mensagemService.showAtencao("Falha de conexão, não foi possivel carregar combo!");
			return;
		}

		this.listaNucleo = [];
		this.listaNucleo.push({label: '-- Selecione --', value: 0})
		let listaRetorno : Array<Nucleo> = resultado.objetoRetorno;
		listaRetorno.forEach(nuc => this.listaNucleo.push({label: nuc.nome, value: nuc.id}))
		this.idNucleo = this.qualidade.nucleo.id;
	}	
	private doListarCombo(){

		this._empresaService.listarCombo().subscribe((resultado) => {
			
			this.setListaCombo(resultado)
		});
	}

	private setListaCombo(resultado : Response){

		if(!resultado.sucesso){
			this._mensagemService.showAtencao("Falha de conexão, não foi possivel carregar combo!");
			return;
		}
		this.listaEmpresa = [];
		this.listaEmpresa.push({label: '-- Selecione --', value: 0})
		let listaRetorno : Array<Empresa> = resultado.objetoRetorno;
		listaRetorno.forEach(emp => this.listaEmpresa.push({label: emp.nome, value: emp.id}));

		this.doListarComboAutor();
	}

	private doListarComboAutor(){
		
		this._autorService.listarCombo().subscribe((resultado) => {
			
			this.setListaComboAutor(resultado)
		});
	}
	private setListaComboAutor(resultado : Response){

		if(!resultado.sucesso){

			this._mensagemService.showAtencao("Falha de conexão, não foi possivel carregar combo!");
			return;
		}

		this.listaAutor = [];
		this.listaAutor.push({label: '-- Selecione --', value: 0})
		let listaRetorno : Array<Usuario> = resultado.objetoRetorno;
		listaRetorno.forEach(resp => this.listaAutor.push({label: resp.colaborador.pessoa.nome, value: resp.id}));

	}

	private recuperarQualidade(id:number){
		
		this._qualidadeService.consultarId(id).subscribe((resultado) => {
			
			this.setQualidade(resultado)
			
		},(error) => {
			
			console.log(error)
		});
	}

	private setQualidade(resultado : Response){

		this.qualidade =  <Qualidade>resultado.objetoRetorno;
		this.recuperarUsuario(this.qualidade.id);
		this.idEmpresa = this.qualidade.empresa.id;
		
		let nucleo = this.qualidade.nucleo;

		if(nucleo){
			this.idNucleo = nucleo.id;
		}

		if(this.qualidade.autor){
			this.idAutor = this.qualidade.autor.id;
		}
		this.listarComboPorIdEmpresa();

	}

	private recuperarUsuario(idQualidade:number){

		/*this._usuarioService.consultarPorQualidade(idQualidade).subscribe((resultado) => {
			
			this.setUsuario(resultado)
			
		},(error) => {
			
			console.log(error)
		});*/
	}

	private setUsuario(resultado : Response){

		if(resultado.objetoRetorno.id){

			this.usuario = resultado.objetoRetorno;
			this.possuiUsuario = true;
			//this.doCarregarListaPerfil();

		}else{

			this.possuiUsuario = false;
		}
	}

	private doMontarQualidade(){
		this.qualidade.empresa = new Empresa();
		this.qualidade.empresa.id = this.idEmpresa;
		
		if(this.idNucleo !== 0){

			this.qualidade.nucleo = new Nucleo();
			this.qualidade.nucleo.id =  this.idNucleo;
		}

		this.qualidade.autor = new Usuario();
		this.qualidade.autor.id = this.idAutor;

	}

	private doSalvar(){

		this.doMontarQualidade();
		this.isPodeSalvar = false;
		this._qualidadeService.salvar(this.qualidade).subscribe((resultado) => {
			if (resultado) {

				if(resultado.sucesso){

					this._mensagemService.showInfo("Solicitação cadastrada com sucesso!");

					this.voltar();
				} else {

					this._mensagemService.showAtencao(resultado.mensagem);
					console.log(resultado);
				}
			}

			this.isPodeSalvar = true;

		});
	}

	private setSalvou(){
		this.voltar();
		this._mensagemService.showInfo("Solicitação cadastrada com sucesso!");
	}
}
