import {Injectable} from '@angular/core';
import {Http, HTTP_PROVIDERS, Headers} from '@angular/http'
import {UrlService} from '../model/url.service';
import {HeadersService} from '../aplicacao/headers.service'
import {LoginService} from '../login/login.service';
import {PaginacaoParams} from '../model/paginacao_params';
import {Qualidade} from '../qualidade/qualidade';

@Injectable()
export class QualidadeService  extends UrlService {

	constructor(_http: Http, _headerService: HeadersService, _loginService: LoginService) {
		super(_http, _headerService, _loginService);
	}

	public listar(params: PaginacaoParams) {
		return super.callHeader(`qualidade/listar?params=${JSON.stringify(params)}`);
	}
/*	public listar(params: PaginacaoParams, idEmpresa: number) {
		return super.callHeader(`qualidade/listar?params=${JSON.stringify(params)}&idEmpresa=${idEmpresa}`);
	}*/

	public listarPorNucleo(params: PaginacaoParams, idEmpresa: number, idNucleo?: number) {
		return super.callHeader(`qualidade/listar?params=${JSON.stringify(params)}&idEmpresa=${idEmpresa}&idNucleo=${idNucleo}`);
	}
	
	public listarCombo() {
		return super.callHeader(`qualidade/listarCombo`);
	}

	public consultarId(id: number) {
		return super.callHeader(`qualidade/consultarId?id=${id}`);
	}

	public salvar(qualidade: Qualidade) {
		return super.callHeaderBody(`qualidade/salvar`, JSON.stringify(qualidade));
	}

	public excluirId(id: number) {

		return super.callHeader(`qualidade/excluirId?id=${id}`);
	}
}