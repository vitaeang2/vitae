import {Usuario} from '../usuario/usuario';
import {Cargo} from '../cargo/cargo';
import {Empresa} from '../empresa/empresa';
import {Nucleo} from '../nucleo/nucleo';

export class Qualidade {

	id: number;

	ativo : boolean;

	registro : number;

	origem : number;

	auditoria : number;

	criacao : string;
	
	autor : Usuario;

	identificacao : string;

	analise_causa : string;

	acao_imediata : string;

	acao_preventiva : string;

	avaliacao_eficacia : string;
	
	empresa : Empresa;

	nucleo : Nucleo;

}