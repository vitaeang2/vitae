import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {DataTable, Column, LazyLoadEvent, Dropdown, SelectItem, Dialog} from 'primeng/primeng';
import {MaterialDesignDirective} from "../aplicacao/material";
import {Qualidade} from "./qualidade";
import {QualidadeService} from "./qualidade.service"
import {Nucleo} from "../nucleo/nucleo";
import {NucleoService} from '../nucleo/nucleo.service';
import {MensagemService} from "../aplicacao/mensagem.service"
import {LoginService} from "../login/login.service"
import {PaginacaoParams} from '../model/paginacao_params';
import {Paginacao} from '../model/paginacao';
import {Response} from '../model/response';
import {VitaeLazy} from '../aplicacao/vitae-lazy.service';

@Component({
    selector: 'qualidade-list',
    templateUrl: './app/qualidade/qualidade-list.html',
    directives: [DataTable, Column, Dropdown, Dialog],
    providers: [QualidadeService, NucleoService]
})
export class QualidadeListComponent extends VitaeLazy  implements OnInit {

    params : PaginacaoParams;
    paginacao : Paginacao = new Paginacao();
    listaNucleo : SelectItem[];
    idNucleoSelecionado : number;
    mostarFiltroNucleo :boolean = false;
    idNucleo : number = 0;

    private idQualidadeSelecionado: number = 0;

    public displayDialogExcluir: boolean = false;

    constructor(private _nucleoService: NucleoService, private _qualidadeService : QualidadeService, private _loginService : LoginService, private _mensagemService : MensagemService, private router: Router){
        super(router);
    }

    ngOnInit(){
        //let nucleo: Nucleo = this._loginService.getUser().qualidade.nucleo;
        let nucleo: Nucleo = null;

/*        if (nucleo && nucleo.id) {
            this.idNucleo = nucleo.id;
            this.mostarFiltroNucleo = false;

        } else {

            this.idNucleo = 0;
            this.mostarFiltroNucleo = true;
            this.carregarListaNucleo();
        }*/

        this.doListar();
    }

    novo () {
        super.novo('/qualidade/');
    }

    editar(id: number){

        super.editar(id, '/qualidade/');
    }

    doListar () {
        if(this.params){
            if(this.mostarFiltroNucleo){

                if(this.idNucleoSelecionado && this.idNucleoSelecionado != 0){

                    this.listarPorNucleo(this.idNucleoSelecionado);

                }else{

                    this.listarPorNucleo(this.idNucleo);
                }

            }else{

                this.listar();
            }
        }
    }

    listarPorNucleo(idNucleo: number){

/*        this._qualidadeService.listarPorNucleo(this.params, this._loginService.getUser().qualidade.empresa.id, idNucleo).subscribe((resultado) => {

            this.setListaQualidade(resultado)
        });*/
    }

    private listar(){
        //this._qualidadeService.listar(this.params, this._loginService.getUser().qualidade.empresa.id).subscribe((resultado) => {
        this._qualidadeService.listar(this.params).subscribe((resultado) => {

            this.setListaQualidade(resultado)
        });
    }

    private setListaQualidade(resultado : Response){

        if(!resultado.sucesso){

            this._mensagemService.showAtencao(resultado.mensagem);
            return;
        }

        this.paginacao = resultado.objetoRetorno;
    }

    private carregarListaNucleo(){

        //this._nucleoService.listarComboPorIdEmpresa(this._loginService.getUser().qualidade.empresa.id).subscribe((resultado) => {
        this._nucleoService.listarComboPorIdEmpresa(null).subscribe((resultado) => {

            this.setListaNucleo(resultado)
        });
    }

    private setListaNucleo(resultado : Response){

        if(!resultado.sucesso){

            this._mensagemService.showAtencao(resultado.mensagem);
            return;
        }
        
        this.listaNucleo = [];
        let lista : Array<Nucleo> = resultado.objetoRetorno;
        this.listaNucleo.push({label: "-- TODOS --", value: 0});
        lista.forEach(nuc => this.listaNucleo.push({label: nuc.nome, value: nuc.id}));
    }

    confirmDiolgExcluir(id:number) {

        this.idQualidadeSelecionado = id;
        this.displayDialogExcluir = true;
    }

    excluir() { 

        this._qualidadeService.excluirId(this.idQualidadeSelecionado).subscribe((resultado) => {

            this._mensagemService.showInfo("Qualidade excluído com sucesso!");
            this.displayDialogExcluir = false;
            this.doListar();
        });

    }
}
