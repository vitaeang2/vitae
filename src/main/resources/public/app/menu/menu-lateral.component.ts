import {Component, OnInit, Input, EventEmitter} from '@angular/core'
import {Routes, ROUTER_DIRECTIVES, Router} from '@angular/router'
//import {MATERIAL_DIRECTIVES, MATERIAL_PROVIDERS} from "ng2-material/all";
import {Menu} from '../menu/menu';
import {MenuService} from '../menu/menu.service';
import {MensagemService} from '../aplicacao/mensagem.service';
import {MenuPipe} from '../menu/menu-pipe';
import {LoginComponent} from '../login/login.component';
import {LoginService} from '../login/login.service';
import {DashboardComponent} from '../aplicacao/dashboard.component';
import {EmpresaDetailComponent} from '../empresa/empresa-detail.component';
import {EmpresaListComponent} from '../empresa/empresa-list.component';
import {Response} from '../model/response';
import {EmpresaComponent} from '../empresa/empresa.component'

@Component({
    selector: 'menu-lateral',
    templateUrl: 'app/menu/menu-lateral.html',
    pipes: [MenuPipe],
    directives: [ROUTER_DIRECTIVES]
})
export class MenuLateralComponent implements OnInit {

    public mostrarMenu: boolean;

    public menuDisplay: string;
    public mostrarMenuDisplay: boolean;

    public dropdownDisplay: string;
    public mostrarDropdownDisplay: boolean;
    public btn: string;

    constructor(private _menuService: MenuService, private _loginService: LoginService, 
        private _mensagemService: MensagemService, private _router: Router) {
    }

    ngOnInit() {
        this.mostrarDropdownDisplay = false;
        this.menuDisplay = '';
        this.btn = 'btn-default';
        this.mostrarMenu = false;
        this.mostrarMenuDisplay = false;
        this.dropdownDisplay = 'none';
    }

    public expande(menus: Menu[], name: string) {
        this.doExpande(menus, name);
    }

    doExpande(menus: Menu[], name: string) {
        menus.forEach(menu => {
            if (menu.name == name && menu.submenus != null) {
                menu.mostrar = menu.mostrar == 'block' ? 'none' : 'block';
            } else if (menu.submenus != null) {
                menu.mostrar = 'none';
            }
        });
    }

    expandeDropdown() {
        this.mostrarDropdownDisplay = !this.mostrarDropdownDisplay;
        this.dropdownDisplay = this.mostrarDropdownDisplay ? "block" : "none";
        this.btn = this.mostrarDropdownDisplay ? "btn-focus" : "btn-default";
    }

    expandeMenu() {
        this.mostrarMenuDisplay = !this.mostrarMenuDisplay;
        this.menuDisplay = this.mostrarMenuDisplay ? "active" : "";
    }

    consultarPermissao(idPagina :number) {
        this._menuService.consultarAcessoPagina(this._loginService.getUser().idPerfil, idPagina)
        .subscribe((resultado) => {
            this.setPermissao(resultado)
        },(error) => {
            console.log(error)
        });
    }

    logout(){
        this._loginService.logout();
        //this._router.navigateByUrl("/index");
        location.reload();
        //this._router.navigate(['/'])
    }

    private setPermissao(resultado : Response){
        if(!resultado.sucesso){
            this._mensagemService.showAtencao(resultado.mensagem);
            return;
        }
        this._loginService.setPermissao(resultado.objetoRetorno);
    }

}
