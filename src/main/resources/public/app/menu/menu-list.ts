import {Menu} from "./menu"

export var ListaMenu: Menu[] = [
    {id: 1, name: "Dashboard", icon: "glyphicon-shopping-cart", caminho: "Dashboard"},
    {id: 1, name: "Cadastro", icon: "glyphicon-list-alt color-txt-b3", mostrar: "none", submenus: [
        {id: 1, name: "Empresa", caminho:"Empresa"},
        {id: 1, name: "Colaborador", caminho:"Empresa"},
        {id: 1, name: "Perfil de acesso", caminho:"Empresa"},
        {id: 1, name: "Produto", caminho:"Empresa"},
        {id: 1, name: "Fornecedores", caminho:"Empresa"}
    ]},
    {id: 1, name: "Estoque", icon: "glyphicon-shopping-cart", caminho: "Empresa"},
    {id: 1, name: "Cotação", icon: "glyphicon-usd", caminho: "Empresa"},
    {id: 1, name: "Pedido de Compra", icon: "glyphicon-edit", caminho: "Empresa"},
    {id: 1, name: "Patrimônio", icon: "glyphicon-blackboard color-txt-b3",  mostrar: "none", submenus: [
        {id: 1, name: "Categoria", caminho:"Empresa"},
        {id: 1, name: "Equipamentos / Históricos", caminho:"Empresa"}
    ]},
    {id: 1, name: "Solicitação", icon: "glyphicon-info-sign color-txt-b3", mostrar: "none", submenus: [
        {id: 1, name: "Suprimento", caminho: "Empresa"},
        {id: 1, name: "Qualidade", caminho: "Empresa"}
    ]},
    {id: 1, name: "Ordem de Serviço", icon: "glyphicon-folder-close color-txt-b3", caminho: "Empresa"}
];