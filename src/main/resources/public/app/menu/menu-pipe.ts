import {Menu} from './menu';
import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'MenuPipe'
})
export class MenuPipe  {
    transform(items : Menu[], filtro :string ) {
        // filter items array, items which match and return true will be kept, false will be filtered out
        return items.filter(item => item.name.indexOf(filtro) !== -1);
    }
}