export interface Menu {
	id: number;
    name: string;
    icon?: string;
    caminho?: string;
    mostrar?: string;
    submenus?: Menu[];
}