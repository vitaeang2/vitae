import {Http, HTTP_PROVIDERS, Headers} from "@angular/http";
import {Pipe, Injectable} from "@angular/core";
import {HeadersService} from '../aplicacao/headers.service';
import {Menu} from "../menu/menu";
import {LoginService} from '../login/login.service';
import {UrlService} from '../model/url.service';

@Injectable()
export class MenuService extends UrlService {
	
	private urlNegocio : string = 'menu';

    public menus: Array<Menu>;
    public mostrarMenu = false;

    constructor(_http: Http, _headerService: HeadersService, _loginService: LoginService) { 
    	super(_http, _headerService, _loginService);
    }

    itensMenu(){
        return this.menus;
    }

    public getMenu(idPerfil : number) {
        return super.callHeader(`${this.urlNegocio}/listarMenu?idPerfil=` + idPerfil);
    }

    public listarPaginasSelecionadas(idPerfil : number) {
        return super.callHeader(`${this.urlNegocio}/listarPaginasSelecionadas?idPerfil=` + idPerfil);
    }

    public listarPaginasNaoSelecionadas(idPerfil : number) {
        return super.callHeader(`${this.urlNegocio}/listarPaginasNaoSelecionadas?idPerfil=` + idPerfil);
    }

    public consultarAcessoPagina(idPerfil : number, idPagina : number) {
        return super.callHeader(`${this.urlNegocio}/consultarAcessoPagina?idPerfil=${idPerfil}&idPagina=${idPagina}`);
    }

}
