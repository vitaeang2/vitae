import {Component} from '@angular/core';
import {FORM_BINDINGS, ControlGroup, FormBuilder, Validators} from '@angular/common';
import {Router} from '@angular/router';
import {UsuarioService} from '../usuario/usuario.service';
import {Usuario} from '../usuario/usuario';
import {LoginService} from '../login/login.service';
import {MenuService} from '../menu/menu.service';
import {md5} from '../aplicacao/md5';
import {Menu} from '../menu/menu';
import {Response} from '../model/response';
import {HeadersService} from '../aplicacao/headers.service';
import {MensagemService} from '../aplicacao/mensagem.service';
import {Dialog} from 'primeng/primeng';
import {InputValidate} from '../componentes/input-validate';
import {InputNumber} from '../componentes/input-number';
import {CustomInput} from '../componentes/custom-input.component';
import {CustomDatePickerComponent} from '../componentes/custom-datepicker.component';

@Component({
    selector: "app",
    templateUrl: 'app/login/login.html',
    styleUrls: ['./app/login/styleLogin.css'],
    directives: [Dialog],
    viewBindings: [FORM_BINDINGS]
})
export class LoginComponent {

    loginForm: ControlGroup;

    mostrarEsqueceuSenha : boolean = false;
    email : string ;

    private usuario: Usuario = new Usuario();
    private showLoading: boolean = false;
    private errorMessage: string = null;

    constructor(private _menuService: MenuService, private _userService: UsuarioService, 
        private _loginService: LoginService, private _mensagemService : MensagemService,
        private _router: Router, builder: FormBuilder) {

        this.loginForm = builder.group({
            login: ["", Validators.required],
            /*telefone: ["", Validators.required],
            cpf: ["", Validators.required],*/
            senha: ["", Validators.required]
        });

      //  this.usuario.login = "sergioadsf@gmail.com";
      //  this.usuario.senha = "123";

    }

    onClick(event) {
        event.preventDefault();
        this.showLoading = true;
        this.errorMessage = null;

        this.usuario.senhaMd5 = md5(this.usuario.senha);
        this._userService.conectar(this.usuario).subscribe((result) => {
            this.onLoginResult(result)
        });
    }

    onLoginResult(result) {
        if (!result.sucesso) {
            this.onLoginError(result.mensagem);
            return;
        }
        this.usuario = <Usuario>result.objetoRetorno;
        this.carregarMenu(this.usuario);
    }

    carregarMenu(usuario){
        this._menuService.getMenu(usuario.idPerfil)
        .subscribe(
            result => this.setOnMenu(result, usuario)
            )
    }

    abrirEsqueceuSenha(){
        this.mostrarEsqueceuSenha = true;
    }

    fecharEsqueceuSenha(){
        this.email = "";
        this.mostrarEsqueceuSenha = false;
    }

    esqueciSenha(){
        this._mensagemService.showInfo("Será enviado um email, aguarde!");
        this._userService.esqueciSenha(this.email).subscribe((resultado) => {
            this.setEsqueciSenha(resultado)
        });
    }

    private setEsqueciSenha(resultado : Response){
        this._mensagemService.showInfo(resultado.objetoRetorno);
        this.fecharEsqueceuSenha();
    }

    private setOnMenu(result, usuario : Usuario){
        this._menuService.menus = <Array<Menu>> result.objetoRetorno;
        this._loginService.setLogin(usuario, usuario.token);
        if(usuario.primeiroAcesso){
            this._router.navigate(['/alterarsenha']);
            return ;
        }
        this._router.navigate(['/dashboard']);
    }

    onLoginError(mensagem) {
        //this.errorMessage = mensagem;
        //this.showLoading = false;
        this._mensagemService.showAtencao(mensagem);
    }

    get value(): string {
        return JSON.stringify(this.loginForm.value, null, 2);
    }

}
