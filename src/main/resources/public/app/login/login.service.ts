import {Http, HTTP_PROVIDERS} from '@angular/http'
import {Injectable} from '@angular/core'
import {Usuario} from '../usuario/usuario'
import {PaginaPermissao} from '../perfil_acesso/pagina_permissao'
import 'rxjs/add/operator/map'

@Injectable()
export class LoginService {

    private user: Usuario = null;
    private permissao : PaginaPermissao = new PaginaPermissao();
    private token: string = null;
    public mostra: boolean = false;

    constructor() { }

    setLogin(u: Usuario, t: string) {
        this.user = u;
        this.token = t;
    }

    getToken(): string {
        return this.token;
    }

    getUser() {
    	if(!this.user){
   		 throw new Error();
    	}
    	
        return this.user;
    }

    setSenha(senha : string) {
        this.user.senha = senha;
    }

    setPrimeiroAcesso(isPrimeiro : boolean) {
        this.user.primeiroAcesso = isPrimeiro;
    }

    isLogged() {
        return this.user != null && this.token != null;
    }
    
    logout() {
        this.user = null;
        this.token = null;
    }

    setPermissao(permissao : PaginaPermissao){
        this.permissao = permissao;
    }

    getPermissao() {
        return this.permissao;
    }
}
