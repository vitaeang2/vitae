import {Component} from '@angular/core';
import {Routes, ROUTER_DIRECTIVES, Router} from '@angular/router';
import {ProdutoListComponent} from './produto-list.component';
import {ProdutoDetailComponent} from "./produto-detail.component";
import {MovimentoEstoqueComponent} from "../movimento_estoque/movimento_estoque.component";


@Component({
	selector: 'produto',
	templateUrl: 'app/produto/produto.html',
	directives: [ROUTER_DIRECTIVES]
})

@Routes([

	{ path: "/", component: ProdutoListComponent},
	{ path: ":id", component: ProdutoDetailComponent},
	{ path: "/estoque/:id/", component: MovimentoEstoqueComponent}
		
	])
export class ProdutoComponent  {

	constructor(private _router : Router) {

		this._router.navigate(['/produto'])
	}
}
