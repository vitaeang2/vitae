import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {DataTable, Column, LazyLoadEvent, Button, FilterMetadata, Dialog} from 'primeng/primeng';
import {MaterialDesignDirective} from "../aplicacao/material";
import {Produto} from "./produto";
import {ProdutoService} from "./produto.service"
import {MensagemService} from "../aplicacao/mensagem.service"
import {LoginService} from "../login/login.service"
import {PaginacaoParams} from '../model/paginacao_params';
import {Paginacao} from '../model/paginacao';
import {Response} from '../model/response';
import {VitaeLazy} from '../aplicacao/vitae-lazy.service';
import {MoneyPipe} from '../componentes/money_pipe';
import {DoublePipe} from '../componentes/double_pipe';

@Component({
    selector: 'produto',
    templateUrl: './app/produto/produto-list.html',
    directives: [DataTable, Column, Button, Dialog],
    providers: [ProdutoService],
    pipes: [MoneyPipe, DoublePipe]
})

export class ProdutoListComponent extends VitaeLazy implements OnInit {

    idProdutoSelecionado : number;
    displayDialogExcluir: boolean;

    constructor(private _produtoService : ProdutoService, private _loginService : LoginService, private _mensagemService : MensagemService, private router: Router) {

        super(router);
    }

    ngOnInit(){
        this.displayDialogExcluir = false;
        this.doListar();
    }

    novo() {

        super.novo('/produto/');
    }

    editar(id: number) {

        super.editar(id, '/produto/');
    }
    
    visualizarEstoque(id: number) {

        super.goTo(id, '/estoque/');
    }

    confirmDiolgExcluir(id:number) {

        this.idProdutoSelecionado = id;
        this.displayDialogExcluir = true;
    }

    excluir() { 

        this._produtoService.excluirId(this.idProdutoSelecionado).subscribe((resultado) => {

            this._mensagemService.showInfo("Produto excluído com sucesso!");
            this.displayDialogExcluir = false;
            this.doListar();
        });

    }

    doListar() {
        if(this.params){
            this._produtoService.listar(this.params).subscribe((resultado) => {

                this.setListaProduto(resultado)
            });
        }
    }

    private setListaProduto(resultado : Response) {

        if (!resultado.sucesso) {

            this._mensagemService.showAtencao(resultado.mensagem);
            
            return;
        }

        this.paginacao = resultado.objetoRetorno;
    }


}
