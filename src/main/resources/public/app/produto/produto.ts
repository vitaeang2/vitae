import {Pessoa} from '../pessoa/pessoa'
import {Cargo} from '../cargo/cargo'
import {Fornecedor} from '../fornecedor/fornecedor'
import {Fabricante} from '../fabricante/fabricante'
import {CategoriaProduto} from '../categoria_produto/categoria_produto'
import {ProdutoEstoque} from './produto_estoque'
import {EnumUnidadeMedida} from '../enum/enum-unidade-medida'

export class Produto {

	id: number;

	selecionado : boolean;

	nome : string;

	dataCadastro : string;

	codigo : string;

	valorVenda : string;

	valorCusto : string;

	//produtoEstoque : ProdutoEstoque;

	minimoEmEstoque : number;

	maximoEmEstoque : number;

	unidadeMedida: number;

	fornecedor : Fornecedor;

	fabricante : Fabricante;

	categoria : CategoriaProduto;

	unidadeMedidaStr: string; 
	
	ativo: boolean; 
}
