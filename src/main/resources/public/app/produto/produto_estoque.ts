import {Produto} from '../produto/produto'
import {Empresa} from '../empresa/empresa'
import {Nucleo} from '../nucleo/nucleo'

export class ProdutoEstoque {

	id: number;

	nome : string;

	quantidade : number;

	produto : Produto;
	
	empresa : Empresa;

	nucleo : Nucleo;
}
