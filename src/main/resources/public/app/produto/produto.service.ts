import {Injectable} from '@angular/core';
import {OnActivate, RouteSegment, Router} from '@angular/router';
import {Http, HTTP_PROVIDERS, Headers} from '@angular/http'
import {UrlService} from '../model/url.service';
import {HeadersService} from '../aplicacao/headers.service'
import {LoginService} from '../login/login.service';
import {PaginacaoParams} from '../model/paginacao_params';
import {Produto} from '../produto/produto';
import {MensagemService} from '../aplicacao/mensagem.service';

@Injectable()
export class ProdutoService  extends UrlService {

	private urlNegocio : string = 'produto';

	constructor(_http: Http, _headerService: HeadersService, _loginService: LoginService) {
		super(_http, _headerService, _loginService);
	}

	public listar(params: PaginacaoParams) {
		return super.callHeader(`${this.urlNegocio}/listar?params=${JSON.stringify(params)}`);
	}

	public listarEstoqueBaixo(params: PaginacaoParams, idEmpresa : number, idNucleo? : number) {
		return super.callHeader(`${this.urlNegocio}/listarEstoqueBaixo?params=${JSON.stringify(params)}&idEmpresa=${idEmpresa}&idNucleo=${idNucleo}`);
	}

	public listarCombo() {
		return super.callHeader(`${this.urlNegocio}/listarCombo`);
	}

	public listarFiltroCotacao(filtro: string) {
		return super.callHeader(`${this.urlNegocio}/listarFiltroCotacao?nome=${filtro}`);
	}

	public consultarId(id: number) {
		return super.callHeader(`${this.urlNegocio}/consultarId?id=${id}`);
	}

	public salvar(produto: Produto, idUsuario : number) {
		return super.callHeaderBody(`${this.urlNegocio}/salvar?idUsuario=${idUsuario}`, JSON.stringify(produto));
	}

	public excluirId(id: number) {

		return super.callHeader(`${this.urlNegocio}/excluirId?id=${id}`);
	}

}
