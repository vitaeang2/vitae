import {Component, OnInit} from '@angular/core';
import {OnActivate, RouteSegment, Router} from '@angular/router';
import {Dialog, Dropdown, SelectItem} from 'primeng/primeng';
import {MensagemService} from '../aplicacao/mensagem.service';
import {LoginService} from '../login/login.service';
import {Produto} from '../produto/produto';
import {ProdutoService} from '../produto/produto.service';
import {ProdutoEstoque} from '../produto/produto_estoque';
import {MeuPanel} from '../aplicacao/panel';
import {CategoriaProduto} from '../categoria_produto/categoria_produto';
import {CategoriaProdutoDialogComponent} from '../categoria_produto/categoria_produto-dialog.component';
import {CategoriaProdutoService} from '../categoria_produto/categoria_produto.service';
import {Fabricante} from '../fabricante/fabricante';
import {FabricanteDialogComponent} from '../fabricante/fabricante-dialog.component';
import {FabricanteService} from '../fabricante/fabricante.service';
import {Fornecedor} from '../fornecedor/fornecedor';
import {FornecedorService} from '../fornecedor/fornecedor.service';
import {Response} from '../model/response';
import {CustomInput} from '../componentes/custom-input.component';
import {EnumUnidadeMedida} from '../enum/enum-unidade-medida';
import { NotEqualValidator } from '../aplicacao/not-equal-validator.directive';
import {TimerWrapper} from '@angular/core/src/facade/async';

@Component({
	templateUrl: './app/produto/produto-detail.html',
	directives: [Dialog, Dropdown, MeuPanel, CategoriaProdutoDialogComponent, FabricanteDialogComponent, CustomInput, NotEqualValidator],
	providers: [ProdutoService, CategoriaProdutoService, FabricanteService, FornecedorService]
})
export class ProdutoDetailComponent implements OnActivate {

	produto : Produto = new Produto();

	idCategoria : number = 0;

	idFabricante : number = 0;

	idFornecedor : number = 0;

	idUnidade : number = 0;

	mostrarDialogCategoria : boolean = false;

	mostrarDialogFabricante : boolean = false;

	listaCategoria : SelectItem[];

	listaFabricante : SelectItem[];

	listaFornecedor : SelectItem[];

	listaUnidades : SelectItem[];

	isEnviado = false;

	constructor(private _produtoService : ProdutoService,private _fornecedorService : FornecedorService, 
		private _loginService: LoginService, private _fabricanteService : FabricanteService, private _categoriaProdutoService : CategoriaProdutoService, 
		private _mensagemService : MensagemService, private _router: Router) {
	}

	routerOnActivate(curr: RouteSegment) {

		this.isEnviado = false;
		let id = <number> +curr.getParam('id');
		this.doListaComboCategoria();
		

		if (id != -1) {
			this.produto.id = id;
			//this.recuperarProduto(id);

		} else {

			this.limpar();
		}
	}

	recuperarProduto(id : number){

		this._produtoService.consultarId(id).subscribe((resultado) => {

			this.setProduto(resultado);

		},(error) => {

			console.log(error)
		});
	}

	limpar(){

		let codigoUnidade = parseInt(EnumUnidadeMedida.QUILOGRAMA.codigo);
		this.produto = new Produto();
		this.produto.unidadeMedida = codigoUnidade;
		this.idUnidade = codigoUnidade;
	}

	showDialogCategoria(){
		this.mostrarDialogCategoria = true;
	}

	closeDialogCategoria(){
		this.mostrarDialogCategoria = false;
		this.doListaComboCategoria();
	}

	showDialogFabricante(){
		this.mostrarDialogFabricante = true;
	}

	closeDialogFabricante(){
		this.mostrarDialogFabricante = false;
		this.doListaComboFabricante();
	}

	salvar() {
		if(this.validarTela() == true){
			this.doSalvar();
		}
	}

	private doSalvar(){
		this.doMontarProduto();
		this.isEnviado = true;
		this._produtoService.salvar(this.produto, this._loginService.getUser().id).subscribe((resultado) => {
			this._mensagemService.showInfo("Produto cadastrado com sucesso!");
			TimerWrapper.setTimeout(() => {  
				this.voltar();
				this.isEnviado = false;
			}, 2000);
		})
	}

	private setProduto(resultado : Response){

		this.produto =  <Produto>resultado.objetoRetorno;
		
		if(this.produto.categoria && this.produto.categoria.id){

			this.idCategoria = this.produto.categoria.id;
		}

		if(this.produto.fabricante && this.produto.fabricante.id){
			this.idFabricante = this.produto.fabricante.id;
		}

		if(this.produto.unidadeMedida){

			this.idUnidade = this.produto.unidadeMedida;
		}

		if(this.produto.fornecedor && this.produto.fornecedor.id) {
		
			this.idFornecedor = this.produto.fornecedor.id;
		}
	}

	private doMontarProduto(){

		this.produto.fornecedor = new Fornecedor();
		this.produto.fornecedor.id = this.idFornecedor;

		this.produto.fabricante = new Fabricante();
		this.produto.fabricante.id = this.idFabricante;

		this.produto.categoria = new CategoriaProduto();
		this.produto.categoria.id = this.idCategoria;

		this.produto.unidadeMedida = this.idUnidade;
	}

	private validarTela() : boolean{
		return true;
	}

	private listarUnidades(){
		this.listaUnidades = EnumUnidadeMedida.toSelectItem();
		if(this.produto && this.produto.id){
			this.recuperarProduto(this.produto.id);
		}
	}

	private doListaComboCategoria(){
		this._categoriaProdutoService.listarCombo().subscribe((resultado) => {
			this.setListaComboCategoria(resultado);
		}) ;
	}

	private setListaComboCategoria(resultado : Response){
		if(!resultado.sucesso){
			return;
		}
		let listaRetorno : Array<CategoriaProduto> = resultado.objetoRetorno;
		this.listaCategoria = [];
		listaRetorno.forEach(p => this.listaCategoria.push({label: p.nome, value: p.id}) );
		this.doListaComboFabricante();
	}

	private doListaComboFabricante(){
		this._fabricanteService.listarCombo().subscribe((resultado) => {
			this.setListaComboFabricante(resultado);
		}) ;
	}

	private setListaComboFabricante(resultado : Response){
		if(!resultado.sucesso){
			return;
		}
		let listaRetorno : Array<Fabricante> = resultado.objetoRetorno;
		this.listaFabricante = [];
		listaRetorno.forEach(p => this.listaFabricante.push({label: p.nome, value: p.id}) );
		this.doListaComboFornecedor();
	}

	private doListaComboFornecedor(){
		this._fornecedorService.listarCombo().subscribe((resultado) => {
			this.setListaComboFornecedor(resultado);
		}) ;
	}

	private setListaComboFornecedor(resultado : Response){

		if(!resultado.sucesso){

			return;
		}

		let listaRetorno : Array<Fornecedor> = resultado.objetoRetorno;
		this.listaFornecedor = [];
		listaRetorno.forEach(p => this.listaFornecedor.push({label: p.pessoa.nome, value: p.id}) );
		this.listarUnidades();
	}

	voltar(){
		this._router.navigate(['/produto']);
	}
}
