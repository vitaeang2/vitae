import {Component} from '@angular/core';
import {Routes, ROUTER_DIRECTIVES, Router} from '@angular/router';
import {GrupoListComponent} from './grupo-list.component';
import {GrupoDetailComponent} from "./grupo-detail.component";

@Component({
	selector: 'grupo',
	templateUrl: 'app/grupo/grupo.html',
	directives: [ROUTER_DIRECTIVES]
})
@Routes([
	{ path: "/", component: GrupoListComponent},
	{ path: ":id", component: GrupoDetailComponent}
	])

export class GrupoComponent  {
	constructor(private _router : Router) {
		this._router.navigate(['/grupo'])
	}
}
