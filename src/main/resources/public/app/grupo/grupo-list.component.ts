import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {DataTable, Column, LazyLoadEvent, Button,Dialog} from 'primeng/primeng';
import {MaterialDesignDirective} from "../aplicacao/material";
import {Grupo} from "./grupo";
import {GrupoService} from "./grupo.service"
import {MensagemService} from "../aplicacao/mensagem.service"
import {LoginService} from "../login/login.service"
import {PaginacaoParams} from '../model/paginacao_params';
import {Paginacao} from '../model/paginacao';
import {Response} from '../model/response';

@Component({
    selector: 'grupo-list',
    templateUrl: './app/grupo/grupo-list.html',
    directives: [DataTable, Column, Button, Dialog],
    providers: [GrupoService]
})
export class GrupoListComponent implements OnInit {

    params: PaginacaoParams;
    paginacao: Paginacao = new Paginacao();

    private itemSelecionadoId: number = 0;

    public displayDialogExcluir: boolean = false;

    public displayDialogExcluirGrupoViculoSolicitacao: boolean = false;

    constructor(private _grupoService: GrupoService, private _loginService: LoginService, private _mensagemService: MensagemService, private _router: Router) {
    }

    ngOnInit() {
        this.doListar();
    }

    novo() {
        this._router.navigate(['/grupo/', -1])
    }

    editar(id: number) {
        this._router.navigate(['/grupo/', id])
    }

    load(event: LazyLoadEvent) {

        this.params = new PaginacaoParams();

        //Calcula a página atual
        let page = 0;
        if (event.first > 0) {
            page = event.first / event.rows;
        }

        this.params.page = page;
        this.params.size = event.rows;
        this.params.sortField = event.sortField;
        this.params.sortOrder = (event.sortOrder == -1) ? 'desc' : 'asc';

        this.doListar();
    }

    private doListar() {
        if (this.params) {
            this._grupoService.listar(this.params).subscribe((resultado) => {
                this.setListaGrupo(resultado)
            });
        }
    }

    private setListaGrupo(resultado: Response) {
        if (!resultado.sucesso) {
            this._mensagemService.showAtencao(resultado.mensagem);
            return;
        }
        this.paginacao = resultado.objetoRetorno;
    }

    confirmDiolgExcluir(id: number) {

        this.itemSelecionadoId = id;
        this.displayDialogExcluir = true;
    }

    excluir(verificarGrupoViculadaSolicitacao: boolean) {

        this._grupoService.excluirId(this.itemSelecionadoId, verificarGrupoViculadaSolicitacao).subscribe((resultado: Response) => {

            if (!resultado.sucesso) {
                this.displayDialogExcluirGrupoViculoSolicitacao = true;
            } else {
                this._mensagemService.showInfo("Grupo excluído com sucesso!");
                this.displayDialogExcluirGrupoViculoSolicitacao = false;
                this.doListar();
            }
            this.displayDialogExcluir = false;              
        });

    }
}
