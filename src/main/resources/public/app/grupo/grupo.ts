import {CategoriaGrupo} from '../categoria_grupo/categoria_grupo';

export class Grupo {

	id: number;

	nome : string;

	ativo: boolean;	

	categoriaGrupo : Array<CategoriaGrupo>;
}
