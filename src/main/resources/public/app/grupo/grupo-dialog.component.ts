import {Component, OnInit} from '@angular/core';
import {Grupo} from './grupo';
import {GrupoService} from './grupo.service';
import {MeuPanel} from '../aplicacao/panel';
import {MensagemService} from '../aplicacao/mensagem.service';
import {Response} from '../model/response';

import {TimerWrapper} from '@angular/core/src/facade/async';

@Component({
	selector: 'grupo-dialog',
	templateUrl: './app/grupo/grupo-dialog.html',
	directives: [MeuPanel],
	providers: [GrupoService]
})
export class GrupoDialogComponent implements OnInit{
	
	grupo : Grupo; 

	isSucesso: boolean;

	isResposta: boolean;

	mensagem: string;

	constructor(private _grupoServico : GrupoService, private _mensagemService : MensagemService){}

	ngOnInit() {
		this.grupo = new Grupo();
	}

	salvar(){
		this.doSalvar();
	}

	limpar(){
		this.grupo = new Grupo();
	}

	private doSalvar(){
		this._grupoServico.salvar(this.grupo).subscribe((resultado) => {
			
			if(resultado.sucesso){
				this._mensagemService.showInfo("Grupo cadastrado com sucesso!");
				this.setGrupo(resultado);				
			} else {
				this._mensagemService.showInfo(resultado.mensagem);
			}
		});
	}

	private setGrupo(resultado : Response){
		this.isResposta = true;
		if(!resultado.sucesso){
			this.mensagem = resultado.mensagem;
			return;
		}

		this.isSucesso = true;
		this.mensagem = "Grupo salvo com sucesso!";

		this._mensagemService.showInfo("Grupo salvo com sucesso!");

		TimerWrapper.setTimeout(() => {  
			this.isResposta = false;
			this.isSucesso = false;
			this.mensagem = "";
			this.grupo = new Grupo();
		}, 2000);
	}
}