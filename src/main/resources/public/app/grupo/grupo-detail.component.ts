import {Component} from '@angular/core';
import {OnActivate, RouteSegment, Router} from '@angular/router';
import {DataTable, Column, LazyLoadEvent, SelectItem, Dropdown, Dialog, TabView, TabPanel} from 'primeng/primeng';
import {GrupoService} from './grupo.service'
import {Grupo} from '../grupo/grupo'
import {CategoriaGrupo} from '../categoria_grupo/categoria_grupo';
import {CategoriaGrupoService} from '../categoria_grupo/categoria_grupo.service';
import {MensagemService} from '../aplicacao/mensagem.service';
import {Response} from '../model/response'
import {MeuPanel} from '../aplicacao/panel';

@Component({
	selector: 'grupo-detail',
	templateUrl: './app/grupo/grupo-detail.html',
	providers: [GrupoService, CategoriaGrupoService],
	directives: [Dialog, Dropdown, MeuPanel, TabView, TabPanel]
})
export class GrupoDetailComponent implements OnActivate {

	grupo : Grupo = new Grupo();

	categoria_grupo : CategoriaGrupo = new CategoriaGrupo();

	public displayDialogExcluirCategoria: boolean = false;

	private categoriaSelecionadoId: number = 0;

	constructor(private _grupoService: GrupoService, private _categoriaGrupoService : CategoriaGrupoService, private _mensagemService : MensagemService, private _router: Router){
	}

	routerOnActivate(curr: RouteSegment) {
		let id = <number> +curr.getParam('id');

		if(id != -1){
			this.recuperarGrupo(id);
		}else{
			this.limpar();
		}
	}

	salvar(){

		this.doSalvar();

	}

	limpar(){
		this.grupo = new Grupo();

		this.grupo.categoriaGrupo = new Array<CategoriaGrupo>();
		this.categoria_grupo = new CategoriaGrupo();

		this.categoriaSelecionadoId = 0;
	}

	voltar(){
		this._router.navigate(['/grupo']);
	}

	private recuperarGrupo(id:number){
		this._grupoService.consultarId(id).subscribe((resultado) => {
			this.setGrupo(resultado)
		},(error) => {
			console.log(error)
		});
	}

	private setGrupo(resultado : Response){
		this.grupo =  <Grupo>resultado.objetoRetorno;
	}

	private doSalvar(){
		this._grupoService.salvar(this.grupo).subscribe((resultado) => {
			
			if(resultado.sucesso){
				this._mensagemService.showInfo("Grupo cadastrado com sucesso!");
				this.voltar();				
			} else {
				this._mensagemService.showInfo(resultado.mensagem);
			}
		});
	}

	private setCategoriaGrupo(item : CategoriaGrupo){

		if(item.nome == null  || item.nome == ""){
			this._mensagemService.showAtencao("Nome é obrigatório");
			return;
		}

		this.grupo.categoriaGrupo.push(item);
		this.categoria_grupo = new CategoriaGrupo();
	}

	confirmDialgExcluirCategoria(item : CategoriaGrupo) {

		this.categoriaSelecionadoId = item.id;
		this.categoria_grupo = item;
		this.displayDialogExcluirCategoria = true;
	}

	excluir() {
		
		this._categoriaGrupoService.excluirId(this.categoriaSelecionadoId).subscribe((resultado) => {

			this._mensagemService.showInfo("Componente Item Patrimônio excluído com sucesso!");
			this.displayDialogExcluirCategoria = false;
		});

		var index = this.grupo.categoriaGrupo.indexOf(this.categoria_grupo);
		this.grupo.categoriaGrupo.splice(index, 1);
		this.categoria_grupo = new CategoriaGrupo();
	}

	editarCategoriaGrupo(item : CategoriaGrupo) {

		this.categoriaSelecionadoId = item.id;
		this.categoria_grupo = item;
		var index = this.grupo.categoriaGrupo.indexOf(this.categoria_grupo);
		this.grupo.categoriaGrupo.splice(index, 1);
	}
}
