export class PaginaPermissao {
	
	id : number;
	idPapel: number;
	idPagina: number;
	nome:string;
	nomePagina:string;
	visualizar:boolean ;
	inserir:boolean ;
	editar:boolean ;
	excluir:boolean ;
	selecionada:boolean ;
}