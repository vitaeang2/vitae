import {Http, HTTP_PROVIDERS, Headers} from "@angular/http"
import {Pipe, Injectable} from "@angular/core";
import {HeadersService} from '../aplicacao/headers.service';
import {Menu} from "../menu/menu";
import {UrlService} from '../model/url.service';
import {LoginService} from '../login/login.service';
import {PaginaPermissao} from './pagina_permissao';


@Injectable()
export class PerfilAcessoService extends UrlService {

	private urlNegocio : string = 'perfilAcesso';
	
    public menus: Array<Menu>;
    public mostrarMenu = false;

    constructor(_http: Http, _headerService: HeadersService, _loginService: LoginService) {
    	super(_http, _headerService, _loginService);
    }

    itensMenu(){
        return this.menus;
    }

    public listarPaginas(idPerfil : number) {
        return super.callHeader(`${this.urlNegocio}/listarPaginaSelecionadas?idPerfil=` + idPerfil);
    }

    public listarPerfis() {
        return super.callHeader(`${this.urlNegocio}/listarPerfis`);
    }

    public salvarPerfil(nome : string) {
        return super.callHeaderBody(`${this.urlNegocio}/salvar`, nome);
    }

    public salvarSelecionados(idPerfil : number, lista : Array<PaginaPermissao>) {
        return super.callHeader(`${this.urlNegocio}/salvarSelecionados?idPerfil=${idPerfil}&lista=${JSON.stringify(lista)}`);
    }

}
