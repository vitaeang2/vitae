import {Component, OnInit} from '@angular/core';
import {Checkbox, Dropdown, SelectItem, Dialog, InputText} from 'primeng/primeng';
import {PaginaPermissao} from './pagina_permissao';
import {PerfilAcesso} from './perfil_acesso';
import {PerfilAcessoService} from './perfil_acesso.service';
import {MenuService} from '../menu/menu.service';
import {LoginService} from '../login/login.service';
import {Response} from '../model/response';
import {MensagemService} from '../aplicacao/mensagem.service'

@Component({
	templateUrl: 'app/perfil_acesso/perfil_acesso.html',
	directives: [Checkbox, Dropdown, Dialog, InputText],
	providers: [PerfilAcessoService]
})
export class PerfilAcessoComponent implements OnInit {

	public mostrarLista :boolean = false;
	public mostrarDialog :boolean = false;

	public listaPerfis : SelectItem[];
	public idPerfilSelecionado : any;
	public listaSelecionados: Array<PaginaPermissao>;
	public perfilAcesso : PerfilAcesso;

	public selecionarVisualizar : boolean = false;  

	constructor(private _menuService : MenuService, private _loginService : LoginService, 
		private _perfilAcessoService : PerfilAcessoService, private _mensagemService : MensagemService){

		this.perfilAcesso = new PerfilAcesso();
	}

	ngOnInit() {
		this.doListarPerfis();
	}

	consultarPermissoes(){
		this._perfilAcessoService.listarPaginas(this.idPerfilSelecionado).subscribe((resultado) => {
			this.setListaPaginasSelecionadas(resultado)
		}, (error) => {
			console.log(error)
		});
	}

	salvar(event){
		this._perfilAcessoService.salvarSelecionados(parseInt(this.idPerfilSelecionado), this.listaSelecionados)
		.subscribe((resultado) => {
			this._mensagemService.showInfo("Permissões salvas com sucesso!")
		}, (error) => {
			console.log(error)
		});
	}

	remover(event){
	}

	showDialog() {
		this.perfilAcesso = new PerfilAcesso();
		this.mostrarDialog = true;
	}

	selecionarTodosVisualizar(check){
		this.listaSelecionados.forEach(p => p.visualizar = check);
	}

	selecionarTodosSalvar(check){
		this.listaSelecionados.forEach(p => p.inserir = check);
	}

	selecionarTodosRemover(check){
		for (var i = this.listaSelecionados.length - 1; i >= 0; i--) {
			this.listaSelecionados[i].excluir = check;
		}
	}

	selecionarTodosEditar(check){
		for (var i = this.listaSelecionados.length - 1; i >= 0; i--) {
			this.listaSelecionados[i].editar = check;
		}
	}

	salvarPerfil(){
		if(!this.perfilAcesso.nome ||  this.perfilAcesso.nome == ""){
			this._mensagemService.showErro("Nome do perfil não pode ser vazio!");
		}else{
			this.mostrarDialog = false;
			this.doSalvarPerfil(this.perfilAcesso.nome);
			this._mensagemService.showInfo("Perfil salvo com sucesso!")
		}
	}

	private doSalvarPerfil(nome){
		this._perfilAcessoService.salvarPerfil(nome).subscribe((resultado) => {
			this.validaSalvar(resultado);
		});
	}

	private validaSalvar(resultado : Response){
		if(resultado.sucesso){
			this.doListarPerfis();
		}
	}

	private setListaPerfis(resultado : Response){
		if(!resultado.sucesso){
			this._mensagemService.showErro(resultado.mensagem);
			return;
		}

		let listaRetorno : Array<PerfilAcesso> = resultado.objetoRetorno;
		this.listaPerfis = [];
		listaRetorno.forEach(p => this.listaPerfis.push({label: p.nome, value: p.id}) );
	}

	private setListaPaginasSelecionadas(resultado : Response){
		if(!resultado.sucesso){
			this._mensagemService.showErro(resultado.mensagem);
			return;
		}

		this.listaSelecionados = [];
		if(resultado.objetoRetorno != null && resultado.objetoRetorno.length > 0){
			this.listaSelecionados = resultado.objetoRetorno;
			this.mostrarLista = true;
		}else{
			this.mostrarLista = false;
		}
	}

	private doListarPerfis(){
		this._perfilAcessoService.listarPerfis().subscribe((resultado) => {
			this.setListaPerfis(resultado)
		}, (error) => {
			console.log(error)
		})
	}

}