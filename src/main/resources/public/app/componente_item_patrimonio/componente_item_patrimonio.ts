export class ComponenteItemPatrimonio {

	id: number;

	descricao : string;
	
	numeroSerie : string;

	fimGarantia : string;

	descricaoGarantia : string;
	
	observacao : string;

	ativo: boolean;

}
