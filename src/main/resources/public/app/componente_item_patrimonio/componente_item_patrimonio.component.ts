import {Component} from '@angular/core';
import {Routes, ROUTER_DIRECTIVES, Router} from '@angular/router';
import {ComponenteItemPatrimonioListComponent} from './componente_item_patrimonio-list.component';
import {ComponenteItemPatrimonioDetailComponent} from "./componente_item_patrimonio-detail.component";

@Component({
	selector: 'componente_item_patrimonio',
	templateUrl: 'app/componente_item_patrimonio/componente_item_patrimonio.html',
	directives: [ROUTER_DIRECTIVES]
})
@Routes([
	{ path: "/", component: ComponenteItemPatrimonioListComponent},
	{ path: ":id", component: ComponenteItemPatrimonioDetailComponent}
	])

export class ComponenteItemPatrimonioComponent  {
	constructor(private _router : Router) {
		this._router.navigate(['/componente_item_patrimonio'])
	}
}
