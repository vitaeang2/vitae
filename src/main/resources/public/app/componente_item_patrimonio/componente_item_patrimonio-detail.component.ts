import {Component} from '@angular/core';
import {OnActivate, RouteSegment, Router} from '@angular/router';
import {SelectItem} from 'primeng/primeng';
import {ComponenteItemPatrimonioService} from './componente_item_patrimonio.service';
import {ComponenteItemPatrimonio} from '../componente_item_patrimonio/componente_item_patrimonio';
import {CustomInput} from '../componentes/custom-input.component';
import { DataValidator } from '../aplicacao/data-validator.directive';
import {CustomDatePickerComponent} from '../componentes/custom-datepicker.component';
import {MensagemService} from '../aplicacao/mensagem.service';
import {Response} from '../model/response';
import {MeuPanel} from '../aplicacao/panel';
import {EnumEstadosBrasileiros} from '../enum/enum-estados-brasileiros';

@Component({
	selector: 'componente_item_patrimonio-detail',
	templateUrl: './app/componente_item_patrimonio/componente_item_patrimonio-detail.html',
	providers: [ComponenteItemPatrimonioService],
	directives: [MeuPanel, CustomInput, DataValidator, CustomDatePickerComponent]
})
export class ComponenteItemPatrimonioDetailComponent implements OnActivate {

	componente_item_patrimonio : ComponenteItemPatrimonio = new ComponenteItemPatrimonio();

	constructor(private _componente_item_patrimonioService: ComponenteItemPatrimonioService, private _mensagemService : MensagemService, private _router: Router){
	}

	routerOnActivate(curr: RouteSegment) {
		let id = <number> +curr.getParam('id');

		if(id != -1){
			this.recuperarComponenteItemPatrimonio(id);
		}else{
			this.limpar();
		}
	}

	salvar(){

		this.doSalvar();

	}

	limpar(){
		this.componente_item_patrimonio = new ComponenteItemPatrimonio();
	}

	voltar(){
		this._router.navigate(['/componente_item_patrimonio']);
	}

	private recuperarComponenteItemPatrimonio(id:number){
		this._componente_item_patrimonioService.consultarId(id).subscribe((resultado) => {
			this.setComponenteItemPatrimonio(resultado)
		},(error) => {
			console.log(error)
		});
	}

	private setComponenteItemPatrimonio(resultado : Response){
		this.componente_item_patrimonio =  <ComponenteItemPatrimonio>resultado.objetoRetorno;
	}

	private doSalvar(){
		this._componente_item_patrimonioService.salvar(this.componente_item_patrimonio).subscribe((resultado) => {
			this._mensagemService.showInfo("Componente Item Patrimônio cadastrado com sucesso!");
			this.voltar();
		});
	}
}
