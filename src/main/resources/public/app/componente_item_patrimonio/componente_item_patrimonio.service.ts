import {Injectable} from '@angular/core';
import {Http, HTTP_PROVIDERS, Headers} from '@angular/http'
import {UrlService} from '../model/url.service';
import {HeadersService} from '../aplicacao/headers.service'
import {LoginService} from '../login/login.service';
import {PaginacaoParams} from '../model/paginacao_params';
import {ComponenteItemPatrimonio} from '../componente_item_patrimonio/componente_item_patrimonio';

@Injectable()
export class ComponenteItemPatrimonioService  extends UrlService {

private urlNegocio : string = 'componente_item_patrimonio'

	constructor(_http: Http, _headerService: HeadersService, _loginService: LoginService) {
		super(_http, _headerService, _loginService);
	}

	public listar(params: PaginacaoParams) {
		return super.callHeader(`${this.urlNegocio}/listar?params=${JSON.stringify(params)}`);
	}
	
	public listarCombo() {
		return super.callHeader(`${this.urlNegocio}/listarCombo`);
	}

	public consultarId(id: number) {
		return super.callHeader(`${this.urlNegocio}/consultarId?id=${id}`);
	}

	public salvar(componenteItemPatrimonio: ComponenteItemPatrimonio) {
		return super.callHeaderBody(`${this.urlNegocio}/salvar`, JSON.stringify(componenteItemPatrimonio));
	}

	public excluirId(id: number) {

		return super.callHeader(`componente_item_patrimonio/excluirId?id=${id}`);
	}	
}