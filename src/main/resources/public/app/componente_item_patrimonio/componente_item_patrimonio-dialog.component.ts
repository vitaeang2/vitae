import {Component, OnInit} from '@angular/core';
import {ComponenteItemPatrimonio} from './componente_item_patrimonio';
import {ComponenteItemPatrimonioService} from './componente_item_patrimonio.service';
import {MeuPanel} from '../aplicacao/panel';
import {CustomInput} from '../componentes/custom-input.component';
import {MensagemService} from '../aplicacao/mensagem.service';
import {Response} from '../model/response';

import {TimerWrapper} from '@angular/core/src/facade/async';

@Component({
	selector: 'componente_item_patrimonio-dialog',
	templateUrl: './app/componente_item_patrimonio/componente_item_patrimonio-dialog.html',
	directives: [MeuPanel, CustomInput],
	providers: [ComponenteItemPatrimonioService]
})
export class ComponenteItemPatrimonioDialogComponent implements OnInit{
	
	componente_item_patrimonio : ComponenteItemPatrimonio; 

	isSucesso: boolean;

	isResposta: boolean;

	mensagem: string;

	constructor(private _componente_item_patrimonioServico : ComponenteItemPatrimonioService, private _mensagemService : MensagemService){}

	ngOnInit() {
		this.componente_item_patrimonio = new ComponenteItemPatrimonio();
	}

	salvar(){
		this._mensagemService.showInfo("Componente Item Patrimônio salvo com sucesso! " + this.componente_item_patrimonio.descricao);
		this.doSalvar();
	}

	limpar(){
		this.componente_item_patrimonio = new ComponenteItemPatrimonio();
	}

	private doSalvar() {
		this._componente_item_patrimonioServico.salvar(this.componente_item_patrimonio).subscribe((resultado) => {
			this.setComponenteItemPatrimonio(resultado);
		});
	}

	private setComponenteItemPatrimonio(resultado : Response){
		this.isResposta = true;
		if(!resultado.sucesso){
			this.mensagem = resultado.mensagem;
			return;
		}

		this.isSucesso = true;
		this.mensagem = "Componente Item Patrimônio salvo com sucesso!";

		this._mensagemService.showInfo("Componente Item Patrimônio salvo com sucesso!");

		TimerWrapper.setTimeout(() => {  
			this.isResposta = false;
			this.isSucesso = false;
			this.mensagem = "";
			this.componente_item_patrimonio = new ComponenteItemPatrimonio();
		}, 2000);
	}
}