import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {DataTable, Column, LazyLoadEvent, Button,Dialog} from 'primeng/primeng';
import {MaterialDesignDirective} from "../aplicacao/material";
import {ComponenteItemPatrimonio} from "./componente_item_patrimonio";
import {ComponenteItemPatrimonioService} from "./componente_item_patrimonio.service"
import {MensagemService} from "../aplicacao/mensagem.service"
import {LoginService} from "../login/login.service"
import {PaginacaoParams} from '../model/paginacao_params';
import {Paginacao} from '../model/paginacao';
import {Response} from '../model/response';

@Component({
    selector: 'componente_item_patrimonio-list',
    templateUrl: './app/componente_item_patrimonio/componente_item_patrimonio-list.html',
    directives: [DataTable, Column, Button, Dialog],
    providers: [ComponenteItemPatrimonioService]
})
export class ComponenteItemPatrimonioListComponent implements OnInit {

    params: PaginacaoParams;
    paginacao: Paginacao = new Paginacao();

    private itemSelecionadoId: number = 0;

    public displayDialogExcluir: boolean = false;

    constructor(private _componente_item_patrimonioService: ComponenteItemPatrimonioService, private _loginService: LoginService, private _mensagemService: MensagemService, private _router: Router) {
    }

    ngOnInit() {
        this.doListar();
    }

    novo() {
        this._router.navigate(['/componente_item_patrimonio/', -1])
    }

    editar(id: number) {
        this._router.navigate(['/componente_item_patrimonio/', id])
    }

    load(event: LazyLoadEvent) {

        this.params = new PaginacaoParams();

        //Calcula a página atual
        let page = 0;
        if (event.first > 0) {
            page = event.first / event.rows;
        }

        this.params.page = page;
        this.params.size = event.rows;
        this.params.sortField = event.sortField;
        this.params.sortOrder = (event.sortOrder == -1) ? 'desc' : 'asc';

        this.doListar();
    }

    private doListar() {
        if (this.params) {
            this._componente_item_patrimonioService.listar(this.params).subscribe((resultado) => {
                this.setListaComponenteItemPatrimonio(resultado)
            });
        }
    }

    private setListaComponenteItemPatrimonio(resultado: Response) {
        if (!resultado.sucesso) {
            this._mensagemService.showAtencao(resultado.mensagem);
            return;
        }
        this.paginacao = resultado.objetoRetorno;
    }

    confirmDiolgExcluir(id: number) {

        this.itemSelecionadoId = id;
        this.displayDialogExcluir = true;
    }

    excluir() {

        this._componente_item_patrimonioService.excluirId(this.itemSelecionadoId).subscribe((resultado: Response) => {

            this._mensagemService.showInfo("Componente Item Patrimônio excluído com sucesso!");
            this.doListar();
            this.displayDialogExcluir = false;              
        });

    }
}
