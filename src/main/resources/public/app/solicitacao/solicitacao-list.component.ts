import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {DataTable, Column, LazyLoadEvent, Dropdown, SelectItem, Dialog} from 'primeng/primeng';
import {MaterialDesignDirective} from "../aplicacao/material";
import {Solicitacao} from "./solicitacao";
import {SolicitacaoService} from "./solicitacao.service"
import {Nucleo} from "../nucleo/nucleo";
import {NucleoService} from '../nucleo/nucleo.service';
import {Local} from "../local/local";
import {EnumStatusSolicitacao} from '../enum/enum-status-solicitacao';
import {LocalService} from '../local/local.service';
import {MensagemService} from "../aplicacao/mensagem.service";
import {LoginService} from "../login/login.service";
import {PaginacaoParams} from '../model/paginacao_params';
import {Paginacao} from '../model/paginacao';
import {Response} from '../model/response';
import {VitaeLazy} from '../aplicacao/vitae-lazy.service';

@Component({
    selector: 'solicitacao-list',
    templateUrl: './app/solicitacao/solicitacao-list.html',
    directives: [DataTable, Column, Dropdown, Dialog],
    providers: [SolicitacaoService, NucleoService, LocalService]
})
export class SolicitacaoListComponent extends VitaeLazy  implements OnInit {

    params : PaginacaoParams;
    paginacao : Paginacao = new Paginacao();

    listaNucleo : SelectItem[];
    idNucleoSelecionado : number = 0;

    listaLocal : SelectItem[];
    idLocalSelecionado : number = 0;

    listaStatusSolicitacao : SelectItem[];
    idStatusSolicitacaoSelecionado : number = 0;

    private idSolicitacaoSelecionado: number = 0;

    public displayDialogExcluir: boolean = false;

    constructor(private _nucleoService: NucleoService, private _localService: LocalService, private _solicitacaoService : SolicitacaoService, 
        private _loginService : LoginService, private _mensagemService : MensagemService, private router: Router){
        super(router);
    }

    ngOnInit(){

        this.doListar();
        this.doListarComboNucleo();
        this.doListarComboLocal();
        this.carregarlistarStatusSolicitacao();
    }

    novo () {
        super.novo('/solicitacao/');
    }

    editar(id: number){

        super.editar(id, '/solicitacao/');
    }

    doListar () {
        if(this.params){

            this.listar();
        }
    }

    private listar(){
        this._solicitacaoService.listar(this.params).subscribe((resultado) => {

            this.setListaSolicitacao(resultado)
        });
    }

    private setListaSolicitacao(resultado : Response){

        if(!resultado.sucesso){

            this._mensagemService.showAtencao(resultado.mensagem);
            return;
        }

        this.paginacao = resultado.objetoRetorno;
    }

    confirmDiolgExcluir(id:number) {

        this.idSolicitacaoSelecionado = id;
        this.displayDialogExcluir = true;
    }

    excluir() { 

        this._solicitacaoService.excluirId(this.idSolicitacaoSelecionado).subscribe((resultado) => {

            this._mensagemService.showInfo("Solicitacao excluído com sucesso!");
            this.displayDialogExcluir = false;
            this.doListar();
        });

    }

    private doListarComboNucleo(){

        this._nucleoService.listarCombo().subscribe((resultado) => {

            this.setListaComboNucleo(resultado)
        });
    }

    private setListaComboNucleo(resultado : Response){

        if(!resultado.sucesso){
            this._mensagemService.showAtencao("Falha de conexão, não foi possivel carregar combo!");
            return;
        }

        this.listaNucleo = [];
        this.listaNucleo.push({label: '-- TODOS --', value: 0})
        let listaRetorno : Array<Nucleo> = resultado.objetoRetorno;
        listaRetorno.forEach(nuc => this.listaNucleo.push({label: nuc.nomeEmpresa + ' - ' + nuc.nome, value: nuc.id}))

    }

    listarPorFiltros(idNucleo: number, idLocal: number, idStatusSolicitacao: number ){

        this._solicitacaoService.listarPorFiltros(this.params, idNucleo, idLocal, idStatusSolicitacao).subscribe((resultado) => {

            this.setListaSolicitacao(resultado)
        });
    }

    private doListarComboLocal(){
        this._localService.listarCombo().subscribe((resultado) => {
            this.setListaComboLocal(resultado);
        }) ;
    }

    private setListaComboLocal(resultado : Response){
        if(!resultado.sucesso){
            this._mensagemService.showAtencao("Falha de conexão, não foi possivel carregar combo!");
            return;
        }
        this.listaLocal = [];
        this.listaLocal.push({label: '-- TODOS --', value: 0});
        let listaRetorno : Array<Local> = resultado.objetoRetorno;
        listaRetorno.forEach(l => this.listaLocal.push({label: l.nome, value: l.id}) );

    }

        private carregarlistarStatusSolicitacao(){
        
        this.listaStatusSolicitacao = [];
        let lista : Array<EnumStatusSolicitacao> = EnumStatusSolicitacao.toArray();
        this.listaStatusSolicitacao.push({label: "-- TODOS --", value: 0});
        lista.forEach(s => this.listaStatusSolicitacao.push({label: s.descricao, value: s.codigo}));
        
    }
}
