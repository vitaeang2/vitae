import {Component} from '@angular/core';
import {FORM_BINDINGS, ControlGroup, FormBuilder, Validators} from '@angular/common';
import {OnActivate, RouteSegment, Router} from '@angular/router';
import {SelectItem, Dropdown, Dialog, TabView, TabPanel} from 'primeng/primeng';
import {EmpresaService} from '../empresa/empresa.service';
import {Empresa} from '../empresa/empresa';
import {Nucleo} from '../nucleo/nucleo';
import {NucleoService} from '../nucleo/nucleo.service';
import {SolicitacaoService} from './solicitacao.service'
import {Solicitacao} from '../solicitacao/solicitacao'
import {MensagemService} from '../aplicacao/mensagem.service';
import {Response} from '../model/response'
import {NumeroBanco} from '../numero_banco/numero_banco';
import {NumeroBancoService} from '../numero_banco/numero_banco.service'
import {Pessoa} from '../pessoa/pessoa';
import {Banco} from '../banco/banco';
import {Usuario} from '../usuario/usuario';
import {UsuarioService} from '../usuario/usuario.service';
import {CustomDatePickerComponent} from '../componentes/custom-datepicker.component';
import {MeuPanel} from '../aplicacao/panel';
import {CustomInput} from '../componentes/custom-input.component';
import {InputNumber} from '../componentes/input-number';
import {EnumEstadosBrasileiros} from '../enum/enum-estados-brasileiros';
import {EnumSexo} from '../enum/enum-sexo';
import {EnumEstadoCivil} from '../enum/enum-estado-civil';
import {EnumTipoPessoa} from '../enum/enum-tipo-pessoas';
import {PerfilAcesso} from '../perfil_acesso/perfil_acesso';
import {TipoSolicitacao} from '../tipo_solicitacao/tipo_solicitacao';
import {TipoSolicitacaoService} from '../tipo_solicitacao/tipo_solicitacao.service';
import {Local} from '../local/local';
import {LocalDialogComponent} from '../local/local-dialog.component';
import {LocalService} from '../local/local.service';
import {LoginService} from "../login/login.service";
import {EnumStatusSolicitacao} from '../enum/enum-status-solicitacao';
import {PerfilAcessoService} from '../perfil_acesso/perfil_acesso.service';
import { NotEqualValidator } from '../aplicacao/not-equal-validator.directive';
import { DataValidator } from '../aplicacao/data-validator.directive';
import {TimerWrapper} from '@angular/core/src/facade/async';


@Component({
	selector: 'solicitacao-detail',
	templateUrl: './app/solicitacao/solicitacao-detail.html',
	providers: [PerfilAcessoService, SolicitacaoService, NucleoService, EmpresaService, UsuarioService, NumeroBancoService, TipoSolicitacaoService, LocalService],
	directives: [MeuPanel, Dropdown, CustomDatePickerComponent, CustomInput, InputNumber, Dialog, NotEqualValidator, TabView, TabPanel, DataValidator],
	viewBindings: [FORM_BINDINGS]
})
export class SolicitacaoDetailComponent implements OnActivate {

	mostrarDialog : boolean;

	isPodeSalvar: boolean = true;

	solicitacao : Solicitacao = new Solicitacao();

	usuario : Usuario = new Usuario();

	listaEmpresa : SelectItem[];
	
	listaNucleo : SelectItem[];
	
	listaResponsavel : SelectItem[];

	listaTipoSolicitacao : SelectItem[];

	listaStatusSolicitacao : SelectItem[];

	idEmpresa : number = 0;
	
	idNucleo : number = 0;
	
	idResponsavel : number = 0;

	idTipoSolicitacao : number = 0;

	idStatusSolicitacao : number = 0;

	idLocal : number = 0;

	possuiUsuario : boolean = false;

	mostrarDialogLocal : boolean = false;

	listaLocal : SelectItem[];

	pt_BR: any;
	
	constructor(private _usuarioService: UsuarioService, private _responsavelService : UsuarioService, private _nucleoService : NucleoService, 
		private _empresaService: EmpresaService, private _solicitacaoService: SolicitacaoService, private _tipoSolicitacaoService : TipoSolicitacaoService, 
		private _loginService : LoginService,	private _mensagemService : MensagemService, private _router: Router, builder: FormBuilder, 
		private _localService : LocalService){
		this.limpar();
	}

	routerOnActivate(curr: RouteSegment) {
		let id = <number> +curr.getParam('id');
		
		this.doListarComboNucleo();

		this.idEmpresa = this._loginService.getUser().colaborador.empresa.id;

		this.limpar();

		if(id != -1){
			this.solicitacao = new Solicitacao();
			this.solicitacao.id = id;
			this.recuperarSolicitacao(id);
		}
	}

	salvar(){
		this.doSalvar();
	}

	limpar(){
		this.solicitacao = new Solicitacao();
		this.solicitacao.responsavel = new Usuario();
		this.solicitacao.empresa = new Empresa();
		this.solicitacao.nucleo = new Nucleo();
		this.solicitacao.abertura = new Date().toLocaleDateString();
		this.usuario = new Usuario();
		this.mostrarDialog = false;
		this.idNucleo = 0;	
		this.idStatusSolicitacao = 1;
		this.idTipoSolicitacao = 0;
		this.idResponsavel = this._loginService.getUser().id;
		this.idLocal = 0;
		this.possuiUsuario = false;
	}

	voltar(){
		this._router.navigate(['/solicitacao']);
	}
/*
	listarComboPorIdEmpresa(){

		if (this.idEmpresa > 0) {

			this._nucleoService.listarComboPorIdEmpresa(this.idEmpresa).subscribe((resultado) => {
				
				this.setListaComboNucleo(resultado)
				
			},(error) => {
				
				console.log(error)
			});

		} else {
			
			this.listaNucleo = [];
		}
	}*/
	
	private doListarComboNucleo(){

		this._nucleoService.listarCombo().subscribe((resultado) => {
			
			this.setListaComboNucleo(resultado)
		});
	}

	private setListaComboNucleo(resultado : Response){

		if(!resultado.sucesso){

			this._mensagemService.showAtencao("Falha de conexão, não foi possivel carregar combo!");
			return;
		}

		this.listaNucleo = [];
		this.listaNucleo.push({label: '-- Selecione --', value: 0})
		let listaRetorno : Array<Nucleo> = resultado.objetoRetorno;
		listaRetorno.forEach(nuc => this.listaNucleo.push({label: nuc.nomeEmpresa + ' - ' + nuc.nome, value: nuc.id}))
		
		/*this.idNucleo = this.solicitacao.nucleo.id;*/

		this.doListaComboTipoSolicitacao();
	}	

	/*private doListarCombo(){

		this._empresaService.listarCombo().subscribe((resultado) => {
			
			this.setListaCombo(resultado)
		});
	}

	private setListaCombo(resultado : Response){

		if(!resultado.sucesso){
			this._mensagemService.showAtencao("Falha de conexão, não foi possivel carregar combo!");
			return;
		}
		this.listaEmpresa = [];
		this.listaEmpresa.push({label: '-- Selecione --', value: 0})
		let listaRetorno : Array<Empresa> = resultado.objetoRetorno;
		listaRetorno.forEach(emp => this.listaEmpresa.push({label: emp.nome, value: emp.id}))

		this.doListaComboTipoSolicitacao();
	}*/

	private doListaComboTipoSolicitacao(){
		this._tipoSolicitacaoService.listarCombo().subscribe((resultado) => {
			this.setListaComboTipoSolicitacao(resultado);
		}) ;
	}

	private setListaComboTipoSolicitacao(resultado : Response){
		if(!resultado.sucesso){
			return;
		}
		this.listaTipoSolicitacao = [];
		let listaRetorno : Array<TipoSolicitacao> = resultado.objetoRetorno;
		listaRetorno.forEach(p => this.listaTipoSolicitacao.push({label: p.nome, value: p.id}) );

		this.listarStatusSolicitacao();
	}

	private listarStatusSolicitacao(){
		this.listaStatusSolicitacao = [];
		this.listaStatusSolicitacao.push({label: "-- Selecione --", value: 0})
		let lista : Array<EnumStatusSolicitacao> = EnumStatusSolicitacao.toArray();
		lista.forEach(g => this.listaStatusSolicitacao.push({label: g.descricao, value: g.codigo}));
		
		this.doListarComboResponsavel();
		
	}

	private doListarComboResponsavel(){
		
		this._responsavelService.listarCombo().subscribe((resultado) => {
			
			this.setListaComboResponsavel(resultado)
		});
	}
	private setListaComboResponsavel(resultado : Response){

		if(!resultado.sucesso){

			this._mensagemService.showAtencao("Falha de conexão, não foi possivel carregar combo!");
			return;
		}

		this.listaResponsavel = [];
		this.listaResponsavel.push({label: '-- Selecione --', value: 0})
		let listaRetorno : Array<Usuario> = resultado.objetoRetorno;
		listaRetorno.forEach(resp => this.listaResponsavel.push({label: resp.colaborador.pessoa.nome, value: resp.id}));

		this.doListaComboLocal();
	}

	private recuperarSolicitacao(id:number){
		
		this._solicitacaoService.consultarId(id).subscribe((resultado) => {
			
			this.setSolicitacao(resultado)
			
		},(error) => {
			
			console.log(error)
		});
	}

	private setSolicitacao(resultado : Response){

		this.solicitacao =  <Solicitacao>resultado.objetoRetorno;
		
		if(this.solicitacao.nucleo){
			this.idNucleo = this.solicitacao.nucleo.id;
		}
		if(this.solicitacao.empresa){
			this.idEmpresa = this.solicitacao.empresa.id;
		}

		this.idStatusSolicitacao = this.solicitacao.statusSolicitacao;
		this.idTipoSolicitacao = this.solicitacao.tipoSolicitacao.id;


		if(this.solicitacao.criador){
			this.idResponsavel = this.solicitacao.criador.id;
		}
		
		this.idLocal = this.solicitacao.local.id;

		/*	this.listarComboPorIdEmpresa();*/
		this.doListarComboNucleo();

	}

	private doMontarSolicitacao(){
		this.solicitacao.empresa = new Empresa();
		this.solicitacao.empresa.id = this.idEmpresa;

		this.solicitacao.nucleo = new Nucleo();
		this.solicitacao.nucleo.id =  this.idNucleo;

		this.solicitacao.statusSolicitacao = this.idStatusSolicitacao;

		this.solicitacao.tipoSolicitacao = new TipoSolicitacao();
		this.solicitacao.tipoSolicitacao.id = this.idTipoSolicitacao;

		this.solicitacao.criador = new Usuario();
		this.solicitacao.criador.id = this.idResponsavel;

		this.solicitacao.local = new Local();
		this.solicitacao.local.id = this.idLocal;

	}

	private doSalvar(){

		this.doMontarSolicitacao();
		this.isPodeSalvar = false;
		this._solicitacaoService.salvar(this.solicitacao).subscribe((resultado) => {
			if (resultado) {

				if(resultado.sucesso){

					this._mensagemService.showInfo("Solicitação cadastrada com sucesso!");

					this.voltar();
				} else {

					this._mensagemService.showAtencao(resultado.mensagem);
					console.log(resultado);
				}
			}

			this.isPodeSalvar = true;

		});
	}

	private setSalvou(){
		this.voltar();
		this._mensagemService.showInfo("Solicitação cadastrada com sucesso!");
	}

	showDialogLocal(){
		this.mostrarDialogLocal = true;
	}

	closeDialogLocal(){
		this.mostrarDialogLocal = false;
		this.doListaComboLocal();
	}
	
	private doListaComboLocal(){
		this._localService.listarCombo().subscribe((resultado) => {
			this.setListaComboLocal(resultado);
		}) ;
	}

	private setListaComboLocal(resultado : Response){
		if(!resultado.sucesso){
			return;
		}
		this.listaLocal = [];
		this.listaLocal.push({label: '-- Selecione --', value: 0});
		let listaRetorno : Array<Local> = resultado.objetoRetorno;
		listaRetorno.forEach(p => this.listaLocal.push({label: p.nome, value: p.id}) );

	}
}
