import {Usuario} from '../usuario/usuario';
import {Cargo} from '../cargo/cargo';
import {Empresa} from '../empresa/empresa';
import {Nucleo} from '../nucleo/nucleo';
import {TipoSolicitacao} from '../tipo_solicitacao/tipo_solicitacao';
import {Local} from '../local/local';

export class Solicitacao {

	id: number;

	assunto : string;
	
	descricao : string;
	
	solucao : string;

	numeroEtiqueta : string;
	
	responsavel : Usuario;
	
	criador : Usuario;
	
	empresa : Empresa;

	nucleo : Nucleo;

	tipoSolicitacao : TipoSolicitacao;

	local : Local;

	statusSolicitacao : number;

	abertura : string;

	termino : string;
	
	previsaoFinalizacao : string;
	
	ativo : boolean;

}