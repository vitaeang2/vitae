import {Injectable} from '@angular/core';
import {Http, HTTP_PROVIDERS, Headers} from '@angular/http'
import {UrlService} from '../model/url.service';
import {HeadersService} from '../aplicacao/headers.service'
import {LoginService} from '../login/login.service';
import {PaginacaoParams} from '../model/paginacao_params';
import {Solicitacao} from '../solicitacao/solicitacao';

@Injectable()
export class SolicitacaoService  extends UrlService {

	constructor(_http: Http, _headerService: HeadersService, _loginService: LoginService) {
		super(_http, _headerService, _loginService);
	}

	public listar(params: PaginacaoParams) {
		return super.callHeader(`solicitacao/listar?params=${JSON.stringify(params)}`);
	}
	
	public listarCombo() {
		return super.callHeader(`solicitacao/listarCombo`);
	}

	public consultarId(id: number) {
		return super.callHeader(`solicitacao/consultarId?id=${id}`);
	}

	public salvar(solicitacao: Solicitacao) {
		return super.callHeaderBody(`solicitacao/salvar`, JSON.stringify(solicitacao));
	}

	public excluirId(id: number) {

		return super.callHeader(`solicitacao/excluirId?id=${id}`);
	}

	public listarPorFiltros(params: PaginacaoParams, idNucleo?: number, idLocal?: number, idStatusSolicitacao?: number) {
		return super.callHeader(`solicitacao/listar?params=${JSON.stringify(params)}&idNucleo=${idNucleo}&idLocal=${idLocal}&idStatusSolicitacao=${idStatusSolicitacao}`);
	}

	public listarPaginadoSolicitacoesAbertas(params: PaginacaoParams, idEmpresa?: number, idNucleo?: number, idLocal?: number) {
		return super.callHeader(`solicitacao/listarPaginadoSolicitacoesAbertas?params=${JSON.stringify(params)}&idEmpresa=${idEmpresa}&idNucleo=${idNucleo}&idLocal=${idLocal}`);
	}
}