import {Component} from '@angular/core';
import {Routes, ROUTER_DIRECTIVES, Router} from '@angular/router';
import {SolicitacaoListComponent} from './solicitacao-list.component';
import {SolicitacaoDetailComponent} from "./solicitacao-detail.component";

@Component({
	selector: 'solicitacao',
	templateUrl: 'app/solicitacao/solicitacao.html',
	directives: [ROUTER_DIRECTIVES]
})
@Routes([
	{ path: "/", component: SolicitacaoListComponent},
	{ path: ":id", component: SolicitacaoDetailComponent}
	])
export class SolicitacaoComponent  {
	constructor(private _router : Router) {
		this._router.navigate(['/solicitacao'])
	}
}
