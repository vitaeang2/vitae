import {Component} from '@angular/core';
import {OnActivate, RouteSegment, Router} from '@angular/router';
import {SelectItem} from 'primeng/primeng';
import {CategoriaGrupoService} from './categoria_grupo.service'
import {CategoriaGrupo} from '../categoria_grupo/categoria_grupo'
import {MensagemService} from '../aplicacao/mensagem.service';
import {Response} from '../model/response'
import {MeuPanel} from '../aplicacao/panel';
import {EnumEstadosBrasileiros} from '../enum/enum-estados-brasileiros';

@Component({
	selector: 'categoria-grupo-detail',
	templateUrl: './app/categoria_grupo/categoria_grupo-detail.html',
	providers: [CategoriaGrupoService],
	directives: [MeuPanel]
})
export class CategoriaGrupoDetailComponent implements OnActivate {

	categoriaGrupo : CategoriaGrupo = new CategoriaGrupo();

	constructor(private _categoriaGrupoService: CategoriaGrupoService, private _mensagemService : MensagemService, private _router: Router){
	}

	routerOnActivate(curr: RouteSegment) {
		let id = <number> +curr.getParam('id');

		if(id != -1){
			this.recuperarCategoriaGrupo(id);
		}else{
			this.limpar();
		}
	}

	salvar(){

		this.doSalvar();

	}

	limpar(){
		this.categoriaGrupo = new CategoriaGrupo();
	}

	voltar(){
		this._router.navigate(['/categoria_grupo']);
	}

	private recuperarCategoriaGrupo(id:number){
		this._categoriaGrupoService.consultarId(id).subscribe((resultado) => {
			this.setCategoriaGrupo(resultado)
		},(error) => {
			console.log(error)
		});
	}

	private setCategoriaGrupo(resultado : Response){
		this.categoriaGrupo =  <CategoriaGrupo>resultado.objetoRetorno;
	}

	private doSalvar(){
		this._categoriaGrupoService.salvar(this.categoriaGrupo).subscribe((resultado) => {
			this._mensagemService.showInfo("Categoria de Grupo cadastrado com sucesso!");
			this.voltar();
		});
	}
}
