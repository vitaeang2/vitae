import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {DataTable, Column, LazyLoadEvent, Button,Dialog} from 'primeng/primeng';
import {MaterialDesignDirective} from "../aplicacao/material";
import {CategoriaGrupo} from "./categoria_grupo";
import {CategoriaGrupoService} from "./categoria_grupo.service"
import {MensagemService} from "../aplicacao/mensagem.service"
import {LoginService} from "../login/login.service"
import {PaginacaoParams} from '../model/paginacao_params';
import {Paginacao} from '../model/paginacao';
import {Response} from '../model/response';

@Component({
    selector: 'categoria-grupo-list',
    templateUrl: './app/categoria_grupo/categoria_grupo-list.html',
    directives: [DataTable, Column, Button, Dialog],
    providers: [CategoriaGrupoService]
})
export class CategoriaGrupoListComponent implements OnInit {

    params: PaginacaoParams;
    paginacao: Paginacao = new Paginacao();

    private itemSelecionadoId: number = 0;

    public displayDialogExcluir: boolean = false;

    public displayDialogExcluirCategoriaViculoGrupo: boolean = false;

    constructor(private _categoriaGrupoService: CategoriaGrupoService, private _loginService: LoginService, private _mensagemService: MensagemService, private _router: Router) {
    }

    ngOnInit() {
        this.doListar();
    }

    novo() {
        this._router.navigate(['/categoria_grupo/', -1])
    }

    editar(id: number) {
        this._router.navigate(['/categoria_grupo/', id])
    }

    load(event: LazyLoadEvent) {

        this.params = new PaginacaoParams();

        //Calcula a página atual
        let page = 0;
        if (event.first > 0) {
            page = event.first / event.rows;
        }

        this.params.page = page;
        this.params.size = event.rows;
        this.params.sortField = event.sortField;
        this.params.sortOrder = (event.sortOrder == -1) ? 'desc' : 'asc';

        this.doListar();
    }

    private doListar() {
        if (this.params) {
            this._categoriaGrupoService.listar(this.params).subscribe((resultado) => {
                this.setListaCategoriaGrupo(resultado)
            });
        }
    }

    private setListaCategoriaGrupo(resultado: Response) {
        if (!resultado.sucesso) {
            this._mensagemService.showAtencao(resultado.mensagem);
            return;
        }
        this.paginacao = resultado.objetoRetorno;
    }

    confirmDiolgExcluir(id: number) {

        this.itemSelecionadoId = id;
        this.displayDialogExcluir = true;
    }

    excluir(verificarCategoriaViculadaGrupo: boolean) {

        this._categoriaGrupoService.excluirId(this.itemSelecionadoId).subscribe((resultado: Response) => {

            if (!resultado.sucesso) {
                this.displayDialogExcluirCategoriaViculoGrupo = true;
            } else {
                this._mensagemService.showInfo("Categoria de Grupo excluído com sucesso!");
                this.displayDialogExcluirCategoriaViculoGrupo = false;
                this.doListar();
            }
            this.displayDialogExcluir = false;              
        });

    }
}
