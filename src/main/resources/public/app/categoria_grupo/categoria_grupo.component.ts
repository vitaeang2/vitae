import {Component} from '@angular/core';
import {Routes, ROUTER_DIRECTIVES, Router} from '@angular/router';
import {CategoriaGrupoListComponent} from './categoria_grupo-list.component';
import {CategoriaGrupoDetailComponent} from "./categoria_grupo-detail.component";

@Component({
	selector: 'categoria_grupo',
	templateUrl: 'app/categoria_grupo/categoria_grupo.html',
	directives: [ROUTER_DIRECTIVES]
})
@Routes([
	{ path: "/", component: CategoriaGrupoListComponent},
	{ path: ":id", component: CategoriaGrupoDetailComponent}
	])

export class CategoriaGrupoComponent  {
	constructor(private _router : Router) {
		this._router.navigate(['/categoria_grupo'])
	}
}
