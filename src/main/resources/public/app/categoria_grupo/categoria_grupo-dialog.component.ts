import {Component, OnInit} from '@angular/core';
import {CategoriaGrupo} from './categoria_grupo';
import {CategoriaGrupoService} from './categoria_grupo.service';
import {MeuPanel} from '../aplicacao/panel';
import {MensagemService} from '../aplicacao/mensagem.service';
import {Response} from '../model/response';

import {TimerWrapper} from '@angular/core/src/facade/async';

@Component({
	selector: 'categoria-grupo-dialog',
	templateUrl: './app/categoria_grupo/categoria_grupo-dialog.html',
	directives: [MeuPanel],
	providers: [CategoriaGrupoService]
})
export class CategoriaGrupoDialogComponent implements OnInit{
	
	categoria : CategoriaGrupo; 

	isSucesso: boolean;

	isResposta: boolean;

	mensagem: string;

	constructor(private _categoriaGrupoServico : CategoriaGrupoService, private _mensagemService : MensagemService){}

	ngOnInit() {
		this.categoria = new CategoriaGrupo();
	}

	salvar(){
		this._mensagemService.showInfo("Categoria salva com sucesso! " + this.categoria.nome);
		this.doSalvar();
	}

	limpar(){
		this.categoria = new CategoriaGrupo();
	}

	private doSalvar() {
		this._categoriaGrupoServico.salvar(this.categoria).subscribe((resultado) => {
			this.setCategoria(resultado);
		});
	}

	private setCategoria(resultado : Response){
		this.isResposta = true;
		if(!resultado.sucesso){
			this.mensagem = resultado.mensagem;
			return;
		}

		this.isSucesso = true;
		this.mensagem = "Categoria de Grupo salva com sucesso!";

		this._mensagemService.showInfo("Categoria de Grupo salva com sucesso!");


		TimerWrapper.setTimeout(() => {  
			this.isResposta = false;
			this.isSucesso = false;
			this.mensagem = "";
			this.categoria = new CategoriaGrupo();
		}, 2000);
	}
}