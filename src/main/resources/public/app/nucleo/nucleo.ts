import {Empresa} from '../empresa/empresa';

export class Nucleo {

	id: number;

	nome : string;

	ativo : boolean;

	email : string;

	dataCadastro : Date;

	tipoPessoa : number;

	telefone : string;

	endereco : string;

	cidade : string;

	bairro: string;

	cep: string;
	
	ufCidade: string;

	idEmpresa: number;

	nomeEmpresa: string;

	constructor(){}

	setNome(nome : string){
		 this.nome = nome;
	}
	getNome(){
		return this.nome;
	}
}