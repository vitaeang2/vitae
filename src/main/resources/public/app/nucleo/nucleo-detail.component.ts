import {Component} from '@angular/core';
import {OnActivate, RouteSegment, Router} from '@angular/router';
import {SelectItem} from 'primeng/primeng';
import {NucleoService} from './nucleo.service'
import {Nucleo} from '../nucleo/nucleo'
import {EmpresaService} from '../empresa/empresa.service';
import {Empresa} from '../empresa/empresa';
import {MensagemService} from '../aplicacao/mensagem.service';
import {MaskInputDirective} from '../componentes/mask.directive';
import {Response} from '../model/response'
import {MeuPanel} from '../aplicacao/panel';
import {EnumEstadosBrasileiros} from '../enum/enum-estados-brasileiros';

@Component({
	selector: 'nucleo-detail',
	templateUrl: './app/nucleo/nucleo-detail.html',
	providers: [NucleoService, EmpresaService],
	directives: [MeuPanel, MaskInputDirective]
})
export class NucleoDetailComponent implements OnActivate {

	nucleo : Nucleo = new Nucleo();

	listaEstado = EnumEstadosBrasileiros

	listaEmpresa : SelectItem[];

	constructor(private _nucleoService: NucleoService, private _empresaService : EmpresaService, private _mensagemService : MensagemService, private _router: Router){
	}

	routerOnActivate(curr: RouteSegment) {
		let id = <number> +curr.getParam('id');

		this.doListarCombo();

		if(id != -1){
			this.recuperarNucleo(id);
		}else{
			this.limpar();
		}
	}

	salvar(){
		this.doSalvar();
	}

	limpar(){
		this.nucleo = new Nucleo();
	}

	voltar(){
		this._router.navigate(['/nucleo']);
	}

	private doListarCombo(){
		this._empresaService.listarCombo().subscribe((resultado) => {
			this.setListaCombo(resultado)
		});
	}

	private recuperarNucleo(id:number){
		this._nucleoService.consultarId(id).subscribe((resultado) => {
			this.setNucleo(resultado)
		},(error) => {
			console.log(error)
		});
	}

	private setNucleo(resultado : Response){
		this.nucleo =  <Nucleo>resultado.objetoRetorno;
	}

	private doSalvar(){
		this._nucleoService.salvar(this.nucleo).subscribe((resultado) => {
			this.setVoltar();
		});
	}

	private setVoltar(){
		this._mensagemService.showInfo("Núcleo cadastrado com sucesso!");
		this._router.navigate(['/nucleo']);
	}

	private setListaCombo(resultado : Response){
		if(!resultado.sucesso){
			this._mensagemService.showAtencao("Falha de conexão, não foi possivel carregar combo!");
			return;
		}
		this.listaEmpresa = [];
		let listaRetorno : Array<Empresa> = resultado.objetoRetorno;
		listaRetorno.forEach(emp => this.listaEmpresa.push({label: emp.nome, value: emp.id}))
	}
}
