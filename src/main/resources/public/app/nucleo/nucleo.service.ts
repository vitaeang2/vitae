import {Injectable} from '@angular/core';
import {Http, HTTP_PROVIDERS, Headers} from '@angular/http'
import {UrlService} from '../model/url.service';
import {HeadersService} from '../aplicacao/headers.service'
import {LoginService} from '../login/login.service';
import {PaginacaoParams} from '../model/paginacao_params';
import {Nucleo} from '../nucleo/nucleo';

@Injectable()
export class NucleoService  extends UrlService {

	constructor(_http: Http, _headerService: HeadersService, _loginService: LoginService) {
		super(_http, _headerService, _loginService);
	}

	public listar(params: PaginacaoParams) {
		return super.callHeader(`nucleo/listar?params=${JSON.stringify(params)}`);
	}

	public consultarId(id: number) {
		return super.callHeader(`nucleo/consultarId?id=${id}`);
	}

	public listarComboPorIdEmpresa(idEmpresa: number) {
		return super.callHeader(`nucleo/listarComboPorIdEmpresa?idEmpresa=${idEmpresa}`);
	}
	
	public listarCombo() {
		return super.callHeader(`nucleo/listarCombo`);
	}

	public salvar(nucleo: Nucleo) {
		return super.callHeaderBody(`nucleo/salvar`, JSON.stringify(nucleo));
	}

	public excluirId(id: number) {

		return super.callHeader(`nucleo/excluirId?id=${id}`);
	}
}