import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {DataTable, Column, LazyLoadEvent, Button, Dialog} from 'primeng/primeng';
import {MaterialDesignDirective} from "../aplicacao/material";
import {Nucleo} from "./nucleo";
import {NucleoService} from "./nucleo.service"
import {MensagemService} from "../aplicacao/mensagem.service"
import {LoginService} from "../login/login.service"
import {PaginacaoParams} from '../model/paginacao_params';
import {Paginacao} from '../model/paginacao';
import {Response} from '../model/response';
import {VitaeLazy} from '../aplicacao/vitae-lazy.service';

@Component({
    selector: 'nucleo',
    templateUrl: './app/nucleo/nucleo-list.html',
    directives: [DataTable, Column, Button, Dialog],
    providers: [NucleoService]
})
export class NucleoListComponent  extends VitaeLazy implements OnInit {

    params : PaginacaoParams;
    paginacao : Paginacao = new Paginacao();

    private itemSelecionadoId: number = 0;

    public displayDialogExcluir: boolean = false;

    constructor(private _nucleoService : NucleoService, private _loginService : LoginService, private _mensagemService : MensagemService, private router: Router){
        super(router);
    }

    ngOnInit(){
        this.doListar();
    }

    novo() {

        super.novo('/nucleo/')
    }
    
    editar(id: number){

        super.editar(id, '/nucleo/')
    }

    doListar() {
        if(this.params){
            this._nucleoService.listar(this.params).subscribe((resultado) => {

                this.setListaNucleo(resultado)
            });
        }
    }

    private setListaNucleo(resultado : Response){

        if(!resultado.sucesso) {

            this._mensagemService.showAtencao(resultado.mensagem);
            return;
        }
        this.paginacao = resultado.objetoRetorno;
    }

    confirmDiolgExcluir(id:number) {

        this.itemSelecionadoId = id;
        this.displayDialogExcluir = true;
    }

    excluir() { 

        this._nucleoService.excluirId(this.itemSelecionadoId).subscribe((resultado) => {

            this._mensagemService.showInfo("Núcleo excluído com sucesso!");
            this.displayDialogExcluir = false;
            this.doListar();
        });

    }
}
