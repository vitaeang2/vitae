import {Component} from '@angular/core';
import {Routes, ROUTER_DIRECTIVES, Router} from '@angular/router';
import {NucleoListComponent} from './nucleo-list.component';
import {NucleoDetailComponent} from "./nucleo-detail.component";

@Component({
	selector: 'nucleo',
	templateUrl: 'app/nucleo/nucleo.html',
	directives: [ROUTER_DIRECTIVES]
})
@Routes([
	{ path: "/", component: NucleoListComponent},
	{ path: ":id", component: NucleoDetailComponent}
	])
export class NucleoComponent  {
	constructor(private _router : Router) {
		this._router.navigate(['/nucleo'])
	}
}
