import {Directive, OnInit, Input, ElementRef} from '@angular/core';
import {NgModel, NgControlName, Control} from '@angular/common';

declare var jQuery: any;

@Directive({
    selector: '[maskInput][ngModel]',
    providers: [NgModel, NgControlName]
})
export class MaskInputDirective implements OnInit {

    @Input() 
    private digits: string = '2';

    @Input()
    private clearOnMaskIncomplete: boolean;

    @Input('maskInput')
    private mask: any;

    private control: Control;

    constructor(public ng: NgModel, private controlName: NgControlName, private el: ElementRef) {
        this.clearOnMaskIncomplete = true;
        this.control = this.controlName.control;
    }

    ngOnInit() {

        let _that = this;

        this.control = this.ng.control != undefined ? this.ng.control : this.control;

        let maskOptions = {
            greedy: true
        };

        if (this.mask == 'currency' || this.mask == 'decimal') {

            maskOptions['prefix'] = this.mask == 'currency' ? "R$ " : '';
            maskOptions['alias'] = 'currency';
            maskOptions['radixPoint'] = ',';
            maskOptions['groupSeparator'] = ".";
            maskOptions['unmaskAsNumber'] = true;
            maskOptions['digits'] = new Number(this.digits).valueOf();
            maskOptions['onKeyDown'] = function(event) {
                setTimeout(function() {

                    let unmaskedValue = event.target.inputmask.unmaskedvalue();
                    //bug jquery.inputmask
                    if (unmaskedValue == ',00')
                        unmaskedValue = null;

                    _that.updateControlValue(unmaskedValue);    
                    _that.ng.viewToModelUpdate(unmaskedValue);
                }, 0)
            }
        } else if(this.mask == 'number'){
            maskOptions['unmaskAsNumber'] = true;
            maskOptions['digits'] = new Number(this.digits).valueOf();
        } else {

            switch (this.mask) {
                case 'cpf':
                this.mask = '999.999.999-99';
                break;
                case 'cnpj':
                this.mask = '99.999.999/9999-99';
                break;
                case 'telefone':
                this.mask = ['(99)9999-9999', '(99)99999-9999'];
                break;
                case 'cep':
                this.mask = '99999-999';
                break;
            }

            maskOptions['mask'] = this.mask;
            maskOptions['oncomplete'] = function(event) {
                _that.ng.viewToModelUpdate(event.target.value);
                _that.updateControlValue(event.target.value);
            };

            maskOptions['onKeyDown']  = function (event) {
                if(event.keyCode == 8 ){
                    if (_that.clearOnMaskIncomplete.toString() == "true") {
                        _that.ng.viewToModelUpdate(undefined);
                        _that.updateControlValue(undefined);
                        input.val(undefined);
                    }
                }
            };

            maskOptions['onincomplete'] = function (event) {
                if (_that.clearOnMaskIncomplete.toString() == "true") {
                    _that.ng.viewToModelUpdate(undefined);
                    _that.updateControlValue(undefined);
                    input.val(undefined);
                }
            };
        }
        let input = jQuery(this.el.nativeElement);
        input.inputmask(maskOptions);
    }

    updateControlValue(value: any) {
        if (this.control)
            this.control.updateValue(value, { onlySelf: false, emitEvent: true, emitModelToViewChange: false });
    }
}