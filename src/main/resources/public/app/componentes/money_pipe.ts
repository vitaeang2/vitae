import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
	name: 'MoneyPipe'
})
export class MoneyPipe {
	transform(filtro :string ) {
		let valor = "";

		if(filtro == null){
			return valor;
		}
		
		let isDot : boolean = false;
		let filtroStr = filtro.toString();
		let pos: number = 0;
		
		if(filtroStr.includes(".")){
			pos = filtroStr.indexOf(".");
			isDot = true;
		} else{
			pos = filtroStr.indexOf(",");
		}
		if(pos < 0){
			valor = `R$ ${filtro},00`;
		}else{
			if(isDot){
				valor = `R$ ${filtroStr.replace(".", ",")}`;
			} else {
				valor = `R$ ${filtroStr}`;
			}
			let str =  filtroStr.substring(pos);
			if(str.length == 2){
				valor = valor + "0";
			}
		}
		return valor;
	}

}