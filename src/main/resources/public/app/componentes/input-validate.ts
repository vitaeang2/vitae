import {Component, ElementRef, Input, Output} from '@angular/core'
import {FORM_DIRECTIVES, NgModel, NgControl} from '@angular/common';

@Component({
	selector: 'input-validate',
	providers: [NgModel],
	template: `
	<label [attr.for]="nome"><ng-content></ng-content></label>
	<input type="text" class="form-control" required [id]="nome" [(ngModel)]="value"> 
	`,
	host: {
		'(mouseenter)': 'onTeste()',
		'(keypress)': 'onTeste()'
	}
})
export class InputValidate {

	protected _value: any = '';

	@Input() nome : string;
	@Input() formulario : any;
	@Input() controle : any;
	@Output() filtrar : any;

	constructor(private ng : NgModel){

	}

	onTeste(){
		console.log(this.ng);
	}

	 get value(): any { return this._value; };
    @Input() set value(v: any) {
        if (v !== this._value) {
            this._value = v;
        }
    }
}

