import {Component, Provider, forwardRef, Input} from "@angular/core";
import {ControlValueAccessor, NG_VALUE_ACCESSOR, CORE_DIRECTIVES, NgClass} from "@angular/common";
import {MaskInputDirective} from "../componentes/mask.directive";
import {BaseInput} from "./baseinput.component";

const noop = () => {};

const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR = new Provider(
  NG_VALUE_ACCESSOR, {
    useExisting: forwardRef(() => CustomInput),
    multi: true
  });

@Component({
    selector: 'custom-input',
    template: `
        <div [ngClass]="colspan">
            <label 
                [attr.for]="id" 
                *ngIf="!hideLabel">
                <ng-content></ng-content>
            </label>
            <form>
                <input
                    #input 
                    type="text"
                    class="form-control"
                    [id]="id"
                    [(ngModel)]="value"
                    [required]="required"
                    [disabled]="disabled" 
                    maskInput="{{mask}}"
                    >
            </form>
        </div>
    `,
    directives: [NgClass, MaskInputDirective],
    providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR]
})
export class CustomInput extends BaseInput implements ControlValueAccessor{
  
    //The internal data model
    @Input() mask: string;
    
    @Input() disabled: boolean = false;
  
    //get accessor
    get value(): any { return this._value; };
  
    //set accessor including call the onchange callback
    set value(v: any) {
      if (v !== this._value) {
        this._value = v;
        this._onChangeCallback(v);
      }
    }
    
    //Set touched on blur
    onTouched() {
      this._onTouchedCallback();
    }

}