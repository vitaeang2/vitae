import { Component } from '@angular/core';

@Component({
	selector: 'input-number',
	template: `
	<input #box class="form-control" (keyup)="onKey(box.value)">
	`
})
export class InputNumber {
	values='';

  /*
  // without strong typing
  onKey(event:any) {
    this.values += event.target.value + ' | ';
  }
  */
  // with strong typing
  onKey(valorInformado:string) {
  	let value = valorInformado.charAt(valorInformado.length-1);
  	if(value === '1' || value === '2' || value === '3' || value === '4' || value === '5' || value === '6' || value === '7' || value === '8' || value === '9' || value === '0'){
  		(<HTMLInputElement>event.target).value = valorInformado;
  		this.values = valorInformado;
  	}else{
  		(<HTMLInputElement>event.target).value = this.values;
  	}
  }
}