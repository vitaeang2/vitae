import {Component, OnInit, Input, forwardRef, Provider} from '@angular/core';
import {NgClass, NG_VALUE_ACCESSOR, ControlValueAccessor} from '@angular/common';

import {MaskInputDirective} from './mask.directive';
import {DatePickerDirective} from './datepicker.directive';
import {BaseInput} from "./baseinput.component";

const CUSTOM_DATE_CONTROL_VALUE_ACCESSOR = new Provider(
    NG_VALUE_ACCESSOR, {
      useExisting: forwardRef(() => CustomDatePickerComponent),
      multi: true
    });

let nextUniqueId = 0;

@Component({
    selector: 'custom-date',
    template: `
         <div class="col-md-12">
            <label><ng-content></ng-content></label>
            <form>
                <div class="input-group date">
                    <input
                        #input 
                        type="text" 
                        [id]="id" 
                        class="form-control"
                        [(ngModel)]="value"
                        [required]="required"
                        [placeholder]="placeholder"
                        [disabled]="disabled"
                        maskInput="99/99/9999"
                        datePicker
                        >
                        <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                </div>
            </form>
        </div>
    `,
    directives: [NgClass, DatePickerDirective, MaskInputDirective],
    providers: [CUSTOM_DATE_CONTROL_VALUE_ACCESSOR]
})
export class CustomDatePickerComponent extends BaseInput implements OnInit  {

    @Input()
    private colspan: string;
    @Input() id: string = `art-date-${nextUniqueId++}`;
    @Input()  boolean = false;
    @Input() placeholder: string = '';
    @Input() disabled: boolean = false;
    
    get value(): any { return this._value; };
    @Input() set value(v: any) {
        if (v !== this._value) {
            this._value = v;
            this._onChangeCallback(v);
        }
    }

    ngOnInit(){
        this.colspan = this.getColSpan(this.colspan);
    }
}