import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
	name: 'DoublePipe'
})
export class DoublePipe {
	transform(filtro :string ) {
		let valor = "";

		if(!filtro){
			return "";
		}

		let filtroStr = filtro.toString();
		let pos: number = filtroStr.indexOf(".");
		if(pos < 0){
			valor = `${filtro},00`;
		}else{
			valor = `${filtroStr.replace(".", ",")}`;
			let str =  filtroStr.substring(pos);
			if(str.length == 2){
				valor = valor + "0";
			}
		}
		return valor;
	}

}