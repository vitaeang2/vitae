import {Directive, OnInit, Input, ElementRef} from '@angular/core';
import {NgModel, NgControlName, Control} from '@angular/common';

declare var jQuery: any;
declare var moment: any;

@Directive({
    selector: '[datePicker][ngModel]',
    providers: [NgModel, NgControlName]
})
export class DatePickerDirective implements OnInit {

    private control: Control;

    constructor(private ng: NgModel, private el: ElementRef, controlName: NgControlName) {
        this.control = controlName.control;
    }

    ngOnInit() {

        this.control = this.ng.control != undefined ? this.ng.control : this.control;

        let options = {
            format: "dd/mm/yyyy",
            language: "pt-BR",
            autoclose: true
        }

        let input = jQuery(this.el.nativeElement);
        input.datepicker(options).on('changeDate', (e) => {
            this.ng.viewToModelUpdate(moment(e.date).format('DD/MM/YYYY'));
            this.updateControlValue(moment(e.date).format('DD/MM/YYYY'));
        });
        setTimeout(()=>{
            if (this.ng.model instanceof Date)
                input.val(moment(this.ng.model).format('DD/MM/YYYY'));
            else
                input.val(this.ng.model);
        },0)
    }
    
    updateControlValue(value: any) {
        if (this.control)
            this.control.updateValue(value, { onlySelf: false, emitEvent: true, emitModelToViewChange: false });
    }
}