import {Component, ElementRef, Input, Output, EventEmitter} from '@angular/core'
import {FORM_DIRECTIVES, NgModel, NgControl} from '@angular/common';

@Component({
	selector: 'input-filtro',
	providers: [NgModel],
	template: `
	<label [attr.for]="nome"><ng-content></ng-content></label>
	<input type="text" class="form-control" [id]="filtro" [(ngModel)]="value"> 
	`,
	host: {
		'(mouseenter)': 'onTeste()',
		'(keypress)': 'onTeste()'
	}
})
export class InputFiltro {

	_value: any = '';

	@Output() filtrar = new EventEmitter();

	constructor(private ng : NgModel){

	}

	onTeste(){
		//console.log(this.ng);
	}

	 get value(): any { return this._value; };
    @Input() set value(v: any) {
        if (v !== this._value) {
            this._value = v;
        }
        this.filtrar.emit(null);
    }
}

