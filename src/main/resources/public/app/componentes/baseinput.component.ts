import {Input} from '@angular/core';
import {ControlValueAccessor} from '@angular/common';

const noop = () => {};

export abstract class BaseInput implements ControlValueAccessor {

    /** Callback registered via registerOnTouched (ControlValueAccessor) */
    protected _onTouchedCallback: () => void = noop;
    /** Callback registered via registerOnChange (ControlValueAccessor) */
    protected _onChangeCallback: (_: any) => void = noop;

    protected _value: any = '';

    getColSpan(colspan: string): string{
        if (colspan == '-1')
            colspan = undefined;

        return 'col-sm-' + (colspan ? colspan : 3);
    }
    
    writeValue(v: any) {
        this._value = v;
    }

    registerOnChange(fn: (_: any) => {}): void {
        this._onChangeCallback = fn;
    }

    registerOnTouched(fn: () => {}): void {
        this._onTouchedCallback = fn;
    }
}