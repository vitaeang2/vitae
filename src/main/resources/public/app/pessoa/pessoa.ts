import {Banco} from '../banco/banco'

export class Pessoa {

	id : number;

	nome : string;
	
	dataCadastro : string;

	tipoPessoa : number;

	naturalidade : string;

	ufNaturalidade : string;
	
	dataNascimento : string;

	estadoCivil : string;

	sexo : string;

	cpfCnpj : string;

	rgIe : string;

	emissorRgIe : string;
	
	dataExpedicaoRgIe : string;

	endereco : string;

	complemento : string;

	numeroEndereco : string;

	bairro : string;

	cep : string;

	cidade : string;

	ufCidade : string;

	telefone : string;

	celular : string;

	email : string;

	observacao : string;

	agencia : string;

	contaCorrente : string;

	observacoesBancaria : string;

	numeroPIS : string;

	numeroCTPS : string;

	banco  : Banco;

}