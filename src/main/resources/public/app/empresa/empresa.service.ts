import {Injectable} from '@angular/core';
import {Http, HTTP_PROVIDERS, Headers} from '@angular/http'
import {UrlService} from '../model/url.service';
import {HeadersService} from '../aplicacao/headers.service'
import {LoginService} from '../login/login.service';
import {PaginacaoParams} from '../model/paginacao_params';
import {Empresa} from '../empresa/empresa';

@Injectable()
export class EmpresaService  extends UrlService {

	mostrarDialogExcluir : boolean = false;

	constructor(_http: Http, _headerService: HeadersService, _loginService: LoginService) {

		super(_http, _headerService, _loginService);
	}

	public listar(params: PaginacaoParams) {

		return super.callHeader(`empresa/listar?params=${JSON.stringify(params)}`);
	}
	
	public listarCombo() {

		return super.callHeader(`empresa/listarCombo`);
	}

	public consultarId(id: number) {

		return super.callHeader(`empresa/consultarId?id=${id}`);
	}

	public excluirId(id: number) {

		return super.callHeader(`empresa/excluirId?id=${id}`);
	}

	public salvar(empresa: Empresa) {

		return super.callHeaderBody(`empresa/salvar`, JSON.stringify(empresa));
	}
}