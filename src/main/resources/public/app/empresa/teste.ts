import {LazyLoadEvent, Button, FilterMetadata} from 'primeng/primeng';
import {MaterialDesignDirective} from "../aplicacao/material";
import {MensagemService} from "../aplicacao/mensagem.service"
import {LoginService} from "../login/login.service"
import {PaginacaoParams} from '../model/paginacao_params';
import {Paginacao} from '../model/paginacao';
import {Response} from '../model/response';

export abstract class VitaeDataTable {

	params : PaginacaoParams;
	paginacao : Paginacao = new Paginacao();

	load(event: LazyLoadEvent) {

		this.params = new PaginacaoParams();
		let v : FilterMetadata = event.filters;
		if(v){
			this.params.filter = v;
			console.log(JSON.stringify(v));
		}

        //Calcula a página atual
        let page = 0;
        if (event.first > 0) {
        	page = event.first / event.rows;
        }

        this.params.page = page;
        this.params.size = event.rows;
        this.params.sortField = event.sortField;
        this.params.sortOrder = (event.sortOrder == -1) ? 'desc' : 'asc';

        this.doListar();
    }


    abstract doListar();
}