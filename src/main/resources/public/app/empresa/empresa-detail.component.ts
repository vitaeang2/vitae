import {Component} from '@angular/core';
import {OnActivate, RouteSegment, Router} from '@angular/router';
import {SelectItem} from 'primeng/primeng';
import {EmpresaService} from './empresa.service';
import {Empresa} from '../empresa/empresa';
import {MensagemService} from '../aplicacao/mensagem.service';
import {Response} from '../model/response';
import {MaskInputDirective} from '../componentes/mask.directive';
import {CustomInput} from '../componentes/custom-input.component';
import {MeuPanel} from '../aplicacao/panel';
import {EnumEstadosBrasileiros} from '../enum/enum-estados-brasileiros';
import {EnumTipoPessoa} from '../enum/enum-tipo-pessoas';

@Component({
	selector: 'empresa-detail',
	templateUrl: './app/empresa/empresa-detail.html',
	providers: [EmpresaService],
	directives: [MeuPanel, MaskInputDirective, CustomInput]
})
export class EmpresaDetailComponent implements OnActivate {

	empresa : Empresa = new Empresa();
	
	pessoas = EnumTipoPessoa;

	listaEstado = EnumEstadosBrasileiros;

	constructor(private _empresaService: EmpresaService, private _mensagemService : MensagemService, private _router: Router){
	}

	routerOnActivate(curr: RouteSegment) {
		let id = <number> +curr.getParam('id');

		if(id != -1){
			this.recuperarEmpresa(id);
		}else{
			this.limpar();
		}
	}

	salvar(){
		this.doSalvar();
		//this.voltar();
	}

	limpar(){
		this.empresa = new Empresa();
		this.empresa.tipoPessoa = EnumTipoPessoa.PJ;
	}

	voltar(){
		this._router.navigate(['/empresa']);
	}

	private recuperarEmpresa(id:number){
		this._empresaService.consultarId(id).subscribe((resultado) => {
			this.setEmpresa(resultado)
		},(error) => {
			console.log(error)
		});
	}

	private setEmpresa(resultado : Response){
		this.empresa =  <Empresa>resultado.objetoRetorno;
	}

	private doSalvar(){
		this._empresaService.salvar(this.empresa).subscribe((resultado) => {
			this._mensagemService.showInfo("Empresa cadastrada com sucesso!");
			this.voltar();
		});
	}
}
