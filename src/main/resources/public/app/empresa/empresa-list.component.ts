import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {DataTable, Column, LazyLoadEvent, Button, FilterMetadata, Dialog} from 'primeng/primeng';
import {MaterialDesignDirective} from "../aplicacao/material";
import {Empresa} from "./empresa";
import {EmpresaService} from "./empresa.service"
import {MensagemService} from "../aplicacao/mensagem.service"
import {LoginService} from "../login/login.service"
import {PaginacaoParams} from '../model/paginacao_params';
import {Paginacao} from '../model/paginacao';
import {Response} from '../model/response';
import {VitaeLazy} from '../aplicacao/vitae-lazy.service';

@Component({
    selector: 'empresa',
    templateUrl: './app/empresa/empresa-list.html',
    directives: [DataTable, Column, Button, Dialog],
    providers: [EmpresaService]
})
export class EmpresaListComponent extends VitaeLazy implements OnInit {

    private idEmpresaSelecionada: number = 0;

    public displayDialogExcluir: boolean = false;

    constructor(private _empresaService : EmpresaService, private _loginService : LoginService, private _mensagemService : MensagemService, private router: Router){
        super(router);
    }

    ngOnInit() {
       // this.doListar();
    }

    novo () { 
        super.novo('/empresa/');
    }
    
    editar (id: number) {
        super.editar(id, '/empresa/');
    }

    doListar(){
        if(this.params){
            this._empresaService.listar(this.params).subscribe((resultado) => {
                this.setListaEmpresa(resultado)
            });
        }
    }

    private setListaEmpresa(resultado : Response){

        if (!resultado.sucesso) {
            this._mensagemService.showAtencao(resultado.mensagem);
            return;
        }

        this.paginacao = resultado.objetoRetorno;
    }

    confirmDiolgExcluir(id:number) {

        this.idEmpresaSelecionada = id;
        this.displayDialogExcluir = true;
    }

    excluir() { 

        this._empresaService.excluirId(this.idEmpresaSelecionada).subscribe((resultado) => {

            this._mensagemService.showInfo("Empresa excluída com sucesso!");
            this.displayDialogExcluir = false;
            this.doListar();
        });

    }
}
