import {Component} from '@angular/core';
import {Routes, ROUTER_DIRECTIVES, Router} from '@angular/router';
import {EmpresaListComponent} from './empresa-list.component';
import {EmpresaDetailComponent} from "./empresa-detail.component";

@Component({
	selector: 'empresa',
	templateUrl: 'app/empresa/empresa.html',
	directives: [ROUTER_DIRECTIVES]
})

@Routes([
		{ path: "/", component: EmpresaListComponent},
		{ path: ":id", component: EmpresaDetailComponent}
	])

export class EmpresaComponent  {

	constructor(private _router : Router) {

		this._router.navigate(['/empresa'])
	}

}
