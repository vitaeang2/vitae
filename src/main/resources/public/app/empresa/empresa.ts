export class Empresa {

	id: number;

	nome : string;

	ativo : boolean;

	email : string;

	dataCadastro : string;

	tipoPessoa : number;

	telefone : string;

	cpfCnpj : string;

	inscricaoEstadual : string;

	inscricaoMunicipal : string;

	endereco : string;

	cidade : string;

	bairro: string;

	cep: string;
	
	ufCidade: string;

	constructor(){}

	setNome(nome : string){
		 this.nome = nome;
	}
	getNome(){
		return this.nome;
	}
}