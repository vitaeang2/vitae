import {ItemPedidoCompra} from './item_pedido_compra';
import {Fornecedor} from '../fornecedor/fornecedor';
import {Empresa} from '../empresa/empresa';
import {Nucleo} from '../nucleo/nucleo';
import {Colaborador} from '../colaborador/colaborador';
import {Cotacao} from '../cotacao/cotacao';

export class PedidoCompra{
	id: number;
	vlrFrete: number;
	vlrDesconto: number;
	vlrTotal: number;
	itensPedidoCompra : Array<ItemPedidoCompra>;
	fornecedor: Fornecedor;
	colaborador : Colaborador;
	cotacao : Cotacao;
	empresa : Empresa;
	nucleo : Nucleo;
	status : number;
	dataPedidoCompra : string;
}