import {PedidoCompra} from './pedido_compra';
import {Produto} from '../produto/produto';
import {EnumItemPedidoCompra} from '../enum/enum-item-pedido-compra';

export class ItemPedidoCompra{
	id: number;
	produto: Produto;
	pedidoCompra: PedidoCompra;
	quantidade: number;
	vlrUnitario: number;
	status: number;
}