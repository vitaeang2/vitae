import {Component} from '@angular/core';
import {NgClass} from '@angular/common';
import {OnActivate, RouteSegment, Router} from '@angular/router';
import {SelectItem} from 'primeng/primeng';
import {PedidoCompraService} from './pedido_compra.service';
import {PedidoCompra} from '../pedido_compra/pedido_compra';
import {ItemPedidoCompra} from '../pedido_compra/item_pedido_compra';
import {Cotacao} from '../cotacao/cotacao';
import {ItemCotacao} from '../cotacao/item_cotacao';
import {CotacaoService} from '../cotacao/cotacao.service';
import {CotacaoFornecedor} from '../cotacao/cotacao_fornecedor';
import {Fornecedor} from '../fornecedor/fornecedor';
import {MensagemService} from '../aplicacao/mensagem.service';
import {Response} from '../model/response';
import {MaskInputDirective} from '../componentes/mask.directive';
import {CustomInput} from '../componentes/custom-input.component';
import {MeuPanel} from '../aplicacao/panel';
import {EnumPedidoCompra} from '../enum/enum-pedido-compra';
import {EnumItemPedidoCompra} from '../enum/enum-item-pedido-compra';
import {EnumTipoPessoa} from '../enum/enum-tipo-pessoas';
import {EnumUnidadeMedida} from '../enum/enum-unidade-medida';
import {MovimentoEstoqueComponent} from '../movimento_estoque/movimento_estoque.component';
import {MovimentoEstoque} from '../movimento_estoque/movimento_estoque';
import {MovimentoEstoqueService} from '../movimento_estoque/movimento_estoque.service';
import {Empresa} from '../empresa/empresa';
import {EmpresaService} from '../empresa/empresa.service';
import {NucleoService} from '../nucleo/nucleo.service';
import {Nucleo} from '../nucleo/nucleo';
import {Produto} from '../produto/produto';
import {Colaborador} from '../colaborador/colaborador';
import {LoginService} from "../login/login.service";

@Component({
	selector: 'pedido_compra-selecionado-detail',
	templateUrl: './app/pedido_compra/pedido_compra-detail-selecionado.html',
	providers: [PedidoCompraService, CotacaoService, MovimentoEstoqueService],
	directives: [MeuPanel, MaskInputDirective, CustomInput, NgClass],
	styleUrls: ['./app/cotacao/table.css']
})
export class PedidoCompraDetailSelecionadoComponent implements OnActivate {

	idPedidoCompra: number;
	isDesabilitado: boolean;

	pedidoCompra: PedidoCompra = new PedidoCompra();

	constructor(private _loginService : LoginService, private _movimentoEstoqueService : MovimentoEstoqueService, 
		private _pedidoCompraService: PedidoCompraService, private _mensagemService : MensagemService, private _router: Router){
	}

	routerOnActivate(curr: RouteSegment) {
		this.isDesabilitado = false;
		this.idPedidoCompra = <number> +curr.getParam('id');

		if(this.idPedidoCompra != -1){
			this.recuperarPedidoCompra(this.idPedidoCompra);
		}
	}

	voltar(){
		this._router.navigate(['/pedidocompra']);
	}
	
	marcarNaoComprar(item: ItemPedidoCompra) {
		item.status = EnumItemPedidoCompra.FINALIZADO_SEM_COMPRAR.codigo;
	}

	marcarComprar(item: ItemPedidoCompra) {
		item.status = EnumItemPedidoCompra.FINALIZADO_COMPRADO.codigo;
	}

	/*darEntradaEstoque(){
		this.isDesabilitado = true;
		let itensPedidoCompra : Array<ItemPedidoCompra> = this.pedidoCompra.itensPedidoCompra;
		//this.pedidoCompra.itensPedidoCompra = null;
		this._movimentoEstoqueService.entrarEstoque(this.pedidoCompra).subscribe((resultado) => {
			this.setAposEntrada(resultado);
			this.voltar();
		});
	}*/

	darEntradaEstoque(ic: ItemPedidoCompra){
		this.isDesabilitado = true;
		//this.pedidoCompra.itensPedidoCompra = null;
		this._movimentoEstoqueService.entrarEstoque(this.pedidoCompra.id, ic).subscribe((resultado) => {
			this.setAposEntrada(resultado);
			//this.voltar();
		});
	}

	private recuperarPedidoCompra(id: number){
		this._pedidoCompraService.consultarComListaItens(id).subscribe((resultado) => {
			this.setMontarPedido(resultado);
		});
	}

	private setAposEntrada(resultado: Response){
		if(!resultado.sucesso){
			this._mensagemService.showInfo(resultado.mensagem);
			return;
		}

		this._mensagemService.showInfo("Produto adicionado ao estoque!");
		this.recuperarPedidoCompra(this.idPedidoCompra);
		//this.pedidoCompra = resultado.objetoRetorno;
		//this.montarItens(this.pedidoCompra.itensPedidoCompra);
	}

	private setMontarPedido(resultado: Response){
		if(!resultado.sucesso){
			this._mensagemService.showInfo(resultado.mensagem);
			return;
		}
		this.pedidoCompra = resultado.objetoRetorno;
		this.isDesabilitado = this.pedidoCompra.status == EnumPedidoCompra.FINALIZADO.codigo;
		this.montarItens(this.pedidoCompra.itensPedidoCompra);
	}

	private montarItens(itens: Array<ItemPedidoCompra>){
		this.pedidoCompra.vlrTotal = 0;
		itens.forEach((item) => {
			this.pedidoCompra.vlrTotal += (item.quantidade * item.vlrUnitario);
			if(item.produto.unidadeMedida){
				item.produto.unidadeMedidaStr = EnumUnidadeMedida.getByCod(item.produto.unidadeMedida.toString()).descricao;
			}
		});

		this.pedidoCompra.vlrTotal += (this.pedidoCompra.vlrFrete - this.pedidoCompra.vlrDesconto);
	}

}