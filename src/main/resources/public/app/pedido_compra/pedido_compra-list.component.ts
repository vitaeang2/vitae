import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {DataTable, Column, LazyLoadEvent, Button, FilterMetadata, SelectItem, Dropdown} from 'primeng/primeng';
import {PedidoCompra} from "./pedido_compra";
import {PedidoCompraService} from "./pedido_compra.service";
import {MensagemService} from "../aplicacao/mensagem.service";
import {LoginService} from "../login/login.service";
import {PaginacaoParams} from '../model/paginacao_params';
import {Paginacao} from '../model/paginacao';
import {Response} from '../model/response';
import {VitaeLazy} from '../aplicacao/vitae-lazy.service';
import {Nucleo} from "../nucleo/nucleo";
import {NucleoService} from '../nucleo/nucleo.service';
import {EnumPedidoCompra} from "../enum/enum-pedido-compra";


@Component({
    selector: 'pedidoCompra',
    templateUrl: './app/pedido_compra/pedido_compra-list.html',
    directives: [DataTable, Column, Button, Dropdown],
    providers: [NucleoService, PedidoCompraService]
})
export class PedidoCompraListComponent extends VitaeLazy {

    params : PaginacaoParams;
    paginacao : Paginacao = new Paginacao();
    listaNucleo : SelectItem[];
    idNucleoSelecionado : number;
	idTipoPedidoSelecionado : number;
    mostarFiltroNucleo :boolean = false;
    idNucleo : number = 0;
	listaTipoPedido: SelectItem[];

    constructor(private _nucleoService: NucleoService, private _pedidoCompraService : PedidoCompraService, private _loginService : LoginService, private _mensagemService : MensagemService, private router: Router){
        super(router);
        
        this.setListaTipoPedido();
        this.idTipoPedidoSelecionado = 0;

        let nucleo: Nucleo = this._loginService.getUser().colaborador.nucleo;
       
        if (nucleo && nucleo.id) {
            this.idNucleo = nucleo.id;
            this.mostarFiltroNucleo = false;
            this.idNucleoSelecionado = this.idNucleo;
        } else {

            this.idNucleo = 0;
            this.mostarFiltroNucleo = true;
            this.idNucleoSelecionado = 0;
            this.carregarListaNucleo();
        }

        //this.doListar();
    }

    novo() { 
        super.novo('/pedidocompra/');
    }
    
    editar(id: number){
        super.editar(id, '/pedidocompra/selecionado/');
    }
    
    doListar(){
    	
    	if(this.idNucleoSelecionado && this.idNucleoSelecionado != 0){
            this.listarPorNucleo(this.idNucleoSelecionado);
        }else{
            this.listarPorNucleo(this.idNucleo);
        }
    }
    
    listarPorTipo(idTipoPedidoSelecionado: number){
    	this.idTipoPedidoSelecionado = idTipoPedidoSelecionado;
    	this.doListar();
    }
    
    listarPorNucleo(idNucleo: number){
    	this._pedidoCompraService.listar(this.params, this.idTipoPedidoSelecionado, this._loginService.getUser().colaborador.empresa.id, this.idNucleoSelecionado).subscribe((resultado) => {
            this.setListaPedidoCompra(resultado)
        });
    }
    
    private setListaTipoPedido(){
        this.listaTipoPedido = [];
        this.listaTipoPedido.push({label: "-- TODOS --", value: 0});
        EnumPedidoCompra.toArray().forEach(epc => this.listaTipoPedido.push({label: epc.descricao, value: epc.codigo}));
    }

    private setListaPedidoCompra(resultado : Response){
        if(!resultado.sucesso){
            this._mensagemService.showAtencao(resultado.mensagem);
            return;
        }
        this.paginacao = resultado.objetoRetorno;
    }
    
    private carregarListaNucleo(){
        this._nucleoService.listarComboPorIdEmpresa(this._loginService.getUser().colaborador.empresa.id).subscribe((resultado) => {
            this.setListaNucleo(resultado)
        });
    }
    
    private setListaNucleo(resultado : Response){
        if(!resultado.sucesso){
            this._mensagemService.showAtencao(resultado.mensagem);
            return;
        }
        this.listaNucleo = [];
        let lista : Array<Nucleo> = resultado.objetoRetorno;
        this.listaNucleo.push({label: "-- TODOS --", value: 0});
        lista.forEach(nuc => this.listaNucleo.push({label: nuc.nome, value: nuc.id}));
    }
}
