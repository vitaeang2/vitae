import {Component} from '@angular/core';
import {Routes, ROUTER_DIRECTIVES, Router} from '@angular/router';
import {PedidoCompraListComponent} from './pedido_compra-list.component';
import {PedidoCompraDetailComponent} from "./pedido_compra-detail.component";
import {PedidoCompraDetailSelecionadoComponent} from "./pedido_compra-detail-selecionado.component";

@Component({
	selector: 'pedido-compra',
	templateUrl: 'app/pedido_compra/pedido_compra.html',
	directives: [ROUTER_DIRECTIVES]
})
@Routes([
	{ path: "/", component: PedidoCompraListComponent},
	{ path: "selecionar/:id", component: PedidoCompraDetailComponent},
	{ path: "selecionado/:id", component: PedidoCompraDetailSelecionadoComponent},
	])
export class PedidoCompraComponent  {
	constructor(private _router : Router) {
		//this._router.navigate(['/pedidocompra'])
	}
}
