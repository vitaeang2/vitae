import {Injectable} from '@angular/core';
import {Http, HTTP_PROVIDERS, Headers} from '@angular/http'
import {UrlService} from '../model/url.service';
import {HeadersService} from '../aplicacao/headers.service'
import {LoginService} from '../login/login.service';
import {PedidoCompra} from '../pedido_compra/pedido_compra';
import {PaginacaoParams} from '../model/paginacao_params';
import {ItemPedidoCompra} from './item_pedido_compra';

@Injectable()
export class PedidoCompraService  extends UrlService {

	private urlNegocio : string = 'pedido_compra';

	constructor(_http: Http, _headerService: HeadersService, _loginService: LoginService) {
		super(_http, _headerService, _loginService);
	}

	public listar(params: PaginacaoParams, idTipoPedidoSelecionado: number, idEmpresa: number, idNucleo: number) {
		return super.callHeader(`${this.urlNegocio}/listar?params=${JSON.stringify(params)}&idTipoPedidoSelecionado=${idTipoPedidoSelecionado}&idEmpresa=${idEmpresa}&idNucleo=${idNucleo}`);
	}

	public consultarComListaItens(id: number) {
		return super.callHeader(`${this.urlNegocio}/consultarComListaItens?&id=${id}`);
	}

	public salvar(pedidoCompra: PedidoCompra, itensPedidoCompra : Array<ItemPedidoCompra>, idCotacao: number) {
		return super.callHeader(`${this.urlNegocio}/salvar?pedidoCompra=${JSON.stringify(pedidoCompra)}&itens=${JSON.stringify(itensPedidoCompra)}&idCotacao=${idCotacao}`);
	}

}