import {Component} from '@angular/core';
import {NgClass} from '@angular/common';
import {OnActivate, RouteSegment, Router} from '@angular/router';
import {SelectItem, Dialog} from 'primeng/primeng';
import {PedidoCompraService} from './pedido_compra.service';
import {PedidoCompra} from '../pedido_compra/pedido_compra';
import {ItemPedidoCompra} from '../pedido_compra/item_pedido_compra';
import {Cotacao} from '../cotacao/cotacao';
import {ItemCotacao} from '../cotacao/item_cotacao';
import {CotacaoService} from '../cotacao/cotacao.service';
import {CotacaoFornecedor} from '../cotacao/cotacao_fornecedor';
import {Fornecedor} from '../fornecedor/fornecedor';
import {MensagemService} from '../aplicacao/mensagem.service';
import {Response} from '../model/response';
import {MaskInputDirective} from '../componentes/mask.directive';
import {CustomInput} from '../componentes/custom-input.component';
import {MeuPanel} from '../aplicacao/panel';
import {EnumEstadosBrasileiros} from '../enum/enum-estados-brasileiros';
import {EnumTipoPessoa} from '../enum/enum-tipo-pessoas';
import {EnumUnidadeMedida} from '../enum/enum-unidade-medida';
import {MovimentoEstoqueComponent} from '../movimento_estoque/movimento_estoque.component';
import {MovimentoEstoque} from '../movimento_estoque/movimento_estoque';
import {MovimentoEstoqueService} from '../movimento_estoque/movimento_estoque.service';
import {Empresa} from '../empresa/empresa';
import {EmpresaService} from '../empresa/empresa.service';
import {NucleoService} from '../nucleo/nucleo.service';
import {Nucleo} from '../nucleo/nucleo';
import {Produto} from '../produto/produto';
import {Colaborador} from '../colaborador/colaborador';
import {LoginService} from "../login/login.service";
import {TimerWrapper} from '@angular/core/src/facade/async';
import {UrlService} from '../model/url.service';

@Component({
	selector: 'pedido_compra-detail',
	templateUrl: './app/pedido_compra/pedido_compra-detail.html',
	providers: [PedidoCompraService, CotacaoService],
	directives: [MeuPanel, MaskInputDirective, CustomInput, NgClass, Dialog],
	styleUrls: ['./app/cotacao/table.css']
})
export class PedidoCompraDetailComponent implements OnActivate {

	pedidoCompra : PedidoCompra = new PedidoCompra();

	listaFornecedores : Array<CotacaoFornecedor>;
	
	listaPedidoCompra : Array<PedidoCompra>;

	listaItens : Array<ItemCotacao>;

	lista: any[] = [];

	idCotacao : number; 
	
	size : number = 0; 

	valorTotal : number = 0.0;

	podeSalvar = false;
	
	pessoas = EnumTipoPessoa;

	listaEstado = EnumEstadosBrasileiros;

	produto : Produto;

	empresa : Empresa;

	nomeNucleo : string = "";
	
	dataValidade : string = "";

	descricao : string = "";

	displayDialog : boolean = false;

	url : string;

	isEnviandoEmail = false; 

	cf;

	constructor(private _cotacaoService: CotacaoService, private _loginService : LoginService,
		private _pedidoCompraService: PedidoCompraService, private _mensagemService : MensagemService, private _router: Router){
	}

	routerOnActivate(curr: RouteSegment) {
		this.url = UrlService.url + "#/fornecedorcotacao";
		this.idCotacao = <number> +curr.getParam('id');

		if(this.idCotacao != -1){
			this.recuperarPedidoCompra(this.idCotacao);
		} else {
			this.limpar();
		}

		this.doListaCotacaoFornecedor();
	}

	abrirDialog(cf){
		this.displayDialog = true;
		this.cf = cf;
	}

	enviarEmail(nome: string, token: string, email: string){
		this.isEnviandoEmail = true; 
		this._cotacaoService.enviarEmail(nome, email, token)
		.subscribe((resultado) => {
			this.setEnviouEmail(resultado);
		},(error) => {
			console.log(error)
		});
	}

	setEnviouEmail(resultado: Response){
		this.isEnviandoEmail = false; 
		if(!resultado.sucesso){
			this._mensagemService.showInfo(resultado.mensagem);
			return;
		}
		this._mensagemService.showInfo("Email enviado com sucesso!");
	}
	salvar(){
		this.podeSalvar = false;
		let pedidoCompra = this.listaPedidoCompra.pop();
		let itens = pedidoCompra.itensPedidoCompra;
		pedidoCompra.itensPedidoCompra = null;
		this.doSalvar(pedidoCompra, itens);
	}

	limpar(){
		this.pedidoCompra = new PedidoCompra();
	}

	voltar(){
		this._router.navigate(['/cotacao']);
		//this._router.navigate(['/pedidocompra/']);
	}

	selecionar(pos, poslat){

		this.lista.forEach((l, index) => {
			l.itens[pos].selecionado = poslat === index;
		});

		this.calcularTotal();
	}

	calcularMenorPreco(){
		let menor = 99999999;
		let maior = -1;
		let posMenor = -1;
		let posMaior = -1;
		for (var i = 0; i < this.lista.length; i++) {
			let cfAtual = this.lista[i];
			if(cfAtual['vlrTotal'] < menor){
				posMenor = i;
				menor = cfAtual['vlrTotal'];
			}
			if(cfAtual['vlrTotal'] > maior){
				posMaior = i;
				maior = cfAtual['vlrTotal'];
			}
		}

		this.lista[posMenor].menorPreco = true;
		this.lista[posMaior].maiorPreco = true;
	}

	calcularTotal(){
		this.listaPedidoCompra = new Array<PedidoCompra>();
		let itensPedidoCompra;
		let vlrFrete = 0.0;
		let vlrDesconto = 0.0;
		let idFornecedor;
		let calcularFreteEDesconto = false;
		let qdtSelecionado = 0;
		this.valorTotal = 0.0;
		this.lista.forEach((l) => {
			itensPedidoCompra = new Array<ItemPedidoCompra>();
			vlrDesconto = l.vlrDesconto;
			vlrFrete = l.vlrFrete;
			idFornecedor = l.idF;
			l.itens.forEach((item) => {
				if(item.selecionado){
					itensPedidoCompra.push(this.montarItemPedido(itensPedidoCompra, item));
					this.valorTotal += item.vlrTotal;
					calcularFreteEDesconto = true; 
					qdtSelecionado++;
				}
			});
			if(calcularFreteEDesconto){
				this.listaPedidoCompra.push(this.montarPedidoCompra(vlrDesconto, vlrFrete, idFornecedor, itensPedidoCompra));
				this.valorTotal += (vlrFrete - vlrDesconto);
			}
			calcularFreteEDesconto = false;
		});

		console.log(this.listaPedidoCompra);
		let tam = this.listaItens.length;
		if(tam > qdtSelecionado){
			this.podeSalvar = false;
		}else{
			this.podeSalvar = true;
		}
	}

	private montarPedidoCompra(vlrDesconto, vlrFrete, idFornecedor, itensPedidoCompra) : PedidoCompra {
		let pedidocompra = new PedidoCompra();
		
		pedidocompra.colaborador = new Colaborador();		
		pedidocompra.colaborador.id = this._loginService.getUser().colaborador.id;		

		pedidocompra.empresa = new Empresa();
		pedidocompra.empresa.id = this._loginService.getUser().colaborador.empresa.id;
		let nucleo = this._loginService.getUser().colaborador.nucleo;
		if(nucleo){
			pedidocompra.nucleo = new Nucleo();
			pedidocompra.nucleo.id = nucleo.id;			
		}

		pedidocompra.fornecedor = new Fornecedor();
		pedidocompra.fornecedor.id = idFornecedor;

		pedidocompra.vlrDesconto = vlrDesconto;
		pedidocompra.vlrFrete = vlrFrete;
		pedidocompra.itensPedidoCompra = itensPedidoCompra;

		return pedidocompra;
	}

	private montarItemPedido(itensPedidoCompra, item) : ItemPedidoCompra{
		let itemPedidoCompra = new ItemPedidoCompra();

		itemPedidoCompra.produto = new Produto();
		itemPedidoCompra.produto.id = item.idProduto;

		itemPedidoCompra.vlrUnitario = item.vlrUnitario;
		itemPedidoCompra.quantidade = item.quantidade;

		return itemPedidoCompra ;
	}

	private recuperarPedidoCompra(id:number){
		this._cotacaoService.listarFornecedoresPorPedidoCompra(id).subscribe((resultado) => {
			this.setListaCotacaoFornecedor(resultado)
		},(error) => {
			console.log(error)
		});
	}

	private setListaCotacaoFornecedor(resultado : Response){
		this.listaFornecedores =  resultado.objetoRetorno;
		this.size = 560 * this.listaFornecedores.length;
		this.lista = [];

		for (var i = this.listaFornecedores.length - 1; i >= 0; i--) {
			let cf: CotacaoFornecedor = this.listaFornecedores[i];

			this.lista[i]= [];
			this.lista[i]['vlrTotal'] = 0;
			this.lista[i]['selecionado'] = false;
			this.lista[i]['idF'] = cf.fornecedor.id;
			this.lista[i]['nomeF'] = cf.fornecedor.pessoa.nome;
			this.lista[i]['token'] = cf.token;
			this.lista[i]['vlrDesconto'] = cf.vlrDesconto;
			this.lista[i]['vlrFrete'] = cf.vlrFrete;
			this.lista[i]['menorPreco'] = false;
			this.lista[i]['itens'] = [];
			this.doItens(i);
			this.nomeNucleo = cf.cotacao.nucleo.nome;
			this.dataValidade = cf.cotacao.dataCotacao;
			this.descricao = cf.cotacao.descricao;
		}
	}

	private doItens(pos : number){
		this._cotacaoService.listarItensPorCotacaoFornecedor(this.listaFornecedores[pos].id).subscribe((resultado) => {
			this.setItens(pos, resultado)
		});
	}

	private setItens(pos : number, resultado : Response){
		this.listaFornecedores[pos].itens = resultado.objetoRetorno;
		let vlrTotal = 0;
		let vlrPorProd = 0;
		for (var i = 0; i < this.listaFornecedores[pos].itens.length; i++) {
			let ic : ItemCotacao = this.listaFornecedores[pos].itens[i];

			vlrPorProd = ic.vlrUnitario * ic.quantidade;
			vlrTotal += vlrPorProd;
			this.lista[pos]['vlrTotal'] = vlrTotal + this.lista[pos]['vlrFrete'] - this.lista[pos]['vlrDesconto'];
			this.lista[pos]['itens'].push({idProduto: ic.produto.id, nome: ic.produto.nome, quantidade: ic.quantidade, vlrUnitario: ic.vlrUnitario, vlrTotal: vlrPorProd});
		}
	}

	private doSalvar(pedidoCompra, itens){
		this._pedidoCompraService.salvar(pedidoCompra, itens, this.idCotacao).subscribe((resultado) => {
			this._mensagemService.showInfo("Pedido Compra cadastrado com sucesso!");
			let pedidoCompra = this.listaPedidoCompra.pop();
			let itens = pedidoCompra.itensPedidoCompra;
			pedidoCompra.itensPedidoCompra = null;
			this.doSalvar(pedidoCompra, itens);
		});
		TimerWrapper.setTimeout(() => {  
			this.voltar();
		}, 2000);
	}

	private doListaCotacaoFornecedor(){
		this._cotacaoService.listarItensPorCotacao(this.idCotacao).subscribe((resultado) => {
			this.setListaItens(resultado)
		})
	}

	private setListaItens(resultado : Response){
		let lista = [];
		this.listaItens = [];
		for (var i = 0; i < resultado.objetoRetorno.length; i++) {
			let ic = resultado.objetoRetorno[i];
			let id = ic.produto.id;

			if(lista.indexOf(id) < 0){
				if(ic.produto.unidadeMedida){
					ic.produto.unidadeMedidaStr = EnumUnidadeMedida.getByCod(ic.produto.unidadeMedida.toString()).descricao;
				}
				lista.push(id);
				this.listaItens.push(ic);
			}
		}
	}

}
