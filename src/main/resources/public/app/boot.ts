/// <reference path="../node_modules/typescript/lib/lib.es6.d.ts" />

import {bootstrap,} from '@angular/platform-browser-dynamic';
import {AppComponent} from './aplicacao/app.component'
import {Http, HTTP_PROVIDERS} from '@angular/http'
import {LoginService} from './login/login.service'
import {UsuarioService} from './usuario/usuario.service'
import {HeadersService} from './aplicacao/headers.service'
import {MenuService} from './menu/menu.service'
import {enableProdMode, provide} from '@angular/core';
import {LocationStrategy, HashLocationStrategy, PathLocationStrategy} from '@angular/common';
import {ROUTER_PROVIDERS} from '@angular/router';
import {MensagemService} from './aplicacao/mensagem.service'

enableProdMode();

bootstrap(AppComponent, [ROUTER_PROVIDERS, provide(LocationStrategy, {useClass: HashLocationStrategy}), HeadersService, HTTP_PROVIDERS, LoginService, 
	UsuarioService, MenuService, MensagemService]);
