import {Injectable} from '@angular/core';
import {Http, HTTP_PROVIDERS, Headers} from '@angular/http'
import {UrlService} from '../model/url.service';
import {HeadersService} from '../aplicacao/headers.service'
import {LoginService} from '../login/login.service';
import {PaginacaoParams} from '../model/paginacao_params';
import {Cargo} from '../cargo/cargo';

@Injectable()
export class CargoService  extends UrlService {

	constructor(_http: Http, _headerService: HeadersService, _loginService: LoginService) {
		super(_http, _headerService, _loginService);
	}

	public listar(params: PaginacaoParams) {
		return super.callHeader(`cargo/listar?params=${JSON.stringify(params)}`);
	}
	
	public listarCombo() {
		return super.callHeader(`cargo/listarCombo`);
	}

	public consultarId(id: number) {
		return super.callHeader(`cargo/consultarId?id=${id}`);
	}

	public salvar(cargo: Cargo) {
		return super.callHeaderBody(`cargo/salvar` ,JSON.stringify(cargo));
	}

	public excluirId(id: number) {

		return super.callHeader(`cargo/excluirId?id=${id}`);
	}
}