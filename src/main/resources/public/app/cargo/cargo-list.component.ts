import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {DataTable, Column, LazyLoadEvent, Button,Dialog} from 'primeng/primeng';
import {MaterialDesignDirective} from "../aplicacao/material";
import {Cargo} from "./cargo";
import {CargoService} from "./cargo.service"
import {MensagemService} from "../aplicacao/mensagem.service"
import {LoginService} from "../login/login.service"
import {PaginacaoParams} from '../model/paginacao_params';
import {Paginacao} from '../model/paginacao';
import {Response} from '../model/response';

@Component({
    selector: 'cargo',
    templateUrl: './app/cargo/cargo-list.html',
    directives: [DataTable, Column, Button,Dialog],
    providers: [CargoService]
})
export class CargoListComponent  implements OnInit {

    params : PaginacaoParams;
    paginacao : Paginacao = new Paginacao();

    private itemSelecionadoId: number = 0;

    public displayDialogExcluir: boolean = false;

    constructor(private _cargoService : CargoService, private _loginService : LoginService, private _mensagemService : MensagemService, private _router: Router){
    }

    ngOnInit(){
        this.doListar();
    }

    novo() {
        this._router.navigate(['/cargo/', -1])
    }
    
    editar(id: number){
        this._router.navigate(['/cargo/',  id ])
    }

    load(event: LazyLoadEvent) {

        this.params = new PaginacaoParams();

        //Calcula a página atual
        let page = 0;
        if (event.first > 0) {
            page = event.first / event.rows;
        }

        this.params.page = page;
        this.params.size = event.rows;
        this.params.sortField = event.sortField;
        this.params.sortOrder = (event.sortOrder == -1) ? 'desc' : 'asc';

        this.doListar();
    }

    private doListar(){
        if(this.params){
            this._cargoService.listar(this.params).subscribe((resultado) => {
                this.setListaCargo(resultado)
            });
        }
    }

    private setListaCargo(resultado : Response){
        if(!resultado.sucesso){
            this._mensagemService.showAtencao(resultado.mensagem);
            return;
        }
        this.paginacao = resultado.objetoRetorno;
    }

    confirmDiolgExcluir(id:number) {

        this.itemSelecionadoId = id;
        this.displayDialogExcluir = true;
    }

    excluir() { 

        this._cargoService.excluirId(this.itemSelecionadoId).subscribe((resultado) => {

            this._mensagemService.showInfo("Cargo excluído com sucesso!");
            this.displayDialogExcluir = false;
            this.doListar();
        });

    }
}
