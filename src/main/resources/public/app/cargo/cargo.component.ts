import {Component} from '@angular/core';
import {Routes, ROUTER_DIRECTIVES, Router} from '@angular/router';
import {CargoListComponent} from './cargo-list.component';
import {CargoDetailComponent} from "./cargo-detail.component";

@Component({
	selector: 'cargo',
	templateUrl: 'app/cargo/cargo.html',
	directives: [ROUTER_DIRECTIVES]
})
@Routes([
	{ path: "/", component: CargoListComponent},
	{ path: ":id", component: CargoDetailComponent}
	])
/*
	Como comitar no git
	git add . <enter>
	git status <enter>
	git commit -m "incluir comentario" <enter>
	git push -u origin
*/
export class CargoComponent  {
	constructor(private _router : Router) {
		this._router.navigate(['/cargo'])
	}
}
