import {Component} from '@angular/core';
import {OnActivate, RouteSegment, Router} from '@angular/router';
import {SelectItem} from 'primeng/primeng';
import {CargoService} from './cargo.service'
import {Cargo} from '../cargo/cargo'
import {MensagemService} from '../aplicacao/mensagem.service';
import {Response} from '../model/response'
import {MeuPanel} from '../aplicacao/panel';
import {EnumEstadosBrasileiros} from '../enum/enum-estados-brasileiros';

@Component({
	selector: 'cargo-detail',
	templateUrl: './app/cargo/cargo-detail.html',
	providers: [CargoService],
	directives: [MeuPanel]
})
export class CargoDetailComponent implements OnActivate {

	cargo : Cargo = new Cargo();

	constructor(private _cargoService: CargoService, private _mensagemService : MensagemService, private _router: Router){
	}

	routerOnActivate(curr: RouteSegment) {
		let id = <number> +curr.getParam('id');

		if(id != -1){
			this.recuperarCargo(id);
		}else{
			this.limpar();
		}
	}

	salvar(){

		this.doSalvar();

	}

	limpar(){
		this.cargo = new Cargo();
	}

	voltar(){
		this._router.navigate(['/cargo']);
	}

	private recuperarCargo(id:number){
		this._cargoService.consultarId(id).subscribe((resultado) => {
			this.setCargo(resultado)
		},(error) => {
			console.log(error)
		});
	}

	private setCargo(resultado : Response){
		this.cargo =  <Cargo>resultado.objetoRetorno;
	}

	private doSalvar(){
		this._cargoService.salvar(this.cargo).subscribe((resultado) => {
			this._mensagemService.showInfo("Cargo cadastrado com sucesso!");
			this.voltar();
		});
	}
}
