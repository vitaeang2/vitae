import {Cotacao} from './cotacao'
import {Fornecedor} from '../fornecedor/fornecedor'
import {ItemCotacao} from '../cotacao/item_cotacao'

export class CotacaoFornecedor {

	id : number;
	vlrFrete: number;
	vlrDesconto: number;
	token: string;
	cotacao : Cotacao;
	fornecedor : Fornecedor;
	menorPreco : string;
	itens: Array<ItemCotacao>;
}