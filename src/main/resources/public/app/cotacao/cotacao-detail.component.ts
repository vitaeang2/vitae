import {Component, Input} from '@angular/core';
import {OnActivate, RouteSegment, Router} from '@angular/router';
import {LoginService} from "../login/login.service"
import {SelectItem} from 'primeng/primeng';
import {CustomInput} from '../componentes/custom-input.component';
import {MyNgInclude} from '../aplicacao/ng-include.component';
import {MensagemService} from '../aplicacao/mensagem.service';
import {Cotacao} from '../cotacao/cotacao';
import {ItemCotacao} from './item_cotacao';
import {CotacaoService} from './cotacao.service';
import {Produto} from '../produto/produto';
import {ProdutoService} from '../produto/produto.service';
import {Fornecedor} from '../fornecedor/fornecedor';
import {FornecedorService} from '../fornecedor/fornecedor.service';
import {Response} from '../model/response';
import {NucleoService} from '../nucleo/nucleo.service';
import {MeuPanel} from '../aplicacao/panel';
import {CustomDatePickerComponent} from '../componentes/custom-datepicker.component';
import {EnumStatusCotacao} from '../enum/enum-status-cotacao';
import {ItemComponentValidacao} from './item_cotacao-validacao.component';
import {Empresa} from '../empresa/empresa';
import {Nucleo} from '../nucleo/nucleo';
import {DataValidator} from '../aplicacao/data-validator.directive';

import {TimerWrapper} from '@angular/core/src/facade/async';

@Component({
	selector: 'cotacao-detail',
	templateUrl: './app/cotacao/cotacao-detail.html',
	providers: [CotacaoService, FornecedorService, ProdutoService, NucleoService],
	directives: [MeuPanel, CustomInput, ItemComponentValidacao, CustomDatePickerComponent, DataValidator],
	styleUrls: ['./app/cotacao/table.css']
})
export class CotacaoDetailComponent implements OnActivate {

	cotacao : Cotacao = new Cotacao();
	
	listaProdutos : Array<Produto>;
	
	listaProdutosAux : Array<Produto>;

	listaProdutosIdAux : Array<number>;
	
	listaProdutosSelecionados : Array<ItemCotacao>;
	
	listaFornecedores : Array<Fornecedor>;

	listaFornecedoresAux : Array<Fornecedor>;

	listaFornecedoresIdAux : Array<number>;
	
	listaFornecedoresSelecionados : Array<Fornecedor>;

	listaNucleo : SelectItem[];

	isEnviado : boolean = false;

	idNucleo : number;
	
	idCotacao : number;
	
	filtroFornecedor : string;

	filtroProduto : string;

	alterou : boolean = false;

	numTela : number = 1; // 1 - tela de seleção de produtos, 2 - tela para informar qtd, 3 - tela para selecionar fornecedores. 

	constructor(private _produtoService: ProdutoService, private _fornecedorService:FornecedorService, 
		private _cotacaoService: CotacaoService, private _loginService: LoginService, 
		private _nucleoService : NucleoService, private _mensagemService : MensagemService, private _router: Router){
	}

	routerOnActivate(curr: RouteSegment) {
		let id = <number> +curr.getParam('id');
		this.idCotacao = id;
		this.listaProdutosIdAux = new Array<number>();
		this.listaProdutosAux = new Array<Produto>();
		this.listaFornecedoresIdAux = new Array<number>();
		this.listaFornecedoresAux = new Array<Fornecedor>();
		this.carregarProduto();
		if(id != -1){
			this.recuperarCotacao(id);
		}else{
			this.limpar();
			this.cotacao.empresa = new Empresa();
			this.cotacao.empresa.id = this._loginService.getUser().colaborador.empresa.id;
			let nucleo = this._loginService.getUser().colaborador.nucleo;
			this.cotacao.nucleo = new Nucleo();
			if(nucleo){
				this.idNucleo = nucleo.id;
				this.cotacao.nucleo.id = this.idNucleo;			
			} else {
				this.listarComboPorIdEmpresa(this.cotacao.empresa.id);
			}
		}
	}

	id : number = 1;

	newProduto(isSelecionado : boolean, nome : string){
		let p = new Produto();

		p.id = this.id;
		p.selecionado = isSelecionado;
		p.nome = nome;
		this.id ++;
		return p;
	}

	chamarTela2(){
		if(!this.isEnviado){
			this.salvarProdutos();
		}
		this.numTela = 2;
	}
	
	chamarTela3(){
		this.salvarQuantidades();
	}

	popularFornecedoresSelecionados(){
		this.listaFornecedoresSelecionados = new Array<Fornecedor>();
		for (var i = this.listaFornecedoresAux.length - 1; i >= 0; i--) {
			let f = this.listaFornecedoresAux[i]
			if(f.selecionado) this.listaFornecedoresSelecionados.push(f);
		}
	}

	salvar(){
		this.popularFornecedoresSelecionados();
		this.doSalvar();
	}

	salvarEnviar(){
		this.popularFornecedoresSelecionados();
		this.doSalvarEnviar();
	}

	filtrarFornecedor(){
		this.doListarFornecedores();
	}

	filtrarProduto(){
		this.doListarProdutos();
	}

	limpar(){
		this.cotacao = new Cotacao();
	}

	voltar(){
		this._router.navigate(['/cotacao']);
	}

	voltarTela1(){
		this.numTela = 1;
	}

	voltarTela2(){
		this.numTela = 2;
	}

	selecionarProduto(prod: Produto){
		this.alterou = true;
		prod.selecionado = true;
		this.listaProdutosIdAux.push(prod.id);
		this.listaProdutosAux.push(prod);
	}

	desmarcarProduto(prod: Produto){
		this.alterou = true;
		prod.selecionado = false;
		let pos : number = this.listaProdutosIdAux.indexOf(prod.id);
		this.listaProdutosIdAux.splice(pos, 1);
		this.listaProdutosAux.splice(pos, 1);
	}

	selecionarFornecedor(fornec: Fornecedor){
		fornec.selecionado = true;
		this.listaFornecedoresIdAux.push(fornec.id);
		this.listaFornecedoresAux.push(fornec);
	}

	desmarcarFornecedor(fornec: Fornecedor){
		fornec.selecionado = false;
		let pos : number = this.listaProdutosIdAux.indexOf(fornec.id);
		this.listaFornecedoresIdAux.splice(pos, 1);
		this.listaFornecedoresAux.splice(pos, 1);
	}

	salvarEmail(fornecedor: Fornecedor){
		this._fornecedorService.atualizarPessoa(fornecedor.pessoa.id, fornecedor.pessoa.email)
		.subscribe((resultado: Response) => {
			if(!resultado.sucesso){
				this._mensagemService.showInfo(resultado.mensagem);
			}
			fornecedor.habilitaEmail = false;
		});
	}

	listarComboPorIdEmpresa(idEmpresa : number){
		if (idEmpresa > 0) {

			this._nucleoService.listarComboPorIdEmpresa(idEmpresa).subscribe((resultado) => {
				
				this.setListaComboNucleo(resultado)
				
			},(error) => {
				
				console.log(error)
			});

		} else {
			
			this.listaNucleo = [];
		}
	}

	private recuperarCotacao(id:number){
		this._cotacaoService.consultarId(id).subscribe((resultado) => {
			this.setCotacao(resultado)
		},(error) => {
			console.log(error)
		});
	}

	private setCotacao(resultado : Response){
		if(!resultado.sucesso){
			return;
		}

		this.cotacao =  <Cotacao>resultado.objetoRetorno;
		this.isEnviado = parseInt(EnumStatusCotacao.SALVO.codigo) != this.cotacao.status;
		this.cotacao.empresa = new Empresa();
		this.cotacao.empresa.id = this._loginService.getUser().colaborador.empresa.id;

		let nucleo = this._loginService.getUser().colaborador.nucleo;
		if(nucleo){
			this.cotacao.nucleo = new Nucleo();
			this.cotacao.nucleo.id = nucleo.id;			
		}
		this.idNucleo = this.cotacao.nucleo.id;
		this._cotacaoService.listarItensPorCotacao(this.cotacao.id).subscribe((resultado) => {
			this.setListaProdutosSelecionados(resultado)
		});
	}

	private setListaProdutosSelecionados(resultado: Response){
		let listaProdutosSel = resultado.objetoRetorno;
		this.listaProdutosAux = new Array<Produto>();
		this.listaProdutosSelecionados = new Array<ItemCotacao>();
		let listaIdsIC = [];

		let listaIdsP = this.listaProdutos.map(p => {
			return p.id;
		})

		for (var i = listaProdutosSel.length - 1; i >= 0; i--) {
			let ic = listaProdutosSel[i];
			let produto = ic.produto;
			if(listaIdsIC.indexOf(produto.id) < 0){
				let pos = listaIdsP.indexOf(produto.id);
				this.listaProdutos[pos].selecionado = true;
				this.listaProdutosSelecionados.push(ic);
				listaIdsIC.push(produto.id);
				let prod : Produto = Object.assign({}, produto);
				prod.selecionado = true;
				this.listaProdutosAux.push(prod);
			} 
		}

	}

	private salvarProdutos(){
		let lista = new Array<Produto>();
		if(this.idCotacao && this.idCotacao > 0 && !this.alterou){
			return;
		} else {
			lista = this.listaProdutosAux;
		}
		this.listaProdutosSelecionados = new Array<ItemCotacao>();
		for (var i = lista.length - 1; i >= 0; i--) {
			let p = lista[i];
			let ic = new ItemCotacao();
			if(p.selecionado){
				ic.produto = p;
				this.listaProdutosSelecionados.push(ic)
			}
		};
	}

	private salvarQuantidades(){
		let isValido : boolean = this.validarQuantidades();


		if(isValido){
			this.numTela = 3;
			this.doListarFornecedores();
		}else {
			this._mensagemService.showAtencao("Quantidade do produto tem que ser maior que 0!");
		}
	}

	private validarQuantidades() : boolean {
		let isValido : boolean = true;

		let ic : ItemCotacao;
		for (var i = this.listaProdutosSelecionados.length - 1; i >= 0; i--) {
			ic = this.listaProdutosSelecionados[i];
			if(!ic.quantidade || ic.quantidade <= 0){
				isValido = false;
				break;
			}
		}

		return isValido;
	}

	private setNucleo(){
		if(this.idNucleo){
			this.cotacao.nucleo.id = this.idNucleo;
		}
	}
	private doSalvar(){
		this.setNucleo();
		this._cotacaoService.salvar(this.cotacao, this.listaFornecedoresSelecionados, this.listaProdutosSelecionados).subscribe((resultado) => {
			this._mensagemService.showInfo("Cotação cadastrada com sucesso!");
			this.isEnviado = true;
			TimerWrapper.setTimeout(() => {  
				this.voltar();
			}, 2000);
		});
	}

	private doSalvarEnviar(){
		this.setNucleo();
		this._cotacaoService.salvarEnviar(this.cotacao, this.listaFornecedoresSelecionados, this.listaProdutosSelecionados).subscribe((resultado) => {
			this._mensagemService.showInfo("Cotação cadastrada com sucesso!");
			this.isEnviado = true;
			TimerWrapper.setTimeout(() => {  
				this.voltar();
			}, 2000);
		});
	}

	private doListarFornecedores(){
		if(!this.filtroFornecedor){
			this._fornecedorService.listarCombo().subscribe((resultado) => {
				this.setListaFornecedores(resultado)
			});
		}else {
			this._fornecedorService.listarFiltroCotacao(this.filtroFornecedor).subscribe((resultado) => {
				this.setListaFornecedores(resultado)
			});
		}
	}

	private doListarProdutos(){
		if(!this.filtroProduto){
			this.carregarProduto();
		}else {
			this._produtoService.listarFiltroCotacao(this.filtroProduto).subscribe((resultado) => {
				this.setListaProdutos(resultado)
			});
		}
	}

	private setListaFornecedores(resultado: Response){
		if(!resultado.sucesso){
			return;
		}

		// Marcar os checkbox's mesmo apos usar o filtro
		this.listaFornecedores = resultado.objetoRetorno;
		if(this.listaFornecedoresIdAux){
			this.listaFornecedores.forEach((p) => {
				if(this.listaFornecedoresIdAux.indexOf(p.id) >= 0){
					p.selecionado = true;
				}
			})
		}
		if(this.cotacao && this.cotacao.id){
			this._cotacaoService.listarCotacaoFornecedorPorCotacao(this.cotacao.id).subscribe((resultado) => {
				this.setListaFornecedoresSelecionados(resultado)
			});
		}
	}

	private setListaFornecedoresSelecionados(resultado: Response){
		let listaIdSelecionados = resultado.objetoRetorno;

		for (let i = listaIdSelecionados.length - 1; i >= 0; i--) {
			let cf = listaIdSelecionados[i];
			for (let j = this.listaFornecedores.length - 1; j >= 0; j--) {
				if(this.listaFornecedores[j].id === cf.fornecedor.id){
					this.listaFornecedores[j].selecionado = true;
				} 
			}
		}
	}

	private carregarProduto(){
		this._produtoService.listarCombo().subscribe((resultado) => {
			this.setListaProdutos(resultado)
		})
	}

	private setListaProdutos(resultado : Response ){
		//if(!this.listaProdutosAux){
			//	this.listaProdutosAux = resultado.objetoRetorno;
			//}

			this.listaProdutos = resultado.objetoRetorno;
			if(this.listaProdutosIdAux){
				this.listaProdutos.forEach((p) => {
					if(this.listaProdutosIdAux.indexOf(p.id) >= 0){
						p.selecionado = true;
					}
				})
			}
		}

		private setListaComboNucleo(resultado : Response){

			if(!resultado.sucesso){

				this._mensagemService.showAtencao("Falha de conexão, não foi possivel carregar combo!");
				return;
			}

			this.listaNucleo = [];
			this.listaNucleo.push({label: '-- Selecione --', value: 0})
			let listaRetorno : Array<Nucleo> = resultado.objetoRetorno;
			listaRetorno.forEach(nuc => this.listaNucleo.push({label: nuc.nome, value: nuc.id}))
		}

	}
