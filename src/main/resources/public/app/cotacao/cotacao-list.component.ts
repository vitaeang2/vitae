import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {DataTable, Column, LazyLoadEvent, SelectItem, Dropdown, Dialog} from 'primeng/primeng';
import {Cotacao} from "./cotacao";
import {CotacaoService} from "./cotacao.service"
import {MensagemService} from "../aplicacao/mensagem.service"
import {LoginService} from "../login/login.service"
import {PaginacaoParams} from '../model/paginacao_params';
import {Nucleo} from '../nucleo/nucleo';
import {Paginacao} from '../model/paginacao';
import {NucleoService} from '../nucleo/nucleo.service';
import {Response} from '../model/response';
import {VitaeLazy} from '../aplicacao/vitae-lazy.service';
import { DataValidator } from '../aplicacao/data-validator.directive';
import {CustomDatePickerComponent} from '../componentes/custom-datepicker.component';

@Component({
    selector: 'cotacao',
    templateUrl: './app/cotacao/cotacao-list.html',
    directives: [DataTable, Column, Dropdown, Dialog, CustomDatePickerComponent, DataValidator],
    providers: [CotacaoService, NucleoService]
})
export class CotacaoListComponent extends VitaeLazy implements OnInit {

    params : PaginacaoParams;
    paginacao : Paginacao = new Paginacao();
    listaNucleo : SelectItem[];
    idNucleoSelecionado : number;
    mostarFiltroNucleo :boolean = false;
    idNucleo : number = 0;

    dataValidadeInicio : string = "";
    dataValidadeFim : string = "";

    private idCotacaoSelecionada: number = 0;

    public displayDialogExcluir: boolean = false;

    constructor(private _nucleoService: NucleoService, private _cotacaoService : CotacaoService, private _loginService : LoginService, private _mensagemService : MensagemService, private router: Router){
        super(router);

    }

    ngOnInit(){
        let nucleo: Nucleo = this._loginService.getUser().colaborador.nucleo;
        this.dataValidadeInicio = "";
        this.dataValidadeFim = "";
        if(nucleo && nucleo.id){
            this.idNucleo = nucleo.id;
            this.mostarFiltroNucleo = false;
        }else{
            this.idNucleo = 0;
            this.mostarFiltroNucleo = true;
            this.carregarListaNucleo();
        }
        
        this.doListar();
    }

    pedido(id: number){
        super.editar(id, '/pedidocompra/selecionar');
    }

    novo() {
        super.novo('/cotacao/');
    }
    
    editar(id: number){
        super.editar(id, '/cotacao/');
    }

    doListar(){

        if(this.params){

            if(this.idNucleoSelecionado && this.idNucleoSelecionado != 0){
                this.listarPorNucleo(this.idNucleoSelecionado);
            }else{
                this.listarPorNucleo(this.idNucleo);
            }
        }
    }

    listarPorNucleo(idNucleo: number){
        this._cotacaoService
        .listar(this.params, this._loginService.getUser().colaborador.empresa.id, idNucleo, 0, this.dataValidadeInicio, this.dataValidadeFim)
        .subscribe((resultado) => {
            this.setListaCotacao(resultado)
        }, (error) => {
            console.log(error)
        });
    }

    confirmDiolgExcluir(id:number) {

        this.idCotacaoSelecionada = id;
        this.displayDialogExcluir = true;
    }

    excluir() { 

        this._cotacaoService.excluirId(this.idCotacaoSelecionada).subscribe((resultado) => {

            this._mensagemService.showInfo("Cotação excluída com sucesso!");
            this.displayDialogExcluir = false;
            this.doListar();
        });

    }

    private setListaCotacao(resultado : Response){
        if(!resultado.sucesso){
            this._mensagemService.showAtencao(resultado.mensagem);
            return;
        }
        this.paginacao = resultado.objetoRetorno;
    }

    private carregarListaNucleo(){
        this._nucleoService.listarComboPorIdEmpresa(this._loginService.getUser().colaborador.empresa.id).subscribe((resultado) => {
            this.setListaNucleo(resultado)
        });
    }

    private setListaNucleo(resultado : Response){
        if(!resultado.sucesso){
            this._mensagemService.showAtencao(resultado.mensagem);
            return;
        }
        this.listaNucleo = [];
        let lista : Array<Nucleo> = resultado.objetoRetorno;
        this.listaNucleo.push({label: "-- TODOS --", value: 0});
        lista.forEach(nuc => this.listaNucleo.push({label: nuc.nome, value: nuc.id}));
    }
}
