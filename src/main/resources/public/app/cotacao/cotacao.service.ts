import {Injectable} from '@angular/core';
import {Http, HTTP_PROVIDERS, Headers} from '@angular/http'
import {UrlService} from '../model/url.service';
import {HeadersService} from '../aplicacao/headers.service'
import {LoginService} from '../login/login.service';
import {Fornecedor} from '../fornecedor/fornecedor';
import {ItemCotacao} from '../cotacao/item_cotacao';
import {CotacaoFornecedor} from '../cotacao/cotacao_fornecedor';
import {PaginacaoParams} from '../model/paginacao_params';
import {Cotacao} from '../cotacao/cotacao';

@Injectable()
export class CotacaoService  extends UrlService {

	private urlNegocio : string = 'cotacao';

	constructor(_http: Http, _headerService: HeadersService, _loginService: LoginService) {
		super(_http, _headerService, _loginService);
	}

	public listar(params: PaginacaoParams, idEmpresa : number, idNucleo? : number, status? : number, dataInicio? : string, dataFim? : string) {
		return super.callHeader(`${this.urlNegocio}/listar?params=${JSON.stringify(params)}&idEmpresa=${idEmpresa}&idNucleo=${idNucleo}&status=${status}&dataInicio=${dataInicio}&dataFim=${dataFim}`);
	}
	
	public listarPaginadoCotacoesRespondidas(params: PaginacaoParams, idEmpresa : number, idNucleo? : number) {
		return super.callHeader(`${this.urlNegocio}/listarPaginadoCotacoesRespondidas?params=${JSON.stringify(params)}&idEmpresa=${idEmpresa}&idNucleo=${idNucleo}`);
	}

	public listarItensPorCotacao(idCotacao: number) {
		return super.callHeader(`${this.urlNegocio}/listarItensPorCotacao?idCotacao=${idCotacao}`);
	}
	
	public listarCotacaoFornecedorPorCotacao(idCotacao: number) {
		return super.callHeader(`${this.urlNegocio}/listarCotacaoFornecedorPorCotacao?idCotacao=${idCotacao}`);
	}

	public listarFornecedoresPorPedidoCompra(idCotacao: number) {
		return super.callHeader(`${this.urlNegocio}/listarFornecedoresPorPedidoCompra?idCotacao=${idCotacao}`);
	}

	public listarCombo() {
		return super.callHeader(`${this.urlNegocio}/listarCombo`);
	}

	public consultarId(id: number) {
		return super.callHeader(`${this.urlNegocio}/consultarId?id=${id}`);
	}

	public salvar(cotacao: Cotacao, listaFornecedoresSelecionados : Array<Fornecedor>, listaItensSelecionados : Array<ItemCotacao>) {
		return super.callHeaderBody(`${this.urlNegocio}/salvar?cotacao=${JSON.stringify(cotacao)}&fornecedores=${JSON.stringify(listaFornecedoresSelecionados)}`, JSON.stringify(listaItensSelecionados));
	}

	public salvarEnviar(cotacao: Cotacao, listaFornecedoresSelecionados : Array<Fornecedor>, listaItensSelecionados : Array<ItemCotacao>) {
		return super.callHeaderBody(`${this.urlNegocio}/salvarEnviar?cotacao=${JSON.stringify(cotacao)}&fornecedores=${JSON.stringify(listaFornecedoresSelecionados)}`, JSON.stringify(listaItensSelecionados));
	}

	public editar(cotacao: Cotacao) {
		return super.callHeader(`${this.urlNegocio}/salvar?cotacao=${JSON.stringify(cotacao)}`);
	}

	public listarItensPorCotacaoFornecedor(idCotacaoFornecedor: number) {
		return super.call(`${this.urlNegocio}/listarItensPorCotacaoFornecedor?idCotacaoFornecedor=${idCotacaoFornecedor}`);
	}

	public consultarCotacaoFornecedorPorToken(token: string) {
		return super.call(`${this.urlNegocio}/consultarCotacaoFornecedorPorToken?token=${token}`);
	}

	public salvarResposta(cotacao: Cotacao, listaItensSelecionados : Array<ItemCotacao>, cotacaoFornecedor: CotacaoFornecedor) {
		return super.callBody(`${this.urlNegocio}/salvarResposta?cotacao=${JSON.stringify(cotacao)}&cotacaoFornecedor=${JSON.stringify(cotacaoFornecedor)}`, JSON.stringify(listaItensSelecionados));
	}

	public excluirId(id: number) {

		return super.callHeader(`${this.urlNegocio}/excluirId?idCotacao=${id}`);
	}

	public enviarEmail(nome: string, email : string, token : string) {
		console.log(encodeURIComponent(`nome=${nome}&email=${email}&token=${token}`))
		return super.callHeader(`${this.urlNegocio}/enviarEmail?nome=${encodeURIComponent(nome)}&email=${email}&token=${encodeURIComponent(token)}`);
	}

}