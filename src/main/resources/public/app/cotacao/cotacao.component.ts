import {Component} from '@angular/core';
import {Routes, ROUTER_DIRECTIVES, Router} from '@angular/router';
import {CotacaoListComponent} from './cotacao-list.component';
import {CotacaoDetailComponent} from "./cotacao-detail.component";
import {PedidoCompraDetailComponent} from '../pedido_compra/pedido_compra-detail.component';

@Component({
	selector: 'cotacao',
	templateUrl: 'app/cotacao/cotacao.html',
	directives: [ROUTER_DIRECTIVES]
})
@Routes([
	{ path: "/", component: CotacaoListComponent},
	{ path: ":id", component: CotacaoDetailComponent},
	{ path: "/pedidocompra/:id", component: PedidoCompraDetailComponent},
	])
export class CotacaoComponent  {
	constructor(private _router : Router) {
		this._router.navigate(['/cotacao'])
	}
}
