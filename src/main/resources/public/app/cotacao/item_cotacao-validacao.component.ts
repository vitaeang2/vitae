import {Component, Provider, forwardRef, Input} from "@angular/core";
import {FormBuilder, FORM_DIRECTIVES, ControlGroup, Control, Validators} from "@angular/common";
import {MaskInputDirective} from "../componentes/mask.directive";
import {BaseInput} from "../componentes/baseinput.component";
import {ItemCotacao} from '../cotacao/item_cotacao';

export interface IValidationResult {
    [key:string]:boolean;
}

@Component({
    selector: 'item-validacao-input',
    template: `
    <form class="form-inline" role="form" [ngFormModel]="senseForm">
        <div class="form-group"
            [class.has-error]="!quantidadeCtl.valid">
            <input type="number"
            style="width:80px"
            class="form-control"
            [disabled]="disabled"
            [ngFormControl]="quantidadeCtl"
            [(ngModel)]="ic.quantidade">
        </div>
    </form>
    `,
    directives: [FORM_DIRECTIVES]
})
export class ItemComponentValidacao {

    @Input() ic: ItemCotacao;
    @Input() disabled: boolean;
    public senseForm: ControlGroup;
    public quantidadeCtl: Control;

    constructor(private formBuilder: FormBuilder) {
        this.senseForm = this.formBuilder.group({
            "quantidade": [0, Validators.compose([Validators.required, ItemComponentValidacao.isValid])]
        });
        this.quantidadeCtl = <Control> this.senseForm.controls["quantidade"];
    }

    static isValid(control: Control): IValidationResult {
        let n = Number(control.value);
        if (isNaN(n) || n <= 0) {
            return {
                "isValidYear": true
            };
        }


        return null;
    }

}