import {CotacaoFornecedor} from './cotacao_fornecedor';
import {Produto} from '../produto/produto';

export class ItemCotacao {

	id : number;
	vlrUnitario : number;
	quantidade : number;
	cotacaoFornecedor : CotacaoFornecedor;
	produto : Produto;
	vlrTotal : number;
	
}