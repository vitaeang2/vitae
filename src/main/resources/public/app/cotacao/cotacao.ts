import {Empresa} from '../empresa/empresa'
import {Nucleo} from '../nucleo/nucleo'

export class Cotacao {
	
	id : number;
	descricao : string;
	dataCotacao : string;
	dataValidade : string;
	status : number;
	empresa : Empresa;
	nucleo : Nucleo;
	
}