import {Component} from '@angular/core';
import {OnActivate, RouteSegment, Router} from '@angular/router';
import {SelectItem} from 'primeng/primeng';
import {CustomInput} from '../componentes/custom-input.component';
import {MensagemService} from '../aplicacao/mensagem.service';
import {Cotacao} from '../cotacao/cotacao';
import {CotacaoFornecedor} from '../cotacao/cotacao_fornecedor';
import {ItemCotacao} from './item_cotacao';
import {CotacaoService} from './cotacao.service';
import {Produto} from '../produto/produto';
import {ProdutoService} from '../produto/produto.service';
import {Fornecedor} from '../fornecedor/fornecedor';
import {FornecedorService} from '../fornecedor/fornecedor.service';
import {Response} from '../model/response';
import {MeuPanel} from '../aplicacao/panel';
import {EnumUnidadeMedida} from '../enum/enum-unidade-medida';

@Component({
	selector: 'fornecedor-cotacao',
	templateUrl: './app/cotacao/fornecedor_cotacao.html',
	providers: [CotacaoService, FornecedorService],
	directives: [MeuPanel, CustomInput],
	styleUrls: ['./app/cotacao/table.css']
})
export class FornecedorCotacaoComponent {

	isSalvo : boolean = false;

	vlrTotal : number = 0.0;

	tokenValido : boolean = false;

	token : string;

	cotacaoFornecedor : CotacaoFornecedor;

	listaItens : Array<ItemCotacao>;

	constructor(private _cotacaoService : CotacaoService, private _mensagemService: MensagemService){
	}

	validar(){
		this.doValidar();
	}

	getUnidadeMedidaStr(idStr) : string {
		if(idStr){
			return EnumUnidadeMedida.getByCod(idStr).descricao;
		}

		return 'VAZIO';
	} 

	salvarResposta(){
		this.doSalvarResposta();
	}

	private doSalvarResposta(){
		this._cotacaoService.salvarResposta(this.cotacaoFornecedor.cotacao, this.listaItens, this.cotacaoFornecedor).subscribe((resultado) => {
			this.setCotacao(resultado)
		})
	}

	private setCotacao(resultado: Response){
		this._mensagemService.showInfo("Cotação enviada com sucesso!");
		this.isSalvo = true;
	}

	private doValidar(){
		this._cotacaoService.consultarCotacaoFornecedorPorToken(this.token).subscribe((resultado) => {
			this.setCotacaoFornecedor(resultado)
		});
	}

	private setCotacaoFornecedor(resultado : Response){
		if(!resultado.sucesso){
			
			this.tokenValido = false;
			this._mensagemService.showInfo("Token inválido!");
			return;
		}
		this.cotacaoFornecedor = resultado.objetoRetorno;
		this.tokenValido = true;
		this.doListaCotacaoFornecedor();
	}

	private doListaCotacaoFornecedor(){
		this._cotacaoService.listarItensPorCotacaoFornecedor(this.cotacaoFornecedor.id).subscribe((resultado) => {
			this.setListaCotacaoFornecedor(resultado)
		})
	}

	private setListaCotacaoFornecedor(resultado : Response){
		this.listaItens = resultado.objetoRetorno.map(ic => {
			if(ic.produto.unidadeMedida){
				ic.produto.unidadeMedidaStr = EnumUnidadeMedida.getByCod(ic.produto.unidadeMedida.toString()).descricao;
			}
			return ic;
		});
	}

	calcular(ic :ItemCotacao){
		ic.vlrTotal = (ic.vlrUnitario * ic.quantidade)
		this.calcularVlrTotal();
	}


	private cont : number = 0;

	calcularVlrTotal(){
		console.log(this.cont++);
		this.vlrTotal = 0;
		this.listaItens.forEach((ic) => {
			this.vlrTotal+= ic.quantidade * ic.vlrUnitario;
		})
		this.vlrTotal += this.cotacaoFornecedor.vlrFrete;
		this.vlrTotal -= this.cotacaoFornecedor.vlrDesconto;
	}

}