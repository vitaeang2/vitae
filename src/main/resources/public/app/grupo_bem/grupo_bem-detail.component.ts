import {Component} from '@angular/core';
import {OnActivate, RouteSegment, Router} from '@angular/router';
import {SelectItem} from 'primeng/primeng';
import {GrupoBemService} from './grupo_bem.service';
import {GrupoBem} from '../grupo_bem/grupo_bem';
import {CustomInput} from '../componentes/custom-input.component';
import {MensagemService} from '../aplicacao/mensagem.service';
import {Response} from '../model/response';
import {MeuPanel} from '../aplicacao/panel';
import {EnumEstadosBrasileiros} from '../enum/enum-estados-brasileiros';

@Component({
	selector: 'grupo_bem-detail',
	templateUrl: './app/grupo_bem/grupo_bem-detail.html',
	providers: [GrupoBemService],
	directives: [MeuPanel, CustomInput]
})
export class GrupoBemDetailComponent implements OnActivate {

	grupo_bem : GrupoBem = new GrupoBem();

	constructor(private _grupo_bemService: GrupoBemService, private _mensagemService : MensagemService, private _router: Router){
	}

	routerOnActivate(curr: RouteSegment) {
		let id = <number> +curr.getParam('id');

		if(id != -1){
			this.recuperarGrupoBem(id);
		}else{
			this.limpar();
		}
	}

	salvar(){

		this.doSalvar();

	}

	limpar(){
		this.grupo_bem = new GrupoBem();
	}

	voltar(){
		this._router.navigate(['/grupo_bem']);
	}

	private recuperarGrupoBem(id:number){
		this._grupo_bemService.consultarId(id).subscribe((resultado) => {
			this.setGrupoBem(resultado)
		},(error) => {
			console.log(error)
		});
	}

	private setGrupoBem(resultado : Response){
		this.grupo_bem =  <GrupoBem>resultado.objetoRetorno;
	}

	private doSalvar(){
		this._grupo_bemService.salvar(this.grupo_bem).subscribe((resultado) => {

			if(resultado.sucesso){
				this._mensagemService.showInfo("Grupo Bem cadastrado com sucesso!");
				this.voltar();				
			} else {
				this._mensagemService.showInfo(resultado.mensagem);
			}
		});
	}
}
