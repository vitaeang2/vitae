export class GrupoBem {

	id: number;

	nome : string;
	
	observacao : string;

	ativo: boolean;

	depreciacaoAnual: number;	

}
