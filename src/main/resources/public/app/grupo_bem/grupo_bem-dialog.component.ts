import {Component, OnInit} from '@angular/core';
import {GrupoBem} from './grupo_bem';
import {GrupoBemService} from './grupo_bem.service';
import {MeuPanel} from '../aplicacao/panel';
import {CustomInput} from '../componentes/custom-input.component';
import {MensagemService} from '../aplicacao/mensagem.service';
import {Response} from '../model/response';

import {TimerWrapper} from '@angular/core/src/facade/async';

@Component({
	selector: 'grupo_bem-dialog',
	templateUrl: './app/grupo_bem/grupo_bem-dialog.html',
	directives: [MeuPanel, CustomInput],
	providers: [GrupoBemService]
})
export class GrupoBemDialogComponent implements OnInit{
	
	grupo_bem : GrupoBem; 

	isSucesso: boolean;

	isResposta: boolean;

	mensagem: string;

	constructor(private _grupo_bemServico : GrupoBemService, private _mensagemService : MensagemService){}

	ngOnInit() {
		this.grupo_bem = new GrupoBem();
	}

	salvar(){
		this._mensagemService.showInfo("Grupo Bem salvo com sucesso! " + this.grupo_bem.nome);
		this.doSalvar();
	}

	limpar(){
		this.grupo_bem = new GrupoBem();
	}

	private doSalvar() {
		this._grupo_bemServico.salvar(this.grupo_bem).subscribe((resultado) => {
			
			if(resultado.sucesso){
				this._mensagemService.showInfo("Grupo Bem cadastrado com sucesso!");
				this.setGrupoBem(resultado);
			} else {
				this._mensagemService.showInfo(resultado.mensagem);
			}
		});
	}

	private setGrupoBem(resultado : Response){
		this.isResposta = true;
		if(!resultado.sucesso){
			this.mensagem = resultado.mensagem;
			return;
		}

		this.isSucesso = true;
		this.mensagem = "Grupo Bem salvo com sucesso!";

		this._mensagemService.showInfo("Grupo Bem salvo com sucesso!");

		TimerWrapper.setTimeout(() => {  
			this.isResposta = false;
			this.isSucesso = false;
			this.mensagem = "";
			this.grupo_bem = new GrupoBem();
		}, 2000);
	}
}