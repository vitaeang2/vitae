import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {DataTable, Column, LazyLoadEvent, Button,Dialog} from 'primeng/primeng';
import {MaterialDesignDirective} from "../aplicacao/material";
import {GrupoBem} from "./grupo_bem";
import {GrupoBemService} from "./grupo_bem.service"
import {MensagemService} from "../aplicacao/mensagem.service"
import {LoginService} from "../login/login.service"
import {PaginacaoParams} from '../model/paginacao_params';
import {Paginacao} from '../model/paginacao';
import {Response} from '../model/response';

@Component({
    selector: 'grupo_bem-list',
    templateUrl: './app/grupo_bem/grupo_bem-list.html',
    directives: [DataTable, Column, Button, Dialog],
    providers: [GrupoBemService]
})
export class GrupoBemListComponent implements OnInit {

    params: PaginacaoParams;
    paginacao: Paginacao = new Paginacao();

    private itemSelecionadoId: number = 0;

    public displayDialogExcluir: boolean = false;

    public displayDialogExcluirGrupoBemViculoPatrimonio: boolean = false;

    constructor(private _grupo_bemService: GrupoBemService, private _loginService: LoginService, private _mensagemService: MensagemService, private _router: Router) {
    }

    ngOnInit() {
        this.doListar();
    }

    novo() {
        this._router.navigate(['/grupo_bem/', -1])
    }

    editar(id: number) {
        this._router.navigate(['/grupo_bem/', id])
    }

    load(event: LazyLoadEvent) {

        this.params = new PaginacaoParams();

        //Calcula a página atual
        let page = 0;
        if (event.first > 0) {
            page = event.first / event.rows;
        }

        this.params.page = page;
        this.params.size = event.rows;
        this.params.sortField = event.sortField;
        this.params.sortOrder = (event.sortOrder == -1) ? 'desc' : 'asc';

        this.doListar();
    }

    private doListar() {
        if (this.params) {
            this._grupo_bemService.listar(this.params).subscribe((resultado) => {
                this.setListaGrupoBem(resultado)
            });
        }
    }

    private setListaGrupoBem(resultado: Response) {
        if (!resultado.sucesso) {
            this._mensagemService.showAtencao(resultado.mensagem);
            return;
        }
        this.paginacao = resultado.objetoRetorno;
    }

    confirmDiolgExcluir(id: number) {

        this.itemSelecionadoId = id;
        this.displayDialogExcluir = true;
    }

    excluir(verificarGrupoBemViculadaPatrimonio: boolean) {

        this._grupo_bemService.excluirId(this.itemSelecionadoId, verificarGrupoBemViculadaPatrimonio).subscribe((resultado: Response) => {

            if (!resultado.sucesso) {
                this.displayDialogExcluirGrupoBemViculoPatrimonio = true;
            } else {
                this._mensagemService.showInfo("Grupo Bem excluído com sucesso!");
                this.displayDialogExcluirGrupoBemViculoPatrimonio = false;
                this.doListar();
            }
            this.displayDialogExcluir = false;              
        });

    }
}
