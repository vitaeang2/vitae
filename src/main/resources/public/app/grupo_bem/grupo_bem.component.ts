import {Component} from '@angular/core';
import {Routes, ROUTER_DIRECTIVES, Router} from '@angular/router';
import {GrupoBemListComponent} from './grupo_bem-list.component';
import {GrupoBemDetailComponent} from "./grupo_bem-detail.component";

@Component({
	selector: 'grupo_bem',
	templateUrl: 'app/grupo_bem/grupo_bem.html',
	directives: [ROUTER_DIRECTIVES]
})
@Routes([
	{ path: "/", component: GrupoBemListComponent},
	{ path: ":id", component: GrupoBemDetailComponent}
	])

export class GrupoBemComponent  {
	constructor(private _router : Router) {
		this._router.navigate(['/grupo_bem'])
	}
}
