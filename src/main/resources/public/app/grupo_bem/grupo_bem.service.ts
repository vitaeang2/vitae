import {Injectable} from '@angular/core';
import {Http, HTTP_PROVIDERS, Headers} from '@angular/http'
import {UrlService} from '../model/url.service';
import {HeadersService} from '../aplicacao/headers.service'
import {LoginService} from '../login/login.service';
import {PaginacaoParams} from '../model/paginacao_params';
import {GrupoBem} from '../grupo_bem/grupo_bem';

@Injectable()
export class GrupoBemService  extends UrlService {

private urlNegocio : string = 'grupo_bem'

	constructor(_http: Http, _headerService: HeadersService, _loginService: LoginService) {
		super(_http, _headerService, _loginService);
	}

	public listar(params: PaginacaoParams) {
		return super.callHeader(`${this.urlNegocio}/listar?params=${JSON.stringify(params)}`);
	}
	
	public listarCombo() {
		return super.callHeader(`${this.urlNegocio}/listarCombo`);
	}

	public consultarId(id: number) {
		return super.callHeader(`${this.urlNegocio}/consultarId?id=${id}`);
	}

	public salvar(grupoBem: GrupoBem) {
		return super.callHeaderBody(`${this.urlNegocio}/salvar`, JSON.stringify(grupoBem));
	}

	public excluirId(id: number, verificarGrupoBemViculadaPatrimonio: boolean) {

		return super.callHeader(`grupo_bem/excluirId?id=${id}&verificarGrupoBemViculadaPatrimonio=${verificarGrupoBemViculadaPatrimonio}`);
	}	
}