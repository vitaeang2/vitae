import {Component, OnInit} from '@angular/core';
import {Fabricante} from './fabricante';
import {FabricanteService} from './fabricante.service';
import {MeuPanel} from '../aplicacao/panel';
import {MensagemService} from '../aplicacao/mensagem.service';
import {Response} from '../model/response';

import {TimerWrapper} from '@angular/core/src/facade/async';

@Component({
	selector: 'fabricante-dialog',
	templateUrl: './app/fabricante/fabricante-dialog.html',
	directives: [MeuPanel],
	providers: [FabricanteService]
})
export class FabricanteDialogComponent implements OnInit{
	
	fabricante : Fabricante; 

	isSucesso: boolean;

	isResposta: boolean;

	mensagem: string;

	constructor(private _fabricanteServico : FabricanteService, private _mensagemService : MensagemService){}

	ngOnInit() {
		this.fabricante = new Fabricante();
	}

	salvar(){
		this._mensagemService.showInfo("Fabricante salvo com sucesso! " + this.fabricante.nome);
		this.doSalvar();
	}

	limpar(){
		this.fabricante = new Fabricante();
	}

	private doSalvar() {
		this._fabricanteServico.salvar(this.fabricante).subscribe((resultado) => {
			this.setFabricante(resultado);
		});
	}

	private setFabricante(resultado : Response){
		this.isResposta = true;
		if(!resultado.sucesso){
			this.mensagem = resultado.mensagem;
			return;
		}

		this.isSucesso = true;
		this.mensagem = "Fabricante salvo com sucesso!";

		this._mensagemService.showInfo("Fabricante salvo com sucesso!");

		TimerWrapper.setTimeout(() => {  
			this.isResposta = false;
			this.isSucesso = false;
			this.mensagem = "";
			this.fabricante = new Fabricante();
		}, 2000);
	}
}