import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {DataTable, Column, LazyLoadEvent, Button,Dialog} from 'primeng/primeng';
import {MaterialDesignDirective} from "../aplicacao/material";
import {Fabricante} from "./fabricante";
import {FabricanteService} from "./fabricante.service"
import {MensagemService} from "../aplicacao/mensagem.service"
import {LoginService} from "../login/login.service"
import {PaginacaoParams} from '../model/paginacao_params';
import {Paginacao} from '../model/paginacao';
import {Response} from '../model/response';
import {VitaeLazy} from '../aplicacao/vitae-lazy.service';

@Component({
    selector: 'fabricante-list',
    templateUrl: './app/fabricante/fabricante-list.html',
    directives: [DataTable, Column, Button, Dialog],
    providers: [FabricanteService]
})
export class FabricanteListComponent extends VitaeLazy implements OnInit  {

    params: PaginacaoParams;

    paginacao: Paginacao = new Paginacao();

    private itemSelecionadoId: number = 0;

    public displayDialogExcluir: boolean = false;

    public displayDialogExcluirFabricanteViculoProduto: boolean = false;

    constructor(private _fabricanteService: FabricanteService, private _loginService: LoginService, 
        private _mensagemService: MensagemService, private router: Router) {
        super(router);
    }

    ngOnInit() {
        this.doListar();
    }

    novo() {
        super.novo('/fabricante/');
    }

    editar(id: number) {
        super.editar(id, '/fabricante/');
    }

    doListar() {
        if (this.params) {
            this._fabricanteService.listar(this.params).subscribe((resultado) => {
                this.setListaFabricante(resultado)
            });
        }
    }

    private setListaFabricante(resultado: Response) {
        if (!resultado.sucesso) {
            this._mensagemService.showAtencao(resultado.mensagem);
            return;
        }
        this.paginacao = resultado.objetoRetorno;
    }

    confirmDiolgExcluir(id: number) {

        this.itemSelecionadoId = id;
        this.displayDialogExcluir = true;
    }

    excluir(verificarFabricanteViculadaProduto: boolean) {

        this._fabricanteService.excluirId(this.itemSelecionadoId, verificarFabricanteViculadaProduto).subscribe((resultado: Response) => {

            if (!resultado.sucesso) {
                this.displayDialogExcluirFabricanteViculoProduto = true;
            } else {
                this._mensagemService.showInfo("Fabricante de Produto excluído com sucesso!");
                this.displayDialogExcluirFabricanteViculoProduto = false;
                this.doListar();
            }
            this.displayDialogExcluir = false;              
        });

    }
}
