import {Component} from '@angular/core';
import {Routes, ROUTER_DIRECTIVES, Router} from '@angular/router';
import {FabricanteListComponent} from './fabricante-list.component';
import {FabricanteDetailComponent} from "./fabricante-detail.component";

@Component({
	selector: 'fabricante',
	templateUrl: 'app/fabricante/fabricante.html',
	directives: [ROUTER_DIRECTIVES]
})
@Routes([
	{ path: "/", component: FabricanteListComponent},
	{ path: ":id", component: FabricanteDetailComponent}
	])

export class FabricanteComponent  {
	constructor(private _router : Router) {
		this._router.navigate(['/fabricante'])
	}
}
