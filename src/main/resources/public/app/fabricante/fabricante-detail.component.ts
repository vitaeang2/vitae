import {Component} from '@angular/core';
import {OnActivate, RouteSegment, Router} from '@angular/router';
import {SelectItem} from 'primeng/primeng';
import {FabricanteService} from './fabricante.service'
import {Fabricante} from '../fabricante/fabricante'
import {MensagemService} from '../aplicacao/mensagem.service';
import {Response} from '../model/response'
import {MeuPanel} from '../aplicacao/panel';
import {EnumEstadosBrasileiros} from '../enum/enum-estados-brasileiros';

@Component({
	selector: 'fabricante-detail',
	templateUrl: './app/fabricante/fabricante-detail.html',
	providers: [FabricanteService],
	directives: [MeuPanel]
})
export class FabricanteDetailComponent implements OnActivate {

	fabricante : Fabricante = new Fabricante();

	constructor(private _fabricanteService: FabricanteService, private _mensagemService : MensagemService, private _router: Router){
	}

	routerOnActivate(curr: RouteSegment) {
		let id = <number> +curr.getParam('id');

		if(id != -1){
			this.recuperarFabricante(id);
		}else{
			this.limpar();
		}
	}

	salvar(){

		this.doSalvar();

	}

	limpar(){
		this.fabricante = new Fabricante();
	}

	voltar(){
		this._router.navigate(['/fabricante']);
	}

	private recuperarFabricante(id:number){
		this._fabricanteService.consultarId(id).subscribe((resultado) => {
			this.setFabricante(resultado)
		},(error) => {
			console.log(error)
		});
	}

	private setFabricante(resultado : Response){
		this.fabricante =  <Fabricante>resultado.objetoRetorno;
	}

	private doSalvar(){
		this._fabricanteService.salvar(this.fabricante).subscribe((resultado) => {
			this._mensagemService.showInfo("Fabricante de Produto cadastrado com sucesso!");
			this.voltar();
		});
	}
}
