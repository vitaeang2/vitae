import { Directive, forwardRef, Attribute } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS } from '@angular/common';

declare var moment: any;

@Directive({
    selector: '[dataValidator][formControlName],[dataValidator][formControl],[dataValidator][ngModel]',
    providers: [
    { provide: NG_VALIDATORS, useExisting: forwardRef(() => DataValidator), multi: true }
    ]
})
export class DataValidator implements Validator {

    constructor(@Attribute('dataValidator') public dataComparar: string) {}

    validate(c: AbstractControl): { [key: string]: any } {
        let v = c.value;

        if(v){

            let data = new moment(c.value, "DD/MM/YYYY");
            let data2;
            if(this.dataComparar){
                if(this.dataComparar == "dataAdmissao"){
                    data2 = new moment(c.root.value.dataAdmissao, "DD/MM/YYYY");
                }
            }else {
                data2 = new Date();
                data2.setHours(0,0,0,0);
            }

            if( data < data2){
                return {
                    dataValidator: false
                }
            }
            return null;
        }
    }
}