import {Directive, ElementRef, Renderer, Input} from '@angular/core';
@Directive({selector: '.md-input', host: {
  '(focus)': 'setInputFocus(true)',
  '(blur)': 'setInputFocus(false)',
}})
export class MaterialDesignDirective {

  renderer: Renderer;
  el: ElementRef;

  constructor(el: ElementRef, renderer: Renderer) {
    this.renderer = renderer;
    this.el = el;
  }

  setInputFocus(isSet: boolean) {
    this.renderer.setElementClass(this.el.nativeElement.parentElement, 'md-input-focused', isSet);
  }
}