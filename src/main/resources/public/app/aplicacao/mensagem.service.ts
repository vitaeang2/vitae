import {Injectable} from '@angular/core'
import {Message} from 'primeng/primeng';

@Injectable()
export class MensagemService {
	
	msgs : Message[] = [];

	showInfo(texto : string) {
		this.msgs = [];
        this.msgs.push({severity:'info', summary:'Informação', detail: texto});
	}

	showAtencao(texto : string) {
		this.msgs = [];
        this.msgs.push({severity:'warn', summary:'Atenção', detail: texto});
	}

	showErro(texto : string) {
        this.msgs = [];
        this.msgs.push({severity:'error', summary:'Atenção', detail: texto});
    }
}