import {Component, EventEmitter, Output, Input} from '@angular/core' 

@Component({ 
	selector: 'meu-panel',
	templateUrl: './app/aplicacao/panel.html'
})
export class MeuPanel {

	@Input() titulo : string;
	@Input() habilita : boolean;
	@Input() temId : boolean;
	@Input() mostrarVoltar : boolean = true;
	@Input() col : string;
	@Output() clickSave = new EventEmitter();
	@Output() clickClear = new EventEmitter();
	@Output() clickBack = new EventEmitter();

	constructor() {
	}

	getCol(){
		if(this.col){
			return `col-md-${this.col}`;
		}
		return `col-md-12`;
	}

	salvar(){
		this.clickSave.emit(null);
	}

	limpar(){
		this.clickClear.emit(null);
	}

	voltar(){
		this.clickBack.emit(null);
	}

}