import { Directive, forwardRef, Attribute } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS } from '@angular/common';
@Directive({
    selector: '[validateNotEqual][formControlName],[validateNotEqual][formControl],[validateNotEqual][ngModel]',
    providers: [
    { provide: NG_VALIDATORS, useExisting: forwardRef(() => NotEqualValidator), multi: true }
    ]
})
export class NotEqualValidator implements Validator {
    constructor( @Attribute('validateNotEqual') public validateNotEqual: string) {}

    validate(c: AbstractControl): { [key: string]: any } {
        // self value (e.g. retype password)
        let v = c.value;

        console.log(this.validateNotEqual);

        if(!c.value || c.value == 0) {
            return {
                validateNotEqual: false
            }
        }

        return null
    }
}