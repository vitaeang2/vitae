import {Injectable} from '@angular/core';
import {Http, HTTP_PROVIDERS, Headers} from '@angular/http';
import {UrlService} from '../model/url.service';
import {HeadersService} from '../aplicacao/headers.service'
import {LoginService} from '../login/login.service';

@Injectable()
export class DashboardService  extends UrlService {

	private urlNegocio : string = 'dashboard';

	constructor(_http: Http, _headerService: HeadersService, _loginService: LoginService) {
		super(_http, _headerService, _loginService);
	}

	public quantidadeProdutoBaixoEstoque(idEmpresa : number, idNucleo? : number) {
		return super.callHeader(`${this.urlNegocio}/quantidadeProdutoBaixoEstoque?idEmpresa=${idEmpresa}&idNucleo=${idNucleo}&status=${status}`);
	}

	public quantidadeCotacoesRespondidas(idEmpresa : number, idNucleo? : number) {
		return super.callHeader(`${this.urlNegocio}/quantidadeCotacoesRespondidas?idEmpresa=${idEmpresa}&idNucleo=${idNucleo}`);
	}
	
	public quantidadeSolicitacoesAbertas(idEmpresa : number, idNucleo? : number) {
		return super.callHeader(`${this.urlNegocio}/quantidadeSolicitacoesAbertas?idEmpresa=${idEmpresa}&idNucleo=${idNucleo}`);
	}
}