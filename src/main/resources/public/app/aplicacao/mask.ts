import {Directive, ElementRef, AfterViewInit, Input} from '@angular/core'

@Directive({
	selector: 'mask'
})
export class MaskDirective implements AfterViewInit {

	@Input() set mask(newValue: string) {
		(this.el.nativeElement).mask(newValue);
	}

	constructor(public el: ElementRef) {};

	ngAfterViewInit(){

	}
}