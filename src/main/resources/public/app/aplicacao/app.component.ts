import {Component, OnInit} from '@angular/core';
import {Routes, ROUTER_DIRECTIVES, Router} from '@angular/router';
import {Growl, Dialog} from 'primeng/primeng';

import {UrlService} from '../model/url.service';
import {MenuService} from '../menu/menu.service';
import {Menu} from '../menu/menu';
import {MenuPipe} from '../menu/menu-pipe';
import {MenuLateralComponent} from '../menu/menu-lateral.component';
import {LoginComponent} from '../login/login.component';
import {LoginService} from '../login/login.service';
import {DashboardComponent} from '../aplicacao/dashboard.component';
import {EmpresaComponent} from '../empresa/empresa.component';
import {NucleoComponent} from '../nucleo/nucleo.component';
import {ColaboradorComponent} from '../colaborador/colaborador.component';
import {CargoComponent} from '../cargo/cargo.component';
import {FornecedorComponent} from '../fornecedor/fornecedor.component';
import {PerfilAcessoComponent} from '../perfil_acesso/perfil_acesso.component';
import {AlterarSenhaComponent} from '../usuario/alterar-senha.component';
import {ProdutoComponent} from '../produto/produto.component';
import {CotacaoComponent} from '../cotacao/cotacao.component';
import {CotacaoListRespondidasComponent} from '../cotacao/cotacao-respondidas-list.component';
import {PedidoCompraComponent} from '../pedido_compra/pedido_compra.component';
import {FornecedorCotacaoComponent} from '../cotacao/fornecedor_cotacao.component';
import {MovimentoEstoqueComponent} from '../movimento_estoque/movimento_estoque.component';
import {CategoriaProdutoComponent} from '../categoria_produto/categoria_produto.component';
import {FabricanteComponent} from '../fabricante/fabricante.component';
import {ProdutoEstoqueBaixoListComponent} from '../produto_estoque_baixo/produto-estoque-baixo-list.component';
import {DepartamentoComponent} from '../departamento/departamento.component';
import {GrupoBemComponent} from '../grupo_bem/grupo_bem.component';
import {PatrimonioComponent} from '../patrimonio/patrimonio.component';
import {ManutencaoPatrimonioComponent} from '../manutencao_patrimonio/manutencao_patrimonio.component';
import {ComponenteItemPatrimonioComponent} from '../componente_item_patrimonio/componente_item_patrimonio.component';
import {SolicitacaoComponent} from '../solicitacao/solicitacao.component';
import {SolicitacaoListEmAbertoComponent} from '../solicitacao/solicitacao-list-em-aberto.component';
import {ControleSolicitacaoComponent} from '../controle_solicitacao/controle_solicitacao.component';
import {QualidadeComponent} from '../qualidade/qualidade.component';
import {LocalComponent} from '../local/local.component';
import {CategoriaGrupoComponent} from '../categoria_grupo/categoria_grupo.component';
import {GrupoComponent} from '../grupo/grupo.component';
import {MensagemService} from '../aplicacao/mensagem.service';

@Component({
    selector: 'my-app',
    directives: [ROUTER_DIRECTIVES, MenuLateralComponent, LoginComponent, Growl],
    templateUrl: 'app/aplicacao/appComponent.html',
    pipes: [MenuPipe]
})

@Routes([
    { path: "/", component: LoginComponent },
    { path: "/login", component: LoginComponent },
    { path: "/dashboard",  component: DashboardComponent },
    { path: "/perfil", component: PerfilAcessoComponent },
    { path: "/empresa", component: EmpresaComponent},
    { path: "/nucleo", component: NucleoComponent},
    { path: "/cargo", component: CargoComponent},
    { path: "/colaborador", component: ColaboradorComponent},
    { path: "/fornecedor", component: FornecedorComponent},
    { path: "/alterarsenha", component: AlterarSenhaComponent},
    { path: "/produto", component: ProdutoComponent},
    { path: "/cotacao", component: CotacaoComponent},
    { path: "/estoque", component: MovimentoEstoqueComponent},
    { path: "/pedidocompra", component: PedidoCompraComponent},
    { path: "/fornecedorcotacao", component: FornecedorCotacaoComponent},
    { path: "/categoriaProduto", component: CategoriaProdutoComponent},
    { path: "/fabricante", component: FabricanteComponent},
    { path: "/produto-estoque-baixo", component: ProdutoEstoqueBaixoListComponent},
    { path: "/cotacao-respondidas", component: CotacaoListRespondidasComponent},
    { path: "/departamento", component: DepartamentoComponent},
    { path: "/grupo_bem", component: GrupoBemComponent},
    { path: "/patrimonio", component: PatrimonioComponent},
    { path: "/manutencao_patrimonio", component: ManutencaoPatrimonioComponent},
    { path: "/componente_item_patrimonio", component: ComponenteItemPatrimonioComponent},
    { path: "/solicitacao", component: SolicitacaoComponent},
    { path: "/solicitacao-list-em-aberto", component: SolicitacaoListEmAbertoComponent},
    { path: "/controle_solicitacao", component: ControleSolicitacaoComponent},
    { path: "/qualidade", component: QualidadeComponent},
    { path: "/local", component: LocalComponent},
    { path: "/categoria_grupo", component: CategoriaGrupoComponent},
    { path: "/grupo", component: GrupoComponent}
    ])

export class AppComponent implements OnInit {

    constructor(private _menuService: MenuService, private _loginService: LoginService, private _router: Router, 
        private _mensagemService : MensagemService) {
    	
    	let url = window.location.href;
    	
    	UrlService.url = window.location.href;
    	
    	if(url && url.includes("#/fornecedorcotacao")){
    		UrlService.url = url.substring(0, url.indexOf("#/fornecedorcotacao"));
    	} else {
            if(url && url.includes("#")){
                UrlService.url = url.substring(0, url.indexOf("#"));
            }else {
    		    UrlService.url = url;
            }
    	}
    }

    ngOnInit() {

        this._router.navigate(['/']);
    }

    isLogado(){

        return this._loginService.isLogged();
    }

}
