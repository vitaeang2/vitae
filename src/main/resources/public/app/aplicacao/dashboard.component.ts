import {Component, OnInit} from '@angular/core';
//import {MATERIAL_DIRECTIVES, MATERIAL_PROVIDERS} from "ng2-material/all";
import {BarChart} from 'primeng/primeng';
import {Usuario} from '../usuario/usuario';
import {Nucleo} from '../nucleo/nucleo';
import {UsuarioService} from '../usuario/usuario.service'
import {LoginService} from '../login/login.service';
import {DashboardService} from './dashboard.service';
import {Response} from '../model/response';
import {Routes, ROUTER_DIRECTIVES, Router} from '@angular/router';

@Component({
    selector: 'dashboard',
    directives: [ROUTER_DIRECTIVES, BarChart],
    providers: [DashboardService],
    templateUrl: 'app/aplicacao/dashboard.html'
})
export class DashboardComponent implements OnInit {
    data: any;
    quantidadeProdutoEstoqueBaixo: number;
    quantidadeCotacoesRespondidas: number;
    quantidadeSolicitacoesAbertas: number;

    constructor(private userService: UsuarioService, private _loginService: LoginService, 
        private _dashboardService: DashboardService, private _router: Router) {
    }

    ngOnInit() {
        this.quantidadeProdutoEstoqueBaixo = 0;
        this.quantidadeCotacoesRespondidas = 0;
        this.quantidadeSolicitacoesAbertas = 0;
        let idEmpresa = this._loginService.getUser().colaborador.empresa.id;
        let nucleo: Nucleo = this._loginService.getUser().colaborador.nucleo;
        let idNucleo = 0;
        if(nucleo && nucleo.id){
            idNucleo = nucleo.id;
        }
        this._dashboardService.quantidadeProdutoBaixoEstoque(idEmpresa, idNucleo).subscribe((resultado) => {
            this.setProdutoEstoque(resultado);
        });

        this._dashboardService.quantidadeCotacoesRespondidas(idEmpresa, idNucleo).subscribe((resultado) => {
            this.setCotacao(resultado);
        }); 

        this._dashboardService.quantidadeSolicitacoesAbertas(idEmpresa, idNucleo).subscribe((resultado) => {
            this.setSolicitacoes(resultado);
        });
        /*this.data = {
            labels: ['Janeiro', 'Fevereiro', 'Março', 'Abril'],
            datasets: [
                {
                    label: 'My First dataset',
                    fillColor: '#42A5F5',
                    strokeColor: '#1E88E5',
                    data: [65, 59, 80, 81]
                },
                {
                    label: 'My Second dataset',
                    fillColor: '#9CCC65',
                    strokeColor: '#7CB342',
                    data: [28, 48, 40, 19]
                }
            ]
        }*/
    }

    setProdutoEstoque(resultado: Response){
        this.quantidadeProdutoEstoqueBaixo = resultado.objetoRetorno;
    }

    setCotacao(resultado: Response){
        this.quantidadeCotacoesRespondidas = resultado.objetoRetorno;
    }

    setSolicitacoes(resultado: Response){
        this.quantidadeSolicitacoesAbertas = resultado.objetoRetorno;
    }

}
