import {Component} from '@angular/core';
import {OnActivate, RouteSegment, Router} from '@angular/router';
import {SelectItem} from 'primeng/primeng';
import {CategoriaProdutoService} from './categoria_produto.service'
import {CategoriaProduto} from '../categoria_produto/categoria_produto'
import {MensagemService} from '../aplicacao/mensagem.service';
import {Response} from '../model/response'
import {MeuPanel} from '../aplicacao/panel';
import {EnumEstadosBrasileiros} from '../enum/enum-estados-brasileiros';

@Component({
	selector: 'categoria-produto-detail',
	templateUrl: './app/categoria_produto/categoria_produto-detail.html',
	providers: [CategoriaProdutoService],
	directives: [MeuPanel]
})
export class CategoriaProdutoDetailComponent implements OnActivate {

	categoriaProduto : CategoriaProduto = new CategoriaProduto();

	constructor(private _categoriaProdutoService: CategoriaProdutoService, private _mensagemService : MensagemService, private _router: Router){
	}

	routerOnActivate(curr: RouteSegment) {
		let id = <number> +curr.getParam('id');

		if(id != -1){
			this.recuperarCategoriaProduto(id);
		}else{
			this.limpar();
		}
	}

	salvar(){

		this.doSalvar();

	}

	limpar(){
		this.categoriaProduto = new CategoriaProduto();
	}

	voltar(){
		this._router.navigate(['/categoriaProduto']);
	}

	private recuperarCategoriaProduto(id:number){
		this._categoriaProdutoService.consultarId(id).subscribe((resultado) => {
			this.setCategoriaProduto(resultado)
		},(error) => {
			console.log(error)
		});
	}

	private setCategoriaProduto(resultado : Response){
		this.categoriaProduto =  <CategoriaProduto>resultado.objetoRetorno;
	}

	private doSalvar(){
		this._categoriaProdutoService.salvar(this.categoriaProduto).subscribe((resultado) => {
			this._mensagemService.showInfo("Categoria de Produto cadastrado com sucesso!");
			this.voltar();
		});
	}
}
