import {Component, OnInit} from '@angular/core';
import {CategoriaProduto} from './categoria_produto';
import {CategoriaProdutoService} from './categoria_produto.service';
import {MeuPanel} from '../aplicacao/panel';
import {MensagemService} from '../aplicacao/mensagem.service';
import {Response} from '../model/response';

import {TimerWrapper} from '@angular/core/src/facade/async';

@Component({
	selector: 'categoria-produto-dialog',
	templateUrl: './app/categoria_produto/categoria_produto-dialog.html',
	directives: [MeuPanel],
	providers: [CategoriaProdutoService]
})
export class CategoriaProdutoDialogComponent implements OnInit{
	
	categoria : CategoriaProduto; 

	isSucesso: boolean;

	isResposta: boolean;

	mensagem: string;

	constructor(private _categoriaProdutoServico : CategoriaProdutoService, private _mensagemService : MensagemService){}

	ngOnInit() {
		this.categoria = new CategoriaProduto();
	}

	salvar(){
		this._mensagemService.showInfo("Categoria salva com sucesso! " + this.categoria.nome);
		this.doSalvar();
	}

	limpar(){
		this.categoria = new CategoriaProduto();
	}

	private doSalvar() {
		this._categoriaProdutoServico.salvar(this.categoria).subscribe((resultado) => {
			this.setCategoria(resultado);
		});
	}

	private setCategoria(resultado : Response){
		this.isResposta = true;
		if(!resultado.sucesso){
			this.mensagem = resultado.mensagem;
			return;
		}

		this.isSucesso = true;
		this.mensagem = "Categoria de Produto salva com sucesso!";

		this._mensagemService.showInfo("Categoria de Produto salva com sucesso!");


		TimerWrapper.setTimeout(() => {  
			this.isResposta = false;
			this.isSucesso = false;
			this.mensagem = "";
			this.categoria = new CategoriaProduto();
		}, 2000);
	}
}