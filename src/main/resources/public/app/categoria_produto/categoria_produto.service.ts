import {Injectable} from '@angular/core';
import {Http, HTTP_PROVIDERS, Headers} from '@angular/http'
import {UrlService} from '../model/url.service';
import {HeadersService} from '../aplicacao/headers.service'
import {LoginService} from '../login/login.service';
import {PaginacaoParams} from '../model/paginacao_params';
import {CategoriaProduto} from '../categoria_produto/categoria_produto';

@Injectable()
export class CategoriaProdutoService  extends UrlService {

private urlNegocio : string = 'categoriaProduto'

	constructor(_http: Http, _headerService: HeadersService, _loginService: LoginService) {
		super(_http, _headerService, _loginService);
	}

	public listar(params: PaginacaoParams) {
		return super.callHeader(`${this.urlNegocio}/listar?params=${JSON.stringify(params)}`);
	}
	
	public listarCombo() {
		return super.callHeader(`${this.urlNegocio}/listarCombo`);
	}

	public consultarId(id: number) {
		return super.callHeader(`${this.urlNegocio}/consultarId?id=${id}`);
	}

	public salvar(categoria: CategoriaProduto) {
		return super.callHeaderBody(`${this.urlNegocio}/salvar`, JSON.stringify(categoria));
	}

	public excluirId(id: number, verificarCategoriaViculadaProduto: boolean) {

		return super.callHeader(`categoriaProduto/excluirId?id=${id}&verificarCategoriaViculadaProduto=${verificarCategoriaViculadaProduto}`);
	}	
}