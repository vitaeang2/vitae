import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {DataTable, Column, LazyLoadEvent, Button,Dialog} from 'primeng/primeng';
import {MaterialDesignDirective} from "../aplicacao/material";
import {CategoriaProduto} from "./categoria_produto";
import {CategoriaProdutoService} from "./categoria_produto.service"
import {MensagemService} from "../aplicacao/mensagem.service"
import {LoginService} from "../login/login.service"
import {PaginacaoParams} from '../model/paginacao_params';
import {Paginacao} from '../model/paginacao';
import {Response} from '../model/response';

@Component({
    selector: 'categoria-produto-list',
    templateUrl: './app/categoria_produto/categoria_produto-list.html',
    directives: [DataTable, Column, Button, Dialog],
    providers: [CategoriaProdutoService]
})
export class CategoriaProdutoListComponent implements OnInit {

    params: PaginacaoParams;
    paginacao: Paginacao = new Paginacao();

    private itemSelecionadoId: number = 0;

    public displayDialogExcluir: boolean = false;

    public displayDialogExcluirCategoriaViculoProduto: boolean = false;

    constructor(private _categoriaProdutoService: CategoriaProdutoService, private _loginService: LoginService, private _mensagemService: MensagemService, private _router: Router) {
    }

    ngOnInit() {
        this.doListar();
    }

    novo() {
        this._router.navigate(['/categoriaProduto/', -1])
    }

    editar(id: number) {
        this._router.navigate(['/categoriaProduto/', id])
    }

    load(event: LazyLoadEvent) {

        this.params = new PaginacaoParams();

        //Calcula a página atual
        let page = 0;
        if (event.first > 0) {
            page = event.first / event.rows;
        }

        this.params.page = page;
        this.params.size = event.rows;
        this.params.sortField = event.sortField;
        this.params.sortOrder = (event.sortOrder == -1) ? 'desc' : 'asc';

        this.doListar();
    }

    private doListar() {
        if (this.params) {
            this._categoriaProdutoService.listar(this.params).subscribe((resultado) => {
                this.setListaCategoriaProduto(resultado)
            });
        }
    }

    private setListaCategoriaProduto(resultado: Response) {
        if (!resultado.sucesso) {
            this._mensagemService.showAtencao(resultado.mensagem);
            return;
        }
        this.paginacao = resultado.objetoRetorno;
    }

    confirmDiolgExcluir(id: number) {

        this.itemSelecionadoId = id;
        this.displayDialogExcluir = true;
    }

    excluir(verificarCategoriaViculadaProduto: boolean) {

        this._categoriaProdutoService.excluirId(this.itemSelecionadoId, verificarCategoriaViculadaProduto).subscribe((resultado: Response) => {

            if (!resultado.sucesso) {
                this.displayDialogExcluirCategoriaViculoProduto = true;
            } else {
                this._mensagemService.showInfo("Categoria de Produto excluído com sucesso!");
                this.displayDialogExcluirCategoriaViculoProduto = false;
                this.doListar();
            }
            this.displayDialogExcluir = false;              
        });

    }
}
