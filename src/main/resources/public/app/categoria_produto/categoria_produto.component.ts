import {Component} from '@angular/core';
import {Routes, ROUTER_DIRECTIVES, Router} from '@angular/router';
import {CategoriaProdutoListComponent} from './categoria_produto-list.component';
import {CategoriaProdutoDetailComponent} from "./categoria_produto-detail.component";

@Component({
	selector: 'categoria-produto',
	templateUrl: 'app/categoria_produto/categoria_produto.html',
	directives: [ROUTER_DIRECTIVES]
})
@Routes([
	{ path: "/", component: CategoriaProdutoListComponent},
	{ path: ":id", component: CategoriaProdutoDetailComponent}
	])

export class CategoriaProdutoComponent  {
	constructor(private _router : Router) {
		this._router.navigate(['/categoriaProduto'])
	}
}
