import {Pessoa} from '../pessoa/pessoa'
import {Cargo} from '../cargo/cargo'
import {Fornecedor} from '../fornecedor/fornecedor'
import {Patrimonio} from '../patrimonio/patrimonio'

export class ManutencaoPatrimonio {

	id: number;

	motivo : string;
	
	descricaoRetorno : string;

	fornecedor : Fornecedor;

	patrimonio : Patrimonio;

	dataSaida : string;

	dataRetorno : string;

	dataPrevistaRetorno : string;

	fimGarantia : string;
	
	valor : number;

	valorPago : number;
	
	ativo: boolean; 
}
