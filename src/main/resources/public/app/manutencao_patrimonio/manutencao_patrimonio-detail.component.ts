import {Component, OnInit} from '@angular/core';
import {OnActivate, RouteSegment, Router} from '@angular/router';
import {Dialog, Dropdown, SelectItem} from 'primeng/primeng';
import {MensagemService} from '../aplicacao/mensagem.service';
import {LoginService} from '../login/login.service';
import {ManutencaoPatrimonio} from '../manutencao_patrimonio/manutencao_patrimonio';
import {ManutencaoPatrimonioService} from '../manutencao_patrimonio/manutencao_patrimonio.service';
import {MeuPanel} from '../aplicacao/panel';
import {Fornecedor} from '../fornecedor/fornecedor';
import {FornecedorService} from '../fornecedor/fornecedor.service';
import {Patrimonio} from '../patrimonio/patrimonio';
import {PatrimonioService} from '../patrimonio/patrimonio.service';
import {Response} from '../model/response';
import {CustomInput} from '../componentes/custom-input.component';
import {EnumUnidadeMedida} from '../enum/enum-unidade-medida';
import { DataValidator } from '../aplicacao/data-validator.directive';
import { NotEqualValidator } from '../aplicacao/not-equal-validator.directive';
import {CustomDatePickerComponent} from '../componentes/custom-datepicker.component';

@Component({
	templateUrl: './app/manutencao_patrimonio/manutencao_patrimonio-detail.html',
	directives: [Dialog, Dropdown, MeuPanel, CustomInput, NotEqualValidator, DataValidator, CustomDatePickerComponent],
	providers: [ManutencaoPatrimonioService, FornecedorService, PatrimonioService, MensagemService]
})
export class ManutencaoPatrimonioDetailComponent implements OnActivate {

	manutencao_patrimonio : ManutencaoPatrimonio = new ManutencaoPatrimonio();

	idFornecedor : number = 0;
	
	idPatrimonio : number = 0;

	listaFornecedor : SelectItem[];
	
	listaPatrimonio : SelectItem[];

	constructor(private _manutencao_patrimonioService : ManutencaoPatrimonioService,private _fornecedorService : FornecedorService, private _patrimonioService : PatrimonioService, 
		private _loginService: LoginService, private _mensagemService : MensagemService, private _router: Router) {
	}

	routerOnActivate(curr: RouteSegment) {

		let id = <number> +curr.getParam('id');
		this.doListaComboFornecedor();
		

		if (id != -1) {
			this.manutencao_patrimonio.id = id;
			//this.recuperarManutencaoPatrimonio(id);

		} else {

			this.limpar();
		}
	}

	recuperarManutencaoPatrimonio(id : number){

		this._manutencao_patrimonioService.consultarId(id).subscribe((resultado) => {

			this.setManutencaoPatrimonio(resultado);

		},(error) => {

			console.log(error)
		});
	}

	limpar(){

		let codigoUnidade = parseInt(EnumUnidadeMedida.QUILOGRAMA.codigo);
		this.manutencao_patrimonio = new ManutencaoPatrimonio();
	}

	salvar() {
		if(this.validarTela() == true){
			this.doSalvar();
		}
	}

	private doSalvar(){
		this.doMontarManutencaoPatrimonio();
		this._manutencao_patrimonioService.salvar(this.manutencao_patrimonio, this._loginService.getUser().id).subscribe((resultado) => {
			this._mensagemService.showInfo("ManutencaoPatrimonio cadastrado com sucesso!");
			this.voltar();
		})
	}

	private setManutencaoPatrimonio(resultado : Response){

		this.manutencao_patrimonio =  <ManutencaoPatrimonio>resultado.objetoRetorno;
		this.idFornecedor = this.manutencao_patrimonio.fornecedor.id;
		this.idPatrimonio = this.manutencao_patrimonio.patrimonio.id;
	}

	private doMontarManutencaoPatrimonio(){

		this.manutencao_patrimonio.fornecedor = new Fornecedor();
		this.manutencao_patrimonio.fornecedor.id = this.idFornecedor;
		this.manutencao_patrimonio.patrimonio = new Patrimonio();
		this.manutencao_patrimonio.patrimonio.id = this.idPatrimonio;

	}

	private validarTela() : boolean{
		return true;
	}

	private doListaComboFornecedor(){
		this._fornecedorService.listarComboPrestadorServico().subscribe((resultado) => {
			this.setListaComboFornecedor(resultado);
		}) ;
	}

	private setListaComboFornecedor(resultado : Response){

		if(!resultado.sucesso){

			return;
		}

		let listaRetorno : Array<Fornecedor> = resultado.objetoRetorno;
		this.listaFornecedor = [];
		listaRetorno.forEach(p => this.listaFornecedor.push({label: p.pessoa.nome, value: p.id}) );
		this.doListaComboPatrimonior();
	}

	private doListaComboPatrimonior(){
		this._patrimonioService.listarCombo().subscribe((resultado) => {
			this.setListaComboPatrimonio(resultado);
		}) ;
	}

	private setListaComboPatrimonio(resultado : Response){

		if(!resultado.sucesso){

			return;
		}

		let listaRetorno : Array<Patrimonio> = resultado.objetoRetorno;
		this.listaPatrimonio = [];
		listaRetorno.forEach(p => this.listaPatrimonio.push({label: p.nome, value: p.id}) );
		if(this.manutencao_patrimonio && this.manutencao_patrimonio.id){
			this.recuperarManutencaoPatrimonio(this.manutencao_patrimonio.id);
		}
	}

	voltar(){
		this._router.navigate(['/manutencao_patrimonio']);
	}
}
