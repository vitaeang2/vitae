import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {DataTable, Column, LazyLoadEvent, Button, FilterMetadata, Dialog, SelectItem} from 'primeng/primeng';
import {MaterialDesignDirective} from "../aplicacao/material";
import {ManutencaoPatrimonio} from "./manutencao_patrimonio";
import {ManutencaoPatrimonioService} from "./manutencao_patrimonio.service";
import {MensagemService} from "../aplicacao/mensagem.service";
import {LoginService} from "../login/login.service";
import {PaginacaoParams} from '../model/paginacao_params';
import {Paginacao} from '../model/paginacao';
import {Response} from '../model/response';
import {VitaeLazy} from '../aplicacao/vitae-lazy.service';

@Component({
    selector: 'manutencao_patrimonio-list',
    templateUrl: './app/manutencao_patrimonio/manutencao_patrimonio-list.html',
    directives: [DataTable, Column, Button, Dialog],
    providers: [ManutencaoPatrimonioService]
})

export class ManutencaoPatrimonioListComponent extends VitaeLazy implements OnInit {

    idManutencaoPatrimonioSelecionado : number;

    displayDialogExcluir: boolean;
    
    listaSituacoesBens : SelectItem[];
    idSituacaoBemSelecionado : number = 0;
    
    numeroEtiqueta : string = "";
    
    nome : string = "";

    constructor(private _manutencao_patrimonioService : ManutencaoPatrimonioService, private _loginService : LoginService, private _mensagemService : MensagemService, 
         private router: Router) {

        super(router);
    }

    ngOnInit(){
        this.displayDialogExcluir = false;
        this.doListar();
    }

    novo() {

        super.novo('/manutencao_patrimonio/');
    }

    editar(id: number) {

        super.editar(id, '/manutencao_patrimonio/');
    }
    
    visualizarEstoque(id: number) {

        super.goTo(id, '/estoque/');
    }

    confirmDiolgExcluir(id:number) {

        this.idManutencaoPatrimonioSelecionado = id;
        this.displayDialogExcluir = true;
    }

    excluir() { 

        this._manutencao_patrimonioService.excluirId(this.idManutencaoPatrimonioSelecionado).subscribe((resultado) => {

            this._mensagemService.showInfo("ManutencaoPatrimonio excluído com sucesso!");
            this.displayDialogExcluir = false;
            this.doListar();
        });

    }

    doListar() {
        if(this.params){
            this._manutencao_patrimonioService.listar(this.params).subscribe((resultado) => {

                this.setListaManutencaoPatrimonio(resultado)
            });
        }
    }

    private setListaManutencaoPatrimonio(resultado : Response) {

        if (!resultado.sucesso) {

            this._mensagemService.showAtencao(resultado.mensagem);
            
            return;
        }

        this.paginacao = resultado.objetoRetorno;
    }


    listarPorFiltros(numeroEtiqueta: string, nome: string){

        this._manutencao_patrimonioService.listarPorFiltros(this.params, numeroEtiqueta, nome).subscribe((resultado) => {

            this.setListaManutencaoPatrimonio(resultado)
        });
    }
}
