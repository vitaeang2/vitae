import {Injectable} from '@angular/core';
import {OnActivate, RouteSegment, Router} from '@angular/router';
import {Http, HTTP_PROVIDERS, Headers} from '@angular/http'
import {UrlService} from '../model/url.service';
import {HeadersService} from '../aplicacao/headers.service'
import {LoginService} from '../login/login.service';
import {PaginacaoParams} from '../model/paginacao_params';
import {ManutencaoPatrimonio} from '../manutencao_patrimonio/manutencao_patrimonio';
import {MensagemService} from '../aplicacao/mensagem.service';

@Injectable()
export class ManutencaoPatrimonioService  extends UrlService {

	private urlNegocio : string = 'manutencao_patrimonio';

	constructor(_http: Http, _headerService: HeadersService, _loginService: LoginService) {
		super(_http, _headerService, _loginService);
	}

	public listar(params: PaginacaoParams) {
		return super.callHeader(`${this.urlNegocio}/listar?params=${JSON.stringify(params)}`);
	}

	public listarCombo() {
		return super.callHeader(`${this.urlNegocio}/listarCombo`);
	}

	public consultarId(id: number) {
		return super.callHeader(`${this.urlNegocio}/consultarId?id=${id}`);
	}

	public salvar(manutencaoPatrimonio: ManutencaoPatrimonio, idUsuario : number) {
		return super.callHeaderBody(`${this.urlNegocio}/salvar?idUsuario=${idUsuario}`, JSON.stringify(manutencaoPatrimonio));
	}

	public excluirId(id: number) {

		return super.callHeader(`${this.urlNegocio}/excluirId?id=${id}`);
	}

	public listarPorFiltros(params: PaginacaoParams, numeroEtiqueta?: string, nome?: string) {
		return super.callHeader(`manutencaoPatrimonio/listar?params=${JSON.stringify(params)}&numeroEtiqueta=${numeroEtiqueta}&nome=${nome}`);
	}

}
