import {Component} from '@angular/core';
import {Routes, ROUTER_DIRECTIVES, Router} from '@angular/router';
import {ManutencaoPatrimonioListComponent} from './manutencao_patrimonio-list.component';
import {ManutencaoPatrimonioDetailComponent} from "./manutencao_patrimonio-detail.component";
import {MovimentoEstoqueComponent} from "../movimento_estoque/movimento_estoque.component";


@Component({
	selector: 'manutencao_patrimonio',
	templateUrl: 'app/manutencao_patrimonio/manutencao_patrimonio.html',
	directives: [ROUTER_DIRECTIVES]
})

@Routes([

	{ path: "/", component: ManutencaoPatrimonioListComponent},
	{ path: ":id", component: ManutencaoPatrimonioDetailComponent},
	{ path: "/estoque/:id/", component: MovimentoEstoqueComponent}
		
	])
export class ManutencaoPatrimonioComponent  {

	constructor(private _router : Router) {

		this._router.navigate(['/manutencao_patrimonio'])
	}
}
