import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {DataTable, Column, LazyLoadEvent, Button,Dialog} from 'primeng/primeng';
import {MaterialDesignDirective} from "../aplicacao/material";
import {Local} from "./local";
import {LocalService} from "./local.service"
import {MensagemService} from "../aplicacao/mensagem.service"
import {LoginService} from "../login/login.service"
import {PaginacaoParams} from '../model/paginacao_params';
import {Paginacao} from '../model/paginacao';
import {Response} from '../model/response';

@Component({
    selector: 'local-list',
    templateUrl: './app/local/local-list.html',
    directives: [DataTable, Column, Button, Dialog],
    providers: [LocalService]
})
export class LocalListComponent implements OnInit {

    params: PaginacaoParams;
    paginacao: Paginacao = new Paginacao();

    private itemSelecionadoId: number = 0;

    public displayDialogExcluir: boolean = false;

    public displayDialogExcluirLocalViculoSolicitacao: boolean = false;

    constructor(private _localService: LocalService, private _loginService: LoginService, private _mensagemService: MensagemService, private _router: Router) {
    }

    ngOnInit() {
        this.doListar();
    }

    novo() {
        this._router.navigate(['/local/', -1])
    }

    editar(id: number) {
        this._router.navigate(['/local/', id])
    }

    load(event: LazyLoadEvent) {

        this.params = new PaginacaoParams();

        //Calcula a página atual
        let page = 0;
        if (event.first > 0) {
            page = event.first / event.rows;
        }

        this.params.page = page;
        this.params.size = event.rows;
        this.params.sortField = event.sortField;
        this.params.sortOrder = (event.sortOrder == -1) ? 'desc' : 'asc';

        this.doListar();
    }

    private doListar() {
        if (this.params) {
            this._localService.listar(this.params).subscribe((resultado) => {
                this.setListaLocal(resultado)
            });
        }
    }

    private setListaLocal(resultado: Response) {
        if (!resultado.sucesso) {
            this._mensagemService.showAtencao(resultado.mensagem);
            return;
        }
        this.paginacao = resultado.objetoRetorno;
    }

    confirmDiolgExcluir(id: number) {

        this.itemSelecionadoId = id;
        this.displayDialogExcluir = true;
    }

    excluir(verificarLocalViculadaSolicitacao: boolean) {

        this._localService.excluirId(this.itemSelecionadoId, verificarLocalViculadaSolicitacao).subscribe((resultado: Response) => {

            if (!resultado.sucesso) {
                this.displayDialogExcluirLocalViculoSolicitacao = true;
            } else {
                this._mensagemService.showInfo("Local excluído com sucesso!");
                this.displayDialogExcluirLocalViculoSolicitacao = false;
                this.doListar();
            }
            this.displayDialogExcluir = false;              
        });

    }
}
