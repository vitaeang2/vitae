import {Component, OnInit} from '@angular/core';
import {Local} from './local';
import {LocalService} from './local.service';
import {MeuPanel} from '../aplicacao/panel';
import {MensagemService} from '../aplicacao/mensagem.service';
import {Response} from '../model/response';

import {TimerWrapper} from '@angular/core/src/facade/async';

@Component({
	selector: 'local-dialog',
	templateUrl: './app/local/local-dialog.html',
	directives: [MeuPanel],
	providers: [LocalService]
})
export class LocalDialogComponent implements OnInit{
	
	local : Local; 

	isSucesso: boolean;

	isResposta: boolean;

	mensagem: string;

	constructor(private _localServico : LocalService, private _mensagemService : MensagemService){}

	ngOnInit() {
		this.local = new Local();
	}

	salvar(){
		this.doSalvar();
	}

	limpar(){
		this.local = new Local();
	}

	private doSalvar(){
		this._localServico.salvar(this.local).subscribe((resultado) => {
			
			if(resultado.sucesso){
				this._mensagemService.showInfo("Local cadastrado com sucesso!");
				this.setLocal(resultado);				
			} else {
				this._mensagemService.showInfo(resultado.mensagem);
			}
		});
	}

	private setLocal(resultado : Response){
		this.isResposta = true;
		if(!resultado.sucesso){
			this.mensagem = resultado.mensagem;
			return;
		}

		this.isSucesso = true;
		this.mensagem = "Local salvo com sucesso!";

		this._mensagemService.showInfo("Local salvo com sucesso!");

		TimerWrapper.setTimeout(() => {  
			this.isResposta = false;
			this.isSucesso = false;
			this.mensagem = "";
			this.local = new Local();
		}, 2000);
	}
}