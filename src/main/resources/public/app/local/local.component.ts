import {Component} from '@angular/core';
import {Routes, ROUTER_DIRECTIVES, Router} from '@angular/router';
import {LocalListComponent} from './local-list.component';
import {LocalDetailComponent} from "./local-detail.component";

@Component({
	selector: 'local',
	templateUrl: 'app/local/local.html',
	directives: [ROUTER_DIRECTIVES]
})
@Routes([
	{ path: "/", component: LocalListComponent},
	{ path: ":id", component: LocalDetailComponent}
	])

export class LocalComponent  {
	constructor(private _router : Router) {
		this._router.navigate(['/local'])
	}
}
