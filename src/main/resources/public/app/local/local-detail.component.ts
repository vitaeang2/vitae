import {Component} from '@angular/core';
import {OnActivate, RouteSegment, Router} from '@angular/router';
import {SelectItem} from 'primeng/primeng';
import {LocalService} from './local.service'
import {Local} from '../local/local'
import {MensagemService} from '../aplicacao/mensagem.service';
import {Response} from '../model/response'
import {MeuPanel} from '../aplicacao/panel';

@Component({
	selector: 'local-detail',
	templateUrl: './app/local/local-detail.html',
	providers: [LocalService],
	directives: [MeuPanel]
})
export class LocalDetailComponent implements OnActivate {

	local : Local = new Local();

	constructor(private _localService: LocalService, private _mensagemService : MensagemService, private _router: Router){
	}

	routerOnActivate(curr: RouteSegment) {
		let id = <number> +curr.getParam('id');

		if(id != -1){
			this.recuperarLocal(id);
		}else{
			this.limpar();
		}
	}

	salvar(){

		this.doSalvar();

	}

	limpar(){
		this.local = new Local();
	}

	voltar(){
		this._router.navigate(['/local']);
	}

	private recuperarLocal(id:number){
		this._localService.consultarId(id).subscribe((resultado) => {
			this.setLocal(resultado)
		},(error) => {
			console.log(error)
		});
	}

	private setLocal(resultado : Response){
		this.local =  <Local>resultado.objetoRetorno;
	}

	private doSalvar(){
		this._localService.salvar(this.local).subscribe((resultado) => {
			
			if(resultado.sucesso){
				this._mensagemService.showInfo("Local cadastrado com sucesso!");
				this.voltar();				
			} else {
				this._mensagemService.showInfo(resultado.mensagem);
			}
		});
	}
}
