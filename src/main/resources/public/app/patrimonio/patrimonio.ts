import {EnumUnidadeMedida} from '../enum/enum-unidade-medida'
import {Departamento} from '../departamento/departamento'
import {GrupoBem} from '../grupo_bem/grupo_bem'
import {ComponenteItemPatrimonio} from '../componente_item_patrimonio/componente_item_patrimonio'


export class Patrimonio {

	id: number;

	nome : string;
	
	observacao : string;

	numeroEtiqueta : string;
	
	numeroNota : string;

	numeroSerie : string;

	descricaoGarantia : string;

	ativo: boolean;	

	unidadeMedida: number;

	unidadeMedidaStr: string; 

	situacaoBemPatrimonio: number;

	situacaoBemStr: string; 

	tipoIncorporacaoPatrimonio: number;

	tipoIncorporacaoStr: string; 
	
	dataNota: string; 

	dataIncorporacao: string; 

	fimGarantia: string; 
	
	departamento : Departamento;
	
	grupoBem : GrupoBem;

	valor : number;

	componenteItemPatrimonio : Array<ComponenteItemPatrimonio>;
}
