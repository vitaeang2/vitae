import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {DataTable, Column, LazyLoadEvent, Button, Dropdown, SelectItem, Dialog} from 'primeng/primeng';
import {MaterialDesignDirective} from "../aplicacao/material";
import { DataValidator } from '../aplicacao/data-validator.directive';
import {CustomDatePickerComponent} from '../componentes/custom-datepicker.component';
import {Patrimonio} from "./patrimonio";
import {PatrimonioService} from "./patrimonio.service";
import {Departamento} from "../departamento/departamento";
import {DepartamentoService} from "../departamento/departamento.service"
import {GrupoBem} from "../grupo_bem/grupo_bem";
import {GrupoBemService} from "../grupo_bem/grupo_bem.service";
import {EnumSituacaoBemPatrimonio} from '../enum/enum-situacao-bem';
import {MensagemService} from "../aplicacao/mensagem.service";
import {LoginService} from "../login/login.service";
import {PaginacaoParams} from '../model/paginacao_params';
import {Paginacao} from '../model/paginacao';
import {Response} from '../model/response';
import {MoneyPipe} from '../componentes/money_pipe';

@Component({
    selector: 'patrimonio-list',
    templateUrl: './app/patrimonio/patrimonio-list.html',
    directives: [DataTable, Column, Button, Dropdown, Dialog, DataValidator, CustomDatePickerComponent],
    providers: [PatrimonioService, DepartamentoService, GrupoBemService],
    pipes: [MoneyPipe]
})
export class PatrimonioListComponent implements OnInit {

    params: PaginacaoParams;
    paginacao: Paginacao = new Paginacao();

    private itemSelecionadoId: number = 0;

    public displayDialogExcluir: boolean = false;

    public displayDialogExcluirPatrimonioViculoPatrimonio: boolean = false;

    listaDepartamento : SelectItem[];
    idDepartamentoSelecionado : number = 0;
    
    listaGrupoBem : SelectItem[];
    idGrupoBemSelecionado : number = 0;
    
    listaSituacoesBens : SelectItem[];
    idSituacaoBemSelecionado : number = 0;
    
    numeroEtiqueta : string = "";
    
    nome : string = "";
    
    numeroNota : string = "";
    
    fimGarantia : string = "";
    
    

    constructor(private _patrimonioService: PatrimonioService, private _departamentoService: DepartamentoService, private _grupoBemService: GrupoBemService, 
        private _loginService: LoginService, private _mensagemService: MensagemService, private _router: Router) {
    }

    ngOnInit() {
        this.doListar();
        this.carregarListaDepartamento();
        this.carregarListaGrupoBem();
        this.carregarlistarsituacoesBens();
    }

    novo() {
        this._router.navigate(['/patrimonio/', -1])
    }

    editar(id: number) {
        this._router.navigate(['/patrimonio/', id])
    }

    load(event: LazyLoadEvent) {

        this.params = new PaginacaoParams();

        //Calcula a página atual
        let page = 0;
        if (event.first > 0) {
            page = event.first / event.rows;
        }

        this.params.page = page;
        this.params.size = event.rows;
        this.params.sortField = event.sortField;
        this.params.sortOrder = (event.sortOrder == -1) ? 'desc' : 'asc';

        this.doListar();
    }

    private doListar() {
        if (this.params) {
            this._patrimonioService.listar(this.params).subscribe((resultado) => {
                this.setListaPatrimonio(resultado)
            });
        }
    }

    private setListaPatrimonio(resultado: Response) {
        if (!resultado.sucesso) {
            this._mensagemService.showAtencao(resultado.mensagem);
            return;
        }
        this.paginacao = resultado.objetoRetorno;
    }

    confirmDiolgExcluir(id: number) {

        this.itemSelecionadoId = id;
        this.displayDialogExcluir = true;
    }

    excluir(verificarPatrimonioViculadaPatrimonio: boolean) {

        this._patrimonioService.excluirId(this.itemSelecionadoId, verificarPatrimonioViculadaPatrimonio).subscribe((resultado: Response) => {

            if (!resultado.sucesso) {
                this.displayDialogExcluirPatrimonioViculoPatrimonio = true;
            } else {
                this._mensagemService.showInfo("Patrimonio excluído com sucesso!");
                this.displayDialogExcluirPatrimonioViculoPatrimonio = false;
                this.doListar();
            }
            this.displayDialogExcluir = false;              
        });

    }

    listarPorFiltros(idDepartamento: number, idGrupoBem: number, numeroEtiqueta: string, nome: string, numeroNota: string, fimGarantia: string, idSituacaoBem: string){

        this._patrimonioService.listarPorFiltros(this.params, idDepartamento, idGrupoBem, numeroEtiqueta, nome, numeroNota, fimGarantia, idSituacaoBem).subscribe((resultado) => {

            this.setListaPatrimonio(resultado)
        });
    }

    private carregarListaDepartamento(){

        this._departamentoService.listarCombo().subscribe((resultado) => {

            this.setListaDepartamento(resultado)
        });
    }

    private setListaDepartamento(resultado : Response){

        if(!resultado.sucesso){

            this._mensagemService.showAtencao(resultado.mensagem);
            return;
        }
        
        this.listaDepartamento = [];
        let lista : Array<Departamento> = resultado.objetoRetorno;
        this.listaDepartamento.push({label: "-- TODOS --", value: 0});
        lista.forEach(d => this.listaDepartamento.push({label: d.nome, value: d.id}));
    }
    
    private carregarListaGrupoBem(){

        this._grupoBemService.listarCombo().subscribe((resultado) => {

            this.setListaGrupoBem(resultado)
        });
    }

    private setListaGrupoBem(resultado : Response){

        if(!resultado.sucesso){

            this._mensagemService.showAtencao(resultado.mensagem);
            return;
        }
        
        this.listaGrupoBem = [];
        let lista : Array<GrupoBem> = resultado.objetoRetorno;
        this.listaGrupoBem.push({label: "-- TODOS --", value: 0});
        lista.forEach(g => this.listaGrupoBem.push({label: g.nome, value: g.id}));
    }

    private carregarlistarsituacoesBens(){
        
        this.listaSituacoesBens = [];
        let lista : Array<EnumSituacaoBemPatrimonio> = EnumSituacaoBemPatrimonio.toArray();
        this.listaSituacoesBens.push({label: "-- TODOS --", value: 0});
        lista.forEach(g => this.listaSituacoesBens.push({label: g.descricao, value: g.codigo}));
        
    }


}
