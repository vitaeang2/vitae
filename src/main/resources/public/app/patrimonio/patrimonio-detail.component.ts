import {Component} from '@angular/core';
import {OnActivate, RouteSegment, Router} from '@angular/router';
import {DataTable, Column, LazyLoadEvent, SelectItem, Dropdown, Dialog, TabView, TabPanel} from 'primeng/primeng';
import {PatrimonioService} from './patrimonio.service'
import {Patrimonio} from '../patrimonio/patrimonio'
import {MensagemService} from '../aplicacao/mensagem.service';
import {Response} from '../model/response'
import {MeuPanel} from '../aplicacao/panel';
import {EnumEstadosBrasileiros} from '../enum/enum-estados-brasileiros';
import {EnumUnidadeMedida} from '../enum/enum-unidade-medida';
import {EnumSituacaoBemPatrimonio} from '../enum/enum-situacao-bem';
import {EnumTipoIncorporacaoPatrimonio} from '../enum/enum-tipo-incorporacao';
import {CustomInput} from '../componentes/custom-input.component';
import { DataValidator } from '../aplicacao/data-validator.directive';
import { NotEqualValidator } from '../aplicacao/not-equal-validator.directive';
import {CustomDatePickerComponent} from '../componentes/custom-datepicker.component';
import {Departamento} from '../departamento/departamento';
import {DepartamentoDialogComponent} from '../departamento/departamento-dialog.component';
import {DepartamentoService} from '../departamento/departamento.service';
import {GrupoBem} from '../grupo_bem/grupo_bem';
import {GrupoBemDialogComponent} from '../grupo_bem/grupo_bem-dialog.component';
import {ComponenteItemPatrimonio} from '../componente_item_patrimonio/componente_item_patrimonio';
import {ComponenteItemPatrimonioService} from '../componente_item_patrimonio/componente_item_patrimonio.service';
import {GrupoBemService} from '../grupo_bem/grupo_bem.service';

@Component({
	selector: 'patrimonio-detail',
	templateUrl: './app/patrimonio/patrimonio-detail.html',
	providers: [PatrimonioService, DepartamentoService, GrupoBemService, ComponenteItemPatrimonioService],
	directives: [Dialog, Dropdown, MeuPanel, CustomInput, NotEqualValidator, DataValidator, CustomDatePickerComponent, DepartamentoDialogComponent, GrupoBemDialogComponent, TabView, TabPanel]
})
export class PatrimonioDetailComponent implements OnActivate {

	patrimonio : Patrimonio = new Patrimonio();

	componente_item_patrimonio : ComponenteItemPatrimonio = new ComponenteItemPatrimonio();

	
	idUnidade : number = 0;

	idSituacaoBem : number = 0;

	idTipoIncorporacao : number = 0;

	idDepartamento : number = 0;

	mostrarDialogDepartamento : boolean = false;

	idGrupoBem : number = 0;

	mostrarDialogGrupoBem : boolean = false;

	public displayDialogExcluirComponente: boolean = false;

	private componenteSelecionadoId: number = 0;

	listaUnidades : SelectItem[];

	listaDepartamento : SelectItem[];
	
	listaGrupoBem : SelectItem[];

	listaSituacoesBens : SelectItem[];
	
	listaTiposIncorporacoes : SelectItem[];

	constructor(private _patrimonioService: PatrimonioService, private _departamentoService : DepartamentoService,
		private _grupoBemService : GrupoBemService, private _componenteItemPatrimonioService : ComponenteItemPatrimonioService, private _mensagemService : MensagemService, private _router: Router){
	}

	routerOnActivate(curr: RouteSegment) {

		let id = <number> +curr.getParam('id');
		this.doListaComboDepartamento();
		
		if(id != -1){
			this.patrimonio.id = id;
			//this.recuperarPatrimonio(id);
		}else{
			this.limpar();
		}
	}

	salvar(){

		this.doSalvar();

	}


	recuperarPatrimonio(id:number){
		this._patrimonioService.consultarId(id).subscribe((resultado) => {
			this.setPatrimonio(resultado)
		},(error) => {
			console.log(error)
		});
	}

	limpar(){

		this.patrimonio = new Patrimonio();

		this.patrimonio.componenteItemPatrimonio = new Array<ComponenteItemPatrimonio>();
		this.componente_item_patrimonio = new ComponenteItemPatrimonio();

		this.idUnidade = 0;
		this.idSituacaoBem = 0;
		this.idTipoIncorporacao = 0;
		this.idDepartamento = 0;
		this.idGrupoBem = 0;
		this.componenteSelecionadoId = 0;
	}

	voltar(){
		this._router.navigate(['/patrimonio']);
	}

	private setPatrimonio(resultado : Response){
		this.patrimonio =  <Patrimonio>resultado.objetoRetorno;
		this.idUnidade = this.patrimonio.unidadeMedida;
		this.idSituacaoBem = this.patrimonio.situacaoBemPatrimonio;
		this.idTipoIncorporacao = this.patrimonio.tipoIncorporacaoPatrimonio;
		this.idDepartamento = this.patrimonio.departamento.id;
		this.idGrupoBem = this.patrimonio.grupoBem.id;

	}

	private doSalvar(){
		this.doMontarPatrimonio();

		this._patrimonioService.salvar(this.patrimonio).subscribe((resultado) => {
			this._mensagemService.showInfo("Patrimonio cadastrado com sucesso!");
			this.voltar();
		});
	}

	private doMontarPatrimonio(){

		this.patrimonio.unidadeMedida = this.idUnidade;
		this.patrimonio.situacaoBemPatrimonio = this.idSituacaoBem;
		this.patrimonio.tipoIncorporacaoPatrimonio = this.idTipoIncorporacao;
		this.patrimonio.departamento = new Departamento();
		this.patrimonio.departamento.id = this.idDepartamento;
		this.patrimonio.grupoBem = new GrupoBem();
		this.patrimonio.grupoBem.id = this.idGrupoBem;
	}

	private listarUnidades(){
		this.listaUnidades = [];
		this.listaUnidades.push({label: "-- Selecione --", value: 0})
		let lista : Array<EnumUnidadeMedida> = EnumUnidadeMedida.toArray();
		lista.forEach(g => this.listaUnidades.push({label: g.descricao, value: g.codigo}));
		
		this.listarSituacoesBens();
		
	}

	private listarSituacoesBens(){
		this.listaSituacoesBens = [];
		this.listaSituacoesBens.push({label: "-- Selecione --", value: 0})
		let lista : Array<EnumSituacaoBemPatrimonio> = EnumSituacaoBemPatrimonio.toArray();
		lista.forEach(g => this.listaSituacoesBens.push({label: g.descricao, value: g.codigo}));

		this.listarTiposIncorporacoes();
	}

	private listarTiposIncorporacoes(){
		this.listaTiposIncorporacoes = [];
		this.listaTiposIncorporacoes.push({label: "-- Selecione --", value: 0})
		let lista : Array<EnumTipoIncorporacaoPatrimonio> = EnumTipoIncorporacaoPatrimonio.toArray();
		lista.forEach(g => this.listaTiposIncorporacoes.push({label: g.descricao, value: g.codigo}));



		if(this.patrimonio && this.patrimonio.id){
			this.recuperarPatrimonio(this.patrimonio.id);
		}
	}

	showDialogDepartamento(){
		this.mostrarDialogDepartamento = true;
	}

	closeDialogDepartamento(){
		this.mostrarDialogDepartamento = false;
		this.doListaComboDepartamento();
	}
	
	private doListaComboDepartamento(){
		this._departamentoService.listarCombo().subscribe((resultado) => {
			this.setListaComboDepartamento(resultado);
		}) ;
	}

	private setListaComboDepartamento(resultado : Response){
		if(!resultado.sucesso){
			return;
		}
		let listaRetorno : Array<Departamento> = resultado.objetoRetorno;
		this.listaDepartamento = [];
		listaRetorno.forEach(p => this.listaDepartamento.push({label: p.nome, value: p.id}) );

		this.doListaComboGrupoBem();
	}

	showDialogGrupoBem(){
		this.mostrarDialogGrupoBem = true;
	}

	closeDialogGrupoBem(){
		this.mostrarDialogGrupoBem = false;
		this.doListaComboGrupoBem();
	}
	
	private doListaComboGrupoBem(){
		this._grupoBemService.listarCombo().subscribe((resultado) => {
			this.setListaComboGrupoBem(resultado);
		}) ;
	}

	private setListaComboGrupoBem(resultado : Response){
		if(!resultado.sucesso){
			return;
		}
		let listaRetorno : Array<GrupoBem> = resultado.objetoRetorno;
		this.listaGrupoBem = [];
		listaRetorno.forEach(p => this.listaGrupoBem.push({label: p.nome, value: p.id}) );

		this.listarUnidades();
	}

	private setComponenteItemPatrimonio(item : ComponenteItemPatrimonio){

		if(item.descricao == null  || item.descricao == ""){
			this._mensagemService.showAtencao("Descrição é obrigatório");
			return;
		}
		/*if(item.observacao == null  || item.observacao == ""){
			this._mensagemService.showAtencao("Observação é obrigatório");
			return;
		}
		if(item.numeroSerie == null  || item.numeroSerie == ""){
			this._mensagemService.showAtencao("Número de Série é obrigatório");
			return;
		}		
		if(item.descricaoGarantia == null  || item.descricaoGarantia == ""){
			this._mensagemService.showAtencao("Descrição da Garantia é obrigatório");
			return;
		}*/
		this.patrimonio.componenteItemPatrimonio.push(item);
		this.componente_item_patrimonio = new ComponenteItemPatrimonio();
	}

	confirmDiolgExcluirComponente(item : ComponenteItemPatrimonio) {

		this.componenteSelecionadoId = item.id;
		this.componente_item_patrimonio = item;
		this.displayDialogExcluirComponente = true;
	}

	excluir() {
		
		this._componenteItemPatrimonioService.excluirId(this.componenteSelecionadoId).subscribe((resultado) => {

			this._mensagemService.showInfo("Componente Item Patrimônio excluído com sucesso!");
			this.displayDialogExcluirComponente = false;
		});

		var index = this.patrimonio.componenteItemPatrimonio.indexOf(this.componente_item_patrimonio);
		this.patrimonio.componenteItemPatrimonio.splice(index, 1);
		this.componente_item_patrimonio = new ComponenteItemPatrimonio();
	}

	editarComponenteItemPatrimonio(item : ComponenteItemPatrimonio) {

		this.componenteSelecionadoId = item.id;
		this.componente_item_patrimonio = item;
		var index = this.patrimonio.componenteItemPatrimonio.indexOf(this.componente_item_patrimonio);
		this.patrimonio.componenteItemPatrimonio.splice(index, 1);
	}
}
