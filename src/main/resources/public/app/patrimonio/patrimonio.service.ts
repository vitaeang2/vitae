import {Injectable} from '@angular/core';
import {Http, HTTP_PROVIDERS, Headers} from '@angular/http'
import {UrlService} from '../model/url.service';
import {HeadersService} from '../aplicacao/headers.service'
import {LoginService} from '../login/login.service';
import {PaginacaoParams} from '../model/paginacao_params';
import {Patrimonio} from '../patrimonio/patrimonio';

@Injectable()
export class PatrimonioService  extends UrlService {

	private urlNegocio : string = 'patrimonio'

	constructor(_http: Http, _headerService: HeadersService, _loginService: LoginService) {
		super(_http, _headerService, _loginService);
	}

	public listar(params: PaginacaoParams) {
		return super.callHeader(`${this.urlNegocio}/listar?params=${JSON.stringify(params)}`);
	}

	public listarPorFiltros(params: PaginacaoParams, idDepartamento?: number, idGrupoBem?: number, numeroEtiqueta?: string, nome?: string, numeroNota?: string, fimGarantia?: string, idSituacaoBem?: string) {
		return super.callHeader(`patrimonio/listar?params=${JSON.stringify(params)}&idDepartamento=${idDepartamento}&idGrupoBem=${idGrupoBem}&numeroEtiqueta=${numeroEtiqueta}&nome=${nome}&numeroNota=${numeroNota}&fimGarantia=${fimGarantia}&idSituacaoBem=${idSituacaoBem}`);
	}
	
	public listarCombo() {
		return super.callHeader(`${this.urlNegocio}/listarCombo`);
	}

	public consultarId(id: number) {
		return super.callHeader(`${this.urlNegocio}/consultarId?id=${id}`);
	}

	public salvar(patrimonio: Patrimonio) {
		return super.callHeaderBody(`${this.urlNegocio}/salvar`, JSON.stringify(patrimonio));
	}

	public excluirId(id: number, verificarPatrimonioViculadaPatrimonio: boolean) {

		return super.callHeader(`patrimonio/excluirId?id=${id}&verificarPatrimonioViculadaPatrimonio=${verificarPatrimonioViculadaPatrimonio}`);
	}	
}