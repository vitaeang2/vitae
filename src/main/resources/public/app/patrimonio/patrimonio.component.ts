import {Component} from '@angular/core';
import {Routes, ROUTER_DIRECTIVES, Router} from '@angular/router';
import {PatrimonioListComponent} from './patrimonio-list.component';
import {PatrimonioDetailComponent} from "./patrimonio-detail.component";

@Component({
	selector: 'patrimonio',
	templateUrl: 'app/patrimonio/patrimonio.html',
	directives: [ROUTER_DIRECTIVES]
})
@Routes([
	{ path: "/", component: PatrimonioListComponent},
	{ path: ":id", component: PatrimonioDetailComponent}
	])

export class PatrimonioComponent  {
	constructor(private _router : Router) {
		this._router.navigate(['/patrimonio'])
	}
}
