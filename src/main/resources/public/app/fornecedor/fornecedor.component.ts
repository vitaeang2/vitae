import {Component} from '@angular/core';
import {Routes, ROUTER_DIRECTIVES, Router} from '@angular/router';
import {FornecedorListComponent} from './fornecedor-list.component';
import {FornecedorDetailComponent} from "./fornecedor-detail.component";

@Component({
	selector: 'fornecedor',
	templateUrl: 'app/fornecedor/fornecedor.html',
	directives: [ROUTER_DIRECTIVES]
})
@Routes([
	{ path: "/", component: FornecedorListComponent},
	{ path: ":id", component: FornecedorDetailComponent}
	])
export class FornecedorComponent  {
	constructor(private _router : Router) {
		this._router.navigate(['/fornecedor'])
	}
}
