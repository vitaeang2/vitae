import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {DataTable, Column, LazyLoadEvent, Button, Dialog} from 'primeng/primeng';
import {MaterialDesignDirective} from "../aplicacao/material";
import {Fornecedor} from "./fornecedor";
import {FornecedorService} from "./fornecedor.service"
import {MensagemService} from "../aplicacao/mensagem.service"
import {LoginService} from "../login/login.service"
import {PaginacaoParams} from '../model/paginacao_params';
import {Paginacao} from '../model/paginacao';
import {Response} from '../model/response';
import {VitaeLazy} from '../aplicacao/vitae-lazy.service';

@Component({
    selector: 'fornecedor',
    templateUrl: './app/fornecedor/fornecedor-list.html',
    directives: [DataTable, Column, Button, Dialog],
    providers: [FornecedorService]
})
export class FornecedorListComponent extends VitaeLazy implements OnInit {

    params : PaginacaoParams;
    paginacao : Paginacao = new Paginacao();


    private itemSelecionadoId: number = 0;

    public displayDialogExcluir: boolean = false;

    constructor(private _fornecedorService : FornecedorService, private _loginService : LoginService, private _mensagemService : MensagemService, private router: Router){
        super(router);
    }

    ngOnInit(){
        this.doListar();
    }

    novo() {
        super.novo('/fornecedor/');
    }
    
    editar(id: number){
        super.editar(id, '/fornecedor/');
    }

    doListar(){
        if(this.params){
            this._fornecedorService.listar(this.params).subscribe((resultado) => {
                this.setListaFornecedor(resultado)
            });
        }
    }

    private setListaFornecedor(resultado : Response){
        if(!resultado.sucesso){
            this._mensagemService.showAtencao(resultado.mensagem);
            return;
        }
        this.paginacao = resultado.objetoRetorno;
    }


    confirmDiolgExcluir(id:number) {
        
        this.itemSelecionadoId = id;
        this.displayDialogExcluir = true;
    }

    excluir() { 

        this._fornecedorService.excluirId(this.itemSelecionadoId).subscribe((resultado) => {

            this._mensagemService.showInfo("Fornecedor excluído com sucesso!");
            this.displayDialogExcluir = false;
            this.doListar();
        });

    }
}
