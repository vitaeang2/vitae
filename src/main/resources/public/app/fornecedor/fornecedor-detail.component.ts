import {Component} from '@angular/core';
import {OnActivate, RouteSegment, Router} from '@angular/router';
import {SelectItem} from 'primeng/primeng';
import {FornecedorService} from './fornecedor.service'
import {Fornecedor} from '../fornecedor/fornecedor'
import {MensagemService} from '../aplicacao/mensagem.service';
import {Response} from '../model/response'
import {Pessoa} from '../pessoa/pessoa'
import {MeuPanel} from '../aplicacao/panel';
import {CustomInput} from '../componentes/custom-input.component';
import {EnumEstadosBrasileiros} from '../enum/enum-estados-brasileiros';

@Component({
	selector: 'fornecedor-detail',
	templateUrl: './app/fornecedor/fornecedor-detail.html',
	providers: [FornecedorService],
	directives: [MeuPanel, CustomInput]
})
export class FornecedorDetailComponent implements OnActivate {

	fornecedor : Fornecedor = new Fornecedor();

	listaEstado = EnumEstadosBrasileiros;

	constructor(private _fornecedorService: FornecedorService, private _mensagemService : MensagemService, private _router: Router){
	}

	routerOnActivate(curr: RouteSegment) {
		let id = <number> +curr.getParam('id');
		this.limpar();

		if(id != -1){
			this.recuperarFornecedor(id);
		}
	}

	salvar(){
		this.doSalvar();
	}

	limpar(){
		this.fornecedor = new Fornecedor();
		this.fornecedor.pessoa = new Pessoa();
	}

	voltar(){
		this._router.navigate(['/fornecedor']);
	}

	private recuperarFornecedor(id:number){
		this._fornecedorService.consultarId(id).subscribe((resultado) => {
			this.setFornecedor(resultado)
		},(error) => {
			console.log(error)
		});
	}

	private setFornecedor(resultado : Response){
		this.fornecedor =  <Fornecedor>resultado.objetoRetorno;
	}

	private doSalvar(){
		this._fornecedorService.salvar(this.fornecedor).subscribe((resultado) => {
			this.setVoltar()
		});
	}

	private setVoltar(){
		this._mensagemService.showInfo("Fornecedor cadastrado com sucesso!")
		this._router.navigate(['/fornecedor']);
	}
}
