import {Pessoa} from '../pessoa/pessoa'

export class Fornecedor {

	id: number;

	selecionado: boolean;
	
	prestadorServico: boolean;

	nome : string;

	pessoa : Pessoa;

	nomeContato : string;

	celularContato : string;

	// Usado para habilitar ou desabilitar email dos fornecedores em cotação
	habilitaEmail: boolean;
}