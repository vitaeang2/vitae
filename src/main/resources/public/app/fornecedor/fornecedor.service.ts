import {Injectable} from '@angular/core';
import {Http, HTTP_PROVIDERS, Headers} from '@angular/http'
import {UrlService} from '../model/url.service';
import {HeadersService} from '../aplicacao/headers.service'
import {LoginService} from '../login/login.service';
import {PaginacaoParams} from '../model/paginacao_params';
import {Fornecedor} from '../fornecedor/fornecedor';

@Injectable()
export class FornecedorService extends UrlService {

	constructor(_http: Http, _headerService: HeadersService, _loginService: LoginService) {
		super(_http, _headerService, _loginService);
	}

	public listar(params: PaginacaoParams) {
		return super.callHeader(`fornecedor/listar?params=${JSON.stringify(params)}`);
	}
	
	public listarCombo() {
		return super.callHeader(`fornecedor/listarCombo`);
	}

	public listarFiltroCotacao(valor: string) {
		return super.callHeader(`fornecedor/listarFiltroCotacao?valor=${valor}`);
	}
		
	public listarComboPrestadorServico() {
		return super.callHeader(`fornecedor/listarComboPrestadorServico`);
	}

	public consultarId(id: number) {
		return super.callHeader(`fornecedor/consultarId?id=${id}`);
	}

	public salvar(fornecedor: Fornecedor) {
		return super.callHeaderBody(`fornecedor/salvar`, JSON.stringify(fornecedor));
	}

	public atualizarPessoa(idPessoa: number, email: string) {
		return super.callHeader(`fornecedor/atualizarPessoa?idPessoa=${idPessoa}&email=${email}`);
	}


	public excluirId(id: number) {

		return super.callHeader(`fornecedor/excluirId?id=${id}`);
	}
}