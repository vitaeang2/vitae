import {SelectItem} from "primeng/primeng";

export class EnumTipoMovimentoEstoque {

		private static values:Array<EnumTipoMovimentoEstoque> = [];

		private static selects:SelectItem[] = [];

		public static SELECIONE:EnumTipoMovimentoEstoque = new EnumTipoMovimentoEstoque(null, "-- Selecione --");
		public static ENTRADA:EnumTipoMovimentoEstoque = new EnumTipoMovimentoEstoque(1, "Entrada");
		public static SAIDA:EnumTipoMovimentoEstoque = new EnumTipoMovimentoEstoque(2, "Saída");
		public static BALANCO:EnumTipoMovimentoEstoque = new EnumTipoMovimentoEstoque(3, "Balanço");

		public codigo:number;
		public descricao:string;

		public static toSelectItem():SelectItem[] {
			return this.selects;
		}

		public static toArray():Array<EnumTipoMovimentoEstoque> {
				return this.values;
		}

		public static getByCod(codigo:number):EnumTipoMovimentoEstoque {
				let retorno:EnumTipoMovimentoEstoque = null;
				this.values.forEach(current=> {
						if (current.codigo == codigo) {
								retorno = current;
								return;
						}

				});
				return retorno;
		}

		constructor(_codigo:number, _descricao:string) {
			this.codigo = _codigo;
			this.descricao = _descricao;
			EnumTipoMovimentoEstoque.values.push(this);
			EnumTipoMovimentoEstoque.selects.push({label: _descricao, value: _codigo});
		}

}
