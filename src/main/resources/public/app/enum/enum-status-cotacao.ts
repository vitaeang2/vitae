import {SelectItem} from "primeng/primeng";

export class EnumStatusCotacao {

	private static values:Array<EnumStatusCotacao> = [];

	private static selects:SelectItem[] = [];

	public static SALVO:EnumStatusCotacao = new EnumStatusCotacao("0", "Salvo");
	public static ENVIAR:EnumStatusCotacao = new EnumStatusCotacao("1", "Enviar");
	public static ENVIADO:EnumStatusCotacao = new EnumStatusCotacao("2", "Enviado");
	public static RESPONDIDO:EnumStatusCotacao = new EnumStatusCotacao("3", "Respondido");
	public static FINALIZADO:EnumStatusCotacao = new EnumStatusCotacao("4", "Finalizado");

	public codigo:string;
	public descricao:string;

	public static toSelectItem():SelectItem[] {
		return this.selects;
	}

	public static toArray():Array<EnumStatusCotacao> {
		return this.values;
	}

	public static getByCod(codigo:string):EnumStatusCotacao {
		let retorno:EnumStatusCotacao = null;
		this.values.forEach(current=> {
			if (current.codigo == codigo) {
				retorno = current;
				return;
			}

		});
		return retorno;
	}

	constructor(_codigo:string, _descricao:string) {
		this.codigo = _codigo;
		this.descricao = _descricao;
		EnumStatusCotacao.values.push(this);
		EnumStatusCotacao.selects.push({label: _descricao, value: _codigo});
	}

}
