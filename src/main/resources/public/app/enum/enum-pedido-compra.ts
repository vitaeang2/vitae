import {SelectItem} from "primeng/primeng";

export class EnumPedidoCompra {

	private static values:Array<EnumPedidoCompra> = [];

	private static selects:SelectItem[] = [];

	public static ABERTO:EnumPedidoCompra = new EnumPedidoCompra(1, "Aberto");
	public static FINALIZADO:EnumPedidoCompra = new EnumPedidoCompra(2, "Finalizado");

	public codigo:number;
	public descricao:string;

	public static toSelectItem():SelectItem[] {
		return this.selects;
	}

	public static toArray():Array<EnumPedidoCompra> {
		return this.values;
	}

	public static getByCod(codigo:number):EnumPedidoCompra {
		let retorno:EnumPedidoCompra = null;
		this.values.forEach(current=> {
			if (current.codigo == codigo) {
				retorno = current;
				return;
			}

		});
		return retorno;
	}

	constructor(_codigo:number, _descricao:string) {
		this.codigo = _codigo;
		this.descricao = _descricao;
		EnumPedidoCompra.values.push(this);
		EnumPedidoCompra.selects.push({label: _descricao, value: _codigo});
	}

}
