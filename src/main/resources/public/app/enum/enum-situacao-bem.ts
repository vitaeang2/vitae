import {SelectItem} from "primeng/primeng";

export class EnumSituacaoBemPatrimonio {

		private static values:Array<EnumSituacaoBemPatrimonio> = [];

		private static selects:SelectItem[] = [];

		public static EM_USO:EnumSituacaoBemPatrimonio = new EnumSituacaoBemPatrimonio("1", "Em Uso (Ativo)");
		public static EM_COMODATO:EnumSituacaoBemPatrimonio = new EnumSituacaoBemPatrimonio("2", "Em Comodato (Ativo)");
		public static EM_DESUSO:EnumSituacaoBemPatrimonio = new EnumSituacaoBemPatrimonio("3", "Em Desuso (Inativo)");
		public static OBSOLETO:EnumSituacaoBemPatrimonio = new EnumSituacaoBemPatrimonio("4", "Obsoleto (Invativo)");
		public static IMPRESTAVEL:EnumSituacaoBemPatrimonio = new EnumSituacaoBemPatrimonio("5", "Imprestável (Inativo)");

		public codigo:string;
		public descricao:string;

		public static toSelectItem():SelectItem[] {
			return this.selects;
		}

		public static toArray():Array<EnumSituacaoBemPatrimonio> {
				return this.values;
		}

		public static getByCod(codigo:string):EnumSituacaoBemPatrimonio {
				let retorno:EnumSituacaoBemPatrimonio = null;
				this.values.forEach(current=> {
						if (current.codigo == codigo) {
								retorno = current;
								return;
						}

				});
				return retorno;
		}

		constructor(_codigo:string, _descricao:string) {
			this.codigo = _codigo;
			this.descricao = _descricao;
			EnumSituacaoBemPatrimonio.values.push(this);
			EnumSituacaoBemPatrimonio.selects.push({label: _descricao, value: _codigo});
		}

}
