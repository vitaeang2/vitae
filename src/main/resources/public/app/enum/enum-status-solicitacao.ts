import {SelectItem} from "primeng/primeng";

export class EnumStatusSolicitacao {

	private static values:Array<EnumStatusSolicitacao> = [];

	private static selects:SelectItem[] = [];

	public static EM_ABERTO:EnumStatusSolicitacao = new EnumStatusSolicitacao("1", "Em Aberto");
	public static FINALIZADO:EnumStatusSolicitacao = new EnumStatusSolicitacao("2", "Finalizado");
	public static EM_ANDAMENTO:EnumStatusSolicitacao = new EnumStatusSolicitacao("3", "Em Andamento");
	public static CANCELADO:EnumStatusSolicitacao = new EnumStatusSolicitacao("4", "Cancelado");

	public codigo:string;
	public descricao:string;

	public static toSelectItem():SelectItem[] {
		return this.selects;
	}

	public static toArray():Array<EnumStatusSolicitacao> {
		return this.values;
	}

	public static getByCod(codigo:string):EnumStatusSolicitacao {
		let retorno:EnumStatusSolicitacao = null;
		this.values.forEach(current=> {
			if (current.codigo == codigo) {
				retorno = current;
				return;
			}

		});
		return retorno;
	}

	constructor(_codigo:string, _descricao:string) {
		this.codigo = _codigo;
		this.descricao = _descricao;
		EnumStatusSolicitacao.values.push(this);
		EnumStatusSolicitacao.selects.push({label: _descricao, value: _codigo});
	}

}
