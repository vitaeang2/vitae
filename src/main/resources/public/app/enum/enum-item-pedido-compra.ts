import {SelectItem} from "primeng/primeng";

export class EnumItemPedidoCompra {

	private static values:Array<EnumItemPedidoCompra> = [];

	private static selects:SelectItem[] = [];

	public static ABERTO:EnumItemPedidoCompra = new EnumItemPedidoCompra(1, "Aberto");
	public static FINALIZADO_COMPRADO:EnumItemPedidoCompra = new EnumItemPedidoCompra(2, "Finalizado Comprado");
	public static FINALIZADO_SEM_COMPRAR:EnumItemPedidoCompra = new EnumItemPedidoCompra(3, "Finalizado sem Comprar");

	public codigo:number;
	public descricao:string;

	public static toSelectItem():SelectItem[] {
		return this.selects;
	}

	public static toArray():Array<EnumItemPedidoCompra> {
		return this.values;
	}

	public static getByCod(codigo:number):EnumItemPedidoCompra {
		let retorno:EnumItemPedidoCompra = null;
		this.values.forEach(current=> {
			if (current.codigo == codigo) {
				retorno = current;
				return;
			}

		});
		return retorno;
	}

	constructor(_codigo:number, _descricao:string) {
		this.codigo = _codigo;
		this.descricao = _descricao;
		EnumItemPedidoCompra.values.push(this);
		EnumItemPedidoCompra.selects.push({label: _descricao, value: _codigo});
	}

}
