import {SelectItem} from "primeng/primeng";

export class EnumTipoDocumento {

	private static values:Array<EnumTipoDocumento> = [];

	private static selects:SelectItem[] = [];

	public static SELECIONE:EnumTipoDocumento = new EnumTipoDocumento(null, "-- Selecione --");
	public static AVALIACAO:EnumTipoDocumento = new EnumTipoDocumento(0, "Avaliação");
	public static HISTORICO_DIVERSOS:EnumTipoDocumento = new EnumTipoDocumento(1, "Histórico Diversos");
	public static ADVERTENCIAS:EnumTipoDocumento = new EnumTipoDocumento(2, "Advertência");

	public codigo:number;
	public descricao:string;

	public static toSelectItem():SelectItem[] {
		return this.selects;
	}

	public static toArray():Array<EnumTipoDocumento> {
		return this.values;
	}

	public static getByCod(codigo:number):EnumTipoDocumento {
		let retorno:EnumTipoDocumento = null;
		this.values.forEach(current=> {
			if (current.codigo == codigo) {
				retorno = current;
				return;
			}

		});
		return retorno;
	}

	constructor(_codigo:number, _descricao:string) {
		this.codigo = _codigo;
		this.descricao = _descricao;
		EnumTipoDocumento.values.push(this);
		EnumTipoDocumento.selects.push({label: _descricao, value: _codigo});
	}

}
