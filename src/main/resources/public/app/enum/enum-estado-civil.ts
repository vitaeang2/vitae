
export enum EnumEstadoCivil {
	SOLTEIRO = 1,
	CASADO = 2,
	SEPARADO = 3,
	DIVORCIADO = 4,
	VIUVO = 5
}
