import {SelectItem} from "primeng/primeng";

export class EnumTipoIncorporacaoPatrimonio {

		private static values:Array<EnumTipoIncorporacaoPatrimonio> = [];

		private static selects:SelectItem[] = [];

		public static AQUISICAO:EnumTipoIncorporacaoPatrimonio = new EnumTipoIncorporacaoPatrimonio("1", "Aquisição");
		public static DOACAO:EnumTipoIncorporacaoPatrimonio = new EnumTipoIncorporacaoPatrimonio("2", "Doação");
		public static PERMUTA:EnumTipoIncorporacaoPatrimonio = new EnumTipoIncorporacaoPatrimonio("3", "Permurta");
		public static MANUFATURA:EnumTipoIncorporacaoPatrimonio = new EnumTipoIncorporacaoPatrimonio("4", "Manufatura");
		public static TRANSFERENCIA:EnumTipoIncorporacaoPatrimonio = new EnumTipoIncorporacaoPatrimonio("5", "Transferência");

		public codigo:string;
		public descricao:string;

		public static toSelectItem():SelectItem[] {
			return this.selects;
		}

		public static toArray():Array<EnumTipoIncorporacaoPatrimonio> {
				return this.values;
		}

		public static getByCod(codigo:string):EnumTipoIncorporacaoPatrimonio {
				let retorno:EnumTipoIncorporacaoPatrimonio = null;
				this.values.forEach(current=> {
						if (current.codigo == codigo) {
								retorno = current;
								return;
						}

				});
				return retorno;
		}

		constructor(_codigo:string, _descricao:string) {
			this.codigo = _codigo;
			this.descricao = _descricao;
			EnumTipoIncorporacaoPatrimonio.values.push(this);
			EnumTipoIncorporacaoPatrimonio.selects.push({label: _descricao, value: _codigo});
		}

}
