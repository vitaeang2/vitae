import {SelectItem} from "primeng/primeng";

export class EnumUnidadeMedida {

		private static values:Array<EnumUnidadeMedida> = [];

		private static selects:SelectItem[] = [];

		public static QUILOGRAMA:EnumUnidadeMedida = new EnumUnidadeMedida("1", "Quilograma");
		public static UNIDADE:EnumUnidadeMedida = new EnumUnidadeMedida("2", "Unidade");
		public static CAIXA:EnumUnidadeMedida = new EnumUnidadeMedida("3", "Caixa");
		public static METRO_LINEAR:EnumUnidadeMedida = new EnumUnidadeMedida("4", "Metro linear");
		public static METRO_QUADRADO:EnumUnidadeMedida = new EnumUnidadeMedida("5", "Metro quadrado");
		public static METRO_CUBICO:EnumUnidadeMedida = new EnumUnidadeMedida("6", "Metro cubico");
		public static PECA:EnumUnidadeMedida = new EnumUnidadeMedida("7", "Peça");

		public codigo:string;
		public descricao:string;

		public static toSelectItem():SelectItem[] {
			return this.selects;
		}

		public static toArray():Array<EnumUnidadeMedida> {
				return this.values;
		}

		public static getByCod(codigo:string):EnumUnidadeMedida {
				let retorno:EnumUnidadeMedida = null;
				this.values.forEach(current=> {
						if (current.codigo == codigo) {
								retorno = current;
								return;
						}

				});
				return retorno;
		}

		constructor(_codigo:string, _descricao:string) {
			this.codigo = _codigo;
			this.descricao = _descricao;
			EnumUnidadeMedida.values.push(this);
			EnumUnidadeMedida.selects.push({label: _descricao, value: _codigo});
		}

}
