import {Component, OnInit} from '@angular/core';
import {FORM_DIRECTIVES} from '@angular/common';
import {Dropdown, SelectItem, InputSwitch} from 'primeng/primeng';

@Component({
  selector: 'app',
  templateUrl: './app/home.html',
  directives: [Dropdown, InputSwitch]
})
export class AppTeste implements OnInit {

  marcado : boolean = true;
  cities: SelectItem[];

  selectedCity: string;

  ngOnInit(){
    this.cities = [];
    this.cities.push({label:'New York', value:'New York'});
    this.cities.push({label:'Rome', value:'Rome'});
    this.cities.push({label:'London', value:'London'});
    this.cities.push({label:'Istanbul', value:'Istanbul'});
    this.cities.push({label:'Paris', value:'Paris'});
  }

  constructor() {
    
  }
}
