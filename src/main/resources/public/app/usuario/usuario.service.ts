import {Http, HTTP_PROVIDERS, Headers} from '@angular/http'
import {Injectable} from '@angular/core'
import {Usuario} from '../usuario/usuario'
import {HeadersService} from '../aplicacao/headers.service'
import {LoginService} from '../login/login.service'
import {UrlService} from '../model/url.service'
import 'rxjs/add/operator/map'

@Injectable()
export class UsuarioService extends UrlService {

    private urlNegocio = 'usuario';

    constructor(_http: Http, _headerService: HeadersService, _loginService: LoginService) {
        super(_http, _headerService, _loginService);
    }

    public conectar(user: Usuario) {
        return super.call(`${this.urlNegocio}/logar?login=${user.login}&senha=${user.senhaMd5}`);
    }

    public alterarSenha(id: number, novaSenha: string) {
        return super.call(`${this.urlNegocio}/alterarSenha?id=${id}&novaSenha=${novaSenha}`);
    }

    public listar(page: number) {
        return super.callHeader('usuario/listar?page=' + page);
    }

    public consultarPorColaborador(idColaborador: number) {
        return super.callHeader(`usuario/consultarPorColaborador?idColaborador=${idColaborador}`);
    }

    public listarTeste() {
        return super.call('usuario/listarMenu?idPerfil=1');
    }

    public esqueciSenha(email: string) {
        return super.call(`usuario/esqueceuSenha?email=${email}`);
    }
    
    public listarCombo() {
        return super.callHeader(`usuario/listarCombo`);
    }

}
