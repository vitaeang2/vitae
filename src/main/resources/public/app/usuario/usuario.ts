import {Colaborador} from '../colaborador/colaborador'

export class Usuario {
    public id: number;
    public login: string;
    public senha: string;
    public senhaMd5: string;
    public idPerfil: number;
    public token : string;
    public colaborador: Colaborador;
    public isNew: boolean;
    public primeiroAcesso: boolean;

    constructor() { }
}