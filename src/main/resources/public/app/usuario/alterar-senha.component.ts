import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {FORM_BINDINGS, ControlGroup, FormBuilder, Validators} from '@angular/common';
import {UsuarioService} from '../usuario/usuario.service';
import {Usuario} from '../usuario/usuario';
import {MeuPanel} from '../aplicacao/panel';
import {LoginService} from '../login/login.service';
import {MensagemService} from '../aplicacao/mensagem.service';
import {Response} from '../model/response';

@Component({
	templateUrl: 'app/usuario/alterar-senha.html',
	directives: [MeuPanel],
	viewBindings: [FORM_BINDINGS]
})
export class AlterarSenhaComponent {

	alterarSenhaForm : ControlGroup;
	novaSenha : string  = '';
	repetirSenha : string = '';

	constructor(private _usuarioService : UsuarioService, private _loginService : LoginService, 
		private _mensagemService : MensagemService, private _router: Router, builder: FormBuilder){
		
		this.alterarSenhaForm = builder.group({
			novasenha: ["", Validators.required],
			repetirsenha: ["", Validators.required]
		});
	}

	salvar(){
		let usuario : Usuario = this._loginService.getUser();
		usuario.senha = this.novaSenha;		
		this.doSalvar(usuario);
	}

	private doSalvar(usuario :Usuario){
		this._usuarioService.alterarSenha(usuario.id, this.novaSenha).subscribe((resultado) => {
			this.setUsuario(resultado);
		});
	}

	private setUsuario(resultado : Response){
		if(!resultado.sucesso){
			this._mensagemService.showAtencao("Falha de conexão, não foi possivel carregar combo!");
			return;
		}
		
		this._mensagemService.showInfo("Senha alterada com sucesso!");
		this._loginService.setPrimeiroAcesso(false);
		this.limpar();
	}

	limpar(){
		this.novaSenha = "";
		this.repetirSenha = "";
	}

	voltar(){
		this._router.navigate(['/dashboard']);
	}

}
