import {DestinoEstoque} from '../destino_estoque/destino_estoque'
import {Nucleo} from '../nucleo/nucleo'
import {Empresa} from '../empresa/empresa'
import {Produto} from '../produto/produto'
import {Colaborador} from '../colaborador/colaborador'

export class MovimentoEstoque {

	id: number;

	data: string;

	tipoMovimentoEstoque: number;
	
	tipoMovimentoEstoqueStr: String;

	produto: Produto;

	quantidadeMovimentada: number;

	valor : number;

	saldo : number;

	colaborador : Colaborador;

	empresa: Empresa;

	nucleo: Nucleo;

	observacao : string;

}