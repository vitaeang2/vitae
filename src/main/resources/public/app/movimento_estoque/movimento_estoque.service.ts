import {Injectable} from '@angular/core';
import {Http, HTTP_PROVIDERS, Headers} from '@angular/http';
import {UrlService} from '../model/url.service';
import {HeadersService} from '../aplicacao/headers.service';
import {LoginService} from '../login/login.service';
import {PaginacaoParams} from '../model/paginacao_params';
import {MovimentoEstoque} from './movimento_estoque';
import {PedidoCompra} from '../pedido_compra/pedido_compra';
import {ItemPedidoCompra} from '../pedido_compra/item_pedido_compra';

@Injectable()
export class MovimentoEstoqueService extends UrlService {

	private urlNegocio : string = 'movimentoEstoque';

	constructor(_http: Http, _headerService: HeadersService, _loginService: LoginService) {
		super(_http, _headerService, _loginService);
	}

	public listarMovimentos(produtoId: number, empresaId: number, nucleoId: number) {
		return super.callHeader(`${this.urlNegocio}/listarMovimentos?produtoId=${JSON.stringify(produtoId)}&empresaId=${JSON.stringify(empresaId)}&nucleoId=${JSON.stringify(nucleoId)}`);
	}
	
	public consultarProdutoEstoque(produtoId: number, empresaId: number, nucleoId: number) {
		return super.callHeader(`${this.urlNegocio}/consultarProdutoEstoque?produtoId=${JSON.stringify(produtoId)}&empresaId=${JSON.stringify(empresaId)}&nucleoId=${JSON.stringify(nucleoId)}`);
	}
	
	public salvar(movimentoEstoque: MovimentoEstoque) {
		return super.callHeaderBody(`${this.urlNegocio}/salvar`, JSON.stringify(movimentoEstoque));
	}

	public entrarEstoque(idPedidoCompra: number, item: ItemPedidoCompra) {
		return super.callHeaderParams(`${this.urlNegocio}/entrarEstoque?idPedidoCompra=${idPedidoCompra}`, JSON.stringify(item));
	}
}