import {Component, OnInit} from '@angular/core';
import {OnActivate, RouteSegment, Router} from '@angular/router';
import {DataTable, Column, SelectItem, Dialog, Dropdown, Fieldset} from 'primeng/primeng';
import {PaginacaoParams} from '../model/paginacao_params';
import {Paginacao} from '../model/paginacao';
import {Response} from '../model/response';
import {LoginService} from "../login/login.service";
import {VitaeLazy} from '../aplicacao/vitae-lazy.service';
import {EnumTipoMovimentoEstoque} from '../enum/enum-tipo-movimento-estoque';
import {EnumUnidadeMedida} from '../enum/enum-unidade-medida';
import {CustomInput} from '../componentes/custom-input.component';
import {Empresa} from '../empresa/empresa';
import {EmpresaService} from '../empresa/empresa.service';
import {NucleoService} from '../nucleo/nucleo.service';
import {MovimentoEstoqueService} from '../movimento_estoque/movimento_estoque.service';
import {MensagemService} from "../aplicacao/mensagem.service";
import {Nucleo} from '../nucleo/nucleo';
import {MovimentoEstoque} from './movimento_estoque';
import {Produto} from '../produto/produto';
import {ProdutoService} from '../produto/produto.service';
import {ProdutoEstoque} from '../produto/produto_estoque';
import {Colaborador} from '../colaborador/colaborador';
import {MoneyPipe} from '../componentes/money_pipe';
import {DoublePipe} from '../componentes/double_pipe';

@Component({
	templateUrl: './app/movimento_estoque/movimento_estoque.html',
	directives: [DataTable, Dialog, Dropdown, CustomInput, Fieldset],
	providers:[EmpresaService, NucleoService, MovimentoEstoqueService, ProdutoService],
	pipes: [MoneyPipe, DoublePipe]
})

export class MovimentoEstoqueComponent extends VitaeLazy implements OnActivate, OnInit {

	saldoTotal : number; 

	idMovimento : number;

	idDestino : number;

	movimento : MovimentoEstoque;
	
	listaMovimentos : Array<MovimentoEstoque>;
	
	listaTipoMovimento : SelectItem[];
	
	listaDestino : SelectItem[];

	mostrarDialogLancamento : boolean = false;
	
	mostrarDialogDestino : boolean = false;

	listaNucleo : SelectItem[];

	listaEmpresa : SelectItem[];
	
	idProduto : number;
	
	idNucleoSelecionado : number;

	idNucleo : number = 0;

	produto : Produto;

	empresa : Empresa;

	produtoEstoque : ProdutoEstoque;

	constructor(private _produtoService: ProdutoService, private _movimentoEstoqueService: MovimentoEstoqueService, 
		private _loginService : LoginService, private _nucleoService: NucleoService, private _empresaService: EmpresaService, 
		private _mensagemService : MensagemService, private router: Router){

		super(router);

		this.idNucleo = 0;
		this.carregarListaNucleo();
		this.produto = new Produto();
		this.empresa = new Empresa();
		this.produtoEstoque = new ProdutoEstoque();
		this.produtoEstoque.quantidade = 0;
		this.movimento = new MovimentoEstoque();
		this.listaMovimentos = new Array<MovimentoEstoque>();
		this.movimento.tipoMovimentoEstoque = EnumTipoMovimentoEstoque.ENTRADA.codigo;
	}

	routerOnActivate(curr: RouteSegment) {

		this.idProduto = <number> +curr.getParam('id');
		//this.carregarListaMovimento();
		this.consultarProduto(this.idProduto);
		this.consultarEmpresa();
	}

	ngOnInit() {

	}

	novo() {

		//super.novo('');
	}

	editar(id: number) {

		//super.editar(id, '');
	}

	
	showDialogLancamento() {
		this.listaTipoMovimento = EnumTipoMovimentoEstoque.toSelectItem();
		this.movimento.tipoMovimentoEstoque = null;
		this.mostrarDialogLancamento = true;
	}

	closeDialogLancamento() {

		this.mostrarDialogLancamento = false;
		this.movimento = new MovimentoEstoque();
	}

	showDialogDestino() {

		this.mostrarDialogDestino = true;
	}

	closeDialogDestino() {

		this.mostrarDialogDestino = false;
	}

	doListar() {

	}

	voltar() {

		super.goBack('/produto');
	}

	carregarListaMovimentoPorNucleo(){
		let idEmpresa = this._loginService.getUser().colaborador.empresa.id;
		let idNucleo = this.idNucleo;

		//this.listaTipoMovimento = EnumTipoMovimentoEstoque.toSelectItem();

		this._movimentoEstoqueService.listarMovimentos(this.idProduto, idEmpresa, idNucleo).subscribe((resultado) => {
			this.setListaMovimento(resultado)
		});

	}


	listarMovimentos(){
		let idNucleoNumber = 0;
		if(this.idNucleo){
			let idNucleoNumber = parseInt(this.idNucleo.toString()); 
		}

		this._movimentoEstoqueService.listarMovimentos(this.idProduto, this.empresa.id, idNucleoNumber).subscribe((resultado) => {
			//this.setProdutoEstoque(resultado);
			this.setListaMovimento(resultado)
		});

	}

	consultarProdutoEstoque(){

		let idNucleoNumber = parseInt(this.idNucleo.toString()); 

		this._movimentoEstoqueService.consultarProdutoEstoque(this.idProduto, this.empresa.id, idNucleoNumber).subscribe((resultado) => {
			this.setProdutoEstoque(resultado)
		});
	}

	salvar(){
		if(!this.movimento.tipoMovimentoEstoque || this.movimento.tipoMovimentoEstoque == 0){
			this._mensagemService.showErro("É necessário selecionar o Tipo de Movimento!");
			return;
		}

		if(!this.idNucleo || this.idNucleo == 0){
			this._mensagemService.showErro("É necessário selecionar o núcleo!");
			return;
		}

		if(!this.movimento.quantidadeMovimentada || this.movimento.quantidadeMovimentada == 0){
			this._mensagemService.showErro("Quantidade deve ser maior que 0!");
			return;
		}

		this.doSalvar();
	}

	private setProdutoEstoque(resultado: Response){
		if(!resultado.sucesso){
			this._mensagemService.showErro("Não foi possivel retornar Produto Estoque!");
			this.produtoEstoque = new ProdutoEstoque();
		}
		
		this.produtoEstoque = resultado.objetoRetorno;

		if(!this.produtoEstoque){
			this.produtoEstoque = new ProdutoEstoque();
			return;
		}

	}

	private consultarEmpresa(){
		let idEmpresa = this._loginService.getUser().colaborador.empresa.id;
		this._empresaService.consultarId(idEmpresa).subscribe((resultado) => {
			this.setEmpresa(resultado);
		})
	}

	private setEmpresa(resultado: Response){
		this.empresa = resultado.objetoRetorno;

		this.listarNucleoPorEmpresa(this.empresa.id);
		this.listarMovimentos();
	}

	private listarNucleoPorEmpresa(idEmpresa : number){
		if(idEmpresa > 0){
			this._nucleoService.listarComboPorIdEmpresa(idEmpresa).subscribe((resultado) => {
				this.setListaComboNucleo(resultado)
			},(error) => {
				console.log(error)
			});
		}else{
			this.listaNucleo = [];
		}
	}

	private setListaComboNucleo(resultado : Response){
		if(!resultado.sucesso){
			this._mensagemService.showAtencao("Falha de conexão, não foi possivel carregar combo!");
			return;
		}
		this.listaNucleo = [];
		this.listaNucleo.push({label: '-- Nenhum --', value: 0})
		let listaRetorno : Array<Nucleo> = resultado.objetoRetorno;
		listaRetorno.forEach(nuc => this.listaNucleo.push({label: nuc.nome, value: nuc.id}))
    	//this.idNucleo = this.colaborador.nucleo.id;
    }


    private consultarProduto(idProduto : number){
    	this._produtoService.consultarId(idProduto).subscribe((resultado) => {
    		this.setProduto(resultado)
    	})
    }

    private setProduto(resultado : Response){
    	this.produto = resultado.objetoRetorno;
    	this.produto.unidadeMedidaStr = EnumUnidadeMedida.getByCod(this.produto.unidadeMedida.toString()).descricao;
    }

    private carregarListaMovimento() {  
    	let idEmpresa = this._loginService.getUser().colaborador.empresa.id;
    	let nucleo = this._loginService.getUser().colaborador.nucleo;
    	let idNucleo = null;

    	if(nucleo && nucleo.id){
    		idNucleo = nucleo.id;
    	}

    	this.listaTipoMovimento = EnumTipoMovimentoEstoque.toSelectItem();

    	this._movimentoEstoqueService.listarMovimentos(this.idProduto, idEmpresa, idNucleo).subscribe((resultado) => {
    		this.setListaMovimento(resultado)
    	});

    }
    
    private setListaMovimento(resultado : Response){
    	this.listaMovimentos = resultado.objetoRetorno;
    	this.saldoTotal = 0;
    	this.listaMovimentos.forEach(p => {
    		if(p.tipoMovimentoEstoque == EnumTipoMovimentoEstoque.SAIDA.codigo){
    			this.saldoTotal -= p.quantidadeMovimentada; 
    		}else{
    			this.saldoTotal += p.quantidadeMovimentada; 
    		}
    	})
    }

    private carregarListaNucleo(){
    	this._nucleoService.listarComboPorIdEmpresa(this._loginService.getUser().colaborador.empresa.id).subscribe((resultado) => {
    		this.setListaNucleo(resultado)
    	});
    }

    private setListaNucleo(resultado : Response){
    	if(!resultado.sucesso){
    		this._mensagemService.showAtencao(resultado.mensagem);
    		return;
    	}
    	this.listaNucleo = [];
    	let lista : Array<Nucleo> = resultado.objetoRetorno;
       // this.listaNucleo.push({label: "-- TODOS --", value: 0});
       lista.forEach(nuc => this.listaNucleo.push({label: nuc.nome, value: nuc.id}));
   }

   private montarMovimento (){
   	this.movimento.empresa = new Empresa();
   	this.movimento.empresa.id = this.empresa.id;
   	if(this.idNucleo != 0){
   		this.movimento.nucleo = new Nucleo();
   		this.movimento.nucleo.id = this.idNucleo;
   	}
   	this.movimento.colaborador = new Colaborador();
   	this.movimento.colaborador.id = this._loginService.getUser().colaborador.id;
   	
   	this.movimento.produto = new Produto();
   	this.movimento.produto.id = this.idProduto;
   	if(!this.movimento.tipoMovimentoEstoque){
   		this.movimento.tipoMovimentoEstoque = EnumTipoMovimentoEstoque.ENTRADA.codigo
   	}
   }

   private doSalvar(){
   	this.montarMovimento();
   	this._movimentoEstoqueService.salvar(this.movimento).subscribe((resultado) => {
   		this.setSalvo()
   	})
   }

   private setSalvo(){
   	this._mensagemService.showInfo("Salvo com sucesso!")
   	//this.carregarListaMovimento();
   	this.closeDialogLancamento();
   	this.consultarProduto(this.idProduto);
   	this.consultarEmpresa();
   	//this.idNucleo = 0;
   }

}