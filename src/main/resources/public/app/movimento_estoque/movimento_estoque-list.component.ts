import {Component, OnInit} from '@angular/core'
import {Router} from '@angular/router';
import {DataTable, Column, SelectItem, Dialog, Dropdown} from 'primeng/primeng';
import {PaginacaoParams} from '../model/paginacao_params';
import {Paginacao} from '../model/paginacao';
import {Response} from '../model/response';
import {LoginService} from "../login/login.service"
import {VitaeLazy} from '../aplicacao/vitae-lazy.service';
import {EnumTipoMovimentoEstoque} from '../enum/enum-tipo-movimento-estoque';
import {CustomInput} from '../componentes/custom-input.component';

@Component({
	templateUrl: './app/movimento_estoque/movimento_estoque-list.html',
	directives: [DataTable, Dialog, Dropdown, CustomInput]
})
export class MovimentoEstoqueListComponent extends VitaeLazy implements OnInit {

	idMovimento : number;

	idDestino : number;

	listaMovimento : SelectItem[];
	
	listaDestino : SelectItem[];

	mostrarDialogLancamento : boolean = false;
	
	mostrarDialogDestino : boolean = false;

	constructor(private _loginService : LoginService, private router: Router){
		super(router);
	}

	ngOnInit(){
		this.carregarListaMovimento();
	}

	novo() { 
		//super.novo('');
	}

	editar(id: number){
		//super.editar(id, '');
	}

	carregarListaMovimento(){
		this.listaMovimento = EnumTipoMovimentoEstoque.toSelectItem();
	}

	showDialogLancamento(){
		this.mostrarDialogLancamento = true;
	}

	closeDialogLancamento(){
		this.mostrarDialogLancamento = false;
	}

	showDialogDestino(){
		this.mostrarDialogDestino = true;
	}

	closeDialogDestino(){
		this.mostrarDialogDestino = false;
	}

	doListar(){
	}
}