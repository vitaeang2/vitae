import {Injectable} from '@angular/core';
import {Http, HTTP_PROVIDERS, Headers} from '@angular/http'
import {UrlService} from '../model/url.service';
import {HeadersService} from '../aplicacao/headers.service'
import {LoginService} from '../login/login.service';

@Injectable()
export class NumeroBancoService  extends UrlService {

	private urlNegocio = 'numeroBanco';

	constructor(_http: Http, _headerService: HeadersService, _loginService: LoginService) {
		super(_http, _headerService, _loginService);
	}

	public listar(nome?) {
		if(nome){
			return this.consultarPorNome(nome);
		}
		return super.callHeader(`${this.urlNegocio}/listar`);
	}

	public consultarPorNome(nome: string) {
		return super.callHeader(`${this.urlNegocio}/consultarPorNome?nome=${nome}`);
	}

}