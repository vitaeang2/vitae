import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {DataTable, Column, LazyLoadEvent, Dropdown, SelectItem, Dialog} from 'primeng/primeng';
import {MaterialDesignDirective} from "../aplicacao/material";
import {ControleSolicitacao} from "./controle_solicitacao";
import {ControleSolicitacaoService} from "./controle_solicitacao.service"
import {Nucleo} from "../nucleo/nucleo";
import {NucleoService} from '../nucleo/nucleo.service';
import {LocalService} from '../local/local.service';
import {Local} from "../local/local";
import {EnumStatusSolicitacao} from '../enum/enum-status-solicitacao';
import {MensagemService} from "../aplicacao/mensagem.service"
import {LoginService} from "../login/login.service"
import {PaginacaoParams} from '../model/paginacao_params';
import {Paginacao} from '../model/paginacao';
import {Response} from '../model/response';
import {VitaeLazy} from '../aplicacao/vitae-lazy.service';

@Component({
    selector: 'controle_solicitacao-list',
    templateUrl: './app/controle_solicitacao/controle_solicitacao-list.html',
    directives: [DataTable, Column, Dropdown, Dialog],
    providers: [ControleSolicitacaoService, NucleoService, LocalService]
})
export class ControleSolicitacaoListComponent extends VitaeLazy  implements OnInit {

    params : PaginacaoParams;
    paginacao : Paginacao = new Paginacao();

    listaNucleo : SelectItem[];
    idNucleoSelecionado : number = 0;

    listaLocal : SelectItem[];
    idLocalSelecionado : number = 0;

    listaStatusSolicitacao : SelectItem[];
    idStatusSolicitacaoSelecionado : number = 0;

    private idSolicitacaoSelecionado: number = 0;

    private idControleSolicitacaoSelecionado: number = 0;

    public displayDialogExcluir: boolean = false;

    constructor(private _nucleoService: NucleoService, private _localService: LocalService, private _controle_solicitacaoService : ControleSolicitacaoService, 
        private _loginService : LoginService, private _mensagemService : MensagemService, private router: Router){
        super(router);
    }

    ngOnInit(){

        this.doListar();
        this.doListarComboNucleo();
        this.doListarComboLocal();
        this.carregarlistarStatusSolicitacao();
    }

    novo () {
        super.novo('/controle_solicitacao/');
    }

    editar(id: number){

        super.editar(id, '/controle_solicitacao/');
    }

    doListar () {
        if(this.params){

            this.listar();
        }
        
    }

    private listar(){
        this._controle_solicitacaoService.listar(this.params).subscribe((resultado) => {

            this.setListaControleSolicitacao(resultado)
        });
    }

    private setListaControleSolicitacao(resultado : Response){

        if(!resultado.sucesso){

            this._mensagemService.showAtencao(resultado.mensagem);
            return;
        }

        this.paginacao = resultado.objetoRetorno;
    }

    confirmDiolgExcluir(id:number) {

        this.idControleSolicitacaoSelecionado = id;
        this.displayDialogExcluir = true;
    }

    excluir() { 

        this._controle_solicitacaoService.excluirId(this.idControleSolicitacaoSelecionado).subscribe((resultado) => {

            this._mensagemService.showInfo("ControleSolicitacao excluído com sucesso!");
            this.displayDialogExcluir = false;
            this.doListar();
        });

    }

    private doListarComboNucleo(){

        this._nucleoService.listarCombo().subscribe((resultado) => {

            this.setListaComboNucleo(resultado)
        });
    }

    private setListaComboNucleo(resultado : Response){

        if(!resultado.sucesso){
            this._mensagemService.showAtencao("Falha de conexão, não foi possivel carregar combo!");
            return;
        }

        this.listaNucleo = [];
        this.listaNucleo.push({label: '-- TODOS --', value: 0})
        let listaRetorno : Array<Nucleo> = resultado.objetoRetorno;
        listaRetorno.forEach(nuc => this.listaNucleo.push({label: nuc.nomeEmpresa + ' - ' + nuc.nome, value: nuc.id}))

    }

    listarPorFiltros(idNucleo: number, idLocal: number, idStatusSolicitacao: number ){

        this._controle_solicitacaoService.listarPorFiltros(this.params, idNucleo, idLocal, idStatusSolicitacao).subscribe((resultado) => {

            this.setListaControleSolicitacao(resultado)
        });
    }

    private doListarComboLocal(){
        this._localService.listarCombo().subscribe((resultado) => {
            this.setListaComboLocal(resultado);
        }) ;
    }

    private setListaComboLocal(resultado : Response){
        if(!resultado.sucesso){
            this._mensagemService.showAtencao("Falha de conexão, não foi possivel carregar combo!");
            return;
        }
        this.listaLocal = [];
        this.listaLocal.push({label: '-- TODOS --', value: 0});
        let listaRetorno : Array<Local> = resultado.objetoRetorno;
        listaRetorno.forEach(l => this.listaLocal.push({label: l.nome, value: l.id}) );

    }

    private carregarlistarStatusSolicitacao(){

        this.listaStatusSolicitacao = [];
        let lista : Array<EnumStatusSolicitacao> = EnumStatusSolicitacao.toArray();
        this.listaStatusSolicitacao.push({label: "-- TODOS --", value: 0});
        lista.forEach(s => this.listaStatusSolicitacao.push({label: s.descricao, value: s.codigo}));
        
    }
}
