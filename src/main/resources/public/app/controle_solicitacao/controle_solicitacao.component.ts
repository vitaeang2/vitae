import {Component} from '@angular/core';
import {Routes, ROUTER_DIRECTIVES, Router} from '@angular/router';
import {ControleSolicitacaoListComponent} from './controle_solicitacao-list.component';
import {ControleSolicitacaoDetailComponent} from "./controle_solicitacao-detail.component";

@Component({
	selector: 'controle_solicitacao',
	templateUrl: 'app/controle_solicitacao/controle_solicitacao.html',
	directives: [ROUTER_DIRECTIVES]
})
@Routes([
	{ path: "/", component: ControleSolicitacaoListComponent},
	{ path: ":id", component: ControleSolicitacaoDetailComponent}
	])
export class ControleSolicitacaoComponent  {
	constructor(private _router : Router) {
		this._router.navigate(['/controle_solicitacao'])
	}
}
