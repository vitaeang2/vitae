import {Injectable} from '@angular/core';
import {Http, HTTP_PROVIDERS, Headers} from '@angular/http'
import {UrlService} from '../model/url.service';
import {HeadersService} from '../aplicacao/headers.service'
import {LoginService} from '../login/login.service';
import {PaginacaoParams} from '../model/paginacao_params';
import {ControleSolicitacao} from '../controle_solicitacao/controle_solicitacao';

@Injectable()
export class ControleSolicitacaoService  extends UrlService {

	constructor(_http: Http, _headerService: HeadersService, _loginService: LoginService) {
		super(_http, _headerService, _loginService);
	}

	public listar(params: PaginacaoParams) {
		return super.callHeader(`controle_solicitacao/listar?params=${JSON.stringify(params)}`);
	}
/*	public listar(params: PaginacaoParams, idEmpresa: number) {
		return super.callHeader(`controle_solicitacao/listar?params=${JSON.stringify(params)}&idEmpresa=${idEmpresa}`);
	}*/

	public listarPorNucleo(params: PaginacaoParams, idEmpresa: number, idNucleo?: number) {
		return super.callHeader(`controle_solicitacao/listar?params=${JSON.stringify(params)}&idEmpresa=${idEmpresa}&idNucleo=${idNucleo}`);
	}
	
	public listarCombo() {
		return super.callHeader(`controle_solicitacao/listarCombo`);
	}

	public consultarId(id: number) {
		return super.callHeader(`controle_solicitacao/consultarId?id=${id}`);
	}

	public salvar(controle_solicitacao: ControleSolicitacao) {
		return super.callHeaderBody(`controle_solicitacao/salvar`, JSON.stringify(controle_solicitacao));
	}

	public excluirId(id: number) {

		return super.callHeader(`controle_solicitacao/excluirId?id=${id}`);
	}
	
	public listarPorFiltros(params: PaginacaoParams, idNucleo?: number, idLocal?: number, idStatusSolicitacao?: number) {
		return super.callHeader(`controle_solicitacao/listar?params=${JSON.stringify(params)}&idNucleo=${idNucleo}&idLocal=${idLocal}&idStatusSolicitacao=${idStatusSolicitacao}`);
	}
}