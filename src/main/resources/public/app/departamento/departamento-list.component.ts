import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {DataTable, Column, LazyLoadEvent, Button,Dialog} from 'primeng/primeng';
import {MaterialDesignDirective} from "../aplicacao/material";
import {Departamento} from "./departamento";
import {DepartamentoService} from "./departamento.service"
import {MensagemService} from "../aplicacao/mensagem.service"
import {LoginService} from "../login/login.service"
import {PaginacaoParams} from '../model/paginacao_params';
import {Paginacao} from '../model/paginacao';
import {Response} from '../model/response';

@Component({
    selector: 'departamento-list',
    templateUrl: './app/departamento/departamento-list.html',
    directives: [DataTable, Column, Button, Dialog],
    providers: [DepartamentoService]
})
export class DepartamentoListComponent implements OnInit {

    params: PaginacaoParams;
    paginacao: Paginacao = new Paginacao();

    private itemSelecionadoId: number = 0;

    public displayDialogExcluir: boolean = false;

    public displayDialogExcluirDepartamentoViculoPatrimonio: boolean = false;

    constructor(private _departamentoService: DepartamentoService, private _loginService: LoginService, private _mensagemService: MensagemService, private _router: Router) {
    }

    ngOnInit() {
        this.doListar();
    }

    novo() {
        this._router.navigate(['/departamento/', -1])
    }

    editar(id: number) {
        this._router.navigate(['/departamento/', id])
    }

    load(event: LazyLoadEvent) {

        this.params = new PaginacaoParams();

        //Calcula a página atual
        let page = 0;
        if (event.first > 0) {
            page = event.first / event.rows;
        }

        this.params.page = page;
        this.params.size = event.rows;
        this.params.sortField = event.sortField;
        this.params.sortOrder = (event.sortOrder == -1) ? 'desc' : 'asc';

        this.doListar();
    }

    private doListar() {
        if (this.params) {
            this._departamentoService.listar(this.params).subscribe((resultado) => {
                this.setListaDepartamento(resultado)
            });
        }
    }

    private setListaDepartamento(resultado: Response) {
        if (!resultado.sucesso) {
            this._mensagemService.showAtencao(resultado.mensagem);
            return;
        }
        this.paginacao = resultado.objetoRetorno;
    }

    confirmDiolgExcluir(id: number) {

        this.itemSelecionadoId = id;
        this.displayDialogExcluir = true;
    }

    excluir(verificarDepartamentoViculadaPatrimonio: boolean) {

        this._departamentoService.excluirId(this.itemSelecionadoId, verificarDepartamentoViculadaPatrimonio).subscribe((resultado: Response) => {

            if (!resultado.sucesso) {
                this.displayDialogExcluirDepartamentoViculoPatrimonio = true;
            } else {
                this._mensagemService.showInfo("Departamento excluído com sucesso!");
                this.displayDialogExcluirDepartamentoViculoPatrimonio = false;
                this.doListar();
            }
            this.displayDialogExcluir = false;              
        });

    }
}
