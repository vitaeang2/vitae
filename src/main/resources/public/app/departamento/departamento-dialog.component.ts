import {Component, OnInit} from '@angular/core';
import {Departamento} from './departamento';
import {DepartamentoService} from './departamento.service';
import {MeuPanel} from '../aplicacao/panel';
import {MensagemService} from '../aplicacao/mensagem.service';
import {Response} from '../model/response';

import {TimerWrapper} from '@angular/core/src/facade/async';

@Component({
	selector: 'departamento-dialog',
	templateUrl: './app/departamento/departamento-dialog.html',
	directives: [MeuPanel],
	providers: [DepartamentoService]
})
export class DepartamentoDialogComponent implements OnInit{
	
	departamento : Departamento; 

	isSucesso: boolean;

	isResposta: boolean;

	mensagem: string;

	constructor(private _departamentoServico : DepartamentoService, private _mensagemService : MensagemService){}

	ngOnInit() {
		this.departamento = new Departamento();
	}

	salvar(){
		this.doSalvar();
	}

	limpar(){
		this.departamento = new Departamento();
	}

	private doSalvar(){
		this._departamentoServico.salvar(this.departamento).subscribe((resultado) => {
			
			if(resultado.sucesso){
				this._mensagemService.showInfo("Departamento cadastrado com sucesso!");
				this.setDepartamento(resultado);				
			} else {
				this._mensagemService.showInfo(resultado.mensagem);
			}
		});
	}

	private setDepartamento(resultado : Response){
		this.isResposta = true;
		if(!resultado.sucesso){
			this.mensagem = resultado.mensagem;
			return;
		}

		this.isSucesso = true;
		this.mensagem = "Departamento salvo com sucesso!";

		this._mensagemService.showInfo("Departamento salvo com sucesso!");

		TimerWrapper.setTimeout(() => {  
			this.isResposta = false;
			this.isSucesso = false;
			this.mensagem = "";
			this.departamento = new Departamento();
		}, 2000);
	}
}