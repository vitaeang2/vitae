import {Component} from '@angular/core';
import {OnActivate, RouteSegment, Router} from '@angular/router';
import {SelectItem} from 'primeng/primeng';
import {DepartamentoService} from './departamento.service'
import {Departamento} from '../departamento/departamento'
import {MensagemService} from '../aplicacao/mensagem.service';
import {Response} from '../model/response'
import {MeuPanel} from '../aplicacao/panel';
import {EnumEstadosBrasileiros} from '../enum/enum-estados-brasileiros';

@Component({
	selector: 'departamento-detail',
	templateUrl: './app/departamento/departamento-detail.html',
	providers: [DepartamentoService],
	directives: [MeuPanel]
})
export class DepartamentoDetailComponent implements OnActivate {

	departamento : Departamento = new Departamento();

	constructor(private _departamentoService: DepartamentoService, private _mensagemService : MensagemService, private _router: Router){
	}

	routerOnActivate(curr: RouteSegment) {
		let id = <number> +curr.getParam('id');

		if(id != -1){
			this.recuperarDepartamento(id);
		}else{
			this.limpar();
		}
	}

	salvar(){

		this.doSalvar();

	}

	limpar(){
		this.departamento = new Departamento();
	}

	voltar(){
		this._router.navigate(['/departamento']);
	}

	private recuperarDepartamento(id:number){
		this._departamentoService.consultarId(id).subscribe((resultado) => {
			this.setDepartamento(resultado)
		},(error) => {
			console.log(error)
		});
	}

	private setDepartamento(resultado : Response){
		this.departamento =  <Departamento>resultado.objetoRetorno;
	}

	private doSalvar(){
		this._departamentoService.salvar(this.departamento).subscribe((resultado) => {
			
			if(resultado.sucesso){
				this._mensagemService.showInfo("Departamento cadastrado com sucesso!");
				this.voltar();				
			} else {
				this._mensagemService.showInfo(resultado.mensagem);
			}
		});
	}
}
