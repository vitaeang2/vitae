import {Component} from '@angular/core';
import {Routes, ROUTER_DIRECTIVES, Router} from '@angular/router';
import {DepartamentoListComponent} from './departamento-list.component';
import {DepartamentoDetailComponent} from "./departamento-detail.component";

@Component({
	selector: 'departamento',
	templateUrl: 'app/departamento/departamento.html',
	directives: [ROUTER_DIRECTIVES]
})
@Routes([
	{ path: "/", component: DepartamentoListComponent},
	{ path: ":id", component: DepartamentoDetailComponent}
	])

export class DepartamentoComponent  {
	constructor(private _router : Router) {
		this._router.navigate(['/departamento'])
	}
}
