-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: vitae
-- ------------------------------------------------------
-- Server version	5.7.11-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `anotacao_colaborador`
--

DROP TABLE IF EXISTS `anotacao_colaborador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anotacao_colaborador` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `data` datetime DEFAULT NULL,
  `historico` varchar(255) DEFAULT NULL,
  `valor` decimal(19,2) DEFAULT NULL,
  `colaborador_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_ebborr1rseq5qa64bkiq58p4d` (`colaborador_id`),
  CONSTRAINT `FK_ebborr1rseq5qa64bkiq58p4d` FOREIGN KEY (`colaborador_id`) REFERENCES `colaborador` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anotacao_colaborador`
--

LOCK TABLES `anotacao_colaborador` WRITE;
/*!40000 ALTER TABLE `anotacao_colaborador` DISABLE KEYS */;
/*!40000 ALTER TABLE `anotacao_colaborador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aplicao`
--

DROP TABLE IF EXISTS `aplicao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aplicao` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aplicao`
--

LOCK TABLES `aplicao` WRITE;
/*!40000 ALTER TABLE `aplicao` DISABLE KEYS */;
/*!40000 ALTER TABLE `aplicao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banco`
--

DROP TABLE IF EXISTS `banco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banco` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `codigo_banco` varchar(10) DEFAULT NULL,
  `nome` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banco`
--

LOCK TABLES `banco` WRITE;
/*!40000 ALTER TABLE `banco` DISABLE KEYS */;
/*!40000 ALTER TABLE `banco` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cargo`
--

DROP TABLE IF EXISTS `cargo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cargo` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `status` bit(1) DEFAULT NULL,
  `nome` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cargo`
--

LOCK TABLES `cargo` WRITE;
/*!40000 ALTER TABLE `cargo` DISABLE KEYS */;
INSERT INTO `cargo` VALUES (1,'','ADMINISTRADOR');
/*!40000 ALTER TABLE `cargo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categoria_grupo`
--

DROP TABLE IF EXISTS `categoria_grupo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categoria_grupo` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `status` bit(1) DEFAULT NULL,
  `nome` varchar(100) NOT NULL,
  `grupo_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_6f51mu5nqado0lapivt43cht9` (`grupo_id`),
  CONSTRAINT `FK_6f51mu5nqado0lapivt43cht9` FOREIGN KEY (`grupo_id`) REFERENCES `grupo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categoria_grupo`
--

LOCK TABLES `categoria_grupo` WRITE;
/*!40000 ALTER TABLE `categoria_grupo` DISABLE KEYS */;
/*!40000 ALTER TABLE `categoria_grupo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categoria_produto`
--

DROP TABLE IF EXISTS `categoria_produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categoria_produto` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `status` bit(1) DEFAULT NULL,
  `nome` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categoria_produto`
--

LOCK TABLES `categoria_produto` WRITE;
/*!40000 ALTER TABLE `categoria_produto` DISABLE KEYS */;
/*!40000 ALTER TABLE `categoria_produto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `colaborador`
--

DROP TABLE IF EXISTS `colaborador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `colaborador` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `data_admissao` datetime DEFAULT NULL,
  `ativo` bit(1) DEFAULT NULL,
  `data_demissao` datetime DEFAULT NULL,
  `tipo_arquivo` varchar(255) DEFAULT NULL,
  `cargo_id` bigint(20) DEFAULT NULL,
  `empresa_id` bigint(20) DEFAULT NULL,
  `nucleo_id` bigint(20) DEFAULT NULL,
  `pessoa_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_6erg05jatrfpc4yl2ev732t7g` (`pessoa_id`),
  KEY `FK_fviywtyddkyai8u1extbauq7r` (`cargo_id`),
  KEY `FK_tc6xgqamipldr33l26y9borfg` (`empresa_id`),
  KEY `FK_q4d2npmh9n47k73fxlvgutilq` (`nucleo_id`),
  CONSTRAINT `FK_6erg05jatrfpc4yl2ev732t7g` FOREIGN KEY (`pessoa_id`) REFERENCES `pessoa` (`id`),
  CONSTRAINT `FK_fviywtyddkyai8u1extbauq7r` FOREIGN KEY (`cargo_id`) REFERENCES `cargo` (`id`),
  CONSTRAINT `FK_q4d2npmh9n47k73fxlvgutilq` FOREIGN KEY (`nucleo_id`) REFERENCES `nucleo` (`id`),
  CONSTRAINT `FK_tc6xgqamipldr33l26y9borfg` FOREIGN KEY (`empresa_id`) REFERENCES `empresa` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `colaborador`
--

LOCK TABLES `colaborador` WRITE;
/*!40000 ALTER TABLE `colaborador` DISABLE KEYS */;
INSERT INTO `colaborador` VALUES (1,'2017-02-27 00:00:00','','2017-02-28 00:00:00',NULL,1,1,NULL,2);
/*!40000 ALTER TABLE `colaborador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `componente_item_patrimonio`
--

DROP TABLE IF EXISTS `componente_item_patrimonio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `componente_item_patrimonio` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `status` bit(1) DEFAULT NULL,
  `descricao` varchar(255) NOT NULL,
  `descricao_garantia` varchar(255) DEFAULT NULL,
  `fim_garantia` datetime DEFAULT NULL,
  `numero_serie` varchar(100) DEFAULT NULL,
  `observacao` varchar(255) DEFAULT NULL,
  `patrimonio_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_t3oo5h5u2qtsmds8dkfddae5f` (`patrimonio_id`),
  CONSTRAINT `FK_t3oo5h5u2qtsmds8dkfddae5f` FOREIGN KEY (`patrimonio_id`) REFERENCES `patrimonio` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `componente_item_patrimonio`
--

LOCK TABLES `componente_item_patrimonio` WRITE;
/*!40000 ALTER TABLE `componente_item_patrimonio` DISABLE KEYS */;
/*!40000 ALTER TABLE `componente_item_patrimonio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cotacao`
--

DROP TABLE IF EXISTS `cotacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cotacao` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `data_cotacao` datetime DEFAULT NULL,
  `data_validade` datetime DEFAULT NULL,
  `descricao` varchar(255) NOT NULL,
  `status_cotacao` int(11) DEFAULT NULL,
  `empresa_id` bigint(20) DEFAULT NULL,
  `nucleo_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_4n2luc1f32rkai9fgxgtni9io` (`empresa_id`),
  KEY `FK_etldmfqhtr1jhh2cgrtw9t866` (`nucleo_id`),
  CONSTRAINT `FK_4n2luc1f32rkai9fgxgtni9io` FOREIGN KEY (`empresa_id`) REFERENCES `empresa` (`id`),
  CONSTRAINT `FK_etldmfqhtr1jhh2cgrtw9t866` FOREIGN KEY (`nucleo_id`) REFERENCES `nucleo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cotacao`
--

LOCK TABLES `cotacao` WRITE;
/*!40000 ALTER TABLE `cotacao` DISABLE KEYS */;
/*!40000 ALTER TABLE `cotacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cotacao_fornecedor`
--

DROP TABLE IF EXISTS `cotacao_fornecedor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cotacao_fornecedor` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `status_cotacao_fornecedor` int(11) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `valor_desconto` decimal(19,2) DEFAULT NULL,
  `valor_frete` decimal(19,2) DEFAULT NULL,
  `id_cotacao` bigint(20) NOT NULL,
  `id_fornecedor` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_dqjbgunsrk0d2100lv7outeb1` (`id_cotacao`),
  KEY `FK_dvw58x5w6mj24lhcntn0r03pr` (`id_fornecedor`),
  CONSTRAINT `FK_dqjbgunsrk0d2100lv7outeb1` FOREIGN KEY (`id_cotacao`) REFERENCES `cotacao` (`id`),
  CONSTRAINT `FK_dvw58x5w6mj24lhcntn0r03pr` FOREIGN KEY (`id_fornecedor`) REFERENCES `fornecedor` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cotacao_fornecedor`
--

LOCK TABLES `cotacao_fornecedor` WRITE;
/*!40000 ALTER TABLE `cotacao_fornecedor` DISABLE KEYS */;
/*!40000 ALTER TABLE `cotacao_fornecedor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `departamento`
--

DROP TABLE IF EXISTS `departamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `departamento` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `status` bit(1) DEFAULT NULL,
  `nome` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_d0gdbc1oh2ffvl6f54xvxghrr` (`nome`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departamento`
--

LOCK TABLES `departamento` WRITE;
/*!40000 ALTER TABLE `departamento` DISABLE KEYS */;
/*!40000 ALTER TABLE `departamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `destino_estoque`
--

DROP TABLE IF EXISTS `destino_estoque`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `destino_estoque` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ativo` bit(1) DEFAULT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `destino_estoque`
--

LOCK TABLES `destino_estoque` WRITE;
/*!40000 ALTER TABLE `destino_estoque` DISABLE KEYS */;
/*!40000 ALTER TABLE `destino_estoque` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documento`
--

DROP TABLE IF EXISTS `documento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documento` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ativo` bit(1) DEFAULT NULL,
  `data_cadastro` datetime DEFAULT NULL,
  `data_evento` datetime DEFAULT NULL,
  `historico` varchar(255) DEFAULT NULL,
  `nome_arquivo` varchar(255) DEFAULT NULL,
  `tipo` int(11) DEFAULT NULL,
  `tipo_arquivo` varchar(255) DEFAULT NULL,
  `valor_recebido` decimal(19,2) DEFAULT NULL,
  `id_colaborador` bigint(20) NOT NULL,
  `id_usuario_adicionou` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_bwy6n43g5i1d4p7ffwv63ug8a` (`id_colaborador`),
  KEY `FK_sekyivynskaam1rh35svmhypg` (`id_usuario_adicionou`),
  CONSTRAINT `FK_bwy6n43g5i1d4p7ffwv63ug8a` FOREIGN KEY (`id_colaborador`) REFERENCES `colaborador` (`id`),
  CONSTRAINT `FK_sekyivynskaam1rh35svmhypg` FOREIGN KEY (`id_usuario_adicionou`) REFERENCES `usuario` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documento`
--

LOCK TABLES `documento` WRITE;
/*!40000 ALTER TABLE `documento` DISABLE KEYS */;
/*!40000 ALTER TABLE `documento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empresa`
--

DROP TABLE IF EXISTS `empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empresa` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `status` bit(1) DEFAULT NULL,
  `bairro` varchar(100) DEFAULT NULL,
  `cep` varchar(20) DEFAULT NULL,
  `cidade` varchar(100) DEFAULT NULL,
  `cpf_cnpj` varchar(20) DEFAULT NULL,
  `data_cadastro` datetime DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `endereco` varchar(200) DEFAULT NULL,
  `inscricao_estadual` varchar(255) DEFAULT NULL,
  `inscricao_municipal` varchar(255) DEFAULT NULL,
  `logomarca` longblob,
  `nome` varchar(255) DEFAULT NULL,
  `telefone` varchar(20) DEFAULT NULL,
  `tipo` int(11) DEFAULT NULL,
  `uf_cidade` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empresa`
--

LOCK TABLES `empresa` WRITE;
/*!40000 ALTER TABLE `empresa` DISABLE KEYS */;
INSERT INTO `empresa` VALUES (1,'','Pq Amazonas','77777-777','GOIÂNIA','11111111/111111',NULL,'adm@gmail.com','TESTE',NULL,NULL,NULL,'Conecta','(22)22222-2222',1,18);
/*!40000 ALTER TABLE `empresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fabricante`
--

DROP TABLE IF EXISTS `fabricante`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fabricante` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `status` bit(1) DEFAULT NULL,
  `nome` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fabricante`
--

LOCK TABLES `fabricante` WRITE;
/*!40000 ALTER TABLE `fabricante` DISABLE KEYS */;
/*!40000 ALTER TABLE `fabricante` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fornecedor`
--

DROP TABLE IF EXISTS `fornecedor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fornecedor` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `status` bit(1) DEFAULT NULL,
  `celular_contato` varchar(255) DEFAULT NULL,
  `nome_contato` varchar(255) DEFAULT NULL,
  `prestador_servico` bit(1) DEFAULT NULL,
  `pessoa_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_5jagccplmej8fj4s891gldy17` (`pessoa_id`),
  CONSTRAINT `FK_5jagccplmej8fj4s891gldy17` FOREIGN KEY (`pessoa_id`) REFERENCES `pessoa` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fornecedor`
--

LOCK TABLES `fornecedor` WRITE;
/*!40000 ALTER TABLE `fornecedor` DISABLE KEYS */;
/*!40000 ALTER TABLE `fornecedor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grupo`
--

DROP TABLE IF EXISTS `grupo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grupo` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `status` bit(1) DEFAULT NULL,
  `nome` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_2iij2lspt3u2dj6c5max7ktt3` (`nome`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grupo`
--

LOCK TABLES `grupo` WRITE;
/*!40000 ALTER TABLE `grupo` DISABLE KEYS */;
/*!40000 ALTER TABLE `grupo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grupo_bem`
--

DROP TABLE IF EXISTS `grupo_bem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grupo_bem` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `status` bit(1) DEFAULT NULL,
  `depreciacao_anual` decimal(19,2) DEFAULT NULL,
  `nome` varchar(200) NOT NULL,
  `observacao` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grupo_bem`
--

LOCK TABLES `grupo_bem` WRITE;
/*!40000 ALTER TABLE `grupo_bem` DISABLE KEYS */;
/*!40000 ALTER TABLE `grupo_bem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_cotacao`
--

DROP TABLE IF EXISTS `item_cotacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_cotacao` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `quantidade` decimal(19,2) DEFAULT NULL,
  `valor_unitario` decimal(19,2) DEFAULT NULL,
  `id_cotacao_fornecedor` bigint(20) NOT NULL,
  `id_produto` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_eb6jqh2gv54y8pyq5y5bj1eie` (`id_cotacao_fornecedor`),
  KEY `FK_annyi1cmmkv399mmx9h4kkluu` (`id_produto`),
  CONSTRAINT `FK_annyi1cmmkv399mmx9h4kkluu` FOREIGN KEY (`id_produto`) REFERENCES `produto` (`id`),
  CONSTRAINT `FK_eb6jqh2gv54y8pyq5y5bj1eie` FOREIGN KEY (`id_cotacao_fornecedor`) REFERENCES `cotacao_fornecedor` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_cotacao`
--

LOCK TABLES `item_cotacao` WRITE;
/*!40000 ALTER TABLE `item_cotacao` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_cotacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_pedido_compra`
--

DROP TABLE IF EXISTS `item_pedido_compra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_pedido_compra` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `quantidade` decimal(19,2) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `valor_unitario` decimal(19,2) DEFAULT NULL,
  `pedido_compra_id` bigint(20) DEFAULT NULL,
  `produto_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_of8il537gdx6c3yyx9q48q8ul` (`pedido_compra_id`),
  KEY `FK_edhq2p4lije576ljxq8lr99j1` (`produto_id`),
  CONSTRAINT `FK_edhq2p4lije576ljxq8lr99j1` FOREIGN KEY (`produto_id`) REFERENCES `produto` (`id`),
  CONSTRAINT `FK_of8il537gdx6c3yyx9q48q8ul` FOREIGN KEY (`pedido_compra_id`) REFERENCES `pedido_compra` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_pedido_compra`
--

LOCK TABLES `item_pedido_compra` WRITE;
/*!40000 ALTER TABLE `item_pedido_compra` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_pedido_compra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `local`
--

DROP TABLE IF EXISTS `local`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `local` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `status` bit(1) DEFAULT NULL,
  `nome` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_tf7tvft57k44bkqbnclrnpqyq` (`nome`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `local`
--

LOCK TABLES `local` WRITE;
/*!40000 ALTER TABLE `local` DISABLE KEYS */;
/*!40000 ALTER TABLE `local` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `manutencao_patrimonio`
--

DROP TABLE IF EXISTS `manutencao_patrimonio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manutencao_patrimonio` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `status` bit(1) DEFAULT NULL,
  `data_prevista_retorno` datetime DEFAULT NULL,
  `data_retorno` datetime DEFAULT NULL,
  `data_saida` datetime DEFAULT NULL,
  `descricao_retorno` varchar(200) NOT NULL,
  `fim_garantia` datetime DEFAULT NULL,
  `motivo` varchar(200) NOT NULL,
  `valor` decimal(19,2) DEFAULT NULL,
  `valor_pago` decimal(19,2) DEFAULT NULL,
  `fornecedor_id` bigint(20) DEFAULT NULL,
  `patrimonio_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_8ah0a4t3h5q0l4b0vutkgf6pg` (`fornecedor_id`),
  KEY `FK_6k8awxwn7h84rqbe57470josj` (`patrimonio_id`),
  CONSTRAINT `FK_6k8awxwn7h84rqbe57470josj` FOREIGN KEY (`patrimonio_id`) REFERENCES `patrimonio` (`id`),
  CONSTRAINT `FK_8ah0a4t3h5q0l4b0vutkgf6pg` FOREIGN KEY (`fornecedor_id`) REFERENCES `fornecedor` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `manutencao_patrimonio`
--

LOCK TABLES `manutencao_patrimonio` WRITE;
/*!40000 ALTER TABLE `manutencao_patrimonio` DISABLE KEYS */;
/*!40000 ALTER TABLE `manutencao_patrimonio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ativo` bit(1) DEFAULT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `nome_pagina` varchar(255) DEFAULT NULL,
  `id_menu_pai` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_cv2hnx909i4u60fnrg36w09f8` (`id_menu_pai`),
  CONSTRAINT `FK_cv2hnx909i4u60fnrg36w09f8` FOREIGN KEY (`id_menu_pai`) REFERENCES `menu` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES (1,'','Cadastro',NULL,NULL),(2,'','Estoque','/estoque',1),(3,'','Cotação','/cotacao',NULL),(4,'','Pedido de Compra','/pedidocompra',NULL),(5,'','Patrimônio',NULL,NULL),(6,'','Solicitação',NULL,NULL),(7,'','Ordem de Serviço','/empresa',NULL),(8,'','Empresa','/empresa',1),(9,'','Colaborador','/colaborador',1),(10,'','Perfil de Acesso','/perfil',1),(11,'','Produto','/produto',1),(12,'','Fornecedores',NULL,1),(13,'','Categoria',NULL,5),(14,'','Equipamentos / Históricos',NULL,5),(15,'','Suprimento',NULL,6),(16,'','Qualidade',NULL,6),(17,'','Nucleo','/nucleo',1),(18,'','Cargo','/cargo',1),(19,'','Categoria de Produto','/categoriaProduto',1),(22,'','Fabricante','/fabricante',1),(23,'','Grupo Bem','/grupo_bem',5),(25,'','Patrimônio','/patrimonio',5),(26,'','Departamento','/departamento',5),(27,'','Componente Item Patrimonio','/componente_item_patrimonio',5),(28,'','Item Pedido Compra','/item_pedido_compra',4),(29,'','Pedido Compra','/pedidocompra',4),(30,'','Fornecedor','/fornecedor',1),(31,'','Manutenção Patrimonio','/manutencao_patrimonio',5),(32,'','Solicitação','/solicitacao',6),(33,'','Controle de Solicitação','/controle_solicitacao',6),(34,'','Qualidade','/qualidade',6),(35,'','Local','/local',6),(37,'','Dashboard','/dashboard',NULL);
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movimento_estoque`
--

DROP TABLE IF EXISTS `movimento_estoque`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movimento_estoque` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `data` datetime DEFAULT NULL,
  `observacao` varchar(255) DEFAULT NULL,
  `quantidade_movimentada` decimal(19,2) DEFAULT NULL,
  `saldo` decimal(19,2) DEFAULT NULL,
  `tipo_movimento_estoque` int(11) DEFAULT NULL,
  `valor` decimal(19,2) DEFAULT NULL,
  `colaborador_id` bigint(20) NOT NULL,
  `empresa_id` bigint(20) DEFAULT NULL,
  `nucleo_id` bigint(20) DEFAULT NULL,
  `produto_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_5prc9ojqko0b618txldyriem3` (`colaborador_id`),
  KEY `FK_ba8838pscbxvthu5u7yaoj9hi` (`empresa_id`),
  KEY `FK_c9x37etubqo5kioooeltqfi5g` (`nucleo_id`),
  KEY `FK_n7wawvic3okyncfvcjjyfopco` (`produto_id`),
  CONSTRAINT `FK_5prc9ojqko0b618txldyriem3` FOREIGN KEY (`colaborador_id`) REFERENCES `colaborador` (`id`),
  CONSTRAINT `FK_ba8838pscbxvthu5u7yaoj9hi` FOREIGN KEY (`empresa_id`) REFERENCES `empresa` (`id`),
  CONSTRAINT `FK_c9x37etubqo5kioooeltqfi5g` FOREIGN KEY (`nucleo_id`) REFERENCES `nucleo` (`id`),
  CONSTRAINT `FK_n7wawvic3okyncfvcjjyfopco` FOREIGN KEY (`produto_id`) REFERENCES `produto` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movimento_estoque`
--

LOCK TABLES `movimento_estoque` WRITE;
/*!40000 ALTER TABLE `movimento_estoque` DISABLE KEYS */;
/*!40000 ALTER TABLE `movimento_estoque` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nucleo`
--

DROP TABLE IF EXISTS `nucleo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nucleo` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `status` bit(1) DEFAULT NULL,
  `bairro` varchar(100) DEFAULT NULL,
  `cep` varchar(20) DEFAULT NULL,
  `cidade` varchar(100) DEFAULT NULL,
  `data_cadastro` datetime DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `endereco` varchar(200) DEFAULT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `telefone` varchar(20) DEFAULT NULL,
  `uf_cidade` int(11) DEFAULT NULL,
  `id_empresa` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_cf4yo06vwvb0en2vjn43u57ie` (`id_empresa`),
  CONSTRAINT `FK_cf4yo06vwvb0en2vjn43u57ie` FOREIGN KEY (`id_empresa`) REFERENCES `empresa` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nucleo`
--

LOCK TABLES `nucleo` WRITE;
/*!40000 ALTER TABLE `nucleo` DISABLE KEYS */;
/*!40000 ALTER TABLE `nucleo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `numero_banco`
--

DROP TABLE IF EXISTS `numero_banco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `numero_banco` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `numero` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=182 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `numero_banco`
--

LOCK TABLES `numero_banco` WRITE;
/*!40000 ALTER TABLE `numero_banco` DISABLE KEYS */;
INSERT INTO `numero_banco` VALUES (1,'Brb - Banco De Brasilia S.A.','70'),(2,'Banco Central Do Brasil','2'),(3,'Banco Pottencial S.A.','735'),(4,'Cc Unicred Do Brasil','136'),(5,'Caixa Economica Federal','104'),(6,'Banco Intermedium S.A.','77'),(7,'Banco Ribeirao Preto S.A.','741'),(8,'Unicred Central Santa Catarina','87'),(9,'Cetelem','739'),(10,'Banco Semear S.A.','743'),(11,'Sc Planner','100'),(12,'Banco Bmfbovespa','96'),(13,'Banco Rabobank International Brasil S.A.','747'),(14,'Banco Cooperativo Sicredi S.A.','748'),(15,'Banco Bnp Paribas Brasil S.A','752'),(16,'Unicred Central Rs - Central De Coop Eco','91'),(17,'Hsbc Bank Brasil S.A.-Banco Multiplo','399'),(18,'Scfi Portocred','108'),(19,'Banco Cooperativo Do Brasil S.A.','756'),(20,'Keb Hana Do Brasil','757'),(21,'Sc Xp Investimentos','102'),(22,'Cc Uniprime Norte Do Parana','84'),(23,'Morgan Stanley','66'),(24,'Sc Link','15'),(25,'Sc Treviso','143'),(26,'Hipercard Banco Multiplo S.A','62'),(27,'Banco J. Safra S.A.','74'),(28,'Uniprime Central - Central Int De Coop D','99'),(29,'Banco Alfa S/a','25'),(30,'Abn Amro','75'),(31,'Banco Cargill S.A','40'),(32,'Banco Bradescard S.A.','63'),(33,'Cc Brasil Central','112'),(34,'Goldman Sachs Do Brasil-Banco Multiplo S','64'),(35,'Cooperativa Central De Credito Noroeste','97'),(36,'Cc Creditran','16'),(37,'Banco Da Amazonia S.A.','3'),(38,'Confidence Corretora De Cambio','60'),(39,'Banco Do Estado Do Para S.A.','37'),(40,'Cooperativa Central De Credito Urbano -','85'),(41,'Cecoopes-Central Das Coop De Econ E Cred','114'),(42,'Banco Bradesco Bbi S.A','36'),(43,'Banco Bradesco Financiamentos S.A.','394'),(44,'Banco Do Nordeste Do Brasil S.A.','4'),(45,'Ccb Brasil','320'),(46,'Scfi Lecca','105'),(47,'Kdb Do Brasil','76'),(48,'Banco Topazio S.A.','82'),(49,'Scm Polocred','93'),(50,'Natixis Brasil S.A. - Banco Multiplo','14'),(51,'Scfi Caruana','130'),(52,'Banco Azteca Do Brasil S.A.','19'),(53,'Sc Codepe','127'),(54,'Banco Original Do Agronegocio S.A.','79'),(55,'Bbn Banco Brasileiro De Negocios S.A','81'),(56,'Banco Gerador S.A.','121'),(57,'Banco Da China Brasil S.A.','83'),(58,'Sc Get Money','138'),(59,'Banco De Pernambuco S.A.-Bandepe','24'),(60,'Banco Randon S.A.','88'),(61,'Banco Confidence De Cambio Sa','95'),(62,'Banco Petra S.A.','94'),(63,'Bi Standard Chartered','118'),(64,'Sc Multimoney','137'),(65,'Scfi Brickell','92'),(66,'Banco Do Estado De Sergipe S.A.','47'),(67,'Bi Br Partners','126'),(68,'Scfi Agiplan','123'),(69,'Banco Western Union Do Brasil S.A.','119'),(70,'Parana Banco S.A.','254'),(71,'Banco Bbm S.A','107'),(72,'Banco Capital S.A.','412'),(73,'Banco Woori Bank Do Brasil S.A','124'),(74,'Scfi Facta Financeira','149'),(75,'Sc Broker Brasil','142'),(76,'Banco Mercantil Do Brasil S.A.','389'),(77,'Banco Itau Bba S.A','184'),(78,'Banco Triangulo S.A.','634'),(79,'Sc Senso','13'),(80,'Icbc Do Brasil Banco Multiplo S.A.','132'),(81,'Ubs Brasil Banco De Investimento S.A.','129'),(82,'Bcam Ms Bank','128'),(83,'Sc Guitta','146'),(84,'Banestes S.A Banco Do Estado Do Espirito','21'),(85,'Banco Abc Brasil S.A.','246'),(86,'Cip C3','0'),(87,'Scotiabank Brasil S.A Banco Multiplo','751'),(88,'Banco Btg Pactual S.A.','208'),(89,'Banco Modal S.A.','746'),(90,'Banco Classico S.A.','241'),(91,'Banco Guanabara S.A.','612'),(92,'Banco Industrial Do Brasil S. A.','604'),(93,'Banco Credit Suisse (brasil) S.A.','505'),(94,'Banco De La Nacion Argentina','300'),(95,'Citibank N.A.','477'),(96,'Banco Cedula S.A.','266'),(97,'Bradesco Berj','122'),(98,'Banco J.P. Morgan S.A.','376'),(99,'Banco Cacique S.A.','263'),(100,'Banco Caixa Geral - Brasil S.A.','473'),(101,'Banco Citibank S.A.','745'),(102,'Banco Boa Vista Interatlantico S.A','248'),(103,'Banco Rodobens S.A','120'),(104,'Banco Fator S.A.','265'),(105,'Bndes','7'),(106,'Dtvm Bgc Liquidez','134'),(107,'Banco Alvorada S.A.','641'),(108,'Banif-Banco Internacional Do Funchal (br','719'),(109,'Itau Bmg Consignado','29'),(110,'Banco Maxima S.A.','243'),(111,'Bi Haitong Do Brasil','78'),(112,'Dtvm Oliveira Trust','111'),(113,'Bny Mellon S.A.','17'),(114,'Banco Nossa Caixa S.A','151'),(115,'Banco De La Provincia De Buenos Aires','495'),(116,'Brasil Plural S.A. Banco Multiplo','125'),(117,'Jpmorgan Chase Bank','488'),(118,'Andbank','65'),(119,'Ing Bank','492'),(120,'Sc Levycam','145'),(121,'Bcv - Banco De Credito E Varejo S.A','250'),(122,'Banco De La Republica Oriental Del Urugu','494'),(123,'Banco Arbi S.A.','213'),(124,'Intesa Sanpaolo','139'),(125,'Bm Tricury','18'),(126,'Banco Safra S.A.','422'),(127,'Intercap','630'),(128,'Banco Fibra S.A.','224'),(129,'Banco Luso Brasileiro S.A.','600'),(130,'Pan','623'),(131,'Banco Bradesco Cartoes S.A.','204'),(132,'Banco Votorantim S.A.','655'),(133,'Banco Itaubank S.A.','479'),(134,'Banco De Tokyo Mitsubishi Ufj Brasil S.A','456'),(135,'Banco Sumitomo Mitsui Brasileiro S.A.','464'),(136,'Itau Unibanco S.A.','341'),(137,'Banco Bradesco S.A.','237'),(138,'Banco Pecunia S.A.','613'),(139,'Banco Itau Holding Financeira S.A','652'),(140,'Banco Sofisa S.A.','637'),(141,'Banco Indusval S.A.','653'),(142,'Bpn Brasil Banco Multiplo S.A','69'),(143,'Unicard Banco Multiplo S.A','230'),(144,'Banco Mizuho Do Brasil S.A.','370'),(145,'Banco Barclays S.A.','740'),(146,'Banco Investcred Unibanco S.A','249'),(147,'Banco Bmg S.A.','318'),(148,'Banco Dibens S.A.','214'),(149,'Banco Ficsa S.A.','626'),(150,'Banco Societe Generale Brasil S.A','366'),(151,'Sc Magliano','113'),(152,'Sc Tullett Prebon','131'),(153,'Sc Souza Barros','901'),(154,'Sc Credit Suisse Hg','11'),(155,'Banco Paulista S.A.','611'),(156,'Bank Of America Merrill Lynch Banco Mult','755'),(157,'Cooperativa De Credito Rural Da Regiao D','89'),(158,'Banco Pine S.A.','643'),(159,'Sc Easynvest','140'),(160,'Banco Daycoval S.A.','707'),(161,'Dtvm Renascenca','101'),(162,'Deutsche Bank S. A. - Banco Alemao','487'),(163,'Banco Cifra S.A.','233'),(164,'Banco Rendimento S.A.','633'),(165,'Banco Bonsucesso S.A.','218'),(166,'Cooperativa Central De Credito Do Estado','90'),(167,'Sc Bt Associados','80'),(168,'Nbc Bank Brasil S.A.- Banco Multiplo','753'),(169,'Banco Credit Agricole Brasil S.A.','222'),(170,'Sistema','754'),(171,'Credialianca Cooperativa De Credito Rura','98'),(172,'Banco Vr S.A.','610'),(173,'Banco Ourinvest S.A.','712'),(174,'Cc Credicoamo Credito Rural Cooperativa','10'),(175,'Banco Santander (brasil) S.A.','33'),(176,'Banco John Deere S.A.','217'),(177,'Banco Do Estado Do Rio Grande Do Sul S.A','41'),(178,'Sc Advanced','117'),(179,'Banco A.J. Renner S.A.','654'),(180,'Banco Original S.A.','212'),(181,'BANCO DO BRASIL','001');
/*!40000 ALTER TABLE `numero_banco` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `papel`
--

DROP TABLE IF EXISTS `papel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `papel` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `versao` bigint(20) DEFAULT NULL,
  `id_menu` bigint(20) NOT NULL,
  `id_perfil_usuario` bigint(20) DEFAULT NULL,
  `id_permissao` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_sqc8i8ykg1iiqui90blti63t1` (`id_menu`),
  KEY `FK_id1nlcem4ri9d8l3ovm5okpky` (`id_perfil_usuario`),
  KEY `FK_1skl42jaye9vfhr496ms0tn5d` (`id_permissao`),
  CONSTRAINT `FK_1skl42jaye9vfhr496ms0tn5d` FOREIGN KEY (`id_permissao`) REFERENCES `permissao` (`id`),
  CONSTRAINT `FK_id1nlcem4ri9d8l3ovm5okpky` FOREIGN KEY (`id_perfil_usuario`) REFERENCES `perfil_usuario` (`id`),
  CONSTRAINT `FK_sqc8i8ykg1iiqui90blti63t1` FOREIGN KEY (`id_menu`) REFERENCES `menu` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `papel`
--

LOCK TABLES `papel` WRITE;
/*!40000 ALTER TABLE `papel` DISABLE KEYS */;
INSERT INTO `papel` VALUES (3,0,10,1,1),(4,0,2,1,1),(5,0,3,1,1),(6,0,4,1,1),(7,0,7,1,1),(8,0,8,1,1),(9,0,9,1,1),(10,0,11,1,1),(11,0,17,1,1),(12,0,18,1,1),(13,0,19,1,1),(14,0,22,1,1),(15,0,23,1,1),(16,0,25,1,1),(17,0,26,1,1),(18,0,27,1,1),(19,0,28,1,1),(20,0,29,1,1),(21,0,30,1,1),(22,0,31,1,1),(23,0,32,1,1),(24,0,33,1,1),(25,0,34,1,1),(26,0,35,1,1),(27,0,37,1,1);
/*!40000 ALTER TABLE `papel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patrimonio`
--

DROP TABLE IF EXISTS `patrimonio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patrimonio` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `status` bit(1) DEFAULT NULL,
  `data_incorporacao` datetime DEFAULT NULL,
  `data_nota` datetime DEFAULT NULL,
  `descricao_garantia` varchar(255) DEFAULT NULL,
  `fim_garantia` datetime DEFAULT NULL,
  `nome` varchar(200) NOT NULL,
  `numero_etiqueta` varchar(100) DEFAULT NULL,
  `numero_nota` varchar(200) DEFAULT NULL,
  `numero_serie` varchar(100) DEFAULT NULL,
  `observacao` varchar(200) DEFAULT NULL,
  `situacao` int(11) DEFAULT NULL,
  `tipo_incorporacao_patrimonio` int(11) DEFAULT NULL,
  `unidade_medida` int(11) DEFAULT NULL,
  `valor` decimal(19,2) DEFAULT NULL,
  `departamento_id` bigint(20) DEFAULT NULL,
  `grupo_bem_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_ks4c5km1k1fow1v84rxb061ap` (`departamento_id`),
  KEY `FK_8fogksxh4ky9npi2c2e90pje6` (`grupo_bem_id`),
  CONSTRAINT `FK_8fogksxh4ky9npi2c2e90pje6` FOREIGN KEY (`grupo_bem_id`) REFERENCES `grupo_bem` (`id`),
  CONSTRAINT `FK_ks4c5km1k1fow1v84rxb061ap` FOREIGN KEY (`departamento_id`) REFERENCES `departamento` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patrimonio`
--

LOCK TABLES `patrimonio` WRITE;
/*!40000 ALTER TABLE `patrimonio` DISABLE KEYS */;
/*!40000 ALTER TABLE `patrimonio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_compra`
--

DROP TABLE IF EXISTS `pedido_compra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_compra` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `data_pedido_compra` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `valor_desconto` decimal(19,2) DEFAULT NULL,
  `valor_frete` decimal(19,2) DEFAULT NULL,
  `colaborador_id` bigint(20) DEFAULT NULL,
  `cotacao_id` bigint(20) DEFAULT NULL,
  `empresa_id` bigint(20) DEFAULT NULL,
  `fornecedor_id` bigint(20) DEFAULT NULL,
  `nucleo_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_hsr03uhpu4dflbwhe1w06dfox` (`colaborador_id`),
  KEY `FK_kjixdrfdndvxyc3andbxlh7ft` (`cotacao_id`),
  KEY `FK_8auk0x3ksk9lv7rpt0f716dhe` (`empresa_id`),
  KEY `FK_f5t0yf33eskm8rlc94rnp6xk2` (`fornecedor_id`),
  KEY `FK_29rk9cknupvbtcx6igpg3wloi` (`nucleo_id`),
  CONSTRAINT `FK_29rk9cknupvbtcx6igpg3wloi` FOREIGN KEY (`nucleo_id`) REFERENCES `nucleo` (`id`),
  CONSTRAINT `FK_8auk0x3ksk9lv7rpt0f716dhe` FOREIGN KEY (`empresa_id`) REFERENCES `empresa` (`id`),
  CONSTRAINT `FK_f5t0yf33eskm8rlc94rnp6xk2` FOREIGN KEY (`fornecedor_id`) REFERENCES `fornecedor` (`id`),
  CONSTRAINT `FK_hsr03uhpu4dflbwhe1w06dfox` FOREIGN KEY (`colaborador_id`) REFERENCES `colaborador` (`id`),
  CONSTRAINT `FK_kjixdrfdndvxyc3andbxlh7ft` FOREIGN KEY (`cotacao_id`) REFERENCES `cotacao` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_compra`
--

LOCK TABLES `pedido_compra` WRITE;
/*!40000 ALTER TABLE `pedido_compra` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedido_compra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perfil_usuario`
--

DROP TABLE IF EXISTS `perfil_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perfil_usuario` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `acesso_servidor` bit(1) NOT NULL,
  `nome_perfil` varchar(255) NOT NULL,
  `versao` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perfil_usuario`
--

LOCK TABLES `perfil_usuario` WRITE;
/*!40000 ALTER TABLE `perfil_usuario` DISABLE KEYS */;
INSERT INTO `perfil_usuario` VALUES (1,'','ADMINISTRADOR',0);
/*!40000 ALTER TABLE `perfil_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissao`
--

DROP TABLE IF EXISTS `permissao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissao` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `edita` bit(1) DEFAULT NULL,
  `exclui` bit(1) DEFAULT NULL,
  `insere` bit(1) DEFAULT NULL,
  `versao` bigint(20) DEFAULT NULL,
  `visualiza` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissao`
--

LOCK TABLES `permissao` WRITE;
/*!40000 ALTER TABLE `permissao` DISABLE KEYS */;
INSERT INTO `permissao` VALUES (1,'','','',1,'');
/*!40000 ALTER TABLE `permissao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pessoa`
--

DROP TABLE IF EXISTS `pessoa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pessoa` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `agencia` varchar(30) DEFAULT NULL,
  `bairro` varchar(255) DEFAULT NULL,
  `celular` varchar(20) DEFAULT NULL,
  `cep` varchar(10) DEFAULT NULL,
  `cidade` varchar(255) DEFAULT NULL,
  `complemento` varchar(255) DEFAULT NULL,
  `conta_corrente` varchar(30) DEFAULT NULL,
  `cpf_cnpj` varchar(20) DEFAULT NULL,
  `data_cadastro` datetime DEFAULT NULL,
  `data_expedicao_rg_ie` datetime DEFAULT NULL,
  `data_nascimento` datetime DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `emissor_rg_ie` varchar(20) DEFAULT NULL,
  `endereco` varchar(255) DEFAULT NULL,
  `estado_civil` int(11) DEFAULT NULL,
  `foto` longblob,
  `naturalidade` varchar(255) DEFAULT NULL,
  `nome` varchar(200) DEFAULT NULL,
  `numero_ctps` varchar(255) DEFAULT NULL,
  `numero_endereco` varchar(255) DEFAULT NULL,
  `numero_pis` varchar(255) DEFAULT NULL,
  `observacao` varchar(255) DEFAULT NULL,
  `observacoes_bancarias` varchar(255) DEFAULT NULL,
  `rg_ie` varchar(20) DEFAULT NULL,
  `sexo` int(11) DEFAULT NULL,
  `telefone` varchar(20) DEFAULT NULL,
  `tipo` int(11) DEFAULT NULL,
  `uf_cidade` int(11) DEFAULT NULL,
  `ud_naturalidade` int(11) DEFAULT NULL,
  `banco_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_bv1lgciww29dqhp8wpm9ytru5` (`banco_id`),
  CONSTRAINT `FK_bv1lgciww29dqhp8wpm9ytru5` FOREIGN KEY (`banco_id`) REFERENCES `numero_banco` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pessoa`
--

LOCK TABLES `pessoa` WRITE;
/*!40000 ALTER TABLE `pessoa` DISABLE KEYS */;
INSERT INTO `pessoa` VALUES (2,'','Teste','(22)22222-2222','77777-777','GOIANIA',NULL,'','12222222222',NULL,'2017-02-01 00:00:00','2017-02-01 00:00:00','sergioadsf@gmail.com','444','TESTE',0,NULL,NULL,'Sergio','',NULL,'',NULL,'','4',0,'(22)22222-2222',1,8,NULL,1);
/*!40000 ALTER TABLE `pessoa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produto`
--

DROP TABLE IF EXISTS `produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produto` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ativo` bit(1) DEFAULT NULL,
  `codigo` varchar(255) DEFAULT NULL,
  `data_cadastro` datetime DEFAULT NULL,
  `maximo_em_estoque` decimal(19,2) DEFAULT NULL,
  `minimo_em_estoque` decimal(19,2) DEFAULT NULL,
  `nome` varchar(200) NOT NULL,
  `unidade_medida` int(11) DEFAULT NULL,
  `valor_custo` decimal(19,2) DEFAULT NULL,
  `valor_venda` decimal(19,2) DEFAULT NULL,
  `categoria_id` bigint(20) DEFAULT NULL,
  `fabricante_id` bigint(20) DEFAULT NULL,
  `fornecedor_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_el0d58htywfs914w4grf6aoa0` (`categoria_id`),
  KEY `FK_cu72orus7wbxu7y54ncjwdr0m` (`fabricante_id`),
  KEY `FK_6ocqxnp8nqfmcn2rdwrnv2795` (`fornecedor_id`),
  CONSTRAINT `FK_6ocqxnp8nqfmcn2rdwrnv2795` FOREIGN KEY (`fornecedor_id`) REFERENCES `fornecedor` (`id`),
  CONSTRAINT `FK_cu72orus7wbxu7y54ncjwdr0m` FOREIGN KEY (`fabricante_id`) REFERENCES `fabricante` (`id`),
  CONSTRAINT `FK_el0d58htywfs914w4grf6aoa0` FOREIGN KEY (`categoria_id`) REFERENCES `categoria_produto` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produto`
--

LOCK TABLES `produto` WRITE;
/*!40000 ALTER TABLE `produto` DISABLE KEYS */;
/*!40000 ALTER TABLE `produto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produto_estoque`
--

DROP TABLE IF EXISTS `produto_estoque`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produto_estoque` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `quantidade` decimal(19,2) DEFAULT NULL,
  `empresa_id` bigint(20) NOT NULL,
  `nucleo_id` bigint(20) NOT NULL,
  `produto_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_b158w9uo89wvg1f4bx4msk2bm` (`empresa_id`),
  KEY `FK_3fl86vj651q4b9pkdpwhnmhj1` (`nucleo_id`),
  KEY `FK_8xw4so3vpta7jxd3bwf7v387f` (`produto_id`),
  CONSTRAINT `FK_3fl86vj651q4b9pkdpwhnmhj1` FOREIGN KEY (`nucleo_id`) REFERENCES `nucleo` (`id`),
  CONSTRAINT `FK_8xw4so3vpta7jxd3bwf7v387f` FOREIGN KEY (`produto_id`) REFERENCES `produto` (`id`),
  CONSTRAINT `FK_b158w9uo89wvg1f4bx4msk2bm` FOREIGN KEY (`empresa_id`) REFERENCES `empresa` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produto_estoque`
--

LOCK TABLES `produto_estoque` WRITE;
/*!40000 ALTER TABLE `produto_estoque` DISABLE KEYS */;
/*!40000 ALTER TABLE `produto_estoque` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qualidade`
--

DROP TABLE IF EXISTS `qualidade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qualidade` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `acao_imediata` varchar(255) DEFAULT NULL,
  `acao_preventiva` varchar(255) DEFAULT NULL,
  `analise_causa` varchar(255) DEFAULT NULL,
  `ativo` bit(1) DEFAULT NULL,
  `auditoria` int(11) DEFAULT NULL,
  `avaliacao_eficacia` varchar(255) DEFAULT NULL,
  `data_criacao` datetime DEFAULT NULL,
  `identificacao` varchar(255) DEFAULT NULL,
  `origem` int(11) DEFAULT NULL,
  `registro` int(11) DEFAULT NULL,
  `autor_id` bigint(20) DEFAULT NULL,
  `empresa_id` bigint(20) DEFAULT NULL,
  `nucleo_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_evjq8xforauxl62xdlyaxvxfp` (`autor_id`),
  KEY `FK_a29vxdg5il923lf7f0t1pqrgt` (`empresa_id`),
  KEY `FK_h2qt9wra3bb7bmt20bwiw2kvt` (`nucleo_id`),
  CONSTRAINT `FK_a29vxdg5il923lf7f0t1pqrgt` FOREIGN KEY (`empresa_id`) REFERENCES `empresa` (`id`),
  CONSTRAINT `FK_evjq8xforauxl62xdlyaxvxfp` FOREIGN KEY (`autor_id`) REFERENCES `usuario` (`id`),
  CONSTRAINT `FK_h2qt9wra3bb7bmt20bwiw2kvt` FOREIGN KEY (`nucleo_id`) REFERENCES `nucleo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qualidade`
--

LOCK TABLES `qualidade` WRITE;
/*!40000 ALTER TABLE `qualidade` DISABLE KEYS */;
/*!40000 ALTER TABLE `qualidade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `solicitacao`
--

DROP TABLE IF EXISTS `solicitacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `solicitacao` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `data_abertura` datetime DEFAULT NULL,
  `assunto` varchar(255) DEFAULT NULL,
  `ativo` bit(1) DEFAULT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  `numero_etiqueta` varchar(100) DEFAULT NULL,
  `data_previsao_finalizacao` datetime DEFAULT NULL,
  `solucao` varchar(255) DEFAULT NULL,
  `status_solicitacao` int(11) DEFAULT NULL,
  `data_termino` datetime DEFAULT NULL,
  `criador_id` bigint(20) DEFAULT NULL,
  `empresa_id` bigint(20) DEFAULT NULL,
  `local_id` bigint(20) DEFAULT NULL,
  `nucleo_id` bigint(20) DEFAULT NULL,
  `responsavel_id` bigint(20) DEFAULT NULL,
  `tipo_solicitacao_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_dp25chc1c3dqcbdpj6urioeme` (`criador_id`),
  KEY `FK_kgqceqn1wkgrf1anu5ur4d4nd` (`empresa_id`),
  KEY `FK_85nkipirw1kxcxme5ax5ds0ew` (`local_id`),
  KEY `FK_dke27sbddv1939m9de0p395is` (`nucleo_id`),
  KEY `FK_e9yyjttmss6j1v7lr93d60hsr` (`responsavel_id`),
  KEY `FK_nuh2geldr16ej7i9uo6v9x1yf` (`tipo_solicitacao_id`),
  CONSTRAINT `FK_85nkipirw1kxcxme5ax5ds0ew` FOREIGN KEY (`local_id`) REFERENCES `local` (`id`),
  CONSTRAINT `FK_dke27sbddv1939m9de0p395is` FOREIGN KEY (`nucleo_id`) REFERENCES `nucleo` (`id`),
  CONSTRAINT `FK_dp25chc1c3dqcbdpj6urioeme` FOREIGN KEY (`criador_id`) REFERENCES `usuario` (`id`),
  CONSTRAINT `FK_e9yyjttmss6j1v7lr93d60hsr` FOREIGN KEY (`responsavel_id`) REFERENCES `usuario` (`id`),
  CONSTRAINT `FK_kgqceqn1wkgrf1anu5ur4d4nd` FOREIGN KEY (`empresa_id`) REFERENCES `empresa` (`id`),
  CONSTRAINT `FK_nuh2geldr16ej7i9uo6v9x1yf` FOREIGN KEY (`tipo_solicitacao_id`) REFERENCES `tipo_solicitacao` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `solicitacao`
--

LOCK TABLES `solicitacao` WRITE;
/*!40000 ALTER TABLE `solicitacao` DISABLE KEYS */;
/*!40000 ALTER TABLE `solicitacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_solicitacao`
--

DROP TABLE IF EXISTS `tipo_solicitacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_solicitacao` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `status` bit(1) DEFAULT NULL,
  `nome` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_solicitacao`
--

LOCK TABLES `tipo_solicitacao` WRITE;
/*!40000 ALTER TABLE `tipo_solicitacao` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipo_solicitacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `codigo_recuperar_senha` varchar(255) DEFAULT NULL,
  `email_enviado` bit(1) DEFAULT NULL,
  `primeiro_acesso` bit(1) DEFAULT NULL,
  `senha` varchar(255) DEFAULT NULL,
  `ultimo_acesso` datetime DEFAULT NULL,
  `id_colaborador` bigint(20) DEFAULT NULL,
  `id_perfil_usuario` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_ko8q62qjx9knoce679glie7j5` (`id_colaborador`),
  KEY `FK_a2m9y7uhabyiqqpu2nttcuo04` (`id_perfil_usuario`),
  CONSTRAINT `FK_a2m9y7uhabyiqqpu2nttcuo04` FOREIGN KEY (`id_perfil_usuario`) REFERENCES `perfil_usuario` (`id`),
  CONSTRAINT `FK_ko8q62qjx9knoce679glie7j5` FOREIGN KEY (`id_colaborador`) REFERENCES `colaborador` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,NULL,NULL,'\0','202cb962ac59075b964b07152d234b70',NULL,1,1);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-02-19  8:47:51
