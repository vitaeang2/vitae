-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: vitae3
-- ------------------------------------------------------
-- Server version	5.7.12-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `pessoa`
--

DROP TABLE IF EXISTS `pessoa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pessoa` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `agencia` varchar(30) DEFAULT NULL,
  `bairro` varchar(255) DEFAULT NULL,
  `celular` varchar(20) DEFAULT NULL,
  `cep` varchar(10) DEFAULT NULL,
  `cidade` varchar(255) DEFAULT NULL,
  `complemento` varchar(255) DEFAULT NULL,
  `conta_corrente` varchar(30) DEFAULT NULL,
  `cpf_cnpj` varchar(20) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `emissor_rg_ie` varchar(20) DEFAULT NULL,
  `endereco` varchar(255) DEFAULT NULL,
  `estado_civil` int(11) DEFAULT NULL,
  `foto` longblob,
  `naturalidade` varchar(255) DEFAULT NULL,
  `nome` varchar(200) DEFAULT NULL,
  `nome_mae` varchar(255) DEFAULT NULL,
  `nome_pai` varchar(255) DEFAULT NULL,
  `numero_ctps` varchar(255) DEFAULT NULL,
  `numero_endereco` varchar(255) DEFAULT NULL,
  `numero_pis` varchar(255) DEFAULT NULL,
  `observacao` varchar(255) DEFAULT NULL,
  `observacoes_bancarias` varchar(255) DEFAULT NULL,
  `rg_ie` varchar(20) DEFAULT NULL,
  `sexo` int(11) DEFAULT NULL,
  `telefone` varchar(20) DEFAULT NULL,
  `uf_cidade` int(11) DEFAULT NULL,
  `uf_naturalidade` int(11) DEFAULT NULL,
  `ud_naturalidade` int(11) DEFAULT NULL,
  `data_cadastro` datetime DEFAULT NULL,
  `data_expedicao_rg_ie` datetime DEFAULT NULL,
  `data_nascimento` datetime DEFAULT NULL,
  `banco_id` bigint(20) DEFAULT NULL,
  `tipo` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_bv1lgciww29dqhp8wpm9ytru5` (`banco_id`),
  CONSTRAINT `FK_bv1lgciww29dqhp8wpm9ytru5` FOREIGN KEY (`banco_id`) REFERENCES `numero_banco` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pessoa`
--

LOCK TABLES `pessoa` WRITE;
/*!40000 ALTER TABLE `pessoa` DISABLE KEYS */;
INSERT INTO `pessoa` VALUES (1,'1212112','T','(54)5454-5454','75754-545','JUSSARA',NULL,'2121212','112.121.212-12','sergioadsf@gmail.com','DGPC','T',1,NULL,NULL,'T23',NULL,NULL,'1515',NULL,'51515',NULL,NULL,'44554545',1,'(54)54545-4545',0,NULL,NULL,'2016-06-07 00:00:00','2016-06-02 00:00:00','2016-05-17 00:00:00',1,NULL);
/*!40000 ALTER TABLE `pessoa` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-10-05 10:26:01
