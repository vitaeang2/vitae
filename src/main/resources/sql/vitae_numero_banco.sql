-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: vitae3
-- ------------------------------------------------------
-- Server version	5.7.12-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `numero_banco`
--

DROP TABLE IF EXISTS `numero_banco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `numero_banco` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `numero` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=181 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `numero_banco`
--

LOCK TABLES `numero_banco` WRITE;
/*!40000 ALTER TABLE `numero_banco` DISABLE KEYS */;
INSERT INTO `numero_banco` VALUES (1,'Brb - Banco De Brasilia S.A.','70'),(2,'Banco Central Do Brasil','2'),(3,'Banco Pottencial S.A.','735'),(4,'Cc Unicred Do Brasil','136'),(5,'Caixa Economica Federal','104'),(6,'Banco Intermedium S.A.','77'),(7,'Banco Ribeirao Preto S.A.','741'),(8,'Unicred Central Santa Catarina','87'),(9,'Cetelem','739'),(10,'Banco Semear S.A.','743'),(11,'Sc Planner','100'),(12,'Banco Bmfbovespa','96'),(13,'Banco Rabobank International Brasil S.A.','747'),(14,'Banco Cooperativo Sicredi S.A.','748'),(15,'Banco Bnp Paribas Brasil S.A','752'),(16,'Unicred Central Rs - Central De Coop Eco','91'),(17,'Hsbc Bank Brasil S.A.-Banco Multiplo','399'),(18,'Scfi Portocred','108'),(19,'Banco Cooperativo Do Brasil S.A.','756'),(20,'Keb Hana Do Brasil','757'),(21,'Sc Xp Investimentos','102'),(22,'Cc Uniprime Norte Do Parana','84'),(23,'Morgan Stanley','66'),(24,'Sc Link','15'),(25,'Sc Treviso','143'),(26,'Hipercard Banco Multiplo S.A','62'),(27,'Banco J. Safra S.A.','74'),(28,'Uniprime Central - Central Int De Coop D','99'),(29,'Banco Alfa S/a','25'),(30,'Abn Amro','75'),(31,'Banco Cargill S.A','40'),(32,'Banco Bradescard S.A.','63'),(33,'Cc Brasil Central','112'),(34,'Goldman Sachs Do Brasil-Banco Multiplo S','64'),(35,'Cooperativa Central De Credito Noroeste','97'),(36,'Cc Creditran','16'),(37,'Banco Da Amazonia S.A.','3'),(38,'Confidence Corretora De Cambio','60'),(39,'Banco Do Estado Do Para S.A.','37'),(40,'Cooperativa Central De Credito Urbano -','85'),(41,'Cecoopes-Central Das Coop De Econ E Cred','114'),(42,'Banco Bradesco Bbi S.A','36'),(43,'Banco Bradesco Financiamentos S.A.','394'),(44,'Banco Do Nordeste Do Brasil S.A.','4'),(45,'Ccb Brasil','320'),(46,'Scfi Lecca','105'),(47,'Kdb Do Brasil','76'),(48,'Banco Topazio S.A.','82'),(49,'Scm Polocred','93'),(50,'Natixis Brasil S.A. - Banco Multiplo','14'),(51,'Scfi Caruana','130'),(52,'Banco Azteca Do Brasil S.A.','19'),(53,'Sc Codepe','127'),(54,'Banco Original Do Agronegocio S.A.','79'),(55,'Bbn Banco Brasileiro De Negocios S.A','81'),(56,'Banco Gerador S.A.','121'),(57,'Banco Da China Brasil S.A.','83'),(58,'Sc Get Money','138'),(59,'Banco De Pernambuco S.A.-Bandepe','24'),(60,'Banco Randon S.A.','88'),(61,'Banco Confidence De Cambio Sa','95'),(62,'Banco Petra S.A.','94'),(63,'Bi Standard Chartered','118'),(64,'Sc Multimoney','137'),(65,'Scfi Brickell','92'),(66,'Banco Do Estado De Sergipe S.A.','47'),(67,'Bi Br Partners','126'),(68,'Scfi Agiplan','123'),(69,'Banco Western Union Do Brasil S.A.','119'),(70,'Parana Banco S.A.','254'),(71,'Banco Bbm S.A','107'),(72,'Banco Capital S.A.','412'),(73,'Banco Woori Bank Do Brasil S.A','124'),(74,'Scfi Facta Financeira','149'),(75,'Sc Broker Brasil','142'),(76,'Banco Mercantil Do Brasil S.A.','389'),(77,'Banco Itau Bba S.A','184'),(78,'Banco Triangulo S.A.','634'),(79,'Sc Senso','13'),(80,'Icbc Do Brasil Banco Multiplo S.A.','132'),(81,'Ubs Brasil Banco De Investimento S.A.','129'),(82,'Bcam Ms Bank','128'),(83,'Sc Guitta','146'),(84,'Banestes S.A Banco Do Estado Do Espirito','21'),(85,'Banco Abc Brasil S.A.','246'),(86,'Cip C3','0'),(87,'Scotiabank Brasil S.A Banco Multiplo','751'),(88,'Banco Btg Pactual S.A.','208'),(89,'Banco Modal S.A.','746'),(90,'Banco Classico S.A.','241'),(91,'Banco Guanabara S.A.','612'),(92,'Banco Industrial Do Brasil S. A.','604'),(93,'Banco Credit Suisse (brasil) S.A.','505'),(94,'Banco De La Nacion Argentina','300'),(95,'Citibank N.A.','477'),(96,'Banco Cedula S.A.','266'),(97,'Bradesco Berj','122'),(98,'Banco J.P. Morgan S.A.','376'),(99,'Banco Cacique S.A.','263'),(100,'Banco Caixa Geral - Brasil S.A.','473'),(101,'Banco Citibank S.A.','745'),(102,'Banco Boa Vista Interatlantico S.A','248'),(103,'Banco Rodobens S.A','120'),(104,'Banco Fator S.A.','265'),(105,'Bndes','7'),(106,'Dtvm Bgc Liquidez','134'),(107,'Banco Alvorada S.A.','641'),(108,'Banif-Banco Internacional Do Funchal (br','719'),(109,'Itau Bmg Consignado','29'),(110,'Banco Maxima S.A.','243'),(111,'Bi Haitong Do Brasil','78'),(112,'Dtvm Oliveira Trust','111'),(113,'Bny Mellon S.A.','17'),(114,'Banco Nossa Caixa S.A','151'),(115,'Banco De La Provincia De Buenos Aires','495'),(116,'Brasil Plural S.A. Banco Multiplo','125'),(117,'Jpmorgan Chase Bank','488'),(118,'Andbank','65'),(119,'Ing Bank','492'),(120,'Sc Levycam','145'),(121,'Bcv - Banco De Credito E Varejo S.A','250'),(122,'Banco De La Republica Oriental Del Urugu','494'),(123,'Banco Arbi S.A.','213'),(124,'Intesa Sanpaolo','139'),(125,'Bm Tricury','18'),(126,'Banco Safra S.A.','422'),(127,'Intercap','630'),(128,'Banco Fibra S.A.','224'),(129,'Banco Luso Brasileiro S.A.','600'),(130,'Pan','623'),(131,'Banco Bradesco Cartoes S.A.','204'),(132,'Banco Votorantim S.A.','655'),(133,'Banco Itaubank S.A.','479'),(134,'Banco De Tokyo Mitsubishi Ufj Brasil S.A','456'),(135,'Banco Sumitomo Mitsui Brasileiro S.A.','464'),(136,'Itau Unibanco S.A.','341'),(137,'Banco Bradesco S.A.','237'),(138,'Banco Pecunia S.A.','613'),(139,'Banco Itau Holding Financeira S.A','652'),(140,'Banco Sofisa S.A.','637'),(141,'Banco Indusval S.A.','653'),(142,'Bpn Brasil Banco Multiplo S.A','69'),(143,'Unicard Banco Multiplo S.A','230'),(144,'Banco Mizuho Do Brasil S.A.','370'),(145,'Banco Barclays S.A.','740'),(146,'Banco Investcred Unibanco S.A','249'),(147,'Banco Bmg S.A.','318'),(148,'Banco Dibens S.A.','214'),(149,'Banco Ficsa S.A.','626'),(150,'Banco Societe Generale Brasil S.A','366'),(151,'Sc Magliano','113'),(152,'Sc Tullett Prebon','131'),(153,'Sc Souza Barros','901'),(154,'Sc Credit Suisse Hg','11'),(155,'Banco Paulista S.A.','611'),(156,'Bank Of America Merrill Lynch Banco Mult','755'),(157,'Cooperativa De Credito Rural Da Regiao D','89'),(158,'Banco Pine S.A.','643'),(159,'Sc Easynvest','140'),(160,'Banco Daycoval S.A.','707'),(161,'Dtvm Renascenca','101'),(162,'Deutsche Bank S. A. - Banco Alemao','487'),(163,'Banco Cifra S.A.','233'),(164,'Banco Rendimento S.A.','633'),(165,'Banco Bonsucesso S.A.','218'),(166,'Cooperativa Central De Credito Do Estado','90'),(167,'Sc Bt Associados','80'),(168,'Nbc Bank Brasil S.A.- Banco Multiplo','753'),(169,'Banco Credit Agricole Brasil S.A.','222'),(170,'Sistema','754'),(171,'Credialianca Cooperativa De Credito Rura','98'),(172,'Banco Vr S.A.','610'),(173,'Banco Ourinvest S.A.','712'),(174,'Cc Credicoamo Credito Rural Cooperativa','10'),(175,'Banco Santander (brasil) S.A.','33'),(176,'Banco John Deere S.A.','217'),(177,'Banco Do Estado Do Rio Grande Do Sul S.A','41'),(178,'Sc Advanced','117'),(179,'Banco A.J. Renner S.A.','654'),(180,'Banco Original S.A.','212');
/*!40000 ALTER TABLE `numero_banco` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-10-05 10:25:58
