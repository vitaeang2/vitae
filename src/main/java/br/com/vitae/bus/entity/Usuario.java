package br.com.vitae.bus.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import br.com.arquitetura.entidade.Entidade;

@SuppressWarnings("serial")
@Entity
public class Usuario extends Entidade<Long> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String senha;

	@Column(name = "codigo_recuperar_senha")
	private String codigoRecuperarSenha;

	@Column(name = "primeiro_acesso")
	private boolean primeiroAcesso;

	@Column(name = "ultimo_acesso", nullable = true)
	private Date ultimoAcesso;
	
	@Column(name = "email_enviado")
	private Boolean emailEnviado;

	@ManyToOne
	@JoinColumn(name = "id_perfil_usuario")
	private PerfilUsuario perfil;

	@ManyToOne
	@JoinColumn(name = "id_colaborador")
	private Colaborador colaborador;

	@Transient
	private String token;

	public Usuario() {
	}

	public Usuario(Long id) {
		super();
		this.id = id;
	}

	public Usuario( Long id, Long idColaborador, Long idPessoa, String nomePessoa ) {
		super();
		this.id = id;
		this.colaborador = new Colaborador(idColaborador, idPessoa, nomePessoa, "");
	}
	
	public Usuario( Long id, Long idColaborador, Long idPessoa, String nomePessoa, String email ) {
		super();
		this.id = id;
		this.colaborador = new Colaborador(idColaborador, idPessoa, nomePessoa, "", email);
	}

	public Usuario( Long id, String senha, boolean primeiroAcesso, Long idPerfil, Long idColaborador, Long idPessoa, String nomePessoa, String cpfCnpj, Long idEmpresa, String nomeEmpresa ) {
		this.id = id;
		this.senha = senha;
		this.primeiroAcesso = primeiroAcesso;
		this.perfil = new PerfilUsuario(idPerfil);
		this.colaborador = new Colaborador(idColaborador, idPessoa, nomePessoa, cpfCnpj, idEmpresa, nomeEmpresa);
	}

	public Usuario( Long id, String senha, String token ) {
		super();
		this.id = id;
		this.senha = senha;
		this.token = token;
	}

	public Long getId() {

		return id;
	}

	public void setId(Long id) {

		this.id = id;
	}

	public String getSenha() {

		return senha;
	}

	public void setSenha(String senha) {

		this.senha = senha;
	}

	public String getToken() {

		return token;
	}

	public void setToken(String token) {

		this.token = token;
	}

	@Override
	public String getLabel() {

		return null;
	}

	public PerfilUsuario getPerfil() {

		return perfil;
	}

	public void setPerfil(PerfilUsuario perfil) {

		this.perfil = perfil;
	}

	public String getCodigoRecuperarSenha() {

		return codigoRecuperarSenha;
	}

	public void setCodigoRecuperarSenha(String codigoRecuperarSenha) {

		this.codigoRecuperarSenha = codigoRecuperarSenha;
	}

	public boolean isPrimeiroAcesso() {

		return primeiroAcesso;
	}

	public void setPrimeiroAcesso(boolean primeiroAcesso) {

		this.primeiroAcesso = primeiroAcesso;
	}

	public Date getUltimoAcesso() {

		return ultimoAcesso;
	}

	public void setUltimoAcesso(Date ultimoAcesso) {

		this.ultimoAcesso = ultimoAcesso;
	}

	public Colaborador getColaborador() {

		return colaborador;
	}

	public void setColaborador(Colaborador colaborador) {

		this.colaborador = colaborador;
	}

	public Boolean getEmailEnviado() {

		return emailEnviado;
	}

	public void setEmailEnviado(Boolean emailEnviado) {

		this.emailEnviado = emailEnviado;
	}

}
