package br.com.vitae.bus.entity;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.com.arquitetura.entidade.Entidade;
import br.com.arquitetura.servico.converter.LocalDateAttributeConverter;
import br.com.vitae.bus.enumerator.EnumDocumento;

/**
 * 
 * <p>
 * <b>Title:</b> Documento.java
 * </p>
 * 
 * <p>
 * <b>Description:</b> Classe responsável por representar a entidade Colaborador
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnologia da Informa��o
 * </p>
 * 
 * @version 1.0.0
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "documento")
public class Documento extends Entidade<Long> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "ativo")
	private Boolean ativo;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_colaborador", nullable = false)
	private Colaborador colaborador;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_usuario_adicionou", nullable = false)
	private Usuario usuarioAdicionou;

	@Column(name = "data_evento")
	@Convert(converter = LocalDateAttributeConverter.class)
	private LocalDate dataEvento;

	@Column(name = "data_cadastro")
	@Convert(converter = LocalDateAttributeConverter.class)
	private LocalDate dataCadastro;

	@Column(name = "tipo")
	private EnumDocumento tipo;

	@Column(name = "historico")
	private String historico;

	@Column(name = "tipo_arquivo")
	private String tipoArquivo;

	@Column(name = "nome_arquivo")
	private String nomeArquivo;

	@Column(name = "valor_recebido")
	private BigDecimal valorRecebido;

	// Usado para mostrar o caminho completo do arquivo
	@Transient
	private String caminhoArquivo;

	public Documento() {
	}

	public Documento(Long id, LocalDate dataEvento, EnumDocumento tipo, String historico, BigDecimal valorRecebido,
			String tipoArquivo, String nomeArquivo) {
		super();
		this.id = id;
		this.dataEvento = dataEvento;
		this.tipo = tipo;
		this.historico = historico;
		this.valorRecebido = valorRecebido;
		this.tipoArquivo = tipoArquivo;
		this.nomeArquivo = nomeArquivo;
	}

	public Documento(Long id, Boolean ativo, Colaborador colaborador, Usuario usuarioAdicionou, LocalDate dataEvento,
			LocalDate dataCadastro, EnumDocumento tipo, String historico, String tipoArquivo, BigDecimal valorRecebido,
			String caminhoArquivo, String nomeArquivo) {
		super();
		this.id = id;
		this.ativo = ativo;
		this.colaborador = colaborador;
		this.usuarioAdicionou = usuarioAdicionou;
		this.dataEvento = dataEvento;
		this.dataCadastro = dataCadastro;
		this.tipo = tipo;
		this.historico = historico;
		this.tipoArquivo = tipoArquivo;
		 this.nomeArquivo = nomeArquivo;
		this.valorRecebido = valorRecebido;
		this.caminhoArquivo = caminhoArquivo;
	}

	@Override
	public Long getId() {

		return id;
	}

	@Override
	public void setId(Long id) {

		this.id = id;
	}

	@Override
	public String getLabel() {

		return null;
	}

	public Boolean getAtivo() {

		return ativo;
	}

	public void setAtivo(Boolean ativo) {

		this.ativo = ativo;
	}

	public Colaborador getColaborador() {

		return colaborador;
	}

	public void setColaborador(Colaborador colaborador) {

		this.colaborador = colaborador;
	}

	public Usuario getUsuarioAdicionou() {

		return usuarioAdicionou;
	}

	public void setUsuarioAdicionou(Usuario usuarioAdicionou) {

		this.usuarioAdicionou = usuarioAdicionou;
	}

	public LocalDate getDataCadastro() {

		return dataCadastro;
	}

	public void setDataCadastro(LocalDate dataCadastro) {

		this.dataCadastro = dataCadastro;
	}

	public EnumDocumento getTipo() {

		return tipo;
	}

	public void setTipo(EnumDocumento tipo) {

		this.tipo = tipo;
	}

	public String getHistorico() {

		return historico;
	}

	public void setHistorico(String historico) {

		this.historico = historico;
	}

	public String getTipoArquivo() {

		return tipoArquivo;
	}

	public void setTipoArquivo(String tipoArquivo) {

		this.tipoArquivo = tipoArquivo;
	}

	public String getCaminhoArquivo() {

		return caminhoArquivo;
	}

	public void setCaminhoArquivo(String caminhoArquivo) {

		this.caminhoArquivo = caminhoArquivo;
	}

	public LocalDate getDataEvento() {

		return dataEvento;
	}

	public void setDataEvento(LocalDate dataEvento) {

		this.dataEvento = dataEvento;
	}

	public BigDecimal getValorRecebido() {

		return valorRecebido;
	}

	public void setValorRecebido(BigDecimal valorRecebido) {

		this.valorRecebido = valorRecebido;
	}

	public String getNomeArquivo() {
		return nomeArquivo;
	}

	public void setNomeArquivo(String nomeArquivo) {
		this.nomeArquivo = nomeArquivo;
	}

}
