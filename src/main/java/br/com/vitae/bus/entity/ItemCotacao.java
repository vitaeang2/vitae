package br.com.vitae.bus.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.arquitetura.entidade.Entidade;
import br.com.vitae.bus.enumerator.EnumUnidadeMedida;

/**
 * 
 * <p>
 * <b>Title:</b> CotacaoFornecedorProduto.java
 * </p>
 * 
 * <p>
 * <b>Description:</b> Classe responsável por representar a entidade CotacaoFornecedorProduto
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnologia da Informa��o
 * </p>
 * 
 * @author Joaquim Barros - joaquimbarros.cmp@gmail.com
 * 
 * @version 1.0.0
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "item_cotacao")
public class ItemCotacao extends Entidade<Long> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "valor_unitario")
	private BigDecimal vlrUnitario;

	@Column(name = "quantidade")
	private BigDecimal quantidade;

	@JoinColumn(name = "id_cotacao_fornecedor", nullable = false)
	@ManyToOne(fetch = FetchType.LAZY)
	private CotacaoFornecedor cotacaoFornecedor;

	@JoinColumn(name = "id_produto", nullable = false)
	@ManyToOne(fetch = FetchType.LAZY)
	private Produto produto;

	public ItemCotacao() {
	}

	public ItemCotacao( Long id ) {
		super();
		this.id = id;
	}

	public ItemCotacao( Long id, BigDecimal vlrUnitario, BigDecimal quantidade, CotacaoFornecedor cotacaoFornecedor, Produto produto ) {
		super();
		this.id = id;
		this.vlrUnitario = vlrUnitario;
		this.quantidade = quantidade;
		this.cotacaoFornecedor = cotacaoFornecedor;
		this.produto = produto;
	}

	public ItemCotacao( Long id, BigDecimal vlrUnitario, BigDecimal quantidade, Long idProduto, String nomeProduto, EnumUnidadeMedida unidadeMedida) {
		super();
		this.id = id;
		this.vlrUnitario = vlrUnitario;
		this.quantidade = quantidade;
		this.produto = new Produto(idProduto, nomeProduto, unidadeMedida);
	}

	/**
	 * Retorna o valor do atributo <code>id</code>
	 *
	 * @return <code>Long</code>
	 */
	public Long getId() {

		return id;
	}

	/**
	 * Define o valor do atributo <code>id</code>.
	 *
	 * @param id
	 */
	public void setId(Long id) {

		this.id = id;
	}

	public BigDecimal getVlrUnitario() {

		return vlrUnitario;
	}

	public void setVlrUnitario(BigDecimal vlrUnitario) {

		this.vlrUnitario = vlrUnitario;
	}

	public BigDecimal getQuantidade() {

		return quantidade;
	}

	public void setQuantidade(BigDecimal quantidade) {

		this.quantidade = quantidade;
	}

	public CotacaoFornecedor getCotacaoFornecedor() {

		return cotacaoFornecedor;
	}

	public void setCotacaoFornecedor(CotacaoFornecedor cotacaoFornecedor) {

		this.cotacaoFornecedor = cotacaoFornecedor;
	}

	public Produto getProduto() {

		return produto;
	}

	public void setProduto(Produto produto) {

		this.produto = produto;
	}

	@Override
	public String getLabel() {

		return null;
	}

}
