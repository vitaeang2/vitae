package br.com.vitae.bus.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.com.arquitetura.entidade.Entidade;
import br.com.arquitetura.servico.converter.LocalDateAttributeConverter;
import br.com.vitae.bus.enumerator.EnumAuditoria;
import br.com.vitae.bus.enumerator.EnumOrigem;
import br.com.vitae.bus.enumerator.EnumRegistro;

/**
 * 
 * <p>
 * <b>Title:</b> Colaborador.java
 * </p>
 * 
 * <p>
 * <b>Description:</b> Classe responsável por representar a entidade Colaborador
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnologia da Informa��o
 * </p>
 * 
 * @author Rogério Ferreira - rogerio0308@homtail.com
 * 
 * @version 1.0.0
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "qualidade")
public class Qualidade extends Entidade<Long> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "ativo")
	private Boolean ativo;

	@Column(name = "registro")
	private EnumRegistro registro;

	@Column(name = "origem")
	private EnumOrigem origem;

	@Column(name = "auditoria")
	private EnumAuditoria auditoria;

	@Column(name = "data_criacao")
	@Convert(converter = LocalDateAttributeConverter.class)
	private LocalDate criacao;

	@OneToOne(fetch = FetchType.LAZY)
	private Usuario autor;

	@Column(name = "identificacao")
	private String identificacao;

	@Column(name = "analise_causa")
	private String analise_causa;

	@Column(name = "acao_imediata")
	private String acao_imediata;

	@Column(name = "acao_preventiva")
	private String acao_preventiva;

	@Column(name = "avaliacao_eficacia")
	private String avaliacao_eficacia;

	@ManyToOne(fetch = FetchType.LAZY)
	private Empresa empresa;

	@ManyToOne(fetch = FetchType.LAZY)
	private Nucleo nucleo;

	public Qualidade() {
	}

	/**
	 * 
	 * Responsável pela cria��o de novas instâncias desta classe para ser usada
	 * na tela de listagem, passando somente os campos que se fazem necessario
	 * na datatable.
	 * 
	 * @param id
	 * @param idPessoa
	 * @param nome
	 * @param cpfCnpj
	 */
	public Qualidade(Long id, String identificacao) {
		super();
		this.id = id;
		this.identificacao = identificacao;
	}

	public Qualidade(Long id,

			Boolean ativo,

			EnumRegistro registro,

			EnumOrigem origem,

			EnumAuditoria auditoria,

			LocalDate criacao,

			Usuario autor,

			String identificacao,

			String analise_causa,

			String acao_imediata,

			String acao_preventiva,

			String avaliacao_eficacia,

			Empresa empresa,

			Nucleo nucleo) {
		super();

		this.id = id;

		this.ativo = ativo;
		
		this.registro = registro;
		
		this.origem = origem;
		
		this.auditoria = auditoria;
		
		this.criacao = criacao;
		
		this.autor = autor;
		
		this.identificacao = identificacao;
		
		this.analise_causa = analise_causa;
		
		this.acao_imediata = acao_imediata;
		
		this.acao_preventiva = acao_preventiva;
		
		this.avaliacao_eficacia = avaliacao_eficacia;
		
		this.empresa = empresa;
		
		this.nucleo = nucleo;

		
	}

	/**
	 * Retorna o valor do atributo <code>id</code>
	 *
	 * @return <code>Long</code>
	 */
	public Long getId() {

		return id;
	}

	/**
	 * Define o valor do atributo <code>id</code>.
	 *
	 * @param id
	 */
	public void setId(Long id) {

		this.id = id;
	}

	public Boolean getAtivo() {

		return ativo;
	}

	public void setAtivo(Boolean ativo) {

		this.ativo = ativo;
	}

	public Nucleo getNucleo() {

		return nucleo;
	}

	public void setNucleo(Nucleo nucleo) {

		this.nucleo = nucleo;
	}

	public Empresa getEmpresa() {

		return empresa;
	}

	public void setEmpresa(Empresa empresa) {

		this.empresa = empresa;
	}

	public EnumRegistro getRegistro() {
		return registro;
	}

	public void setRegistro(EnumRegistro registro) {
		this.registro = registro;
	}

	public EnumOrigem getOrigem() {
		return origem;
	}

	public void setOrigem(EnumOrigem origem) {
		this.origem = origem;
	}

	public EnumAuditoria getAuditoria() {
		return auditoria;
	}

	public void setAuditoria(EnumAuditoria auditoria) {
		this.auditoria = auditoria;
	}

	public LocalDate getCriacao() {
		return criacao;
	}

	public void setCriacao(LocalDate criacao) {
		this.criacao = criacao;
	}

	public Usuario getAutor() {
		return autor;
	}

	public void setAutor(Usuario autor) {
		this.autor = autor;
	}

	public String getIdentificacao() {
		return identificacao;
	}

	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}

	public String getAnalise_causa() {
		return analise_causa;
	}

	public void setAnalise_causa(String analise_causa) {
		this.analise_causa = analise_causa;
	}

	public String getAcao_imediata() {
		return acao_imediata;
	}

	public void setAcao_imediata(String acao_imediata) {
		this.acao_imediata = acao_imediata;
	}

	public String getAcao_preventiva() {
		return acao_preventiva;
	}

	public void setAcao_preventiva(String acao_preventiva) {
		this.acao_preventiva = acao_preventiva;
	}

	public String getAvaliacao_eficacia() {
		return avaliacao_eficacia;
	}

	public void setAvaliacao_eficacia(String avaliacao_eficacia) {
		this.avaliacao_eficacia = avaliacao_eficacia;
	}

	@Override
	public String getLabel() {

		// TODO Auto-generated method stub
		return null;
	}

}
