package br.com.vitae.bus.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.arquitetura.entidade.Entidade;

/**
 * 
 * <p>
 * <b>Title:</b> CategoriaProduto.java
 * </p>
 * 
 * <p>
 * <b>Description:</b> Classe responsável de representar a categoria de um Produto
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnologia da Informa��o
 * </p>
 * 
 * @author Joaquim Barros - joaquimbarros.cmp@gmail.com
 * 
 * @version 1.0.0
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "categoria_produto")
public class CategoriaProduto extends Entidade<Long> {

	@Id
	@Column(name = "id", unique = true, nullable = false, insertable = true, updatable = true)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(length = 100, nullable = false)
	private String nome;
	
	@Column(name = "status", length = 1, nullable = true)
	private Boolean ativo;

	public CategoriaProduto() {
	}

	public CategoriaProduto( Long id ) {
		super();
		this.id = id;
	}

	public CategoriaProduto( Long id, String nome ) {
		super();
		this.id = id;
		this.nome = nome;
	}
	
	public CategoriaProduto( Long id, String nome, boolean ativo ) {

		super();
		this.id = id;
		this.nome = nome;
		this.ativo = ativo;
	}

	/**
	 * Retorna o valor do atributo <code>id</code>
	 *
	 * @return <code>Long</code>
	 */
	public Long getId() {

		return id;
	}

	/**
	 * Define o valor do atributo <code>id</code>.
	 *
	 * @param id
	 */
	public void setId(Long id) {

		this.id = id;
	}

	/**
	 * Retorna o valor do atributo <code>nome</code>
	 *
	 * @return <code>String</code>
	 */
	public String getNome() {

		return nome;
	}

	/**
	 * Define o valor do atributo <code>nome</code>.
	 *
	 * @param nome
	 */
	public void setNome(String nome) {

		this.nome = nome;
	}

	@Override
	public String getLabel() {

		// TODO Auto-generated method stub
		return null;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

}
