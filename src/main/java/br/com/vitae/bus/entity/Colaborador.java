package br.com.vitae.bus.entity;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.com.arquitetura.entidade.Entidade;
import br.com.arquitetura.servico.converter.LocalDateAttributeConverter;

/**
 * 
 * <p>
 * <b>Title:</b> Colaborador.java
 * </p>
 * 
 * <p>
 * <b>Description:</b> Classe responsável por representar a entidade Colaborador
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnologia da Informa��o
 * </p>
 * 
 * @author Joaquim Barros - joaquimbarros.cmp@gmail.com
 * 
 * @version 1.0.0
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "colaborador")
public class Colaborador extends Entidade<Long> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "ativo")
	private Boolean ativo;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, optional = false)
	private Pessoa pessoa;

	@ManyToOne(fetch = FetchType.LAZY)
	private Cargo cargo;

	@ManyToOne(fetch = FetchType.LAZY)
	private Empresa empresa;

	@ManyToOne(fetch = FetchType.LAZY)
	private Nucleo nucleo;

	@Column(name = "data_admissao")
	@Convert(converter = LocalDateAttributeConverter.class)
	private LocalDate admissao;

	@Column(name = "data_demissao")
	@Convert(converter = LocalDateAttributeConverter.class)
	private LocalDate demissao;

	@Column(name = "tipo_arquivo")
	private String tipoArquivo;

	@Transient
	private String nomeArquivo;

	public Colaborador() {
	}

	/**
	 * 
	 * Responsável pela cria��o de novas instâncias desta classe para ser usada na tela de listagem, passando somente os campos que se fazem necessario na datatable.
	 * 
	 * @param id
	 * @param idPessoa
	 * @param nome
	 * @param cpfCnpj
	 */
	public Colaborador( Long id, Long idPessoa, String nome, String cpfCnpj ) {
		super();
		this.id = id;
		this.pessoa = new Pessoa(idPessoa, nome, cpfCnpj);
	}

	public Colaborador( Long id, Long idPessoa, String nome, String cpfCnpj, String email ) {
		super();
		this.id = id;
		this.pessoa = new Pessoa(idPessoa, nome, cpfCnpj, email);
	}

	public Colaborador( Long id ) {
		super();
		this.id = id;
	}

	public Colaborador( Long id, Long idPessoa, String nome, String cpfCnpj, Long idEmpresa, String nomeEmpresa ) {
		super();
		this.id = id;
		this.pessoa = new Pessoa(idPessoa, nome, cpfCnpj);
		this.empresa = new Empresa(idEmpresa, nomeEmpresa);
	}

	public Colaborador( Long id, Long idPessoa, String nome, String cpfCnpj, Long idEmpresa, String nomeEmpresa, Nucleo nucleo ) {
		super();
		this.id = id;
		this.pessoa = new Pessoa(idPessoa, nome, cpfCnpj);
		this.empresa = new Empresa(idEmpresa, nomeEmpresa);
		if (nucleo != null) {
			this.nucleo = new Nucleo(nucleo.getId(), nucleo.getNome());
		}
	}

	public Colaborador( Long id, Long idPessoa, String nome, String cpfCnpj, Long idEmpresa, String nomeEmpresa, Long idNucleo, String nomeNucleo ) {
		super();
		this.id = id;
		this.pessoa = new Pessoa(idPessoa, nome, cpfCnpj);
		this.empresa = new Empresa(idEmpresa, nomeEmpresa);
		this.nucleo = new Nucleo(idNucleo, nomeNucleo);
	}

	public Colaborador( Long id, String nomeArquivo, String tipoArquivo, Pessoa pessoa, Cargo cargo, Nucleo nucleo, Empresa empresa, LocalDate admissao, LocalDate demissao, Boolean ativo ) {
		super();
		this.id = id;
		this.nomeArquivo = nomeArquivo;
		this.tipoArquivo = tipoArquivo;
		this.pessoa = pessoa;
		this.cargo = cargo;
		this.nucleo = nucleo;
		this.empresa = empresa;
		this.admissao = admissao;
		this.demissao = demissao;
		this.ativo = ativo;
	}

	public Colaborador( Long id, String nomeArquivo, Boolean ativo, Pessoa pessoa, Cargo cargo, Nucleo nucleo, Empresa empresa, LocalDate admissao, LocalDate demissao, List<AnotacaoColaborador> anotacoes ) {
		super();
		this.id = id;
		this.nomeArquivo = nomeArquivo;
		this.ativo = ativo;
		this.pessoa = pessoa;
		this.cargo = cargo;
		this.nucleo = nucleo;
		this.empresa = empresa;
		this.admissao = admissao;
		this.demissao = demissao;
		// this.anotacoes = anotacoes;
	}

	// TODO Definir como vai ficar o horario de trabalho, se vai ser só horario ou string
	// TODO Definir arquivos digitalizados - Salvar pelo tipo

	// @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "colaborador", orphanRemoval = true)
	// private List<AnotacaoColaborador> anotacoes;

	/**
	 * Retorna o valor do atributo <code>id</code>
	 *
	 * @return <code>Long</code>
	 */
	public Long getId() {

		return id;
	}

	/**
	 * Define o valor do atributo <code>id</code>.
	 *
	 * @param id
	 */
	public void setId(Long id) {

		this.id = id;
	}

	/**
	 * Retorna o valor do atributo <code>pessoa</code>
	 *
	 * @return <code>Pessoa</code>
	 */
	public Pessoa getPessoa() {

		return pessoa;
	}

	/**
	 * Define o valor do atributo <code>pessoa</code>.
	 *
	 * @param pessoa
	 */
	public void setPessoa(Pessoa pessoa) {

		this.pessoa = pessoa;
	}

	/**
	 * Retorna o valor do atributo <code>cargo</code>
	 *
	 * @return <code>Cargo</code>
	 */
	public Cargo getCargo() {

		return cargo;
	}

	/**
	 * Define o valor do atributo <code>cargo</code>.
	 *
	 * @param cargo
	 */
	public void setCargo(Cargo cargo) {

		this.cargo = cargo;
	}

	/**
	 * Retorna o valor do atributo <code>admissao</code>
	 *
	 * @return <code>LocalDate</code>
	 */
	public LocalDate getAdmissao() {

		return admissao;
	}

	/**
	 * Define o valor do atributo <code>admissao</code>.
	 *
	 * @param admissao
	 */
	public void setAdmissao(LocalDate admissao) {

		this.admissao = admissao;
	}

	/**
	 * Retorna o valor do atributo <code>demissao</code>
	 *
	 * @return <code>LocalDate</code>
	 */
	public LocalDate getDemissao() {

		return demissao;
	}

	/**
	 * Define o valor do atributo <code>demissao</code>.
	 *
	 * @param demissao
	 */
	public void setDemissao(LocalDate demissao) {

		this.demissao = demissao;
	}

	/**
	 * Retorna o valor do atributo <code>anotacoes</code>
	 *
	 * @return <code>List<AnotacaoColaborador></code>
	 */
	// public List<AnotacaoColaborador> getAnotacoes() {
	//
	// return anotacoes;
	// }
	//
	// /**
	// * Define o valor do atributo <code>anotacoes</code>.
	// *
	// * @param anotacoes
	// */
	// public void setAnotacoes(List<AnotacaoColaborador> anotacoes) {
	//
	// this.anotacoes = anotacoes;
	// }

	public Boolean getAtivo() {

		return ativo;
	}

	public void setAtivo(Boolean ativo) {

		this.ativo = ativo;
	}

	public Nucleo getNucleo() {

		return nucleo;
	}

	public void setNucleo(Nucleo nucleo) {

		this.nucleo = nucleo;
	}

	public Empresa getEmpresa() {

		return empresa;
	}

	public void setEmpresa(Empresa empresa) {

		this.empresa = empresa;
	}

	@Override
	public String getLabel() {

		// TODO Auto-generated method stub
		return null;
	}

	public String getNomeArquivo() {

		return nomeArquivo;
	}

	public void setNomeArquivo(String nomeArquivo) {

		this.nomeArquivo = nomeArquivo;
	}

	public String getTipoArquivo() {

		return tipoArquivo;
	}

	public void setTipoArquivo(String tipoArquivo) {

		this.tipoArquivo = tipoArquivo;
	}

}
