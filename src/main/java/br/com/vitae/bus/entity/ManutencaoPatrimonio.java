package br.com.vitae.bus.entity;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.com.arquitetura.entidade.Entidade;
import br.com.arquitetura.servico.converter.LocalDateAttributeConverter;

/**
 * 
 * <p>
 * <b>Title:</b> Produto.java
 * </p>
 * 
 * <p>
 * <b>Description:</b> Classe responsável de representar a entidade Produto
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnologia da Informa��o
 * </p>
 * 
 * @author Rogério Ferreira - rogerio0308@homtail.com
 * 
 * @version 1.0.0
 */

@SuppressWarnings("serial")
@Entity
@Table(name = "manutencao_patrimonio")
public class ManutencaoPatrimonio extends Entidade<Long> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "motivo", length = 200, nullable = false)
	private String motivo;

	@Column(name = "descricao_retorno", length = 200, nullable = false)
	private String descricaoRetorno;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	private Fornecedor fornecedor;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	private Patrimonio patrimonio;

	@Column(name = "data_saida")
	@Convert(converter = LocalDateAttributeConverter.class)
	private LocalDate dataSaida;

	@Column(name = "data_retorno")
	@Convert(converter = LocalDateAttributeConverter.class)
	private LocalDate dataRetorno;

	@Column(name = "data_prevista_retorno")
	@Convert(converter = LocalDateAttributeConverter.class)
	private LocalDate dataPrevistaRetorno;

	@Column(name = "fim_garantia")
	@Convert(converter = LocalDateAttributeConverter.class)
	private LocalDate fimGarantia;

	@Column(name = "valor")
	private BigDecimal valor;

	@Column(name = "valor_pago")
	private BigDecimal valorPago;

	@Column(name = "status", length = 1, nullable = true)
	private Boolean ativo;

	@Transient
	private Long idUsuario;

	/**
	 * 
	 * Responsável pela cria��o de novas instâncias desta classe.
	 */
	public ManutencaoPatrimonio() {

	}
	

	public ManutencaoPatrimonio( Long id, String motivo, Boolean ativo ) {

		super();
		this.id = id;
		this.motivo = motivo;
		this.ativo = ativo;
	}

	public ManutencaoPatrimonio(Long id) {

		super();
		this.id = id;
	}

	public ManutencaoPatrimonio(Long id,

			String motivo,

			String descricaoRetorno,

			Fornecedor fornecedor,

			Patrimonio patrimonio,

			LocalDate dataSaida,

			LocalDate dataRetorno,

			LocalDate dataPrevistaRetorno,

			LocalDate fimGarantia,

			BigDecimal valor,

			BigDecimal valorPago,

			Boolean ativo) {

		super();
		this.id = id;

		this.motivo = motivo;

		this.descricaoRetorno = descricaoRetorno;

		this.fornecedor = fornecedor;

		this.patrimonio = patrimonio;

		this.dataSaida = dataSaida;

		this.dataRetorno = dataRetorno;

		this.dataPrevistaRetorno = dataPrevistaRetorno;

		this.fimGarantia = fimGarantia;

		this.valor = valor;

		this.valorPago = valorPago;

		this.ativo = ativo;
	}

	/**
	 * Retorna o valor do atributo <code>id</code>
	 *
	 * @return <code>Long</code>
	 */
	public Long getId() {

		return id;
	}

	/**
	 * Define o valor do atributo <code>id</code>.
	 *
	 * @param id
	 */
	public void setId(Long id) {

		this.id = id;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public String getDescricaoRetorno() {
		return descricaoRetorno;
	}

	public void setDescricaoRetorno(String descricaoRetorno) {
		this.descricaoRetorno = descricaoRetorno;
	}

	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public Patrimonio getPatrimonio() {
		return patrimonio;
	}

	public void setPatrimonio(Patrimonio patrimonio) {
		this.patrimonio = patrimonio;
	}

	public LocalDate getDataSaida() {
		return dataSaida;
	}

	public void setDataSaida(LocalDate dataSaida) {
		this.dataSaida = dataSaida;
	}

	public LocalDate getDataRetorno() {
		return dataRetorno;
	}

	public void setDataRetorno(LocalDate dataRetorno) {
		this.dataRetorno = dataRetorno;
	}

	public LocalDate getDataPrevistaRetorno() {
		return dataPrevistaRetorno;
	}

	public void setDataPrevistaRetorno(LocalDate dataPrevistaRetorno) {
		this.dataPrevistaRetorno = dataPrevistaRetorno;
	}

	public LocalDate getFimGarantia() {
		return fimGarantia;
	}

	public void setFimGarantia(LocalDate fimGarantia) {
		this.fimGarantia = fimGarantia;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public BigDecimal getValorPago() {
		return valorPago;
	}

	public void setValorPago(BigDecimal valorPago) {
		this.valorPago = valorPago;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	@Override
	public String getLabel() {

		// TODO Auto-generated method stub
		return null;
	}

}
