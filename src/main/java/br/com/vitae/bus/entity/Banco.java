package br.com.vitae.bus.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.arquitetura.entidade.Entidade;

/**
 * 
 * <p>
 * <b>Title:</b> Banco.java
 * </p>
 * 
 * <p>
 * <b>Description:</b> Classe responsável por representar a entidade Banco
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnologia da Informa��o
 * </p>
 * 
 * @author Joaquim Barros - joaquimbarros.cmp@gmail.com
 * 
 * @version 1.0.0
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "banco")
public class Banco extends Entidade<Long> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(length = 10)
	private String codigoBanco;

	@Column(length = 100, nullable = false)
	private String nome;

	/**
	 * Retorna o valor do atributo <code>id</code>
	 *
	 * @return <code>Long</code>
	 */
	public Long getId() {

		return id;
	}

	/**
	 * Define o valor do atributo <code>id</code>.
	 *
	 * @param id
	 */
	public void setId(Long id) {

		this.id = id;
	}

	/**
	 * Retorna o valor do atributo <code>codigoBanco</code>
	 *
	 * @return <code>String</code>
	 */
	public String getCodigoBanco() {

		return codigoBanco;
	}

	/**
	 * Define o valor do atributo <code>codigoBanco</code>.
	 *
	 * @param codigoBanco
	 */
	public void setCodigoBanco(String codigoBanco) {

		this.codigoBanco = codigoBanco;
	}

	/**
	 * Retorna o valor do atributo <code>nome</code>
	 *
	 * @return <code>String</code>
	 */
	public String getNome() {

		return nome;
	}

	/**
	 * Define o valor do atributo <code>nome</code>.
	 *
	 * @param nome
	 */
	public void setNome(String nome) {

		this.nome = nome;
	}

	@Override
	public String getLabel() {

		// TODO Auto-generated method stub
		return null;
	}

}
