package br.com.vitae.bus.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import br.com.arquitetura.entidade.Entidade;

@SuppressWarnings("serial")
@Entity
public class Fornecedor extends Entidade<Long> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "status", length = 1, nullable = true)
	private Boolean ativo;
	
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, optional = false)
	private Pessoa pessoa;

	@Column(name = "nome_contato")
	private String nomeContato;

	@Column(name = "celular_contato")
	private String celularContato;
	
	@Column(name = "prestador_servico")
	private boolean prestadorServico;

	public Fornecedor() {
	}

	public Fornecedor( Long id ) {
		super();
		this.id = id;
	}
	
	public Fornecedor( Long id, String nomeContato, Long idPessoa, String nomePessoa, String cpfCnpj, String email) {
		this.id = id;
		this.nomeContato = nomeContato;
		this.pessoa = new Pessoa(idPessoa, nomePessoa, cpfCnpj, email);
	}
	
	public Fornecedor( Long id, String nomeContato, Long idPessoa, String nomePessoa, String cpfCnpj, String email, boolean prestadorServico) {
		this.id = id;
		this.nomeContato = nomeContato;
		this.prestadorServico = prestadorServico;
		this.pessoa = new Pessoa(idPessoa, nomePessoa, cpfCnpj, email);
	}
	
	public Fornecedor( Long id, Long idPessoa, String nomePessoa, String cpfCnpj, String email, boolean prestadorServico ) {
		this.id = id;
		this.prestadorServico = prestadorServico;
		this.pessoa = new Pessoa(idPessoa, nomePessoa, cpfCnpj, email);
	}

	public Fornecedor( Long id, Pessoa pessoa, String nomeContato, String celularContato ) {
		super();
		this.id = id;
		this.pessoa = pessoa;
		this.nomeContato = nomeContato;
		this.celularContato = celularContato;
	}
	
	public Fornecedor( Long id, Boolean ativo, Pessoa pessoa, String nomeContato, String celularContato, boolean prestadorServico ) {
		super();
		this.id = id;
		this.ativo = ativo;
		this.pessoa = pessoa;
		this.nomeContato = nomeContato;
		this.celularContato = celularContato;
		this.prestadorServico = prestadorServico;
	}

	/**
	 * Retorna o valor do atributo <code>id</code>
	 *
	 * @return <code>Long</code>
	 */
	public Long getId() {

		return id;
	}

	/**
	 * Define o valor do atributo <code>id</code>.
	 *
	 * @param id
	 */
	public void setId(Long id) {

		this.id = id;
	}
	
	

	
	/**
	 * Retorna o valor do atributo <code>ativo</code>
	 *
	 * @return <code>Boolean</code>
	 */
	public Boolean getAtivo() {
	
		return ativo;
	}

	
	/**
	 * Define o valor do atributo <code>ativo</code>.
	 *
	 * @param ativo 
	 */
	public void setAtivo(Boolean ativo) {
	
		this.ativo = ativo;
	}

	/**
	 * Retorna o valor do atributo <code>pessoa</code>
	 *
	 * @return <code>Pessoa</code>
	 */
	public Pessoa getPessoa() {

		return pessoa;
	}

	/**
	 * Define o valor do atributo <code>pessoa</code>.
	 *
	 * @param pessoa
	 */
	public void setPessoa(Pessoa pessoa) {

		this.pessoa = pessoa;
	}

	/**
	 * Retorna o valor do atributo <code>nomeContato</code>
	 *
	 * @return <code>String</code>
	 */
	public String getNomeContato() {

		return nomeContato;
	}

	/**
	 * Define o valor do atributo <code>nomeContato</code>.
	 *
	 * @param nomeContato
	 */
	public void setNomeContato(String nomeContato) {

		this.nomeContato = nomeContato;
	}

	/**
	 * Retorna o valor do atributo <code>celularContato</code>
	 *
	 * @return <code>String</code>
	 */
	public String getCelularContato() {

		return celularContato;
	}

	/**
	 * Define o valor do atributo <code>celularContato</code>.
	 *
	 * @param celularContato
	 */
	public void setCelularContato(String celularContato) {

		this.celularContato = celularContato;
	}

	
	public boolean isPrestadorServico() {
	
		return prestadorServico;
	}

	
	public void setPrestadorServico(boolean prestadorServico) {
	
		this.prestadorServico = prestadorServico;
	}

	@Override
	public String getLabel() {

		// TODO Auto-generated method stub
		return null;
	}

}
