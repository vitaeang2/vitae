package br.com.vitae.bus.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.arquitetura.entidade.Entidade;

/**
 * 
 * <p>
 * <b>Title:</b> GrupoBem.java
 * </p>
 * 
 * <p>
 * <b>Description:</b> Classe responsável por representar a entidade GrupoBem do Patrimonio
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnologia da Informa��o
 * </p>
 * 
 * @author Rogério Ferreira - rogerio0308@homtail.com
 * 
 * @version 1.0.0
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "grupo_bem")
public class GrupoBem extends Entidade<Long> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(length = 200, nullable = false)
	private String nome;
	
	@Column(length = 200, nullable = true)
	private String observacao;

	@Column(name = "status", length = 1, nullable = true)
	private Boolean ativo;

	@Column(name = "depreciacao_anual")
	private BigDecimal depreciacaoAnual;

	public GrupoBem() {
	}

	public GrupoBem(Long id) {
		super();
		this.id = id;
	}

	public GrupoBem(Long id, String nome) {
		super();
		this.id = id;
		this.nome = nome;
	}

	public GrupoBem(Long id, String nome, boolean ativo, BigDecimal depreciacaoAnual, String observacao) {
		super();
		this.id = id;
		this.nome = nome;
		this.ativo = ativo;
		this.depreciacaoAnual = depreciacaoAnual;
		this.observacao = observacao;
	}

	/**
	 * Retorna o valor do atributo <code>id</code>
	 *
	 * @return <code>Long</code>
	 */
	public Long getId() {

		return id;
	}

	/**
	 * Define o valor do atributo <code>id</code>.
	 *
	 * @param id
	 */
	public void setId(Long id) {

		this.id = id;
	}

	/**
	 * Retorna o valor do atributo <code>nome</code>
	 *
	 * @return <code>String</code>
	 */
	public String getNome() {

		return nome;
	}

	/**
	 * Define o valor do atributo <code>nome</code>.
	 *
	 * @param nome
	 */
	public void setNome(String nome) {

		this.nome = nome;
	}

	@Override
	public String getLabel() {

		// TODO Auto-generated method stub
		return null;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public BigDecimal getDepreciacaoAnual() {
		return depreciacaoAnual;
	}

	public void setDepreciacaoAnual(BigDecimal depreciacaoAnual) {
		this.depreciacaoAnual = depreciacaoAnual;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
}
