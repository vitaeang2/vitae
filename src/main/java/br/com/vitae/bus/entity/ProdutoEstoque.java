package br.com.vitae.bus.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.arquitetura.entidade.Entidade;
import br.com.vitae.bus.dto.ProdutoEstoqueDTO;
import br.com.vitae.bus.enumerator.EnumUnidadeMedida;

@SuppressWarnings("serial")
@Entity
// @Table(name = "produto_estoque", uniqueConstraints = @UniqueConstraint(columnNames = {"empresa_id", "nucleo_id", "produto_id" }))
@Table(name = "produto_estoque")
public class ProdutoEstoque extends Entidade<Long> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "quantidade")
	private BigDecimal quantidade;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	private Produto produto;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	private Empresa empresa;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	private Nucleo nucleo;
	
	public ProdutoEstoque() {

	}
	
	public ProdutoEstoque( ProdutoEstoqueDTO produtoEstoqueDTO ) {

	}

	public ProdutoEstoque( Long id, BigDecimal quantidade ) {

		super();
		this.id = id;
		this.quantidade = quantidade;
	}

	public ProdutoEstoque( Long id, BigDecimal quantidade, Long idProduto, String nomeProduto, BigDecimal valorVenda, BigDecimal valorCusto, EnumUnidadeMedida unidadeMedida ) {
		
		super();
		this.id = id;
		this.quantidade = quantidade;
		this.produto = new Produto(id, nomeProduto, valorVenda, valorCusto, unidadeMedida);
	}

	public ProdutoEstoque( Long id, BigDecimal quantidade, Long idProduto, String nomeProduto, BigDecimal valorVenda, BigDecimal valorCusto, BigDecimal minimoEmEstoque, EnumUnidadeMedida unidadeMedida ) {
		
		super();
		this.id = id;
		this.quantidade = quantidade;
		this.produto = new Produto(id, nomeProduto, valorVenda, valorCusto, minimoEmEstoque, unidadeMedida);
	}

	public ProdutoEstoque( Long id, BigDecimal quantidade, Produto produto, Empresa empresa, Nucleo nucleo ) {

		super();
		this.id = id;
		this.quantidade = quantidade;
		this.produto = produto;
		this.empresa = empresa;
		this.nucleo = nucleo;
	}

	/**
	 * Retorna o valor do atributo <code>id</code>
	 *
	 * @return <code>Long</code>
	 */
	public Long getId() {

		return id;
	}

	/**
	 * Define o valor do atributo <code>id</code>.
	 *
	 * @param id
	 */
	public void setId(Long id) {

		this.id = id;
	}

	/**
	 * Retorna o valor do atributo <code>quantidade</code>
	 *
	 * @return <code>BigDecimal</code>
	 */
	public BigDecimal getQuantidade() {

		return quantidade;
	}

	/**
	 * Define o valor do atributo <code>quantidade</code>.
	 *
	 * @param quantidade
	 */
	public void setQuantidade(BigDecimal quantidade) {

		this.quantidade = quantidade;
	}

	/**
	 * Retorna o valor do atributo <code>produto</code>
	 *
	 * @return <code>Produto</code>
	 */
	public Produto getProduto() {

		return produto;
	}

	/**
	 * Define o valor do atributo <code>produto</code>.
	 *
	 * @param produto
	 */
	public void setProduto(Produto produto) {

		this.produto = produto;
	}

	/**
	 * Retorna o valor do atributo <code>empresa</code>
	 *
	 * @return <code>Empresa</code>
	 */
	public Empresa getEmpresa() {

		return empresa;
	}

	/**
	 * Define o valor do atributo <code>empresa</code>.
	 *
	 * @param empresa
	 */
	public void setEmpresa(Empresa empresa) {

		this.empresa = empresa;
	}

	/**
	 * Retorna o valor do atributo <code>nucleo</code>
	 *
	 * @return <code>Nucleo</code>
	 */
	public Nucleo getNucleo() {

		return nucleo;
	}

	/**
	 * Define o valor do atributo <code>nucleo</code>.
	 *
	 * @param nucleo
	 */
	public void setNucleo(Nucleo nucleo) {

		this.nucleo = nucleo;
	}

	@Override
	public String getLabel() {

		// TODO Auto-generated method stub
		return null;
	}

}
