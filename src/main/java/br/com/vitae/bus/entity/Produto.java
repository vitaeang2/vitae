package br.com.vitae.bus.entity;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import br.com.arquitetura.entidade.Entidade;
import br.com.arquitetura.servico.converter.LocalDateAttributeConverter;
import br.com.vitae.bus.enumerator.EnumUnidadeMedida;

/**
 * 
 * <p>
 * <b>Title:</b> Produto.java
 * </p>
 * 
 * <p>
 * <b>Description:</b> Classe responsável de representar a entidade Produto
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnologia da Informa��o
 * </p>
 * 
 * @author Joaquim Barros - joaquimbarros.cmp@gmail.com
 * 
 * @version 1.0.0
 */

@SuppressWarnings("serial")
@Entity
public class Produto extends Entidade<Long> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "nome", length = 200, nullable = false)
	private String nome;

	@Column(name = "data_cadastro")
	@Convert(converter = LocalDateAttributeConverter.class)
	private LocalDate dataCadastro;

	@Column(name = "codigo")
	private String codigo;

	@Column(name = "valor_venda")
	private BigDecimal valorVenda;

	@Column(name = "valor_custo")
	private BigDecimal valorCusto;

	// @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, optional = false)
	// private ProdutoEstoque produtoEstoque;

	@Column(name = "minimo_em_estoque")
	private BigDecimal minimoEmEstoque;

	@Column(name = "maximo_em_estoque")
	private BigDecimal maximoEmEstoque;

	@Column(name = "unidade_medida")
	private EnumUnidadeMedida unidadeMedida;

	@Column(name = "ativo")
	private Boolean ativo;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	private Fornecedor fornecedor;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	private Fabricante fabricante;

	@ManyToOne(fetch = FetchType.LAZY)
	private CategoriaProduto categoria;

	@Transient
	private Long idUsuario;

	/**
	 * 
	 * Responsável pela cria��o de novas instâncias desta classe.
	 */
	public Produto() {

	}

	public Produto( Long id, String nome, Boolean ativo ) {

		super();
		this.id = id;
		this.nome = nome;
		this.ativo = ativo;
	}

	public Produto( Long id ) {

		super();
		this.id = id;
	}

	public Produto( Long id, String nome, EnumUnidadeMedida unidadeMedida) {

		this.id = id;
		this.nome = nome;
		this.unidadeMedida = unidadeMedida;
	}

	/**
	 * 
	 * Responsável pela cria��o de novas instâncias desta classe. Usada na Datatable
	 * 
	 * 
	 * @param id
	 * @param nome
	 * @param valorVenda
	 * @param valorCusto
	 */
	public Produto( Long id, String nome, BigDecimal valorVenda, BigDecimal valorCusto, BigDecimal minimoEmEstoque, Boolean ativo) {

		super();
		this.id = id;
		this.nome = nome;
		this.valorVenda = valorVenda;
		this.valorCusto = valorCusto;
		this.minimoEmEstoque = minimoEmEstoque;
		this.ativo = ativo;
	}
	
	public Produto( Long id, String nome, BigDecimal valorVenda, BigDecimal valorCusto, EnumUnidadeMedida unidadeMedida) {

		super();
		this.id = id;
		this.nome = nome;
		this.valorVenda = valorVenda;
		this.valorCusto = valorCusto;
		this.unidadeMedida = unidadeMedida;
	}

	public Produto( Long id, String nome, BigDecimal valorVenda, BigDecimal valorCusto, BigDecimal minimoEmEstoque, EnumUnidadeMedida unidadeMedida) {
		
		super();
		this.id = id;
		this.nome = nome;
		this.valorVenda = valorVenda;
		this.valorCusto = valorCusto;
		this.minimoEmEstoque = minimoEmEstoque;
		this.unidadeMedida = unidadeMedida;
	}

	public Produto( Long id, String nome, LocalDate dataCadastro, String codigo, BigDecimal valorVenda, BigDecimal valorCusto, ProdutoEstoque produtoEstoque, BigDecimal minimoEmEstoque, BigDecimal maximoEmEstoque, EnumUnidadeMedida unidadeMedida, Fornecedor fornecedor, Fabricante fabricante, CategoriaProduto categoria, Boolean ativo ) {

		super();
		this.id = id;
		this.nome = nome;
		this.dataCadastro = dataCadastro;
		this.codigo = codigo;
		this.valorVenda = valorVenda;
		this.valorCusto = valorCusto;
		// this.produtoEstoque = produtoEstoque;
		this.minimoEmEstoque = minimoEmEstoque;
		this.maximoEmEstoque = maximoEmEstoque;
		this.unidadeMedida = unidadeMedida;
		this.fornecedor = fornecedor;
		this.fabricante = fabricante;
		this.categoria = categoria;
		this.ativo = ativo;
	}

	/**
	 * Retorna o valor do atributo <code>id</code>
	 *
	 * @return <code>Long</code>
	 */
	public Long getId() {

		return id;
	}

	/**
	 * Define o valor do atributo <code>id</code>.
	 *
	 * @param id
	 */
	public void setId(Long id) {

		this.id = id;
	}

	/**
	 * Retorna o valor do atributo <code>nome</code>
	 *
	 * @return <code>String</code>
	 */
	public String getNome() {

		return nome;
	}

	/**
	 * Define o valor do atributo <code>nome</code>.
	 *
	 * @param nome
	 */
	public void setNome(String nome) {

		this.nome = nome;
	}

	/**
	 * Retorna o valor do atributo <code>dataCadastro</code>
	 *
	 * @return <code>LocalDate</code>
	 */
	public LocalDate getDataCadastro() {

		return dataCadastro;
	}

	/**
	 * Define o valor do atributo <code>dataCadastro</code>.
	 *
	 * @param dataCadastro
	 */
	public void setDataCadastro(LocalDate dataCadastro) {

		this.dataCadastro = dataCadastro;
	}

	/**
	 * Retorna o valor do atributo <code>codigo</code>
	 *
	 * @return <code>String</code>
	 */
	public String getCodigo() {

		return codigo;
	}

	/**
	 * Define o valor do atributo <code>codigo</code>.
	 *
	 * @param codigo
	 */
	public void setCodigo(String codigo) {

		this.codigo = codigo;
	}

	/**
	 * Retorna o valor do atributo <code>valorVenda</code>
	 *
	 * @return <code>BigDecimal</code>
	 */
	public BigDecimal getValorVenda() {

		return valorVenda;
	}

	/**
	 * Define o valor do atributo <code>valorVenda</code>.
	 *
	 * @param valorVenda
	 */
	public void setValorVenda(BigDecimal valorVenda) {

		this.valorVenda = valorVenda;
	}

	/**
	 * Retorna o valor do atributo <code>valorCusto</code>
	 *
	 * @return <code>BigDecimal</code>
	 */
	public BigDecimal getValorCusto() {

		return valorCusto;
	}

	/**
	 * Define o valor do atributo <code>valorCusto</code>.
	 *
	 * @param valorCusto
	 */
	public void setValorCusto(BigDecimal valorCusto) {

		this.valorCusto = valorCusto;
	}

	/**
	 * Retorna o valor do atributo <code>minimoEmEstoque</code>
	 *
	 * @return <code>BigDecimal</code>
	 */
	public BigDecimal getMinimoEmEstoque() {

		return minimoEmEstoque;
	}

	/**
	 * Define o valor do atributo <code>minimoEmEstoque</code>.
	 *
	 * @param minimoEmEstoque
	 */
	public void setMinimoEmEstoque(BigDecimal minimoEmEstoque) {

		this.minimoEmEstoque = minimoEmEstoque;
	}

	/**
	 * Retorna o valor do atributo <code>maximoEmEstoque</code>
	 *
	 * @return <code>BigDecimal</code>
	 */
	public BigDecimal getMaximoEmEstoque() {

		return maximoEmEstoque;
	}

	/**
	 * Define o valor do atributo <code>maximoEmEstoque</code>.
	 *
	 * @param maximoEmEstoque
	 */
	public void setMaximoEmEstoque(BigDecimal maximoEmEstoque) {

		this.maximoEmEstoque = maximoEmEstoque;
	}

	/**
	 * Retorna o valor do atributo <code>unidadeMedida</code>
	 *
	 * @return <code>EnumUnidadeMedida</code>
	 */
	public EnumUnidadeMedida getUnidadeMedida() {

		return unidadeMedida;
	}

	/**
	 * Define o valor do atributo <code>unidadeMedida</code>.
	 *
	 * @param unidadeMedida
	 */
	public void setUnidadeMedida(EnumUnidadeMedida unidadeMedida) {

		this.unidadeMedida = unidadeMedida;
	}

	/**
	 * Retorna o valor do atributo <code>fornecedor</code>
	 *
	 * @return <code>Fornecedor</code>
	 */
	public Fornecedor getFornecedor() {

		return fornecedor;
	}

	/**
	 * Define o valor do atributo <code>fornecedor</code>.
	 *
	 * @param fornecedor
	 */
	public void setFornecedor(Fornecedor fornecedor) {

		this.fornecedor = fornecedor;
	}

	/**
	 * Retorna o valor do atributo <code>fabricante</code>
	 *
	 * @return <code>Fabricante</code>
	 */
	public Fabricante getFabricante() {

		return fabricante;
	}

	/**
	 * Define o valor do atributo <code>fabricante</code>.
	 *
	 * @param fabricante
	 */
	public void setFabricante(Fabricante fabricante) {

		this.fabricante = fabricante;
	}

	/**
	 * Retorna o valor do atributo <code>categoria</code>
	 *
	 * @return <code>CategoriaProduto</code>
	 */
	public CategoriaProduto getCategoria() {

		return categoria;
	}

	/**
	 * Define o valor do atributo <code>categoria</code>.
	 *
	 * @param categoria
	 */
	public void setCategoria(CategoriaProduto categoria) {

		this.categoria = categoria;
	}

	public Long getIdUsuario() {

		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {

		this.idUsuario = idUsuario;
	}

	public Boolean getAtivo() {

		return ativo;
	}

	public void setAtivo(Boolean ativo) {

		this.ativo = ativo;
	}

	@Override
	public String getLabel() {

		// TODO Auto-generated method stub
		return null;
	}

}
