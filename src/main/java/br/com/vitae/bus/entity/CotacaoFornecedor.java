package br.com.vitae.bus.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.arquitetura.entidade.Entidade;
import br.com.vitae.bus.enumerator.EnumStatusCotacaoFornecedor;

/**
 * 
 * <p>
 * <b>Title:</b> CotacaoFornecedor.java
 * </p>
 * 
 * <p>
 * <b>Description:</b> Classe responsável por representar a entidade CotacaoFornecedor
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnologia da Informa��o
 * </p>
 * 
 * @author Joaquim Barros - joaquimbarros.cmp@gmail.com
 * 
 * @version 1.0.0
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "cotacao_fornecedor")
public class CotacaoFornecedor extends Entidade<Long> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "valor_frete")
	private BigDecimal vlrFrete;

	@Column(name = "valor_desconto")
	private BigDecimal vlrDesconto;

	@Column(name = "token")
	private String token;

	@JoinColumn(name = "id_cotacao", nullable = false)
	@ManyToOne(fetch = FetchType.LAZY)
	private Cotacao cotacao;

	@JoinColumn(name = "id_fornecedor", nullable = false)
	@ManyToOne(fetch = FetchType.LAZY)
	private Fornecedor fornecedor;

	@Column(name = "status_cotacao_fornecedor")
	//@Enumerated(EnumType.ORDINAL)
	private EnumStatusCotacaoFornecedor status;

	public CotacaoFornecedor() {
	}

	public CotacaoFornecedor( Long id ) {
		super();
		this.id = id;
	}
	
	public CotacaoFornecedor( Long id, Long idFornecedor ) {
		super();
		this.id = id;
		this.fornecedor = new Fornecedor(idFornecedor);
	}


	public CotacaoFornecedor( Long id, String token, EnumStatusCotacaoFornecedor status, Long idCotacao, Long idFornecedor, String nomeContato, Long idPessoa, String nomePessoa, String cpfCnpj, String email, boolean prestadorServico ) {
		super();
		this.id = id;
		this.token = token;
		this.cotacao = new Cotacao(idCotacao);
		this.fornecedor = new Fornecedor(idFornecedor, nomeContato, idPessoa, nomePessoa, cpfCnpj, email, prestadorServico);
		this.status = status;
	}

	public CotacaoFornecedor( Long id, BigDecimal vlrFrete, BigDecimal vlrDesconto, String token, Cotacao cotacao, Fornecedor fornecedor, EnumStatusCotacaoFornecedor status ) {
		super();
		this.id = id;
		this.vlrFrete = vlrFrete;
		this.vlrDesconto = vlrDesconto;
		this.token = token;
		this.cotacao = cotacao;
		this.fornecedor = fornecedor;
		this.status = status;
	}

	/**
	 * Retorna o valor do atributo <code>id</code>
	 *
	 * @return <code>Long</code>
	 */
	public Long getId() {

		return id;
	}

	/**
	 * Define o valor do atributo <code>id</code>.
	 *
	 * @param id
	 */
	public void setId(Long id) {

		this.id = id;
	}

	/**
	 * 
	 * Método responsável por
	 *
	 * @author Sergio Filho - sergioadsf@gmail.com
	 *
	 * @return
	 */
	public Cotacao getCotacao() {

		return cotacao;
	}

	/**
	 * 
	 * Método responsável por
	 *
	 * @author Sergio Filho - sergioadsf@gmail.com
	 *
	 * @param cotacao
	 */
	public void setCotacao(Cotacao cotacao) {

		this.cotacao = cotacao;
	}

	/**
	 * 
	 * Método responsável por
	 *
	 * @author Sergio Filho - sergioadsf@gmail.com
	 *
	 * @return
	 */
	public Fornecedor getFornecedor() {

		return fornecedor;
	}

	/**
	 * 
	 * Método responsável por
	 *
	 * @author Sergio Filho - sergioadsf@gmail.com
	 *
	 * @param fornecedor
	 */
	public void setFornecedor(Fornecedor fornecedor) {

		this.fornecedor = fornecedor;
	}

	public BigDecimal getVlrFrete() {

		return vlrFrete;
	}

	public void setVlrFrete(BigDecimal vlrFrete) {

		this.vlrFrete = vlrFrete;
	}

	public BigDecimal getVlrDesconto() {

		return vlrDesconto;
	}

	public void setVlrDesconto(BigDecimal vlrDesconto) {

		this.vlrDesconto = vlrDesconto;
	}

	public String getToken() {

		return token;
	}

	public void setToken(String token) {

		this.token = token;
	}

	public EnumStatusCotacaoFornecedor getStatus() {

		return status;
	}

	public void setStatus(EnumStatusCotacaoFornecedor status) {

		this.status = status;
	}

	@Override
	public String getLabel() {

		return null;
	}

}
