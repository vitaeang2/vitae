package br.com.vitae.bus.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.arquitetura.entidade.Entidade;
import br.com.arquitetura.servico.converter.LocalDateAttributeConverter;
import br.com.vitae.bus.enumerator.EnumStatusCotacao;

/**
 * 
 * <p>
 * <b>Title:</b> Cotacao.java
 * </p>
 * 
 * <p>
 * <b>Description:</b> Classe responsável por representar a entidade Cotacao
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnologia da Informa��o
 * </p>
 * 
 * @author Joaquim Barros - joaquimbarros.cmp@gmail.com
 * 
 * @version 1.0.0
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "cotacao")
public class Cotacao extends Entidade<Long> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(nullable = false)
	private String descricao;

	@Column(name = "data_cotacao")
	@Convert(converter = LocalDateAttributeConverter.class)
	private LocalDate dataCotacao;

	@Column(name = "data_validade")
	@Convert(converter = LocalDateAttributeConverter.class)
	private LocalDate dataValidade;

	@ManyToOne(fetch = FetchType.LAZY)
	private Empresa empresa;

	@ManyToOne(fetch = FetchType.LAZY)
	private Nucleo nucleo;

	@Column(name = "status_cotacao")
	private EnumStatusCotacao status;

	public Cotacao() {
	}

	public Cotacao( Long idCotacao ) {
		id = idCotacao;
	}
	
	public Cotacao( Long id, LocalDate dataCotacao ) {
		this.id = id;
		this.dataCotacao = dataCotacao;
	}

	public Cotacao( Long idCotacao, String descricao ) {
		id = idCotacao;
		this.descricao = descricao;
	}

	public Cotacao( Long idCotacao, String descricao, EnumStatusCotacao status ) {
		id = idCotacao;
		this.descricao = descricao;
		this.status = status;
	}

	public Cotacao( Long idCotacao, String descricao, Long idEmpresa, String nomeEmpresa, Long idNucleo, String nomeNucleo ) {
		id = idCotacao;
	}

	public Cotacao( Long id, String descricao, LocalDate dataCotacao, LocalDate dataValidade, EnumStatusCotacao status, Empresa empresa, Nucleo nucleo ) {
		super();
		this.id = id;
		this.descricao = descricao;
		this.dataCotacao = dataCotacao;
		this.dataValidade = dataValidade;
		this.empresa = empresa;
		this.nucleo = nucleo;
		this.status = status;
	}

	public Cotacao( Long id, String descricao, LocalDate dataCotacao, LocalDate dataValidade, EnumStatusCotacao status ) {
		super();
		this.id = id;
		this.descricao = descricao;
		this.dataCotacao = dataCotacao;
		this.dataValidade = dataValidade;
		this.status = status;
	}

	/**
	 * Retorna o valor do atributo <code>id</code>
	 *
	 * @return <code>Long</code>
	 */
	public Long getId() {

		return id;
	}

	/**
	 * Define o valor do atributo <code>id</code>.
	 *
	 * @param id
	 */
	public void setId(Long id) {

		this.id = id;
	}

	/**
	 * Retorna o valor do atributo <code>descricao</code>
	 *
	 * @return <code>String</code>
	 */
	public String getDescricao() {

		return descricao;
	}

	/**
	 * Define o valor do atributo <code>descricao</code>.
	 *
	 * @param descricao
	 */
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	public LocalDate getDataCotacao() {

		return dataCotacao;
	}

	public void setDataCotacao(LocalDate dataCotacao) {

		this.dataCotacao = dataCotacao;
	}

	public EnumStatusCotacao getStatus() {

		return status;
	}

	public void setStatus(EnumStatusCotacao status) {

		this.status = status;
	}

	public Empresa getEmpresa() {

		return empresa;
	}

	public void setEmpresa(Empresa empresa) {

		this.empresa = empresa;
	}

	public Nucleo getNucleo() {

		return nucleo;
	}

	public void setNucleo(Nucleo nucleo) {

		this.nucleo = nucleo;
	}

	public LocalDate getDataValidade() {

		return dataValidade;
	}

	public void setDataValidade(LocalDate dataValidade) {

		this.dataValidade = dataValidade;
	}

	@Override
	public String getLabel() {

		return null;
	}

}
