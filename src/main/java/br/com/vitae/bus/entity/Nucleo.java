package br.com.vitae.bus.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.arquitetura.entidade.Entidade;
import br.com.arquitetura.servico.converter.LocalDateAttributeConverter;
import br.com.arquitetura.util.DataUtil;
import br.com.vitae.bus.enumerator.EnumEstadosBrasileiros;

/**
 * 
 * <p>
 * <b>Title:</b> Nucleo.java
 * </p>
 * 
 * <p>
 * <b>Description:</b> Classe responsável por representar a entidade Nucleo
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnologia da Informa��o
 * </p>
 * 
 * @author Sergio Filho - sergioadsf@gmail.com
 * 
 * @version 1.0.0
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "nucleo")
public class Nucleo extends Entidade<Long> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "nome")
	private String nome;

	@Column(name = "status", length = 1, nullable = true)
	private Boolean ativo;

	@Column(name = "email", length = 200)
	private String email;

	@Column(name = "data_cadastro")
	@Convert(converter = LocalDateAttributeConverter.class)
	private LocalDate dataCadastro;

	@Column(name = "telefone", length = 20)
	private String telefone;

	@Column(name = "endereco", length = 200)
	private String endereco;

	@Column(name = "cidade", length = 100)
	private String cidade;

	@Column(name = "bairro", length = 100)
	private String bairro;

	@Column(name = "cep", length = 20)
	private String cep;

	@Column(name = "uf_cidade")
	private EnumEstadosBrasileiros ufCidade;

	@ManyToOne
	@JoinColumn(name = "id_empresa")
	private Empresa empresa;

	public Nucleo() {

	}

	public Nucleo(Long id, String nome) {

		super();
		this.id = id;
		this.nome = nome;
	}

	public Nucleo(Long id, String nome, Empresa empresa) {

		super();
		this.id = id;
		this.nome = nome;
		this.empresa = new Empresa(empresa.getId(), empresa.getNome());
	}

	public Nucleo(Long id) {

		super();
		this.id = id;
	}

	public Nucleo(Long id, String nome, Boolean ativo, String email, String dataCadastro, String telefone,
			String endereco, String cidade, String bairro, String cep, EnumEstadosBrasileiros ufCidade,
			Empresa empresa) {

		super();
		this.id = id;
		this.nome = nome;
		this.ativo = ativo;
		this.email = email;
		this.dataCadastro = DataUtil.strToLocalDate(dataCadastro);
		this.telefone = telefone;
		this.endereco = endereco;
		this.cidade = cidade;
		this.bairro = bairro;
		this.cep = cep;
		this.ufCidade = ufCidade;
		this.empresa = empresa;
	}

	/**
	 * Retorna o valor do atributo <code>id</code>
	 *
	 * @return <code>Long</code>
	 */
	public Long getId() {

		return id;
	}

	/**
	 * Define o valor do atributo <code>id</code>.
	 *
	 * @param id
	 */
	public void setId(Long id) {

		this.id = id;
	}

	/**
	 * Retorna o valor do atributo <code>nome</code>
	 *
	 * @return <code>String</code>
	 */
	public String getNome() {

		return nome;
	}

	/**
	 * Define o valor do atributo <code>nome</code>.
	 *
	 * @param nome
	 */
	public void setNome(String nome) {

		this.nome = nome;
	}

	/**
	 * Retorna o valor do atributo <code>ativo</code>
	 *
	 * @return <code>Boolean</code>
	 */
	public Boolean getAtivo() {

		return ativo;
	}

	/**
	 * Define o valor do atributo <code>ativo</code>.
	 *
	 * @param ativo
	 */
	public void setAtivo(Boolean ativo) {

		this.ativo = ativo;
	}

	/**
	 * Retorna o valor do atributo <code>email</code>
	 *
	 * @return <code>String</code>
	 */
	public String getEmail() {

		return email;
	}

	/**
	 * Define o valor do atributo <code>email</code>.
	 *
	 * @param email
	 */
	public void setEmail(String email) {

		this.email = email;
	}

	/**
	 * Retorna o valor do atributo <code>dataCadastro</code>
	 *
	 * @return <code>LocalDate</code>
	 */
	public LocalDate getDataCadastro() {

		return dataCadastro;
	}

	/**
	 * Define o valor do atributo <code>dataCadastro</code>.
	 *
	 * @param dataCadastro
	 */
	public void setDataCadastro(LocalDate dataCadastro) {

		this.dataCadastro = dataCadastro;
	}

	/**
	 * Retorna o valor do atributo <code>telefone</code>
	 *
	 * @return <code>String</code>
	 */
	public String getTelefone() {

		return telefone;
	}

	/**
	 * Define o valor do atributo <code>telefone</code>.
	 *
	 * @param telefone
	 */
	public void setTelefone(String telefone) {

		this.telefone = telefone;
	}

	/**
	 * Retorna o valor do atributo <code>endereco</code>
	 *
	 * @return <code>String</code>
	 */
	public String getEndereco() {

		return endereco;
	}

	/**
	 * Define o valor do atributo <code>endereco</code>.
	 *
	 * @param endereco
	 */
	public void setEndereco(String endereco) {

		this.endereco = endereco;
	}

	/**
	 * Retorna o valor do atributo <code>cidade</code>
	 *
	 * @return <code>String</code>
	 */
	public String getCidade() {

		return cidade;
	}

	/**
	 * Define o valor do atributo <code>cidade</code>.
	 *
	 * @param cidade
	 */
	public void setCidade(String cidade) {

		this.cidade = cidade;
	}

	/**
	 * Retorna o valor do atributo <code>bairro</code>
	 *
	 * @return <code>String</code>
	 */
	public String getBairro() {

		return bairro;
	}

	/**
	 * Define o valor do atributo <code>bairro</code>.
	 *
	 * @param bairro
	 */
	public void setBairro(String bairro) {

		this.bairro = bairro;
	}

	/**
	 * Retorna o valor do atributo <code>cep</code>
	 *
	 * @return <code>String</code>
	 */
	public String getCep() {

		return cep;
	}

	/**
	 * Define o valor do atributo <code>cep</code>.
	 *
	 * @param cep
	 */
	public void setCep(String cep) {

		this.cep = cep;
	}

	/**
	 * Retorna o valor do atributo <code>ufCidade</code>
	 *
	 * @return <code>EnumEstadosBrasileiros</code>
	 */
	public EnumEstadosBrasileiros getUfCidade() {

		return ufCidade;
	}

	/**
	 * Define o valor do atributo <code>ufCidade</code>.
	 *
	 * @param ufCidade
	 */
	public void setUfCidade(EnumEstadosBrasileiros ufCidade) {

		this.ufCidade = ufCidade;
	}

	/**
	 * 
	 * Método responsável por
	 *
	 *
	 * @return
	 */
	public Empresa getEmpresa() {

		return empresa;
	}

	/**
	 * 
	 * Método responsável por
	 *
	 *
	 * @param empresa
	 */
	public void setEmpresa(Empresa empresa) {

		this.empresa = empresa;
	}

	@Override
	public String getLabel() {

		// TODO Auto-generated method stub
		return null;
	}

}
