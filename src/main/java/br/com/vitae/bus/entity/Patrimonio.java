package br.com.vitae.bus.entity;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.arquitetura.entidade.Entidade;
import br.com.arquitetura.servico.converter.LocalDateAttributeConverter;
import br.com.vitae.bus.enumerator.EnumSituacaoBemPatrimonio;
import br.com.vitae.bus.enumerator.EnumTipoIncorporacaoPatrimonio;
import br.com.vitae.bus.enumerator.EnumUnidadeMedida;

/**
 * 
 * <p>
 * <b>Title:</b> Patrimonio.java
 * </p>
 * 
 * <p>
 * <b>Description:</b> Classe responsável por representar a entidade Patrimonio
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnologia da Informa��o
 * </p>
 * 
 * @author Rogério Ferreira - rogerio0308@homtail.com
 * 
 * @version 1.0.0
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "patrimonio")
public class Patrimonio extends Entidade<Long> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "nome", length = 200, nullable = false)
	private String nome;

	@Column(name = "numero_etiqueta", length = 100)
	private String numeroEtiqueta;

	@Column(name = "status", length = 1, nullable = true)
	private Boolean ativo;

	@Column(name = "observacao", length = 200, nullable = true)
	private String observacao;

	@Column(name = "unidade_medida")
	private EnumUnidadeMedida unidadeMedida;

	@Column(name = "situacao")
	private EnumSituacaoBemPatrimonio situacaoBemPatrimonio;

	/**
	 * Identifica como o patrimônio foi adquirido
	 */
	@Column(name = "tipo_incorporacao_patrimonio")
	private EnumTipoIncorporacaoPatrimonio tipoIncorporacaoPatrimonio;

	@Column(name = "numero_nota", length = 200, nullable = true)
	private String numeroNota;

	@Column(name = "numero_serie", length = 100)
	private String numeroSerie;

	@Column(name = "descricao_garantia")
	private String descricaoGarantia;

	@Column(name = "data_nota")
	@Convert(converter = LocalDateAttributeConverter.class)
	private LocalDate dataNota;

	@Column(name = "data_incorporacao")
	@Convert(converter = LocalDateAttributeConverter.class)
	private LocalDate dataIncorporacao;

	@Column(name = "fim_garantia")
	@Convert(converter = LocalDateAttributeConverter.class)
	private LocalDate fimGarantia;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	private GrupoBem grupoBem;

	/**
	 * Representa o departamento onde o patrimonio se encontra
	 */
	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	private Departamento departamento;

	@Column(name = "valor")
	private BigDecimal valor;

	public Patrimonio() {
	}

	public Patrimonio(Long id) {
		super();
		this.id = id;
	}

	public Patrimonio(Long id, String nome) {
		super();
		this.id = id;
		this.nome = nome;
	}

	public Patrimonio(Long id, String nome, String observacao, boolean ativo, EnumUnidadeMedida unidadeMedida,
			EnumSituacaoBemPatrimonio situacaoBemPatrimonio, GrupoBem grupoBem, Departamento departamento,
			EnumTipoIncorporacaoPatrimonio tipoIncorporacaoPatrimonio, String numeroNota, LocalDate dataIncorporacao,
			BigDecimal valor, String numeroSerie, String numeroEtiqueta, LocalDate dataNota, LocalDate fimGarantia,
			String descricaoGarantia) {
		super();
		this.id = id;
		this.nome = nome;
		this.ativo = ativo;
		this.observacao = observacao;
		this.numeroEtiqueta = numeroEtiqueta;
		this.unidadeMedida = unidadeMedida;
		this.situacaoBemPatrimonio = situacaoBemPatrimonio;
		this.tipoIncorporacaoPatrimonio = tipoIncorporacaoPatrimonio;
		this.numeroNota = numeroNota;
		this.numeroSerie = numeroSerie;
		this.descricaoGarantia = descricaoGarantia;
		this.dataNota = dataNota;
		this.dataIncorporacao = dataIncorporacao;
		this.fimGarantia = fimGarantia;
		this.grupoBem = grupoBem;
		this.departamento = departamento;
		this.valor = valor;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome
	 *            the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * @return the ativo
	 */
	public Boolean getAtivo() {
		return ativo;
	}

	/**
	 * @param ativo
	 *            the ativo to set
	 */
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	/**
	 * @return the observacao
	 */
	public String getObservacao() {
		return observacao;
	}

	/**
	 * @param observacao
	 *            the observacao to set
	 */
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	/**
	 * @return the unidadeMedida
	 */
	public EnumUnidadeMedida getUnidadeMedida() {
		return unidadeMedida;
	}

	/**
	 * @param unidadeMedida
	 *            the unidadeMedida to set
	 */
	public void setUnidadeMedida(EnumUnidadeMedida unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}

	/**
	 * @return the situacaoBemPatrimonio
	 */
	public EnumSituacaoBemPatrimonio getSituacaoBemPatrimonio() {
		return situacaoBemPatrimonio;
	}

	/**
	 * @param situacaoBemPatrimonio
	 *            the situacaoBemPatrimonio to set
	 */
	public void setSituacaoBemPatrimonio(EnumSituacaoBemPatrimonio situacaoBemPatrimonio) {
		this.situacaoBemPatrimonio = situacaoBemPatrimonio;
	}

	/**
	 * @return the grupoBem
	 */
	public GrupoBem getGrupoBem() {
		return grupoBem;
	}

	/**
	 * @param grupoBem
	 *            the grupoBem to set
	 */
	public void setGrupoBem(GrupoBem grupoBem) {
		this.grupoBem = grupoBem;
	}

	/**
	 * @return the departamento
	 */
	public Departamento getDepartamento() {
		return departamento;
	}

	/**
	 * @param departamento
	 *            the departamento to set
	 */
	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}

	/**
	 * @return the tipoIncorporacaoPatrimonio
	 */
	public EnumTipoIncorporacaoPatrimonio getTipoIncorporacaoPatrimonio() {
		return tipoIncorporacaoPatrimonio;
	}

	/**
	 * @param tipoIncorporacaoPatrimonio
	 *            the tipoIncorporacaoPatrimonio to set
	 */
	public void setTipoIncorporacaoPatrimonio(EnumTipoIncorporacaoPatrimonio tipoIncorporacaoPatrimonio) {
		this.tipoIncorporacaoPatrimonio = tipoIncorporacaoPatrimonio;
	}

	/**
	 * @return the numeroNota
	 */
	public String getNumeroNota() {
		return numeroNota;
	}

	/**
	 * @param numeroNota
	 *            the numeroNota to set
	 */
	public void setNumeroNota(String numeroNota) {
		this.numeroNota = numeroNota;
	}

	/**
	 * @return the dataIncorporacao
	 */
	public LocalDate getDataIncorporacao() {
		return dataIncorporacao;
	}

	/**
	 * @param dataIncorporacao
	 *            the dataIncorporacao to set
	 */
	public void setDataIncorporacao(LocalDate dataIncorporacao) {
		this.dataIncorporacao = dataIncorporacao;
	}

	/**
	 * @return the valor
	 */
	public BigDecimal getValor() {
		return valor;
	}

	/**
	 * @param valor
	 *            the valor to set
	 */
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	/**
	 * @return the numeroSerie
	 */
	public String getNumeroSerie() {
		return numeroSerie;
	}

	/**
	 * @param numeroSerie
	 *            the numeroSerie to set
	 */
	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}

	/**
	 * @return the dataNota
	 */
	public LocalDate getDataNota() {
		return dataNota;
	}

	/**
	 * @param dataNota
	 *            the dataNota to set
	 */
	public void setDataNota(LocalDate dataNota) {
		this.dataNota = dataNota;
	}

	/**
	 * @return the fimGarantia
	 */
	public LocalDate getFimGarantia() {
		return fimGarantia;
	}

	/**
	 * @param fimGarantia
	 *            the fimGarantia to set
	 */
	public void setFimGarantia(LocalDate fimGarantia) {
		this.fimGarantia = fimGarantia;
	}

	/**
	 * @return the descricaoGarantia
	 */
	public String getDescricaoGarantia() {
		return descricaoGarantia;
	}

	/**
	 * @param descricaoGarantia
	 *            the descricaoGarantia to set
	 */
	public void setDescricaoGarantia(String descricaoGarantia) {
		this.descricaoGarantia = descricaoGarantia;
	}

	public String getNumeroEtiqueta() {
		return numeroEtiqueta;
	}

	public void setNumeroEtiqueta(String numeroEtiqueta) {
		this.numeroEtiqueta = numeroEtiqueta;
	}

	@Override
	public String getLabel() {
		// TODO Auto-generated method stub
		return null;
	}
}
