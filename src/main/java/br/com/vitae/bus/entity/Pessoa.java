package br.com.vitae.bus.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

import br.com.arquitetura.entidade.Entidade;
import br.com.arquitetura.servico.converter.LocalDateAttributeConverter;
import br.com.arquitetura.util.Utils;
import br.com.vitae.bus.enumerator.EnumEstadoCivil;
import br.com.vitae.bus.enumerator.EnumEstadosBrasileiros;
import br.com.vitae.bus.enumerator.EnumPessoa;
import br.com.vitae.bus.enumerator.EnumSexo;

@SuppressWarnings("serial")
@Entity
public class Pessoa extends Entidade<Long> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "nome", length = 200)
	private String nome;

	@Column(name = "data_cadastro")
	@Convert(converter = LocalDateAttributeConverter.class)
	private LocalDate dataCadastro;

	@Column(name = "naturalidade")
	private String naturalidade;

	@Column(name = "ud_naturalidade", length = 1)
	private Integer ufNaturalidade;

	@Column(name = "data_nascimento")
	@Convert(converter = LocalDateAttributeConverter.class)
	private LocalDate dataNascimento;

	@Column(name = "estado_civil")
	private EnumEstadoCivil estadoCivil;

	@Column(name = "sexo")
	private EnumSexo sexo;

	@Column(name = "cpf_cnpj", length = 20)
	private String cpfCnpj;

	@Column(name = "rg_ie", length = 20)
	private String rgIe;

	@Column(name = "emissor_rg_ie", length = 20)
	private String emissorRgIe;

	@Column(name = "data_expedicao_rg_ie")
	@Convert(converter = LocalDateAttributeConverter.class)
	private LocalDate dataExpedicaoRgIe;

	@Column(name = "endereco")
	private String endereco;

	@Column(name = "complemento")
	private String complemento;

	@Column(name = "numero_endereco")
	private String numeroEndereco;

	@Column(name = "bairro")
	private String bairro;

	@Column(name = "cep", length = 10)
	private String cep;

	@Column(name = "cidade")
	private String cidade;

	@Column(name = "uf_cidade")
	private EnumEstadosBrasileiros ufCidade;

	@Column(name = "telefone", length = 20)
	private String telefone;

	@Column(name = "celular", length = 20)
	private String celular;

	@Column(name = "email")
	private String email;

	@Column(name = "foto")
	@Lob
	private byte[] foto;

	@Column(name = "observacao")
	private String observacao;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	private NumeroBanco banco;

	@Column(name = "agencia", length = 30)
	private String agencia;

	@Column(name = "conta_corrente", length = 30)
	private String contaCorrente;

	@Column(name = "observacoes_bancarias")
	private String observacoesBancaria;

	@Column(name = "numero_pis")
	private String numeroPIS;

	@Column(name = "numero_ctps")
	private String numeroCTPS;

	@Column(name = "tipo")
	private EnumPessoa tipoPessoa;

	public Pessoa() {
	}

	public Pessoa( Long id, String nome, String cpfCnpj ) {
		super();
		this.id = id;
		this.nome = nome;
		this.cpfCnpj = cpfCnpj;
	}

	public Pessoa( Long id, String nome, String cpfCnpj, String email ) {
		super();
		this.id = id;
		this.nome = nome;
		this.cpfCnpj = cpfCnpj;
		this.email = email;
	}

	public Pessoa( Long id, String nome, LocalDate dataCadastro, String naturalidade, Integer ufNaturalidade, LocalDate dataNascimento, EnumEstadoCivil estadoCivil, EnumSexo sexo, String cpfCnpj, String rgIe, String emissorRgIe, LocalDate dataExpedicaoRgIe, String endereco, String complemento, String numeroEndereco, String bairro, String cep, String cidade, EnumEstadosBrasileiros ufCidade, String telefone, String celular, String email, byte[] foto, String observacao, NumeroBanco banco,
			String agencia, String contaCorrente, String observacoesBancaria, String numeroPIS, String numeroCTPS, EnumPessoa tipoPessoa ) {
		super();
		this.id = id;
		this.nome = nome;
		this.dataCadastro = dataCadastro;
		this.naturalidade = naturalidade;
		this.ufNaturalidade = ufNaturalidade;
		this.dataNascimento = dataNascimento;
		this.estadoCivil = estadoCivil;
		this.sexo = sexo;

		if (tipoPessoa == EnumPessoa.PF) {
			this.cpfCnpj = Utils.removeMascaraCPF(cpfCnpj);
		} else {
			this.cpfCnpj = Utils.removeMascaraCNPJ(cpfCnpj);
		}

		this.rgIe = rgIe;
		this.emissorRgIe = emissorRgIe;
		this.dataExpedicaoRgIe = dataExpedicaoRgIe;
		this.endereco = endereco;
		this.complemento = complemento;
		this.numeroEndereco = numeroEndereco;
		this.bairro = bairro;
		this.cep = cep;
		this.cidade = cidade;
		this.ufCidade = ufCidade;
		this.telefone = telefone;
		this.celular = celular;
		this.email = email;
		this.foto = foto;
		this.observacao = observacao;
		this.banco = banco;
		this.agencia = agencia;
		this.contaCorrente = contaCorrente;
		this.observacoesBancaria = observacoesBancaria;
		this.numeroPIS = numeroPIS;
		this.numeroCTPS = numeroCTPS;
		this.tipoPessoa = tipoPessoa;
	}

	/**
	 * Retorna o valor do atributo <code>id</code>
	 *
	 * @return <code>Long</code>
	 */
	public Long getId() {

		return id;
	}

	/**
	 * Define o valor do atributo <code>id</code>.
	 *
	 * @param id
	 */
	public void setId(Long id) {

		this.id = id;
	}

	/**
	 * Retorna o valor do atributo <code>nome</code>
	 *
	 * @return <code>String</code>
	 */
	public String getNome() {

		return nome;
	}

	/**
	 * Define o valor do atributo <code>nome</code>.
	 *
	 * @param nome
	 */
	public void setNome(String nome) {

		this.nome = nome;
	}

	/**
	 * Retorna o valor do atributo <code>dataCadastro</code>
	 *
	 * @return <code>LocalDate</code>
	 */
	public LocalDate getDataCadastro() {

		return dataCadastro;
	}

	/**
	 * Define o valor do atributo <code>dataCadastro</code>.
	 *
	 * @param dataCadastro
	 */
	public void setDataCadastro(LocalDate dataCadastro) {

		this.dataCadastro = dataCadastro;
	}

	/**
	 * Retorna o valor do atributo <code>naturalidade</code>
	 *
	 * @return <code>String</code>
	 */
	public String getNaturalidade() {

		return naturalidade;
	}

	/**
	 * Define o valor do atributo <code>naturalidade</code>.
	 *
	 * @param naturalidade
	 */
	public void setNaturalidade(String naturalidade) {

		this.naturalidade = naturalidade;
	}

	/**
	 * Retorna o valor do atributo <code>ufNaturalidade</code>
	 *
	 * @return <code>Integer</code>
	 */
	public Integer getUfNaturalidade() {

		return ufNaturalidade;
	}

	/**
	 * Define o valor do atributo <code>ufNaturalidade</code>.
	 *
	 * @param ufNaturalidade
	 */
	public void setUfNaturalidade(Integer ufNaturalidade) {

		this.ufNaturalidade = ufNaturalidade;
	}

	/**
	 * Retorna o valor do atributo <code>dataNascimento</code>
	 *
	 * @return <code>LocalDate</code>
	 */
	public LocalDate getDataNascimento() {

		return dataNascimento;
	}

	/**
	 * Define o valor do atributo <code>dataNascimento</code>.
	 *
	 * @param dataNascimento
	 */
	public void setDataNascimento(LocalDate dataNascimento) {

		this.dataNascimento = dataNascimento;
	}

	/**
	 * Retorna o valor do atributo <code>estadoCivil</code>
	 *
	 * @return <code>EnumEstadoCivil</code>
	 */
	public EnumEstadoCivil getEstadoCivil() {

		return estadoCivil;
	}

	/**
	 * Define o valor do atributo <code>estadoCivil</code>.
	 *
	 * @param estadoCivil
	 */
	public void setEstadoCivil(EnumEstadoCivil estadoCivil) {

		this.estadoCivil = estadoCivil;
	}

	/**
	 * Retorna o valor do atributo <code>sexo</code>
	 *
	 * @return <code>EnumSexo</code>
	 */
	public EnumSexo getSexo() {

		return sexo;
	}

	/**
	 * Define o valor do atributo <code>sexo</code>.
	 *
	 * @param sexo
	 */
	public void setSexo(EnumSexo sexo) {

		this.sexo = sexo;
	}

	/**
	 * Retorna o valor do atributo <code>cpfCnpj</code>
	 *
	 * @return <code>String</code>
	 */
	public String getCpfCnpj() {

		return cpfCnpj;
	}

	/**
	 * Define o valor do atributo <code>cpfCnpj</code>.
	 *
	 * @param cpfCnpj
	 */
	public void setCpfCnpj(String cpfCnpj) {

		this.cpfCnpj = cpfCnpj;
	}

	/**
	 * Retorna o valor do atributo <code>rgIe</code>
	 *
	 * @return <code>String</code>
	 */
	public String getRgIe() {

		return rgIe;
	}

	/**
	 * Define o valor do atributo <code>rgIe</code>.
	 *
	 * @param rgIe
	 */
	public void setRgIe(String rgIe) {

		this.rgIe = rgIe;
	}

	/**
	 * Retorna o valor do atributo <code>emissorRgIe</code>
	 *
	 * @return <code>String</code>
	 */
	public String getEmissorRgIe() {

		return emissorRgIe;
	}

	/**
	 * Define o valor do atributo <code>emissorRgIe</code>.
	 *
	 * @param emissorRgIe
	 */
	public void setEmissorRgIe(String emissorRgIe) {

		this.emissorRgIe = emissorRgIe;
	}

	/**
	 * Retorna o valor do atributo <code>dataExpedicaoRgIe</code>
	 *
	 * @return <code>Date</code>
	 */
	public LocalDate getDataExpedicaoRgIe() {

		return dataExpedicaoRgIe;
	}

	/**
	 * Define o valor do atributo <code>dataExpedicaoRgIe</code>.
	 *
	 * @param dataExpedicaoRgIe
	 */
	public void setDataExpedicaoRgIe(LocalDate dataExpedicaoRgIe) {

		this.dataExpedicaoRgIe = dataExpedicaoRgIe;
	}

	/**
	 * Retorna o valor do atributo <code>endereco</code>
	 *
	 * @return <code>String</code>
	 */
	public String getEndereco() {

		return endereco;
	}

	/**
	 * Define o valor do atributo <code>endereco</code>.
	 *
	 * @param endereco
	 */
	public void setEndereco(String endereco) {

		this.endereco = endereco;
	}

	/**
	 * Retorna o valor do atributo <code>complemento</code>
	 *
	 * @return <code>String</code>
	 */
	public String getComplemento() {

		return complemento;
	}

	/**
	 * Define o valor do atributo <code>complemento</code>.
	 *
	 * @param complemento
	 */
	public void setComplemento(String complemento) {

		this.complemento = complemento;
	}

	/**
	 * Retorna o valor do atributo <code>numeroEndereco</code>
	 *
	 * @return <code>String</code>
	 */
	public String getNumeroEndereco() {

		return numeroEndereco;
	}

	/**
	 * Define o valor do atributo <code>numeroEndereco</code>.
	 *
	 * @param numeroEndereco
	 */
	public void setNumeroEndereco(String numeroEndereco) {

		this.numeroEndereco = numeroEndereco;
	}

	/**
	 * Retorna o valor do atributo <code>bairro</code>
	 *
	 * @return <code>String</code>
	 */
	public String getBairro() {

		return bairro;
	}

	/**
	 * Define o valor do atributo <code>bairro</code>.
	 *
	 * @param bairro
	 */
	public void setBairro(String bairro) {

		this.bairro = bairro;
	}

	/**
	 * Retorna o valor do atributo <code>cep</code>
	 *
	 * @return <code>String</code>
	 */
	public String getCep() {

		return cep;
	}

	/**
	 * Define o valor do atributo <code>cep</code>.
	 *
	 * @param cep
	 */
	public void setCep(String cep) {

		this.cep = cep;
	}

	/**
	 * Retorna o valor do atributo <code>cidade</code>
	 *
	 * @return <code>String</code>
	 */
	public String getCidade() {

		return cidade;
	}

	/**
	 * Define o valor do atributo <code>cidade</code>.
	 *
	 * @param cidade
	 */
	public void setCidade(String cidade) {

		this.cidade = cidade;
	}

	/**
	 * Retorna o valor do atributo <code>ufCidade</code>
	 *
	 * @return <code>EnumEstadosBrasileiros</code>
	 */
	public EnumEstadosBrasileiros getUfCidade() {

		return ufCidade;
	}

	/**
	 * Define o valor do atributo <code>ufCidade</code>.
	 *
	 * @param ufCidade
	 */
	public void setUfCidade(EnumEstadosBrasileiros ufCidade) {

		this.ufCidade = ufCidade;
	}

	/**
	 * Retorna o valor do atributo <code>telefone</code>
	 *
	 * @return <code>String</code>
	 */
	public String getTelefone() {

		return telefone;
	}

	/**
	 * Define o valor do atributo <code>telefone</code>.
	 *
	 * @param telefone
	 */
	public void setTelefone(String telefone) {

		this.telefone = telefone;
	}

	/**
	 * Retorna o valor do atributo <code>celular</code>
	 *
	 * @return <code>String</code>
	 */
	public String getCelular() {

		return celular;
	}

	/**
	 * Define o valor do atributo <code>celular</code>.
	 *
	 * @param celular
	 */
	public void setCelular(String celular) {

		this.celular = celular;
	}

	/**
	 * Retorna o valor do atributo <code>email</code>
	 *
	 * @return <code>String</code>
	 */
	public String getEmail() {

		return email;
	}

	/**
	 * Define o valor do atributo <code>email</code>.
	 *
	 * @param email
	 */
	public void setEmail(String email) {

		this.email = email;
	}

	/**
	 * Retorna o valor do atributo <code>foto</code>
	 *
	 * @return <code>byte[]</code>
	 */
	public byte[] getFoto() {

		return foto;
	}

	/**
	 * Define o valor do atributo <code>foto</code>.
	 *
	 * @param foto
	 */
	public void setFoto(byte[] foto) {

		this.foto = foto;
	}

	/**
	 * Retorna o valor do atributo <code>observacao</code>
	 *
	 * @return <code>String</code>
	 */
	public String getObservacao() {

		return observacao;
	}

	/**
	 * Define o valor do atributo <code>observacao</code>.
	 *
	 * @param observacao
	 */
	public void setObservacao(String observacao) {

		this.observacao = observacao;
	}

	/**
	 * Retorna o valor do atributo <code>banco</code>
	 *
	 * @return <code>Banco</code>
	 */
	public NumeroBanco getBanco() {

		return banco;
	}

	/**
	 * Define o valor do atributo <code>banco</code>.
	 *
	 * @param banco
	 */
	public void setBanco(NumeroBanco banco) {

		this.banco = banco;
	}

	/**
	 * Retorna o valor do atributo <code>agencia</code>
	 *
	 * @return <code>String</code>
	 */
	public String getAgencia() {

		return agencia;
	}

	/**
	 * Define o valor do atributo <code>agencia</code>.
	 *
	 * @param agencia
	 */
	public void setAgencia(String agencia) {

		this.agencia = agencia;
	}

	/**
	 * Retorna o valor do atributo <code>contaCorrente</code>
	 *
	 * @return <code>String</code>
	 */
	public String getContaCorrente() {

		return contaCorrente;
	}

	/**
	 * Define o valor do atributo <code>contaCorrente</code>.
	 *
	 * @param contaCorrente
	 */
	public void setContaCorrente(String contaCorrente) {

		this.contaCorrente = contaCorrente;
	}

	/**
	 * Retorna o valor do atributo <code>observacoesBancaria</code>
	 *
	 * @return <code>String</code>
	 */
	public String getObservacoesBancaria() {

		return observacoesBancaria;
	}

	/**
	 * Define o valor do atributo <code>observacoesBancaria</code>.
	 *
	 * @param observacoesBancaria
	 */
	public void setObservacoesBancaria(String observacoesBancaria) {

		this.observacoesBancaria = observacoesBancaria;
	}

	/**
	 * Retorna o valor do atributo <code>numeroPIS</code>
	 *
	 * @return <code>String</code>
	 */
	public String getNumeroPIS() {

		return numeroPIS;
	}

	/**
	 * Define o valor do atributo <code>numeroPIS</code>.
	 *
	 * @param numeroPIS
	 */
	public void setNumeroPIS(String numeroPIS) {

		this.numeroPIS = numeroPIS;
	}

	/**
	 * Retorna o valor do atributo <code>numeroCTPS</code>
	 *
	 * @return <code>String</code>
	 */
	public String getNumeroCTPS() {

		return numeroCTPS;
	}

	/**
	 * Define o valor do atributo <code>numeroCTPS</code>.
	 *
	 * @param numeroCTPS
	 */
	public void setNumeroCTPS(String numeroCTPS) {

		this.numeroCTPS = numeroCTPS;
	}

	public EnumPessoa getTipoPessoa() {

		return tipoPessoa;
	}

	public void setTipoPessoa(EnumPessoa tipoPessoa) {

		this.tipoPessoa = tipoPessoa;
	}

	@Override
	public String getLabel() {

		// TODO Auto-generated method stub
		return null;
	}

}
