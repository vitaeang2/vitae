package br.com.vitae.bus.entity;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.arquitetura.entidade.Entidade;
import br.com.arquitetura.servico.converter.LocalDateAttributeConverter;

/**
 * 
 * <p>
 * <b>Title:</b> AnotacaoColaborador.java
 * </p>
 * 
 * <p>
 * <b>Description: Classe Responsável por representar a entidade AnotacaoColaborador </b>
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnologia da Informa��o
 * </p>
 * 
 * @author Joaquim Barros - joaquimbarros.cmp@gmail.com
 * 
 * @version 1.0.0
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "anotacao_colaborador")
public class AnotacaoColaborador extends Entidade<Long> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "data")
	@Convert(converter = LocalDateAttributeConverter.class)
	private LocalDate data;

	@Column(name = "historico")
	private String historico;

	@Column(name = "valor")
	private BigDecimal valor;

	@JoinColumn(nullable = true)
	@ManyToOne(fetch = FetchType.LAZY)
	private Colaborador colaborador;

	/**
	 * Retorna o valor do atributo <code>id</code>
	 *
	 * @return <code>Long</code>
	 */
	public Long getId() {

		return id;
	}

	/**
	 * Define o valor do atributo <code>id</code>.
	 *
	 * @param id
	 */
	public void setId(Long id) {

		this.id = id;
	}

	/**
	 * Retorna o valor do atributo <code>data</code>
	 *
	 * @return <code>LocalDate</code>
	 */
	public LocalDate getData() {

		return data;
	}

	/**
	 * Define o valor do atributo <code>data</code>.
	 *
	 * @param data
	 */
	public void setData(LocalDate data) {

		this.data = data;
	}

	/**
	 * Retorna o valor do atributo <code>historico</code>
	 *
	 * @return <code>String</code>
	 */
	public String getHistorico() {

		return historico;
	}

	/**
	 * Define o valor do atributo <code>historico</code>.
	 *
	 * @param historico
	 */
	public void setHistorico(String historico) {

		this.historico = historico;
	}

	/**
	 * Retorna o valor do atributo <code>valor</code>
	 *
	 * @return <code>BigDecimal</code>
	 */
	public BigDecimal getValor() {

		return valor;
	}

	/**
	 * Define o valor do atributo <code>valor</code>.
	 *
	 * @param valor
	 */
	public void setValor(BigDecimal valor) {

		this.valor = valor;
	}

	/**
	 * Retorna o valor do atributo <code>colaborador</code>
	 *
	 * @return <code>Colaborador</code>
	 */
	public Colaborador getColaborador() {

		return colaborador;
	}

	/**
	 * Define o valor do atributo <code>colaborador</code>.
	 *
	 * @param colaborador
	 */
	public void setColaborador(Colaborador colaborador) {

		this.colaborador = colaborador;
	}

	@Override
	public String getLabel() {

		// TODO Auto-generated method stub
		return null;
	}

}
