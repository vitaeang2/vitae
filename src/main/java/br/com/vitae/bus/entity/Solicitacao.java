package br.com.vitae.bus.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.com.arquitetura.entidade.Entidade;
import br.com.arquitetura.servico.converter.LocalDateAttributeConverter;
import br.com.vitae.bus.enumerator.EnumStatusSolicitacao;

/**
 * 
 * <p>
 * <b>Title:</b> Colaborador.java
 * </p>
 * 
 * <p>
 * <b>Description:</b> Classe responsável por representar a entidade Colaborador
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnologia da Informa��o
 * </p>
 * 
 * @author Rogério Ferreira - rogerio0308@homtail.com
 * 
 * @version 1.0.0
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "solicitacao")
public class Solicitacao extends Entidade<Long> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "ativo")
	private Boolean ativo;

	@Column(name = "assunto")
	private String assunto;

	@Column(name = "descricao")
	private String descricao;

	@Column(name = "solucao")
	private String solucao;

	@Column(name = "numero_etiqueta", length = 100)
	private String numeroEtiqueta;

	@ManyToOne(fetch = FetchType.LAZY)
	private Empresa empresa;

	@ManyToOne(fetch = FetchType.LAZY)
	private Nucleo nucleo;

	@OneToOne(fetch = FetchType.LAZY)
	private Usuario responsavel;

	@OneToOne(fetch = FetchType.LAZY)
	private Usuario criador;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	private TipoSolicitacao tipoSolicitacao;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	private Local local;

	@Column(name = "status_solicitacao")
	private EnumStatusSolicitacao statusSolicitacao;

	@Column(name = "data_abertura")
	@Convert(converter = LocalDateAttributeConverter.class)
	private LocalDate abertura;

	@Column(name = "data_termino")
	@Convert(converter = LocalDateAttributeConverter.class)
	private LocalDate termino;

	@Column(name = "data_previsao_finalizacao")
	@Convert(converter = LocalDateAttributeConverter.class)
	private LocalDate previsaoFinalizacao;

	public Solicitacao() {
	}

	/**
	 * 
	 * Responsável pela cria��o de novas instâncias desta classe para ser usada
	 * na tela de listagem, passando somente os campos que se fazem necessario
	 * na datatable.
	 * 
	 * @param id
	 * @param idPessoa
	 * @param nome
	 * @param cpfCnpj
	 */
	public Solicitacao(Long id, String descricao) {
		super();
		this.id = id;
		this.descricao = descricao;
	}

	public Solicitacao(Long id,

			Boolean ativo,

			String assunto,

			String descricao,

			String solucao,

			String numeroEtiqueta,

			Empresa empresa,

			Nucleo nucleo,

			Usuario responsavel,

			Usuario criador,

			TipoSolicitacao tipoSolicitacao,

			EnumStatusSolicitacao statusSolicitacao,

			LocalDate abertura,

			LocalDate termino,

			LocalDate previsaoFinalizacao,
			
			Local local) {
		super();

		this.id = id;

		this.ativo = ativo;

		this.assunto = assunto;

		this.descricao = descricao;

		this.solucao = solucao;

		this.numeroEtiqueta = numeroEtiqueta;

		this.empresa = empresa;

		this.nucleo = nucleo;

		this.responsavel = responsavel;

		this.criador = criador;

		this.tipoSolicitacao = tipoSolicitacao;

		this.statusSolicitacao = statusSolicitacao;

		this.abertura = abertura;

		this.termino = termino;

		this.previsaoFinalizacao = previsaoFinalizacao;
		
		this.local = local;
	}

	/**
	 * Retorna o valor do atributo <code>id</code>
	 *
	 * @return <code>Long</code>
	 */
	public Long getId() {

		return id;
	}

	/**
	 * Define o valor do atributo <code>id</code>.
	 *
	 * @param id
	 */
	public void setId(Long id) {

		this.id = id;
	}

	public Boolean getAtivo() {

		return ativo;
	}

	public void setAtivo(Boolean ativo) {

		this.ativo = ativo;
	}

	public Nucleo getNucleo() {

		return nucleo;
	}

	public void setNucleo(Nucleo nucleo) {

		this.nucleo = nucleo;
	}

	public Empresa getEmpresa() {

		return empresa;
	}

	public void setEmpresa(Empresa empresa) {

		this.empresa = empresa;
	}

	public String getAssunto() {
		return assunto;
	}

	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getSolucao() {
		return solucao;
	}

	public void setSolucao(String solucao) {
		this.solucao = solucao;
	}

	public String getNumeroEtiqueta() {
		return numeroEtiqueta;
	}

	public void setNumeroEtiqueta(String numeroEtiqueta) {
		this.numeroEtiqueta = numeroEtiqueta;
	}

	public Usuario getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(Usuario responsavel) {
		this.responsavel = responsavel;
	}

	public EnumStatusSolicitacao getStatusSolicitacao() {
		return statusSolicitacao;
	}

	public void setStatusSolicitacao(EnumStatusSolicitacao statusSolicitacao) {
		this.statusSolicitacao = statusSolicitacao;
	}

	public LocalDate getAbertura() {
		return abertura;
	}

	public void setAbertura(LocalDate abertura) {
		this.abertura = abertura;
	}

	public LocalDate getTermino() {
		return termino;
	}

	public void setTermino(LocalDate termino) {
		this.termino = termino;
	}

	public LocalDate getPrevisaoFinalizacao() {
		return previsaoFinalizacao;
	}

	public void setPrevisaoFinalizacao(LocalDate previsaoFinalizacao) {
		this.previsaoFinalizacao = previsaoFinalizacao;
	}

	public TipoSolicitacao getTipoSolicitacao() {
		return tipoSolicitacao;
	}

	public void setTipoSolicitacao(TipoSolicitacao tipoSolicitacao) {
		this.tipoSolicitacao = tipoSolicitacao;
	}

	public Usuario getCriador() {
		return criador;
	}

	public void setCriador(Usuario criador) {
		this.criador = criador;
	}

	public Local getLocal() {
		return local;
	}

	public void setLocal(Local local) {
		this.local = local;
	}

	@Override
	public String getLabel() {

		// TODO Auto-generated method stub
		return null;
	}

}
