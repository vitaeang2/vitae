package br.com.vitae.bus.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.arquitetura.entidade.Entidade;
import br.com.vitae.bus.enumerator.EnumItemPedidoCompra;

/**
 * 
 * <p>
 * <b>Title:</b> ItemPedidoCompra.java
 * </p>
 * 
 * <p>
 * <b>Description:</b> Classe responsável por representar a entidade ItemPedidoCompra
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnologia da Informa��o
 * </p>
 * 
 * @author Joaquim Barros - joaquimbarros.cmp@gmail.com
 * 
 * @version 1.0.0
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "item_pedido_compra")
public class ItemPedidoCompra extends Entidade<Long> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "valor_unitario")
	private BigDecimal vlrUnitario;

	@Column(name = "quantidade")
	private BigDecimal quantidade;

	@Column(name = "status")
	private EnumItemPedidoCompra status;

	@ManyToOne(fetch = FetchType.LAZY)
	private PedidoCompra pedidoCompra;

	@ManyToOne(fetch = FetchType.LAZY)
	private Produto produto;

	// itensItemPedidoCompra : Array<ItemPedidoCompra>;

	public ItemPedidoCompra() {
	}

	public ItemPedidoCompra( Long idItemPedidoCompra ) {
		id = idItemPedidoCompra;
	}

	public ItemPedidoCompra( Long id, BigDecimal vlrUnitario, BigDecimal quantidade, PedidoCompra pedidoCompra, Produto produto, EnumItemPedidoCompra status ) {
		super();
		this.id = id;
		this.vlrUnitario = vlrUnitario;
		this.quantidade = quantidade;
		this.pedidoCompra = pedidoCompra;
		this.produto = produto;
		this.status = status;
	}

	/**
	 * Retorna o valor do atributo <code>id</code>
	 *
	 * @return <code>Long</code>
	 */
	public Long getId() {

		return id;
	}

	/**
	 * Define o valor do atributo <code>id</code>.
	 *
	 * @param id
	 */
	public void setId(Long id) {

		this.id = id;
	}

	public BigDecimal getVlrUnitario() {

		return vlrUnitario;
	}

	public void setVlrUnitario(BigDecimal vlrUnitario) {

		this.vlrUnitario = vlrUnitario;
	}

	public BigDecimal getQuantidade() {

		return quantidade;
	}

	public void setQuantidade(BigDecimal quantidade) {

		this.quantidade = quantidade;
	}

	public PedidoCompra getPedidoCompra() {

		return pedidoCompra;
	}

	public void setPedidoCompra(PedidoCompra pedidoCompra) {

		this.pedidoCompra = pedidoCompra;
	}

	public Produto getProduto() {

		return produto;
	}

	public void setProduto(Produto produto) {

		this.produto = produto;
	}

	public EnumItemPedidoCompra getStatus() {

		return status;
	}

	public void setStatus(EnumItemPedidoCompra status) {

		this.status = status;
	}

	@Override
	public String getLabel() {

		return null;
	}

}
