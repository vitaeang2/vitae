package br.com.vitae.bus.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.arquitetura.entidade.Entidade;

/**
 * 
 * <p>
 * <b>Title:</b> Empresa.java
 * </p>
 * 
 * <p>
 * <b>Description:</b> Classe responsável por representar a entidade Empresa
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnologia da Informa��o
 * </p>
 * 
 * @version 1.0.0
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "aplicao")
public class Aplicacao extends Entidade<Long> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "url")
	private String url;

	@Column(name = "nome")
	private String nome;

	public Aplicacao() {
	}

	@Override
	public Long getId() {

		return id;
	}

	@Override
	public void setId(Long id) {

	}

	public String getUrl() {

		return url;
	}

	public void setUrl(String url) {

		this.url = url;
	}

	public String getNome() {

		return nome;
	}

	public void setNome(String nome) {

		this.nome = nome;
	}

	@Override
	public String getLabel() {

		return null;
	}

}
