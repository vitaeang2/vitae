package br.com.vitae.bus.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.arquitetura.entidade.Entidade;

@SuppressWarnings("serial")
// @Cache(usage=CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Entity
@Table(name = "numero_banco")
public class NumeroBanco extends Entidade<Long> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "numero")
	private String numero;

	@Column(name = "nome")
	private String nome;

	public NumeroBanco() {
	}

	public NumeroBanco( Long id ) {
		this.id = id;
	}

	public NumeroBanco( Long id, String numero, String nome ) {
		super();
		this.id = id;
		this.numero = numero;
		this.nome = nome;
	}

	public Long getId() {

		return this.id;
	}

	public void setId(Long id) {

		this.id = id;
	}

	public String getNumero() {

		return this.numero;
	}

	public void setNumero(String numero) {

		this.numero = numero;
	}

	public String getNome() {

		return this.nome;
	}

	public void setNome(String nome) {

		this.nome = nome;
	}

	public String getLabel() {

		return getNome();
	}

}
