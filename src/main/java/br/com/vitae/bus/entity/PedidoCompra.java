package br.com.vitae.bus.entity;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.arquitetura.entidade.Entidade;
import br.com.arquitetura.servico.converter.LocalDateAttributeConverter;
import br.com.vitae.bus.enumerator.EnumPedidoCompra;

/**
 * 
 * <p>
 * <b>Title:</b> PedidoCompra.java
 * </p>
 * 
 * <p>
 * <b>Description:</b> Classe responsável por representar a entidade PedidoCompra
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnologia da Informa��o
 * </p>
 * 
 * @author Joaquim Barros - joaquimbarros.cmp@gmail.com
 * 
 * @version 1.0.0
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "pedido_compra")
public class PedidoCompra extends Entidade<Long> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "data_pedido_compra")
	@Convert(converter = LocalDateAttributeConverter.class)
	private LocalDate dataPedidoCompra;

	@Column(name = "valor_frete")
	private BigDecimal vlrFrete;

	@Column(name = "valor_desconto")
	private BigDecimal vlrDesconto;

	@Column(name = "status")
	private EnumPedidoCompra status;

	@ManyToOne(fetch = FetchType.LAZY)
	private Colaborador colaborador;

	@ManyToOne(fetch = FetchType.LAZY)
	private Empresa empresa;

	@ManyToOne(fetch = FetchType.LAZY)
	private Nucleo nucleo;

	@ManyToOne(fetch = FetchType.LAZY)
	private Fornecedor fornecedor;

	@ManyToOne(fetch = FetchType.LAZY)
	private Cotacao cotacao;

	public PedidoCompra() {
	}

	public PedidoCompra( Long idPedidoCompra ) {
		id = idPedidoCompra;
	}

	public PedidoCompra( Long id, LocalDate dataPedidoCompra, BigDecimal vlrFrete, BigDecimal vlrDesconto, Long idFornecedor, String nomeContatoFornecedor, Long idPessoa, String nomePessoa, String cpfCnpjPessoa, String email, EnumPedidoCompra status) {
		super();
		this.id = id;
		this.dataPedidoCompra = dataPedidoCompra;
		this.vlrFrete = vlrFrete;
		this.vlrDesconto = vlrDesconto;
		this.status = status;
		this.fornecedor = new Fornecedor(idFornecedor, nomeContatoFornecedor, idPessoa, nomePessoa, cpfCnpjPessoa, email);
	}
	
	public PedidoCompra( Long id, LocalDate dataPedidoCompra, BigDecimal vlrFrete, BigDecimal vlrDesconto, Long idFornecedor, String nomeContatoFornecedor, Long idPessoa, String nomePessoa, String cpfCnpjPessoa, String email, Long idCotacao, LocalDate dataCotacao, EnumPedidoCompra status) {
		super();
		this.id = id;
		this.dataPedidoCompra = dataPedidoCompra;
		this.vlrFrete = vlrFrete;
		this.vlrDesconto = vlrDesconto;
		this.status = status;
		this.fornecedor = new Fornecedor(idFornecedor, nomeContatoFornecedor, idPessoa, nomePessoa, cpfCnpjPessoa, email);
		this.cotacao = new Cotacao(idCotacao, dataCotacao);
	}

	public PedidoCompra( Long id, LocalDate dataPedidoCompra, BigDecimal vlrFrete, BigDecimal vlrDesconto, Colaborador colaborador, Empresa empresa, Nucleo nucleo, Fornecedor fornecedor, EnumPedidoCompra status  ) {
		super();
		this.id = id;
		this.dataPedidoCompra = dataPedidoCompra;
		this.vlrFrete = vlrFrete;
		this.vlrDesconto = vlrDesconto;
		this.status = status;
		this.colaborador = colaborador;
		this.empresa = empresa;
		this.nucleo = nucleo;
		this.fornecedor = fornecedor;
	}

	/**
	 * Retorna o valor do atributo <code>id</code>
	 *
	 * @return <code>Long</code>
	 */
	public Long getId() {

		return id;
	}

	/**
	 * Define o valor do atributo <code>id</code>.
	 *
	 * @param id
	 */
	public void setId(Long id) {

		this.id = id;
	}

	public LocalDate getDataPedidoCompra() {

		return dataPedidoCompra;
	}

	public void setDataPedidoCompra(LocalDate dataPedidoCompra) {

		this.dataPedidoCompra = dataPedidoCompra;
	}

	public Empresa getEmpresa() {

		return empresa;
	}

	public void setEmpresa(Empresa empresa) {

		this.empresa = empresa;
	}

	public Nucleo getNucleo() {

		return nucleo;
	}

	public void setNucleo(Nucleo nucleo) {

		this.nucleo = nucleo;
	}

	public BigDecimal getVlrFrete() {

		return vlrFrete;
	}

	public void setVlrFrete(BigDecimal vlrFrete) {

		this.vlrFrete = vlrFrete;
	}

	public BigDecimal getVlrDesconto() {

		return vlrDesconto;
	}

	public void setVlrDesconto(BigDecimal vlrDesconto) {

		this.vlrDesconto = vlrDesconto;
	}

	public Colaborador getColaborador() {

		return colaborador;
	}

	public void setColaborador(Colaborador colaborador) {

		this.colaborador = colaborador;
	}

	public Fornecedor getFornecedor() {

		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {

		this.fornecedor = fornecedor;
	}

	public Cotacao getCotacao() {

		return cotacao;
	}

	public void setCotacao(Cotacao cotacao) {

		this.cotacao = cotacao;
	}

	public EnumPedidoCompra getStatus() {

		return status;
	}

	public void setStatus(EnumPedidoCompra status) {

		this.status = status;
	}

	@Override
	public String getLabel() {

		return null;
	}

}
