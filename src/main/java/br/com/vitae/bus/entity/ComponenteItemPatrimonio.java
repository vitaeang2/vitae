package br.com.vitae.bus.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.arquitetura.entidade.Entidade;
import br.com.arquitetura.servico.converter.LocalDateAttributeConverter;

/**
 * 
 * <p>
 * <b>Title:</b> Cargo.java
 * </p>
 * 
 * <p>
 * <b>Description:</b> Classe responsável por representar a entidade
 * ComponenteItemPatrimonio Essa classe servirá apenas para cadatrar itens que
 * fazem parte de um kit do patrimônio, porém não precisa ser cadastrado como um
 * patrimônio, apenas para carater de informação.
 * 
 * Exemplo: Computador ( é um o patrimonio), (Teclado, mouse, são itens do
 * patrimonio computador)
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnologia da Informação
 * </p>
 * 
 * @author Joaquim Barros - joaquimbarros.cmp@gmail.com
 * 
 * @version 1.0.0
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "componente_item_patrimonio")
public class ComponenteItemPatrimonio extends Entidade<Long> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "descricao", nullable = false)
	private String descricao;

	@Column(name = "numero_serie", length = 100)
	private String numeroSerie;

	@Column(name = "fim_garantia")
	@Convert(converter = LocalDateAttributeConverter.class)
	private LocalDate fimGarantia;

	@Column(name = "descricao_garantia")
	private String descricaoGarantia;

	@Column(name = "observacao")
	private String observacao;

	@ManyToOne(fetch = FetchType.LAZY)
	private Patrimonio patrimonio;

	@Column(name = "status", length = 1, nullable = true)
	private Boolean ativo;

	public ComponenteItemPatrimonio() {

	}

	public ComponenteItemPatrimonio(Long id, String descricao) {
		super();
		this.id = id;
		this.descricao = descricao;
	}

	public ComponenteItemPatrimonio(Long id, String descricao, String numeroSerie, LocalDate fimGarantia,
			String descricaoGarantia, String observacao, Patrimonio patrimonio, boolean ativo) {
		super();
		this.id = id;
		this.descricao = descricao;
		this.numeroSerie = numeroSerie;
		this.fimGarantia = fimGarantia;
		this.descricaoGarantia = descricaoGarantia;
		this.observacao = observacao;
		this.patrimonio = patrimonio;
		this.ativo = ativo;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * @param descricao
	 *            the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * @return the numeroSerie
	 */
	public String getNumeroSerie() {
		return numeroSerie;
	}

	/**
	 * @param numeroSerie
	 *            the numeroSerie to set
	 */
	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}

	/**
	 * @return the fimGarantia
	 */
	public LocalDate getFimGarantia() {
		return fimGarantia;
	}

	/**
	 * @param fimGarantia
	 *            the fimGarantia to set
	 */
	public void setFimGarantia(LocalDate fimGarantia) {
		this.fimGarantia = fimGarantia;
	}

	/**
	 * @return the descricaoGarantia
	 */
	public String getDescricaoGarantia() {
		return descricaoGarantia;
	}

	/**
	 * @param descricaoGarantia
	 *            the descricaoGarantia to set
	 */
	public void setDescricaoGarantia(String descricaoGarantia) {
		this.descricaoGarantia = descricaoGarantia;
	}

	/**
	 * @return the observacao
	 */
	public String getObservacao() {
		return observacao;
	}

	/**
	 * @param observacao
	 *            the observacao to set
	 */
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	/**
	 * @return the patrimonio
	 */
	public Patrimonio getPatrimonio() {
		return patrimonio;
	}

	/**
	 * @param patrimonio
	 *            the patrimonio to set
	 */
	public void setPatrimonio(Patrimonio patrimonio) {
		this.patrimonio = patrimonio;
	}

	@Override
	public String getLabel() {
		// TODO Auto-generated method stub
		return null;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

}
