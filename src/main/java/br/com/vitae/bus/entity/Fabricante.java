package br.com.vitae.bus.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.arquitetura.entidade.Entidade;

/**
 * 
 * <p>
 * <b>Title:</b> Fabricante.java
 * </p>
 * 
 * <p>
 * <b>Description:</b> Classe responsável por representar a entidade Fabricante
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnologia da Informa��o
 * </p>
 * 
 * @author Joaquim Barros - joaquimbarros.cmp@gmail.com
 * 
 * @version 1.0.0
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "fabricante")
public class Fabricante extends Entidade<Long> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(length = 200, nullable = false)
	private String nome;

	@Column(name = "status", length = 1, nullable = true)
	private Boolean ativo;

	public Fabricante() {
	}

	public Fabricante(Long id) {
		super();
		this.id = id;
	}

	public Fabricante(Long id, String nome) {
		super();
		this.id = id;
		this.nome = nome;
	}
	
	public Fabricante(Long id, String nome, boolean ativo) {
		super();
		this.id = id;
		this.nome = nome;
		this.ativo = ativo;
	}

	/**
	 * Retorna o valor do atributo <code>id</code>
	 *
	 * @return <code>Long</code>
	 */
	public Long getId() {

		return id;
	}

	/**
	 * Define o valor do atributo <code>id</code>.
	 *
	 * @param id
	 */
	public void setId(Long id) {

		this.id = id;
	}

	/**
	 * Retorna o valor do atributo <code>nome</code>
	 *
	 * @return <code>String</code>
	 */
	public String getNome() {

		return nome;
	}

	/**
	 * Define o valor do atributo <code>nome</code>.
	 *
	 * @param nome
	 */
	public void setNome(String nome) {

		this.nome = nome;
	}

	@Override
	public String getLabel() {

		// TODO Auto-generated method stub
		return null;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
}
