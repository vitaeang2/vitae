package br.com.vitae.bus.entity;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.arquitetura.entidade.Entidade;
import br.com.arquitetura.servico.converter.LocalDateAttributeConverter;
import br.com.arquitetura.util.DataUtil;
import br.com.vitae.bus.enumerator.EnumTipoMovimentoEstoque;

/**
 * 
 * <p>
 * <b>Title: </b> MovimentoEstoque.java
 * </p>
 * 
 * <p>
 * <b>Description: Classe responsável por fazer o controle das movimentos no estoque</b>
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnologia
 * </p>
 * 
 * @author Joaquim Barros - joaquimbarros@mobidental.com.br
 * 
 * @version 1.0.0
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "movimento_estoque")
public class MovimentoEstoque extends Entidade<Long> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "data")
	@Convert(converter = LocalDateAttributeConverter.class)
	private LocalDate data;

	@Column(name = "tipo_movimento_estoque")
	private EnumTipoMovimentoEstoque tipoMovimentoEstoque;

	@JoinColumn(nullable = false)
	@ManyToOne(fetch = FetchType.LAZY)
	private Produto produto;

	@Column(name = "quantidade_movimentada")
	private BigDecimal quantidadeMovimentada;

	@Column(name = "valor")
	private BigDecimal valor;

	@Column(name = "saldo")
	private BigDecimal saldo;

	@JoinColumn(nullable = false)
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	private Colaborador colaborador;

	@ManyToOne(fetch = FetchType.LAZY)
	private Empresa empresa;

	@ManyToOne(fetch = FetchType.LAZY)
	private Nucleo nucleo;

	@Column(name = "observacao")
	private String observacao;

	public MovimentoEstoque() {
	}

	public MovimentoEstoque( Long id, String data, EnumTipoMovimentoEstoque tipoMovimentoEstoque, Produto produto, BigDecimal quantidadeMovimentada, BigDecimal valor, BigDecimal saldo, Colaborador colaborador, Empresa empresa, Nucleo nucleo, String observacao ) {

		super();
		this.id = id;
		this.data = DataUtil.strToLocalDate(data);
		this.tipoMovimentoEstoque = tipoMovimentoEstoque;
		this.produto = produto;
		this.quantidadeMovimentada = quantidadeMovimentada;
		this.valor = valor;
		this.saldo = saldo;
		this.colaborador = colaborador;
		this.empresa = empresa;
		this.nucleo = nucleo;
		this.observacao = observacao;
	}

	/**
	 * Retorna o valor do atributo <code>id</code>
	 *
	 * @return <code>Long</code>
	 */
	public Long getId() {

		return id;
	}

	/**
	 * Define o valor do atributo <code>id</code>.
	 *
	 * @param id
	 */
	public void setId(Long id) {

		this.id = id;
	}

	/**
	 * Retorna o valor do atributo <code>data</code>
	 *
	 * @return <code>LocalDate</code>
	 */
	public LocalDate getData() {

		return data;
	}

	/**
	 * Define o valor do atributo <code>data</code>.
	 *
	 * @param data
	 */
	public void setData(LocalDate data) {

		this.data = data;
	}

	/**
	 * Retorna o valor do atributo <code>tipoMovimentoEstoque</code>
	 *
	 * @return <code>EnumTipoMovimentoEstoque</code>
	 */
	public EnumTipoMovimentoEstoque getTipoMovimentoEstoque() {

		return tipoMovimentoEstoque;
	}

	/**
	 * Define o valor do atributo <code>tipoMovimentoEstoque</code>.
	 *
	 * @param tipoMovimentoEstoque
	 */
	public void setTipoMovimentoEstoque(EnumTipoMovimentoEstoque tipoMovimentoEstoque) {

		this.tipoMovimentoEstoque = tipoMovimentoEstoque;
	}

	/**
	 * Retorna o valor do atributo <code>produto</code>
	 *
	 * @return <code>Produto</code>
	 */
	public Produto getProduto() {

		return produto;
	}

	/**
	 * Define o valor do atributo <code>produto</code>.
	 *
	 * @param produto
	 */
	public void setProduto(Produto produto) {

		this.produto = produto;
	}

	/**
	 * Retorna o valor do atributo <code>quantidadeMovimentada</code>
	 *
	 * @return <code>BigDecimal</code>
	 */
	public BigDecimal getQuantidadeMovimentada() {

		return quantidadeMovimentada;
	}

	/**
	 * Define o valor do atributo <code>quantidadeMovimentada</code>.
	 *
	 * @param quantidadeMovimentada
	 */
	public void setQuantidadeMovimentada(BigDecimal quantidadeMovimentada) {

		this.quantidadeMovimentada = quantidadeMovimentada;
	}

	/**
	 * Retorna o valor do atributo <code>valor</code>
	 *
	 * @return <code>BigDecimal</code>
	 */
	public BigDecimal getValor() {

		return valor;
	}

	/**
	 * Define o valor do atributo <code>valor</code>.
	 *
	 * @param valor
	 */
	public void setValor(BigDecimal valor) {

		this.valor = valor;
	}

	/**
	 * Retorna o valor do atributo <code>saldo</code>
	 *
	 * @return <code>BigDecimal</code>
	 */
	public BigDecimal getSaldo() {

		return saldo;
	}

	/**
	 * Define o valor do atributo <code>saldo</code>.
	 *
	 * @param saldo
	 */
	public void setSaldo(BigDecimal saldo) {

		this.saldo = saldo;
	}

	/**
	 * Retorna o valor do atributo <code>colaborador</code>
	 *
	 * @return <code>Colaborador</code>
	 */
	public Colaborador getColaborador() {

		return colaborador;
	}

	/**
	 * Define o valor do atributo <code>colaborador</code>.
	 *
	 * @param colaborador
	 */
	public void setColaborador(Colaborador colaborador) {

		this.colaborador = colaborador;
	}

	/**
	 * Retorna o valor do atributo <code>empresa</code>
	 *
	 * @return <code>Empresa</code>
	 */
	public Empresa getEmpresa() {

		return empresa;
	}

	/**
	 * Define o valor do atributo <code>empresa</code>.
	 *
	 * @param empresa
	 */
	public void setEmpresa(Empresa empresa) {

		this.empresa = empresa;
	}

	/**
	 * Retorna o valor do atributo <code>nucleo</code>
	 *
	 * @return <code>Nucleo</code>
	 */
	public Nucleo getNucleo() {

		return nucleo;
	}

	/**
	 * Define o valor do atributo <code>nucleo</code>.
	 *
	 * @param nucleo
	 */
	public void setNucleo(Nucleo nucleo) {

		this.nucleo = nucleo;
	}

	/**
	 * Retorna o valor do atributo <code>observacao</code>
	 *
	 * @return <code>String</code>
	 */
	public String getObservacao() {

		return observacao;
	}

	/**
	 * Define o valor do atributo <code>observacao</code>.
	 *
	 * @param observacao
	 */
	public void setObservacao(String observacao) {

		this.observacao = observacao;
	}

	@Override
	public String getLabel() {

		// TODO Auto-generated method stub
		return null;
	}

}
