package br.com.vitae.bus.interceptor;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import br.com.vitae.jwt.JWT;

public class TokenInterceptor extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		String requestURI = request.getRequestURI();
		System.out.println(requestURI);
		String token = request.getHeader("x-access-token");
		System.out.println(token);

		if (!requestURI.equals("/") && !requestURI.equals("/usuario/logar")) {
			try {
				JWT.parseJWT(token);
			} catch (Exception e) {
				response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				PrintWriter writer = response.getWriter();
				writer.println("HTTP Status " + HttpServletResponse.SC_UNAUTHORIZED + " - " + e.getMessage());
				throw new Exception(e.getMessage());
			}
		}

		return super.preHandle(request, response, handler);
	}

	protected void validarToken(String token) throws Exception {

	}

}
