package br.com.vitae.bus.conv;

import java.util.ArrayList;
import java.util.List;

import br.com.arquitetura.servico.converter.Converter;
import br.com.vitae.bus.dto.CargoDTO;
import br.com.vitae.bus.entity.Cargo;

/**
 * 
 * <p>
 * <b>Title:</b> CargoConverter.java
 * </p>
 * 
 * <p>
 * <b>Description:</b>
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnlogia da Informacao
 * </p>
 * 
 * @author Sergio Filho - sergioadsf@gmail.com
 * 
 * @version 1.0.0
 */
public class CargoConverter implements Converter<Long, CargoDTO, Cargo> {

	private static CargoConverter instance;

	static {
		instance = new CargoConverter();
	}

	private CargoConverter() {
	}

	@Override
	public Cargo dto2Entidade(CargoDTO dto) {

		if (dto == null) {
			return null;
		}
		Cargo cargo = new Cargo(dto.getId(), dto.getNome(), dto.getAtivo() == null ? false : dto.getAtivo());
		return cargo;
	}

	@Override
	public CargoDTO entidade2Dto(Cargo entidade) {

		return new CargoDTO(entidade);
	}

	@Override
	public List<CargoDTO> listEntidade2ListDto(List<Cargo> listaEntidade) {

		List<CargoDTO> lista = new ArrayList<>();
		for (Cargo cargo : listaEntidade) {
			lista.add(entidade2Dto(cargo));
		}
		return lista;
	}

	public static CargoConverter getInstance() {

		return instance;
	}

}
