package br.com.vitae.bus.conv;

import java.util.ArrayList;
import java.util.List;

import br.com.arquitetura.servico.converter.Converter;
import br.com.vitae.bus.dto.UsuarioDTO;
import br.com.vitae.bus.entity.Usuario;

public class UsuarioConverter implements Converter<Long, UsuarioDTO, Usuario> {

	private static UsuarioConverter instance;

	static {
		instance = new UsuarioConverter();
	}

	private UsuarioConverter() {
	}

	@Override
	public Usuario dto2Entidade(UsuarioDTO dto) {

		Usuario usuario = new Usuario(dto.getId(), dto.getNome(), dto.getSenha());
		usuario.setToken(dto.getToken());
		return usuario;
	}

	@Override
	public UsuarioDTO entidade2Dto(Usuario entidade) {

		return new UsuarioDTO(entidade);
	}

	@Override
	public List<UsuarioDTO> listEntidade2ListDto(List<Usuario> listaEntidade) {

		List<UsuarioDTO> lista = new ArrayList<>();
		for (Usuario usuario : listaEntidade) {
			lista.add(entidade2Dto(usuario));
		}
		return lista;
	}

	public static UsuarioConverter getInstance() {

		return instance;
	}

}
