package br.com.vitae.bus.conv;

import java.util.ArrayList;
import java.util.List;

import br.com.arquitetura.servico.converter.Converter;
import br.com.vitae.bus.dto.MenuDTO;
import br.com.vitae.bus.entity.Menu;

public class MenuConverter implements Converter<Long, MenuDTO, Menu> {

	private static MenuConverter instance;

	static {
		instance = new MenuConverter();
	}

	private MenuConverter() {
	}

	@Override
	public Menu dto2Entidade(MenuDTO dto) {
		Menu usuario = new Menu(dto.getId(), null, dto.getName(), dto.getCaminho());
		return usuario;
	}

	@Override
	public MenuDTO entidade2Dto(Menu entidade) {
		return new MenuDTO(entidade);
	}

	@Override
	public List<MenuDTO> listEntidade2ListDto(List<Menu> listaEntidade) {
		List<MenuDTO> lista = new ArrayList<>();
		for (Menu usuario : listaEntidade) {
			lista.add(entidade2Dto(usuario));
		}
		return lista;
	}

	public static MenuConverter getInstance() {
		return instance;
	}

}
