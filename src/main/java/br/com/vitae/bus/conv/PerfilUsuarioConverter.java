package br.com.vitae.bus.conv;

import java.util.ArrayList;
import java.util.List;

import br.com.arquitetura.servico.converter.Converter;
import br.com.vitae.bus.dto.PerfilUsuarioDTO;
import br.com.vitae.bus.entity.PerfilUsuario;

public class PerfilUsuarioConverter implements Converter<Long, PerfilUsuarioDTO, PerfilUsuario> {

	private static PerfilUsuarioConverter instance;

	static {
		instance = new PerfilUsuarioConverter();
	}

	private PerfilUsuarioConverter() {
	}

	@Override
	public PerfilUsuario dto2Entidade(PerfilUsuarioDTO dto) {

		PerfilUsuario usuario = new PerfilUsuario(dto.getId(), dto.getNome());
		return usuario;
	}

	@Override
	public PerfilUsuarioDTO entidade2Dto(PerfilUsuario entidade) {

		return new PerfilUsuarioDTO(entidade);
	}

	@Override
	public List<PerfilUsuarioDTO> listEntidade2ListDto(List<PerfilUsuario> listaEntidade) {

		List<PerfilUsuarioDTO> lista = new ArrayList<>();
		for (PerfilUsuario usuario : listaEntidade) {
			lista.add(entidade2Dto(usuario));
		}
		return lista;
	}

	public static PerfilUsuarioConverter getInstance() {

		return instance;
	}

}
