package br.com.vitae.bus.conv;

import java.util.ArrayList;
import java.util.List;

import br.com.arquitetura.servico.converter.Converter;
import br.com.arquitetura.util.DataUtil;
import br.com.vitae.bus.dto.CotacaoDTO;
import br.com.vitae.bus.dto.EmpresaDTO;
import br.com.vitae.bus.dto.NucleoDTO;
import br.com.vitae.bus.entity.Cotacao;
import br.com.vitae.bus.entity.Empresa;
import br.com.vitae.bus.entity.Nucleo;
import br.com.vitae.bus.enumerator.EnumEstadosBrasileiros;
import br.com.vitae.bus.enumerator.EnumStatusCotacao;

/**
 * 
 * <p>
 * <b>Title:</b> CotacaoConverter.java
 * </p>
 * 
 * <p>
 * <b>Description:</b>
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnlogia da Informacao
 * </p>
 * 
 * @author Sergio Filho - sergioadsf@gmail.com
 * 
 * @version 1.0.0
 */
public class CotacaoConverter implements Converter<Long, CotacaoDTO, Cotacao> {

	private static CotacaoConverter instance;

	static {
		instance = new CotacaoConverter();
	}

	private CotacaoConverter() {
	}

	@Override
	public Cotacao dto2Entidade(CotacaoDTO dto) {

		if (dto == null) {
			return null;
		}
		
		NucleoDTO nucleoDTO = dto.getNucleo();
		EmpresaDTO empresaDTO = dto.getEmpresa();
		Nucleo nucleo = null;
		Empresa empresa = null;
		
		if (empresaDTO != null) {
			empresa = new Empresa(empresaDTO.getId());
		}

		if (nucleoDTO != null) {
			Empresa empresaN = null;
			if(nucleoDTO.getIdEmpresa() != null){
				empresaN = new Empresa(nucleoDTO.getIdEmpresa());
			}
			nucleo = new Nucleo(nucleoDTO.getId(), nucleoDTO.getNome(), nucleoDTO.getAtivo(), nucleoDTO.getEmail(), nucleoDTO.getDataCadastro(), nucleoDTO.getTelefone(), nucleoDTO.getEndereco(), nucleoDTO.getCidade(), nucleoDTO.getBairro(), nucleoDTO.getCep(), EnumEstadosBrasileiros.get(nucleoDTO.getUfCidade()), empresaN);
		}

		
		Cotacao cotacao = new Cotacao(dto.getId(), dto.getDescricao(), DataUtil.strToLocalDate(dto.getDataCotacao()), DataUtil.strToLocalDate(dto.getDataValidade()), dto.getStatus() == null ? null : EnumStatusCotacao.get(dto.getStatus()), empresa, nucleo);
		return cotacao;
	}

	@Override
	public CotacaoDTO entidade2Dto(Cotacao entidade) {

		if (entidade == null) {
			return null;
		}

		return new CotacaoDTO(entidade);
	}

	@Override
	public List<CotacaoDTO> listEntidade2ListDto(List<Cotacao> listaEntidade) {

		List<CotacaoDTO> lista = new ArrayList<>();
		for (Cotacao cotacao : listaEntidade) {
			lista.add(entidade2Dto(cotacao));
		}
		return lista;
	}

	public static CotacaoConverter getInstance() {

		return instance;
	}

}
