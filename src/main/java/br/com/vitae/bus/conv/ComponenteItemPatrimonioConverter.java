package br.com.vitae.bus.conv;

import java.util.ArrayList;
import java.util.List;

import br.com.arquitetura.servico.converter.Converter;
import br.com.arquitetura.util.DataUtil;
import br.com.vitae.bus.dto.ComponenteItemPatrimonioDTO;
import br.com.vitae.bus.dto.PatrimonioDTO;
import br.com.vitae.bus.entity.ComponenteItemPatrimonio;
import br.com.vitae.bus.entity.Patrimonio;

/**
 * 
 * <p>
 * <b>Title:</b> ComponenteItemPatrimonioConverter.java
 * </p>
 * 
 * <p>
 * <b>Description:</b>
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnlogia da Informacao
 * </p>
 * 
 * @author Rogério Ferreira - rogerio0308@homtail.com
 * 
 * @version 1.0.0
 */
public class ComponenteItemPatrimonioConverter
		implements Converter<Long, ComponenteItemPatrimonioDTO, ComponenteItemPatrimonio> {

	private static ComponenteItemPatrimonioConverter instance;

	static {
		instance = new ComponenteItemPatrimonioConverter();
	}

	private ComponenteItemPatrimonioConverter() {
	}

	@Override
	public ComponenteItemPatrimonio dto2Entidade(ComponenteItemPatrimonioDTO dto) {

		if (dto == null) {
			return null;
		}
		Patrimonio patrimonio = null;
		PatrimonioDTO patrimonioDTO = dto.getPatrimonio();
		if (patrimonioDTO != null) {
			patrimonio = new Patrimonio(patrimonioDTO.getId());
		}
		ComponenteItemPatrimonio componenteItemPatrimonio = new ComponenteItemPatrimonio(dto.getId(),
				dto.getDescricao(), dto.getNumeroSerie(), DataUtil.strToLocalDate(dto.getFimGarantia()),
				dto.getDescricaoGarantia(), dto.getObservacao(), patrimonio,
				dto.getAtivo() == null ? false : dto.getAtivo());
		return componenteItemPatrimonio;
	}

	@Override
	public ComponenteItemPatrimonioDTO entidade2Dto(ComponenteItemPatrimonio entidade) {

		return new ComponenteItemPatrimonioDTO(entidade);
	}

	@Override
	public List<ComponenteItemPatrimonioDTO> listEntidade2ListDto(List<ComponenteItemPatrimonio> listaEntidade) {

		List<ComponenteItemPatrimonioDTO> lista = new ArrayList<>();
		for (ComponenteItemPatrimonio componenteItemPatrimonio : listaEntidade) {
			lista.add(entidade2Dto(componenteItemPatrimonio));
		}
		return lista;
	}

	public static ComponenteItemPatrimonioConverter getInstance() {

		return instance;
	}

}
