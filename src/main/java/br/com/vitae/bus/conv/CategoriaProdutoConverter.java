package br.com.vitae.bus.conv;

import java.util.ArrayList;
import java.util.List;

import br.com.arquitetura.servico.converter.Converter;
import br.com.vitae.bus.dto.CategoriaProdutoDTO;
import br.com.vitae.bus.entity.CategoriaProduto;

/**
 * 
 * <p>
 * <b>Title:</b> CategoriaProdutoConverter.java
 * </p>
 * 
 * <p>
 * <b>Description:</b>
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnlogia da Informacao
 * </p>
 * 
 * @author Sergio Filho - sergioadsf@gmail.com
 * 
 * @version 1.0.0
 */
public class CategoriaProdutoConverter implements Converter<Long, CategoriaProdutoDTO, CategoriaProduto> {

	private static CategoriaProdutoConverter instance;

	static {
		instance = new CategoriaProdutoConverter();
	}

	private CategoriaProdutoConverter() {
	}

	@Override
	public CategoriaProduto dto2Entidade(CategoriaProdutoDTO dto) {

		if (dto == null) {
			return null;
		}
		CategoriaProduto categoriaProduto = new CategoriaProduto(dto.getId(), dto.getNome(), dto.getAtivo() == null ? false : dto.getAtivo());
		return categoriaProduto;
	}

	@Override
	public CategoriaProdutoDTO entidade2Dto(CategoriaProduto entidade) {

		return new CategoriaProdutoDTO(entidade);
	}

	@Override
	public List<CategoriaProdutoDTO> listEntidade2ListDto(List<CategoriaProduto> listaEntidade) {

		List<CategoriaProdutoDTO> lista = new ArrayList<>();
		for (CategoriaProduto cargo : listaEntidade) {
			lista.add(entidade2Dto(cargo));
		}
		return lista;
	}

	public static CategoriaProdutoConverter getInstance() {

		return instance;
	}

}
