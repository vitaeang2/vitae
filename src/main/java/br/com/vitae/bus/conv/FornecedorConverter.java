package br.com.vitae.bus.conv;

import java.util.ArrayList;
import java.util.List;

import br.com.arquitetura.servico.converter.Converter;
import br.com.arquitetura.util.DataUtil;
import br.com.arquitetura.util.IsNullUtil;
import br.com.vitae.bus.dto.BancoDTO;
import br.com.vitae.bus.dto.FornecedorDTO;
import br.com.vitae.bus.dto.PessoaDTO;
import br.com.vitae.bus.entity.Fornecedor;
import br.com.vitae.bus.entity.NumeroBanco;
import br.com.vitae.bus.entity.Pessoa;
import br.com.vitae.bus.enumerator.EnumEstadoCivil;
import br.com.vitae.bus.enumerator.EnumEstadosBrasileiros;
import br.com.vitae.bus.enumerator.EnumPessoa;
import br.com.vitae.bus.enumerator.EnumSexo;

public class FornecedorConverter implements Converter<Long, FornecedorDTO, Fornecedor> {

	private static FornecedorConverter instance;

	static {
		instance = new FornecedorConverter();
	}

	private FornecedorConverter() {
	}

	@Override
	public Fornecedor dto2Entidade(FornecedorDTO dto) {

		if (dto == null) {
			
			return null;
		}
		
		PessoaDTO pessoaDTO = dto.getPessoa();
		Pessoa pessoa = null;

		if (pessoaDTO != null) {
			
			BancoDTO bancoDTO = pessoaDTO.getBanco();
			NumeroBanco banco = null;
			
			if (bancoDTO != null && !IsNullUtil.isNullOrEmpty(bancoDTO.getId())) {
				
				banco = new NumeroBanco();
				banco.setId(bancoDTO.getId());
				banco.setNome(bancoDTO.getNome());
			}
			pessoa = new Pessoa(pessoaDTO.getId(), pessoaDTO.getNome(), DataUtil.strToLocalDate(pessoaDTO.getDataCadastro()), pessoaDTO.getNaturalidade(), pessoaDTO.getUfNaturalidade(), DataUtil.strToLocalDate(pessoaDTO.getDataNascimento()), EnumEstadoCivil.get(pessoaDTO.getEstadoCivil()), EnumSexo.get(pessoaDTO.getSexo()), pessoaDTO.getCpfCnpj(), pessoaDTO.getRgIe(), pessoaDTO.getEmissorRgIe(), DataUtil.strToLocalDate(pessoaDTO.getDataExpedicaoRgIe()), pessoaDTO.getEndereco(),
					pessoaDTO.getComplemento(), pessoaDTO.getNumeroEndereco(), pessoaDTO.getBairro(), pessoaDTO.getCep(), pessoaDTO.getCidade(), EnumEstadosBrasileiros.get(pessoaDTO.getUfCidade()), pessoaDTO.getTelefone(), pessoaDTO.getCelular(), pessoaDTO.getEmail(), null, pessoaDTO.getObservacao(), banco, pessoaDTO.getAgencia(), pessoaDTO.getContaCorrente(), pessoaDTO.getObservacoesBancaria(), pessoaDTO.getNumeroPIS(), pessoaDTO.getNumeroCTPS(), EnumPessoa.get(pessoaDTO.getTipoPessoa()));
		}
		
		Fornecedor fornecedor = new Fornecedor(dto.getId(), dto.getAtivo(),  pessoa, dto.getNomeContato(), dto.getCelularContato(), dto.isPrestadorServico());
		return fornecedor;
	}

	@Override
	public FornecedorDTO entidade2Dto(Fornecedor entidade) {

		return new FornecedorDTO(entidade);
	}

	@Override
	public List<FornecedorDTO> listEntidade2ListDto(List<Fornecedor> listaEntidade) {

		List<FornecedorDTO> lista = new ArrayList<>();
		for (Fornecedor fornecedor : listaEntidade) {
			lista.add(entidade2Dto(fornecedor));
		}
		return lista;
	}

	public static FornecedorConverter getInstance() {

		return instance;
	}

}
