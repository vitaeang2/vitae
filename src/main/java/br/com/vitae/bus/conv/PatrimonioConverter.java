package br.com.vitae.bus.conv;

import java.util.ArrayList;
import java.util.List;

import br.com.arquitetura.servico.converter.Converter;
import br.com.arquitetura.util.DataUtil;
import br.com.vitae.bus.dto.DepartamentoDTO;
import br.com.vitae.bus.dto.GrupoBemDTO;
import br.com.vitae.bus.dto.PatrimonioDTO;
import br.com.vitae.bus.entity.Departamento;
import br.com.vitae.bus.entity.GrupoBem;
import br.com.vitae.bus.entity.Patrimonio;
import br.com.vitae.bus.enumerator.EnumSituacaoBemPatrimonio;
import br.com.vitae.bus.enumerator.EnumTipoIncorporacaoPatrimonio;
import br.com.vitae.bus.enumerator.EnumUnidadeMedida;

/**
 * 
 * <p>
 * <b>Title:</b> PatrimonioConverter.java
 * </p>
 * 
 * <p>
 * <b>Description:</b>
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnlogia da Informacao
 * </p>
 * 
 * @author Rogério Ferreira - rogerio0308@homtail.com
 * 
 * @version 1.0.0
 */
public class PatrimonioConverter implements Converter<Long, PatrimonioDTO, Patrimonio> {

	private static PatrimonioConverter instance;

	static {
		instance = new PatrimonioConverter();
	}

	private PatrimonioConverter() {
	}

	@Override
	public Patrimonio dto2Entidade(PatrimonioDTO dto) {

		if (dto == null) {
			return null;
		}

		GrupoBem grupoBem = null;
		GrupoBemDTO grupoBemDTO = dto.getGrupoBem();
		if (grupoBemDTO != null) {
			grupoBem = new GrupoBem(grupoBemDTO.getId());
		}

		Departamento departamento = null;
		DepartamentoDTO departamentoDTO = dto.getDepartamento();
		if (departamentoDTO != null) {
			departamento = new Departamento(departamentoDTO.getId());
		}
		
		Patrimonio datrimonio = new Patrimonio(dto.getId(), dto.getNome(), dto.getObservacao(),
				dto.getAtivo() == null ? false : dto.getAtivo(), EnumUnidadeMedida.get(dto.getUnidadeMedida()),
				EnumSituacaoBemPatrimonio.get(dto.getSituacaoBemPatrimonio()), grupoBem, departamento,
				EnumTipoIncorporacaoPatrimonio.get(dto.getTipoIncorporacaoPatrimonio()), dto.getNumeroNota(),
				DataUtil.strToLocalDate(dto.getDataIncorporacao()), dto.getValor(), dto.getNumeroSerie(),
				dto.getNumeroEtiqueta(), DataUtil.strToLocalDate(dto.getDataNota()),
				DataUtil.strToLocalDate(dto.getFimGarantia()), dto.getDescricaoGarantia());
		return datrimonio;
	}

	@Override
	public PatrimonioDTO entidade2Dto(Patrimonio entidade) {

		return new PatrimonioDTO(entidade);
	}

	@Override
	public List<PatrimonioDTO> listEntidade2ListDto(List<Patrimonio> listaEntidade) {

		List<PatrimonioDTO> lista = new ArrayList<>();
		for (Patrimonio cargo : listaEntidade) {
			lista.add(entidade2Dto(cargo));
		}
		return lista;
	}

	public static PatrimonioConverter getInstance() {

		return instance;
	}

}
