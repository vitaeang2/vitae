package br.com.vitae.bus.conv;

import java.util.ArrayList;
import java.util.List;

import br.com.arquitetura.servico.converter.Converter;
import br.com.vitae.bus.dto.NucleoDTO;
import br.com.vitae.bus.entity.Empresa;
import br.com.vitae.bus.entity.Nucleo;
import br.com.vitae.bus.enumerator.EnumEstadosBrasileiros;

public class NucleoConverter implements Converter<Long, NucleoDTO, Nucleo> {

	private static NucleoConverter instance;

	static {
		instance = new NucleoConverter();
	}

	private NucleoConverter() {
	}

	@Override
	public Nucleo dto2Entidade(NucleoDTO dto) {

		if (dto == null) {
			return null;
		}
		Nucleo nucleo = new Nucleo(dto.getId(), dto.getNome(), dto.getAtivo(), dto.getEmail(), dto.getDataCadastro(), dto.getTelefone(), dto.getEndereco(), dto.getCidade(), dto.getBairro(), dto.getCep(), EnumEstadosBrasileiros.get(dto.getUfCidade()), new Empresa(dto.getIdEmpresa(), dto.getNomeEmpresa()));
		return nucleo;
	}

	@Override
	public NucleoDTO entidade2Dto(Nucleo entidade) {

		return new NucleoDTO(entidade);
	}

	@Override
	public List<NucleoDTO> listEntidade2ListDto(List<Nucleo> listaEntidade) {

		List<NucleoDTO> lista = new ArrayList<>();
		for (Nucleo nucleo : listaEntidade) {
			lista.add(entidade2Dto(nucleo));
		}
		return lista;
	}

	public static NucleoConverter getInstance() {

		return instance;
	}

}
