package br.com.vitae.bus.conv;

import java.util.ArrayList;
import java.util.List;

import br.com.arquitetura.servico.converter.Converter;
import br.com.vitae.bus.dto.FabricanteDTO;
import br.com.vitae.bus.entity.Fabricante;

/**
 * 
 * <p>
 * <b>Title:</b> FabricanteConverter.java
 * </p>
 * 
 * <p>
 * <b>Description:</b>
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnlogia da Informacao
 * </p>
 * 
 * @author Sergio Filho - sergioadsf@gmail.com
 * 
 * @version 1.0.0
 */
public class FabricanteConverter implements Converter<Long, FabricanteDTO, Fabricante> {

	private static FabricanteConverter instance;

	static {
		instance = new FabricanteConverter();
	}

	private FabricanteConverter() {
	}

	@Override
	public Fabricante dto2Entidade(FabricanteDTO dto) {

		if (dto == null) {
			return null;
		}
		Fabricante cargo = new Fabricante(dto.getId(), dto.getNome(), dto.getAtivo() == null ? false : dto.getAtivo());
		return cargo;
	}

	@Override
	public FabricanteDTO entidade2Dto(Fabricante entidade) {

		return new FabricanteDTO(entidade);
	}

	@Override
	public List<FabricanteDTO> listEntidade2ListDto(List<Fabricante> listaEntidade) {

		List<FabricanteDTO> lista = new ArrayList<>();
		for (Fabricante cargo : listaEntidade) {
			lista.add(entidade2Dto(cargo));
		}
		return lista;
	}

	public static FabricanteConverter getInstance() {

		return instance;
	}

}
