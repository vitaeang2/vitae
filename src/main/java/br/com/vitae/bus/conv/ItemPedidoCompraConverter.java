package br.com.vitae.bus.conv;

import java.util.ArrayList;
import java.util.List;

import br.com.arquitetura.servico.converter.Converter;
import br.com.vitae.bus.dto.ItemPedidoCompraDTO;
import br.com.vitae.bus.dto.PedidoCompraDTO;
import br.com.vitae.bus.dto.ProdutoDTO;
import br.com.vitae.bus.entity.ItemPedidoCompra;
import br.com.vitae.bus.entity.PedidoCompra;
import br.com.vitae.bus.entity.Produto;
import br.com.vitae.bus.enumerator.EnumItemPedidoCompra;

public class ItemPedidoCompraConverter implements Converter<Long, ItemPedidoCompraDTO, ItemPedidoCompra> {

	private static ItemPedidoCompraConverter instance;

	static {
		instance = new ItemPedidoCompraConverter();
	}

	private ItemPedidoCompraConverter() {
	}

	@Override
	public ItemPedidoCompra dto2Entidade(ItemPedidoCompraDTO dto) {

		if (dto == null) {
			return null;
		}

		PedidoCompraDTO pedidoCompraDTO = dto.getPedidoCompra();
		ProdutoDTO produtoDTO = dto.getProduto();
		
		PedidoCompra pedidoCompra = null;
		Produto produto = null;

		if (pedidoCompraDTO != null) {
			pedidoCompra = new PedidoCompra(pedidoCompraDTO.getId());
		}

		if (produtoDTO != null) {
			produto = new Produto(produtoDTO.getId());
		}

		ItemPedidoCompra itemPedidoCompra = new ItemPedidoCompra(dto.getId(),dto.getVlrUnitario(), dto.getQuantidade(), pedidoCompra, produto, dto.getStatus() != null ? EnumItemPedidoCompra.get(dto.getStatus()) : null);
		return itemPedidoCompra;
	}

	@Override
	public ItemPedidoCompraDTO entidade2Dto(ItemPedidoCompra entidade) {

		return new ItemPedidoCompraDTO(entidade);
	}

	@Override
	public List<ItemPedidoCompraDTO> listEntidade2ListDto(List<ItemPedidoCompra> listaEntidade) {

		List<ItemPedidoCompraDTO> lista = new ArrayList<>();
		for (ItemPedidoCompra colaborador : listaEntidade) {
			lista.add(entidade2Dto(colaborador));
		}
		return lista;
	}

	public static ItemPedidoCompraConverter getInstance() {

		return instance;
	}

}
