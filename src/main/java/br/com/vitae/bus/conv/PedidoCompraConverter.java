package br.com.vitae.bus.conv;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import br.com.arquitetura.servico.converter.Converter;
import br.com.arquitetura.util.DataUtil;
import br.com.vitae.bus.dto.ColaboradorDTO;
import br.com.vitae.bus.dto.CotacaoDTO;
import br.com.vitae.bus.dto.EmpresaDTO;
import br.com.vitae.bus.dto.FornecedorDTO;
import br.com.vitae.bus.dto.NucleoDTO;
import br.com.vitae.bus.dto.PedidoCompraDTO;
import br.com.vitae.bus.entity.Colaborador;
import br.com.vitae.bus.entity.Cotacao;
import br.com.vitae.bus.entity.Empresa;
import br.com.vitae.bus.entity.Fornecedor;
import br.com.vitae.bus.entity.Nucleo;
import br.com.vitae.bus.entity.PedidoCompra;
import br.com.vitae.bus.enumerator.EnumEstadosBrasileiros;
import br.com.vitae.bus.enumerator.EnumPedidoCompra;

public class PedidoCompraConverter implements Converter<Long, PedidoCompraDTO, PedidoCompra> {

	private static PedidoCompraConverter instance;

	static {
		instance = new PedidoCompraConverter();
	}

	private PedidoCompraConverter() {
	}

	@Override
	public PedidoCompra dto2Entidade(PedidoCompraDTO dto) {

		if (dto == null) {
			return null;
		}
		
		FornecedorDTO fornecedorDTO = dto.getFornecedor();
		NucleoDTO nucleoDTO = dto.getNucleo();
		EmpresaDTO empresaDTO = dto.getEmpresa();
		ColaboradorDTO colaboradorDTO = dto.getColaborador();
		CotacaoDTO cotacaoDTO = dto.getCotacao();
		Nucleo nucleo = null;
		Empresa empresa = null;
		Colaborador colaborador = null;
		Cotacao cotacao = null;

		Fornecedor fornecedor = null;

		if (fornecedorDTO != null) {
			fornecedor = new Fornecedor(fornecedorDTO.getId());
		}
		
		if (empresaDTO != null) {
			empresa = new Empresa(empresaDTO.getId());
		}
		
		if (colaboradorDTO != null) {
			colaborador = new Colaborador(colaboradorDTO.getId());
		}

		if (cotacaoDTO != null) {
			cotacao = new Cotacao();
			cotacao.setId(cotacaoDTO.getId());
			cotacao.setDescricao(cotacaoDTO.getDescricao());
			cotacao.setDataCotacao(DataUtil.strToLocalDate(cotacaoDTO.getDataCotacao()));
		}

		if (nucleoDTO != null) {
			Empresa empresaN = null;
			if (nucleoDTO.getIdEmpresa() != null) {
				empresaN = new Empresa(nucleoDTO.getIdEmpresa());
			}
			nucleo = new Nucleo(nucleoDTO.getId(), nucleoDTO.getNome(), nucleoDTO.getAtivo(), nucleoDTO.getEmail(), nucleoDTO.getDataCadastro(), nucleoDTO.getTelefone(), nucleoDTO.getEndereco(), nucleoDTO.getCidade(), nucleoDTO.getBairro(), nucleoDTO.getCep(), EnumEstadosBrasileiros.get(nucleoDTO.getUfCidade()), empresaN);
		}

		PedidoCompra pedidoCompra = new PedidoCompra(dto.getId(), dto.getDataPedidoCompra() == null ? LocalDate.now() : DataUtil.strToLocalDate(dto.getDataPedidoCompra()), dto.getVlrFrete(), dto.getVlrDesconto(), colaborador, empresa, nucleo, fornecedor, dto.getStatus() == null? EnumPedidoCompra.ABERTO : EnumPedidoCompra.get(dto.getStatus()));
		return pedidoCompra;
	}

	@Override
	public PedidoCompraDTO entidade2Dto(PedidoCompra entidade) {

		return new PedidoCompraDTO(entidade);
	}

	@Override
	public List<PedidoCompraDTO> listEntidade2ListDto(List<PedidoCompra> listaEntidade) {

		List<PedidoCompraDTO> lista = new ArrayList<>();
		for (PedidoCompra colaborador : listaEntidade) {
			lista.add(entidade2Dto(colaborador));
		}
		return lista;
	}

	public static PedidoCompraConverter getInstance() {

		return instance;
	}

}
