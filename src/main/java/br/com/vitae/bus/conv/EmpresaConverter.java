package br.com.vitae.bus.conv;

import java.util.ArrayList;
import java.util.List;

import br.com.arquitetura.servico.converter.Converter;
import br.com.vitae.bus.dto.EmpresaDTO;
import br.com.vitae.bus.entity.Empresa;
import br.com.vitae.bus.enumerator.EnumEstadosBrasileiros;
import br.com.vitae.bus.enumerator.EnumPessoa;

public class EmpresaConverter implements Converter<Long, EmpresaDTO, Empresa> {

	private static EmpresaConverter instance;

	static {
		instance = new EmpresaConverter();
	}

	private EmpresaConverter() {
	}

	@Override
	public Empresa dto2Entidade(EmpresaDTO dto) {

		if (dto == null) {
			return null;
		}

		Empresa empresa = new Empresa(dto.getId(), dto.getNome(), dto.getAtivo(), dto.getEmail(), dto.getDataCadastro(), dto.getTelefone(), dto.getCpfCnpj(), dto.getInscricaoEstadual(), dto.getInscricaoMunicipal(), dto.getEndereco(), dto.getCidade(), dto.getBairro(), dto.getCep(), EnumEstadosBrasileiros.get(dto.getUfCidade()), EnumPessoa.get(dto.getTipoPessoa()));
		return empresa;
	}

	@Override
	public EmpresaDTO entidade2Dto(Empresa entidade) {

		return new EmpresaDTO(entidade);
	}

	@Override
	public List<EmpresaDTO> listEntidade2ListDto(List<Empresa> listaEntidade) {

		List<EmpresaDTO> lista = new ArrayList<>();
		for (Empresa empresa : listaEntidade) {
			lista.add(entidade2Dto(empresa));
		}
		return lista;
	}

	public static EmpresaConverter getInstance() {

		return instance;
	}

}
