package br.com.vitae.bus.conv;

import java.util.ArrayList;
import java.util.List;

import br.com.arquitetura.servico.converter.Converter;
import br.com.arquitetura.util.DataUtil;
import br.com.vitae.bus.dto.CotacaoDTO;
import br.com.vitae.bus.dto.CotacaoFornecedorDTO;
import br.com.vitae.bus.dto.FornecedorDTO;
import br.com.vitae.bus.entity.Cotacao;
import br.com.vitae.bus.entity.CotacaoFornecedor;
import br.com.vitae.bus.entity.Fornecedor;
import br.com.vitae.bus.enumerator.EnumStatusCotacao;
import br.com.vitae.bus.enumerator.EnumStatusCotacaoFornecedor;

/**
 * 
 * <p>
 * <b>Title:</b> CotacaoFornecedorConverter.java
 * </p>
 * 
 * <p>
 * <b>Description:</b>
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnlogia da Informacao
 * </p>
 * 
 * @author Sergio Filho - sergioadsf@gmail.com
 * 
 * @version 1.0.0
 */
public class CotacaoFornecedorConverter implements Converter<Long, CotacaoFornecedorDTO, CotacaoFornecedor> {

	private static CotacaoFornecedorConverter instance;

	static {
		instance = new CotacaoFornecedorConverter();
	}

	private CotacaoFornecedorConverter() {
	}

	@Override
	public CotacaoFornecedor dto2Entidade(CotacaoFornecedorDTO dto) {

		if (dto == null) {
			return null;
		}

		Cotacao cotacao = null;
		CotacaoDTO cotacaoDTO = dto.getCotacao();
		if (cotacaoDTO != null) {
			cotacao = new Cotacao(cotacaoDTO.getId(), cotacaoDTO.getDescricao(), DataUtil.strToLocalDate(cotacaoDTO.getDataCotacao()), DataUtil.strToLocalDate(cotacaoDTO.getDataValidade()), EnumStatusCotacao.get(cotacaoDTO.getStatus()));
		}

		Fornecedor fornecedor = null;
		FornecedorDTO fornecedorDTO = dto.getFornecedor();
		if (fornecedorDTO != null) {
			fornecedor = new Fornecedor(fornecedorDTO.getId());
		}

		CotacaoFornecedor cotacaoFornecedor = new CotacaoFornecedor(dto.getId(), dto.getVlrFrete(), dto.getVlrDesconto(), dto.getToken(), cotacao, fornecedor, dto.getStatus() == null ? null : EnumStatusCotacaoFornecedor.get(dto.getStatus()));
		return cotacaoFornecedor;
	}

	@Override
	public CotacaoFornecedorDTO entidade2Dto(CotacaoFornecedor entidade) {

		return new CotacaoFornecedorDTO(entidade);
	}

	@Override
	public List<CotacaoFornecedorDTO> listEntidade2ListDto(List<CotacaoFornecedor> listaEntidade) {

		List<CotacaoFornecedorDTO> lista = new ArrayList<>();
		for (CotacaoFornecedor cotacaoFornecedor : listaEntidade) {
			lista.add(entidade2Dto(cotacaoFornecedor));
		}
		return lista;
	}

	public static CotacaoFornecedorConverter getInstance() {

		return instance;
	}

}
