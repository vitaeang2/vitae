package br.com.vitae.bus.conv;

import java.util.ArrayList;
import java.util.List;

import br.com.arquitetura.servico.converter.Converter;
import br.com.arquitetura.util.DataUtil;
import br.com.vitae.bus.dto.EmpresaDTO;
import br.com.vitae.bus.dto.LocalDTO;
import br.com.vitae.bus.dto.NucleoDTO;
import br.com.vitae.bus.dto.SolicitacaoDTO;
import br.com.vitae.bus.dto.TipoSolicitacaoDTO;
import br.com.vitae.bus.dto.UsuarioDTO;
import br.com.vitae.bus.entity.Empresa;
import br.com.vitae.bus.entity.Local;
import br.com.vitae.bus.entity.Nucleo;
import br.com.vitae.bus.entity.Solicitacao;
import br.com.vitae.bus.entity.TipoSolicitacao;
import br.com.vitae.bus.entity.Usuario;
import br.com.vitae.bus.enumerator.EnumStatusSolicitacao;

public class SolicitacaoConverter implements Converter<Long, SolicitacaoDTO, Solicitacao> {

	private static SolicitacaoConverter instance;

	static {
		instance = new SolicitacaoConverter();
	}

	private SolicitacaoConverter() {
	}

	@Override
	public Solicitacao dto2Entidade(SolicitacaoDTO dto) {

		if (dto == null) {
			return null;
		}
		NucleoDTO nucleoDTO = dto.getNucleo();
		Nucleo nucleo = null;
		EmpresaDTO empresaDTO = dto.getEmpresa();
		Empresa empresa = null;
		UsuarioDTO usuarioDTO = dto.getResponsavel();
		Usuario responsavel = null;
		UsuarioDTO criadorDTO = dto.getCriador();
		Usuario criador = null;
		TipoSolicitacaoDTO tipoSolicitacaoDTO = dto.getTipoSolicitacao();
		TipoSolicitacao tipoSolicitacao = null;
		LocalDTO localDTO = dto.getLocal();
		Local local = null;

		if (usuarioDTO != null && usuarioDTO.getId() != null && usuarioDTO.getId().longValue() != 0) {
			responsavel = new Usuario(usuarioDTO.getId());
		}
		
		if (criadorDTO != null && criadorDTO.getId() != null && criadorDTO.getId().longValue() != 0) {
			criador = new Usuario(criadorDTO.getId());
		}
		
		if (tipoSolicitacaoDTO != null && tipoSolicitacaoDTO.getId() != null && tipoSolicitacaoDTO.getId().longValue() != 0) {
			tipoSolicitacao = new TipoSolicitacao(tipoSolicitacaoDTO.getId());
		}

		if (empresaDTO != null && empresaDTO.getId() != null && empresaDTO.getId().longValue() != 0) {
			empresa = new Empresa(empresaDTO.getId());
		}

		if (nucleoDTO != null && nucleoDTO.getId() != null && nucleoDTO.getId().longValue() != 0) {
			Empresa empresaN = null;
			nucleo = new Nucleo(nucleoDTO.getId());
			if (nucleoDTO.getIdEmpresa() != null) {
				empresaN = new Empresa(nucleoDTO.getIdEmpresa());
				nucleo.setEmpresa(empresaN);
			}
		}
		
		if (localDTO != null && localDTO.getId() != null && localDTO.getId().longValue() != 0) {
			local = new Local(localDTO.getId());
		}

		Solicitacao solicitacao = new Solicitacao(dto.getId(), dto.getAtivo() == null ? false : dto.getAtivo(),
				dto.getAssunto(), dto.getDescricao(), dto.getSolucao(), dto.getNumeroEtiqueta(), empresa, nucleo, responsavel, criador, tipoSolicitacao,
				EnumStatusSolicitacao.get(dto.getStatusSolicitacao()), DataUtil.strToLocalDate(dto.getAbertura()),
				DataUtil.strToLocalDate(dto.getTermino()), DataUtil.strToLocalDate(dto.getPrevisaoFinalizacao()), local);
		return solicitacao;
	}

	@Override
	public SolicitacaoDTO entidade2Dto(Solicitacao entidade) {

		return new SolicitacaoDTO(entidade);
	}

	@Override
	public List<SolicitacaoDTO> listEntidade2ListDto(List<Solicitacao> listaEntidade) {

		List<SolicitacaoDTO> lista = new ArrayList<>();
		for (Solicitacao solicitacao : listaEntidade) {
			lista.add(entidade2Dto(solicitacao));
		}
		return lista;
	}

	public static SolicitacaoConverter getInstance() {

		return instance;
	}

}
