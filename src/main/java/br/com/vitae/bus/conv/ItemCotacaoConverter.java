package br.com.vitae.bus.conv;

import java.util.ArrayList;
import java.util.List;

import br.com.arquitetura.servico.converter.Converter;
import br.com.vitae.bus.dto.CotacaoFornecedorDTO;
import br.com.vitae.bus.dto.ItemCotacaoDTO;
import br.com.vitae.bus.dto.ProdutoDTO;
import br.com.vitae.bus.entity.CotacaoFornecedor;
import br.com.vitae.bus.entity.ItemCotacao;
import br.com.vitae.bus.entity.Produto;

/**
 * 
 * <p>
 * <b>Title:</b> ItemCotacaoConverter.java
 * </p>
 * 
 * <p>
 * <b>Description:</b>
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnlogia da Informacao
 * </p>
 * 
 * @author Sergio Filho - sergioadsf@gmail.com
 * 
 * @version 1.0.0
 */
public class ItemCotacaoConverter implements Converter<Long, ItemCotacaoDTO, ItemCotacao> {

	private static ItemCotacaoConverter instance;

	static {
		instance = new ItemCotacaoConverter();
	}

	private ItemCotacaoConverter() {
	}

	@Override
	public ItemCotacao dto2Entidade(ItemCotacaoDTO dto) {

		if (dto == null) {
			return null;
		}

		CotacaoFornecedor cotacaoFornecedor = null;
		CotacaoFornecedorDTO cotacaoFornecedorDTO = dto.getCotacaoFornecedor();
		if (cotacaoFornecedorDTO != null) {
			cotacaoFornecedor = CotacaoFornecedorConverter.getInstance().dto2Entidade(cotacaoFornecedorDTO);
		}

		Produto produto = null;
		ProdutoDTO produtoDTO = dto.getProduto();
		if (produtoDTO != null) {
			produto = ProdutoConverter.getInstance().dto2Entidade(produtoDTO);
		}

		ItemCotacao itemCotacao = new ItemCotacao(dto.getId(), dto.getVlrUnitario(), dto.getQuantidade(), cotacaoFornecedor, produto);
		return itemCotacao;
	}

	@Override
	public ItemCotacaoDTO entidade2Dto(ItemCotacao entidade) {

		return new ItemCotacaoDTO(entidade);
	}

	@Override
	public List<ItemCotacaoDTO> listEntidade2ListDto(List<ItemCotacao> listaEntidade) {

		List<ItemCotacaoDTO> lista = new ArrayList<>();
		for (ItemCotacao itemCotacao : listaEntidade) {
			lista.add(entidade2Dto(itemCotacao));
		}
		return lista;
	}

	public static ItemCotacaoConverter getInstance() {

		return instance;
	}

}
