package br.com.vitae.bus.conv;

import java.util.ArrayList;
import java.util.List;

import br.com.arquitetura.servico.converter.Converter;
import br.com.vitae.bus.dto.ColaboradorDTO;
import br.com.vitae.bus.dto.EmpresaDTO;
import br.com.vitae.bus.dto.MovimentoEstoqueDTO;
import br.com.vitae.bus.dto.NucleoDTO;
import br.com.vitae.bus.dto.ProdutoDTO;
import br.com.vitae.bus.entity.Colaborador;
import br.com.vitae.bus.entity.Empresa;
import br.com.vitae.bus.entity.MovimentoEstoque;
import br.com.vitae.bus.entity.Nucleo;
import br.com.vitae.bus.entity.Produto;
import br.com.vitae.bus.enumerator.EnumTipoMovimentoEstoque;

public class MovimentoEstoqueConverter implements Converter<Long, MovimentoEstoqueDTO, MovimentoEstoque> {

	private static MovimentoEstoqueConverter instance;

	static {
		instance = new MovimentoEstoqueConverter();
	}

	private MovimentoEstoqueConverter() {

	}

	@Override
	public MovimentoEstoque dto2Entidade(MovimentoEstoqueDTO dto) {

		if (dto == null) {
			return null;
		}

		EmpresaDTO empresaDTO = dto.getEmpresa();
		NucleoDTO nucleoDTO = dto.getNucleo();
		ColaboradorDTO colaboradorDTO = dto.getColaborador();
		ProdutoDTO produtoDTO = dto.getProduto();

		Colaborador colaborador = null;
		Nucleo nucleo = null;
		Empresa empresa = null;
		Produto produto = null;

		if (empresaDTO != null) {

			empresa = new Empresa(empresaDTO.getId());
		}

		if (nucleoDTO != null) {

			nucleo = new Nucleo(nucleoDTO.getId());
		}

		if (colaboradorDTO != null) {

			colaborador = new Colaborador(colaboradorDTO.getId());
		}

		EnumTipoMovimentoEstoque tipoMovimentoEstoque = EnumTipoMovimentoEstoque.get(dto.getTipoMovimentoEstoque());

		if (produtoDTO != null) {

			produto = new Produto(produtoDTO.getId());
		}

		MovimentoEstoque movimentoEstoque = new MovimentoEstoque(dto.getId(), dto.getData(), tipoMovimentoEstoque, produto, dto.getQuantidadeMovimentada(), dto.getValor(), dto.getSaldo(), colaborador, empresa, nucleo, dto.getObservacao());

		return movimentoEstoque;

	}

	@Override
	public MovimentoEstoqueDTO entidade2Dto(MovimentoEstoque entidade) {

		return new MovimentoEstoqueDTO(entidade);
	}

	@Override
	public List<MovimentoEstoqueDTO> listEntidade2ListDto(List<MovimentoEstoque> listaEntidade) {

		List<MovimentoEstoqueDTO> lista = new ArrayList<>();
		
		for (MovimentoEstoque movimento : listaEntidade) {
			
			lista.add(entidade2Dto(movimento));
		}
		
		return lista;
	}

	public static MovimentoEstoqueConverter getInstance() {

		return instance;
	}

}
