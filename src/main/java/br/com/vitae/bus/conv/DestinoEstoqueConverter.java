package br.com.vitae.bus.conv;

import java.util.ArrayList;
import java.util.List;

import br.com.arquitetura.servico.converter.Converter;
import br.com.vitae.bus.dto.DestinoEstoqueDTO;
import br.com.vitae.bus.entity.DestinoEstoque;

/**
 * 
 * <p>
 * <b>Title:</b> DestinoEstoqueConverter.java
 * </p>
 * 
 * <p>
 * <b>Description:</b>
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnlogia da Informacao
 * </p>
 * 
 * @author Sergio Filho - sergioadsf@gmail.com
 * 
 * @version 1.0.0
 */
public class DestinoEstoqueConverter implements Converter<Long, DestinoEstoqueDTO, DestinoEstoque> {

	private static DestinoEstoqueConverter instance;

	static {
		instance = new DestinoEstoqueConverter();
	}

	private DestinoEstoqueConverter() {
	}

	@Override
	public DestinoEstoque dto2Entidade(DestinoEstoqueDTO dto) {

		if (dto == null) {
			return null;
		}
		DestinoEstoque cargo = new DestinoEstoque(dto.getId(), dto.getDescricao(), dto.getAtivo());
		return cargo;
	}

	@Override
	public DestinoEstoqueDTO entidade2Dto(DestinoEstoque entidade) {

		return new DestinoEstoqueDTO(entidade);
	}

	@Override
	public List<DestinoEstoqueDTO> listEntidade2ListDto(List<DestinoEstoque> listaEntidade) {

		List<DestinoEstoqueDTO> lista = new ArrayList<>();
		for (DestinoEstoque entidade : listaEntidade) {
			lista.add(entidade2Dto(entidade));
		}
		return lista;
	}

	public static DestinoEstoqueConverter getInstance() {

		return instance;
	}

}
