package br.com.vitae.bus.conv;

import java.util.ArrayList;
import java.util.List;

import br.com.arquitetura.servico.converter.Converter;
import br.com.vitae.bus.dto.EmpresaDTO;
import br.com.vitae.bus.dto.NucleoDTO;
import br.com.vitae.bus.dto.ProdutoDTO;
import br.com.vitae.bus.dto.ProdutoEstoqueDTO;
import br.com.vitae.bus.entity.Empresa;
import br.com.vitae.bus.entity.Nucleo;
import br.com.vitae.bus.entity.Produto;
import br.com.vitae.bus.entity.ProdutoEstoque;
import br.com.vitae.bus.enumerator.EnumEstadosBrasileiros;

/**
 * 
 * <p>
 * <b>Title:</b> ProdutoEstoqueConverter.java
 * </p>
 * 
 * <p>
 * <b>Description:</b>
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnlogia da Informacao
 * </p>
 * 
 * @author Sergio Filho - sergioadsf@gmail.com
 * 
 * @version 1.0.0
 */
public class ProdutoEstoqueConverter implements Converter<Long, ProdutoEstoqueDTO, ProdutoEstoque> {

	private static ProdutoEstoqueConverter instance;

	static {
		instance = new ProdutoEstoqueConverter();
	}

	private ProdutoEstoqueConverter() {
	}

	@Override
	public ProdutoEstoque dto2Entidade(ProdutoEstoqueDTO dto) {

		if (dto == null) {
			return null;
		}
		
		NucleoDTO nucleoDTO = dto.getNucleo();
		EmpresaDTO empresaDTO = dto.getEmpresa();
		ProdutoDTO produtoDTO = dto.getProduto();
		Nucleo nucleo = null;
		Empresa empresa = null;
		Produto produto = null;
		
		if (produtoDTO != null) {
			produto = new Produto(produtoDTO.getId());
		}
		
		if (empresaDTO != null) {
			empresa = new Empresa(empresaDTO.getId());
		}

		if (nucleoDTO != null) {
			Empresa empresaN = null;
			if(nucleoDTO.getIdEmpresa() != null){
				empresaN = new Empresa(nucleoDTO.getIdEmpresa());
			}
			nucleo = new Nucleo(nucleoDTO.getId(), nucleoDTO.getNome(), nucleoDTO.getAtivo(), nucleoDTO.getEmail(), nucleoDTO.getDataCadastro(), nucleoDTO.getTelefone(), nucleoDTO.getEndereco(), nucleoDTO.getCidade(), nucleoDTO.getBairro(), nucleoDTO.getCep(), EnumEstadosBrasileiros.get(nucleoDTO.getUfCidade()), empresaN);
		}

		
		ProdutoEstoque cotacao = new ProdutoEstoque(dto.getId(), dto.getQuantidade(), produto, empresa, nucleo);
		return cotacao;
	}

	@Override
	public ProdutoEstoqueDTO entidade2Dto(ProdutoEstoque entidade) {

		if (entidade == null) {
			return null;
		}

		return new ProdutoEstoqueDTO(entidade);
	}

	@Override
	public List<ProdutoEstoqueDTO> listEntidade2ListDto(List<ProdutoEstoque> listaEntidade) {

		List<ProdutoEstoqueDTO> lista = new ArrayList<>();
		for (ProdutoEstoque cotacao : listaEntidade) {
			lista.add(entidade2Dto(cotacao));
		}
		return lista;
	}

	public static ProdutoEstoqueConverter getInstance() {

		return instance;
	}

}
