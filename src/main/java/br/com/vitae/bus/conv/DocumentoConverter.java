package br.com.vitae.bus.conv;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import br.com.arquitetura.servico.converter.Converter;
import br.com.arquitetura.util.DataUtil;
import br.com.vitae.bus.dto.DocumentoDTO;
import br.com.vitae.bus.entity.Colaborador;
import br.com.vitae.bus.entity.Documento;
import br.com.vitae.bus.entity.Usuario;
import br.com.vitae.bus.enumerator.EnumDocumento;
import br.com.vitae.bus.util.ConversorUtil;

public class DocumentoConverter implements Converter<Long, DocumentoDTO, Documento> {

	private static DocumentoConverter instance;

	static {
		instance = new DocumentoConverter();
	}

	private DocumentoConverter() {
	}

	@Override
	public Documento dto2Entidade(DocumentoDTO dto) {

		if (dto == null) {
			return null;
		}
		
		BigDecimal valor = ConversorUtil.strToBigDecimal(dto.getValorRecebido());
		
		Documento doc = new Documento(dto.getId(), dto.getAtivo(), new Colaborador(dto.getIdColaborador()), new Usuario(dto.getIdUsuarioAdicionou()), 
				DataUtil.strToLocalDate(dto.getDataEvento()), DataUtil.strToLocalDate(dto.getDataCadastro()), EnumDocumento.get(dto.getTipoDocumento()), 
				dto.getHistorico(), dto.getTipoArquivo(), valor, dto.getCaminhoArquivo(), dto.getNomeArquivo());
		return doc;
	}

	@Override
	public DocumentoDTO entidade2Dto(Documento entidade) {

		return new DocumentoDTO(entidade);
	}

	@Override
	public List<DocumentoDTO> listEntidade2ListDto(List<Documento> listaEntidade) {

		List<DocumentoDTO> lista = new ArrayList<>();
		for (Documento nucleo : listaEntidade) {
			lista.add(entidade2Dto(nucleo));
		}
		return lista;
	}

	public static DocumentoConverter getInstance() {

		return instance;
	}

}
