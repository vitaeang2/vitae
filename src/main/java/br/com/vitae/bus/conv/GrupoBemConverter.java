package br.com.vitae.bus.conv;

import java.util.ArrayList;
import java.util.List;

import br.com.arquitetura.servico.converter.Converter;
import br.com.vitae.bus.dto.GrupoBemDTO;
import br.com.vitae.bus.entity.GrupoBem;

/**
 * 
 * <p>
 * <b>Title:</b> GrupoBemConverter.java
 * </p>
 * 
 * <p>
 * <b>Description:</b>
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnlogia da Informacao
 * </p>
 * 
 * @author Rogério Ferreira - rogerio0308@homtail.com
 * 
 * @version 1.0.0
 */
public class GrupoBemConverter implements Converter<Long, GrupoBemDTO, GrupoBem> {

	private static GrupoBemConverter instance;

	static {
		instance = new GrupoBemConverter();
	}

	private GrupoBemConverter() {
	}

	@Override
	public GrupoBem dto2Entidade(GrupoBemDTO dto) {

		if (dto == null) {
			return null;
		}
		GrupoBem cargo = new GrupoBem(dto.getId(), dto.getNome(), dto.getAtivo() == null ? false : dto.getAtivo(), dto.getDepreciacaoAnual(), dto.getObservacao());
		return cargo;
	}

	@Override
	public GrupoBemDTO entidade2Dto(GrupoBem entidade) {

		return new GrupoBemDTO(entidade);
	}

	@Override
	public List<GrupoBemDTO> listEntidade2ListDto(List<GrupoBem> listaEntidade) {

		List<GrupoBemDTO> lista = new ArrayList<>();
		for (GrupoBem cargo : listaEntidade) {
			lista.add(entidade2Dto(cargo));
		}
		return lista;
	}

	public static GrupoBemConverter getInstance() {

		return instance;
	}

}
