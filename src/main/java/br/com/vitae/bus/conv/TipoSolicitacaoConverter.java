package br.com.vitae.bus.conv;

import java.util.ArrayList;
import java.util.List;

import br.com.arquitetura.servico.converter.Converter;
import br.com.vitae.bus.dto.TipoSolicitacaoDTO;
import br.com.vitae.bus.entity.TipoSolicitacao;

/**
 * 
 * <p>
 * <b>Title:</b> TipoSolicitacaoConverter.java
 * </p>
 * 
 * <p>
 * <b>Description:</b>
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnlogia da Informacao
 * </p>
 * 
 * @author Rogério Ferreira - rogerio0308@homtail.com
 * 
 * @version 1.0.0
 */
public class TipoSolicitacaoConverter implements Converter<Long, TipoSolicitacaoDTO, TipoSolicitacao> {

	private static TipoSolicitacaoConverter instance;

	static {
		instance = new TipoSolicitacaoConverter();
	}

	private TipoSolicitacaoConverter() {
	}

	@Override
	public TipoSolicitacao dto2Entidade(TipoSolicitacaoDTO dto) {

		if (dto == null) {
			return null;
		}
		TipoSolicitacao tipoSolicitacao = new TipoSolicitacao(dto.getId(), dto.getNome(), dto.getAtivo() == null ? false : dto.getAtivo());
		return tipoSolicitacao;
	}

	@Override
	public TipoSolicitacaoDTO entidade2Dto(TipoSolicitacao entidade) {

		return new TipoSolicitacaoDTO(entidade);
	}

	@Override
	public List<TipoSolicitacaoDTO> listEntidade2ListDto(List<TipoSolicitacao> listaEntidade) {

		List<TipoSolicitacaoDTO> lista = new ArrayList<>();
		for (TipoSolicitacao cargo : listaEntidade) {
			lista.add(entidade2Dto(cargo));
		}
		return lista;
	}

	public static TipoSolicitacaoConverter getInstance() {

		return instance;
	}

}
