package br.com.vitae.bus.conv;

import java.util.ArrayList;
import java.util.List;

import br.com.arquitetura.servico.converter.Converter;
import br.com.arquitetura.util.DataUtil;
import br.com.arquitetura.util.IsNullUtil;
import br.com.vitae.bus.dto.BancoDTO;
import br.com.vitae.bus.dto.CargoDTO;
import br.com.vitae.bus.dto.ColaboradorDTO;
import br.com.vitae.bus.dto.EmpresaDTO;
import br.com.vitae.bus.dto.NucleoDTO;
import br.com.vitae.bus.dto.PessoaDTO;
import br.com.vitae.bus.entity.Cargo;
import br.com.vitae.bus.entity.Colaborador;
import br.com.vitae.bus.entity.Empresa;
import br.com.vitae.bus.entity.Nucleo;
import br.com.vitae.bus.entity.NumeroBanco;
import br.com.vitae.bus.entity.Pessoa;
import br.com.vitae.bus.enumerator.EnumEstadoCivil;
import br.com.vitae.bus.enumerator.EnumEstadosBrasileiros;
import br.com.vitae.bus.enumerator.EnumPessoa;
import br.com.vitae.bus.enumerator.EnumSexo;

public class ColaboradorConverter implements Converter<Long, ColaboradorDTO, Colaborador> {

	private static ColaboradorConverter instance;

	static {
		instance = new ColaboradorConverter();
	}

	private ColaboradorConverter() {
	}

	@Override
	public Colaborador dto2Entidade(ColaboradorDTO dto) {

		if (dto == null) {
			return null;
		}
		CargoDTO cargoDTO = dto.getCargo();
		NucleoDTO nucleoDTO = dto.getNucleo();
		EmpresaDTO empresaDTO = dto.getEmpresa();
		PessoaDTO pessoaDTO = dto.getPessoa();
		Pessoa pessoa = null;
		Cargo cargo = null;
		Nucleo nucleo = null;
		Empresa empresa = null;

		if (pessoaDTO != null) {
			
			BancoDTO bancoDTO = pessoaDTO.getBanco();
			NumeroBanco banco = null;
			
			if (bancoDTO != null && !IsNullUtil.isNullOrEmpty(bancoDTO.getId())) {
				
				banco = new NumeroBanco();
				banco.setId(bancoDTO.getId());
				banco.setNome(bancoDTO.getNome());
			}
			
			pessoa = new Pessoa(pessoaDTO.getId(), pessoaDTO.getNome(), DataUtil.strToLocalDate(pessoaDTO.getDataCadastro()), pessoaDTO.getNaturalidade(), pessoaDTO.getUfNaturalidade(), DataUtil.strToLocalDate(pessoaDTO.getDataNascimento()), EnumEstadoCivil.get(pessoaDTO.getEstadoCivil()), EnumSexo.get(pessoaDTO.getSexo()), pessoaDTO.getCpfCnpj(), pessoaDTO.getRgIe(), pessoaDTO.getEmissorRgIe(), DataUtil.strToLocalDate(pessoaDTO.getDataExpedicaoRgIe()), pessoaDTO.getEndereco(),
					pessoaDTO.getComplemento(), pessoaDTO.getNumeroEndereco(), pessoaDTO.getBairro(), pessoaDTO.getCep(), pessoaDTO.getCidade(), EnumEstadosBrasileiros.get(pessoaDTO.getUfCidade()), pessoaDTO.getTelefone(), pessoaDTO.getCelular(), pessoaDTO.getEmail(), null, pessoaDTO.getObservacao(), banco, pessoaDTO.getAgencia(), pessoaDTO.getContaCorrente(), pessoaDTO.getObservacoesBancaria(), pessoaDTO.getNumeroPIS(), pessoaDTO.getNumeroCTPS(), EnumPessoa.get(pessoaDTO.getTipoPessoa()));
		}

		if (cargoDTO != null) {
			cargo = new Cargo(cargoDTO.getId());
		}

		if (empresaDTO != null) {
			empresa = new Empresa(empresaDTO.getId());
		}

		if (nucleoDTO != null && nucleoDTO.getId()!= null && nucleoDTO.getId().longValue() != 0) {
			Empresa empresaN = null;
			nucleo = new Nucleo(nucleoDTO.getId());
			if (nucleoDTO.getIdEmpresa() != null) {
				empresaN = new Empresa(nucleoDTO.getIdEmpresa());
				nucleo.setEmpresa(empresaN);
			}
		}

		Colaborador colaborador = new Colaborador(dto.getId(), dto.getNomeArquivo(), dto.getTipoArquivo(), pessoa, cargo, nucleo, empresa, DataUtil.strToLocalDate(dto.getAdmissao()), DataUtil.strToLocalDate(dto.getDemissao()), dto.getAtivo() == null ? false : dto.getAtivo());
		return colaborador;
	}

	@Override
	public ColaboradorDTO entidade2Dto(Colaborador entidade) {

		return new ColaboradorDTO(entidade);
	}

	@Override
	public List<ColaboradorDTO> listEntidade2ListDto(List<Colaborador> listaEntidade) {

		List<ColaboradorDTO> lista = new ArrayList<>();
		for (Colaborador colaborador : listaEntidade) {
			lista.add(entidade2Dto(colaborador));
		}
		return lista;
	}

	public static ColaboradorConverter getInstance() {

		return instance;
	}

}
