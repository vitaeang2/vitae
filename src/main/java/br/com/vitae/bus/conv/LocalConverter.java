package br.com.vitae.bus.conv;

import java.util.ArrayList;
import java.util.List;

import br.com.arquitetura.servico.converter.Converter;
import br.com.vitae.bus.dto.LocalDTO;
import br.com.vitae.bus.entity.Local;

/**
 * 
 * <p>
 * <b>Title:</b> LocalConverter.java
 * </p>
 * 
 * <p>
 * <b>Description:</b>
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnlogia da Informacao
 * </p>
 * 
 * @author Rogério Ferreira - rogerio0308@homtail.com
 * 
 * @version 1.0.0
 */
public class LocalConverter implements Converter<Long, LocalDTO, Local> {

	private static LocalConverter instance;

	static {
		instance = new LocalConverter();
	}

	private LocalConverter() {
	}

	@Override
	public Local dto2Entidade(LocalDTO dto) {

		if (dto == null) {
			return null;
		}
		Local cargo = new Local(dto.getId(), dto.getNome(), dto.getAtivo() == null ? false : dto.getAtivo());
		return cargo;
	}

	@Override
	public LocalDTO entidade2Dto(Local entidade) {

		return new LocalDTO(entidade);
	}

	@Override
	public List<LocalDTO> listEntidade2ListDto(List<Local> listaEntidade) {

		List<LocalDTO> lista = new ArrayList<>();
		for (Local cargo : listaEntidade) {
			lista.add(entidade2Dto(cargo));
		}
		return lista;
	}

	public static LocalConverter getInstance() {

		return instance;
	}

}
