package br.com.vitae.bus.conv;

import java.util.ArrayList;
import java.util.List;

import br.com.arquitetura.servico.converter.Converter;
import br.com.arquitetura.util.DataUtil;
import br.com.vitae.bus.dto.CategoriaProdutoDTO;
import br.com.vitae.bus.dto.FabricanteDTO;
import br.com.vitae.bus.dto.FornecedorDTO;
import br.com.vitae.bus.dto.ProdutoDTO;
import br.com.vitae.bus.entity.CategoriaProduto;
import br.com.vitae.bus.entity.Fabricante;
import br.com.vitae.bus.entity.Fornecedor;
import br.com.vitae.bus.entity.Produto;
import br.com.vitae.bus.entity.ProdutoEstoque;
import br.com.vitae.bus.enumerator.EnumUnidadeMedida;

/**
 * 
 * <p>
 * <b>Title:</b> ProdutoConverter.java
 * </p>
 * 
 * <p>
 * <b>Description:</b>
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnlogia da Informacao
 * </p>
 * 
 * @author Sergio Filho - sergioadsf@gmail.com
 * 
 * @version 1.0.0
 */
public class ProdutoConverter implements Converter<Long, ProdutoDTO, Produto> {

	private static ProdutoConverter instance;

	static {

		instance = new ProdutoConverter();
	}

	private ProdutoConverter() {
	}

	@Override
	public Produto dto2Entidade(ProdutoDTO dto) {

		if (dto == null) {
			return null;
		}
		
		ProdutoEstoque produtoEstoque = null;
		
	/*	ProdutoEstoqueDTO produtoEstoqueDTO = dto.getProdutoEstoque();
		
		if (produtoEstoqueDTO != null) {

			produtoEstoque = new ProdutoEstoque(produtoEstoqueDTO.getId(), produtoEstoqueDTO.getQuantidade());
		}
*/
		Fornecedor fornecedor = null;
		FornecedorDTO fornecedorDTO = dto.getFornecedor();
		if (fornecedorDTO != null && fornecedorDTO.getId() > 0) {

			fornecedor = new Fornecedor(fornecedorDTO.getId());

		}

		Fabricante fabricante = null;
		FabricanteDTO fabricanteDTO = dto.getFabricante();
		if (fabricanteDTO != null) {
			fabricante = new Fabricante(fabricanteDTO.getId());
		}

		CategoriaProduto categoria = null;
		CategoriaProdutoDTO categoriaDTO = dto.getCategoria();
		if (categoriaDTO != null) {
			categoria = new CategoriaProduto(categoriaDTO.getId());
		}

		Produto produto = new Produto(dto.getId(), dto.getNome(), DataUtil.strToLocalDate(dto.getDataCadastro()), dto.getCodigo(), dto.getValorVenda(), dto.getValorCusto(), produtoEstoque, dto.getMinimoEmEstoque(), dto.getMaximoEmEstoque(), EnumUnidadeMedida.get(dto.getUnidadeMedida()), fornecedor, fabricante, categoria, dto.getAtivo() == null ? Boolean.FALSE : dto.getAtivo());
		return produto;
	}

	@Override
	public ProdutoDTO entidade2Dto(Produto entidade) {

		return new ProdutoDTO(entidade);
	}

	@Override
	public List<ProdutoDTO> listEntidade2ListDto(List<Produto> listaEntidade) {

		List<ProdutoDTO> lista = new ArrayList<>();
		for (Produto produto : listaEntidade) {
			lista.add(entidade2Dto(produto));
		}
		return lista;
	}

	public static ProdutoConverter getInstance() {

		return instance;
	}

}
