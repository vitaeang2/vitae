package br.com.vitae.bus.conv;

import java.util.ArrayList;
import java.util.List;

import br.com.arquitetura.servico.converter.Converter;
import br.com.vitae.bus.dto.NumeroBancoDTO;
import br.com.vitae.bus.entity.NumeroBanco;

/**
 * 
 * <p>
 * <b>Title:</b> NumeroBancoConverter.java
 * </p>
 * 
 * <p>
 * <b>Description:</b>
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnlogia da Informacao
 * </p>
 * 
 * @author Sergio Filho - sergioadsf@gmail.com
 * 
 * @version 1.0.0
 */
public class NumeroBancoConverter implements Converter<Long, NumeroBancoDTO, NumeroBanco> {

	private static NumeroBancoConverter instance;

	static {
		instance = new NumeroBancoConverter();
	}

	private NumeroBancoConverter() {
	}

	@Override
	public NumeroBanco dto2Entidade(NumeroBancoDTO dto) {

		if (dto == null) {
			return null;
		}
		NumeroBanco cargo = new NumeroBanco(dto.getId(), dto.getNumero(), dto.getNome());
		return cargo;
	}

	@Override
	public NumeroBancoDTO entidade2Dto(NumeroBanco entidade) {

		return new NumeroBancoDTO(entidade);
	}

	@Override
	public List<NumeroBancoDTO> listEntidade2ListDto(List<NumeroBanco> listaEntidade) {

		List<NumeroBancoDTO> lista = new ArrayList<>();
		for (NumeroBanco nb : listaEntidade) {
			lista.add(entidade2Dto(nb));
		}
		return lista;
	}

	public static NumeroBancoConverter getInstance() {

		return instance;
	}

}
