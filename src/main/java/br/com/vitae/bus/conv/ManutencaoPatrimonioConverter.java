package br.com.vitae.bus.conv;

import java.util.ArrayList;
import java.util.List;

import br.com.arquitetura.servico.converter.Converter;
import br.com.arquitetura.util.DataUtil;
import br.com.vitae.bus.dto.FornecedorDTO;
import br.com.vitae.bus.dto.ManutencaoPatrimonioDTO;
import br.com.vitae.bus.dto.PatrimonioDTO;
import br.com.vitae.bus.entity.Fornecedor;
import br.com.vitae.bus.entity.ManutencaoPatrimonio;
import br.com.vitae.bus.entity.Patrimonio;

/**
 * 
 * <p>
 * <b>Title:</b> ManutencaoPatrimonioConverter.java
 * </p>
 * 
 * <p>
 * <b>Description:</b>
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnlogia da Informacao
 * </p>
 * 
 * @author Rogério Ferreira - rogerio0308@homtail.com
 * 
 * @version 1.0.0
 */
public class ManutencaoPatrimonioConverter implements Converter<Long, ManutencaoPatrimonioDTO, ManutencaoPatrimonio> {

	private static ManutencaoPatrimonioConverter instance;

	static {

		instance = new ManutencaoPatrimonioConverter();
	}

	private ManutencaoPatrimonioConverter() {
	}

	@Override
	public ManutencaoPatrimonio dto2Entidade(ManutencaoPatrimonioDTO dto) {

		if (dto == null) {
			return null;
		}

		Fornecedor fornecedor = null;
		FornecedorDTO fornecedorDTO = dto.getFornecedor();
		if (fornecedorDTO != null && fornecedorDTO.getId() > 0) {

			fornecedor = new Fornecedor(fornecedorDTO.getId());

		}

		Patrimonio patrimonio = null;
		PatrimonioDTO patrimonioDTO = dto.getPatrimonio();
		if (patrimonioDTO != null) {
			patrimonio = new Patrimonio(patrimonioDTO.getId());
		}

		ManutencaoPatrimonio manutencaoPatrimonio = new ManutencaoPatrimonio(dto.getId(), dto.getMotivo(),
				dto.getDescricaoRetorno(), fornecedor, patrimonio, DataUtil.strToLocalDate(dto.getDataSaida()),
				DataUtil.strToLocalDate(dto.getDataRetorno()), DataUtil.strToLocalDate(dto.getDataPrevistaRetorno()),
				DataUtil.strToLocalDate(dto.getFimGarantia()), dto.getValor(), dto.getValorPago(),
				dto.getAtivo() == null ? Boolean.FALSE : dto.getAtivo());
		return manutencaoPatrimonio;
	}

	@Override
	public ManutencaoPatrimonioDTO entidade2Dto(ManutencaoPatrimonio entidade) {

		return new ManutencaoPatrimonioDTO(entidade);
	}

	@Override
	public List<ManutencaoPatrimonioDTO> listEntidade2ListDto(List<ManutencaoPatrimonio> listaEntidade) {

		List<ManutencaoPatrimonioDTO> lista = new ArrayList<>();
		for (ManutencaoPatrimonio manutencaoPatrimonio : listaEntidade) {
			lista.add(entidade2Dto(manutencaoPatrimonio));
		}
		return lista;
	}

	public static ManutencaoPatrimonioConverter getInstance() {

		return instance;
	}

}
