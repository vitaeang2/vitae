package br.com.vitae.bus.conv;

import java.util.ArrayList;
import java.util.List;

import br.com.arquitetura.servico.converter.Converter;
import br.com.arquitetura.util.DataUtil;
import br.com.vitae.bus.dto.EmpresaDTO;
import br.com.vitae.bus.dto.NucleoDTO;
import br.com.vitae.bus.dto.QualidadeDTO;
import br.com.vitae.bus.dto.UsuarioDTO;
import br.com.vitae.bus.entity.Empresa;
import br.com.vitae.bus.entity.Nucleo;
import br.com.vitae.bus.entity.Qualidade;
import br.com.vitae.bus.entity.Usuario;
import br.com.vitae.bus.enumerator.EnumAuditoria;
import br.com.vitae.bus.enumerator.EnumOrigem;
import br.com.vitae.bus.enumerator.EnumRegistro;

public class QualidadeConverter implements Converter<Long, QualidadeDTO, Qualidade> {

	private static QualidadeConverter instance;

	static {
		instance = new QualidadeConverter();
	}

	private QualidadeConverter() {
	}

	@Override
	public Qualidade dto2Entidade(QualidadeDTO dto) {

		if (dto == null) {
			return null;
		}
		NucleoDTO nucleoDTO = dto.getNucleo();
		Nucleo nucleo = null;
		EmpresaDTO empresaDTO = dto.getEmpresa();
		Empresa empresa = null;
		UsuarioDTO usuarioDTO = dto.getAutor();
		Usuario autor = null;

		if (usuarioDTO != null && usuarioDTO.getId() != null && usuarioDTO.getId().longValue() != 0) {
			autor = new Usuario(usuarioDTO.getId());
		}

		if (empresaDTO != null && empresaDTO.getId() != null && empresaDTO.getId().longValue() != 0) {
			empresa = new Empresa(empresaDTO.getId());
		}

		if (nucleoDTO != null && nucleoDTO.getId() != null && nucleoDTO.getId().longValue() != 0) {
			Empresa empresaN = null;
			nucleo = new Nucleo(nucleoDTO.getId());
			if (nucleoDTO.getIdEmpresa() != null) {
				empresaN = new Empresa(nucleoDTO.getIdEmpresa());
				nucleo.setEmpresa(empresaN);
			}
		}

		Qualidade qualidade = new Qualidade(dto.getId(), dto.getAtivo() == null ? false : dto.getAtivo(),
				EnumRegistro.get(dto.getRegistro()), EnumOrigem.get(dto.getOrigem()),
				EnumAuditoria.get(dto.getAuditoria()), DataUtil.strToLocalDate(dto.getCriacao()), autor,
				dto.getIdentificacao(), dto.getAnalise_causa(), dto.getAcao_imediata(), dto.getAcao_preventiva(),
				dto.getAvaliacao_eficacia(), empresa, nucleo);
		return qualidade;
	}

	@Override
	public QualidadeDTO entidade2Dto(Qualidade entidade) {

		return new QualidadeDTO(entidade);
	}

	@Override
	public List<QualidadeDTO> listEntidade2ListDto(List<Qualidade> listaEntidade) {

		List<QualidadeDTO> lista = new ArrayList<>();
		for (Qualidade qualidade : listaEntidade) {
			lista.add(entidade2Dto(qualidade));
		}
		return lista;
	}

	public static QualidadeConverter getInstance() {

		return instance;
	}

}
