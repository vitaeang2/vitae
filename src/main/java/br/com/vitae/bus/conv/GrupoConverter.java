package br.com.vitae.bus.conv;

import java.util.ArrayList;
import java.util.List;

import br.com.arquitetura.servico.converter.Converter;
import br.com.vitae.bus.dto.GrupoDTO;
import br.com.vitae.bus.entity.Grupo;

/**
 * 
 * <p>
 * <b>Title:</b> GrupoConverter.java
 * </p>
 * 
 * <p>
 * <b>Description:</b>
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnlogia da Informacao
 * </p>
 * 
 * @author Rogério Ferreira - rogerio0308@homtail.com
 * 
 * @version 1.0.0
 */
public class GrupoConverter implements Converter<Long, GrupoDTO, Grupo> {

	private static GrupoConverter instance;

	static {
		instance = new GrupoConverter();
	}

	private GrupoConverter() {
	}

	@Override
	public Grupo dto2Entidade(GrupoDTO dto) {

		if (dto == null) {
			return null;
		}
		Grupo cargo = new Grupo(dto.getId(), dto.getNome(), dto.getAtivo() == null ? false : dto.getAtivo());
		return cargo;
	}

	@Override
	public GrupoDTO entidade2Dto(Grupo entidade) {

		return new GrupoDTO(entidade);
	}

	@Override
	public List<GrupoDTO> listEntidade2ListDto(List<Grupo> listaEntidade) {

		List<GrupoDTO> lista = new ArrayList<>();
		for (Grupo cargo : listaEntidade) {
			lista.add(entidade2Dto(cargo));
		}
		return lista;
	}

	public static GrupoConverter getInstance() {

		return instance;
	}

}
