package br.com.vitae.bus.conv;

import java.util.ArrayList;
import java.util.List;

import br.com.arquitetura.servico.converter.Converter;
import br.com.vitae.bus.dto.CategoriaGrupoDTO;
import br.com.vitae.bus.dto.GrupoDTO;
import br.com.vitae.bus.entity.CategoriaGrupo;
import br.com.vitae.bus.entity.Grupo;

/**
 * 
 * <p>
 * <b>Title:</b> CategoriaGrupoConverter.java
 * </p>
 * 
 * <p>
 * <b>Description:</b>
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnlogia da Informacao
 * </p>
 * 
 * @author Rogério Ferreira - rogerio0308@hotmail.com
 * 
 * @version 1.0.0
 */
public class CategoriaGrupoConverter implements Converter<Long, CategoriaGrupoDTO, CategoriaGrupo> {

	private static CategoriaGrupoConverter instance;

	static {
		instance = new CategoriaGrupoConverter();
	}

	private CategoriaGrupoConverter() {
	}

	@Override
	public CategoriaGrupo dto2Entidade(CategoriaGrupoDTO dto) {

		if (dto == null) {
			return null;
		}
		Grupo grupo = null;
		GrupoDTO grupoDTO = dto.getGrupo();
		if (grupoDTO != null) {
			grupo = new Grupo(grupoDTO.getId());
		}
		
		CategoriaGrupo categoriaGrupo = new CategoriaGrupo(dto.getId(), dto.getNome(), grupo, dto.getAtivo() == null ? false : dto.getAtivo());
		return categoriaGrupo;
	}

	@Override
	public CategoriaGrupoDTO entidade2Dto(CategoriaGrupo entidade) {

		return new CategoriaGrupoDTO(entidade);
	}

	@Override
	public List<CategoriaGrupoDTO> listEntidade2ListDto(List<CategoriaGrupo> listaEntidade) {

		List<CategoriaGrupoDTO> lista = new ArrayList<>();
		for (CategoriaGrupo cargo : listaEntidade) {
			lista.add(entidade2Dto(cargo));
		}
		return lista;
	}

	public static CategoriaGrupoConverter getInstance() {

		return instance;
	}

}
