package br.com.vitae.bus.conv;

import java.util.ArrayList;
import java.util.List;

import br.com.arquitetura.servico.converter.Converter;
import br.com.vitae.bus.dto.DepartamentoDTO;
import br.com.vitae.bus.entity.Departamento;

/**
 * 
 * <p>
 * <b>Title:</b> DepartamentoConverter.java
 * </p>
 * 
 * <p>
 * <b>Description:</b>
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnlogia da Informacao
 * </p>
 * 
 * @author Rogério Ferreira - rogerio0308@homtail.com
 * 
 * @version 1.0.0
 */
public class DepartamentoConverter implements Converter<Long, DepartamentoDTO, Departamento> {

	private static DepartamentoConverter instance;

	static {
		instance = new DepartamentoConverter();
	}

	private DepartamentoConverter() {
	}

	@Override
	public Departamento dto2Entidade(DepartamentoDTO dto) {

		if (dto == null) {
			return null;
		}
		Departamento cargo = new Departamento(dto.getId(), dto.getNome(), dto.getAtivo() == null ? false : dto.getAtivo());
		return cargo;
	}

	@Override
	public DepartamentoDTO entidade2Dto(Departamento entidade) {

		return new DepartamentoDTO(entidade);
	}

	@Override
	public List<DepartamentoDTO> listEntidade2ListDto(List<Departamento> listaEntidade) {

		List<DepartamentoDTO> lista = new ArrayList<>();
		for (Departamento cargo : listaEntidade) {
			lista.add(entidade2Dto(cargo));
		}
		return lista;
	}

	public static DepartamentoConverter getInstance() {

		return instance;
	}

}
