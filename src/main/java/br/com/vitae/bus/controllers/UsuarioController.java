package br.com.vitae.bus.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.arquitetura.model.Response;
import br.com.arquitetura.vo.PaginacaoBuilder;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.arquitetura.web.controller.BaseController;
import br.com.vitae.bus.conv.UsuarioConverter;
import br.com.vitae.bus.dto.UsuarioDTO;
import br.com.vitae.bus.entity.Usuario;
import br.com.vitae.bus.service.IUsuarioServico;
import br.com.vitae.jwt.JWT;

@Controller
@RequestMapping("/usuario")
public class UsuarioController extends BaseController<Usuario> {
	
	@Autowired
	private IUsuarioServico iUsuarioServico;

	@RequestMapping(path = "/logar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response login(String login, String senha) {
		

		try {
			UsuarioDTO usuario = iUsuarioServico.findPorLoginSenha(login, senha);

			/*
			 * mailSenderService.setTemplateName("esqueciSenha.vm"); HashMap<String, Object> map = new HashMap<>(); map.put("senhaProvisoria", "1211534165Teste"); mailSenderService.sendHtmlWithTemplate("sergioadsf@gmail.com", "Teste", map);
			 */
			usuario.setToken(criarToken(usuario));
			return enviarSucesso(usuario);
		} catch (Exception e) {
			return enviarFalha("Usuário e/ou senha não existe(m)!");
		}
	}

	private String criarToken(UsuarioDTO usuario) {

		return JWT.createJWT(usuario.getId(), usuario.getNome(), usuario.getColaborador().getPessoa().getEmail());
	}

	@RequestMapping(path = "/salvar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response criar(String login, String senha, String nome) {

		try {
			Usuario usuario = new Usuario();
//			usuario.setLogin(login);
			usuario.setSenha(senha);
			return enviarSucesso((Usuario) iUsuarioServico.salvarOuAtualizar(usuario));
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/alterarSenha", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response alterarSenha(Long id, String novaSenha) {

		try {
			// TODO - Retornar o DTO
			iUsuarioServico.alterarSenha(id, novaSenha);
			return enviarSucesso();
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/listar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listar(String token, PaginacaoParams params) {

		try {
			return enviarSucesso(PaginacaoBuilder.create(UsuarioConverter.getInstance(), Usuario.class, params).search());
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/consultarPorColaborador", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response consultarPorColaborador(Long idColaborador) {

		try {
			return enviarSucesso(iUsuarioServico.findPorColaboradorDTO(idColaborador));
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/esqueceuSenha", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response esqueceuSenha(String email) {

		try {
			return enviarSucesso(iUsuarioServico.esqueceuSenha(email) ? "Solicita��o enviada com sucesso!" : "Solicita��o n�o pode ser enviada - Email n�o cadastrado!");
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}
	

	@RequestMapping(path = "/listarCombo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listarCombo() {

		try {
			return enviarSucesso(iUsuarioServico.listarCombo());
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}
}
