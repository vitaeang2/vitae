package br.com.vitae.bus.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.arquitetura.model.Response;
import br.com.arquitetura.web.controller.BaseController;
import br.com.vitae.bus.dto.ProdutoDTO;
import br.com.vitae.bus.service.ICotacaoServico;
import br.com.vitae.bus.service.IProdutoEstoqueServico;
import br.com.vitae.bus.service.ISolicitacaoServico;

@Controller
@RequestMapping("/dashboard")
public class DashboardController extends BaseController<ProdutoDTO> {

	@Autowired
	private IProdutoEstoqueServico iProdutoEstoqueServico;
	
	@Autowired
	private ICotacaoServico iCotacaoServico;

	@Autowired
	private ISolicitacaoServico iSolicitacaoServico;

	@RequestMapping(path = "/quantidadeProdutoBaixoEstoque", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response quantidadeProdutoBaixoEstoque(Long idEmpresa, Long idNucleo) {

		try {
			return enviarSucesso(iProdutoEstoqueServico.quantidadeProdutoBaixoEstoque(idEmpresa, idNucleo));
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}
	
	@RequestMapping(path = "/quantidadeCotacoesRespondidas", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response quantidadeCotacoesRespondidas(Long idEmpresa, Long idNucleo) {
		
		try {
			return enviarSucesso(iCotacaoServico.quantidadeCotacoesRespondidas(idEmpresa, idNucleo));
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/quantidadeSolicitacoesAbertas", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response quantidadeSolicitacoesAbertas(Long idEmpresa, Long idNucleo) {
		
		try {
			return enviarSucesso(iSolicitacaoServico.quantidadeSolicitacoesAbertas(idEmpresa, idNucleo));
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}
}
