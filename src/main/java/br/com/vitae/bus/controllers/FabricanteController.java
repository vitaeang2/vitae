package br.com.vitae.bus.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.arquitetura.model.Response;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.arquitetura.web.controller.BaseController;
import br.com.vitae.bus.dto.FabricanteDTO;
import br.com.vitae.bus.service.IFabricanteServico;

@Controller
@RequestMapping("/fabricante")
public class FabricanteController extends BaseController<FabricanteDTO> {

	@Autowired
	private IFabricanteServico fabricanteServico;

	@RequestMapping(path = "/listarCombo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listarCombo() {

		try {
			return enviarSucesso(fabricanteServico.listarCombo());
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}


	@RequestMapping(path = "/listar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listar(String params) {

		try {
			PaginacaoParams pp = retornar(PaginacaoParams.class, params);
			return enviarSucesso(fabricanteServico.listarPaginado(pp));
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/consultarId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response consultarId(Long id) {

		try {
			FabricanteDTO fabricanteDTO = fabricanteServico.consultarIdDTO(id);
			return enviarSucesso(fabricanteDTO);
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/salvar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response salvar(@RequestBody String fabricante) {

		try {
			FabricanteDTO fabricanteDTO = retornar(FabricanteDTO.class, fabricante);
			fabricanteServico.salvarOuAtualizar(fabricanteDTO);
			return enviarSucesso();
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}
	
	@RequestMapping(path = "/excluirId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response excluir(Long id, boolean verificarFabricanteViculadaProduto) {

		try {
			if (verificarFabricanteViculadaProduto) {
				FabricanteDTO fabricanteDTO = fabricanteServico
						.consultarFabricanteSemViculoProduto(id);
				if (fabricanteDTO != null) {
					fabricanteDTO.setAtivo(Boolean.FALSE);
					fabricanteServico.salvarOuAtualizar(fabricanteDTO);
				} else {
					return enviarFalha(
							"Fabricante de Produto está vinculado a algum Produto.");
				}
			} else {
				FabricanteDTO fabricanteDTO = fabricanteServico.consultarIdDTO(id);
				fabricanteDTO.setAtivo(Boolean.FALSE);
				fabricanteServico.salvarOuAtualizar(fabricanteDTO);
			}
			return enviarSucesso();

		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

}
