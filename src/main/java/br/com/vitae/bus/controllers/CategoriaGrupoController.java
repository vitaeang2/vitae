package br.com.vitae.bus.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.arquitetura.model.Response;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.arquitetura.web.controller.BaseController;
import br.com.vitae.bus.dto.CategoriaGrupoDTO;
import br.com.vitae.bus.service.ICategoriaGrupoServico;

@Controller
@RequestMapping("/categoria_grupo")
public class CategoriaGrupoController extends BaseController<CategoriaGrupoDTO> {

	@Autowired
	private ICategoriaGrupoServico categoriaGrupoServico;

	@RequestMapping(path = "/listarCombo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listarCombo() {

		try {
			return enviarSucesso(categoriaGrupoServico.listarCombo());
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/listar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listar(String params) {

		try {
			PaginacaoParams pp = retornar(PaginacaoParams.class, params);
			return enviarSucesso(categoriaGrupoServico.listarPaginado(pp));
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/consultarId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response consultarId(Long id) {

		try {
			CategoriaGrupoDTO categoriaGrupoDTO = categoriaGrupoServico.consultarIdDTO(id);
			return enviarSucesso(categoriaGrupoDTO);
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/salvar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response salvar(@RequestBody String categoria) {

		try {
			CategoriaGrupoDTO categoriaGrupoDTO = retornar(CategoriaGrupoDTO.class, categoria);
			categoriaGrupoServico.salvarOuAtualizar(categoriaGrupoDTO);
			return enviarSucesso();
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/excluirId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response excluir(Long id) {

		try {
			CategoriaGrupoDTO categoriaGrupoDTO = categoriaGrupoServico.consultarIdDTO(id);
			categoriaGrupoDTO.setAtivo(Boolean.FALSE);
			categoriaGrupoServico.salvarOuAtualizar(categoriaGrupoDTO);

			return enviarSucesso();

		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

}
