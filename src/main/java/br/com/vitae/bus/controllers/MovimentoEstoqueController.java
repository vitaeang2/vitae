package br.com.vitae.bus.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.exception.NegocioException;
import br.com.arquitetura.model.Response;
import br.com.arquitetura.web.controller.BaseController;
import br.com.vitae.bus.dto.ItemPedidoCompraDTO;
import br.com.vitae.bus.dto.MovimentoEstoqueDTO;
import br.com.vitae.bus.dto.ProdutoDTO;
import br.com.vitae.bus.service.IMovimentoEstoqueServico;
import br.com.vitae.bus.service.IProdutoEstoqueServico;

@Controller
@RequestMapping("/movimentoEstoque")
public class MovimentoEstoqueController extends BaseController<ProdutoDTO> {

	@Autowired
	private IMovimentoEstoqueServico movimentoEstoqueServico;

	@Autowired
	private IProdutoEstoqueServico produtoEstoqueServico;

	@RequestMapping(path = "/listarMovimentos", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listarMovimentos(Long produtoId, Long empresaId, Long nucleoId) {

		try {
			List<MovimentoEstoqueDTO> list = movimentoEstoqueServico.getMovimentosEstoque(produtoId, nucleoId, empresaId);
			return enviarSucesso(list);

		} catch (Exception e) {

			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/salvar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response salvar(@RequestBody String movimento) {

		try {
			MovimentoEstoqueDTO movimentoEstoqueDTO = retornar(MovimentoEstoqueDTO.class, movimento);
			movimentoEstoqueServico.salvarOuAtualizar(movimentoEstoqueDTO);
			return enviarSucesso();
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/consultarProdutoEstoque", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response consultarProdutoEstoque(Long produtoId, Long empresaId, Long nucleoId) {

		try {
			return enviarSucesso(produtoEstoqueServico.consultarProdutoEstoqueDTO(produtoId, empresaId, nucleoId));
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}
	
//	@RequestMapping(path = "/entrarEstoque", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
//	@ResponseBody
//	public Response entrarEstoque(@RequestBody String pedidoCompra) {
//
//		try {
//			PedidoCompraDTO pedidoCompraDTO = retornar(PedidoCompraDTO.class, pedidoCompra);
//			List<ItemPedidoCompraDTO> listaPedidoCompraDTO = pedidoCompraDTO.getItensPedidoCompra();
////			List<ItemPedidoCompraDTO> listaPedidoCompraDTO = retornar(new String(), new TypeToken<List<ItemPedidoCompraDTO>>(){});
//			
//			movimentoEstoqueServico.entrarEstoque(pedidoCompraDTO, listaPedidoCompraDTO);
//			return enviarSucesso();
//		} catch (Exception e) {
//
//			if (e instanceof NegocioException) {
//
//				return enviarFalha(e.getMessage());
//			}
//
//			if (e instanceof DaoException) {
//
//				e.printStackTrace();
//				return enviarFalha(e.getMessage());
//			}
//		}
//		
//		return enviarFalha("Houve um erro ao salvar o Pedido de Compra.");
//	}

	@RequestMapping(path = "/entrarEstoque", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response entrarEstoque(Long idPedidoCompra, @RequestBody String itemPedidoCompra) {
		
		try {
//			PedidoCompraDTO pedidoCompraDTO = retornar(PedidoCompraDTO.class, pedidoCompra);
			ItemPedidoCompraDTO itemPedidoCompraDTO = retornar(ItemPedidoCompraDTO.class, itemPedidoCompra);
//			List<ItemPedidoCompraDTO> listaPedidoCompraDTO = pedidoCompraDTO.getItensPedidoCompra();
//			List<ItemPedidoCompraDTO> listaPedidoCompraDTO = retornar(new String(), new TypeToken<List<ItemPedidoCompraDTO>>(){});
			
			movimentoEstoqueServico.entrarEstoque(idPedidoCompra, itemPedidoCompraDTO);
			return enviarSucesso();
		} catch (Exception e) {
			
			if (e instanceof NegocioException) {
				
				return enviarFalha(e.getMessage());
			}
			
			if (e instanceof DaoException) {
				
				e.printStackTrace();
				return enviarFalha(e.getMessage());
			}
		}
		
		return enviarFalha("Houve um erro ao salvar o Pedido de Compra.");
	}


}
