package br.com.vitae.bus.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.arquitetura.model.Response;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.arquitetura.web.controller.BaseController;
import br.com.vitae.bus.dto.FornecedorDTO;
import br.com.vitae.bus.service.IFornecedorServico;
import br.com.vitae.bus.service.IPessoaServico;

@Controller
@RequestMapping("/fornecedor")
public class FornecedorController extends BaseController<FornecedorDTO> {

	@Autowired
	private IFornecedorServico fornecedorServico;

	@Autowired
	private IPessoaServico pessoaServico;

	@RequestMapping(path = "/listarComboPrestadorServico", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listarComboPrestadorServico() {

		try {
			return enviarSucesso(fornecedorServico.listarComboPrestadorServico());
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}
	
	@RequestMapping(path = "/listarCombo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listarCombo() {
		
		try {
			return enviarSucesso(fornecedorServico.listarCombo());
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}
	
	@RequestMapping(path = "/listarFiltroCotacao", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listarFiltroCotacao(String valor) {
		
		try {
			return enviarSucesso(fornecedorServico.listarFiltroCotacao(valor));
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/listar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listar(String params) {

		try {
			PaginacaoParams pp = retornar(PaginacaoParams.class, params);
			return enviarSucesso(fornecedorServico.listarPaginado(pp));
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/consultarId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response consultarId(Long id) {

		try {
			FornecedorDTO fornecedorDTO = fornecedorServico.consultarIdDTO(id);
			return enviarSucesso(fornecedorDTO);
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/salvar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response salvar(@RequestBody String fornecedor) {

		try {
			FornecedorDTO fornecedorDTO = retornar(FornecedorDTO.class, fornecedor);
			fornecedorServico.salvarOuAtualizar(fornecedorDTO);
			return enviarSucesso();
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}
	
	@RequestMapping(path = "/atualizarPessoa", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response atualizarPessoa(Long idPessoa, String email) {

		try {
			pessoaServico.atualizarEmail(idPessoa, email);
			return enviarSucesso();
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}
	
	
	
	@RequestMapping(path = "/excluirId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response excluir(Long id) {

		try {

			FornecedorDTO fornecedorDTO = fornecedorServico.consultarIdDTO(id);
			fornecedorDTO.setAtivo(Boolean.FALSE);
			fornecedorServico.salvarOuAtualizar(fornecedorDTO);

			return enviarSucesso();

		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}


}
