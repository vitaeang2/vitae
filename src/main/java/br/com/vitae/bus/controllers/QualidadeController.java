package br.com.vitae.bus.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.exception.NegocioException;
import br.com.arquitetura.model.Response;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.arquitetura.web.controller.BaseController;
import br.com.vitae.bus.dto.QualidadeDTO;
import br.com.vitae.bus.service.IQualidadeServico;

@Controller
@RequestMapping("/qualidade")
public class QualidadeController extends BaseController<QualidadeDTO> {

	@Autowired
	private IQualidadeServico qualidadeServico;
	
	@Autowired
	private HttpServletRequest request;

	@RequestMapping(path = "/listarCombo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listarCombo() {

		try {
			return enviarSucesso(qualidadeServico.listarCombo());
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/listar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listar(String params) {

		try {
			PaginacaoParams pp = retornar(PaginacaoParams.class, params);
			return enviarSucesso(qualidadeServico.listarPaginado(pp));
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/consultarId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response consultarId(Long id) {

		try {
			QualidadeDTO qualidadeDTO = qualidadeServico.consultarIdDTO(id);
			return enviarSucesso(qualidadeDTO);
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/salvar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response salvar(@RequestBody String qualidade) {

		try {
			QualidadeDTO qualidadeDTO = retornar(QualidadeDTO.class, qualidade);
			return enviarSucesso(qualidadeServico.salvarOuAtualizar(qualidadeDTO));
		} catch (Exception e) {

			if (e instanceof NegocioException) {

				return enviarFalha(e.getMessage());
			}

			if (e instanceof DaoException) {

				e.printStackTrace();
				return enviarFalha(e.getMessage());
			}
		}
		
		return enviarFalha("Houve um erro ao salvar o qualidade.");
	}


	@RequestMapping(path = "/excluirId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response excluir(Long id) {

		try {

			qualidadeServico.excluirId(id);

			return enviarSucesso();

		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}
}
