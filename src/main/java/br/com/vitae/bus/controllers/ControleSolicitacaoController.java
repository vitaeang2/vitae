package br.com.vitae.bus.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.exception.NegocioException;
import br.com.arquitetura.model.Response;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.arquitetura.web.controller.BaseController;
import br.com.vitae.bus.dto.SolicitacaoDTO;
import br.com.vitae.bus.service.ISolicitacaoServico;

@Controller
@RequestMapping("/controle_solicitacao")
public class ControleSolicitacaoController extends BaseController<SolicitacaoDTO> {

	@Autowired
	private ISolicitacaoServico solicitacaoServico;
	
	@Autowired
	private HttpServletRequest request;

	@RequestMapping(path = "/listarCombo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listarCombo() {

		try {
			return enviarSucesso(solicitacaoServico.listarCombo());
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/listar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listar(String params, Long idNucleo, Long idLocal, Long idStatusSolicitacao) {

		try {
			PaginacaoParams pp = retornar(PaginacaoParams.class, params);
			return enviarSucesso(solicitacaoServico.listarPaginado(pp, idNucleo, idLocal, idStatusSolicitacao));
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/consultarId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response consultarId(Long id) {

		try {
			SolicitacaoDTO solicitacaoDTO = solicitacaoServico.consultarIdDTO(id);
			return enviarSucesso(solicitacaoDTO);
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/salvar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response salvar(@RequestBody String solicitacao) {

		try {
			SolicitacaoDTO solicitacaoDTO = retornar(SolicitacaoDTO.class, solicitacao);
			return enviarSucesso(solicitacaoServico.salvarOuAtualizar(solicitacaoDTO));
		} catch (Exception e) {

			if (e instanceof NegocioException) {

				return enviarFalha(e.getMessage());
			}

			if (e instanceof DaoException) {

				e.printStackTrace();
				return enviarFalha(e.getMessage());
			}
		}
		
		return enviarFalha("Houve um erro ao salvar o solicitacao.");
	}


	@RequestMapping(path = "/excluirId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response excluir(Long id) {

		try {

			solicitacaoServico.excluirId(id);

			return enviarSucesso();

		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}
}
