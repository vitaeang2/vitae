package br.com.vitae.bus.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.arquitetura.model.Response;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.arquitetura.web.controller.BaseController;
import br.com.vitae.bus.dto.TipoSolicitacaoDTO;
import br.com.vitae.bus.service.ITipoSolicitacaoServico;

@Controller
@RequestMapping("/tipoSolicitacao")
public class TipoSolicitacaoController extends BaseController<TipoSolicitacaoDTO> {

	@Autowired
	private ITipoSolicitacaoServico tipoSolicitacaoServico;

	@RequestMapping(path = "/listarCombo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listarCombo() {

		try {
			return enviarSucesso(tipoSolicitacaoServico.listarCombo());
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/listar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listar(String params) {

		try {
			PaginacaoParams pp = retornar(PaginacaoParams.class, params);
			return enviarSucesso(tipoSolicitacaoServico.listarPaginado(pp));
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/consultarId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response consultarId(Long id) {

		try {
			TipoSolicitacaoDTO tipoSolicitacaoDTO = tipoSolicitacaoServico.consultarIdDTO(id);
			return enviarSucesso(tipoSolicitacaoDTO);
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/salvar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response salvar(@RequestBody String tipoSolicitacao) {

		try {
			TipoSolicitacaoDTO tipoSolicitacaoDTO = retornar(TipoSolicitacaoDTO.class, tipoSolicitacao);
			tipoSolicitacaoServico.salvarOuAtualizar(tipoSolicitacaoDTO);
			return enviarSucesso();
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/excluirId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response excluir(Long id, boolean verificarTipoSolicitacaoViculadaSolicitacao) {

		try {
			if (verificarTipoSolicitacaoViculadaSolicitacao) {
				TipoSolicitacaoDTO tipoSolicitacaoDTO = tipoSolicitacaoServico
						.consultarTipoSolicitacaoSemViculoSolicitacao(id);
				if (tipoSolicitacaoDTO != null) {
					tipoSolicitacaoDTO.setAtivo(Boolean.FALSE);
					tipoSolicitacaoServico.salvarOuAtualizar(tipoSolicitacaoDTO);
				} else {
					return enviarFalha(
							"TipoSolicitacao de Solicitacao está vinculada a algum Solicitacao.");
				}
			} else {
				TipoSolicitacaoDTO tipoSolicitacaoDTO = tipoSolicitacaoServico.consultarIdDTO(id);
				tipoSolicitacaoDTO.setAtivo(Boolean.FALSE);
				tipoSolicitacaoServico.salvarOuAtualizar(tipoSolicitacaoDTO);
			}
			return enviarSucesso();

		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

}
