package br.com.vitae.bus.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.reflect.TypeToken;

import br.com.arquitetura.model.Response;
import br.com.arquitetura.web.controller.BaseController;
import br.com.vitae.bus.dto.PaginaPermissaoDTO;
import br.com.vitae.bus.dto.PerfilUsuarioDTO;
import br.com.vitae.bus.entity.PerfilUsuario;
import br.com.vitae.bus.service.IPerfilAcessoServico;

@Controller
@RequestMapping("/perfilAcesso")
public class PerfilAcessoController extends BaseController<PerfilUsuarioDTO> {

	@Autowired
	private IPerfilAcessoServico perfilAcessoServico;

	@RequestMapping(path = "/listarPerfis", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listarPerfis() {
		try {
			return enviarSucesso(perfilAcessoServico.listarPerfis());
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/listarPaginaSelecionadas", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listarPaginaSelecionadas(Long idPerfil) {
		try {
			return enviarSucesso(perfilAcessoServico.listarPaginaSelecionadas(idPerfil));
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/salvar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response salvar(@RequestBody String nome) {
		try {
			perfilAcessoServico.salvarOuAtualizar(new PerfilUsuario(nome.toUpperCase()));
			return enviarSucesso();
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/salvarSelecionados", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response salvarSelecionados(Long idPerfil, String lista) {
		try {
			List<PaginaPermissaoDTO> listaPermissao = retornar(lista, new TypeToken<List<PaginaPermissaoDTO>>(){}); 
			perfilAcessoServico.salvarPermissoes(idPerfil, listaPermissao);
			return enviarSucesso();
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

}