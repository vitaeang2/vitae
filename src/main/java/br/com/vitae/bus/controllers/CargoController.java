package br.com.vitae.bus.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.arquitetura.model.Response;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.arquitetura.web.controller.BaseController;
import br.com.vitae.bus.dto.CargoDTO;
import br.com.vitae.bus.service.ICargoServico;

@Controller
@RequestMapping("/cargo")
public class CargoController extends BaseController<CargoDTO> {

	@Autowired
	private ICargoServico cargoServico;

	@RequestMapping(path = "/listarCombo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listarCombo() {

		try {
			return enviarSucesso(cargoServico.listarCombo());
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/listar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listar(String params) {

		try {
			PaginacaoParams pp = retornar(PaginacaoParams.class, params);
			return enviarSucesso(cargoServico.listarPaginado(pp));
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/consultarId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response consultarId(Long id) {

		try {
			CargoDTO cargoDTO = cargoServico.consultarIdDTO(id);
			return enviarSucesso(cargoDTO);
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/salvar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response salvar(@RequestBody String cargo) {

		try {
			CargoDTO cargoDTO = retornar(CargoDTO.class, cargo);
			cargoServico.salvarOuAtualizar(cargoDTO);
			return enviarSucesso();
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}
	
	
	@RequestMapping(path = "/excluirId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response excluir(Long id) {

		try {

			CargoDTO cargoDTO = cargoServico.consultarIdDTO(id);
			cargoDTO.setAtivo(Boolean.FALSE);
			cargoServico.salvarOuAtualizar(cargoDTO);

			return enviarSucesso();

		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}


}
