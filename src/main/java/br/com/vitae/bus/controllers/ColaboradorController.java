package br.com.vitae.bus.controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.exception.NegocioException;
import br.com.arquitetura.model.Response;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.arquitetura.web.controller.BaseController;
import br.com.vitae.bus.dto.ColaboradorDTO;
import br.com.vitae.bus.dto.DocumentoDTO;
import br.com.vitae.bus.service.IColaboradorServico;
import br.com.vitae.bus.service.IDocumentoServico;

@Controller
@RequestMapping("/colaborador")
public class ColaboradorController extends BaseController<ColaboradorDTO> {

	@Autowired
	private IColaboradorServico colaboradorServico;

	@Autowired
	private IDocumentoServico documentoServico;
	
	@Autowired
	private HttpServletRequest request;

	@RequestMapping(path = "/listarCombo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listarCombo() {

		try {
			return enviarSucesso(colaboradorServico.listarCombo());
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/listar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listar(String params, Long idEmpresa, Long idNucleo) {

		try {
			PaginacaoParams pp = retornar(PaginacaoParams.class, params);
			return enviarSucesso(colaboradorServico.listarPaginado(pp, idEmpresa, idNucleo));
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/listarDocumentos", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listarDocumentos(Long idColaborador) {
		
		try {
//			return enviarSucesso(documentoServico.listarPaginado(idColaborador));
			return enviarSucesso(documentoServico.listarDTO(request, idColaborador));
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/consultarDocumento", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response consultarDocumento(Long id) {
		
		try {
			DocumentoDTO documentoDTO = documentoServico.consultarDTO(request, id);
			return enviarSucesso(documentoDTO);
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/removerArquivo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response removerArquivo(Long id, Long idColaborador) {
		
		try {
			DocumentoDTO documentoDTO = documentoServico.removerArquivo(request, id, idColaborador);
			return enviarSucesso(documentoDTO);
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/removerDocumento", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response removerDocumento(Long id, Long idColaborador) {
		
		try {
			documentoServico.remover(request, id, idColaborador);
			return enviarSucesso();
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	
	@RequestMapping(path = "/consultarId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response consultarId(Long id) {

		try {
			String caminhoArquivo = request.getServletContext().getRealPath(String.format("%s/%s/foto", "resources/colaborador", id));
			String url = request.getRequestURL().toString();
			url = url.substring(0, url.indexOf("colaborador"));
			ColaboradorDTO colaboradorDTO = colaboradorServico.consultarIdDTO(id, caminhoArquivo, url);
			return enviarSucesso(colaboradorDTO);
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/salvar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response salvar(Long idPerfil, @RequestBody String colaborador) {

		try {
			String caminho = request.getServletContext().getRealPath("resources/colaborador");
			ColaboradorDTO colaboradorDTO = retornar(ColaboradorDTO.class, colaborador);
			return enviarSucesso(colaboradorServico.salvarOuAtualizar(colaboradorDTO, idPerfil, caminho));
		} catch (Exception e) {

			if (e instanceof NegocioException) {

				return enviarFalha(e.getMessage());
			}

			if (e instanceof DaoException) {

				e.printStackTrace();
				return enviarFalha(e.getMessage());
			}
		}
		
		return enviarFalha("Houve um erro ao salvar o colaborador.");
	}
	
	@RequestMapping(path = "/salvarDocumento", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response salvarDocumento(@RequestBody String documento) {

		try {
			DocumentoDTO documentoDTO = retornar(DocumentoDTO.class, documento);
			documentoServico.salvarOuAtualizar(documentoDTO);
			return enviarSucesso("");
		} catch (Exception e) {

			if (e instanceof NegocioException) {

				return enviarFalha(e.getMessage());
			}

			if (e instanceof DaoException) {

				e.printStackTrace();
				return enviarFalha(e.getMessage());
			}
		}
		
		return enviarFalha("Houve um erro ao salvar o colaborador.");
	}

	@RequestMapping(path = "/possuiEmail", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response possuiEmail(String email) {

		try {

			return enviarSucesso(colaboradorServico.possuiColaboradorPorEmail(email));

		} catch (Exception e) {

			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/possuiCnj", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response possuiCnj(String cnj) {

		try {
			return enviarSucesso(colaboradorServico.possuiColaboradorPorCnj(cnj));
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}
	
	@RequestMapping(path = "/excluirId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response excluir(Long id) {

		try {

			colaboradorServico.excluirId(id);

			return enviarSucesso();

		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/foto", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response foto(String id) throws IOException, ServletException {

		colaboradorServico.criarFoto(request, id);
		return enviarSucesso("");
	}
	
	@RequestMapping(path = "/removerFoto", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response removerFoto(String id) {

		try {
			colaboradorServico.removerFoto(request, id);
			return enviarSucesso();
		} catch (Exception e) {
			return enviarFalha("N�o foi possivel remover a foto, favor tentar mais tarde!");
		}
	}
	
	@RequestMapping(path = "/documento", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response documento(String documento) throws IOException, ServletException {

		DocumentoDTO documentoDTO = retornar(DocumentoDTO.class, documento);
		colaboradorServico.criarDocumento(request, documentoDTO);
		return enviarSucesso("");
	}
	
//	@RequestMapping(path = "/removerDocumento", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
//	@ResponseBody
//	public Response removerDocumento(String id) {
//
//		try {
//			colaboradorServico.removerFoto(request, id);
//			return enviarSucesso();
//		} catch (Exception e) {
//			return enviarFalha("N�o foi possivel remover a foto, favor tentar mais tarde!");
//		}
//	}

	
}
