package br.com.vitae.bus.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.arquitetura.model.Response;
import br.com.arquitetura.web.controller.BaseController;
import br.com.vitae.bus.dto.NumeroBancoDTO;
import br.com.vitae.bus.service.INumeroBancoServico;

@Controller
@RequestMapping("/numeroBanco")
public class NumeroBancoController extends BaseController<NumeroBancoDTO> {

	@Autowired
	private INumeroBancoServico numeroBancoServico;

	@RequestMapping(path = "/listar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listarCombo() {

		try {
			return enviarSucesso(numeroBancoServico.listarNumerosBancos());
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/consultarPorNome", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response consultarPorNome(String nome) {

		try {
			List<NumeroBancoDTO> numeroBancoDTO = numeroBancoServico.consultarPorNome(nome);
			return enviarSucesso(numeroBancoDTO);
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

}
