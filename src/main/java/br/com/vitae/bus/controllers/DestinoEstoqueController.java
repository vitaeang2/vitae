package br.com.vitae.bus.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.arquitetura.model.Response;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.arquitetura.web.controller.BaseController;
import br.com.vitae.bus.dto.DestinoEstoqueDTO;
import br.com.vitae.bus.service.IDestinoEstoqueServico;

@Controller
@RequestMapping("/destinoEstoque")
public class DestinoEstoqueController extends BaseController<DestinoEstoqueDTO> {

	@Autowired
	private IDestinoEstoqueServico destinoEstoqueServico;

	@RequestMapping(path = "/listar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listar() {

		try {
			return enviarSucesso(destinoEstoqueServico.findAllDestinosEstoque());
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/listarPaginado", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listarPaginado(String params) {

		try {
			PaginacaoParams pp = retornar(PaginacaoParams.class, params);
			return enviarSucesso(destinoEstoqueServico.listarPaginado(pp));
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/consultarId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response consultarId(Long id) {

		try {
			DestinoEstoqueDTO destinoEstoqueDTO = destinoEstoqueServico.consultarIdDTO(id);
			return enviarSucesso(destinoEstoqueDTO);
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/salvar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response salvar(@RequestBody String destinoEstoque) {

		try {
			DestinoEstoqueDTO destinoEstoqueDTO = retornar(DestinoEstoqueDTO.class, destinoEstoque);
			destinoEstoqueServico.salvarOuAtualizar(destinoEstoqueDTO);
			return enviarSucesso();
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

}
