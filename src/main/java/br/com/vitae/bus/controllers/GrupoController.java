package br.com.vitae.bus.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.arquitetura.model.Response;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.arquitetura.web.controller.BaseController;
import br.com.vitae.bus.dto.GrupoDTO;
import br.com.vitae.bus.service.IGrupoServico;

@Controller
@RequestMapping("/grupo")
public class GrupoController extends BaseController<GrupoDTO> {

	@Autowired
	private IGrupoServico grupoServico;

	@RequestMapping(path = "/listarCombo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listarCombo() {

		try {
			return enviarSucesso(grupoServico.listarCombo());
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/listar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listar(String params) {

		try {
			PaginacaoParams pp = retornar(PaginacaoParams.class, params);
			return enviarSucesso(grupoServico.listarPaginado(pp));
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/consultarId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response consultarId(Long id) {

		try {
			GrupoDTO grupoDTO = grupoServico.consultarIdDTO(id);
			return enviarSucesso(grupoDTO);
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/salvar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response salvar(@RequestBody String grupo) {

		try {
			GrupoDTO grupoDTO = retornar(GrupoDTO.class, grupo);
			grupoServico.salvarOuAtualizar(grupoDTO);
			return enviarSucesso();

		} catch (DataIntegrityViolationException e) {
			return enviarFalha("O Grupo informado já está cadastrado!");
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/excluirId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response excluir(Long id, boolean verificarGrupoViculadaPatrimonio) {

		try {

			if (verificarGrupoViculadaPatrimonio) {
				GrupoDTO grupoDTO = grupoServico.consultarGrupoSemViculoPatrimonio(id);
				if (grupoDTO != null) {
					grupoDTO.setAtivo(Boolean.FALSE);
					grupoServico.salvarOuAtualizar(grupoDTO);
				} else {
					return enviarFalha("Grupo está vinculado a algum Patrimônio.");
				}
			} else {
				GrupoDTO grupoDTO = grupoServico.consultarIdDTO(id);
				grupoDTO.setAtivo(Boolean.FALSE);
				grupoServico.salvarOuAtualizar(grupoDTO);
			}
			return enviarSucesso();

		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

}
