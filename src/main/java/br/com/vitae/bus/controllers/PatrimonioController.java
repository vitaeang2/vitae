package br.com.vitae.bus.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.arquitetura.model.Response;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.arquitetura.web.controller.BaseController;
import br.com.vitae.bus.dto.PatrimonioDTO;
import br.com.vitae.bus.service.IPatrimonioServico;

@Controller
@RequestMapping("/patrimonio")
public class PatrimonioController extends BaseController<PatrimonioDTO> {

	@Autowired
	private IPatrimonioServico patrimonioServico;

	@RequestMapping(path = "/listarCombo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listarCombo() {

		try {
			return enviarSucesso(patrimonioServico.listarCombo());
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/listar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listar(String params, Long idDepartamento, Long idGrupoBem, String numeroEtiqueta, String nome, String numeroNota, String fimGarantia, String idSituacaoBem) {

		try {
			PaginacaoParams pp = retornar(PaginacaoParams.class, params);
			return enviarSucesso(patrimonioServico.listarPaginado(pp, idDepartamento, idGrupoBem, numeroEtiqueta, nome, numeroNota, fimGarantia, idSituacaoBem));
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/consultarId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response consultarId(Long id) {

		try {
			PatrimonioDTO patrimonioDTO = patrimonioServico.consultarIdDTO(id);
			return enviarSucesso(patrimonioDTO);
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/salvar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response salvar(@RequestBody String patrimonio) {

		try {			PatrimonioDTO patrimonioDTO = retornar(PatrimonioDTO.class, patrimonio);
			patrimonioServico.salvarOuAtualizar(patrimonioDTO);
			return enviarSucesso();
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/excluirId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response excluir(Long id, boolean verificarPatrimonioViculadaPatrimonio) {

		try {
			PatrimonioDTO patrimonioDTO = patrimonioServico.consultarIdDTO(id);
			patrimonioDTO.setAtivo(Boolean.FALSE);
			patrimonioServico.salvarOuAtualizar(patrimonioDTO);
			
			return enviarSucesso();

		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

}
