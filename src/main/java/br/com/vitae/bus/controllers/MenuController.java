package br.com.vitae.bus.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.arquitetura.model.Response;
import br.com.arquitetura.web.controller.BaseController;
import br.com.vitae.bus.conv.MenuConverter;
import br.com.vitae.bus.dto.PaginaPermissaoDTO;
import br.com.vitae.bus.entity.Usuario;
import br.com.vitae.bus.service.IMenuServico;
import br.com.vitae.bus.service.IPerfilAcessoServico;

@Controller
@RequestMapping("/menu")
public class MenuController extends BaseController<Usuario> {

	@Autowired
	private IMenuServico menuServico;

	@Autowired
	private IPerfilAcessoServico perfilAcessoServico;

	@RequestMapping(path = "/listarMenu", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listarMenu(Long idPerfil) {

		try {
			return enviarSucesso(menuServico.carregarMenu(idPerfil));
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/listarPaginasSelecionadas", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listarPaginasSelecionadas(Long idPerfil) {

		try {
			return enviarSucesso(MenuConverter.getInstance().listEntidade2ListDto(menuServico.listarPaginaSelecionadas(idPerfil)));
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/listarPaginasNaoSelecionadas", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listarPaginasNaoSelecionadas(Long idPerfil) {

		try {
			return enviarSucesso(MenuConverter.getInstance().listEntidade2ListDto(menuServico.listarPaginaNaoSelecionadas(idPerfil)));
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/consultarAcessoPagina", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response consultarAcessoPagina(Long idPerfil, Long idPagina) {

		try {
			PaginaPermissaoDTO paginaPermissao = perfilAcessoServico.consultarPermissaoPagina(idPerfil, idPagina);
			return enviarSucesso(paginaPermissao);
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}
}
