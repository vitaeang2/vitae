package br.com.vitae.bus.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.arquitetura.model.Response;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.arquitetura.web.controller.BaseController;
import br.com.vitae.bus.dto.GrupoBemDTO;
import br.com.vitae.bus.service.IGrupoBemServico;

@Controller
@RequestMapping("/grupo_bem")
public class GrupoBemController extends BaseController<GrupoBemDTO> {

	@Autowired
	private IGrupoBemServico grupoBemServico;

	@RequestMapping(path = "/listarCombo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listarCombo() {

		try {
			return enviarSucesso(grupoBemServico.listarCombo());
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/listar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listar(String params) {

		try {
			PaginacaoParams pp = retornar(PaginacaoParams.class, params);
			return enviarSucesso(grupoBemServico.listarPaginado(pp));
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/consultarId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response consultarId(Long id) {

		try {
			GrupoBemDTO grupoBemDTO = grupoBemServico.consultarIdDTO(id);
			return enviarSucesso(grupoBemDTO);
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/salvar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response salvar(@RequestBody String grupoBem) {

		try {
			GrupoBemDTO grupoBemDTO = retornar(GrupoBemDTO.class, grupoBem);
			grupoBemServico.salvarOuAtualizar(grupoBemDTO);
			return enviarSucesso();
		} catch (DataIntegrityViolationException e) {
			return enviarFalha("O Grupo Bem informado já está cadastrado!");
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/excluirId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response excluir(Long id, boolean verificarGrupoBemViculadaPatrimonio) {

		try {
			if (verificarGrupoBemViculadaPatrimonio) {
				GrupoBemDTO grupoBemDTO = grupoBemServico
						.consultarGrupoBemSemViculoPatrimonio(id);
				if (grupoBemDTO != null) {
					grupoBemDTO.setAtivo(Boolean.FALSE);
					grupoBemServico.salvarOuAtualizar(grupoBemDTO);
				} else {
					return enviarFalha(
							"Grupo Bem está vinculado a algum Patrimonio.");
				}
			} else {
				GrupoBemDTO grupoBemDTO = grupoBemServico.consultarIdDTO(id);
				grupoBemDTO.setAtivo(Boolean.FALSE);
				grupoBemServico.salvarOuAtualizar(grupoBemDTO);
			}
			return enviarSucesso();
			
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

}
