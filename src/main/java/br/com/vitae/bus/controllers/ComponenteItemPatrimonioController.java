package br.com.vitae.bus.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.arquitetura.model.Response;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.arquitetura.web.controller.BaseController;
import br.com.vitae.bus.dto.ComponenteItemPatrimonioDTO;
import br.com.vitae.bus.service.IComponenteItemPatrimonioServico;

@Controller
@RequestMapping("/componente_item_patrimonio")
public class ComponenteItemPatrimonioController extends BaseController<ComponenteItemPatrimonioDTO> {

	@Autowired
	private IComponenteItemPatrimonioServico componenteItemPatrimonioServico;

	@RequestMapping(path = "/listarCombo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listarCombo() {

		try {
			return enviarSucesso(componenteItemPatrimonioServico.listarCombo());
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/listar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listar(String params) {

		try {
			PaginacaoParams pp = retornar(PaginacaoParams.class, params);
			return enviarSucesso(componenteItemPatrimonioServico.listarPaginado(pp));
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/consultarId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response consultarId(Long id) {

		try {
			ComponenteItemPatrimonioDTO componenteItemPatrimonioDTO = componenteItemPatrimonioServico.consultarIdDTO(id);
			return enviarSucesso(componenteItemPatrimonioDTO);
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/salvar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response salvar(@RequestBody String componenteItemPatrimonio) {

		try {
			ComponenteItemPatrimonioDTO componenteItemPatrimonioDTO = retornar(ComponenteItemPatrimonioDTO.class, componenteItemPatrimonio);
			componenteItemPatrimonioServico.salvarOuAtualizar(componenteItemPatrimonioDTO);
			return enviarSucesso();
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/excluirId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response excluir(Long id) {

		try {
			ComponenteItemPatrimonioDTO componenteItemPatrimonioDTO = componenteItemPatrimonioServico.consultarIdDTO(id);
			componenteItemPatrimonioDTO.setAtivo(Boolean.FALSE);
			componenteItemPatrimonioServico.salvarOuAtualizar(componenteItemPatrimonioDTO);
			
			return enviarSucesso();

		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

}
