package br.com.vitae.bus.controllers;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.reflect.TypeToken;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.model.Response;
import br.com.arquitetura.util.DataUtil;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.arquitetura.web.controller.BaseController;
import br.com.vitae.bus.dto.CotacaoDTO;
import br.com.vitae.bus.dto.CotacaoFornecedorDTO;
import br.com.vitae.bus.dto.FornecedorDTO;
import br.com.vitae.bus.dto.ItemCotacaoDTO;
import br.com.vitae.bus.entity.CotacaoFornecedor;
import br.com.vitae.bus.service.ICotacaoFornecedorServico;
import br.com.vitae.bus.service.ICotacaoServico;
import br.com.vitae.bus.service.IItemCotacaoServico;

@Controller
@RequestMapping("/cotacao")
public class CotacaoController extends BaseController<CotacaoDTO> {

	@Autowired
	private ICotacaoServico cotacaoServico;

	@Autowired
	private ICotacaoFornecedorServico iCotacaoFornecedorServico;

	@Autowired
	private IItemCotacaoServico iItemcotacaoServico;

	private CotacaoDTO cotacaoDTO;

	private List<FornecedorDTO> listaFornecedoresDTO;

	private List<ItemCotacaoDTO> listaItensDTO;

	@RequestMapping(path = "/listarCombo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listarCombo() {

		try {
			return enviarSucesso(cotacaoServico.listarCombo());
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/consultarId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response consultarId(Long id) {

		try {
			CotacaoDTO cotacaoDTO = cotacaoServico.consultarIdDTO(id);
			return enviarSucesso(cotacaoDTO);
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/listarItensPorCotacao", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listarItensPorCotacao(Long idCotacao) {

		try {
			return enviarSucesso(iItemcotacaoServico.listarProdutosSelecionados(idCotacao));
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/listarFornecedoresPorPedidoCompra", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listarFornecedoresPorPedidoCompra(Long idCotacao) {

		try {
			return enviarSucesso(iCotacaoFornecedorServico.listarFornecedoresPorPedidoCompra(idCotacao));
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/listarItensPorCotacaoFornecedor", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listarItensPorCotacaoFornecedor(Long idCotacaoFornecedor) {

		try {
			return enviarSucesso(iItemcotacaoServico.listarItensPorCotacaoFornecedor(idCotacaoFornecedor));
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/listarCotacaoFornecedorPorCotacao", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listarCotacaoFornecedorPorCotacao(Long idCotacao) {

		try {
			return enviarSucesso(iCotacaoFornecedorServico.listarFornecedoresPorCotacao(idCotacao));
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/salvar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response salvar(String cotacao, String fornecedores, @RequestBody String itens) {

		try {
			recuperarDados(cotacao, itens, fornecedores);
			cotacaoServico.salvarOuAtualizar(cotacaoDTO, listaFornecedoresDTO, listaItensDTO);
			return enviarSucesso();
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/salvarResposta", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response salvarResposta(String cotacao, String cotacaoFornecedor, @RequestBody String itens) {

		try {
			recuperarDados(cotacao, itens, null);
			CotacaoFornecedorDTO cf = retornar(CotacaoFornecedorDTO.class, cotacaoFornecedor);
			cotacaoServico.salvarResposta(cotacaoDTO, listaItensDTO, cf);
			return enviarSucesso();
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/salvarEnviar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response salvarEnviar(String cotacao, String fornecedores, @RequestBody String itens) {

		try {
			recuperarDados(cotacao, itens, fornecedores);
			return enviarSucesso(cotacaoServico.salvarEEnviar(cotacaoDTO, listaFornecedoresDTO, listaItensDTO));
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/consultarCotacaoFornecedorPorToken", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response consultarCotacaoFornecedorPorToken(String token) {

		try {
			return enviarSucesso(iCotacaoFornecedorServico.consultarCotacaoFornecedorPorToken(token));
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/listar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listar(String params, Long idEmpresa, Long idNucleo, Integer status, String dataInicio, String dataFim) {

		try {
			
			LocalDate dtInicio = null;
			LocalDate dtFim = null;
			boolean isInicio = false;
			boolean isFim = false;

			if(dataInicio != null && !dataInicio.equals("")  && !dataInicio.equals("undefined")){
				dtInicio = DataUtil.strToLocalDate(dataInicio);
				isInicio = true;
			}
			
			if(dataFim != null && !dataFim.equals("")  && !dataFim.equals("undefined")){
				dtFim = DataUtil.strToLocalDate(dataFim);
				isFim = true;
			}
			
			if(isInicio && isFim){
				if(dtInicio.isAfter(dtFim)){
					return enviarFalha("Data Fim não pode ser maior que Data Início!");
				}
			}
			
			PaginacaoParams pp = retornar(PaginacaoParams.class, params);
			return enviarSucesso(cotacaoServico.listarPaginado(pp, idEmpresa, idNucleo, status, dtInicio, dtFim));
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/listarPaginadoCotacoesRespondidas", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listarPaginadoCotacoesRespondidas(String params, Long idEmpresa, Long idNucleo) {

		try {
			PaginacaoParams pp = retornar(PaginacaoParams.class, params);
			return enviarSucesso(cotacaoServico.listarPaginadoCotacoesRespondidas(pp, idEmpresa, idNucleo));
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/excluirId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response excluirCotacao(Long idCotacao) {

		try {
			cotacaoServico.excluirCotacao(idCotacao);
			return enviarSucesso();
		} catch (Exception e) {
			return enviarFalha("N�o foi poss�vel excluir a cota��o!");
		}
	}
	
	@RequestMapping(path = "/enviarEmail", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response enviarEmail(String nome, String email, String token) {

		try {
			if(cotacaoServico.enviarEmail(nome, email, token)){
				return enviarSucesso("Email enviado com sucesso!");
			}
			
			return enviarFalha("Não foi possível enviar o email, favor tentar mais tarde, ou enviar manual!");
			
		} catch (Exception e) {
			return enviarFalha("N�o foi poss�vel excluir a cota��o!");
		}
	}

	private void recuperarDados(String cotacao, String itens, String fornecedores) {

		cotacaoDTO = retornar(CotacaoDTO.class, cotacao);
		if (fornecedores != null) {
			listaFornecedoresDTO = retornar(fornecedores, new TypeToken<List<FornecedorDTO>>() {
			});
		}
		listaItensDTO = retornar(itens, new TypeToken<List<ItemCotacaoDTO>>() {
		});
	}

}
