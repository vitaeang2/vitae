package br.com.vitae.bus.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.arquitetura.model.Response;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.arquitetura.web.controller.BaseController;
import br.com.vitae.bus.dto.ProdutoDTO;
import br.com.vitae.bus.service.IProdutoEstoqueServico;
import br.com.vitae.bus.service.IProdutoServico;

@Controller
@RequestMapping("/produto")
public class ProdutoController extends BaseController<ProdutoDTO> {

	@Autowired
	private IProdutoServico produtoServico;

	@Autowired
	private IProdutoEstoqueServico iProdutoEstoqueServico;

	@RequestMapping(path = "/listarCombo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listarCombo() {

		try {
			return enviarSucesso(produtoServico.listarCombo());
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/listarFiltroCotacao", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listarFiltroCotacao(String nome) {
		
		try {
			return enviarSucesso(produtoServico.listarFiltroCotacao(nome));
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/listar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listar(String params) {

		try {
			
			PaginacaoParams pp = retornar(PaginacaoParams.class, params);
			return enviarSucesso(produtoServico.listarPaginado(pp));
			
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}
	
	@RequestMapping(path = "/listarEstoqueBaixo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listarEstoqueBaixo(String params, Long idEmpresa, Long idNucleo) {

		try {
			
			PaginacaoParams pp = retornar(PaginacaoParams.class, params);
			return enviarSucesso(iProdutoEstoqueServico.listarProdutoBaixoEstoquePaginado(pp, idEmpresa, idNucleo));
			
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/consultarId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response consultarId(Long id) {

		try {
			ProdutoDTO produtoDTO = produtoServico.consultarIdDTO(id);
			return enviarSucesso(produtoDTO);
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/salvar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response salvar(Long idUsuario, @RequestBody String produto) {

		try {
			ProdutoDTO produtoDTO = retornar(ProdutoDTO.class, produto);
			produtoServico.salvarOuAtualizar(idUsuario, produtoDTO);
			return enviarSucesso();
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}
	
	@RequestMapping(path = "/excluirId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response excluir(Long id) {

		try {

			produtoServico.excluir(id);

			return enviarSucesso();

		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}


}
