package br.com.vitae.bus.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.arquitetura.model.Response;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.arquitetura.web.controller.BaseController;
import br.com.vitae.bus.dto.ManutencaoPatrimonioDTO;
import br.com.vitae.bus.service.IManutencaoPatrimonioServico;

@Controller
@RequestMapping("/manutencao_patrimonio")
public class ManutencaoPatrimonioController extends BaseController<ManutencaoPatrimonioDTO> {

	@Autowired
	private IManutencaoPatrimonioServico manutencaoPatrimonioServico;

	@RequestMapping(path = "/listarCombo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listarCombo() {

		try {
			return enviarSucesso(manutencaoPatrimonioServico.listarCombo());
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

/*	@RequestMapping(path = "/listar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listar(String params) {

		try {

			PaginacaoParams pp = retornar(PaginacaoParams.class, params);
			return enviarSucesso(manutencaoPatrimonioServico.listarPaginado(pp));

		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}*/
	
	@RequestMapping(path = "/listar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listar(String params, String numeroEtiqueta, String nome) {

		try {
			PaginacaoParams pp = retornar(PaginacaoParams.class, params);
			return enviarSucesso(manutencaoPatrimonioServico.listarPaginado(pp, numeroEtiqueta, nome));
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/consultarId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response consultarId(Long id) {

		try {
			ManutencaoPatrimonioDTO manutencaoPatrimonioDTO = manutencaoPatrimonioServico.consultarIdDTO(id);
			return enviarSucesso(manutencaoPatrimonioDTO);
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/salvar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response salvar(Long idUsuario, @RequestBody String manutencaoPatrimonio) {

		try {
			ManutencaoPatrimonioDTO manutencaoPatrimonioDTO = retornar(ManutencaoPatrimonioDTO.class,
					manutencaoPatrimonio);
			manutencaoPatrimonioServico.salvarOuAtualizar(idUsuario, manutencaoPatrimonioDTO);
			return enviarSucesso();
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/excluirId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response excluir(Long id) {

		try {

			manutencaoPatrimonioServico.excluir(id);

			return enviarSucesso();

		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

}
