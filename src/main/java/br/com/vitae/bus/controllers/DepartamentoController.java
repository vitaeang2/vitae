package br.com.vitae.bus.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.arquitetura.model.Response;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.arquitetura.web.controller.BaseController;
import br.com.vitae.bus.dto.DepartamentoDTO;
import br.com.vitae.bus.service.IDepartamentoServico;

@Controller
@RequestMapping("/departamento")
public class DepartamentoController extends BaseController<DepartamentoDTO> {

	@Autowired
	private IDepartamentoServico departamentoServico;

	@RequestMapping(path = "/listarCombo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listarCombo() {

		try {
			return enviarSucesso(departamentoServico.listarCombo());
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/listar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listar(String params) {

		try {
			PaginacaoParams pp = retornar(PaginacaoParams.class, params);
			return enviarSucesso(departamentoServico.listarPaginado(pp));
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/consultarId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response consultarId(Long id) {

		try {
			DepartamentoDTO departamentoDTO = departamentoServico.consultarIdDTO(id);
			return enviarSucesso(departamentoDTO);
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/salvar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response salvar(@RequestBody String departamento) {

		try {
			DepartamentoDTO departamentoDTO = retornar(DepartamentoDTO.class, departamento);
			departamentoServico.salvarOuAtualizar(departamentoDTO);
			return enviarSucesso();

		} catch (DataIntegrityViolationException e) {
			return enviarFalha("O Departamento informado já está cadastrado!");
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/excluirId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response excluir(Long id, boolean verificarDepartamentoViculadaPatrimonio) {

		try {

			if (verificarDepartamentoViculadaPatrimonio) {
				DepartamentoDTO departamentoDTO = departamentoServico.consultarDepartamentoSemViculoPatrimonio(id);
				if (departamentoDTO != null) {
					departamentoDTO.setAtivo(Boolean.FALSE);
					departamentoServico.salvarOuAtualizar(departamentoDTO);
				} else {
					return enviarFalha("Departamento está vinculado a algum Patrimônio.");
				}
			} else {
				DepartamentoDTO departamentoDTO = departamentoServico.consultarIdDTO(id);
				departamentoDTO.setAtivo(Boolean.FALSE);
				departamentoServico.salvarOuAtualizar(departamentoDTO);
			}
			return enviarSucesso();

		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

}
