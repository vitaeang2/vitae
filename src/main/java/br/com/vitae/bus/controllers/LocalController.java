package br.com.vitae.bus.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.arquitetura.model.Response;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.arquitetura.web.controller.BaseController;
import br.com.vitae.bus.dto.LocalDTO;
import br.com.vitae.bus.service.ILocalServico;

@Controller
@RequestMapping("/local")
public class LocalController extends BaseController<LocalDTO> {

	@Autowired
	private ILocalServico localServico;

	@RequestMapping(path = "/listarCombo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listarCombo() {

		try {
			return enviarSucesso(localServico.listarCombo());
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/listar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listar(String params) {

		try {
			PaginacaoParams pp = retornar(PaginacaoParams.class, params);
			return enviarSucesso(localServico.listarPaginado(pp));
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/consultarId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response consultarId(Long id) {

		try {
			LocalDTO localDTO = localServico.consultarIdDTO(id);
			return enviarSucesso(localDTO);
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/salvar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response salvar(@RequestBody String local) {

		try {
			LocalDTO localDTO = retornar(LocalDTO.class, local);
			localServico.salvarOuAtualizar(localDTO);
			return enviarSucesso();

		} catch (DataIntegrityViolationException e) {
			return enviarFalha("O Local informado já está cadastrado!");
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/excluirId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response excluir(Long id, boolean verificarLocalViculadaPatrimonio) {

		try {

			if (verificarLocalViculadaPatrimonio) {
				LocalDTO localDTO = localServico.consultarLocalSemViculoPatrimonio(id);
				if (localDTO != null) {
					localDTO.setAtivo(Boolean.FALSE);
					localServico.salvarOuAtualizar(localDTO);
				} else {
					return enviarFalha("Local está vinculado a algum Patrimônio.");
				}
			} else {
				LocalDTO localDTO = localServico.consultarIdDTO(id);
				localDTO.setAtivo(Boolean.FALSE);
				localServico.salvarOuAtualizar(localDTO);
			}
			return enviarSucesso();

		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

}
