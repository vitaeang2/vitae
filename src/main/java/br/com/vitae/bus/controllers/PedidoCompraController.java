package br.com.vitae.bus.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.reflect.TypeToken;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.exception.NegocioException;
import br.com.arquitetura.model.Response;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.arquitetura.web.controller.BaseController;
import br.com.vitae.bus.dto.ColaboradorDTO;
import br.com.vitae.bus.dto.ItemPedidoCompraDTO;
import br.com.vitae.bus.dto.PedidoCompraDTO;
import br.com.vitae.bus.service.IPedidoCompraServico;

@Controller
@RequestMapping("/pedido_compra")
public class PedidoCompraController extends BaseController<ColaboradorDTO> {

	@Autowired
	private IPedidoCompraServico pedidoCompraServicoServico;
	
	@RequestMapping(path = "/consultarComListaItens", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response consultarComListaItens(Long id) {
		try{
			PedidoCompraDTO pedido = pedidoCompraServicoServico.consultarComListaItens(id);
			return enviarSucesso(pedido);
		}catch(Exception e){
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/listar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listar(String params, Integer idTipoPedidoSelecionado, Long idEmpresa, Long idNucleo) {

		try {
			PaginacaoParams pp = retornar(PaginacaoParams.class, params);
			return enviarSucesso(pedidoCompraServicoServico.listarPaginado(pp, idTipoPedidoSelecionado, idEmpresa, idNucleo));
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/salvar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response salvar(String pedidoCompra, String itens, Long idCotacao) {

		try {
			PedidoCompraDTO pedidoCompraDTO = retornar(PedidoCompraDTO.class, pedidoCompra);
			List<ItemPedidoCompraDTO> listaPedidoCompraDTO = retornar(itens, new TypeToken<List<ItemPedidoCompraDTO>>(){});
			return enviarSucesso(pedidoCompraServicoServico.salvarOuAtualizar(pedidoCompraDTO, listaPedidoCompraDTO, idCotacao));
		} catch (Exception e) {

			if (e instanceof NegocioException) {

				return enviarFalha(e.getMessage());
			}

			if (e instanceof DaoException) {

				e.printStackTrace();
				return enviarFalha(e.getMessage());
			}
		}
		
		return enviarFalha("Houve um erro ao salvar o Pedido de Compra.");
	}

}
