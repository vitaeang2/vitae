package br.com.vitae.bus.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.arquitetura.model.Response;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.arquitetura.web.controller.BaseController;
import br.com.vitae.bus.dto.CategoriaProdutoDTO;
import br.com.vitae.bus.service.ICategoriaProdutoServico;

@Controller
@RequestMapping("/categoriaProduto")
public class CategoriaProdutoController extends BaseController<CategoriaProdutoDTO> {

	@Autowired
	private ICategoriaProdutoServico categoriaProdutoServico;

	@RequestMapping(path = "/listarCombo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listarCombo() {

		try {
			return enviarSucesso(categoriaProdutoServico.listarCombo());
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/listar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response listar(String params) {

		try {
			PaginacaoParams pp = retornar(PaginacaoParams.class, params);
			return enviarSucesso(categoriaProdutoServico.listarPaginado(pp));
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/consultarId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response consultarId(Long id) {

		try {
			CategoriaProdutoDTO categoriaProdutoDTO = categoriaProdutoServico.consultarIdDTO(id);
			return enviarSucesso(categoriaProdutoDTO);
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/salvar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response salvar(@RequestBody String categoria) {

		try {
			CategoriaProdutoDTO categoriaProdutoDTO = retornar(CategoriaProdutoDTO.class, categoria);
			categoriaProdutoServico.salvarOuAtualizar(categoriaProdutoDTO);
			return enviarSucesso();
		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

	@RequestMapping(path = "/excluirId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response excluir(Long id, boolean verificarCategoriaViculadaProduto) {

		try {
			if (verificarCategoriaViculadaProduto) {
				CategoriaProdutoDTO categoriaProdutoDTO = categoriaProdutoServico
						.consultarCategoriaSemViculoProduto(id);
				if (categoriaProdutoDTO != null) {
					categoriaProdutoDTO.setAtivo(Boolean.FALSE);
					categoriaProdutoServico.salvarOuAtualizar(categoriaProdutoDTO);
				} else {
					return enviarFalha(
							"Categoria de Produto est� vinculada a algum Produto.");
				}
			} else {
				CategoriaProdutoDTO categoriaProdutoDTO = categoriaProdutoServico.consultarIdDTO(id);
				categoriaProdutoDTO.setAtivo(Boolean.FALSE);
				categoriaProdutoServico.salvarOuAtualizar(categoriaProdutoDTO);
			}
			return enviarSucesso();

		} catch (Exception e) {
			return enviarFalha(e.getMessage());
		}
	}

}
