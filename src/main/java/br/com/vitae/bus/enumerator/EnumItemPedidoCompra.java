package br.com.vitae.bus.enumerator;

public enum EnumItemPedidoCompra {
	
	 ABERTO(1,"Aberto"),  
	 FINALIZADO_COMPRADO(2,"Finalizado Comprado"),  
	 FINALIZADO_SEM_COMPRAR(3,"Finalizado sem Comprar");  

	private int codigo;

	private String descricao;

	public int getCodigo() {

		return codigo;
	}

	public void setCodigo(final Integer codigo) {

		this.codigo = codigo;
	}

	public String getDescricao() {

		return descricao;
	}

	public void setDescricao(final String descricao) {

		this.descricao = descricao;
	}

	private EnumItemPedidoCompra( final Integer codigo, final String descricao ) {

		this.codigo = codigo;
		this.descricao = descricao;
	}

	public static int getCodigo(final String descricao) {

		int codigo = 0;
		for (EnumItemPedidoCompra tipo : values()) {
			if (tipo.toString().equals(descricao)) {
				codigo = tipo.getCodigo();
			}
		}
		return codigo;
	}

	public static String getDescricao(final Integer codigo) {

		String s = null;
		for (EnumItemPedidoCompra tipo : values()) {
			if (tipo.codigo == codigo) {
				s = tipo.getDescricao();
				break;
			}
		}
		return s;
	}

	public static EnumItemPedidoCompra get(final Integer codigo) {

		EnumItemPedidoCompra s = null;
		if (codigo != -1) {
			for (EnumItemPedidoCompra tipo : values()) {
				if (tipo.codigo == codigo) {
					s = tipo;
					break;
				}

			}
		}
		return s;
	}

}
