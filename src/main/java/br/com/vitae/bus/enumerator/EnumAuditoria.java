package br.com.vitae.bus.enumerator;

/**
 * 
 * <p>
 * <b>Title:</b> EnumSexo.java
 * </p>
 * 
 * <p>
 * <b>Description:</b> Enum responsável por representar o sexo de uma pessoa
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnologia da Informa��o
 * </p>
 * 
 * @author Rogério Ferreira - rogerio0308@homtail.com
 * 
 * @version 1.0.0
 */

public enum EnumAuditoria {

	SOLUCIONADO(1, "Solucionado"),
	NAO_SOLUCIONADO(2, "Não Solucionado"),
	NOVA_AUDITORIA(3, "Nova Auditoria"),
	ABRIR_NOVO_REGISTRO(4, "Abrir Novo Registro"); 

	private int codigo;

	private String descricao;

	public int getCodigo() {

		return codigo;
	}

	public void setCodigo(final Integer codigo) {

		this.codigo = codigo;
	}

	public String getDescricao() {

		return descricao;
	}

	public void setDescricao(final String descricao) {

		this.descricao = descricao;
	}

	private EnumAuditoria( final Integer codigo, final String descricao ) {

		this.codigo = codigo;
		this.descricao = descricao;
	}

	public static int getCodigo(final String descricao) {

		int codigo = 0;
		for (EnumAuditoria tipo : values()) {
			if (tipo.toString().equals(descricao)) {
				codigo = tipo.getCodigo();
			}
		}
		return codigo;
	}

	public static String getDescricao(final Integer codigo) {

		String s = null;
		for (EnumAuditoria tipo : values()) {
			if (tipo.codigo == codigo) {
				s = tipo.getDescricao();
				break;
			}
		}
		return s;
	}

	public static EnumAuditoria get(final Integer codigo) {

		EnumAuditoria s = null;
		if (codigo != null && codigo != -1) {
			for (EnumAuditoria tipo : values()) {
				if (tipo.codigo == codigo) {
					s = tipo;
					break;
				}

			}
		}
		return s;
	}

}
