package br.com.vitae.bus.enumerator;

/**
 * 
 * <p>
 * <b>Title:</b> EnumStatusCotacaoFornecedor.java
 * </p>
 * 
 * <p>
 * <b>Description:</b> Enum responsável de fornecer um padrão aos registros excluídos 
 * </p>	
 * 	
 * <p>	
 * <b>Company: </b> B3 - Tecnologia da Informa��o
 * </p>	
 * 	
 * @author Sérgio Filho - sergioadsf@gmail.com
 * 
 * @version 1.0.0
 */
public enum EnumStatusCotacaoFornecedor {

	ENVIAR(0,"Enviar"),
	ENVIADO(1,"Enviado"),
	RESPONDIDO(2,"Respondido"),
	NAO_RESPONDIDO(3,"N�o Respondido");

	private int codigo;

	private String descricao;

	public int getCodigo() {

		return codigo;
	}

	public void setCodigo(final Integer codigo) {

		this.codigo = codigo;
	}

	public String getDescricao() {

		return descricao;
	}

	public void setDescricao(final String descricao) {

		this.descricao = descricao;
	}

	private EnumStatusCotacaoFornecedor( final Integer codigo, final String descricao ) {

		this.codigo = codigo;
		this.descricao = descricao;
	}

	public static int getCodigo(final String descricao) {

		int codigo = 0;
		for (EnumStatusCotacaoFornecedor tipo : values()) {
			if (tipo.toString().equals(descricao)) {
				codigo = tipo.getCodigo();
			}
		}
		return codigo;
	}

	public static String getDescricao(final Integer codigo) {

		String s = null;
		for (EnumStatusCotacaoFornecedor tipo : values()) {
			if (tipo.codigo == codigo) {
				s = tipo.getDescricao();
				break;
			}
		}
		return s;
	}

	public static EnumStatusCotacaoFornecedor get(final Integer codigo) {

		EnumStatusCotacaoFornecedor s = null;
		if (codigo != -1) {
			for (EnumStatusCotacaoFornecedor tipo : values()) {
				if (tipo.codigo == codigo) {
					s = tipo;
					break;
				}

			}
		}
		return s;
	}

}
