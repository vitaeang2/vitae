package br.com.vitae.bus.enumerator;

/**
 * 
 * <p>
 * <b>Title:</b> EnumSexo.java
 * </p>
 * 
 * <p>
 * <b>Description:</b> Enum responsável por representar o sexo de uma pessoa
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnologia da Informa��o
 * </p>
 * 
 * @author Rogério Ferreira - rogerio0308@homtail.com
 * 
 * @version 1.0.0
 */

public enum EnumOrigem {

	FORNECEDOR(1, "Fornecedor"),
	CLIENTE(2, "Cliente"), 
	PROCESSO(3, "Processo"); 

	private int codigo;

	private String descricao;

	public int getCodigo() {

		return codigo;
	}

	public void setCodigo(final Integer codigo) {

		this.codigo = codigo;
	}

	public String getDescricao() {

		return descricao;
	}

	public void setDescricao(final String descricao) {

		this.descricao = descricao;
	}

	private EnumOrigem( final Integer codigo, final String descricao ) {

		this.codigo = codigo;
		this.descricao = descricao;
	}

	public static int getCodigo(final String descricao) {

		int codigo = 0;
		for (EnumOrigem tipo : values()) {
			if (tipo.toString().equals(descricao)) {
				codigo = tipo.getCodigo();
			}
		}
		return codigo;
	}

	public static String getDescricao(final Integer codigo) {

		String s = null;
		for (EnumOrigem tipo : values()) {
			if (tipo.codigo == codigo) {
				s = tipo.getDescricao();
				break;
			}
		}
		return s;
	}

	public static EnumOrigem get(final Integer codigo) {

		EnumOrigem s = null;
		if (codigo != null && codigo != -1) {
			for (EnumOrigem tipo : values()) {
				if (tipo.codigo == codigo) {
					s = tipo;
					break;
				}

			}
		}
		return s;
	}

}
