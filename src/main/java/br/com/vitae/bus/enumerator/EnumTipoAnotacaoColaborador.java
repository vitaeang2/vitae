package br.com.vitae.bus.enumerator;


/**
 * 
 * <p>
 * <b>Title:</b> EnumTipoAnotacaoColaborador.java
 * </p>
 * 
 * <p>
 * <b>Description:</b> Enum responsável de disponibilizar os tipos de anotações
 * </p>	
 * 	
 * <p>	
 * <b>Company: </b> B3 - Tecnologia da Informa��o
 * </p>	
 * 	
 * @author Joaquim Barros - joaquimbarros.cmp@gmail.com
 * 
 * @version 1.0.0
 */
public enum EnumTipoAnotacaoColaborador {
	
	
	AVALIACAO(1,"Avalia��o"),
	HISTORICO_DIVERSOS(2,"Histórico Diversos"),
	ADVERTENCIA(3,"Advertência"),
	TREINAMENTO(4,"Treinamento");

	private int codigo;

	private String descricao;

	public int getCodigo() {

		return codigo;
	}

	public void setCodigo(final Integer codigo) {

		this.codigo = codigo;
	}

	public String getDescricao() {

		return descricao;
	}

	public void setDescricao(final String descricao) {

		this.descricao = descricao;
	}

	private EnumTipoAnotacaoColaborador( final Integer codigo, final String descricao ) {

		this.codigo = codigo;
		this.descricao = descricao;
	}

	public static int getCodigo(final String descricao) {

		int codigo = 0;
		for (EnumTipoAnotacaoColaborador tipo : values()) {
			if (tipo.toString().equals(descricao)) {
				codigo = tipo.getCodigo();
			}
		}
		return codigo;
	}

	public static String getDescricao(final Integer codigo) {

		String s = null;
		for (EnumTipoAnotacaoColaborador tipo : values()) {
			if (tipo.codigo == codigo) {
				s = tipo.getDescricao();
				break;
			}
		}
		return s;
	}

	public static EnumTipoAnotacaoColaborador get(final Integer codigo) {

		EnumTipoAnotacaoColaborador s = null;
		if (codigo != -1) {
			for (EnumTipoAnotacaoColaborador tipo : values()) {
				if (tipo.codigo == codigo) {
					s = tipo;
					break;
				}

			}
		}
		return s;
	}

}
