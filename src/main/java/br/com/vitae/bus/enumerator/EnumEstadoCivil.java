package br.com.vitae.bus.enumerator;

/**
 * 
 * <p>
 * <b>Title:</b> EnumEstadoCivil.java
 * </p>
 * 
 * <p>
 * <b>Description:</b> Classe responsável por representar os estados civis
 * </p>	
 * 	
 * <p>	
 * <b>Company: </b> B3 - Tecnologia da Informa��o
 * </p>	
 * 	
 * @author Joaquim Barros - joaquimbarros.cmp@gmail.com
 * 
 * @version 1.0.0
 */
public enum EnumEstadoCivil {
	
	
	SOLTEIRO(1,"Solteiro"),
	CASADO(2,"Casado"),
	SEPARADO(3,"Separado"),
	DIVORCIADO(4,"Divorciado"),
	VIUVO(5,"Viúvo");

	private int codigo;

	private String descricao;

	public int getCodigo() {

		return codigo;
	}

	public void setCodigo(final Integer codigo) {

		this.codigo = codigo;
	}

	public String getDescricao() {

		return descricao;
	}

	public void setDescricao(final String descricao) {

		this.descricao = descricao;
	}

	private EnumEstadoCivil( final Integer codigo, final String descricao ) {

		this.codigo = codigo;
		this.descricao = descricao;
	}

	public static int getCodigo(final String descricao) {

		int codigo = 0;
		for (EnumEstadoCivil tipo : values()) {
			if (tipo.toString().equals(descricao)) {
				codigo = tipo.getCodigo();
			}
		}
		return codigo;
	}

	public static String getDescricao(final Integer codigo) {

		String s = null;
		for (EnumEstadoCivil tipo : values()) {
			if (tipo.codigo == codigo) {
				s = tipo.getDescricao();
				break;
			}
		}
		return s;
	}

	public static EnumEstadoCivil get(final Integer codigo) {

		EnumEstadoCivil s = null;
		if (codigo != -1) {
			for (EnumEstadoCivil tipo : values()) {
				if (tipo.codigo == codigo) {
					s = tipo;
					break;
				}

			}
		}
		return s;
	}

}
