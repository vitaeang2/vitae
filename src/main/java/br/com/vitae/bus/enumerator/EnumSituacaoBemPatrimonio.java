package br.com.vitae.bus.enumerator;

/**
 * 
 * <p>
 * <b>Title:</b> EnumSituacaoBemPatrimonio.java
 * </p>
 * 
 * <p>
 * <b>Description:</b> Enum responsável por representar a situação de um bem do Patrimonio
 * </p>
 * 
 * <p
 * <b>Company: </b> B3 - Tecnologia da Informação
 * </p>
 * 
 * @author Joaquim Barros - joaquimbarros.cmp@gmail.com
 * 
 * @version 1.0.0
 */

public enum EnumSituacaoBemPatrimonio {

	EM_USO(1, "Em Uso (Ativo)"), EM_COMODATO(2, "Em Comodato (Ativo)"), EM_DESUSO(
			3, "Em Desuso (Inativo)"), OBSOLETO(4, "Obsoleto (Invativo)"), IMPRESTAVEL(
			5, "Imprestável (Inativo)");

	private int codigo;

	private String descricao;

	public int getCodigo() {

		return codigo;
	}

	public void setCodigo(final Integer codigo) {

		this.codigo = codigo;
	}

	public String getDescricao() {

		return descricao;
	}

	public void setDescricao(final String descricao) {

		this.descricao = descricao;
	}

	private EnumSituacaoBemPatrimonio(final Integer codigo,
			final String descricao) {

		this.codigo = codigo;
		this.descricao = descricao;
	}

	public static int getCodigo(final String descricao) {

		int codigo = 0;
		for (EnumSituacaoBemPatrimonio tipo : values()) {
			if (tipo.toString().equals(descricao)) {
				codigo = tipo.getCodigo();
			}
		}
		return codigo;
	}

	public static String getDescricao(final Integer codigo) {

		String s = null;
		for (EnumSituacaoBemPatrimonio tipo : values()) {
			if (tipo.codigo == codigo) {
				s = tipo.getDescricao();
				break;
			}
		}
		return s;
	}

	public static EnumSituacaoBemPatrimonio get(final Integer codigo) {

		EnumSituacaoBemPatrimonio s = null;
		if (codigo != -1) {
			for (EnumSituacaoBemPatrimonio tipo : values()) {
				if (tipo.codigo == codigo) {
					s = tipo;
					break;
				}

			}
		}
		return s;
	}

}
