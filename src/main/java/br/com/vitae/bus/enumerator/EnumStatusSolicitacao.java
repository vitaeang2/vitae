package br.com.vitae.bus.enumerator;

/**
 * 
 * <p>
 * <b>Title:</b> EnumUnidadeMedida.java
 * </p>
 * 
 * <p>
 * <b>Description:</b> Enum responsável por representar a unidade de medida de um produto
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnologia da Informa��o
 * </p>
 * 
 * @author Rogério Ferreira - rogerio0308@homtail.com
 * 
 * @version 1.0.0
 */

public enum EnumStatusSolicitacao {

		EM_ABERTO(1,"Em Aberto"),
		FINALIZADO(2,"Finalizado"),
		EM_ANDAMENTO(3,"Em Andamento"),
		CANCELADO(4,"Cancelado");
	
	private int codigo;

	private String descricao;

	public int getCodigo() {

		return codigo;
	}

	public void setCodigo(final Integer codigo) {

		this.codigo = codigo;
	}

	public String getDescricao() {

		return descricao;
	}

	public void setDescricao(final String descricao) {

		this.descricao = descricao;
	}

	private EnumStatusSolicitacao( final Integer codigo, final String descricao ) {

		this.codigo = codigo;
		this.descricao = descricao;
	}

	public static int getCodigo(final String descricao) {

		int codigo = 0;
		for (EnumStatusSolicitacao tipo : values()) {
			if (tipo.toString().equals(descricao)) {
				codigo = tipo.getCodigo();
			}
		}
		return codigo;
	}

	public static String getDescricao(final Integer codigo) {

		String s = null;
		for (EnumStatusSolicitacao tipo : values()) {
			if (tipo.codigo == codigo) {
				s = tipo.getDescricao();
				break;
			}
		}
		return s;
	}

	public static EnumStatusSolicitacao get(final Integer codigo) {

		EnumStatusSolicitacao s = null;
		if (codigo != -1) {
			for (EnumStatusSolicitacao tipo : values()) {
				if (tipo.codigo == codigo) {
					s = tipo;
					break;
				}

			}
		}
		return s;
	}

}
