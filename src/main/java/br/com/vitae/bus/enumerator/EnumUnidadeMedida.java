package br.com.vitae.bus.enumerator;

/**
 * 
 * <p>
 * <b>Title:</b> EnumUnidadeMedida.java
 * </p>
 * 
 * <p>
 * <b>Description:</b> Enum responsável por representar a unidade de medida de um produto
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnologia da Informa��o
 * </p>
 * 
 * @author Joaquim Barros - joaquimbarros.cmp@gmail.com
 * 
 * @version 1.0.0
 */

public enum EnumUnidadeMedida {

		QUILOGRAMA(1,"Quilograma"),
		UNIDADE(2,"Unidade"),
		CAIXA(3,"Caixa"),
		METRO_LINEAR(4,"Metro linear"),
		METRO_QUADRADO(5,"Metro quadrado"),
		METRO_CUBICO(6,"Metro cubico"),
		PECA(7,"Peça");
	
	private int codigo;

	private String descricao;

	public int getCodigo() {

		return codigo;
	}

	public void setCodigo(final Integer codigo) {

		this.codigo = codigo;
	}

	public String getDescricao() {

		return descricao;
	}

	public void setDescricao(final String descricao) {

		this.descricao = descricao;
	}

	private EnumUnidadeMedida( final Integer codigo, final String descricao ) {

		this.codigo = codigo;
		this.descricao = descricao;
	}

	public static int getCodigo(final String descricao) {

		int codigo = 0;
		for (EnumUnidadeMedida tipo : values()) {
			if (tipo.toString().equals(descricao)) {
				codigo = tipo.getCodigo();
			}
		}
		return codigo;
	}

	public static String getDescricao(final Integer codigo) {

		String s = null;
		for (EnumUnidadeMedida tipo : values()) {
			if (tipo.codigo == codigo) {
				s = tipo.getDescricao();
				break;
			}
		}
		return s;
	}

	public static EnumUnidadeMedida get(final Integer codigo) {

		EnumUnidadeMedida s = null;
		if (codigo != -1) {
			for (EnumUnidadeMedida tipo : values()) {
				if (tipo.codigo == codigo) {
					s = tipo;
					break;
				}

			}
		}
		return s;
	}

}
