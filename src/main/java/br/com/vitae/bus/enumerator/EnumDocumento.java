package br.com.vitae.bus.enumerator;

/**
 * 
 * <p>
 * <b>Title:</b> EnumStatusCotacao.java
 * </p>
 * 
 * <p>
 * <b>Description:</b> Enum responsável de fornecer um padrão aos registros excluídos
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnologia da Informa��o
 * </p>
 * 
 * @author Sérgio Filho - sergioadsf@gmail.com
 * 
 * @version 1.0.0
 */
public enum EnumDocumento {

	AVALIACAO("Avaliação"), HISTORICO_DIVERSOS("Histórico Diversos"), ADVERTENCIAS("Advertências");

	private String nome;

	private EnumDocumento( String nome ) {
		this.nome = nome;
	}

	private String getNome() {

		return nome;
	}

	public static int getCodigo(final EnumDocumento eDoc) {

		int codigo = -1;
		for (EnumDocumento tipo : values()) {
			if (tipo == eDoc) {
				codigo = tipo.ordinal();
			}
		}
		return codigo;
	}

	public static EnumDocumento get(final int codigo) {

		for (EnumDocumento tipo : values()) {
			if (tipo.ordinal() == codigo) {
				return tipo;
			}
		}
		return ADVERTENCIAS;
	}

	public static String getDescription(Integer tipoDocumento) {

		for (EnumDocumento tipo : values()) {
			if (tipo.ordinal() == tipoDocumento) {
				return tipo.getNome();
			}
		}
		return ADVERTENCIAS.getNome();
	}

}
