package br.com.vitae.bus.enumerator;

/**
 * 
 * <p>
 * <b>Title:</b> EnumTipoIncorporacaoPatrimonio.java
 * </p>
 * 
 * <p>
 * <b>Description:</b> Enum responsável de fornecedor a informação como o bem do patrimonio foi adquirido
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnologia da Informação
 * </p>
 * 
 * @author Joaquim Barros - joaquimbarros.cmp@gmail.com
 * 
 * @version 1.0.0
 */
public enum EnumTipoIncorporacaoPatrimonio {

	AQUISICAO(1, "Aquisição"), 
	DOACAO(2, "Doação"), 
	PERMUTA(3, "Permurta"), 
	MANUFATURA(4, "Manufatura"), 
	TRANSFERENCIA(5, "Transferência");

	private int codigo;

	private String descricao;

	public int getCodigo() {

		return codigo;
	}

	public void setCodigo(final Integer codigo) {

		this.codigo = codigo;
	}

	public String getDescricao() {

		return descricao;
	}

	public void setDescricao(final String descricao) {

		this.descricao = descricao;
	}

	private EnumTipoIncorporacaoPatrimonio(final Integer codigo,
			final String descricao) {

		this.codigo = codigo;
		this.descricao = descricao;
	}

	public static int getCodigo(final String descricao) {

		int codigo = 0;
		for (EnumTipoIncorporacaoPatrimonio tipo : values()) {
			if (tipo.toString().equals(descricao)) {
				codigo = tipo.getCodigo();
			}
		}
		return codigo;
	}

	public static String getDescricao(final Integer codigo) {

		String s = null;
		for (EnumTipoIncorporacaoPatrimonio tipo : values()) {
			if (tipo.codigo == codigo) {
				s = tipo.getDescricao();
				break;
			}
		}
		return s;
	}

	public static EnumTipoIncorporacaoPatrimonio get(final Integer codigo) {

		EnumTipoIncorporacaoPatrimonio s = null;
		if (codigo != -1) {
			for (EnumTipoIncorporacaoPatrimonio tipo : values()) {
				if (tipo.codigo == codigo) {
					s = tipo;
					break;
				}

			}
		}
		return s;
	}

}
