package br.com.vitae.bus.enumerator;

/**
 * 
 * <p>
 * <b>Title:</b> EnumAtivoInativo.java
 * </p>
 * 
 * <p>
 * <b>Description:</b> Enum responsável de fornecer um padrão aos registros excluídos 
 * </p>	
 * 	
 * <p>	
 * <b>Company: </b> B3 - Tecnologia da Informa��o
 * </p>	
 * 	
 * @author Joaquim Barros - joaquimbarros.cmp@gmail.com
 * 
 * @version 1.0.0
 */
public enum EnumAtivoInativo {

	ATIVO(1,"Ativo"),
	INATIVO(0,"Invativo");

	private int codigo;

	private String descricao;

	public int getCodigo() {

		return codigo;
	}

	public void setCodigo(final Integer codigo) {

		this.codigo = codigo;
	}

	public String getDescricao() {

		return descricao;
	}

	public void setDescricao(final String descricao) {

		this.descricao = descricao;
	}

	private EnumAtivoInativo( final Integer codigo, final String descricao ) {

		this.codigo = codigo;
		this.descricao = descricao;
	}

	public static int getCodigo(final String descricao) {

		int codigo = 0;
		for (EnumAtivoInativo tipo : values()) {
			if (tipo.toString().equals(descricao)) {
				codigo = tipo.getCodigo();
			}
		}
		return codigo;
	}

	public static String getDescricao(final Integer codigo) {

		String s = null;
		for (EnumAtivoInativo tipo : values()) {
			if (tipo.codigo == codigo) {
				s = tipo.getDescricao();
				break;
			}
		}
		return s;
	}

	public static EnumAtivoInativo get(final Integer codigo) {

		EnumAtivoInativo s = null;
		if (codigo != -1) {
			for (EnumAtivoInativo tipo : values()) {
				if (tipo.codigo == codigo) {
					s = tipo;
					break;
				}

			}
		}
		return s;
	}

}
