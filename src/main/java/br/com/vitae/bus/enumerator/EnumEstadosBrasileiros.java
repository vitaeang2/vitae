package br.com.vitae.bus.enumerator;

/**
 * 
 * <p>
 * <b>Title:</b> EnumEstadosBrasileiros.java
 * </p>
 * 
 * <p>
 * <b>Description:</b> Enum responsável por representar as siglas dos estados brasileiros
 * </p>	
 * 	
 * <p>	
 * <b>Company: </b> B3 - Tecnologia da Informa��o
 * </p>	
 * 	
 * @author Joaquim Barros - joaquimbarros.cmp@gmail.com
 * 
 * @version 1.0.0
 */
public enum EnumEstadosBrasileiros {
	
	 AC(1,"AC"),  
	 AL(2,"AL"),  
	 AM(3,"AM"),  
	 AP(4,"AP"),  
	 BA(5,"BA"),  
	 CE(6,"CE"),  
	 DF(7,"DF"),  
	 ES(8,"ES"),  
	 GO(9,"GO"),  
	 MA(10,"MA"),  
	 MG(11,"MG"),  
	 MS(12,"MS"),  
	 MT(13,"MT"),  
	 PA(14,"PA"),  
	 PB(15,"PB"),  
	 PE(16,"PE"),  
	 PI(17,"PI"),  
	 PR(18,"PR"),  
	 RJ(19,"RJ"),  
	 RN(20,"RN"),  
	 RO(21,"RO"),  
	 RR(22,"RR"),  
	 RS(23,"RS"),  
	 SC(24,"SC"),  
	 SE(25,"SE"),  
	 SP(26,"SP"),  
	 TO(27,"TO"); 

	private int codigo;

	private String descricao;

	public int getCodigo() {

		return codigo;
	}

	public void setCodigo(final Integer codigo) {

		this.codigo = codigo;
	}

	public String getDescricao() {

		return descricao;
	}

	public void setDescricao(final String descricao) {

		this.descricao = descricao;
	}

	private EnumEstadosBrasileiros( final Integer codigo, final String descricao ) {

		this.codigo = codigo;
		this.descricao = descricao;
	}

	public static int getCodigo(final String descricao) {

		int codigo = 0;
		for (EnumEstadosBrasileiros tipo : values()) {
			if (tipo.toString().equals(descricao)) {
				codigo = tipo.getCodigo();
			}
		}
		return codigo;
	}

	public static String getDescricao(final Integer codigo) {

		String s = null;
		for (EnumEstadosBrasileiros tipo : values()) {
			if (tipo.codigo == codigo) {
				s = tipo.getDescricao();
				break;
			}
		}
		return s;
	}

	public static EnumEstadosBrasileiros get(final Integer codigo) {

		EnumEstadosBrasileiros s = null;
		if (codigo != -1) {
			for (EnumEstadosBrasileiros tipo : values()) {
				if (tipo.codigo == codigo) {
					s = tipo;
					break;
				}

			}
		}
		return s;
	}

}
