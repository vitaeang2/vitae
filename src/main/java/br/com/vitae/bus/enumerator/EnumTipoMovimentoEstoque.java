package br.com.vitae.bus.enumerator;

/**
 * 
 * <p>
 * <b>Title:</b> EnumTipoMovimentoEstoque.java
 * </p>
 * 
 * <p>
 * <b>Description:</b> Enum responsável por representar o tipo da movimenta��o do estoque
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnologia da Informa��o
 * </p>
 * 
 * @author Joaquim Barros - joaquimbarros.cmp@gmail.com
 * 
 * @version 1.0.0
 */

public enum EnumTipoMovimentoEstoque {

	ENTRADA(1,"Entrada"),
	SAIDA(2,"Saída"),
	BALANCO(3,"Balanço");
	
	private int codigo;

	private String descricao;

	public int getCodigo() {

		return codigo;
	}

	public void setCodigo(final Integer codigo) {

		this.codigo = codigo;
	}

	public String getDescricao() {

		return descricao;
	}

	public void setDescricao(final String descricao) {

		this.descricao = descricao;
	}

	private EnumTipoMovimentoEstoque( final Integer codigo, final String descricao ) {

		this.codigo = codigo;
		this.descricao = descricao;
	}

	public static int getCodigo(final String descricao) {

		int codigo = 0;
		for (EnumTipoMovimentoEstoque tipo : values()) {
			if (tipo.toString().equals(descricao)) {
				codigo = tipo.getCodigo();
			}
		}
		return codigo;
	}

	public static String getDescricao(final Integer codigo) {

		String s = null;
		for (EnumTipoMovimentoEstoque tipo : values()) {
			if (tipo.codigo == codigo) {
				s = tipo.getDescricao();
				break;
			}
		}
		return s;
	}

	public static EnumTipoMovimentoEstoque get(final Integer codigo) {

		EnumTipoMovimentoEstoque s = null;
		if (codigo != -1) {
			for (EnumTipoMovimentoEstoque tipo : values()) {
				if (tipo.codigo == codigo) {
					s = tipo;
					break;
				}

			}
		}
		return s;
	}

}
