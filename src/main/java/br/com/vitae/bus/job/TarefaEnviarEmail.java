package br.com.vitae.bus.job;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import br.com.arquitetura.servico.MailSenderService;
import br.com.vitae.bus.entity.Cotacao;
import br.com.vitae.bus.entity.CotacaoFornecedor;
import br.com.vitae.bus.enumerator.EnumStatusCotacao;
import br.com.vitae.bus.enumerator.EnumStatusCotacaoFornecedor;
import br.com.vitae.bus.service.ICotacaoFornecedorServico;
import br.com.vitae.bus.service.ICotacaoServico;

/**
 * Classe de exemplo da utiliza��o do agendamento de tarefas. Essa classe foi mapeada no arquivo jobs.properties
 * 
 * @author sergio
 * 
 */
public class TarefaEnviarEmail implements Job {

	private static List<CotacaoFornecedor> listaFornecedores = new ArrayList<>();

	private static ICotacaoServico iCotacaoServico;

	private static ICotacaoFornecedorServico iCotacaoFornecedorServico;

	private static MailSenderService mailSenderService;

	public void execute(JobExecutionContext arg0) throws JobExecutionException {

		if (mailSenderService == null) {
			mailSenderService = TGBusinessFactory.getInstance().getMailSenderService();
		}

		if (iCotacaoServico == null) {
			iCotacaoServico = TGBusinessFactory.getInstance().getCotacaoServico();
		}

		if (iCotacaoFornecedorServico == null) {
			iCotacaoFornecedorServico = TGBusinessFactory.getInstance().getCotacaoFornecedorServico();
		}

		Cotacao cotacao = null;
		listaFornecedores.clear();
		listaFornecedores.addAll(iCotacaoFornecedorServico.listarEnviarEmailCotacao());
		for (CotacaoFornecedor cotacaoFornecedor : listaFornecedores) {
			if (iCotacaoServico.enviarEmail(cotacaoFornecedor)) {
				if (cotacao == null) {
					cotacao = iCotacaoServico.consultar(Cotacao.class, cotacaoFornecedor.getCotacao().getId());
					cotacao.setStatus(EnumStatusCotacao.ENVIADO);
				}
				cotacaoFornecedor.setStatus(EnumStatusCotacaoFornecedor.ENVIADO);
				iCotacaoFornecedorServico.salvarOuAtualizar(cotacaoFornecedor);
			}
		}

		if (cotacao != null) {
			iCotacaoServico.salvarOuAtualizar(cotacao);
		}
		System.out.println("Sucesso - " + new Date());
	}

}
