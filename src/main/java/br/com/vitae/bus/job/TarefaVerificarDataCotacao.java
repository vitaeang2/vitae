package br.com.vitae.bus.job;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import br.com.vitae.bus.service.ICotacaoFornecedorServico;
import br.com.vitae.bus.service.ICotacaoServico;

/**
 * Classe de exemplo da utiliza��o do agendamento de tarefas. Essa classe foi mapeada no arquivo jobs.properties
 * 
 * @author sergio
 * 
 */
public class TarefaVerificarDataCotacao implements Job {

	private static ICotacaoServico iCotacaoServico;

	private static ICotacaoFornecedorServico iCotacaoFornecedorServico;

	public void execute(JobExecutionContext arg0) throws JobExecutionException {

		if (iCotacaoServico == null) {
			iCotacaoServico = TGBusinessFactory.getInstance().getCotacaoServico();
		}

		if (iCotacaoFornecedorServico == null) {
			iCotacaoFornecedorServico = TGBusinessFactory.getInstance().getCotacaoFornecedorServico();
		}

		iCotacaoServico.atualizarNaoRespondido();
	}

}
