package br.com.vitae.bus.job;

import java.util.ArrayList;
import java.util.List;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import br.com.arquitetura.servico.MailSenderService;
import br.com.arquitetura.util.PasswordBuilder;
import br.com.arquitetura.util.Utils;
import br.com.vitae.bus.entity.Usuario;
import br.com.vitae.bus.service.IUsuarioServico;

/**
 * Classe de exemplo da utiliza��o do agendamento de tarefas. Essa classe foi mapeada no arquivo jobs.properties
 * 
 * @author sergio
 * 
 */
public class TarefaEnviarEmailUsuario implements Job {

	private static List<Usuario> listaUsuario = new ArrayList<>();

	private static IUsuarioServico iUsuarioServico;

	private static MailSenderService mailSenderService;

	public void execute(JobExecutionContext arg0) throws JobExecutionException {

		if (mailSenderService == null) {
			mailSenderService = TGBusinessFactory.getInstance().getMailSenderService();
		}

		if (iUsuarioServico == null) {
			iUsuarioServico = TGBusinessFactory.getInstance().getUsuarioServico();
		}

		listaUsuario.clear();
		listaUsuario.addAll(iUsuarioServico.listarEnviarEmailUsuarioCriado());
		for (Usuario usuario : listaUsuario) {
			try {
				String senha = PasswordBuilder.gerarSenha();
				iUsuarioServico.enviarEmail(usuario.getColaborador().getPessoa().getEmail(), senha, usuario.getColaborador().getPessoa().getNome());
				usuario = iUsuarioServico.consultar(Usuario.class, usuario.getId());
				usuario.setEmailEnviado(Boolean.TRUE);
				usuario.setSenha(Utils.md5(senha));
			} catch (Exception e) {
				usuario.setEmailEnviado(Boolean.FALSE);
			}
			
			iUsuarioServico.salvarOuAtualizar(usuario);
		}
	}

}
