package br.com.vitae.bus.job;

import br.com.arquitetura.context.SpringCtxHolder;
import br.com.arquitetura.servico.MailSenderService;
import br.com.vitae.bus.service.ICotacaoFornecedorServico;
import br.com.vitae.bus.service.ICotacaoServico;
import br.com.vitae.bus.service.IUsuarioServico;
import br.com.vitae.bus.service.impl.CotacaoFornecedorServicoImpl;
import br.com.vitae.bus.service.impl.CotacaoServicoImpl;
import br.com.vitae.bus.service.impl.UsuarioServico;

public class TGBusinessFactory extends SpringCtxHolder {

	private static MailSenderService mailSenderService;
	private static ICotacaoServico iCotacaoServico;
	private static IUsuarioServico iUsuarioServico;
	private static ICotacaoFornecedorServico iCotacaoFornecedorServico;

	private TGBusinessFactory() {
	}

	private static TGBusinessFactory INSTANCE = null;

	public static TGBusinessFactory getInstance() {
		if (INSTANCE == null)
			INSTANCE = new TGBusinessFactory();
		return INSTANCE;
	}

	public ICotacaoServico getCotacaoServico() {
		if (iCotacaoServico == null)
			iCotacaoServico = (ICotacaoServico) super
					.getBean(CotacaoServicoImpl.class);
		return iCotacaoServico;
	}
	
	public IUsuarioServico getUsuarioServico() {
		if (iUsuarioServico == null)
			iUsuarioServico = (IUsuarioServico) super
					.getBean(UsuarioServico.class);
		return iUsuarioServico;
	}
	
	public ICotacaoFornecedorServico getCotacaoFornecedorServico() {
		if (iCotacaoFornecedorServico == null)
			iCotacaoFornecedorServico = (ICotacaoFornecedorServico) super
					.getBean(CotacaoFornecedorServicoImpl.class);
		return iCotacaoFornecedorServico;
	}

	public MailSenderService getMailSenderService() {
		if (mailSenderService == null)
			mailSenderService = (MailSenderService) super
					.getBean(MailSenderService.class);
		return mailSenderService;
	}

}