package br.com.vitae.bus;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.springframework.context.annotation.Configuration;

import br.com.arquitetura.util.JobsConfigureUtil;
import br.com.arquitetura.util.PathConfigureUtil;

@Configuration
@WebListener
public class PreConfigureListener implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent sc) {
	}

	@Override
	public void contextInitialized(ServletContextEvent sc) {
		String pathContext = String.format("%s%s", sc.getServletContext()
				.getRealPath(""), "resources\\");
		String jobFile = "jobs.properties";
		JobsConfigureUtil.loadJobFile(pathContext + jobFile);
		String configFile = "config.properties";
		PathConfigureUtil.loadJobFile(pathContext + configFile);
		try {
			JobsConfigureUtil.startJobs();
			PathConfigureUtil.startJobs(sc.getServletContext().getContextPath());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}