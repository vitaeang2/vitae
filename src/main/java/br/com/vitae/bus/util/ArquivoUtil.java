package br.com.vitae.bus.util;

import java.io.File;

import javax.servlet.http.Part;

public class ArquivoUtil {
	
	public static void remover(String path) {
		File files = new File(path);
		for (File file : files.listFiles()) {
			file.delete();
		}
		files.delete();
	}

	public static void removerFoto(String path, String name) {
		File files = new File(path);
		for (File file : files.listFiles()) {
			if(file.getName().contains(name))
				file.delete();
		}
	}
	
	public static void removerArquivo(String path, String name) {
		File files = new File(path);
		for (File file : files.listFiles()) {
			String filename = file.getName();
			filename = filename.substring(0, filename.lastIndexOf("."));
			if(filename.contains(name))
				file.delete();
		}
	}

	public static String criarPastaFoto(String appPath, String id) {

		appPath = String.format("%s/%s/foto", appPath, id);
		criarPasta(appPath);

		return appPath;
	}
	
	public static String criarPasta(String appPath, String id) {

		appPath = String.format("%s/%s/", appPath, id);
		criarPasta(appPath);

		return appPath;
	}

	public static String criarPastaDocumento(String appPath, String id) {
		
		appPath = String.format("%s/%s/documento", appPath, id);
		criarPasta(appPath);
		
		return appPath;
	}

	public static String criarPastaImagens(String appPath, String id) {

		appPath = String.format("%s/%s", appPath, id);
		criarPasta(appPath);

		return appPath;
	}

	public static void criarPasta(String appPath) {

		File fileSaveDir = new File(appPath);
		if (!fileSaveDir.exists()) {
			fileSaveDir.mkdirs();
		}
	}

	/**
	 * Extracts file name from HTTP header content-disposition
	 */
	public static String extractFileName(Part part) {

		String contentDisp = part.getHeader("content-disposition");
		String[] items = contentDisp.split(";");
		for (String s : items) {
			if (s.trim().startsWith("filename")) {
				return s.substring(s.indexOf("=") + 2, s.length() - 1);
			}
		}
		return "";
	}

}
