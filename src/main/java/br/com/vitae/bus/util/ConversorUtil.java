package br.com.vitae.bus.util;

import java.io.File;
import java.math.BigDecimal;

public class ConversorUtil {
	
	public static BigDecimal strToBigDecimal(String valor) {
		String valorRecebidoStr = valor;
		if(valor != null){
			return new BigDecimal(valorRecebidoStr);
		}
		
		return null;
	}

	public static String BigDecimalToStr(BigDecimal valor) {
		return valor != null ? valor.toString().replace(".", ",") : null;
	}

	public static void removerFoto(String path, String name) {
		File files = new File(path);
		for (File file : files.listFiles()) {
			if(file.getName().contains(name))
				file.delete();
		}
	}
	

}
