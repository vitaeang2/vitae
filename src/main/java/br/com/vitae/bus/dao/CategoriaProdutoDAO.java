package br.com.vitae.bus.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.repository.Dao;
import br.com.arquitetura.vo.PaginacaoBuilder;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.conv.CategoriaProdutoConverter;
import br.com.vitae.bus.dto.CategoriaProdutoDTO;
import br.com.vitae.bus.entity.CategoriaProduto;

@Repository
public class CategoriaProdutoDAO {

	@Autowired
	private Dao dao;

	public List<CategoriaProduto> listarCombo() {

		StringBuilder sb = new StringBuilder();
		sb.append("select new br.com.vitae.bus.entity.CategoriaProduto(id, nome) from ");
		sb.append(CategoriaProduto.class.getName());
		sb.append(" where ativo =  ").append(Boolean.TRUE);
		return dao.listar(sb.toString(), CategoriaProduto.class);
	}

	@SuppressWarnings("unchecked")
	public PaginacaoDTO<CategoriaProdutoDTO> listarPaginado(PaginacaoParams pp) throws DaoException {

		StringBuilder sb = new StringBuilder();
		sb.append("from ");
		sb.append(CategoriaProduto.class.getName());
		sb.append(" where ativo =  ").append(Boolean.TRUE);

		return PaginacaoBuilder
				.create(CategoriaProdutoConverter.getInstance(), CategoriaProduto.class, sb.toString(), pp).search();
	}

	public CategoriaProdutoDTO consultarCategoriaSemViculoProduto(Long id) throws DaoException {

		StringBuilder sb = new StringBuilder();
		sb.append("SELECT cp ");
		sb.append("FROM   Produto p ");
		sb.append("       RIGHT JOIN p.categoria cp ");
		sb.append("WHERE  cp.id = ").append(id);
		sb.append("       AND p.id IS NULL");
		
		CategoriaProduto categoria = (CategoriaProduto) dao.consultar(sb.toString(),
				CategoriaProduto.class);
		return categoria == null ? null : CategoriaProdutoConverter.getInstance().entidade2Dto(categoria);
	}
}
