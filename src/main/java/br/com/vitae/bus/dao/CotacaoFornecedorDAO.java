package br.com.vitae.bus.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.arquitetura.repository.Dao;
import br.com.vitae.bus.entity.CotacaoFornecedor;
import br.com.vitae.bus.enumerator.EnumStatusCotacaoFornecedor;

@Repository
public class CotacaoFornecedorDAO {

	@Autowired
	private Dao dao;

	public List<CotacaoFornecedor> listarEnviarEmailCotacao() {

		StringBuilder stringBuilder = new StringBuilder();
		// id, String token, EnumStatusCotacaoFornecedor status, Long idCotacao, Long idFornecedor, String nomeContato, Long idPessoa, String nomePessoa, String cpfCnpj, String email ) {
		stringBuilder.append("select new br.com.vitae.bus.entity.CotacaoFornecedor(id, token, status, cotacao.id, fornecedor.id, ");
		stringBuilder.append(" fornecedor.nomeContato, fornecedor.pessoa.id, fornecedor.pessoa.nome, fornecedor.pessoa.cpfCnpj, fornecedor.pessoa.email , fornecedor.prestadorServico) from ");
		stringBuilder.append(CotacaoFornecedor.class.getName());
		stringBuilder.append(" where status = ");
		stringBuilder.append(EnumStatusCotacaoFornecedor.ENVIAR.ordinal());
		return dao.listar(stringBuilder.toString(), CotacaoFornecedor.class);
	}

	public List<CotacaoFornecedor> listarPorCotacao(Long idCotacao) {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("select new br.com.vitae.bus.entity.CotacaoFornecedor(id) ");
		stringBuilder.append(" from ");
		stringBuilder.append(CotacaoFornecedor.class.getName());
		stringBuilder.append(" where cotacao.id = ");
		stringBuilder.append(idCotacao);
		return dao.listar(stringBuilder.toString(), CotacaoFornecedor.class);
	}

	public List<CotacaoFornecedor> listarFornecedoresPorCotacao(Long idCotacao) {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("select new br.com.vitae.bus.entity.CotacaoFornecedor(id, fornecedor.id) ");
		stringBuilder.append(" from ");
		stringBuilder.append(CotacaoFornecedor.class.getName());
		stringBuilder.append(" where cotacao.id = ");
		stringBuilder.append(idCotacao);
		return dao.listar(stringBuilder.toString(), CotacaoFornecedor.class);
	}
	
	public List<CotacaoFornecedor> listarFornecedoresPorPedidoCompra(Long idCotacao) {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(" from ");
		stringBuilder.append(CotacaoFornecedor.class.getName());
		stringBuilder.append(" where cotacao.id = ");
		stringBuilder.append(idCotacao);
		return dao.listar(stringBuilder.toString(), CotacaoFornecedor.class);
	}

	public CotacaoFornecedor consultarCotacaoFornecedorPorToken(String token) {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(" from ");
		stringBuilder.append(CotacaoFornecedor.class.getName());
		stringBuilder.append(" where token = '");
		stringBuilder.append(token);
		stringBuilder.append("'");
		return dao.consultar(stringBuilder.toString(), CotacaoFornecedor.class);
	}

}
