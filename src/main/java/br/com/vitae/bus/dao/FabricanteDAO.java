package br.com.vitae.bus.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.repository.Dao;
import br.com.arquitetura.vo.PaginacaoBuilder;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.conv.FabricanteConverter;
import br.com.vitae.bus.dto.FabricanteDTO;
import br.com.vitae.bus.entity.Fabricante;

@Repository
public class FabricanteDAO {

	@Autowired
	private Dao dao;

	public List<Fabricante> listarCombo() {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("select new br.com.vitae.bus.entity.Fabricante(id, nome) from ");
		stringBuilder.append(Fabricante.class.getName());
		return dao.listar(stringBuilder.toString(), Fabricante.class);
	}

	public FabricanteDTO consultarFabricanteSemViculoProduto(Long id) throws DaoException {

		StringBuilder sb = new StringBuilder();
		sb.append("SELECT f ");
		sb.append("FROM   Produto p ");
		sb.append("       RIGHT JOIN p.fabricante f ");
		sb.append("WHERE  f.id = ").append(id);
		sb.append("       AND p.id IS NULL");

		Fabricante fabricante = (Fabricante) dao.consultar(sb.toString(), Fabricante.class);
		return fabricante == null ? null : FabricanteConverter.getInstance().entidade2Dto(fabricante);
	}

	@SuppressWarnings("unchecked")
	public PaginacaoDTO<FabricanteDTO> listarPaginado(PaginacaoParams pp) throws DaoException {

		StringBuilder sb = new StringBuilder();
		sb.append("from ");
		sb.append(Fabricante.class.getName());
		sb.append(" where ativo =  ").append(Boolean.TRUE);

		return PaginacaoBuilder.create(FabricanteConverter.getInstance(), Fabricante.class, sb.toString(), pp).search();
	}

}
