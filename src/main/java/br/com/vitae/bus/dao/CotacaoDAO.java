package br.com.vitae.bus.dao;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.repository.Dao;
import br.com.arquitetura.util.DataUtil;
import br.com.arquitetura.vo.PaginacaoBuilder;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.conv.CotacaoConverter;
import br.com.vitae.bus.dto.CotacaoDTO;
import br.com.vitae.bus.entity.Cotacao;
import br.com.vitae.bus.enumerator.EnumStatusCotacao;

@Repository
public class CotacaoDAO {

	@Autowired
	private Dao dao;

	public List<Cotacao> listarCombo() {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("select new br.com.vitae.bus.entity.Cotacao(id, descricao) from ");
		stringBuilder.append(Cotacao.class.getName());
		return dao.listar(stringBuilder.toString(), Cotacao.class);
	}

	@SuppressWarnings("unchecked")
	public PaginacaoDTO<CotacaoDTO> listarPaginado(PaginacaoParams pp, Long idEmpresa, Long idNucleo, Integer status, LocalDate dataInicio, LocalDate dataFim) throws DaoException {

		StringBuilder sb = new StringBuilder();
		sb.append("select new br.com.vitae.bus.entity.Cotacao(id, descricao, dataCotacao, dataValidade, status) from  ");
		sb.append(Cotacao.class.getName());
		sb.append(" where empresa.id =  ");
		sb.append(idEmpresa);
		if (status != null && status.intValue() != 0) {
			sb.append(" and status =  ");
			sb.append(EnumStatusCotacao.get(status).ordinal());
		}
		if (idNucleo != null && idNucleo.intValue() != 0) {
			sb.append(" and nucleo.id =  ");
			sb.append(idNucleo);
		}
		
		if(dataInicio != null && !dataInicio.equals("")){
			sb.append(" and dataValidade >= '");
			sb.append(dataInicio);
			sb.append("'");
		}
		
		if(dataFim != null && !dataFim.equals("")){
			sb.append(" and dataValidade <= '");
			sb.append(dataFim);
			sb.append("'");
		}
		
		return PaginacaoBuilder.create(CotacaoConverter.getInstance(), Cotacao.class, sb.toString(), pp).search();
	}

	public List<Cotacao> listarCotacaoPorDataAutal(Date data) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("select new br.com.vitae.bus.entity.Cotacao(id) from ");
		stringBuilder.append(Cotacao.class.getName());
		stringBuilder.append(" where dataValidade = ");
		stringBuilder.append(" :dataValidade ");
		stringBuilder.append(" and (status = ");
		stringBuilder.append(EnumStatusCotacao.ENVIAR.ordinal());
		stringBuilder.append(" or status = ");
		stringBuilder.append(EnumStatusCotacao.ENVIADO.ordinal());
		stringBuilder.append(" ) ");
		
		
		TypedQuery<Cotacao> query = dao.createQuery(stringBuilder.toString(), Cotacao.class);
		query.setParameter("dataValidade", DataUtil.dateToLocalDate(data));
		return query.getResultList();
	}
	
	public long quantidadeCotacoesRespondidas(Long idEmpresa, Long idNucleo) throws DaoException {

		StringBuilder sb = new StringBuilder();
		sb.append("select count(id) from  ");
		sb.append(Cotacao.class.getName());
		sb.append(" where empresa.id =  ");
		sb.append(idEmpresa);
		sb.append(" and status =  ");
		sb.append(EnumStatusCotacao.RESPONDIDO.ordinal());
		if (idNucleo != null && idNucleo.intValue() != 0) {
			sb.append(" and nucleo.id =  ");
			sb.append(idNucleo);
		}
		return dao.queryCount(sb.toString());
	}
	
	@SuppressWarnings("unchecked")
	public PaginacaoDTO<CotacaoDTO> listarPaginadoCotacoesRespondidas(PaginacaoParams pp, Long idEmpresa, Long idNucleo) throws DaoException {

		StringBuilder sb = new StringBuilder();
		sb.append("select new br.com.vitae.bus.entity.Cotacao(id, descricao, dataCotacao, dataValidade, status) from  ");
		sb.append(Cotacao.class.getName());
		sb.append(" where empresa.id =  ");
		sb.append(idEmpresa);
		sb.append(" and status =  ");
		sb.append(EnumStatusCotacao.RESPONDIDO.ordinal());
		if (idNucleo != null && idNucleo.intValue() != 0) {
			sb.append(" and nucleo.id =  ");
			sb.append(idNucleo);
		}
		return PaginacaoBuilder.create(CotacaoConverter.getInstance(), Cotacao.class, sb.toString(), pp).search();
	}

}
