package br.com.vitae.bus.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.repository.Dao;
import br.com.arquitetura.vo.PaginacaoBuilder;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.conv.ManutencaoPatrimonioConverter;
import br.com.vitae.bus.dto.ManutencaoPatrimonioDTO;
import br.com.vitae.bus.entity.ManutencaoPatrimonio;

@Repository
public class ManutencaoPatrimonioDAO {

	@Autowired
	private Dao dao;

	public List<ManutencaoPatrimonio> listarCombo() {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("select new br.com.vitae.bus.entity.ManutencaoPatrimonio(id, motivo, ativo) from ");
		stringBuilder.append(ManutencaoPatrimonio.class.getName());
		stringBuilder.append(" where ativo = ");
		stringBuilder.append(Boolean.TRUE);
		return dao.listar(stringBuilder.toString(), ManutencaoPatrimonio.class);
	}

	@SuppressWarnings("unchecked")
	public PaginacaoDTO<ManutencaoPatrimonioDTO> listarPaginado(PaginacaoParams pp) throws DaoException {

		StringBuilder sb = new StringBuilder();
		sb.append("select distinct new br.com.vitae.bus.entity.ManutencaoPatrimonio(id, motivo, ativo) from ");
		sb.append(ManutencaoPatrimonio.class.getName());
		sb.append(" where ativo = ");
		sb.append(Boolean.TRUE);
		return PaginacaoBuilder
				.create(ManutencaoPatrimonioConverter.getInstance(), ManutencaoPatrimonio.class, sb.toString(), pp)
				.search();
	}

	@SuppressWarnings("unchecked")
	public PaginacaoDTO<ManutencaoPatrimonioDTO> listarPaginado(PaginacaoParams pp, String numeroEtiqueta, String nome)
			throws DaoException {

		StringBuilder sb = new StringBuilder();
		sb.append("from ");
		sb.append(ManutencaoPatrimonio.class.getName());
		sb.append(" alias mp ");
		sb.append(" where mp.ativo =  ").append(Boolean.TRUE);

		if (numeroEtiqueta != null && !numeroEtiqueta.equals("") && !numeroEtiqueta.equals("0")) {
			sb.append(" and mp.numeroEtiqueta = '").append(numeroEtiqueta).append("' ");
		}
		if (nome != null && !nome.equals("") && !nome.equals("0")) {
			sb.append(" and upper(mp.nome) like '").append(nome.toUpperCase()).append("%' ");
		}

		return PaginacaoBuilder
				.create(ManutencaoPatrimonioConverter.getInstance(), ManutencaoPatrimonio.class, sb.toString(), pp)
				.search();
	}

}
