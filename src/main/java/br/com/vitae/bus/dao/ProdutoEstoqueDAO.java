package br.com.vitae.bus.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.repository.Dao;
import br.com.arquitetura.vo.PaginacaoBuilder;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.conv.ProdutoEstoqueConverter;
import br.com.vitae.bus.dto.ProdutoEstoqueDTO;
import br.com.vitae.bus.entity.ProdutoEstoque;

@Repository
public class ProdutoEstoqueDAO {

	@Autowired
	private Dao dao;

	public ProdutoEstoque consultarProdutoEstoque(Long produtoId, Long empresaId, Long nucleoId) {

		StringBuilder sb = new StringBuilder();
		sb.append(" select bean from ");
		sb.append(ProdutoEstoque.class.getName());
		sb.append(" bean inner join bean.produto as produto ");
		sb.append(" inner join bean.empresa as empresa ");
		sb.append(" where produto.id = ").append(produtoId);
		sb.append(" and empresa.id = ").append(empresaId);

		if (nucleoId != null && nucleoId.intValue() != 0) {
			sb.append(" and bean.nucleo.id = ").append(nucleoId);
		}
		return dao.consultar(sb.toString(), ProdutoEstoque.class);
	}
	
	public long quantidadeProdutoBaixoEstoque(Long empresaId, Long nucleoId) throws DaoException {

		StringBuilder sb = new StringBuilder();
		sb.append(" select count(produto.id) from ");
		sb.append(ProdutoEstoque.class.getName());
		sb.append(" bean inner join bean.produto as produto ");
		sb.append(" inner join bean.empresa as empresa  ");
		sb.append(" where  empresa.id = ").append(empresaId);
		sb.append(" and produto.ativo = ").append(Boolean.TRUE);

		if (nucleoId != null && nucleoId.intValue() != 0) {
			sb.append(" and bean.nucleo.id = ").append(nucleoId);
		}
		
		sb.append(" and produto.minimoEmEstoque >= ").append(" bean.quantidade ");
		return dao.queryCount(sb.toString());
	}

	public PaginacaoDTO<ProdutoEstoqueDTO> listarProdutoBaixoEstoquePaginado(PaginacaoParams pp, Long empresaId, Long nucleoId) throws DaoException {
		
		StringBuilder sb = new StringBuilder();
		sb.append(" select new br.com.vitae.bus.entity.ProdutoEstoque(bean.id, bean.quantidade, produto.id, produto.nome, produto.valorVenda, produto.valorCusto, produto.minimoEmEstoque, produto.unidadeMedida) from ");
		sb.append(ProdutoEstoque.class.getName());
		sb.append(" alias bean inner join bean.produto as produto ");
		sb.append(" inner join bean.empresa as empresa  ");
		sb.append(" where  empresa.id = ").append(empresaId);
		sb.append(" and produto.ativo = ").append(Boolean.TRUE);
		
		if (nucleoId != null && nucleoId.intValue() != 0) {
			sb.append(" and bean.nucleo.id = ").append(nucleoId);
		}
		
		sb.append(" and produto.minimoEmEstoque >= ").append(" bean.quantidade ");
		return PaginacaoBuilder.create(ProdutoEstoqueConverter.getInstance(), ProdutoEstoque.class, sb.toString(), pp).search();
	}

}
