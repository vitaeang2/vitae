package br.com.vitae.bus.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.repository.Dao;
import br.com.arquitetura.vo.PaginacaoBuilder;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.conv.QualidadeConverter;
import br.com.vitae.bus.dto.QualidadeDTO;
import br.com.vitae.bus.entity.Qualidade;
import br.com.vitae.bus.entity.Usuario;

@Repository
public class QualidadeDAO {

	@Autowired
	private Dao dao;

	public List<Qualidade> listarCombo() {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("select new br.com.vitae.bus.entity.Qualidade(id, descricao) from ");
		stringBuilder.append(Qualidade.class.getName());
		stringBuilder.append(" where ativo = ");
		stringBuilder.append(Boolean.TRUE);
		return dao.listar(stringBuilder.toString(), Qualidade.class);
	}

	public Qualidade consultarPorIdUsuario(Long idUsuario) {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("select new br.com.vitae.bus.entity.Qualidade(qualidade.id) from ");
		stringBuilder.append(Usuario.class.getName());
		stringBuilder.append(" where id = ");
		stringBuilder.append(idUsuario);
		return dao.consultar(stringBuilder.toString(), Qualidade.class);
	}

	@SuppressWarnings("unchecked")
	public PaginacaoDTO<QualidadeDTO> listarPaginado(PaginacaoParams pp) throws DaoException {

		StringBuilder sb = new StringBuilder();
		sb.append("from ");
		sb.append(Qualidade.class.getName());
		sb.append(" where ativo = ");
		sb.append(Boolean.TRUE);

		return PaginacaoBuilder.create(QualidadeConverter.getInstance(), Qualidade.class, sb.toString(), pp).search();
	}
	
	@SuppressWarnings("unchecked")
	public PaginacaoDTO<QualidadeDTO> listarPaginado(PaginacaoParams pp, Long idEmpresa, Long idNucleo) throws DaoException {
		
		StringBuilder sb = new StringBuilder();
		sb.append("select distinct new br.com.vitae.bus.entity.Qualidade(qualidade.id, qualidade.empresa.id, qualidade.empresa.nome, nuc) from ");
		sb.append(Qualidade.class.getName());
		sb.append(" alias qualidade left join qualidade.nucleo as nuc ");
		sb.append(" where qualidade.empresa.id =  ");
		sb.append(idEmpresa);
		sb.append(" and qualidade.ativo = ");
		sb.append(Boolean.TRUE);
		
		
		if (idNucleo != null && idNucleo.intValue() != 0) {
			sb.append(" and qualidade.nucleo.id =  ");
			sb.append(idNucleo);
		}
		return PaginacaoBuilder.create(QualidadeConverter.getInstance(), Qualidade.class, sb.toString(), pp).search();
	}
	
	public long quantidadeQualidadeAbertas(Long idEmpresa, Long idNucleo) throws DaoException {

		StringBuilder sb = new StringBuilder();
		sb.append("select count(id) from ");
		sb.append(Qualidade.class.getName());
		sb.append(" where empresa.id =  ");
		sb.append(idEmpresa);
		sb.append(" and ativo = ");
		sb.append(Boolean.TRUE);
		sb.append(" and ativo = ");
		sb.append(Boolean.TRUE);
		
		if (idNucleo != null && idNucleo.intValue() != 0) {
			sb.append(" and nucleo.id =  ");
			sb.append(idNucleo);
		}

		return dao.queryCount(sb.toString());
	}
}
