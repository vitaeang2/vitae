package br.com.vitae.bus.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.repository.Dao;
import br.com.arquitetura.vo.PaginacaoBuilder;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.conv.CargoConverter;
import br.com.vitae.bus.dto.CargoDTO;
import br.com.vitae.bus.entity.Cargo;

@Repository
public class CargoDAO {

	@Autowired
	private Dao dao;

	public List<Cargo> listarCombo() {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("select new br.com.vitae.bus.entity.Cargo(id, nome) from ");
		stringBuilder.append(Cargo.class.getName());
		stringBuilder.append(" where ativo =  ").append(Boolean.TRUE);
		return dao.listar(stringBuilder.toString(), Cargo.class);
	}
	
	
	@SuppressWarnings("unchecked")
	public PaginacaoDTO<CargoDTO> listarPaginado(PaginacaoParams pp) throws DaoException {

		StringBuilder sb = new StringBuilder();
		sb.append("from ");
		sb.append(Cargo.class.getName());
	    sb.append(" where ativo =  ").append(Boolean.TRUE);
	    
		return PaginacaoBuilder.create(CargoConverter.getInstance(), Cargo.class, sb.toString(), pp).search();
	}

}
