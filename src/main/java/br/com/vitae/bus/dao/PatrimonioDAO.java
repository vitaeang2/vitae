package br.com.vitae.bus.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.repository.Dao;
import br.com.arquitetura.util.DataUtil;
import br.com.arquitetura.vo.PaginacaoBuilder;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.conv.PatrimonioConverter;
import br.com.vitae.bus.dto.PatrimonioDTO;
import br.com.vitae.bus.entity.Patrimonio;

@Repository
public class PatrimonioDAO {

	@Autowired
	private Dao dao;

	public List<Patrimonio> listarCombo() {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("select new br.com.vitae.bus.entity.Patrimonio(id, nome) from ");
		stringBuilder.append(Patrimonio.class.getName());
		return dao.listar(stringBuilder.toString(), Patrimonio.class);
	}

	@SuppressWarnings("unchecked")
	public PaginacaoDTO<PatrimonioDTO> listarPaginado(PaginacaoParams pp, Long idDepartamento, Long idGrupoBem, String numeroEtiqueta, String nome, String numeroNota, String fimGarantia, String idSituacaoBem) throws DaoException {

		StringBuilder sb = new StringBuilder();
		sb.append("from ");
		sb.append(Patrimonio.class.getName());
		sb.append(" alias p ");
		sb.append(" where p.ativo =  ").append(Boolean.TRUE);

		if (idDepartamento != null && idDepartamento.intValue() != 0) {
			sb.append(" and p.departamento.id = ").append(idDepartamento);
		}
		if (idGrupoBem != null && idGrupoBem.intValue() != 0) {
			sb.append(" and p.grupoBem.id = ").append(idGrupoBem);
		}
		if (numeroEtiqueta != null && !numeroEtiqueta.equals("") && !numeroEtiqueta.equals("0")) {
			sb.append(" and p.numeroEtiqueta = '").append(numeroEtiqueta).append("' ");
		}
		if (nome != null && !nome.equals("") && !nome.equals("0")) {
			sb.append(" and upper(p.nome) like '").append(nome.toUpperCase()).append("%' ");
		}
		if (numeroNota != null && !numeroNota.equals("") && !numeroNota.equals("0")) {
			sb.append(" and p.numeroNota = '").append(numeroNota).append("' ");
		}
		if (fimGarantia != null && !fimGarantia.equals("")) {
			sb.append(" and p.fimGarantia = '").append(DataUtil.strToLocalDate(fimGarantia)).append("' ");
		}
		if (idSituacaoBem != null && !idSituacaoBem.equals("") && !idSituacaoBem.equals("0")) {
			sb.append(" and p.situacaoBemPatrimonio = ").append(new Integer(idSituacaoBem) - 1);
		}

		return PaginacaoBuilder.create(PatrimonioConverter.getInstance(), Patrimonio.class, sb.toString(), pp).search();
	}

}
