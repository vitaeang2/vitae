package br.com.vitae.bus.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.arquitetura.repository.Dao;
import br.com.vitae.bus.entity.DestinoEstoque;

@Repository
public class DestinoEstoqueDAO {

	@Autowired
	private Dao dao;

	public List<DestinoEstoque> findAllDestinosEstoque() {

		StringBuilder stringBuilder = new StringBuilder("from ").append(DestinoEstoque.class.getName()).append(" where ativo = ").append(Boolean.TRUE).append(" order by descricao ");

		return dao.listar(stringBuilder.toString(), DestinoEstoque.class);

	}

}
