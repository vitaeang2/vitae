package br.com.vitae.bus.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.arquitetura.repository.Dao;
import br.com.vitae.bus.entity.Papel;

@Repository
public class PapelDAO {

	@Autowired
	private Dao dao;

	public Papel listarPorPerfilPagina(Long idPerfilUsuario, Long idPagina) {
		Class<Papel> class1 = Papel.class;
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(" from ");
		stringBuilder.append(Papel.class.getName());
		stringBuilder.append(" pap where pap.perfilUsuario.id = ");
		stringBuilder.append(idPerfilUsuario);
		stringBuilder.append(" and pap.menu.id = ");
		stringBuilder.append(idPagina);
		return dao.consultar(stringBuilder.toString(), class1);
	}

}
