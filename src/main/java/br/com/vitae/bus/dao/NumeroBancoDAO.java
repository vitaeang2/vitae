package br.com.vitae.bus.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.arquitetura.repository.Dao;
import br.com.vitae.bus.entity.NumeroBanco;

@Repository
public class NumeroBancoDAO {

	@Autowired
	private Dao dao;

//	public List<Nucleo> listarComboPorIdEmpresa(Long idEmpresa) {
//
//		StringBuilder stringBuilder = new StringBuilder();
//		stringBuilder.append("select new br.com.vitae.bus.entity.Nucleo(id, nome) from ");
//		stringBuilder.append(Nucleo.class.getName());
//		stringBuilder.append(" where ativo = ");
//		stringBuilder.append(Boolean.TRUE);
//		stringBuilder.append(" and empresa.id = ");
//		stringBuilder.append(idEmpresa);
//		return dao.listar(stringBuilder.toString(), NumeroBanco.class);
//	}

	public List<NumeroBanco> listarNumerosBancos() {

		String hql = " from ".concat(NumeroBanco.class.getName());
		return this.dao.listar(hql, NumeroBanco.class);
	}

	public List<NumeroBanco> consultarPorNome(String nomeBanco) {

		StringBuilder sb = new StringBuilder(" from " + NumeroBanco.class.getName() + " N where 1 = 1");
		if (( nomeBanco != null ) && ( !nomeBanco.equals("") )) {
			sb.append(" and N.nome like '%" + nomeBanco + "%'");
		}
		return this.dao.listar(sb.toString(), NumeroBanco.class);
	}
}
