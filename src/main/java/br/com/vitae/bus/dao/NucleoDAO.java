package br.com.vitae.bus.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.repository.Dao;
import br.com.arquitetura.vo.PaginacaoBuilder;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.conv.NucleoConverter;
import br.com.vitae.bus.dto.NucleoDTO;
import br.com.vitae.bus.entity.Colaborador;
import br.com.vitae.bus.entity.Nucleo;

@Repository
public class NucleoDAO {

	@Autowired
	private Dao dao;

	public List<Nucleo> listarComboPorIdEmpresa(Long idEmpresa) {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("select new br.com.vitae.bus.entity.Nucleo(id, nome) from ");
		stringBuilder.append(Nucleo.class.getName());
		stringBuilder.append(" where ativo = ");
		stringBuilder.append(Boolean.TRUE);
		stringBuilder.append(" and empresa.id = ");
		stringBuilder.append(idEmpresa);
		return dao.listar(stringBuilder.toString(), Nucleo.class);
	}

	public Nucleo consultarPorColaborador(Long id) {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("select colab.nucleo from ");
		stringBuilder.append(Colaborador.class.getName());
		stringBuilder.append(" as colab where colab.id = ");
		stringBuilder.append(id);
		return dao.consultar(stringBuilder.toString(), Nucleo.class);
	}
	
	
	public boolean possuiNucleoPorColaborador(Long idColaborador) throws DaoException {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("select count(nucleo) from ");
		stringBuilder.append(Colaborador.class.getName());
		stringBuilder.append(" where id = ");
		stringBuilder.append(idColaborador);
		return dao.queryCount(stringBuilder.toString()) > 0;
	}
	
	@SuppressWarnings("unchecked")
	public PaginacaoDTO<NucleoDTO> listarPaginado(PaginacaoParams pp) throws DaoException {

		StringBuilder sb = new StringBuilder();
		sb.append("from ");
		sb.append(Nucleo.class.getName());
	    sb.append(" where ativo =  ").append(Boolean.TRUE);
	    
		return PaginacaoBuilder.create(NucleoConverter.getInstance(), Nucleo.class, sb.toString(), pp).search();
	}
	

	public List<Nucleo> listarCombo() {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(" select new br.com.vitae.bus.entity.Nucleo(nucleo.id, nucleo.nome, nucleo.empresa) from ");
		stringBuilder.append(Nucleo.class.getName());
		stringBuilder.append(" nucleo inner join nucleo.empresa as empresa ");
		stringBuilder.append(" where nucleo.ativo = ");
		stringBuilder.append(Boolean.TRUE);
		return dao.listar(stringBuilder.toString(), Nucleo.class);
	}

}
