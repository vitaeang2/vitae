package br.com.vitae.bus.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.repository.Dao;
import br.com.arquitetura.vo.PaginacaoBuilder;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.conv.FornecedorConverter;
import br.com.vitae.bus.dto.FornecedorDTO;
import br.com.vitae.bus.entity.Fornecedor;

@Repository
public class FornecedorDAO {

	@Autowired
	private Dao dao;

	public List<Fornecedor> listarCombo() {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("select new br.com.vitae.bus.entity.Fornecedor(id, pessoa.id, pessoa.nome, pessoa.cpfCnpj, pessoa.email, prestadorServico) from ");
		stringBuilder.append(Fornecedor.class.getName());
		stringBuilder.append(" where ativo = ");
		stringBuilder.append(Boolean.TRUE);
		stringBuilder.append(" order by pessoa.nome");
		return dao.listar(stringBuilder.toString(), Fornecedor.class);
	}
	
	public List<Fornecedor> listarComboPrestadorServico() {
		
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("select new br.com.vitae.bus.entity.Fornecedor(id, pessoa.id, pessoa.nome, pessoa.cpfCnpj, pessoa.email, prestadorServico) from ");
		stringBuilder.append(Fornecedor.class.getName());
		stringBuilder.append(" where ativo = ");
		stringBuilder.append(Boolean.TRUE);
		stringBuilder.append(" and prestadorServico = ");
		stringBuilder.append(Boolean.TRUE);
		return dao.listar(stringBuilder.toString(), Fornecedor.class);
	}

	
	@SuppressWarnings("unchecked")
	public PaginacaoDTO<FornecedorDTO> listarPaginado(PaginacaoParams pp) throws DaoException {

		StringBuilder sb = new StringBuilder();
		sb.append("from ");
		sb.append(Fornecedor.class.getName());
	    sb.append(" where ativo =  ").append(Boolean.TRUE);
	    
		return PaginacaoBuilder.create(FornecedorConverter.getInstance(), Fornecedor.class, sb.toString(), pp).search();
	}

	public List<Fornecedor> listarFiltroCotacao(String valor) {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("select new br.com.vitae.bus.entity.Fornecedor(id, pessoa.id, pessoa.nome, pessoa.cpfCnpj, pessoa.email, prestadorServico) from ");
		stringBuilder.append(Fornecedor.class.getName());
		stringBuilder.append(" where ativo = ");
		stringBuilder.append(Boolean.TRUE);
		stringBuilder.append(" and pessoa.nome like '");
		stringBuilder.append(valor);
		stringBuilder.append("%'");
		stringBuilder.append(" order by pessoa.nome");
		return dao.listar(stringBuilder.toString(), Fornecedor.class);
	}
}
