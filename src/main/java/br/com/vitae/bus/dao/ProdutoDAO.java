package br.com.vitae.bus.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.repository.Dao;
import br.com.arquitetura.vo.PaginacaoBuilder;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.conv.ProdutoConverter;
import br.com.vitae.bus.dto.ProdutoDTO;
import br.com.vitae.bus.entity.Produto;

@Repository
public class ProdutoDAO {

	@Autowired
	private Dao dao;

	public List<Produto> listarCombo() {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("select new br.com.vitae.bus.entity.Produto(id, nome, ativo) from ");
		stringBuilder.append(Produto.class.getName());
		stringBuilder.append(" where ativo = ");
		stringBuilder.append(Boolean.TRUE);
		stringBuilder.append(" order by nome");
		return dao.listar(stringBuilder.toString(), Produto.class);
	}

	public List<Produto> listarFiltroCotacao(String nome) {
		
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("select new br.com.vitae.bus.entity.Produto(id, nome, ativo) from ");
		stringBuilder.append(Produto.class.getName());
		stringBuilder.append(" where ativo = ");
		stringBuilder.append(Boolean.TRUE);
		stringBuilder.append(" and nome like '");
		stringBuilder.append(nome);
		stringBuilder.append("%'");
		stringBuilder.append(" order by nome");
		
		
		return dao.listar(stringBuilder.toString(), Produto.class);
	}

	@SuppressWarnings("unchecked")
	public PaginacaoDTO<ProdutoDTO> listarPaginado(PaginacaoParams pp) throws DaoException {

		StringBuilder sb = new StringBuilder();
		sb.append("select distinct new br.com.vitae.bus.entity.Produto(id, nome, coalesce(valorVenda,0), coalesce(valorCusto,0), minimoEmEstoque, ativo) from ");
		sb.append(Produto.class.getName());
		sb.append(" where ativo = ");
		sb.append(Boolean.TRUE);
//		sb.append(" order by nome");
		return PaginacaoBuilder.create(ProdutoConverter.getInstance(), Produto.class, sb.toString(), pp).search();
	}
	
//	@SuppressWarnings("unchecked")
//	public PaginacaoDTO<ProdutoDTO> listarEstoqueBaixoPaginado(PaginacaoParams pp, Long empresaId, Long nucleoId) throws DaoException {
//		
//		StringBuilder sb = new StringBuilder();
//		sb.append(" select distinct new br.com.vitae.bus.entity.Produto(produto.id, produto.nome, produto.valorVenda, produto.valorCusto, produto.ativo) from ");
//		sb.append(ProdutoEstoque.class.getName());
//		sb.append(" bean inner join bean.produto alias produto ");
//		sb.append(" inner join bean.empresa as empresa  ");
//		sb.append(" where  empresa.id = ").append(empresaId);
//		sb.append(" and produto.ativo = ").append(Boolean.TRUE);
//
//		if (nucleoId != null && nucleoId.intValue() != 0) {
//			sb.append(" and bean.nucleo.id = ").append(nucleoId);
//		}		
//		sb.append(" and produto.minimoEmEstoque >= ").append(" bean.quantidade ");
//		
//		return PaginacaoBuilder.create(ProdutoConverter.getInstance(), Produto.class, sb.toString(), pp).search();
////		return dao.listar(stringBuilder.toString(), Produto.class);
//		
///*		StringBuilder sb = new StringBuilder();
//		sb.append("select distinct new br.com.vitae.bus.entity.Produto(id, nome, valorVenda, valorCusto, ativo) from ");
//		sb.append(Produto.class.getName());
//		sb.append(" where ativo = ");
//		sb.append(Boolean.TRUE);
//		return PaginacaoBuilder.create(ProdutoConverter.getInstance(), Produto.class, sb.toString(), pp).search();*/
//	}

}
