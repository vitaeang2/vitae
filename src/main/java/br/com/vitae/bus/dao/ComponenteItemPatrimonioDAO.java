package br.com.vitae.bus.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.repository.Dao;
import br.com.arquitetura.vo.PaginacaoBuilder;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.conv.ComponenteItemPatrimonioConverter;
import br.com.vitae.bus.dto.ComponenteItemPatrimonioDTO;
import br.com.vitae.bus.entity.ComponenteItemPatrimonio;

@Repository
public class ComponenteItemPatrimonioDAO {

	@Autowired
	private Dao dao;

	public List<ComponenteItemPatrimonio> listarCombo() {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("select new br.com.vitae.bus.entity.ComponenteItemPatrimonio(id, nome) from ");
		stringBuilder.append(ComponenteItemPatrimonio.class.getName());
		return dao.listar(stringBuilder.toString(), ComponenteItemPatrimonio.class);
	}

	@SuppressWarnings("unchecked")
	public PaginacaoDTO<ComponenteItemPatrimonioDTO> listarPaginado(PaginacaoParams pp) throws DaoException {

		StringBuilder sb = new StringBuilder();
		sb.append("from ");
		sb.append(ComponenteItemPatrimonio.class.getName());
		sb.append(" where ativo =  ").append(Boolean.TRUE);

		return PaginacaoBuilder.create(ComponenteItemPatrimonioConverter.getInstance(), ComponenteItemPatrimonio.class,
				sb.toString(), pp).search();
	}

	public List<ComponenteItemPatrimonio> listarPorPatrimonio(Long idPatrimonio) {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(" from ");
		stringBuilder.append(ComponenteItemPatrimonio.class.getName());
		stringBuilder.append(" where ");
		stringBuilder.append(" patrimonio.id  = ");
		stringBuilder.append(idPatrimonio);
		stringBuilder.append(" and ativo =  ").append(Boolean.TRUE);
		return dao.listar(stringBuilder.toString(), ComponenteItemPatrimonio.class);
	}

}
