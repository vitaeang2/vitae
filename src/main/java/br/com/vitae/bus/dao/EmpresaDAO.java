package br.com.vitae.bus.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.repository.Dao;
import br.com.arquitetura.vo.PaginacaoBuilder;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.conv.EmpresaConverter;
import br.com.vitae.bus.dto.EmpresaDTO;
import br.com.vitae.bus.entity.Empresa;

@Repository
public class EmpresaDAO {

	@Autowired
	private Dao dao;

	public List<Empresa> listarCombo() {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("select new br.com.vitae.bus.entity.Empresa(id, nome) from ");
		stringBuilder.append(Empresa.class.getName());
		stringBuilder.append(" where ativo = ");
		stringBuilder.append(Boolean.TRUE);
		return dao.listar(stringBuilder.toString(), Empresa.class);
	}
	
	
	
	@SuppressWarnings("unchecked")
	public PaginacaoDTO<EmpresaDTO> listarPaginado(PaginacaoParams pp) throws DaoException {

		StringBuilder sb = new StringBuilder();
		sb.append(" from ");
		sb.append(Empresa.class.getName());
	    sb.append(" where ativo =  ").append(Boolean.TRUE);
	    
		return PaginacaoBuilder.create(EmpresaConverter.getInstance(), Empresa.class, sb.toString(), pp).search();
	}

}
