package br.com.vitae.bus.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.arquitetura.repository.Dao;
import br.com.vitae.bus.entity.ItemPedidoCompra;

@Repository
public class ItemPedidoCompraDAO {

	@Autowired
	private Dao dao;

	public List<ItemPedidoCompra> listarPorPedido(Long idPedido) {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(" from ");
		stringBuilder.append(ItemPedidoCompra.class.getName());
		stringBuilder.append(" where ");
		stringBuilder.append(" pedidoCompra.id  = ");
		stringBuilder.append(idPedido);
		return dao.listar(stringBuilder.toString(), ItemPedidoCompra.class);
	}

}
