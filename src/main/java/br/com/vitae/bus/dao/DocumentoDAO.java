package br.com.vitae.bus.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.repository.Dao;
import br.com.arquitetura.vo.PaginacaoBuilder;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.conv.DocumentoConverter;
import br.com.vitae.bus.dto.DocumentoDTO;
import br.com.vitae.bus.entity.Documento;

@Repository
public class DocumentoDAO {

	@Autowired
	private Dao dao;

	@SuppressWarnings("unchecked")
	public PaginacaoDTO<DocumentoDTO> listarPaginado(PaginacaoParams pp, Long idColaborador) throws DaoException {

		StringBuilder sb = new StringBuilder();
		sb.append("from ");
		sb.append(Documento.class.getName());
		sb.append(" where ativo =  ").append(Boolean.TRUE);

		return PaginacaoBuilder.create(DocumentoConverter.getInstance(), Documento.class, sb.toString(), pp).search();
	}

	public List<Documento> listar(Long idColaborador) {

		StringBuilder sb = new StringBuilder();
		sb.append(" select new br.com.vitae.bus.entity.Documento(id, dataEvento, tipo, historico, valorRecebido, tipoArquivo, nomeArquivo) from ");
		sb.append(Documento.class.getName());
		sb.append(" where ativo =  ").append(Boolean.TRUE);
		return dao.listar(sb.toString(), Documento.class);
	}

}
