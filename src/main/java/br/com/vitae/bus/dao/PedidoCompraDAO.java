package br.com.vitae.bus.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.repository.Dao;
import br.com.arquitetura.vo.PaginacaoBuilder;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.conv.PedidoCompraConverter;
import br.com.vitae.bus.dto.PedidoCompraDTO;
import br.com.vitae.bus.entity.PedidoCompra;
import br.com.vitae.bus.enumerator.EnumPedidoCompra;

@Repository
public class PedidoCompraDAO {

	@Autowired
	private Dao dao;

	public List<PedidoCompra> listarCombo() {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("select new br.com.vitae.bus.entity.PedidoCompra(id, descricao) from ");
		stringBuilder.append(PedidoCompra.class.getName());
		return dao.listar(stringBuilder.toString(), PedidoCompra.class);
	}

	@SuppressWarnings("unchecked")
	public PaginacaoDTO<PedidoCompraDTO> listarPaginado(PaginacaoParams pp, Integer idTipoPedidoSelecionado, Long idEmpresa, Long idNucleo) throws DaoException {

		StringBuilder sb = new StringBuilder();
		sb.append("select new br.com.vitae.bus.entity.PedidoCompra(id, dataPedidoCompra, vlrFrete, vlrDesconto, fornecedor.id, fornecedor.nomeContato, ");
		sb.append("fornecedor.pessoa.id, fornecedor.pessoa.nome, fornecedor.pessoa.cpfCnpj, fornecedor.pessoa.email, status) from  ");
		sb.append(PedidoCompra.class.getName());
		sb.append(" where empresa.id =  ");
		sb.append(idEmpresa);
//		if (status != null && status.intValue() != 0) {
//			sb.append(" and status =  ");
//		}
		if (idNucleo != null && idNucleo.intValue() != 0) {
			sb.append(" and nucleo.id =  ");
			sb.append(idNucleo);
		}
		if (idTipoPedidoSelecionado != null && idTipoPedidoSelecionado.intValue() != 0) {
			sb.append(" and status =  ");
			sb.append(EnumPedidoCompra.get(idTipoPedidoSelecionado).ordinal());
		}
		return PaginacaoBuilder.create(PedidoCompraConverter.getInstance(), PedidoCompra.class, sb.toString(), pp).search();
	}

}
