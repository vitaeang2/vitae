package br.com.vitae.bus.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.repository.Dao;
import br.com.arquitetura.vo.PaginacaoBuilder;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.conv.TipoSolicitacaoConverter;
import br.com.vitae.bus.dto.TipoSolicitacaoDTO;
import br.com.vitae.bus.entity.TipoSolicitacao;

@Repository
public class TipoSolicitacaoDAO {

	@Autowired
	private Dao dao;

	public List<TipoSolicitacao> listarCombo() {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("select new br.com.vitae.bus.entity.TipoSolicitacao(id, nome) from ");
		stringBuilder.append(TipoSolicitacao.class.getName());
		return dao.listar(stringBuilder.toString(), TipoSolicitacao.class);
	}

	@SuppressWarnings("unchecked")
	public PaginacaoDTO<TipoSolicitacaoDTO> listarPaginado(PaginacaoParams pp) throws DaoException {

		StringBuilder sb = new StringBuilder();
		sb.append("from ");
		sb.append(TipoSolicitacao.class.getName());
		sb.append(" where ativo =  ").append(Boolean.TRUE);

		return PaginacaoBuilder
				.create(TipoSolicitacaoConverter.getInstance(), TipoSolicitacao.class, sb.toString(), pp).search();
	}

	public TipoSolicitacaoDTO consultarCategoriaSemViculoSolicitacao(Long id) throws DaoException {

		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ts ");
		sb.append("FROM   Solicitacao s ");
		sb.append("       RIGHT JOIN s.tipoSolicitacao ts ");
		sb.append("WHERE  ts.id = ").append(id);
		sb.append("       AND s.id IS NULL");
		
		TipoSolicitacao tipoSolicitacao = (TipoSolicitacao) dao.consultar(sb.toString(),
				TipoSolicitacao.class);
		return tipoSolicitacao == null ? null : TipoSolicitacaoConverter.getInstance().entidade2Dto(tipoSolicitacao);
	}
}
