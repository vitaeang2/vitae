package br.com.vitae.bus.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.repository.Dao;
import br.com.arquitetura.vo.PaginacaoBuilder;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.conv.GrupoConverter;
import br.com.vitae.bus.dto.GrupoDTO;
import br.com.vitae.bus.entity.Grupo;

@Repository
public class GrupoDAO {

	@Autowired
	private Dao dao;

	public List<Grupo> listarCombo() {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("select new br.com.vitae.bus.entity.Grupo(id, nome) from ");
		stringBuilder.append(Grupo.class.getName());
		return dao.listar(stringBuilder.toString(), Grupo.class);
	}

	@SuppressWarnings("unchecked")
	public PaginacaoDTO<GrupoDTO> listarPaginado(PaginacaoParams pp) throws DaoException {

		StringBuilder sb = new StringBuilder();
		sb.append("from ");
		sb.append(Grupo.class.getName());
		sb.append(" where ativo =  ").append(Boolean.TRUE);

		return PaginacaoBuilder
				.create(GrupoConverter.getInstance(), Grupo.class, sb.toString(), pp).search();
	}
	
	public GrupoDTO consultarGrupoSemViculoPatrimonio(Long id) throws DaoException {
		
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT d ");
		sb.append("FROM   Patrimonio p ");
		sb.append("       RIGHT JOIN p.grupo d ");
		sb.append("WHERE  d.id = ").append(id);
		sb.append("       AND p.id IS NULL");

		Grupo grupo = (Grupo) dao.consultar(sb.toString(),
				Grupo.class);
		return grupo == null ? null : GrupoConverter.getInstance().entidade2Dto(grupo);
	}	

}
