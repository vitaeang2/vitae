package br.com.vitae.bus.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.arquitetura.repository.Dao;
import br.com.vitae.bus.entity.ItemCotacao;

@Repository
public class ItemCotacaoDAO {

	@Autowired
	private Dao dao;

	public List<ItemCotacao> listarPorCotacaoFornecedor(Long idCF) {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("select new br.com.vitae.bus.entity.ItemCotacao(id) ");
		stringBuilder.append(" from ");
		stringBuilder.append(ItemCotacao.class.getName());
		stringBuilder.append(" where cotacaoFornecedor.id = ");
		stringBuilder.append(idCF);
		return dao.listar(stringBuilder.toString(), ItemCotacao.class);
	}

	public List<ItemCotacao> listarProdutosSelecionados(Long idCotacao) {

		StringBuilder stringBuilder = new StringBuilder();
		// Long id, BigDecimal vlrUnitario, BigDecimal quantidade, CotacaoFornecedor cotacaoFornecedor, Produto produto
		stringBuilder.append("select distinct new br.com.vitae.bus.entity.ItemCotacao(id, vlrUnitario, quantidade, produto.id, produto.nome, produto.unidadeMedida) ");
		stringBuilder.append(" from ");
		stringBuilder.append(ItemCotacao.class.getName());
		stringBuilder.append(" where cotacaoFornecedor.cotacao.id = ");
		stringBuilder.append(idCotacao);
		return dao.listar(stringBuilder.toString(), ItemCotacao.class);
	}

	public List<ItemCotacao> listarItensPorCotacaoFornecedor(Long idCotacaoFornecedor) {

		StringBuilder stringBuilder = new StringBuilder();
		// Long id, BigDecimal vlrUnitario, BigDecimal quantidade, CotacaoFornecedor cotacaoFornecedor, Produto produto
		stringBuilder.append("select distinct new br.com.vitae.bus.entity.ItemCotacao(id, vlrUnitario, quantidade, produto.id, produto.nome, produto.unidadeMedida) ");
		stringBuilder.append(" from ");
		stringBuilder.append(ItemCotacao.class.getName());
		stringBuilder.append(" where cotacaoFornecedor.id = ");
		stringBuilder.append(idCotacaoFornecedor);
		return dao.listar(stringBuilder.toString(), ItemCotacao.class);
	}

}
