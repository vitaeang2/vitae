package br.com.vitae.bus.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.repository.Dao;
import br.com.arquitetura.vo.PaginacaoBuilder;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.conv.SolicitacaoConverter;
import br.com.vitae.bus.dto.SolicitacaoDTO;
import br.com.vitae.bus.entity.Solicitacao;
import br.com.vitae.bus.entity.Usuario;
import br.com.vitae.bus.enumerator.EnumStatusSolicitacao;

@Repository
public class SolicitacaoDAO {

	@Autowired
	private Dao dao;

	public List<Solicitacao> listarCombo() {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("select new br.com.vitae.bus.entity.Solicitacao(id, descricao) from ");
		stringBuilder.append(Solicitacao.class.getName());
		stringBuilder.append(" where ativo = ");
		stringBuilder.append(Boolean.TRUE);
		return dao.listar(stringBuilder.toString(), Solicitacao.class);
	}

	public Solicitacao consultarPorIdUsuario(Long idUsuario) {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("select new br.com.vitae.bus.entity.Solicitacao(solicitacao.id) from ");
		stringBuilder.append(Usuario.class.getName());
		stringBuilder.append(" where id = ");
		stringBuilder.append(idUsuario);
		return dao.consultar(stringBuilder.toString(), Solicitacao.class);
	}

	@SuppressWarnings("unchecked")
	public PaginacaoDTO<SolicitacaoDTO> listarPaginado(PaginacaoParams pp, Long idNucleo, Long idLocal, Long idStatusSolicitacao) throws DaoException {

		StringBuilder sb = new StringBuilder();
		sb.append("from ");
		sb.append(Solicitacao.class.getName());
		sb.append(" alias s ");
		sb.append(" where s.ativo = ");
		sb.append(Boolean.TRUE);

		if (idNucleo != null && idNucleo.intValue() != 0) {
			sb.append(" and s.nucleo.id = ").append(idNucleo);
		}

		if (idLocal != null && idLocal.intValue() != 0) {
			sb.append(" and s.local.id = ").append(idLocal);
		}
		
		if (idStatusSolicitacao != null && idStatusSolicitacao.intValue() != 0) {
			sb.append(" and s.statusSolicitacao = ").append(idStatusSolicitacao - 1);
		}

		return PaginacaoBuilder.create(SolicitacaoConverter.getInstance(), Solicitacao.class, sb.toString(), pp).search();
	}

	@SuppressWarnings("unchecked")
	public PaginacaoDTO<SolicitacaoDTO> listarPaginadoSolicitacoesAbertas(PaginacaoParams pp, Long idEmpresa, Long idNucleo, Long idLocal) throws DaoException {

		StringBuilder sb = new StringBuilder();
		sb.append(" from ");
		sb.append(Solicitacao.class.getName());
		sb.append(" where ativo = ");
		sb.append(Boolean.TRUE);
		sb.append(" and statusSolicitacao = ");
		sb.append(EnumStatusSolicitacao.EM_ABERTO.ordinal());

		sb.append(" and empresa.id = ").append(idEmpresa);

		if (idNucleo != null && idNucleo.intValue() != 0) {
			sb.append(" and nucleo.id = ").append(idNucleo);
		}

		if (idLocal != null && idLocal.intValue() != 0) {
			sb.append(" and local.id = ").append(idLocal);
		}

		return PaginacaoBuilder.create(SolicitacaoConverter.getInstance(), Solicitacao.class, sb.toString(), pp).search();
	}
	
	public long quantidadeSolicitacoesAbertas(Long idEmpresa, Long idNucleo) throws DaoException {

		StringBuilder sb = new StringBuilder();
		sb.append(" select count (id) from ");
		sb.append(Solicitacao.class.getName());
		sb.append(" where ativo = ");
		sb.append(Boolean.TRUE);
		sb.append(" and statusSolicitacao = ");
		sb.append(EnumStatusSolicitacao.EM_ABERTO.ordinal());

		sb.append(" and empresa.id = ").append(idEmpresa);

		if (idNucleo != null && idNucleo.intValue() != 0) {
			sb.append(" and nucleo.id = ").append(idNucleo);
		}

		return dao.queryCount(sb.toString());
	}

}
