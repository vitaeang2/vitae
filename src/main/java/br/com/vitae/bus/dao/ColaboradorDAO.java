package br.com.vitae.bus.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.repository.Dao;
import br.com.arquitetura.vo.PaginacaoBuilder;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.conv.ColaboradorConverter;
import br.com.vitae.bus.dto.ColaboradorDTO;
import br.com.vitae.bus.entity.Colaborador;
import br.com.vitae.bus.entity.Usuario;

@Repository
public class ColaboradorDAO {

	@Autowired
	private Dao dao;

	public List<Colaborador> listarCombo() {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("select new br.com.vitae.bus.entity.Colaborador(id, pessoa, nomeContato, celularContato) from ");
		stringBuilder.append(Colaborador.class.getName());
		stringBuilder.append(" where ativo = ");
		stringBuilder.append(Boolean.TRUE);
		return dao.listar(stringBuilder.toString(), Colaborador.class);
	}

	// @SuppressWarnings("unchecked")
	// public PaginacaoDTO<ColaboradorDTO> listarPaginado(PaginacaoParams pp, Long idEmpresa) throws DaoException {
	//
	// StringBuilder sb = new StringBuilder();
	// sb.append("select distinct new br.com.vitae.bus.entity.Colaborador(colab.id, colab.pessoa.id, colab.pessoa.nome, colab.pessoa.cpfCnpj, colab.empresa.id, colab.empresa.nome, nuc) from ");
	// sb.append(Colaborador.class.getName());
	// sb.append(" alias colab left join colab.nucleo as nuc ");
	// sb.append(" where colab.empresa.id = ");
	// sb.append(idEmpresa);
	// return PaginacaoBuilder.create(ColaboradorConverter.getInstance(), Colaborador.class, sb.toString(), pp).search();
	// }

	public Colaborador consultarPorIdUsuario(Long idUsuario) {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("select new br.com.vitae.bus.entity.Colaborador(colaborador.id) from ");
		stringBuilder.append(Usuario.class.getName());
		stringBuilder.append(" where id = ");
		stringBuilder.append(idUsuario);
		return dao.consultar(stringBuilder.toString(), Colaborador.class);
	}

	@SuppressWarnings("unchecked")
	public PaginacaoDTO<ColaboradorDTO> listarPaginado(PaginacaoParams pp, Long idEmpresa, Long idNucleo) throws DaoException {

		StringBuilder sb = new StringBuilder();
		sb.append("select distinct new br.com.vitae.bus.entity.Colaborador(colab.id, colab.pessoa.id, colab.pessoa.nome, colab.pessoa.cpfCnpj, colab.empresa.id, colab.empresa.nome, nuc) from ");
		sb.append(Colaborador.class.getName());
		sb.append(" alias colab left join colab.nucleo as nuc ");
		sb.append(" where colab.empresa.id =  ");
		sb.append(idEmpresa);
		sb.append(" and colab.ativo = ");
		sb.append(Boolean.TRUE);
		

		if (idNucleo != null && idNucleo.intValue() != 0) {
			sb.append(" and colab.nucleo.id =  ");
			sb.append(idNucleo);
		}
		return PaginacaoBuilder.create(ColaboradorConverter.getInstance(), Colaborador.class, sb.toString(), pp).search();
	}

	public boolean possuiColaboradorPorEmail(String email) throws DaoException {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("select count(colab) from ");
		stringBuilder.append(Colaborador.class.getName());
		stringBuilder.append(" as colab where colab.pessoa.email = ");
		stringBuilder.append("'"+email+"'");
		return dao.queryCount(stringBuilder.toString()) > 0;
	}

	public boolean possuiColaboradorPorCnj(String cnj) throws DaoException {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("select count(colab) from ");
		stringBuilder.append(Colaborador.class.getName());
		stringBuilder.append(" as colab where colab.pessoa.cpfCnpj = ");
		stringBuilder.append(cnj);
		return dao.queryCount(stringBuilder.toString()) > 0;
	}

}
