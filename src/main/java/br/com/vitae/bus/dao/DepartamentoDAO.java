package br.com.vitae.bus.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.repository.Dao;
import br.com.arquitetura.vo.PaginacaoBuilder;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.conv.DepartamentoConverter;
import br.com.vitae.bus.dto.DepartamentoDTO;
import br.com.vitae.bus.entity.Departamento;

@Repository
public class DepartamentoDAO {

	@Autowired
	private Dao dao;

	public List<Departamento> listarCombo() {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("select new br.com.vitae.bus.entity.Departamento(id, nome) from ");
		stringBuilder.append(Departamento.class.getName());
		return dao.listar(stringBuilder.toString(), Departamento.class);
	}

	@SuppressWarnings("unchecked")
	public PaginacaoDTO<DepartamentoDTO> listarPaginado(PaginacaoParams pp) throws DaoException {

		StringBuilder sb = new StringBuilder();
		sb.append("from ");
		sb.append(Departamento.class.getName());
		sb.append(" where ativo =  ").append(Boolean.TRUE);

		return PaginacaoBuilder
				.create(DepartamentoConverter.getInstance(), Departamento.class, sb.toString(), pp).search();
	}
	
	public DepartamentoDTO consultarDepartamentoSemViculoPatrimonio(Long id) throws DaoException {
		
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT d ");
		sb.append("FROM   Patrimonio p ");
		sb.append("       RIGHT JOIN p.departamento d ");
		sb.append("WHERE  d.id = ").append(id);
		sb.append("       AND p.id IS NULL");

		Departamento departamento = (Departamento) dao.consultar(sb.toString(),
				Departamento.class);
		return departamento == null ? null : DepartamentoConverter.getInstance().entidade2Dto(departamento);
	}	

}
