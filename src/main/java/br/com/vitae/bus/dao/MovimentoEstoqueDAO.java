package br.com.vitae.bus.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.arquitetura.repository.Dao;
import br.com.vitae.bus.entity.MovimentoEstoque;

@Repository
public class MovimentoEstoqueDAO {

	@Autowired
	private Dao dao;

	public List<MovimentoEstoque> getMovimentosEstoque(Long produtoId, Long nucleoId, Long empresaId) {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(" select bean from ");
		stringBuilder.append(MovimentoEstoque.class.getName());
		stringBuilder.append(" bean inner join bean.produto as produto  ");
		stringBuilder.append(" left outer join bean.empresa as empresa left outer join bean.nucleo as nucleo ");
		stringBuilder.append(" where produto.id  = ").append(produtoId).append(" and empresa.id = ");
		stringBuilder.append(empresaId);

		if (nucleoId != null && nucleoId.intValue() != 0) {
			stringBuilder.append(" and nucleo.id = ").append(nucleoId);
		}

		return dao.listar(stringBuilder.toString(), MovimentoEstoque.class);
	}

}
