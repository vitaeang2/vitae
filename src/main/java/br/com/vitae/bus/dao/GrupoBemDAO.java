package br.com.vitae.bus.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.repository.Dao;
import br.com.arquitetura.vo.PaginacaoBuilder;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.conv.GrupoBemConverter;
import br.com.vitae.bus.dto.GrupoBemDTO;
import br.com.vitae.bus.entity.GrupoBem;

@Repository
public class GrupoBemDAO {

	@Autowired
	private Dao dao;

	public List<GrupoBem> listarCombo() {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("select new br.com.vitae.bus.entity.GrupoBem(id, nome) from ");
		stringBuilder.append(GrupoBem.class.getName());
		return dao.listar(stringBuilder.toString(), GrupoBem.class);
	}

	@SuppressWarnings("unchecked")
	public PaginacaoDTO<GrupoBemDTO> listarPaginado(PaginacaoParams pp) throws DaoException {

		StringBuilder sb = new StringBuilder();
		sb.append("from ");
		sb.append(GrupoBem.class.getName());
		sb.append(" where ativo =  ").append(Boolean.TRUE);

		return PaginacaoBuilder
				.create(GrupoBemConverter.getInstance(), GrupoBem.class, sb.toString(), pp).search();
	}
	

	public GrupoBemDTO consultarGrupoBemSemViculoPatrimonio(Long id) throws DaoException {

		StringBuilder sb = new StringBuilder();
		sb.append("SELECT g ");
		sb.append("FROM   Patrimonio p ");
		sb.append("       RIGHT JOIN p.grupoBem g ");
		sb.append("WHERE  g.id = ").append(id);
		sb.append("       AND p.id IS NULL");

		GrupoBem grupoBem = (GrupoBem) dao.consultar(sb.toString(), GrupoBem.class);
		return grupoBem == null ? null : GrupoBemConverter.getInstance().entidade2Dto(grupoBem);
	}

}
