package br.com.vitae.bus.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.repository.Dao;
import br.com.arquitetura.vo.PaginacaoBuilder;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.conv.CategoriaGrupoConverter;
import br.com.vitae.bus.dto.CategoriaGrupoDTO;
import br.com.vitae.bus.entity.CategoriaGrupo;

@Repository
public class CategoriaGrupoDAO {

	@Autowired
	private Dao dao;

	public List<CategoriaGrupo> listarCombo() {

		StringBuilder sb = new StringBuilder();
		sb.append("select new br.com.vitae.bus.entity.CategoriaGrupo(id, nome) from ");
		sb.append(CategoriaGrupo.class.getName());
		sb.append(" where ativo =  ").append(Boolean.TRUE);
		return dao.listar(sb.toString(), CategoriaGrupo.class);
	}

	@SuppressWarnings("unchecked")
	public PaginacaoDTO<CategoriaGrupoDTO> listarPaginado(PaginacaoParams pp) throws DaoException {

		StringBuilder sb = new StringBuilder();
		sb.append("from ");
		sb.append(CategoriaGrupo.class.getName());
		sb.append(" where ativo =  ").append(Boolean.TRUE);

		return PaginacaoBuilder
				.create(CategoriaGrupoConverter.getInstance(), CategoriaGrupo.class, sb.toString(), pp).search();
	}

	public CategoriaGrupoDTO consultarCategoriaSemViculoGrupo(Long id) throws DaoException {

		StringBuilder sb = new StringBuilder();
		sb.append("SELECT cp ");
		sb.append("FROM   Grupo p ");
		sb.append("       RIGHT JOIN p.categoria cp ");
		sb.append("WHERE  cp.id = ").append(id);
		sb.append("       AND p.id IS NULL");
		
		CategoriaGrupo categoria = (CategoriaGrupo) dao.consultar(sb.toString(),
				CategoriaGrupo.class);
		return categoria == null ? null : CategoriaGrupoConverter.getInstance().entidade2Dto(categoria);
	}
	

	public List<CategoriaGrupo> listarPorGrupo(Long idGrupo) {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(" from ");
		stringBuilder.append(CategoriaGrupo.class.getName());
		stringBuilder.append(" where ");
		stringBuilder.append(" grupo.id  = ");
		stringBuilder.append(idGrupo);
		stringBuilder.append(" and ativo =  ").append(Boolean.TRUE);
		return dao.listar(stringBuilder.toString(), CategoriaGrupo.class);
	}
}
