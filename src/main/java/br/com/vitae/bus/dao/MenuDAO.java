package br.com.vitae.bus.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.arquitetura.repository.Dao;
import br.com.vitae.bus.entity.Menu;
import br.com.vitae.bus.entity.Papel;

@Repository
public class MenuDAO {

	@Autowired
	private Dao dao;

	public List<Menu> listarMenu(Long idPerfilUsuario) {

		Class<Menu> class1 = Menu.class;
		StringBuilder sb = new StringBuilder();
		sb.append("select distinct menu.* from usuario usu");
		sb.append(" inner join perfil_usuario pu on pu.id = usu.id_perfil_usuario");
		sb.append(" inner join papel pap on pu.id = pap.id_perfil_usuario");
		sb.append(" inner join permissao per on per.id = pap.id_permissao");
		sb.append(" inner join menu menu on menu.id = pap.id_menu").append(" where pu.id = ");
		sb.append(idPerfilUsuario).append(" and per.visualiza = ").append(Boolean.TRUE);
		sb.append(" and ativo = ").append(Boolean.TRUE);
		sb.append(" order by menu.id ");
		return dao.listarNativeQuery(sb.toString(), class1);
	}

	public List<Menu> listarPaginaNaoSelecionado(Long idPerfilUsuario) {

		Class<Menu> class1 = Menu.class;
		StringBuilder sb = new StringBuilder();
		sb.append("from ").append(class1.getName());
		sb.append(" menu where menu.id not in ( ");
		sb.append("  SELECT papel.menu.id FROM  ");
		sb.append(Papel.class.getName());
		sb.append(" papel where papel.perfilUsuario.id = ");
		sb.append(idPerfilUsuario);
		sb.append(" ) and menu.nomePagina is not null ");
		return dao.listar(sb.toString(), class1);
	}

	public List<Menu> listarPaginaSelecionado(Long idPerfilUsuario) {

		Class<Menu> class1 = Menu.class;
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("select menu from ");
		stringBuilder.append(Papel.class.getName());
		stringBuilder.append(" pap inner join pap.menu menu where pap.perfilUsuario.id = ");
		stringBuilder.append(idPerfilUsuario);
		stringBuilder.append(" and menu.nomePagina is not null");
		return dao.listar(stringBuilder.toString(), class1);
	}

}
