package br.com.vitae.bus.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.repository.Dao;
import br.com.arquitetura.vo.PaginacaoBuilder;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.conv.LocalConverter;
import br.com.vitae.bus.dto.LocalDTO;
import br.com.vitae.bus.entity.Local;

@Repository
public class LocalDAO {

	@Autowired
	private Dao dao;

	public List<Local> listarCombo() {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("select new br.com.vitae.bus.entity.Local(id, nome) from ");
		stringBuilder.append(Local.class.getName());
		stringBuilder.append(" where ativo =  ").append(Boolean.TRUE);
		return dao.listar(stringBuilder.toString(), Local.class);
	}

	@SuppressWarnings("unchecked")
	public PaginacaoDTO<LocalDTO> listarPaginado(PaginacaoParams pp) throws DaoException {

		StringBuilder sb = new StringBuilder();
		sb.append("from ");
		sb.append(Local.class.getName());
		sb.append(" where ativo =  ").append(Boolean.TRUE);

		return PaginacaoBuilder
				.create(LocalConverter.getInstance(), Local.class, sb.toString(), pp).search();
	}
	
	public LocalDTO consultarLocalSemViculoPatrimonio(Long id) throws DaoException {
		
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT d ");
		sb.append("FROM   Patrimonio p ");
		sb.append("       RIGHT JOIN p.local d ");
		sb.append("WHERE  d.id = ").append(id);
		sb.append("       AND p.id IS NULL");

		Local local = (Local) dao.consultar(sb.toString(),
				Local.class);
		return local == null ? null : LocalConverter.getInstance().entidade2Dto(local);
	}	

}
