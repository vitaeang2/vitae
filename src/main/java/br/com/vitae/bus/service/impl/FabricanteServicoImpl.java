package br.com.vitae.bus.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.servico.ServiceImpl;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.conv.FabricanteConverter;
import br.com.vitae.bus.dao.FabricanteDAO;
import br.com.vitae.bus.dto.FabricanteDTO;
import br.com.vitae.bus.entity.Fabricante;
import br.com.vitae.bus.service.IFabricanteServico;

@SuppressWarnings("serial")
@Service
public class FabricanteServicoImpl extends ServiceImpl<Fabricante> implements IFabricanteServico {

	@Autowired
	private FabricanteDAO fabricanteDao;

	@Override
	@Transactional(readOnly = true)
	public FabricanteDTO consultarIdDTO(Long id) {

		Fabricante fabricante = consultar(Fabricante.class, id);
		return FabricanteConverter.getInstance().entidade2Dto(fabricante);
	}

	@Override
	@Transactional(readOnly = false)
	public void salvarOuAtualizar(FabricanteDTO fabricanteDTO) {

		Fabricante fabricante = FabricanteConverter.getInstance().dto2Entidade(fabricanteDTO);
		if (fabricante.getId() == null) {			
			fabricante.setAtivo(Boolean.TRUE);
		}	
		salvarOuAtualizar(fabricante);
	}


	@Override
	@Transactional(readOnly = true)
	public PaginacaoDTO<FabricanteDTO> listarPaginado(PaginacaoParams pp) throws DaoException {

		return fabricanteDao.listarPaginado(pp);
	}

	@Override
	@Transactional(readOnly = true)
	public List<FabricanteDTO> listarCombo() {

		List<Fabricante> lista = fabricanteDao.listarCombo();
		return FabricanteConverter.getInstance().listEntidade2ListDto(lista);
	}

	@Override
	public FabricanteDTO consultarFabricanteSemViculoProduto(Long id) throws DaoException {
		return fabricanteDao.consultarFabricanteSemViculoProduto(id);
	}	
}
