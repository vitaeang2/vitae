package br.com.vitae.bus.service.impl;

import java.time.LocalDate;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.servico.ServiceImpl;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.conv.DocumentoConverter;
import br.com.vitae.bus.dao.DocumentoDAO;
import br.com.vitae.bus.dto.DocumentoDTO;
import br.com.vitae.bus.entity.Documento;
import br.com.vitae.bus.service.IDocumentoServico;
import br.com.vitae.bus.util.ArquivoUtil;

@SuppressWarnings("serial")
@Service
public class DocumentoServicoImpl extends ServiceImpl<Documento> implements IDocumentoServico {
	
	private static final String RESOURCES_COLABORADOR = "resources/colaborador/";

	@Autowired
	private DocumentoDAO documentoDao;

	@Override
	@Transactional(readOnly = false)
	public Documento salvarOuAtualizar(DocumentoDTO documentoDTO) {

		Documento documento = DocumentoConverter.getInstance().dto2Entidade(documentoDTO);

		if (documento.getId() == null) {

			documento.setAtivo(Boolean.TRUE);
			documento.setDataCadastro(LocalDate.now());
		}

		return (Documento) salvarOuAtualizar(documento);
	}

	@Override
	@Transactional(readOnly = true)
	public PaginacaoDTO<DocumentoDTO> listarPaginado(PaginacaoParams pp, Long idColaborador) throws DaoException {

		return documentoDao.listarPaginado(pp, idColaborador);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Documento> listar(Long idColaborador) {
		return documentoDao.listar(idColaborador);
	}

	@Override
	@Transactional(readOnly = true)
	public List<DocumentoDTO> listarDTO(HttpServletRequest request, Long idColaborador) {
		List<DocumentoDTO> listaDTO = DocumentoConverter.getInstance()
				.listEntidade2ListDto(documentoDao.listar(idColaborador));
		
		
		
		for (DocumentoDTO docDTO : listaDTO) {
			docDTO.setCaminhoArquivo(getUrlArquivo(idColaborador, request, docDTO));
		}
		
		return listaDTO;
	}

	private String getUrlArquivo(Long idColaborador, HttpServletRequest request, DocumentoDTO docDTO) {
		
		String url = request.getRequestURL().toString();
		url = url.substring(0, url.indexOf("colaborador"));
		String tipo = docDTO.getTipoArquivo();

		if (tipo != null && !tipo.equals("")) {
			String idColabStr = idColaborador.toString();
			String idStr = docDTO.getId().toString();
			tipo = tipo.substring(tipo.indexOf("/") + 1);
			return String.format("%s/%s/%s/documento/%s", url, RESOURCES_COLABORADOR, idColabStr, idStr + "." + tipo);
		}
		
		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public DocumentoDTO consultarDTO(HttpServletRequest request, Long id) {
		Documento documento = consultar(Documento.class, id);
		DocumentoDTO documentoDTO = DocumentoConverter.getInstance().entidade2Dto(documento);
		documentoDTO.setCaminhoArquivo(getUrlArquivo(documentoDTO.getIdColaborador(), request, documentoDTO));
		return documentoDTO;
	}

	@Override
	@Transactional(readOnly = false)
	public DocumentoDTO removerArquivo(HttpServletRequest request, Long id, Long idColaborador) {
		Documento documento = consultar(Documento.class, Long.valueOf(id));
		documento.setTipoArquivo(null);
		String path = ArquivoUtil.criarPastaDocumento(request.getServletContext().getRealPath("resources/colaborador"),
				idColaborador.toString());
		ArquivoUtil.removerArquivo(path, id.toString());
		salvarOuAtualizar(documento);
		return DocumentoConverter.getInstance().entidade2Dto(documento);
	}
	
	@Override
	@Transactional(readOnly = false)
	public void remover(HttpServletRequest request, Long id, Long idColaborador) {
		Documento documento = consultar(Documento.class, Long.valueOf(id));
		String path = ArquivoUtil.criarPastaDocumento(request.getServletContext().getRealPath("resources/colaborador"),
				idColaborador.toString());
		ArquivoUtil.removerArquivo(path, id.toString());
		remover(documento);
	}

}
