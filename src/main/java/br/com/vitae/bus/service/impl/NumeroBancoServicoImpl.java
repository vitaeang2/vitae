package br.com.vitae.bus.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.arquitetura.servico.ServiceImpl;
import br.com.vitae.bus.conv.NumeroBancoConverter;
import br.com.vitae.bus.dao.NumeroBancoDAO;
import br.com.vitae.bus.dto.NumeroBancoDTO;
import br.com.vitae.bus.entity.NumeroBanco;
import br.com.vitae.bus.service.INumeroBancoServico;

@SuppressWarnings("serial")
@Service
public class NumeroBancoServicoImpl extends ServiceImpl<NumeroBanco> implements INumeroBancoServico {

	@Autowired
	private NumeroBancoDAO numeroBancoDao;

	@Override
	@Transactional(readOnly = true)
	public List<NumeroBancoDTO> listarNumerosBancos() {

		List<NumeroBanco> lista = numeroBancoDao.listarNumerosBancos();
		return NumeroBancoConverter.getInstance().listEntidade2ListDto(lista);
	}

	@Override
	@Transactional(readOnly = true)
	public List<NumeroBancoDTO> consultarPorNome(String nomeBanco) {

		List<NumeroBanco> lista = numeroBancoDao.consultarPorNome(nomeBanco);
		return NumeroBancoConverter.getInstance().listEntidade2ListDto(lista);
	}

}
