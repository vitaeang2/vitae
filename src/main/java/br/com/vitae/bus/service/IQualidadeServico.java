package br.com.vitae.bus.service;

import java.util.List;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.exception.NegocioException;
import br.com.arquitetura.servico.IService;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.dto.QualidadeDTO;
import br.com.vitae.bus.entity.Qualidade;

public interface IQualidadeServico extends IService<Qualidade> {

	QualidadeDTO consultarIdDTO(Long id);

	QualidadeDTO salvarOuAtualizar(QualidadeDTO qualidadeDTO) throws NegocioException, DaoException;


	List<QualidadeDTO> listarCombo();
	
	PaginacaoDTO<QualidadeDTO> listarPaginado(PaginacaoParams pp) throws DaoException;
	
	PaginacaoDTO<QualidadeDTO> listarPaginado(PaginacaoParams pp, Long idEmpresa) throws DaoException;
	
	PaginacaoDTO<QualidadeDTO> listarPaginado(PaginacaoParams pp, Long idEmpresa, Long idNucleo) throws DaoException;
	
	void excluirId(Long id);
}
