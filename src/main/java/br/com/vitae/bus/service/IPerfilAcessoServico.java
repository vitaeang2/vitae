package br.com.vitae.bus.service;

import java.util.List;

import br.com.arquitetura.servico.IService;
import br.com.vitae.bus.dto.PaginaPermissaoDTO;
import br.com.vitae.bus.dto.PerfilUsuarioDTO;
import br.com.vitae.bus.entity.PerfilUsuario;

public interface IPerfilAcessoServico extends IService<PerfilUsuario> {

	List<PaginaPermissaoDTO> listarPaginaSelecionadas(Long idPerfilUsuario);

	List<PerfilUsuarioDTO> listarPerfis();

	void salvarPermissoes(Long idPerfil, List<PaginaPermissaoDTO> lista);
	
	PaginaPermissaoDTO consultarPermissaoPagina(Long idPerfilUsuario, Long idPagina);
}
