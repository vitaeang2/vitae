package br.com.vitae.bus.service;

import java.util.List;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.servico.IService;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.dto.FornecedorDTO;
import br.com.vitae.bus.entity.Fornecedor;

public interface IFornecedorServico extends IService<Fornecedor> {

	FornecedorDTO consultarIdDTO(Long id);

	void salvarOuAtualizar(FornecedorDTO empresaDTO);

	PaginacaoDTO<FornecedorDTO> listarPaginado(PaginacaoParams pp) throws DaoException;

	List<FornecedorDTO> listarCombo();

	List<FornecedorDTO> listarComboPrestadorServico();

	List<FornecedorDTO> listarFiltroCotacao(String valor);
}
