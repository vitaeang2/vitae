package br.com.vitae.bus.service.impl;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.servico.MailSenderService;
import br.com.arquitetura.servico.ServiceImpl;
import br.com.arquitetura.util.PasswordBuilder;
import br.com.arquitetura.util.Utils;
import br.com.vitae.bus.conv.UsuarioConverter;
import br.com.vitae.bus.dao.UsuarioDAO;
import br.com.vitae.bus.dto.UsuarioDTO;
import br.com.vitae.bus.entity.Nucleo;
import br.com.vitae.bus.entity.Usuario;
import br.com.vitae.bus.service.INucleoServico;
import br.com.vitae.bus.service.IUsuarioServico;

@SuppressWarnings("serial")
@Service
public class UsuarioServico extends ServiceImpl<Usuario> implements IUsuarioServico {

	@Autowired
	private UsuarioDAO usuarioDao;

	@Autowired
	private INucleoServico iNucleoServico;

	@Autowired
	private MailSenderService mailSenderService;

	@Override
	@Transactional(readOnly = true)
	public UsuarioDTO findPorLoginSenha(String login, String senha) {

		Usuario usuario = usuarioDao.findPorLoginSenha(login, senha);

		Long idColaborador = usuario.getColaborador().getId();
		try {
			if (iNucleoServico.possuiNucleoPorColaborador(idColaborador)) {
				Nucleo nucleo = iNucleoServico.consultarPorColaborador(idColaborador);
				usuario.getColaborador().setNucleo(nucleo);
			}
			return UsuarioConverter.getInstance().entidade2Dto(usuario);
		} catch (DaoException e) {
			e.printStackTrace();
		}
		return null;

	}

	@Override
	@Transactional(readOnly = true)
	public Usuario findPorColaborador(Long idColaborador) {

		return usuarioDao.findPorColaborador(idColaborador);
	}

	@Override
	@Transactional(readOnly = true)
	public UsuarioDTO findPorColaboradorDTO(Long idColaborador) {

		Usuario usuario = usuarioDao.findPorColaborador(idColaborador);
		return new UsuarioDTO(usuario);
	}

	@Override
	@Transactional(readOnly = false)
	public void alterarSenha(Long id, String novaSenha) {

		Usuario usuarioSalvar = consultar(Usuario.class, id);
		usuarioSalvar.setPrimeiroAcesso(false);
		usuarioSalvar.setSenha(Utils.md5(novaSenha));
		salvarOuAtualizar(usuarioSalvar);
	}

	@Override
	@Transactional(readOnly = false)
	public boolean esqueceuSenha(String email) {

		Usuario usuario = usuarioDao.findPorEmail(email);
		String senha = PasswordBuilder.gerarSenha();

		try {
			alterarSenha(usuario.getId(), senha);
			enviarEmail(email, senha, usuario.getColaborador().getPessoa().getNome());
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Usuario> listarEnviarEmailUsuarioCriado() {

		return usuarioDao.listarEnviarEmailUsuarioCriado();
	}

	public void enviarEmail(String email, String senha, String nome) throws Exception {

		mailSenderService.setTemplateName("esqueciSenha.vm");
		HashMap<String, Object> map = new HashMap<>();
		map.put("nome", nome);
		map.put("senhaProvisoria", senha);
		mailSenderService.sendHtmlWithTemplate(email, "Solicita��o de senha", map);
	}

	@Override
	public List<UsuarioDTO> listarCombo() {
		List<Usuario> lista = usuarioDao.listarCombo();
		return UsuarioConverter.getInstance().listEntidade2ListDto(lista);
	}

}
