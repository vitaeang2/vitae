package br.com.vitae.bus.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.servico.ServiceImpl;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.conv.ManutencaoPatrimonioConverter;
import br.com.vitae.bus.dao.ManutencaoPatrimonioDAO;
import br.com.vitae.bus.dto.ManutencaoPatrimonioDTO;
import br.com.vitae.bus.entity.ManutencaoPatrimonio;
import br.com.vitae.bus.service.IManutencaoPatrimonioServico;

@SuppressWarnings("serial")
@Service
public class ManutencaoPatrimonioServicoImpl extends ServiceImpl<ManutencaoPatrimonio>
		implements IManutencaoPatrimonioServico {

	@Autowired
	private ManutencaoPatrimonioDAO manutencaoPatrimonioDao;

	@Override
	@Transactional(readOnly = true)
	public ManutencaoPatrimonioDTO consultarIdDTO(Long id) {

		ManutencaoPatrimonio manutencaoPatrimonio = consultar(ManutencaoPatrimonio.class, id);
		return ManutencaoPatrimonioConverter.getInstance().entidade2Dto(manutencaoPatrimonio);
	}

	@Override
	@Transactional(readOnly = false)
	public void salvarOuAtualizar(Long idUsuario, ManutencaoPatrimonioDTO manutencaoPatrimonioDTO) {

		ManutencaoPatrimonio manutencaoPatrimonio = ManutencaoPatrimonioConverter.getInstance()
				.dto2Entidade(manutencaoPatrimonioDTO);

		if (manutencaoPatrimonio != null && manutencaoPatrimonio.getId() == null) {

			// manutencaoPatrimonio.setDataCadastro(LocalDate.now());
			manutencaoPatrimonio.setAtivo(Boolean.TRUE);
		}

		manutencaoPatrimonio.setIdUsuario(idUsuario);
		manutencaoPatrimonio = (ManutencaoPatrimonio) salvarOuAtualizar(manutencaoPatrimonio);

	}

	@Override
	@Transactional(readOnly = true)
	public PaginacaoDTO<ManutencaoPatrimonioDTO> listarPaginado(PaginacaoParams pp) throws DaoException {

		return manutencaoPatrimonioDao.listarPaginado(pp);
	}

	@Override
	@Transactional(readOnly = true)
	public List<ManutencaoPatrimonioDTO> listarCombo() {

		List<ManutencaoPatrimonio> lista = manutencaoPatrimonioDao.listarCombo();
		return ManutencaoPatrimonioConverter.getInstance().listEntidade2ListDto(lista);
	}

	@Override
	@Transactional(readOnly = false)
	public void excluir(Long id) {

		ManutencaoPatrimonio manutencaoPatrimonio = consultar(ManutencaoPatrimonio.class, id);
		manutencaoPatrimonio.setAtivo(Boolean.FALSE);
		salvarOuAtualizar(manutencaoPatrimonio);
	}

	@Override
	@Transactional(readOnly = true)
	public PaginacaoDTO<ManutencaoPatrimonioDTO> listarPaginado(PaginacaoParams pp, String numeroEtiqueta, String nome) throws DaoException {

		return manutencaoPatrimonioDao.listarPaginado(pp, numeroEtiqueta, nome);
	}

}
