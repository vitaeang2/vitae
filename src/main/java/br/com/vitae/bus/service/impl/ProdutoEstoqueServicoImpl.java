package br.com.vitae.bus.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.servico.ServiceImpl;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.conv.ProdutoEstoqueConverter;
import br.com.vitae.bus.dao.ProdutoEstoqueDAO;
import br.com.vitae.bus.dto.ProdutoEstoqueDTO;
import br.com.vitae.bus.entity.ProdutoEstoque;
import br.com.vitae.bus.service.IProdutoEstoqueServico;

@SuppressWarnings("serial")
@Service
public class ProdutoEstoqueServicoImpl extends ServiceImpl<ProdutoEstoque> implements IProdutoEstoqueServico {

	@Autowired
	ProdutoEstoqueDAO produtoEstoqueDAO;

	@Override
	@Transactional(readOnly = true)
	public ProdutoEstoque consultarProdutoEstoque(Long produtoId, Long empresaId, Long nucleoId) {

		return this.produtoEstoqueDAO.consultarProdutoEstoque(produtoId, empresaId, nucleoId);
	}

	@Override
	@Transactional(readOnly = true)
	public ProdutoEstoqueDTO consultarProdutoEstoqueDTO(Long produtoId, Long empresaId, Long nucleoId) {

		ProdutoEstoque produtoEstoque = consultarProdutoEstoque(produtoId, empresaId, nucleoId);
		return ProdutoEstoqueConverter.getInstance().entidade2Dto(produtoEstoque);
	}

	@Override
	@Transactional(readOnly = true)
	public long quantidadeProdutoBaixoEstoque(Long empresaId, Long nucleoId) throws DaoException {

		return produtoEstoqueDAO.quantidadeProdutoBaixoEstoque(empresaId, nucleoId);
	}

	@Override
	@Transactional(readOnly = true)
	public PaginacaoDTO<ProdutoEstoqueDTO> listarProdutoBaixoEstoquePaginado(PaginacaoParams pp, Long empresaId, Long nucleoId) throws DaoException {
		
		return produtoEstoqueDAO.listarProdutoBaixoEstoquePaginado(pp, empresaId, nucleoId);
	}

}
