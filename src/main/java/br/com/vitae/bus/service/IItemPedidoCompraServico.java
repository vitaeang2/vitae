package br.com.vitae.bus.service;

import java.util.List;

import br.com.arquitetura.servico.IService;
import br.com.vitae.bus.dto.ItemPedidoCompraDTO;
import br.com.vitae.bus.entity.ItemPedidoCompra;

public interface IItemPedidoCompraServico extends IService<ItemPedidoCompra> {

	ItemPedidoCompraDTO consultarIdDTO(Long id);

	List<ItemPedidoCompraDTO> listarPorPedidoDTO(Long idPedido);

	List<ItemPedidoCompra> listarPorPedido(Long idPedido);

}
