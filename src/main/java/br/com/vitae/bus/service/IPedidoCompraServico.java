package br.com.vitae.bus.service;

import java.util.List;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.servico.IService;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.dto.ItemPedidoCompraDTO;
import br.com.vitae.bus.dto.PedidoCompraDTO;
import br.com.vitae.bus.entity.PedidoCompra;

public interface IPedidoCompraServico extends IService<PedidoCompra> {

	PedidoCompraDTO consultarIdDTO(Long id);

	PaginacaoDTO<PedidoCompraDTO> listarPaginado(PaginacaoParams pp) throws DaoException;

	List<PedidoCompraDTO> listarCombo();

	PedidoCompraDTO salvarOuAtualizar(PedidoCompraDTO pedidoCompraDTO, List<ItemPedidoCompraDTO> listaItens, Long idCotacao);


	PaginacaoDTO<PedidoCompraDTO> listarPaginado(PaginacaoParams pp, Integer idTipoPedidoSelecionado, Long idEmpresa, Long idNucleo) throws DaoException;

	PedidoCompraDTO consultarComListaItens(Long id);
}
