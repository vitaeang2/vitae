package br.com.vitae.bus.service;

import java.util.List;

import br.com.arquitetura.servico.IService;
import br.com.vitae.bus.dto.UsuarioDTO;
import br.com.vitae.bus.entity.Usuario;

public interface IUsuarioServico extends IService<Usuario> {

	/**
	 * 
	 * 
	 * @param login
	 * @param senha
	 * @return
	 */
	UsuarioDTO findPorLoginSenha(String login, String senha);
	
	Usuario findPorColaborador(Long idColaborador);
	
	UsuarioDTO findPorColaboradorDTO(Long idColaborador);

	void alterarSenha(Long id, String novaSenha);

	boolean esqueceuSenha(String email);

	List<Usuario> listarEnviarEmailUsuarioCriado();
	
	void enviarEmail(String email, String senha, String nome) throws Exception;
	
	List<UsuarioDTO> listarCombo();
}
