package br.com.vitae.bus.service.impl;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.servico.ServiceImpl;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.conv.ProdutoConverter;
import br.com.vitae.bus.dao.ProdutoDAO;
import br.com.vitae.bus.dto.ProdutoDTO;
import br.com.vitae.bus.entity.Produto;
import br.com.vitae.bus.service.IProdutoServico;

@SuppressWarnings("serial")
@Service
public class ProdutoServicoImpl extends ServiceImpl<Produto> implements IProdutoServico {

	@Autowired
	private ProdutoDAO produtoDao;

	@Override
	@Transactional(readOnly = true)
	public ProdutoDTO consultarIdDTO(Long id) {

		Produto produto = consultar(Produto.class, id);
		return ProdutoConverter.getInstance().entidade2Dto(produto);
	}

	@Override
	@Transactional(readOnly = false)
	public void salvarOuAtualizar(Long idUsuario, ProdutoDTO produtoDTO) {

		Produto produto = ProdutoConverter.getInstance().dto2Entidade(produtoDTO);

		if (produto != null && produto.getId() == null) {

			produto.setDataCadastro(LocalDate.now());
			produto.setAtivo(Boolean.TRUE);
		}

		produto.setIdUsuario(idUsuario);
		produto = (Produto) salvarOuAtualizar(produto);

	}

	@Override
	@Transactional(readOnly = true)
	public PaginacaoDTO<ProdutoDTO> listarPaginado(PaginacaoParams pp) throws DaoException {

		return produtoDao.listarPaginado(pp);
	}

	@Override
	@Transactional(readOnly = true)
	public List<ProdutoDTO> listarCombo() {

		List<Produto> lista = produtoDao.listarCombo();
		return ProdutoConverter.getInstance().listEntidade2ListDto(lista);
	}

	@Override
	@Transactional(readOnly = false)
	public void excluir(Long id) {

		Produto produto = consultar(Produto.class, id);
		produto.setAtivo(Boolean.FALSE);
		salvarOuAtualizar(produto);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Produto> listarFiltroCotacao(String nome) {
		return produtoDao.listarFiltroCotacao(nome);
	}

//	@Override
//	public PaginacaoDTO<ProdutoDTO> listarEstoqueBaixoPaginado(PaginacaoParams pp, Long empresaId, Long nucleoId)
//			throws DaoException {
//		return produtoDao.listarEstoqueBaixoPaginado(pp, empresaId, nucleoId);
//	}

}
