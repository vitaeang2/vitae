package br.com.vitae.bus.service.impl;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.arquitetura.servico.ServiceImpl;
import br.com.vitae.bus.conv.ItemPedidoCompraConverter;
import br.com.vitae.bus.conv.MovimentoEstoqueConverter;
import br.com.vitae.bus.dao.MovimentoEstoqueDAO;
import br.com.vitae.bus.dto.ItemPedidoCompraDTO;
import br.com.vitae.bus.dto.MovimentoEstoqueDTO;
import br.com.vitae.bus.dto.PedidoCompraDTO;
import br.com.vitae.bus.entity.Colaborador;
import br.com.vitae.bus.entity.Empresa;
import br.com.vitae.bus.entity.ItemPedidoCompra;
import br.com.vitae.bus.entity.MovimentoEstoque;
import br.com.vitae.bus.entity.Nucleo;
import br.com.vitae.bus.entity.PedidoCompra;
import br.com.vitae.bus.entity.Produto;
import br.com.vitae.bus.entity.ProdutoEstoque;
import br.com.vitae.bus.enumerator.EnumItemPedidoCompra;
import br.com.vitae.bus.enumerator.EnumPedidoCompra;
import br.com.vitae.bus.enumerator.EnumTipoMovimentoEstoque;
import br.com.vitae.bus.service.IEmpresaServico;
import br.com.vitae.bus.service.IItemPedidoCompraServico;
import br.com.vitae.bus.service.IMovimentoEstoqueServico;
import br.com.vitae.bus.service.INucleoServico;
import br.com.vitae.bus.service.IPedidoCompraServico;
import br.com.vitae.bus.service.IProdutoEstoqueServico;
import br.com.vitae.bus.service.IProdutoServico;

@SuppressWarnings("serial")
@Service
public class MovimentoEstoqueServicoImpl extends ServiceImpl<MovimentoEstoque> implements IMovimentoEstoqueServico {

	@Autowired
	private MovimentoEstoqueDAO movimentoEstoqueDAO;

	@Autowired
	private IProdutoEstoqueServico produtoEstoqueServico;

	@Autowired
	private IEmpresaServico empresaServico;

	@Autowired
	private INucleoServico nucleoServico;

	@Autowired
	private IProdutoServico produtoServico;

	@Autowired
	private IPedidoCompraServico iPedidoCompraServico;

	@Autowired
	private IItemPedidoCompraServico iItemPedidoCompraServico;

	@Override
	@Transactional(readOnly = false)
	public void salvarOuAtualizar(MovimentoEstoqueDTO movimentoDTO) {

		MovimentoEstoque movimentoEstoque = MovimentoEstoqueConverter.getInstance().dto2Entidade(movimentoDTO);

		salvarOuAtualizarComProdutoEstoque(movimentoEstoque);
	}

	@Override
	@Transactional(readOnly = false)
	public MovimentoEstoque salvarOuAtualizarComProdutoEstoque(MovimentoEstoque movimentoEstoque) {

		if (movimentoEstoque.getId() == null) {
			movimentoEstoque.setData(LocalDate.now());
		}

		Produto produto = this.produtoServico.consultar(Produto.class, movimentoEstoque.getProduto().getId());

		ProdutoEstoque produtoEstoque = produtoEstoqueServico.consultarProdutoEstoque(produto.getId(), movimentoEstoque.getEmpresa() == null ? null : movimentoEstoque.getEmpresa().getId(), movimentoEstoque.getNucleo() == null ? null : movimentoEstoque.getNucleo().getId());

		if (produtoEstoque == null) {

			produtoEstoque = new ProdutoEstoque();
			produtoEstoque.setEmpresa(empresaServico.consultar(Empresa.class, movimentoEstoque.getEmpresa().getId()));

			if (movimentoEstoque.getNucleo() != null && movimentoEstoque.getNucleo().getId() != null) {
				produtoEstoque.setNucleo(nucleoServico.consultar(Nucleo.class, movimentoEstoque.getNucleo().getId()));
			}
			produtoEstoque.setProduto(produto);
			produtoEstoque.setQuantidade(BigDecimal.ZERO);
		}

		if (movimentoEstoque.getTipoMovimentoEstoque().getCodigo() == EnumTipoMovimentoEstoque.BALANCO.getCodigo()) {

			NumberFormat nf = new java.text.DecimalFormat("#,###,##0.00");

			String observacao = movimentoEstoque.getObservacao() == null ? "" : movimentoEstoque.getObservacao();

			movimentoEstoque.setObservacao(observacao + " Saldo: " + nf.format(movimentoEstoque.getQuantidadeMovimentada()) + " (Saldo anterior: " + nf.format(produtoEstoque.getQuantidade()) + ")");
		}

		produtoEstoque.setQuantidade(getSaldoEstoque(movimentoEstoque.getTipoMovimentoEstoque(), produtoEstoque, movimentoEstoque.getQuantidadeMovimentada()));

		movimentoEstoque.setSaldo(produtoEstoque.getQuantidade());

		movimentoEstoque.setProduto(produto);

		salvarOuAtualizar(movimentoEstoque);

		produtoEstoqueServico.salvarOuAtualizar(produtoEstoque);
		return movimentoEstoque;
	}

	@Transactional(readOnly = true)
	public BigDecimal getSaldoEstoque(EnumTipoMovimentoEstoque tipoMovimentacao, final ProdutoEstoque produtoEstoque, BigDecimal quantidadeMovimentada) {

		BigDecimal saldoEmEstoque = null;

		if (tipoMovimentacao == EnumTipoMovimentoEstoque.ENTRADA) {

			saldoEmEstoque = produtoEstoque.getQuantidade().add(quantidadeMovimentada);

		} else if (tipoMovimentacao == EnumTipoMovimentoEstoque.SAIDA) {

			saldoEmEstoque = produtoEstoque.getQuantidade().subtract(quantidadeMovimentada);

		} else if (tipoMovimentacao == EnumTipoMovimentoEstoque.BALANCO) {

			saldoEmEstoque = quantidadeMovimentada;
		}

		return saldoEmEstoque;
	}

	@Override
	@Transactional(readOnly = true)
	public List<MovimentoEstoqueDTO> getMovimentosEstoque(Long produtoId, Long nucleoId, Long empresaId) {

		List<MovimentoEstoque> movimentos = movimentoEstoqueDAO.getMovimentosEstoque(produtoId, nucleoId, empresaId);
		List<MovimentoEstoqueDTO> lista = MovimentoEstoqueConverter.getInstance().listEntidade2ListDto(movimentos);
		return lista;
	}

	@Override
	@Transactional(readOnly = false)
	public void entrarEstoque(PedidoCompraDTO pedidoCompraDTO, List<ItemPedidoCompraDTO> listaPedidoCompraDTO) {

		MovimentoEstoque movimentoEstoque = null;
		Colaborador colaborador = new Colaborador(pedidoCompraDTO.getColaborador().getId());
		Empresa empresa = new Empresa(pedidoCompraDTO.getEmpresa().getId());
		Nucleo nucleo = new Nucleo(pedidoCompraDTO.getNucleo().getId());

		atualizarPedidoCompra(pedidoCompraDTO);

		ItemPedidoCompra itemPedidoCompra = null;
		for (ItemPedidoCompraDTO itemPedidoCompraDTO : listaPedidoCompraDTO) {
			movimentoEstoque = new MovimentoEstoque();
			itemPedidoCompra = ItemPedidoCompraConverter.getInstance().dto2Entidade(itemPedidoCompraDTO);

			itemPedidoCompra.setStatus(EnumItemPedidoCompra.FINALIZADO_COMPRADO);
			iItemPedidoCompraServico.salvarOuAtualizar(itemPedidoCompra);

			if (itemPedidoCompra.getStatus() == EnumItemPedidoCompra.FINALIZADO_COMPRADO) {
				movimentoEstoque.setColaborador(colaborador);
				movimentoEstoque.setData(LocalDate.now());
				movimentoEstoque.setEmpresa(empresa);
				movimentoEstoque.setNucleo(nucleo);
				movimentoEstoque.setProduto(new Produto(itemPedidoCompraDTO.getProduto().getId()));
				movimentoEstoque.setQuantidadeMovimentada(itemPedidoCompraDTO.getQuantidade());
				movimentoEstoque.setValor(itemPedidoCompraDTO.getVlrUnitario());
				// movimentoEstoque.setSaldo(this.getSaldoEstoque(EnumTipoMovimentoEstoque.ENTRADA, produtoEstoque, quantidadeMovimentada));
				movimentoEstoque.setTipoMovimentoEstoque(EnumTipoMovimentoEstoque.ENTRADA);
				salvarOuAtualizarComProdutoEstoque(movimentoEstoque);
			}
		}
	}
	
	
	@Transactional(readOnly = false)
	public void entrarEstoque(Long idPedidoCompra, ItemPedidoCompraDTO itemPedidoCompraDTO) {

		PedidoCompra pedidoCompra = iPedidoCompraServico.consultar(PedidoCompra.class, idPedidoCompra);
		MovimentoEstoque movimentoEstoque = null;
		Colaborador colaborador = new Colaborador(pedidoCompra.getColaborador().getId());
		Empresa empresa = new Empresa(pedidoCompra.getEmpresa().getId());
		Nucleo nucleo = new Nucleo(pedidoCompra.getNucleo().getId());


		ItemPedidoCompra itemPedidoCompra = null;
		movimentoEstoque = new MovimentoEstoque();
		itemPedidoCompra = ItemPedidoCompraConverter.getInstance().dto2Entidade(itemPedidoCompraDTO);

		itemPedidoCompra.setStatus(EnumItemPedidoCompra.FINALIZADO_COMPRADO);
		iItemPedidoCompraServico.salvarOuAtualizar(itemPedidoCompra);

		if (itemPedidoCompra.getStatus() == EnumItemPedidoCompra.FINALIZADO_COMPRADO) {
			movimentoEstoque.setColaborador(colaborador);
			movimentoEstoque.setData(LocalDate.now());
			movimentoEstoque.setEmpresa(empresa);
			movimentoEstoque.setNucleo(nucleo);
			movimentoEstoque.setProduto(new Produto(itemPedidoCompraDTO.getProduto().getId()));
			movimentoEstoque.setQuantidadeMovimentada(itemPedidoCompraDTO.getQuantidade());
			movimentoEstoque.setValor(itemPedidoCompraDTO.getVlrUnitario());
			// movimentoEstoque.setSaldo(this.getSaldoEstoque(EnumTipoMovimentoEstoque.ENTRADA, produtoEstoque, quantidadeMovimentada));
			movimentoEstoque.setTipoMovimentoEstoque(EnumTipoMovimentoEstoque.ENTRADA);
			salvarOuAtualizarComProdutoEstoque(movimentoEstoque);
		}
		atualizarPedidoCompra(pedidoCompra);
	}

	private void atualizarPedidoCompra(PedidoCompra pedidoCompra) {
		
		List<ItemPedidoCompra> itensPedidoCompra = iItemPedidoCompraServico.listarPorPedido(pedidoCompra.getId());
		for (ItemPedidoCompra item : itensPedidoCompra) {
			if(item.getStatus() == EnumItemPedidoCompra.ABERTO){
				return;
			}
		}

		pedidoCompra.setStatus(EnumPedidoCompra.FINALIZADO);
		iPedidoCompraServico.salvarOuAtualizar(pedidoCompra);
	}
	
	private void atualizarPedidoCompra(PedidoCompraDTO pedidoCompraDTO) {

		PedidoCompra pedidoCompra = iPedidoCompraServico.consultar(PedidoCompra.class, pedidoCompraDTO.getId());
		List<ItemPedidoCompra> itensPedidoCompra = iItemPedidoCompraServico.listarPorPedido(pedidoCompra.getId());
		for (ItemPedidoCompra item : itensPedidoCompra) {
			if(item.getStatus() == EnumItemPedidoCompra.ABERTO){
				return;
			}
		}
		
		pedidoCompra.setStatus(EnumPedidoCompra.FINALIZADO);
		iPedidoCompraServico.salvarOuAtualizar(pedidoCompra);
	}

}
