package br.com.vitae.bus.service;

import java.util.List;

import br.com.arquitetura.servico.IService;
import br.com.vitae.bus.dto.CotacaoFornecedorDTO;
import br.com.vitae.bus.entity.CotacaoFornecedor;

public interface ICotacaoFornecedorServico extends IService<CotacaoFornecedor> {

	List<CotacaoFornecedor> listarEnviarEmailCotacao();

	List<CotacaoFornecedor> listarPorCotacao(Long idCotacao);

	List<CotacaoFornecedor> listarFornecedoresPorCotacao(Long idCotacao);
	
	List<CotacaoFornecedorDTO> listarFornecedoresPorPedidoCompra(Long idCotacao);
	
	CotacaoFornecedorDTO consultarCotacaoFornecedorPorToken(String token);
}
