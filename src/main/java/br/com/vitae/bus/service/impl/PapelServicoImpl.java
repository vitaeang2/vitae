package br.com.vitae.bus.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.arquitetura.servico.ServiceImpl;
import br.com.vitae.bus.dao.PapelDAO;
import br.com.vitae.bus.entity.Papel;
import br.com.vitae.bus.service.IPapelServico;

@SuppressWarnings("serial")
@Service
public class PapelServicoImpl extends ServiceImpl<Papel> implements IPapelServico {
	
	@Autowired
	private PapelDAO papelDao;

	@Override
	@Transactional(readOnly = false)
	public void removerLista(List<Papel> lista) {
		lista.forEach(papel -> remover(papel));
	}

	@Override
	@Transactional(readOnly = false)
	public Papel listarPorPerfilPagina(Long idPerfilUsuario, Long idPagina) {
		return papelDao.listarPorPerfilPagina(idPerfilUsuario, idPagina);
	}
}
