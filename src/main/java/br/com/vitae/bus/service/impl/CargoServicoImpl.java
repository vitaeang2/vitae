package br.com.vitae.bus.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.servico.ServiceImpl;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.conv.CargoConverter;
import br.com.vitae.bus.dao.CargoDAO;
import br.com.vitae.bus.dto.CargoDTO;
import br.com.vitae.bus.entity.Cargo;
import br.com.vitae.bus.service.ICargoServico;

@SuppressWarnings("serial")
@Service
public class CargoServicoImpl extends ServiceImpl<Cargo> implements ICargoServico {

	@Autowired
	private CargoDAO cargoDao;

	@Override
	@Transactional(readOnly = true)
	public CargoDTO consultarIdDTO(Long id) {

		Cargo cargo = consultar(Cargo.class, id);
		return CargoConverter.getInstance().entidade2Dto(cargo);
	}

	@Override
	@Transactional(readOnly = false)
	public void salvarOuAtualizar(CargoDTO cargoDTO) {

		Cargo cargo = CargoConverter.getInstance().dto2Entidade(cargoDTO);
		
		if (cargo.getId() == null) {
			
			cargo.setAtivo(Boolean.TRUE);
		}
		
		salvarOuAtualizar(cargo);
	}

	@Override
	@Transactional(readOnly = true)
	public PaginacaoDTO<CargoDTO> listarPaginado(PaginacaoParams pp) throws DaoException {

		 return cargoDao.listarPaginado(pp);
	}

	@Override
	@Transactional(readOnly = true)
	public List<CargoDTO> listarCombo() {

		List<Cargo> lista = cargoDao.listarCombo();
		return CargoConverter.getInstance().listEntidade2ListDto(lista);
	}
}
