package br.com.vitae.bus.service.impl;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.servico.ServiceImpl;
import br.com.arquitetura.vo.PaginacaoBuilder;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.conv.ItemPedidoCompraConverter;
import br.com.vitae.bus.conv.PedidoCompraConverter;
import br.com.vitae.bus.dao.PedidoCompraDAO;
import br.com.vitae.bus.dto.ItemPedidoCompraDTO;
import br.com.vitae.bus.dto.PedidoCompraDTO;
import br.com.vitae.bus.entity.Cotacao;
import br.com.vitae.bus.entity.ItemPedidoCompra;
import br.com.vitae.bus.entity.Nucleo;
import br.com.vitae.bus.entity.PedidoCompra;
import br.com.vitae.bus.enumerator.EnumItemPedidoCompra;
import br.com.vitae.bus.enumerator.EnumPedidoCompra;
import br.com.vitae.bus.enumerator.EnumStatusCotacao;
import br.com.vitae.bus.service.ICotacaoServico;
import br.com.vitae.bus.service.IItemPedidoCompraServico;
import br.com.vitae.bus.service.IPedidoCompraServico;

@SuppressWarnings("serial")
@Service
public class PedidoCompraServicoImpl extends ServiceImpl<PedidoCompra> implements IPedidoCompraServico {

	@Autowired
	private PedidoCompraDAO pedidoCompraDao;

	@Autowired
	private IItemPedidoCompraServico iItemPedidoCompraServico;

	@Autowired
	private ICotacaoServico iCotacaoServico;

	@Override
	@Transactional(readOnly = true)
	public PedidoCompraDTO consultarIdDTO(Long id) {

		PedidoCompra pedidoCompra = consultar(PedidoCompra.class, id);
		return PedidoCompraConverter.getInstance().entidade2Dto(pedidoCompra);
	}

	@Override
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public PaginacaoDTO<PedidoCompraDTO> listarPaginado(PaginacaoParams pp) throws DaoException {

		return PaginacaoBuilder.create(PedidoCompraConverter.getInstance(), PedidoCompra.class, pp).search();
	}

	@Override
	@Transactional(readOnly = true)
	public PaginacaoDTO<PedidoCompraDTO> listarPaginado(PaginacaoParams pp, Integer idTipoPedidoSelecionado, Long idEmpresa, Long idNucleo) throws DaoException {

		return pedidoCompraDao.listarPaginado(pp, idTipoPedidoSelecionado, idEmpresa, idNucleo);
	}

	@Override
	@Transactional(readOnly = true)
	public List<PedidoCompraDTO> listarCombo() {

		List<PedidoCompra> lista = pedidoCompraDao.listarCombo();
		return PedidoCompraConverter.getInstance().listEntidade2ListDto(lista);
	}

	@Override
	@Transactional(readOnly = false)
	public PedidoCompraDTO salvarOuAtualizar(PedidoCompraDTO pedidoCompraDTO, List<ItemPedidoCompraDTO> listaItensDTO, Long idCotacao) {

		Cotacao cotacao = iCotacaoServico.consultar(Cotacao.class, idCotacao);

		cotacao.setStatus(EnumStatusCotacao.FINALIZADO);

		iCotacaoServico.salvarOuAtualizar(cotacao);

		PedidoCompra pedidoCompra = PedidoCompraConverter.getInstance().dto2Entidade(pedidoCompraDTO);
		if (pedidoCompra.getId() == null) {
			pedidoCompra.setDataPedidoCompra(LocalDate.now());
		}

		pedidoCompra.setCotacao(new Cotacao(idCotacao));
		pedidoCompra.setStatus(EnumPedidoCompra.ABERTO);
		pedidoCompra.setNucleo(new Nucleo(cotacao.getNucleo().getId()));
		pedidoCompra = (PedidoCompra) salvarOuAtualizar(pedidoCompra);

		ItemPedidoCompra itemPedidoCompra = null;
		for (ItemPedidoCompraDTO itemPedidoCompraDTO : listaItensDTO) {
			itemPedidoCompra = ItemPedidoCompraConverter.getInstance().dto2Entidade(itemPedidoCompraDTO);
			itemPedidoCompra.setPedidoCompra(new PedidoCompra(pedidoCompra.getId()));
			itemPedidoCompra.setStatus(EnumItemPedidoCompra.ABERTO);
			itemPedidoCompra = (ItemPedidoCompra) iItemPedidoCompraServico.salvarOuAtualizar(itemPedidoCompra);
			itemPedidoCompraDTO.setId(itemPedidoCompra.getId());
		}

		pedidoCompraDTO = PedidoCompraConverter.getInstance().entidade2Dto(pedidoCompra);
		pedidoCompraDTO.setItensPedidoCompra(listaItensDTO);
		return pedidoCompraDTO;
	}

	@Override
	@Transactional(readOnly = true)
	public PedidoCompraDTO consultarComListaItens(Long id) {

		PedidoCompraDTO pedidoCompraDTO;

		PedidoCompra pedido = consultar(PedidoCompra.class, id);
		pedidoCompraDTO = PedidoCompraConverter.getInstance().entidade2Dto(pedido);
		pedidoCompraDTO.setItensPedidoCompra(iItemPedidoCompraServico.listarPorPedidoDTO(id));
		return pedidoCompraDTO;
	}

}
