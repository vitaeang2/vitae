package br.com.vitae.bus.service.impl;

import java.io.File;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.exception.NegocioException;
import br.com.arquitetura.servico.ServiceImpl;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.conv.QualidadeConverter;
import br.com.vitae.bus.dao.QualidadeDAO;
import br.com.vitae.bus.dto.QualidadeDTO;
import br.com.vitae.bus.entity.Qualidade;
import br.com.vitae.bus.service.IQualidadeServico;

@SuppressWarnings("serial")
@Service
public class QualidadeServicoImpl extends ServiceImpl<Qualidade> implements IQualidadeServico {

	@Autowired
	private QualidadeDAO qualidadeDAO;

	@Override
	@Transactional(readOnly = true)
	public QualidadeDTO consultarIdDTO(Long id) {

		Qualidade qualidade = consultar(Qualidade.class, id);
		QualidadeDTO qualidadeDTO = QualidadeConverter.getInstance().entidade2Dto(qualidade);
		return qualidadeDTO;
	}

	@Override
	@Transactional(readOnly = false)
	public QualidadeDTO salvarOuAtualizar(QualidadeDTO qualidadeDTO) throws NegocioException, DaoException {

		Qualidade qualidade = QualidadeConverter.getInstance().dto2Entidade(qualidadeDTO);

		if (qualidade.getId() == null) {
			qualidade.setAtivo(Boolean.TRUE);
		}
		qualidade = (Qualidade) salvarOuAtualizar(qualidade);
		return QualidadeConverter.getInstance().entidade2Dto(qualidade);

	}

	@Override
	@Transactional(readOnly = true)
	public PaginacaoDTO<QualidadeDTO> listarPaginado(PaginacaoParams pp, Long idEmpresa) throws DaoException {

		return qualidadeDAO.listarPaginado(pp, idEmpresa, null);
	}

	@Override
	@Transactional(readOnly = true)
	public List<QualidadeDTO> listarCombo() {

		List<Qualidade> lista = qualidadeDAO.listarCombo();
		return QualidadeConverter.getInstance().listEntidade2ListDto(lista);
	}

	@Override
	@Transactional(readOnly = true)
	public PaginacaoDTO<QualidadeDTO> listarPaginado(PaginacaoParams pp, Long idEmpresa, Long idNucleo)
			throws DaoException {

		return qualidadeDAO.listarPaginado(pp, idEmpresa, idNucleo);
	}

	@Override
	@Transactional(readOnly = false)
	public void excluirId(Long id) {

		Qualidade qualidade = consultar(Qualidade.class, id);
		qualidade.setAtivo(Boolean.FALSE);
		salvarOuAtualizar(qualidade);
	}

	public void removerFoto(String path) {
		File files = new File(path);
		for (File file : files.listFiles()) {
			file.delete();
		}
	}

	@Override
	public PaginacaoDTO<QualidadeDTO> listarPaginado(PaginacaoParams pp) throws DaoException {
		return qualidadeDAO.listarPaginado(pp);
	}
}
