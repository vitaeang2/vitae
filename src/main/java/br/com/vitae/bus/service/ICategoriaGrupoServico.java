package br.com.vitae.bus.service;

import java.util.List;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.servico.IService;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.dto.CategoriaGrupoDTO;
import br.com.vitae.bus.entity.CategoriaGrupo;

public interface ICategoriaGrupoServico extends IService<CategoriaGrupo> {

	CategoriaGrupoDTO consultarIdDTO(Long id);

	void salvarOuAtualizar(CategoriaGrupoDTO categoriaGrupoDTO);

	List<CategoriaGrupoDTO> listarCombo();

	PaginacaoDTO<CategoriaGrupoDTO> listarPaginado(PaginacaoParams pp) throws DaoException;
	
	public CategoriaGrupoDTO consultarCategoriaSemViculoGrupo(Long id) throws DaoException;
	
	public List<CategoriaGrupoDTO> listarPorGrupo(Long idGrupo);
}
