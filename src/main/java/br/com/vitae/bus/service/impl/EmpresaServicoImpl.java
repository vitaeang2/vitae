package br.com.vitae.bus.service.impl;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.servico.ServiceImpl;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.conv.EmpresaConverter;
import br.com.vitae.bus.dao.EmpresaDAO;
import br.com.vitae.bus.dto.EmpresaDTO;
import br.com.vitae.bus.entity.Empresa;
import br.com.vitae.bus.service.IEmpresaServico;

@SuppressWarnings("serial")
@Service
public class EmpresaServicoImpl extends ServiceImpl<Empresa> implements IEmpresaServico {

	@Autowired
	private EmpresaDAO empresaDAO;

	@Override
	@Transactional(readOnly = true)
	public EmpresaDTO consultarIdDTO(Long id) {

		Empresa empresa = consultar(Empresa.class, id);
		return EmpresaConverter.getInstance().entidade2Dto(empresa);
	}

	@Override
	@Transactional(readOnly = false)
	public void salvarOuAtualizar(EmpresaDTO empresaDTO) {

		Empresa empresa = EmpresaConverter.getInstance().dto2Entidade(empresaDTO);
		if (empresa != null && empresa.getId() == null) {
			empresa.setAtivo(true);
			empresa.setDataCadastro(LocalDate.now());
		}
		salvarOuAtualizar(empresa);
	}

	@Override
	@Transactional(readOnly = true)
	public PaginacaoDTO<EmpresaDTO> listarPaginado(PaginacaoParams pp) throws DaoException {

		return  empresaDAO.listarPaginado(pp);
	}

	@Override
	@Transactional(readOnly = true)
	public List<EmpresaDTO> listarCombo() {

		List<Empresa> lista = empresaDAO.listarCombo();
		return EmpresaConverter.getInstance().listEntidade2ListDto(lista);
	}
}
