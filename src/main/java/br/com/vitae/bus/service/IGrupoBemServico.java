package br.com.vitae.bus.service;

import java.util.List;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.servico.IService;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.dto.GrupoBemDTO;
import br.com.vitae.bus.entity.GrupoBem;

public interface IGrupoBemServico extends IService<GrupoBem> {

	GrupoBemDTO consultarIdDTO(Long id);

	void salvarOuAtualizar(GrupoBemDTO grupoBemDTO);

	List<GrupoBemDTO> listarCombo();
	
	public PaginacaoDTO<GrupoBemDTO> listarPaginado(PaginacaoParams pp) throws DaoException;
	
	public GrupoBemDTO consultarGrupoBemSemViculoPatrimonio(Long id) throws DaoException;
}
