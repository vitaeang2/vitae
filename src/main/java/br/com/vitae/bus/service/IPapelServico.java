package br.com.vitae.bus.service;

import java.util.List;

import br.com.arquitetura.servico.IService;
import br.com.vitae.bus.entity.Papel;

public interface IPapelServico extends IService<Papel> {

	void removerLista(List<Papel> lista);

	Papel listarPorPerfilPagina(Long idPerfilUsuario, Long idPagina);
}
