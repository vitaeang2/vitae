package br.com.vitae.bus.service;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.servico.IService;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.dto.ProdutoEstoqueDTO;
import br.com.vitae.bus.entity.ProdutoEstoque;

public interface IProdutoEstoqueServico extends IService<ProdutoEstoque> {

	ProdutoEstoque consultarProdutoEstoque(Long produtoId, Long empresaId, Long nucleoId);
	ProdutoEstoqueDTO consultarProdutoEstoqueDTO(Long produtoId, Long empresaId, Long nucleoId);
	long quantidadeProdutoBaixoEstoque(Long empresaId, Long nucleoId) throws DaoException;
	PaginacaoDTO<ProdutoEstoqueDTO> listarProdutoBaixoEstoquePaginado(PaginacaoParams pp, Long empresaId, Long nucleoId) throws DaoException;
}
