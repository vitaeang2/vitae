package br.com.vitae.bus.service;

import java.util.List;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.servico.IService;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.dto.ComponenteItemPatrimonioDTO;
import br.com.vitae.bus.entity.ComponenteItemPatrimonio;

public interface IComponenteItemPatrimonioServico extends IService<ComponenteItemPatrimonio> {

	ComponenteItemPatrimonioDTO consultarIdDTO(Long id);

	void salvarOuAtualizar(ComponenteItemPatrimonioDTO componenteItemPatrimonioDTO);

	List<ComponenteItemPatrimonioDTO> listarCombo();
	
	public PaginacaoDTO<ComponenteItemPatrimonioDTO> listarPaginado(PaginacaoParams pp) throws DaoException;
	
	public List<ComponenteItemPatrimonioDTO> listarPorPatrimonio(Long idPatrimonio);
}
