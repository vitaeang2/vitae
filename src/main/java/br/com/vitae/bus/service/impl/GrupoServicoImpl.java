package br.com.vitae.bus.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.servico.ServiceImpl;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.conv.CategoriaGrupoConverter;
import br.com.vitae.bus.conv.GrupoConverter;
import br.com.vitae.bus.dao.GrupoDAO;
import br.com.vitae.bus.dto.CategoriaGrupoDTO;
import br.com.vitae.bus.dto.GrupoDTO;
import br.com.vitae.bus.entity.CategoriaGrupo;
import br.com.vitae.bus.entity.ComponenteItemPatrimonio;
import br.com.vitae.bus.entity.Grupo;
import br.com.vitae.bus.entity.Patrimonio;
import br.com.vitae.bus.service.ICategoriaGrupoServico;
import br.com.vitae.bus.service.IGrupoServico;

@SuppressWarnings("serial")
@Service
public class GrupoServicoImpl extends ServiceImpl<Grupo> implements IGrupoServico {

	@Autowired
	private GrupoDAO grupoDao;

	@Autowired
	private ICategoriaGrupoServico iCategoriaGrupoServico;

	@Override
	@Transactional(readOnly = true)
	public GrupoDTO consultarIdDTO(Long id) {

		GrupoDTO grupoDTO;
		Grupo grupo = consultar(Grupo.class, id);
		grupoDTO = GrupoConverter.getInstance().entidade2Dto(grupo);
		grupoDTO.setCategoriaGrupo(iCategoriaGrupoServico.listarPorGrupo(grupo.getId()));
		return grupoDTO;

	}

	@Override
	@Transactional(readOnly = false)
	public void salvarOuAtualizar(GrupoDTO grupoDTO) {

		Grupo grupo = GrupoConverter.getInstance().dto2Entidade(grupoDTO);
		if (grupo.getId() == null) {
			grupo.setAtivo(Boolean.TRUE);
		}
		grupo = (Grupo) salvarOuAtualizar(grupo);

		CategoriaGrupo item = null;
		for (CategoriaGrupoDTO itemCategoriaGrupoDTO : grupoDTO.getCategoriaGrupo()) {
			item = CategoriaGrupoConverter.getInstance().dto2Entidade(itemCategoriaGrupoDTO);
			item.setGrupo(new Grupo(grupo.getId()));
			item.setAtivo(Boolean.TRUE);
			item = (CategoriaGrupo) iCategoriaGrupoServico.salvarOuAtualizar(item);
		}
	}

	@Override
	@Transactional(readOnly = true)
	public PaginacaoDTO<GrupoDTO> listarPaginado(PaginacaoParams pp) throws DaoException {

		return grupoDao.listarPaginado(pp);
	}

	@Override
	@Transactional(readOnly = true)
	public List<GrupoDTO> listarCombo() {

		List<Grupo> lista = grupoDao.listarCombo();
		return GrupoConverter.getInstance().listEntidade2ListDto(lista);
	}

	@Override
	public GrupoDTO consultarGrupoSemViculoPatrimonio(Long id) throws DaoException {
		return grupoDao.consultarGrupoSemViculoPatrimonio(id);
	}
}
