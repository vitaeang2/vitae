package br.com.vitae.bus.service;

import java.util.List;

import br.com.arquitetura.servico.IService;
import br.com.vitae.bus.dto.ItemCotacaoDTO;
import br.com.vitae.bus.entity.ItemCotacao;

public interface IItemCotacaoServico extends IService<ItemCotacao> {

	List<ItemCotacao> listarPorCotacaoFornecedor(Long idCF);

	List<ItemCotacaoDTO> listarProdutosSelecionados(Long idCotacao);

	List<ItemCotacaoDTO> listarItensPorCotacaoFornecedor(Long idCotacaoFornecedor);
}
