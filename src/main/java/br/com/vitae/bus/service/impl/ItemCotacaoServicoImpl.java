package br.com.vitae.bus.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.arquitetura.servico.ServiceImpl;
import br.com.vitae.bus.conv.ItemCotacaoConverter;
import br.com.vitae.bus.dao.ItemCotacaoDAO;
import br.com.vitae.bus.dto.ItemCotacaoDTO;
import br.com.vitae.bus.entity.ItemCotacao;
import br.com.vitae.bus.service.IItemCotacaoServico;

@SuppressWarnings("serial")
@Service
public class ItemCotacaoServicoImpl extends ServiceImpl<ItemCotacao> implements IItemCotacaoServico {

	@Autowired
	private ItemCotacaoDAO iItemCotacaoDAO;

	@Override
	@Transactional(readOnly = true)
	public List<ItemCotacao> listarPorCotacaoFornecedor(Long idCF) {

		return iItemCotacaoDAO.listarPorCotacaoFornecedor(idCF);
	}

	@Override
	@Transactional(readOnly = true)
	public List<ItemCotacaoDTO> listarProdutosSelecionados(Long idCotacao) {

		List<ItemCotacao> lista = iItemCotacaoDAO.listarProdutosSelecionados(idCotacao);
		return ItemCotacaoConverter.getInstance().listEntidade2ListDto(lista);
	}

	@Override
	@Transactional(readOnly = true)
	public List<ItemCotacaoDTO> listarItensPorCotacaoFornecedor(Long idCotacaoFornecedor) {

		List<ItemCotacao> lista = iItemCotacaoDAO.listarItensPorCotacaoFornecedor(idCotacaoFornecedor);
		return ItemCotacaoConverter.getInstance().listEntidade2ListDto(lista);
	}

}
