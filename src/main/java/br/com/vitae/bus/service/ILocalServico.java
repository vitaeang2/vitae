package br.com.vitae.bus.service;

import java.util.List;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.servico.IService;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.dto.LocalDTO;
import br.com.vitae.bus.entity.Local;

public interface ILocalServico extends IService<Local> {

	LocalDTO consultarIdDTO(Long id);

	void salvarOuAtualizar(LocalDTO localDTO);

	List<LocalDTO> listarCombo();
	
	public PaginacaoDTO<LocalDTO> listarPaginado(PaginacaoParams pp) throws DaoException;
	
	public LocalDTO consultarLocalSemViculoPatrimonio(Long id) throws DaoException;
}
