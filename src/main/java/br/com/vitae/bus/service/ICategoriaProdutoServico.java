package br.com.vitae.bus.service;

import java.util.List;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.servico.IService;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.dto.CategoriaProdutoDTO;
import br.com.vitae.bus.entity.CategoriaProduto;

public interface ICategoriaProdutoServico extends IService<CategoriaProduto> {

	CategoriaProdutoDTO consultarIdDTO(Long id);

	void salvarOuAtualizar(CategoriaProdutoDTO categoriaProdutoDTO);

	List<CategoriaProdutoDTO> listarCombo();

	PaginacaoDTO<CategoriaProdutoDTO> listarPaginado(PaginacaoParams pp) throws DaoException;
	
	public CategoriaProdutoDTO consultarCategoriaSemViculoProduto(Long id) throws DaoException;
}
