package br.com.vitae.bus.service.impl;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.exception.NegocioException;
import br.com.arquitetura.servico.MailSenderService;
import br.com.arquitetura.servico.ServiceImpl;
import br.com.arquitetura.util.IsNullUtil;
import br.com.arquitetura.util.PasswordBuilder;
import br.com.arquitetura.util.Utils;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.conv.ColaboradorConverter;
import br.com.vitae.bus.dao.ColaboradorDAO;
import br.com.vitae.bus.dto.ColaboradorDTO;
import br.com.vitae.bus.dto.DocumentoDTO;
import br.com.vitae.bus.entity.Colaborador;
import br.com.vitae.bus.entity.Documento;
import br.com.vitae.bus.entity.PerfilUsuario;
import br.com.vitae.bus.entity.Usuario;
import br.com.vitae.bus.service.IColaboradorServico;
import br.com.vitae.bus.service.IDocumentoServico;
import br.com.vitae.bus.service.IPerfilAcessoServico;
import br.com.vitae.bus.service.IUsuarioServico;
import br.com.vitae.bus.util.ArquivoUtil;
import net.coobird.thumbnailator.Thumbnails;

@SuppressWarnings("serial")
@Service
public class ColaboradorServicoImpl extends ServiceImpl<Colaborador> implements IColaboradorServico {

	private static final String RESOURCES_COLABORADOR = "resources/colaborador/";

	private static final String TEMPLATE_CRIAR_SENHA = "criarSenha.vm";

	private static final int THUMB_WIDTH = 64;

	private static final int THUMB_HEIGHT = 64;
	
	@Autowired
	private IDocumentoServico documentoServico;

	@Autowired
	private ColaboradorDAO colaboradorDAO;

	@Autowired
	private IPerfilAcessoServico iPerfilAcessoServico;

	@Autowired
	private IUsuarioServico iUsuarioServico;

	@Autowired
	private IDocumentoServico iDocumentoServico;

	@Autowired
	private MailSenderService mailSenderService;

	@Override
	@Transactional(readOnly = true)
	public ColaboradorDTO consultarIdDTO(Long id, String caminho, String url) {

		Colaborador colaborador = consultar(Colaborador.class, id);
		ColaboradorDTO colaboradorDTO = ColaboradorConverter.getInstance().entidade2Dto(colaborador);
		File file = new File(caminho);
		boolean temFoto = file.exists() && file.list().length > 0;
		colaboradorDTO.setTemFoto(temFoto);
		if (temFoto) {
			colaboradorDTO.setCaminhoArquivo(String.format("%s/%s/%s/foto/%s", url, RESOURCES_COLABORADOR, id, file.list()[0]));
		}
		return colaboradorDTO;
	}

	@Override
	@Transactional(readOnly = false)
	public ColaboradorDTO salvarOuAtualizar(ColaboradorDTO colaboradorDTO, Long idPerfil, String caminho) throws NegocioException, DaoException {

		Usuario usuario = null;
		PerfilUsuario perfil = null;

		if (validaPerfil(idPerfil)) {

			perfil = iPerfilAcessoServico.consultar(PerfilUsuario.class, idPerfil);
		}

		Colaborador colaborador = ColaboradorConverter.getInstance().dto2Entidade(colaboradorDTO);

		// Validar data adminissão < demissão
		if (colaborador.getAdmissao() != null && colaborador.getDemissao() != null) {

			if (colaborador.getAdmissao().isAfter(colaborador.getDemissao())) {

				throw new NegocioException("A data da demissão deve ser maior que a data de admissão");
			}
		}

		// Validando e-mail
		if (IsNullUtil.isNullOrEmpty(colaborador.getId()) && !IsNullUtil.isNullOrEmpty(colaborador.getPessoa().getEmail())) {

			if (possuiColaboradorPorEmail(colaborador.getPessoa().getEmail())) {

				throw new NegocioException("O e-mail informado já está cadastrado!");
			}
		}

		if (colaborador != null && colaborador.getId() == null) {

			colaborador = salvarColaborador(perfil, colaborador);

		} else {

			usuario = iUsuarioServico.findPorColaborador(colaborador.getId());

			if (usuario != null) {

				colaborador = atualizarColaboradorUsuario(usuario, perfil, colaborador);

			} else {

				colaborador = atualizarColaboradorSalvarUsuario(perfil, colaborador);
			}
		}

		salvarImagem(colaborador.getId(), colaboradorDTO.getNomeArquivo(), colaboradorDTO.getTipoArquivo(), caminho);

		return ColaboradorConverter.getInstance().entidade2Dto(colaborador);

	}

	private void salvarImagem(Long id, String nomeArquivo, String tipoArquivo, String caminho) {

		if (nomeArquivo == null || nomeArquivo.equals("")) {
			return;
		}

		File fileTemp = new File(caminho + "/temp/" + nomeArquivo);
		File fileDest = new File(caminho + "/" + id + "." + tipoArquivo.substring(tipoArquivo.indexOf("/") + 1));
		try {
			copyFile(fileTemp, fileDest);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void copyFile(File source, File destination) throws IOException {

		if (destination.exists()) {
			destination.delete();
		}

		Thumbnails.of(source).size(THUMB_HEIGHT, THUMB_WIDTH).toFile(destination);

		// FileChannel sourceChannel = null;
		// FileChannel destinationChannel = null;
		// try {
		// sourceChannel = new FileInputStream(source).getChannel();
		// destinationChannel = new FileOutputStream(destination).getChannel();
		// sourceChannel.transferTo(0, sourceChannel.size(), destinationChannel);
		// } finally {
		// if (sourceChannel != null && sourceChannel.isOpen())
		// sourceChannel.close();
		// if (destinationChannel != null && destinationChannel.isOpen())
		// destinationChannel.close();
		// }
	}

	// private void atualizarColaboradorSalvarUsuario(PerfilUsuario perfil, Colaborador colaborador) {
	//
	// salvarUsuario(perfil, colaborador);
	//
	// }

	private Colaborador atualizarColaboradorUsuario(Usuario usuario, PerfilUsuario perfil, Colaborador colaborador) {

		usuario.setPerfil(perfil);
		Colaborador colab = (Colaborador) salvarOuAtualizar(colaborador);
		usuario.setColaborador(colab);
		iUsuarioServico.salvarOuAtualizar(usuario);

		return colab;
	}

	@Transactional(readOnly = false)
	private Colaborador salvarColaborador(PerfilUsuario perfil, Colaborador colaborador) {

		if (colaborador.getPessoa() != null) {

			colaborador.getPessoa().setDataCadastro(LocalDate.now());
			colaborador.setAtivo(true);
		}

		return atualizarColaboradorSalvarUsuario(perfil, colaborador);
	}

	private Colaborador atualizarColaboradorSalvarUsuario(PerfilUsuario perfil, Colaborador colaborador) {

		Usuario usuario;
		// PerfilUsuario perfil = iPerfilAcessoServico.consultar(PerfilUsuario.class, idPerfil);
		usuario = montarUsuario(colaborador, perfil);

		// TODO - Enviar email
		Colaborador colabNovo = (Colaborador) salvarOuAtualizar(colaborador);
		if (usuario != null) {
			usuario.setColaborador(colabNovo);
			iUsuarioServico.salvarOuAtualizar(usuario);
		}

		return colabNovo;
	}

	private boolean validaPerfil(Long idPerfil) {

		return idPerfil != null && idPerfil.longValue() > 0;
	}

	private Usuario montarUsuario(Colaborador colaborador, PerfilUsuario perfil) {

		if (perfil == null) {
			return null;
		}

		String nome = colaborador.getPessoa().getNome();
		String email = colaborador.getPessoa().getEmail();
		String senha = PasswordBuilder.gerarSenha();

		Usuario usuario = new Usuario();
		usuario.setPerfil(perfil);
		usuario.setPrimeiroAcesso(true);
		usuario.setSenha(Utils.md5(senha));
		usuario.setEmailEnviado(Boolean.TRUE);

		try {
			enviarEmail(nome, email, senha);
		} catch (Exception e) {
			usuario.setEmailEnviado(Boolean.FALSE);
			e.printStackTrace();
		}
		return usuario;
	}

	private void enviarEmail(String nome, String email, String senha) throws Exception {

		mailSenderService.setTemplateName(TEMPLATE_CRIAR_SENHA);
		HashMap<String, Object> map = new HashMap<>();
		map.put("nome", nome);
		map.put("email", email);
		map.put("senha", senha);
		mailSenderService.sendHtmlWithTemplate(email, "Cria��o de Usuário", map);
	}

	@Override
	@Transactional(readOnly = true)
	public PaginacaoDTO<ColaboradorDTO> listarPaginado(PaginacaoParams pp, Long idEmpresa) throws DaoException {

		return colaboradorDAO.listarPaginado(pp, idEmpresa, null);
	}

	@Override
	@Transactional(readOnly = true)
	public List<ColaboradorDTO> listarCombo() {

		List<Colaborador> lista = colaboradorDAO.listarCombo();
		return ColaboradorConverter.getInstance().listEntidade2ListDto(lista);
	}

	@Override
	public Colaborador getColaboradorLogado() {

		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public Colaborador consultarPorIdUsuario(Long idUsuario) {

		return colaboradorDAO.consultarPorIdUsuario(idUsuario);
	}

	@Override
	@Transactional(readOnly = true)
	public PaginacaoDTO<ColaboradorDTO> listarPaginado(PaginacaoParams pp, Long idEmpresa, Long idNucleo) throws DaoException {

		// if (idNucleo != null) {
		return colaboradorDAO.listarPaginado(pp, idEmpresa, idNucleo);
		// }

		// return colaboradorDAO.listarPaginado(pp, idEmpresa, null);

	}

	@Override
	@Transactional(readOnly = true)
	public boolean possuiColaboradorPorEmail(String email) throws DaoException {

		return colaboradorDAO.possuiColaboradorPorEmail(email);
	}

	@Override
	@Transactional(readOnly = true)
	public boolean possuiColaboradorPorCnj(String cnj) throws DaoException {

		return colaboradorDAO.possuiColaboradorPorCnj(cnj);
	}

	@Override
	@Transactional(readOnly = false)
	public void excluirId(Long id) {

		Colaborador colaborador = consultar(Colaborador.class, id);
		
		List<Documento> documentos = documentoServico.listar(id);
		for (Documento documento : documentos) {
			documento.setAtivo(Boolean.FALSE);
			documentoServico.salvarOuAtualizar(documento);
		}
		
//		String path = ArquivoUtil.criarPasta(request.getServletContext().getRealPath("resources/colaborador"),
//				id.toString());
//		
//		ArquivoUtil.remover(path);
		colaborador.setAtivo(Boolean.FALSE);
		salvarOuAtualizar(colaborador);
	}

	@Override
	public void criarFoto(HttpServletRequest request, String id) throws IOException, ServletException {

		String path = ArquivoUtil.criarPastaFoto(request.getServletContext().getRealPath("resources/colaborador"), id);
		ArquivoUtil.remover(path);
		File file = new File(path);
		if(!file.exists()){
			file.mkdirs();
		}
		for (Part part : request.getParts()) {
			String fileName = ArquivoUtil.extractFileName(part);
			part.write(path + File.separator + fileName);
		}
	}
	
	@Override
	public void removerFoto(HttpServletRequest request, String id) throws IOException, ServletException {
		String path = ArquivoUtil.criarPastaFoto(request.getServletContext().getRealPath("resources/colaborador/"), id);
		ArquivoUtil.remover(path);
	}
	
	@Override
	@Transactional(readOnly = false)
	public void criarDocumento(HttpServletRequest request, DocumentoDTO documentoDTO) throws IOException, ServletException {
		
		Documento documento = iDocumentoServico.salvarOuAtualizar(documentoDTO);
		
		String idStr = documento.getId().toString();
		
		String path = ArquivoUtil.criarPastaDocumento(request.getServletContext().getRealPath("resources/colaborador"), documentoDTO.getIdColaborador().toString());
		ArquivoUtil.removerFoto(path, idStr);
		for (Part part : request.getParts()) {
			String fileName = ArquivoUtil.extractFileName(part);
			String tipo = fileName.substring(fileName.lastIndexOf("."));
			part.write(path + File.separator + idStr + tipo);
		}
	}


}
