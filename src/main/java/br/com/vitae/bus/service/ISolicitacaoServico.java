package br.com.vitae.bus.service;

import java.util.List;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.exception.NegocioException;
import br.com.arquitetura.servico.IService;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.dto.SolicitacaoDTO;
import br.com.vitae.bus.entity.Solicitacao;

public interface ISolicitacaoServico extends IService<Solicitacao> {

	SolicitacaoDTO consultarIdDTO(Long id);

	SolicitacaoDTO salvarOuAtualizar(SolicitacaoDTO solicitacaoDTO) throws NegocioException, DaoException;


	List<SolicitacaoDTO> listarCombo();
	
	PaginacaoDTO<SolicitacaoDTO> listarPaginado(PaginacaoParams pp, Long idNucleo, Long idLocal, Long idStatusSolicitacao) throws DaoException;
	
	void excluirId(Long id);
	
	PaginacaoDTO<SolicitacaoDTO> listarPaginadoSolicitacoesAbertas(PaginacaoParams pp, Long idEmpresa, Long idNucleo, Long idLocal) throws DaoException;
	
	long quantidadeSolicitacoesAbertas(Long idEmpresa, Long idNucleo) throws DaoException;
}
