package br.com.vitae.bus.service;

import java.util.List;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.servico.IService;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.dto.FabricanteDTO;
import br.com.vitae.bus.entity.Fabricante;

public interface IFabricanteServico extends IService<Fabricante> {

	FabricanteDTO consultarIdDTO(Long id);

	void salvarOuAtualizar(FabricanteDTO fabricanteDTO);

	List<FabricanteDTO> listarCombo();
	
	public FabricanteDTO consultarFabricanteSemViculoProduto(Long id) throws DaoException;
	
	public PaginacaoDTO<FabricanteDTO> listarPaginado(PaginacaoParams pp) throws DaoException;
}
