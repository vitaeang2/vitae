package br.com.vitae.bus.service;

import java.util.List;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.servico.IService;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.dto.TipoSolicitacaoDTO;
import br.com.vitae.bus.entity.TipoSolicitacao;

public interface ITipoSolicitacaoServico extends IService<TipoSolicitacao> {

	TipoSolicitacaoDTO consultarIdDTO(Long id);

	void salvarOuAtualizar(TipoSolicitacaoDTO tipoSolicitacaoDTO);

	List<TipoSolicitacaoDTO> listarCombo();

	PaginacaoDTO<TipoSolicitacaoDTO> listarPaginado(PaginacaoParams pp) throws DaoException;
	
	public TipoSolicitacaoDTO consultarTipoSolicitacaoSemViculoSolicitacao(Long id) throws DaoException;
}
