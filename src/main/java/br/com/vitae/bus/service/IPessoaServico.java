package br.com.vitae.bus.service;

import br.com.arquitetura.servico.IService;
import br.com.vitae.bus.entity.Pessoa;

public interface IPessoaServico extends IService<Pessoa> {

	void atualizarEmail(Long id, String email);
}
