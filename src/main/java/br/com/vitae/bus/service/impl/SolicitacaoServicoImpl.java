package br.com.vitae.bus.service.impl;

import java.io.File;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.exception.NegocioException;
import br.com.arquitetura.servico.ServiceImpl;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.conv.SolicitacaoConverter;
import br.com.vitae.bus.dao.SolicitacaoDAO;
import br.com.vitae.bus.dto.SolicitacaoDTO;
import br.com.vitae.bus.entity.Solicitacao;
import br.com.vitae.bus.service.ISolicitacaoServico;

@SuppressWarnings("serial")
@Service
public class SolicitacaoServicoImpl extends ServiceImpl<Solicitacao> implements ISolicitacaoServico {

	@Autowired
	private SolicitacaoDAO solicitacaoDAO;

	@Override
	@Transactional(readOnly = true)
	public SolicitacaoDTO consultarIdDTO(Long id) {

		Solicitacao solicitacao = consultar(Solicitacao.class, id);
		SolicitacaoDTO solicitacaoDTO = SolicitacaoConverter.getInstance().entidade2Dto(solicitacao);
		return solicitacaoDTO;
	}

	@Override
	@Transactional(readOnly = false)
	public SolicitacaoDTO salvarOuAtualizar(SolicitacaoDTO solicitacaoDTO) throws NegocioException, DaoException {

		Solicitacao solicitacao = SolicitacaoConverter.getInstance().dto2Entidade(solicitacaoDTO);

		// Validar data adminissão < demissão
		if (solicitacao.getAbertura() != null && solicitacao.getPrevisaoFinalizacao() != null) {

			if (solicitacao.getAbertura().isAfter(solicitacao.getPrevisaoFinalizacao())) {
				throw new NegocioException("A Data de Previsão de Finalização deve ser maior que a data de Abertura");
			}
		}

		if (solicitacao.getStatusSolicitacao().getCodigo() == 2 && solicitacao.getTermino() == null) {
			throw new NegocioException(
					"A Data de termino é obrigatória quando o Status da Solicitação for igual a Finalizado");
		}

		if (solicitacao.getId() == null) {
			solicitacao.setAtivo(Boolean.TRUE);
		}
		solicitacao = (Solicitacao) salvarOuAtualizar(solicitacao);
		return SolicitacaoConverter.getInstance().entidade2Dto(solicitacao);
	}

	@Override
	@Transactional(readOnly = true)
	public PaginacaoDTO<SolicitacaoDTO> listarPaginado(PaginacaoParams pp, Long idNucleo, Long idLocal,
			Long idStatusSolicitacao) throws DaoException {

		return solicitacaoDAO.listarPaginado(pp, idNucleo, idLocal, idStatusSolicitacao);
	}

	@Override
	@Transactional(readOnly = true)
	public List<SolicitacaoDTO> listarCombo() {

		List<Solicitacao> lista = solicitacaoDAO.listarCombo();
		return SolicitacaoConverter.getInstance().listEntidade2ListDto(lista);
	}

	@Override
	@Transactional(readOnly = false)
	public void excluirId(Long id) {

		Solicitacao solicitacao = consultar(Solicitacao.class, id);
		solicitacao.setAtivo(Boolean.FALSE);
		salvarOuAtualizar(solicitacao);
	}

	@Override
	@Transactional(readOnly = true)
	public PaginacaoDTO<SolicitacaoDTO> listarPaginadoSolicitacoesAbertas(PaginacaoParams pp, Long idEmpresa,
			Long idNucleo, Long idLocal) throws DaoException {

		return solicitacaoDAO.listarPaginadoSolicitacoesAbertas(pp, idEmpresa, idNucleo, idLocal);
	}

	@Override
	@Transactional(readOnly = false)
	public long quantidadeSolicitacoesAbertas(Long idEmpresa, Long idNucleo) throws DaoException {

		return solicitacaoDAO.quantidadeSolicitacoesAbertas(idEmpresa, idNucleo);
	}

	public void removerFoto(String path) {

		File files = new File(path);
		for (File file : files.listFiles()) {
			file.delete();
		}
	}
}
