package br.com.vitae.bus.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.servico.IService;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.dto.DocumentoDTO;
import br.com.vitae.bus.entity.Documento;

public interface IDocumentoServico extends IService<Documento> {

	Documento salvarOuAtualizar(DocumentoDTO documentoDTO);

	PaginacaoDTO<DocumentoDTO> listarPaginado(PaginacaoParams pp, Long idColaborador) throws DaoException;

	List<DocumentoDTO> listarDTO(HttpServletRequest request, Long idColaborador);

	DocumentoDTO consultarDTO(HttpServletRequest request, Long id);
	
	DocumentoDTO removerArquivo(HttpServletRequest request, Long id, Long idColaborador);

	void remover(HttpServletRequest request, Long id, Long idColaborador);

	List<Documento> listar(Long id);

}
