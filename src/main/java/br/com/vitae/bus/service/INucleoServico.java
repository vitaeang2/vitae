package br.com.vitae.bus.service;

import java.util.List;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.servico.IService;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.dto.NucleoDTO;
import br.com.vitae.bus.entity.Nucleo;

public interface INucleoServico extends IService<Nucleo> {

	NucleoDTO consultarIdDTO(Long id);

	void salvarOuAtualizar(NucleoDTO nucleoDTO);

	PaginacaoDTO<NucleoDTO> listarPaginado(PaginacaoParams pp) throws DaoException;

	List<NucleoDTO> listarComboPorIdEmpresa(Long idEmpresa);

	Nucleo consultarPorColaborador(Long id);
	
	public boolean possuiNucleoPorColaborador(Long idColaborador) throws DaoException;

	List<NucleoDTO> listarCombo();
}