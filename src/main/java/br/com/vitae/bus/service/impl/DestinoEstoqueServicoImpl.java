package br.com.vitae.bus.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.servico.ServiceImpl;
import br.com.arquitetura.vo.PaginacaoBuilder;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.conv.DestinoEstoqueConverter;
import br.com.vitae.bus.dao.DestinoEstoqueDAO;
import br.com.vitae.bus.dto.DestinoEstoqueDTO;
import br.com.vitae.bus.entity.DestinoEstoque;
import br.com.vitae.bus.service.IDestinoEstoqueServico;

@SuppressWarnings("serial")
@Service
public class DestinoEstoqueServicoImpl extends ServiceImpl<DestinoEstoque> implements IDestinoEstoqueServico {

	@Autowired
	private DestinoEstoqueDAO destinoEstoqueDao;

	@Override
	@Transactional(readOnly = true)
	public DestinoEstoqueDTO consultarIdDTO(Long id) {

		DestinoEstoque categoria = consultar(DestinoEstoque.class, id);
		return DestinoEstoqueConverter.getInstance().entidade2Dto(categoria);
	}

	@Override
	@Transactional(readOnly = false)
	public void salvarOuAtualizar(DestinoEstoqueDTO categoriaDTO) {

		DestinoEstoque categoria = DestinoEstoqueConverter.getInstance().dto2Entidade(categoriaDTO);
		salvarOuAtualizar(categoria);
	}

	@Override
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public PaginacaoDTO<DestinoEstoqueDTO> listarPaginado(PaginacaoParams pp) throws DaoException {

		return PaginacaoBuilder.create(DestinoEstoqueConverter.getInstance(), DestinoEstoque.class, pp).search();
	}

	@Override
	@Transactional(readOnly = true)
	public List<DestinoEstoqueDTO> findAllDestinosEstoque() {

		List<DestinoEstoque> lista = destinoEstoqueDao.findAllDestinosEstoque();
		return DestinoEstoqueConverter.getInstance().listEntidade2ListDto(lista);
	}
}
