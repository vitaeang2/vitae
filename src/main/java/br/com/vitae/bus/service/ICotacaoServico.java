package br.com.vitae.bus.service;

import java.time.LocalDate;
import java.util.List;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.servico.IService;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.dto.CotacaoDTO;
import br.com.vitae.bus.dto.CotacaoFornecedorDTO;
import br.com.vitae.bus.dto.FornecedorDTO;
import br.com.vitae.bus.dto.ItemCotacaoDTO;
import br.com.vitae.bus.entity.Cotacao;
import br.com.vitae.bus.entity.CotacaoFornecedor;

public interface ICotacaoServico extends IService<Cotacao> {

	CotacaoDTO consultarIdDTO(Long id);

	void salvarOuAtualizar(CotacaoDTO cotacaoDTO);

	PaginacaoDTO<CotacaoDTO> listarPaginado(PaginacaoParams pp) throws DaoException;

	List<CotacaoDTO> listarCombo();

	CotacaoDTO salvarOuAtualizar(CotacaoDTO cotacaoDTO, List<FornecedorDTO> listaFornecedores, List<ItemCotacaoDTO> listaItens);

	CotacaoDTO salvarEEnviar(CotacaoDTO cotacaoDTO, List<FornecedorDTO> listaFornecedoresDTO, List<ItemCotacaoDTO> listaItensDTO);
	
	CotacaoDTO salvarResposta(CotacaoDTO cotacaoDTO, List<ItemCotacaoDTO> listaItensDTO, CotacaoFornecedorDTO cotacaoFornecedor);

	PaginacaoDTO<CotacaoDTO> listarPaginado(PaginacaoParams pp, Long idEmpresa, Long idNucleo, Integer  status, LocalDate dataInicio, LocalDate dataFim) throws DaoException;

	List<Cotacao> listarCotacaoPorDataAutal();

	void atualizarNaoRespondido();
	
	boolean enviarEmail(CotacaoFornecedor cf);
	
	void excluirCotacao(Long idCotacao) throws Exception;
	
	long quantidadeCotacoesRespondidas(Long idEmpresa, Long idNucleo) throws DaoException;
	
	PaginacaoDTO<CotacaoDTO> listarPaginadoCotacoesRespondidas(PaginacaoParams pp, Long idEmpresa, Long idNucleo) throws DaoException;
	
	boolean enviarEmail(String nome, String email, String token);
	
}
