package br.com.vitae.bus.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.arquitetura.servico.ServiceImpl;
import br.com.vitae.bus.conv.CotacaoFornecedorConverter;
import br.com.vitae.bus.dao.CotacaoFornecedorDAO;
import br.com.vitae.bus.dto.CotacaoFornecedorDTO;
import br.com.vitae.bus.entity.CotacaoFornecedor;
import br.com.vitae.bus.service.ICotacaoFornecedorServico;

@SuppressWarnings("serial")
@Service
public class CotacaoFornecedorServicoImpl extends ServiceImpl<CotacaoFornecedor> implements ICotacaoFornecedorServico {

	@Autowired
	private CotacaoFornecedorDAO cotacaoFornecedorDao;

	@Override
	@Transactional(readOnly = true)
	public List<CotacaoFornecedor> listarEnviarEmailCotacao() {

		return cotacaoFornecedorDao.listarEnviarEmailCotacao();
	}

	@Override
	@Transactional(readOnly = true)
	public List<CotacaoFornecedor> listarPorCotacao(Long idCotacao) {

		return cotacaoFornecedorDao.listarPorCotacao(idCotacao);
	}

	@Override
	@Transactional(readOnly = true)
	public List<CotacaoFornecedor> listarFornecedoresPorCotacao(Long idCotacao) {

		return cotacaoFornecedorDao.listarFornecedoresPorCotacao(idCotacao);
	}

	@Override
	@Transactional(readOnly = true)
	public CotacaoFornecedorDTO consultarCotacaoFornecedorPorToken(String token) {

		CotacaoFornecedor cf = cotacaoFornecedorDao.consultarCotacaoFornecedorPorToken(token);
		return CotacaoFornecedorConverter.getInstance().entidade2Dto(cf);
	}

	@Override
	@Transactional(readOnly = true)
	public List<CotacaoFornecedorDTO> listarFornecedoresPorPedidoCompra(Long idCotacao) {

		List<CotacaoFornecedor> lista = cotacaoFornecedorDao.listarFornecedoresPorPedidoCompra(idCotacao);
		return CotacaoFornecedorConverter.getInstance().listEntidade2ListDto(lista);
	}
}
