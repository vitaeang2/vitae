package br.com.vitae.bus.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.arquitetura.servico.ServiceImpl;
import br.com.vitae.bus.conv.PerfilUsuarioConverter;
import br.com.vitae.bus.dao.PerfilUsuarioDAO;
import br.com.vitae.bus.dto.PaginaPermissaoDTO;
import br.com.vitae.bus.dto.PerfilUsuarioDTO;
import br.com.vitae.bus.entity.Menu;
import br.com.vitae.bus.entity.Papel;
import br.com.vitae.bus.entity.PerfilUsuario;
import br.com.vitae.bus.entity.Permissao;
import br.com.vitae.bus.service.IMenuServico;
import br.com.vitae.bus.service.IPapelServico;
import br.com.vitae.bus.service.IPerfilAcessoServico;
import br.com.vitae.bus.service.IPermissaoServico;

@SuppressWarnings("serial")
@Service
public class PerfilUsuarioServicoImpl extends ServiceImpl<PerfilUsuario> implements IPerfilAcessoServico {

	@Autowired
	private IPermissaoServico iPermissaoServico;

	@Autowired
	private IPapelServico iPapelServico;

	@Autowired
	private IMenuServico iMenuServico;

	@Autowired
	private PerfilUsuarioDAO perfilUsuarioDao;

	@Override
	@Transactional(readOnly = true)
	public List<PaginaPermissaoDTO> listarPaginaSelecionadas(Long idPerfilUsuario) {

		List<PaginaPermissaoDTO> listaPerfil = new ArrayList<PaginaPermissaoDTO>();
		List<Menu> listaPaginasSelecionadas = iMenuServico.listarPaginaSelecionadas(idPerfilUsuario);
		montarPaginas(idPerfilUsuario, listaPerfil, listaPaginasSelecionadas, true);

		List<Menu> listaPaginasNaoSelecionadas = iMenuServico.listarPaginaNaoSelecionadas(idPerfilUsuario);
		montarPaginas(idPerfilUsuario, listaPerfil, listaPaginasNaoSelecionadas, false);
		return listaPerfil;
	}

	private void montarPaginas(Long idPerfilUsuario, List<PaginaPermissaoDTO> listaPerfil, List<Menu> listaMenu, boolean isSelecionado) {

		PaginaPermissaoDTO paginaPermissao;
		PerfilUsuario perfilBanco = consultar(PerfilUsuario.class, idPerfilUsuario);
		for (Menu menu : listaMenu) {
			paginaPermissao = new PaginaPermissaoDTO();
			paginaPermissao.setId(idPerfilUsuario);
			paginaPermissao.setIdPagina(menu.getId());
			paginaPermissao.setNome(perfilBanco.getNomePerfil());
			paginaPermissao.setNomePagina(menu.getNome());
			paginaPermissao.setSelecionada(isSelecionado);

			Permissao permissao = iPermissaoServico.consultarPermissao(idPerfilUsuario, menu.getId());
			montaPermissao(paginaPermissao, permissao);

			listaPerfil.add(paginaPermissao);
		}
	}

	private void montaPermissao(PaginaPermissaoDTO paginaPermissao, Permissao permissao) {

		if (permissao == null) {
			paginaPermissao.setEditar(false);
			paginaPermissao.setExcluir(false);
			paginaPermissao.setInserir(false);
			paginaPermissao.setVisualizar(false);
			return;
		}
		paginaPermissao.setEditar(permissao.isEdita());
		paginaPermissao.setExcluir(permissao.isExclui());
		paginaPermissao.setInserir(permissao.isInsere());
		paginaPermissao.setVisualizar(permissao.isVisualiza());
	}

	@Override
	@Transactional(readOnly = true)
	public List<PerfilUsuarioDTO> listarPerfis() {

		List<PerfilUsuario> lista = perfilUsuarioDao.listarPerfis();
		return PerfilUsuarioConverter.getInstance().listEntidade2ListDto(lista);
	}

	@Override
	@Transactional(readOnly = false)
	public void salvarPermissoes(Long idPerfil, List<PaginaPermissaoDTO> lista) {

		List<Papel> listaPapeisRemover = perfilUsuarioDao.listarPapeisId(idPerfil);
		iPapelServico.removerLista(listaPapeisRemover);
		Papel papel;
		for (PaginaPermissaoDTO pp : lista) {
			Permissao permissao = montarPermissao(pp);

			papel = new Papel();
			papel.setPermissao(permissao);
			papel.setPerfilUsuario(consultar(PerfilUsuario.class, idPerfil));
			papel.setMenu(iMenuServico.consultar(Menu.class, pp.getIdPagina()));

			iPapelServico.salvarOuAtualizar(papel);
		}
	}

	private Permissao montarPermissao(PaginaPermissaoDTO pp) {

		boolean visualizar = pp.isVisualizar();
		boolean inserir = pp.isInserir();
		boolean editar = pp.isEditar();
		boolean excluir = pp.isExcluir();
		Permissao permissao = iPermissaoServico.consultarPermissao(visualizar, inserir, editar, excluir);
		if (permissao == null) {
			permissao = new Permissao();
			permissao.setVisualiza(visualizar);
			permissao.setEdita(editar);
			permissao.setInsere(inserir);
			permissao.setExclui(excluir);

			permissao = (Permissao) iPermissaoServico.salvarOuAtualizar(permissao);
		}
		return permissao;
	}

	@Override
	@Transactional(readOnly = true)
	public PaginaPermissaoDTO consultarPermissaoPagina(Long idPerfilUsuario, Long idPagina) {

		Papel papel = iPapelServico.listarPorPerfilPagina(idPerfilUsuario, idPagina);
		return montarPaginaPermissao(idPerfilUsuario, idPagina, papel);
	}

	private PaginaPermissaoDTO montarPaginaPermissao(Long idPerfilUsuario, Long idPagina, Papel papel) {

		PaginaPermissaoDTO pp = new PaginaPermissaoDTO();
		Permissao permissao = papel.getPermissao();
		pp.setEditar(permissao.isEdita());
		pp.setVisualizar(permissao.isVisualiza());
		pp.setInserir(permissao.isInsere());
		pp.setExcluir(permissao.isExclui());
		pp.setIdPagina(idPagina);
		pp.setId(idPerfilUsuario);
		pp.setNome(papel.getPerfilUsuario().getNomePerfil());
		pp.setNomePagina(papel.getMenu().getNomePagina());
		pp.setIdPapel(papel.getId());
		return pp;
	}

}
