package br.com.vitae.bus.service.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.arquitetura.servico.ServiceImpl;
import br.com.vitae.bus.dao.MenuDAO;
import br.com.vitae.bus.dto.MenuDTO;
import br.com.vitae.bus.entity.Menu;
import br.com.vitae.bus.service.IMenuServico;

@SuppressWarnings("serial")
@Service
public class MenuServico extends ServiceImpl<Menu> implements IMenuServico {

	@Autowired
	private MenuDAO menuDao;

	@Override
	@Transactional(readOnly = true)
	public List<Menu> listarMenu(Long idPerfilUsuario) {
		List<Menu> lista = menuDao.listarMenu(idPerfilUsuario);
		return lista;
	}

	@Transactional(readOnly = true)
	public Collection<MenuDTO> carregarMenu(Long idPerfilUsuario) {
		Long id = null;
		MenuDTO menuDTO = null;
		List<Menu> lista = menuDao.listarMenu(idPerfilUsuario);
		Map<String, MenuDTO> menus = new HashMap<>();

		for (Menu menu : lista) {
			Menu pai = menu.getMenuPai();
			if (pai != null) {
				menuDTO = menus.get(pai.getId().toString());
				if (menuDTO == null) {
//					tree = new TreeMenu(pai);
					menuDTO = new MenuDTO(pai.getId(), pai.getNome(), pai.getNomePagina(), "");
				}
				menuDTO.addSubmenu(menu.getId(), menu.getNome(), menu.getNomePagina());
				id = pai.getId();
			} else {
				menuDTO = new MenuDTO(menu.getId(),menu.getNome(), menu.getNomePagina(), "");
				id = menu.getId();
			}
			menus.put(id.toString(), menuDTO);
		}
		return menus.values();
	}


	@Override
	@Transactional(readOnly = true)
	public List<Menu> listarPaginaNaoSelecionadas(Long idPerfilUsuario) {
		return menuDao.listarPaginaNaoSelecionado(idPerfilUsuario);
	}


	@Override
	@Transactional(readOnly = true)
	public List<Menu> listarPaginaSelecionadas(Long idPerfilUsuario) {
		return menuDao.listarPaginaSelecionado(idPerfilUsuario);
	}


}
