package br.com.vitae.bus.service;

import java.util.List;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.servico.IService;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.dto.DepartamentoDTO;
import br.com.vitae.bus.entity.Departamento;

public interface IDepartamentoServico extends IService<Departamento> {

	DepartamentoDTO consultarIdDTO(Long id);

	void salvarOuAtualizar(DepartamentoDTO departamentoDTO);

	List<DepartamentoDTO> listarCombo();
	
	public PaginacaoDTO<DepartamentoDTO> listarPaginado(PaginacaoParams pp) throws DaoException;
	
	public DepartamentoDTO consultarDepartamentoSemViculoPatrimonio(Long id) throws DaoException;
}
