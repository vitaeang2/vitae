package br.com.vitae.bus.service;

import java.util.List;

import br.com.arquitetura.servico.IService;
import br.com.vitae.bus.dto.NumeroBancoDTO;
import br.com.vitae.bus.entity.NumeroBanco;

public interface INumeroBancoServico extends IService<NumeroBanco> {

	public List<NumeroBancoDTO> listarNumerosBancos();

	public List<NumeroBancoDTO> consultarPorNome(String nomeBanco);
}
