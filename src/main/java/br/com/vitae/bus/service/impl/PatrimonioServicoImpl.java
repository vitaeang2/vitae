package br.com.vitae.bus.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.servico.ServiceImpl;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.conv.ComponenteItemPatrimonioConverter;
import br.com.vitae.bus.conv.PatrimonioConverter;
import br.com.vitae.bus.dao.PatrimonioDAO;
import br.com.vitae.bus.dto.ComponenteItemPatrimonioDTO;
import br.com.vitae.bus.dto.PatrimonioDTO;
import br.com.vitae.bus.entity.ComponenteItemPatrimonio;
import br.com.vitae.bus.entity.Patrimonio;
import br.com.vitae.bus.service.IComponenteItemPatrimonioServico;
import br.com.vitae.bus.service.IPatrimonioServico;

@SuppressWarnings("serial")
@Service
public class PatrimonioServicoImpl extends ServiceImpl<Patrimonio> implements IPatrimonioServico {

	@Autowired
	private PatrimonioDAO patrimonioDao;

	@Autowired
	private IComponenteItemPatrimonioServico iComponenteItemPatrimonioServico;

	@Override
	@Transactional(readOnly = true)
	public PatrimonioDTO consultarIdDTO(Long id) {

		PatrimonioDTO patrimonioDTO;
		Patrimonio patrimonio = consultar(Patrimonio.class, id);
		patrimonioDTO = PatrimonioConverter.getInstance().entidade2Dto(patrimonio);
		patrimonioDTO
				.setComponenteItemPatrimonio(iComponenteItemPatrimonioServico.listarPorPatrimonio(patrimonio.getId()));

		return patrimonioDTO;
	}

	@Override
	@Transactional(readOnly = false)
	public void salvarOuAtualizar(PatrimonioDTO patrimonioDTO) {

		Patrimonio patrimonio = PatrimonioConverter.getInstance().dto2Entidade(patrimonioDTO);

		if (patrimonio.getId() == null) {
			patrimonio.setAtivo(Boolean.TRUE);
		}

		patrimonio = (Patrimonio) salvarOuAtualizar(patrimonio);

		ComponenteItemPatrimonio itemComponenteItemPatrimonio = null;
		for (ComponenteItemPatrimonioDTO itemPatrimonioDTO : patrimonioDTO.getComponenteItemPatrimonio()) {
			itemComponenteItemPatrimonio = ComponenteItemPatrimonioConverter.getInstance()
					.dto2Entidade(itemPatrimonioDTO);
			itemComponenteItemPatrimonio.setPatrimonio(new Patrimonio(patrimonio.getId()));
			itemComponenteItemPatrimonio.setAtivo(Boolean.TRUE);
			itemComponenteItemPatrimonio = (ComponenteItemPatrimonio) iComponenteItemPatrimonioServico
					.salvarOuAtualizar(itemComponenteItemPatrimonio);
		}
	}

	@Override
	@Transactional(readOnly = true)
	public PaginacaoDTO<PatrimonioDTO> listarPaginado(PaginacaoParams pp, Long idDepartamento, Long idGrupoBem,
			String numeroEtiqueta, String nome, String numeroNota, String fimGarantia, String idSituacaoBem)
					throws DaoException {

		return patrimonioDao.listarPaginado(pp, idDepartamento, idGrupoBem, numeroEtiqueta, nome, numeroNota,
				fimGarantia, idSituacaoBem);
	}

	@Override
	@Transactional(readOnly = true)
	public List<PatrimonioDTO> listarCombo() {

		List<Patrimonio> lista = patrimonioDao.listarCombo();
		return PatrimonioConverter.getInstance().listEntidade2ListDto(lista);
	}
}
