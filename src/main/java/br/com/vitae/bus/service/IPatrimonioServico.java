package br.com.vitae.bus.service;

import java.util.List;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.servico.IService;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.dto.PatrimonioDTO;
import br.com.vitae.bus.entity.Patrimonio;

public interface IPatrimonioServico extends IService<Patrimonio> {

	PatrimonioDTO consultarIdDTO(Long id);
	
	public void salvarOuAtualizar(PatrimonioDTO patrimonioDTO);

	List<PatrimonioDTO> listarCombo();
	
	public PaginacaoDTO<PatrimonioDTO> listarPaginado(PaginacaoParams pp, Long idDepartamento, Long idGrupoBem, String numeroEtiqueta, String nome, String numeroNota, String fimGarantia, String idSituacaoBem) throws DaoException;
}
