package br.com.vitae.bus.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.arquitetura.servico.ServiceImpl;
import br.com.vitae.bus.conv.ItemPedidoCompraConverter;
import br.com.vitae.bus.dao.ItemPedidoCompraDAO;
import br.com.vitae.bus.dto.ItemPedidoCompraDTO;
import br.com.vitae.bus.entity.ItemPedidoCompra;
import br.com.vitae.bus.service.IItemPedidoCompraServico;

@SuppressWarnings("serial")
@Service
public class ItemPedidoCompraServicoImpl extends ServiceImpl<ItemPedidoCompra> implements IItemPedidoCompraServico {

	@Autowired
	private ItemPedidoCompraDAO itemPedidoCompraDao;

	@Override
	@Transactional(readOnly = true)
	public ItemPedidoCompraDTO consultarIdDTO(Long id) {

		ItemPedidoCompra pedidoCompra = consultar(ItemPedidoCompra.class, id);
		return ItemPedidoCompraConverter.getInstance().entidade2Dto(pedidoCompra);
	}

	@Override
	@Transactional(readOnly = true)
	public List<ItemPedidoCompraDTO> listarPorPedidoDTO(Long idPedido) {

		List<ItemPedidoCompra> lista = itemPedidoCompraDao.listarPorPedido(idPedido);
		return ItemPedidoCompraConverter.getInstance().listEntidade2ListDto(lista);
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<ItemPedidoCompra> listarPorPedido(Long idPedido) {

		return itemPedidoCompraDao.listarPorPedido(idPedido);
	}

}
