package br.com.vitae.bus.service;

import java.util.List;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.servico.IService;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.dto.EmpresaDTO;
import br.com.vitae.bus.entity.Empresa;

public interface IEmpresaServico extends IService<Empresa> {

	EmpresaDTO consultarIdDTO(Long id);

	void salvarOuAtualizar(EmpresaDTO empresaDTO);

	PaginacaoDTO<EmpresaDTO> listarPaginado(PaginacaoParams pp) throws DaoException;

	List<EmpresaDTO> listarCombo();
}
