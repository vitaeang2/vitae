package br.com.vitae.bus.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.servico.ServiceImpl;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.conv.GrupoBemConverter;
import br.com.vitae.bus.dao.GrupoBemDAO;
import br.com.vitae.bus.dto.GrupoBemDTO;
import br.com.vitae.bus.entity.GrupoBem;
import br.com.vitae.bus.service.IGrupoBemServico;

@SuppressWarnings("serial")
@Service
public class GrupoBemServicoImpl extends ServiceImpl<GrupoBem> implements IGrupoBemServico {

	@Autowired
	private GrupoBemDAO grupoBemDao;

	@Override
	@Transactional(readOnly = true)
	public GrupoBemDTO consultarIdDTO(Long id) {

		GrupoBem grupoBem = consultar(GrupoBem.class, id);
		return GrupoBemConverter.getInstance().entidade2Dto(grupoBem);
	}

	@Override
	@Transactional(readOnly = false)
	public void salvarOuAtualizar(GrupoBemDTO grupoBemDTO) {

		GrupoBem grupoBem = GrupoBemConverter.getInstance().dto2Entidade(grupoBemDTO);
		if (grupoBem.getId() == null) {			
			grupoBem.setAtivo(Boolean.TRUE);
		}	
		salvarOuAtualizar(grupoBem);
	}


	@Override
	@Transactional(readOnly = true)
	public PaginacaoDTO<GrupoBemDTO> listarPaginado(PaginacaoParams pp) throws DaoException {

		return grupoBemDao.listarPaginado(pp);
	}

	@Override
	@Transactional(readOnly = true)
	public List<GrupoBemDTO> listarCombo() {

		List<GrupoBem> lista = grupoBemDao.listarCombo();
		return GrupoBemConverter.getInstance().listEntidade2ListDto(lista);
	}

	@Override
	public GrupoBemDTO consultarGrupoBemSemViculoPatrimonio(Long id) throws DaoException {
		return grupoBemDao.consultarGrupoBemSemViculoPatrimonio(id);
	}
}
