package br.com.vitae.bus.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.arquitetura.servico.ServiceImpl;
import br.com.vitae.bus.dao.PermissaoDAO;
import br.com.vitae.bus.entity.Permissao;
import br.com.vitae.bus.service.IPermissaoServico;

@SuppressWarnings("serial")
@Service
public class PermissaoServicoImpl extends ServiceImpl<Permissao> implements IPermissaoServico {
	
	@Autowired
	private PermissaoDAO permissaoDao;

	@Override
	@Transactional(readOnly = true)
	public Permissao consultarPermissao(Long idPerfilUsuario, Long idPagina) {
		return permissaoDao.consultarPermissao(idPerfilUsuario, idPagina);
	}

	@Override
	@Transactional(readOnly = true)
	public Permissao consultarPermissao(boolean visualizar, boolean inserir, boolean editar, boolean excluir) {
		return permissaoDao.consultarPermissao(visualizar, inserir, editar, excluir);
	}
}
