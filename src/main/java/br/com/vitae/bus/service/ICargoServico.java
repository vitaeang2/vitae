package br.com.vitae.bus.service;

import java.util.List;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.servico.IService;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.dto.CargoDTO;
import br.com.vitae.bus.entity.Cargo;

public interface ICargoServico extends IService<Cargo> {

	CargoDTO consultarIdDTO(Long id);

	void salvarOuAtualizar(CargoDTO cargoDTO);

	PaginacaoDTO<CargoDTO> listarPaginado(PaginacaoParams pp) throws DaoException;

	List<CargoDTO> listarCombo();
}
