package br.com.vitae.bus.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.servico.ServiceImpl;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.conv.ComponenteItemPatrimonioConverter;
import br.com.vitae.bus.dao.ComponenteItemPatrimonioDAO;
import br.com.vitae.bus.dto.ComponenteItemPatrimonioDTO;
import br.com.vitae.bus.entity.ComponenteItemPatrimonio;
import br.com.vitae.bus.service.IComponenteItemPatrimonioServico;

@SuppressWarnings("serial")
@Service
public class ComponenteItemPatrimonioServicoImpl extends ServiceImpl<ComponenteItemPatrimonio> implements IComponenteItemPatrimonioServico {

	@Autowired
	private ComponenteItemPatrimonioDAO componenteItemPatrimonioDao;

	@Override
	@Transactional(readOnly = true)
	public ComponenteItemPatrimonioDTO consultarIdDTO(Long id) {

		ComponenteItemPatrimonio componenteItemPatrimonio = consultar(ComponenteItemPatrimonio.class, id);
		return ComponenteItemPatrimonioConverter.getInstance().entidade2Dto(componenteItemPatrimonio);
	}

	@Override
	@Transactional(readOnly = false)
	public void salvarOuAtualizar(ComponenteItemPatrimonioDTO componenteItemPatrimonioDTO) {

		ComponenteItemPatrimonio componenteItemPatrimonio = ComponenteItemPatrimonioConverter.getInstance().dto2Entidade(componenteItemPatrimonioDTO);
		if (componenteItemPatrimonio.getId() == null) {			
			componenteItemPatrimonio.setAtivo(Boolean.TRUE);
		}	
		salvarOuAtualizar(componenteItemPatrimonio);
	}


	@Override
	@Transactional(readOnly = true)
	public PaginacaoDTO<ComponenteItemPatrimonioDTO> listarPaginado(PaginacaoParams pp) throws DaoException {

		return componenteItemPatrimonioDao.listarPaginado(pp);
	}

	@Override
	@Transactional(readOnly = true)
	public List<ComponenteItemPatrimonioDTO> listarCombo() {

		List<ComponenteItemPatrimonio> lista = componenteItemPatrimonioDao.listarCombo();
		return ComponenteItemPatrimonioConverter.getInstance().listEntidade2ListDto(lista);
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<ComponenteItemPatrimonioDTO> listarPorPatrimonio(Long idPatrimonio) {

		List<ComponenteItemPatrimonio> lista = componenteItemPatrimonioDao.listarPorPatrimonio(idPatrimonio);
		return ComponenteItemPatrimonioConverter.getInstance().listEntidade2ListDto(lista);
	}
}
