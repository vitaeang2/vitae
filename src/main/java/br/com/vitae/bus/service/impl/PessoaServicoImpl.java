package br.com.vitae.bus.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.arquitetura.servico.ServiceImpl;
import br.com.vitae.bus.entity.Pessoa;
import br.com.vitae.bus.service.IPessoaServico;

@SuppressWarnings("serial")
@Service
public class PessoaServicoImpl extends ServiceImpl<Pessoa> implements IPessoaServico {

	@Override
	@Transactional(readOnly = false)
	public void atualizarEmail(Long id, String email) {
		Pessoa pessoa = consultar(Pessoa.class, id);
		pessoa.setEmail(email);
		
		salvarOuAtualizar(pessoa);
	}

}