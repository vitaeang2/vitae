package br.com.vitae.bus.service;

import java.util.List;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.servico.IService;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.dto.DestinoEstoqueDTO;
import br.com.vitae.bus.entity.DestinoEstoque;

public interface IDestinoEstoqueServico extends IService<DestinoEstoque> {
	
	PaginacaoDTO<DestinoEstoqueDTO> listarPaginado(PaginacaoParams pp) throws DaoException;

	DestinoEstoqueDTO consultarIdDTO(Long id);

	void salvarOuAtualizar(DestinoEstoqueDTO destinoEstoqueDTO);

	List<DestinoEstoqueDTO> findAllDestinosEstoque();
}
