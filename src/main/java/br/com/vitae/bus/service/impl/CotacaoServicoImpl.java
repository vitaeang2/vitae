package br.com.vitae.bus.service.impl;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.servico.MailSenderService;
import br.com.arquitetura.servico.ServiceImpl;
import br.com.arquitetura.util.DataUtil;
import br.com.arquitetura.util.PasswordBuilder;
import br.com.arquitetura.util.PathConfigureUtil;
import br.com.arquitetura.vo.PaginacaoBuilder;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.conv.CotacaoConverter;
import br.com.vitae.bus.conv.FornecedorConverter;
import br.com.vitae.bus.conv.ItemCotacaoConverter;
import br.com.vitae.bus.dao.CotacaoDAO;
import br.com.vitae.bus.dto.CotacaoDTO;
import br.com.vitae.bus.dto.CotacaoFornecedorDTO;
import br.com.vitae.bus.dto.FornecedorDTO;
import br.com.vitae.bus.dto.ItemCotacaoDTO;
import br.com.vitae.bus.entity.Cotacao;
import br.com.vitae.bus.entity.CotacaoFornecedor;
import br.com.vitae.bus.entity.Fornecedor;
import br.com.vitae.bus.entity.ItemCotacao;
import br.com.vitae.bus.enumerator.EnumStatusCotacao;
import br.com.vitae.bus.enumerator.EnumStatusCotacaoFornecedor;
import br.com.vitae.bus.service.ICotacaoFornecedorServico;
import br.com.vitae.bus.service.ICotacaoServico;
import br.com.vitae.bus.service.IItemCotacaoServico;

@SuppressWarnings("serial")
@Service
public class CotacaoServicoImpl extends ServiceImpl<Cotacao> implements ICotacaoServico {

	private static final String TEMPLATE_COTACAO = "cotacao.vm";

	@Autowired
	private MailSenderService mailSenderService;

	@Autowired
	private CotacaoDAO cotacaoDao;

	@Autowired
	private IItemCotacaoServico iItemCotacaoServico;

	@Autowired
	private ICotacaoFornecedorServico iCotacaoFornecedorServico;

	@Override
	@Transactional(readOnly = true)
	public CotacaoDTO consultarIdDTO(Long id) {

		Cotacao cotacao = consultar(Cotacao.class, id);
		return CotacaoConverter.getInstance().entidade2Dto(cotacao);
	}

	@Override
	@Transactional(readOnly = false)
	public void salvarOuAtualizar(CotacaoDTO cotacaoDTO) {

		Cotacao cotacao = CotacaoConverter.getInstance().dto2Entidade(cotacaoDTO);
		salvarOuAtualizar(cotacao);
	}

	@Override
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public PaginacaoDTO<CotacaoDTO> listarPaginado(PaginacaoParams pp) throws DaoException {

		return PaginacaoBuilder.create(CotacaoConverter.getInstance(), Cotacao.class, pp).search();
	}

	@Override
	@Transactional(readOnly = true)
	public PaginacaoDTO<CotacaoDTO> listarPaginado(PaginacaoParams pp, Long idEmpresa, Long idNucleo, Integer status, LocalDate dataInicio, LocalDate dataFim) throws DaoException {
		return cotacaoDao.listarPaginado(pp, idEmpresa, idNucleo, status, dataInicio, dataFim);
	}

	@Override
	@Transactional(readOnly = true)
	public List<CotacaoDTO> listarCombo() {

		List<Cotacao> lista = cotacaoDao.listarCombo();
		return CotacaoConverter.getInstance().listEntidade2ListDto(lista);
	}

	@Override
	@Transactional(readOnly = false)
	public CotacaoDTO salvarOuAtualizar(CotacaoDTO cotacaoDTO, List<FornecedorDTO> listaFornecedoresDTO, List<ItemCotacaoDTO> listaItensDTO) {

		Cotacao cotacao = CotacaoConverter.getInstance().dto2Entidade(cotacaoDTO);
		if (cotacao.getId() == null) {
			cotacao.setDataCotacao(LocalDate.now());
			cotacao.setStatus(EnumStatusCotacao.SALVO);
		}

		return salvarOuAtualizar(cotacao, listaFornecedoresDTO, listaItensDTO, false);
	}

	@Override
	@Transactional(readOnly = false)
	public CotacaoDTO salvarEEnviar(CotacaoDTO cotacaoDTO, List<FornecedorDTO> listaFornecedoresDTO, List<ItemCotacaoDTO> listaItensDTO) {

		Cotacao cotacao = CotacaoConverter.getInstance().dto2Entidade(cotacaoDTO);
		cotacao.setStatus(EnumStatusCotacao.ENVIAR);

		return salvarOuAtualizar(cotacao, listaFornecedoresDTO, listaItensDTO, true);

	}

	@Override
	@Transactional(readOnly = false)
	public CotacaoDTO salvarResposta(CotacaoDTO cotacaoDTO, List<ItemCotacaoDTO> listaItensDTO, CotacaoFornecedorDTO cotacaoFornecedorDTO) {

		Cotacao cotacao = consultar(Cotacao.class, cotacaoDTO.getId());
		cotacao.setStatus(EnumStatusCotacao.RESPONDIDO);

		return salvarOuAtualizar(cotacao, listaItensDTO, cotacaoFornecedorDTO);

	}

	@Transactional(readOnly = false)
	private CotacaoDTO salvarOuAtualizar(Cotacao cotacao, List<ItemCotacaoDTO> listaItensDTO, CotacaoFornecedorDTO cotacaoFornecedorDTO) {

		cotacao = (Cotacao) salvarOuAtualizar(cotacao);

		CotacaoFornecedor cotacaoFornecedor = iCotacaoFornecedorServico.consultar(CotacaoFornecedor.class, cotacaoFornecedorDTO.getId());
		cotacaoFornecedor.setCotacao(cotacao);
		cotacaoFornecedor.setVlrDesconto(cotacaoFornecedorDTO.getVlrDesconto() == null ? new BigDecimal(0) : cotacaoFornecedorDTO.getVlrDesconto());
		cotacaoFornecedor.setVlrFrete(cotacaoFornecedorDTO.getVlrFrete() == null ? new BigDecimal(0) : cotacaoFornecedorDTO.getVlrFrete());
		cotacaoFornecedor.setStatus(EnumStatusCotacaoFornecedor.RESPONDIDO);

		cotacaoFornecedor = (CotacaoFornecedor) iCotacaoFornecedorServico.salvarOuAtualizar(cotacaoFornecedor);

		ItemCotacao ic = null;
		for (ItemCotacaoDTO icDTO : listaItensDTO) {
			ic = iItemCotacaoServico.consultar(ItemCotacao.class, icDTO.getId());
			ic.setCotacaoFornecedor(cotacaoFornecedor);
			ic.setQuantidade(icDTO.getQuantidade());
			ic.setVlrUnitario(icDTO.getVlrUnitario());
			iItemCotacaoServico.salvarOuAtualizar(ic);
		}

		return CotacaoConverter.getInstance().entidade2Dto(cotacao);
	}

	@Transactional(readOnly = false)
	private CotacaoDTO salvarOuAtualizar(Cotacao cotacao, List<FornecedorDTO> listaFornecedoresDTO, List<ItemCotacaoDTO> listaItensDTO, boolean isEnviar) {

		List<CotacaoFornecedor> cotacoesFornecedores = new ArrayList<>();

		List<Fornecedor> fornecedores = new ArrayList<>();
		List<ItemCotacao> itens = new ArrayList<>();

		if (cotacao.getId() != null) {
			removerItens(cotacao.getId());
		}
		cotacao.setDataCotacao(LocalDate.now());
		cotacao = (Cotacao) salvarOuAtualizar(cotacao);

		CotacaoFornecedor cf = null;
		ItemCotacao ic = null;
		for (FornecedorDTO fornecedorDTO : listaFornecedoresDTO) {
			Fornecedor fornecedor = FornecedorConverter.getInstance().dto2Entidade(fornecedorDTO);
			fornecedores.add(fornecedor);
			cf = new CotacaoFornecedor();
			cf.setFornecedor(fornecedor);
			cf.setCotacao(cotacao);
			cf.setVlrDesconto(BigDecimal.ZERO);
			cf.setVlrFrete(BigDecimal.ZERO);
			cf.setCotacao(cotacao);
			cotacoesFornecedores.add(cf);
			if (isEnviar) {
				cf.setToken(PasswordBuilder.gerarToken());
				cf.setStatus(EnumStatusCotacaoFornecedor.ENVIAR);
				// enviarEmail(cf);
			}

			cf = (CotacaoFornecedor) iCotacaoFornecedorServico.salvarOuAtualizar(cf);

			for (ItemCotacaoDTO itemCotacaoDTO : listaItensDTO) {
				ic = ItemCotacaoConverter.getInstance().dto2Entidade(itemCotacaoDTO);
				ic.setVlrUnitario(BigDecimal.ZERO);
				ic.setCotacaoFornecedor(cf);
				itens.add(ic);

				iItemCotacaoServico.salvarOuAtualizar(ic);
			}

		}
		// cotacao.setStatus(EnumStatusCotacao.ENVIADO);

		// Retornar cotacao com ID
		return CotacaoConverter.getInstance().entidade2Dto(cotacao);
	}

	@Override
	@Transactional(readOnly = false)
	public void excluirCotacao(Long idCotacao) throws Exception {

		try {

			Cotacao cotacao = consultar(Cotacao.class, idCotacao);

			removerItens(idCotacao);

			remover(cotacao);
		} catch (Exception e) {
			throw new Exception();
		}
	}

	@Transactional(readOnly = false)
	private void removerItens(Long idCotacao) {

		List<CotacaoFornecedor> listaCF = iCotacaoFornecedorServico.listarPorCotacao(idCotacao);
		List<ItemCotacao> listaIC = null;
		for (CotacaoFornecedor cf : listaCF) {
			listaIC = iItemCotacaoServico.listarPorCotacaoFornecedor(cf.getId());
			for (ItemCotacao itemCotacao : listaIC) {
				itemCotacao = iItemCotacaoServico.consultar(ItemCotacao.class, itemCotacao.getId());
				iItemCotacaoServico.remover(itemCotacao);
			}
			cf = iCotacaoFornecedorServico.consultar(CotacaoFornecedor.class, cf.getId());
			iCotacaoFornecedorServico.remover(cf);
		}
	}

	@Override
	public boolean enviarEmail(CotacaoFornecedor cf) {

		mailSenderService.setTemplateName(TEMPLATE_COTACAO);
		HashMap<String, Object> map = new HashMap<>();
		map.put("nome", cf.getFornecedor().getPessoa().getNome());
		map.put("link", PathConfigureUtil.URL+"/#/fornecedorcotacao");
		map.put("token", cf.getToken());
		try {
			mailSenderService.sendHtmlWithTemplate(cf.getFornecedor().getPessoa().getEmail(), "[GM - Vitae] Cotação", map);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	@Override
	public boolean enviarEmail(String nome, String email, String token) {
		
		mailSenderService.setTemplateName(TEMPLATE_COTACAO);
		HashMap<String, Object> map = new HashMap<>();
		map.put("nome", nome);
		map.put("link", PathConfigureUtil.URL+"/#/fornecedorcotacao");
		map.put("token", token);
		try {
			mailSenderService.sendHtmlWithTemplate(email, "[GM - Vitae] Cotação", map);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Cotacao> listarCotacaoPorDataAutal() {

		return cotacaoDao.listarCotacaoPorDataAutal(DataUtil.getDataTimeComTime0(new Date()));
	}

	@Override
	@Transactional(readOnly = false)
	public void atualizarNaoRespondido() {

		Cotacao cotacao = null;
		CotacaoFornecedor cotacaoFornecedor = null;

		List<Cotacao> listaCotacao = listarCotacaoPorDataAutal();

		for (Cotacao cotacaoData : listaCotacao) {
			cotacao = consultar(Cotacao.class, cotacaoData.getId());
			cotacao.setStatus(EnumStatusCotacao.FINALIZADO);
			salvarOuAtualizar(cotacao);

			List<CotacaoFornecedor> listaCF = iCotacaoFornecedorServico.listarFornecedoresPorCotacao(cotacaoData.getId());

			for (CotacaoFornecedor cf : listaCF) {
				cotacaoFornecedor = iCotacaoFornecedorServico.consultar(CotacaoFornecedor.class, cf.getId());
				if (cotacaoFornecedor.getStatus() != EnumStatusCotacaoFornecedor.RESPONDIDO) {
					cotacaoFornecedor.setStatus(EnumStatusCotacaoFornecedor.NAO_RESPONDIDO);
					cotacaoFornecedor.setToken(null);
					iCotacaoFornecedorServico.salvarOuAtualizar(cotacaoFornecedor);
				}
			}
		}
	}
	
	@Override
	@Transactional(readOnly = true)
	public long quantidadeCotacoesRespondidas(Long idEmpresa, Long idNucleo) throws DaoException{
		return cotacaoDao.quantidadeCotacoesRespondidas(idEmpresa, idNucleo);
	}
	
	@Override
	@Transactional(readOnly = true)
	public PaginacaoDTO<CotacaoDTO> listarPaginadoCotacoesRespondidas(PaginacaoParams pp, Long idEmpresa, Long idNucleo) throws DaoException{
		return cotacaoDao.listarPaginadoCotacoesRespondidas(pp, idEmpresa, idNucleo);
	}

}
