package br.com.vitae.bus.service;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.exception.NegocioException;
import br.com.arquitetura.servico.IService;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.dto.ColaboradorDTO;
import br.com.vitae.bus.dto.DocumentoDTO;
import br.com.vitae.bus.entity.Colaborador;

public interface IColaboradorServico extends IService<Colaborador> {

	ColaboradorDTO consultarIdDTO(Long id, String caminho, String url);

	ColaboradorDTO salvarOuAtualizar(ColaboradorDTO empresaDTO, Long idPerfil, String caminho) throws NegocioException, DaoException;

	PaginacaoDTO<ColaboradorDTO> listarPaginado(PaginacaoParams pp, Long idEmpresa) throws DaoException;

	List<ColaboradorDTO> listarCombo();
	
	Colaborador getColaboradorLogado();

	Colaborador consultarPorIdUsuario(Long idUsuario);

	PaginacaoDTO<ColaboradorDTO> listarPaginado(PaginacaoParams pp, Long idEmpresa, Long idNucleo) throws DaoException;
	
	boolean possuiColaboradorPorEmail(String email) throws DaoException;

	boolean possuiColaboradorPorCnj(String cnj) throws DaoException;

	void excluirId(Long id);
	
	void criarFoto(HttpServletRequest request, String id) throws IOException, ServletException;

	void removerFoto(HttpServletRequest request, String id) throws IOException, ServletException;

	void criarDocumento(HttpServletRequest request, DocumentoDTO documentoDTO) throws IOException, ServletException;
}
