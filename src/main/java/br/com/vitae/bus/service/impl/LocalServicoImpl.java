package br.com.vitae.bus.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.servico.ServiceImpl;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.conv.LocalConverter;
import br.com.vitae.bus.dao.LocalDAO;
import br.com.vitae.bus.dto.LocalDTO;
import br.com.vitae.bus.entity.Local;
import br.com.vitae.bus.service.ILocalServico;

@SuppressWarnings("serial")
@Service
public class LocalServicoImpl extends ServiceImpl<Local> implements ILocalServico {

	@Autowired
	private LocalDAO localDao;

	@Override
	@Transactional(readOnly = true)
	public LocalDTO consultarIdDTO(Long id) {

		Local local = consultar(Local.class, id);
		return LocalConverter.getInstance().entidade2Dto(local);
	}

	@Override
	@Transactional(readOnly = false)
	public void salvarOuAtualizar(LocalDTO localDTO) {

		Local local = LocalConverter.getInstance().dto2Entidade(localDTO);
		if (local.getId() == null) {			
			local.setAtivo(Boolean.TRUE);
		}	
		salvarOuAtualizar(local);
	}


	@Override
	@Transactional(readOnly = true)
	public PaginacaoDTO<LocalDTO> listarPaginado(PaginacaoParams pp) throws DaoException {

		return localDao.listarPaginado(pp);
	}

	@Override
	@Transactional(readOnly = true)
	public List<LocalDTO> listarCombo() {

		List<Local> lista = localDao.listarCombo();
		return LocalConverter.getInstance().listEntidade2ListDto(lista);
	}

	@Override
	public LocalDTO consultarLocalSemViculoPatrimonio(Long id) throws DaoException {
		return localDao.consultarLocalSemViculoPatrimonio(id);
	}
}
