package br.com.vitae.bus.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.servico.ServiceImpl;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.conv.CategoriaProdutoConverter;
import br.com.vitae.bus.dao.CategoriaProdutoDAO;
import br.com.vitae.bus.dto.CategoriaProdutoDTO;
import br.com.vitae.bus.entity.CategoriaProduto;
import br.com.vitae.bus.service.ICategoriaProdutoServico;

@SuppressWarnings("serial")
@Service
public class CategoriaProdutoServicoImpl extends ServiceImpl<CategoriaProduto> implements ICategoriaProdutoServico {

	@Autowired
	private CategoriaProdutoDAO categoriaDao;

	@Override
	@Transactional(readOnly = true)
	public CategoriaProdutoDTO consultarIdDTO(Long id) {

		CategoriaProduto categoria = consultar(CategoriaProduto.class, id);
		return CategoriaProdutoConverter.getInstance().entidade2Dto(categoria);
	}

	@Override
	@Transactional(readOnly = false)
	public void salvarOuAtualizar(CategoriaProdutoDTO categoriaDTO) {

		CategoriaProduto categoria = CategoriaProdutoConverter.getInstance().dto2Entidade(categoriaDTO);
		if (categoria.getId() == null) {			
			categoria.setAtivo(Boolean.TRUE);
		}		
		salvarOuAtualizar(categoria);
	}

	// @Override
	// @SuppressWarnings("unchecked")
	// @Transactional(readOnly = true)
	// public PaginacaoDTO<CategoriaProdutoDTO> listarPaginado(PaginacaoParams
	// pp) throws DaoException {
	//
	// return PaginacaoBuilder.create(CategoriaProdutoConverter.getInstance(),
	// CategoriaProduto.class, pp).search();
	// }

	@Override
	@Transactional(readOnly = true)
	public List<CategoriaProdutoDTO> listarCombo() {

		List<CategoriaProduto> lista = categoriaDao.listarCombo();
		return CategoriaProdutoConverter.getInstance().listEntidade2ListDto(lista);
	}

	@Override
	@Transactional(readOnly = true)
	public PaginacaoDTO<CategoriaProdutoDTO> listarPaginado(PaginacaoParams pp) throws DaoException {

		return categoriaDao.listarPaginado(pp);
	}

	@Override
	public CategoriaProdutoDTO consultarCategoriaSemViculoProduto(Long id) throws DaoException {
		return categoriaDao.consultarCategoriaSemViculoProduto(id);
	}
}
