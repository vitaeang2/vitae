package br.com.vitae.bus.service;

import java.util.List;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.servico.IService;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.dto.ManutencaoPatrimonioDTO;
import br.com.vitae.bus.entity.ManutencaoPatrimonio;

public interface IManutencaoPatrimonioServico extends IService<ManutencaoPatrimonio> {

	ManutencaoPatrimonioDTO consultarIdDTO(Long id);

	void salvarOuAtualizar(Long idUsuario, ManutencaoPatrimonioDTO produtoDTO);

	PaginacaoDTO<ManutencaoPatrimonioDTO> listarPaginado(PaginacaoParams pp) throws DaoException;

	public PaginacaoDTO<ManutencaoPatrimonioDTO> listarPaginado(PaginacaoParams pp, String numeroEtiqueta, String nome)
			throws DaoException;

	List<ManutencaoPatrimonioDTO> listarCombo();

	void excluir(Long id);
}
