package br.com.vitae.bus.service;

import br.com.arquitetura.servico.IService;
import br.com.vitae.bus.entity.Permissao;

public interface IPermissaoServico extends IService<Permissao> {

	Permissao consultarPermissao(Long idPerfilUsuario, Long idPagina);

	Permissao consultarPermissao(boolean visualizar, boolean inserir, boolean editar, boolean excluir);
}
