package br.com.vitae.bus.service;

import java.util.List;

import br.com.arquitetura.servico.IService;
import br.com.vitae.bus.dto.ItemPedidoCompraDTO;
import br.com.vitae.bus.dto.MovimentoEstoqueDTO;
import br.com.vitae.bus.dto.PedidoCompraDTO;
import br.com.vitae.bus.entity.MovimentoEstoque;

public interface IMovimentoEstoqueServico extends IService<MovimentoEstoque> {

	void salvarOuAtualizar(MovimentoEstoqueDTO movimentoEstoqueDTO);

	List<MovimentoEstoqueDTO> getMovimentosEstoque(Long produtoId, Long nucleoId, Long empresaId);

	void entrarEstoque(PedidoCompraDTO pedidoCompraDTO, List<ItemPedidoCompraDTO> listaPedidoCompraDTO);
	
	MovimentoEstoque salvarOuAtualizarComProdutoEstoque(MovimentoEstoque movimentoEstoque);
	
	void entrarEstoque(Long idPedidoCompra, ItemPedidoCompraDTO itemPedidoCompraDTO);
}
