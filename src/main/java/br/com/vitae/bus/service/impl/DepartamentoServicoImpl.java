package br.com.vitae.bus.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.servico.ServiceImpl;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.conv.DepartamentoConverter;
import br.com.vitae.bus.dao.DepartamentoDAO;
import br.com.vitae.bus.dto.DepartamentoDTO;
import br.com.vitae.bus.entity.Departamento;
import br.com.vitae.bus.service.IDepartamentoServico;

@SuppressWarnings("serial")
@Service
public class DepartamentoServicoImpl extends ServiceImpl<Departamento> implements IDepartamentoServico {

	@Autowired
	private DepartamentoDAO departamentoDao;

	@Override
	@Transactional(readOnly = true)
	public DepartamentoDTO consultarIdDTO(Long id) {

		Departamento departamento = consultar(Departamento.class, id);
		return DepartamentoConverter.getInstance().entidade2Dto(departamento);
	}

	@Override
	@Transactional(readOnly = false)
	public void salvarOuAtualizar(DepartamentoDTO departamentoDTO) {

		Departamento departamento = DepartamentoConverter.getInstance().dto2Entidade(departamentoDTO);
		if (departamento.getId() == null) {			
			departamento.setAtivo(Boolean.TRUE);
		}	
		salvarOuAtualizar(departamento);
	}


	@Override
	@Transactional(readOnly = true)
	public PaginacaoDTO<DepartamentoDTO> listarPaginado(PaginacaoParams pp) throws DaoException {

		return departamentoDao.listarPaginado(pp);
	}

	@Override
	@Transactional(readOnly = true)
	public List<DepartamentoDTO> listarCombo() {

		List<Departamento> lista = departamentoDao.listarCombo();
		return DepartamentoConverter.getInstance().listEntidade2ListDto(lista);
	}

	@Override
	public DepartamentoDTO consultarDepartamentoSemViculoPatrimonio(Long id) throws DaoException {
		return departamentoDao.consultarDepartamentoSemViculoPatrimonio(id);
	}
}
