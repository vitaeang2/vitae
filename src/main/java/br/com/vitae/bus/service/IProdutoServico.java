package br.com.vitae.bus.service;

import java.util.List;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.servico.IService;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.dto.ProdutoDTO;
import br.com.vitae.bus.entity.Produto;

public interface IProdutoServico extends IService<Produto> {

	ProdutoDTO consultarIdDTO(Long id);

	void salvarOuAtualizar(Long idUsuario, ProdutoDTO produtoDTO);

	PaginacaoDTO<ProdutoDTO> listarPaginado(PaginacaoParams pp) throws DaoException;
	
	//public PaginacaoDTO<ProdutoDTO> listarEstoqueBaixoPaginado(PaginacaoParams pp, Long empresaId, Long nucleoId) throws DaoException;

	List<ProdutoDTO> listarCombo();

	void excluir(Long id);
	
	List<Produto> listarFiltroCotacao(String nome);
}
