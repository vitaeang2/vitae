package br.com.vitae.bus.service.impl;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.servico.ServiceImpl;
import br.com.arquitetura.util.IsNullUtil;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.conv.NucleoConverter;
import br.com.vitae.bus.dao.NucleoDAO;
import br.com.vitae.bus.dto.NucleoDTO;
import br.com.vitae.bus.entity.Nucleo;
import br.com.vitae.bus.service.INucleoServico;

@SuppressWarnings("serial")
@Service
public class NucleoServicoImpl extends ServiceImpl<Nucleo> implements INucleoServico {

	@Autowired
	private NucleoDAO nucleoDao;

	@Override
	@Transactional(readOnly = true)
	public NucleoDTO consultarIdDTO(Long id) {

		Nucleo nucleo = consultar(Nucleo.class, id);
		return NucleoConverter.getInstance().entidade2Dto(nucleo);
	}

	@Override
	@Transactional(readOnly = false)
	public void salvarOuAtualizar(NucleoDTO nucleoDTO) {

		Nucleo nucleo = NucleoConverter.getInstance().dto2Entidade(nucleoDTO);

		if (nucleo != null && IsNullUtil.isNullOrEmpty(nucleo.getId())) {

			nucleo.setAtivo(true);
			nucleo.setDataCadastro(LocalDate.now());
		}

		salvarOuAtualizar(nucleo);
	}

	@Override
	@Transactional(readOnly = true)
	public PaginacaoDTO<NucleoDTO> listarPaginado(PaginacaoParams pp) throws DaoException {

		return nucleoDao.listarPaginado(pp);
	}

	@Override
	@Transactional(readOnly = true)
	public List<NucleoDTO> listarComboPorIdEmpresa(Long idEmpresa) {

		List<Nucleo> listaNucleo = nucleoDao.listarComboPorIdEmpresa(idEmpresa);
		return NucleoConverter.getInstance().listEntidade2ListDto(listaNucleo);
	}

	@Override
	@Transactional(readOnly = true)
	public Nucleo consultarPorColaborador(Long id) {

		return nucleoDao.consultarPorColaborador(id);
	}

	@Override
	@Transactional(readOnly = true)
	public boolean possuiNucleoPorColaborador(Long idColaborador) throws DaoException {

		return nucleoDao.possuiNucleoPorColaborador(idColaborador);
	}

	@Override
	public List<NucleoDTO> listarCombo() {

		List<Nucleo> lista = nucleoDao.listarCombo();
		return NucleoConverter.getInstance().listEntidade2ListDto(lista);
	}
}
