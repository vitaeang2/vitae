package br.com.vitae.bus.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.servico.ServiceImpl;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.conv.CategoriaGrupoConverter;
import br.com.vitae.bus.conv.ComponenteItemPatrimonioConverter;
import br.com.vitae.bus.dao.CategoriaGrupoDAO;
import br.com.vitae.bus.dto.CategoriaGrupoDTO;
import br.com.vitae.bus.entity.CategoriaGrupo;
import br.com.vitae.bus.entity.ComponenteItemPatrimonio;
import br.com.vitae.bus.service.ICategoriaGrupoServico;

@SuppressWarnings("serial")
@Service
public class CategoriaGrupoServicoImpl extends ServiceImpl<CategoriaGrupo> implements ICategoriaGrupoServico {

	@Autowired
	private CategoriaGrupoDAO categoriaDao;

	@Override
	@Transactional(readOnly = true)
	public CategoriaGrupoDTO consultarIdDTO(Long id) {

		CategoriaGrupo categoria = consultar(CategoriaGrupo.class, id);
		return CategoriaGrupoConverter.getInstance().entidade2Dto(categoria);
	}

	@Override
	@Transactional(readOnly = false)
	public void salvarOuAtualizar(CategoriaGrupoDTO categoriaDTO) {

		CategoriaGrupo categoria = CategoriaGrupoConverter.getInstance().dto2Entidade(categoriaDTO);
		if (categoria.getId() == null) {
			categoria.setAtivo(Boolean.TRUE);
		}
		salvarOuAtualizar(categoria);
	}

	@Override
	@Transactional(readOnly = true)
	public List<CategoriaGrupoDTO> listarCombo() {

		List<CategoriaGrupo> lista = categoriaDao.listarCombo();
		return CategoriaGrupoConverter.getInstance().listEntidade2ListDto(lista);
	}

	@Override
	@Transactional(readOnly = true)
	public PaginacaoDTO<CategoriaGrupoDTO> listarPaginado(PaginacaoParams pp) throws DaoException {

		return categoriaDao.listarPaginado(pp);
	}

	@Override
	public CategoriaGrupoDTO consultarCategoriaSemViculoGrupo(Long id) throws DaoException {
		return categoriaDao.consultarCategoriaSemViculoGrupo(id);
	}

	@Override
	public List<CategoriaGrupoDTO> listarPorGrupo(Long idGrupo) {

		List<CategoriaGrupo> lista = categoriaDao.listarPorGrupo(idGrupo);
		return CategoriaGrupoConverter.getInstance().listEntidade2ListDto(lista);
	}
}
