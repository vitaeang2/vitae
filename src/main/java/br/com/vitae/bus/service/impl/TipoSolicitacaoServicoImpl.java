package br.com.vitae.bus.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.servico.ServiceImpl;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.conv.TipoSolicitacaoConverter;
import br.com.vitae.bus.dao.TipoSolicitacaoDAO;
import br.com.vitae.bus.dto.TipoSolicitacaoDTO;
import br.com.vitae.bus.entity.TipoSolicitacao;
import br.com.vitae.bus.service.ITipoSolicitacaoServico;

@SuppressWarnings("serial")
@Service
public class TipoSolicitacaoServicoImpl extends ServiceImpl<TipoSolicitacao> implements ITipoSolicitacaoServico {

	@Autowired
	private TipoSolicitacaoDAO tipoSolicitacaoDao;

	@Override
	@Transactional(readOnly = true)
	public TipoSolicitacaoDTO consultarIdDTO(Long id) {

		TipoSolicitacao tipoSolicitacao = consultar(TipoSolicitacao.class, id);
		return TipoSolicitacaoConverter.getInstance().entidade2Dto(tipoSolicitacao);
	}

	@Override
	@Transactional(readOnly = false)
	public void salvarOuAtualizar(TipoSolicitacaoDTO tipoSolicitacaoDTO) {

		TipoSolicitacao tipoSolicitacao = TipoSolicitacaoConverter.getInstance().dto2Entidade(tipoSolicitacaoDTO);
		if (tipoSolicitacao.getId() == null) {			
			tipoSolicitacao.setAtivo(Boolean.TRUE);
		}		
		salvarOuAtualizar(tipoSolicitacao);
	}

	@Override
	@Transactional(readOnly = true)
	public List<TipoSolicitacaoDTO> listarCombo() {

		List<TipoSolicitacao> lista = tipoSolicitacaoDao.listarCombo();
		return TipoSolicitacaoConverter.getInstance().listEntidade2ListDto(lista);
	}

	@Override
	@Transactional(readOnly = true)
	public PaginacaoDTO<TipoSolicitacaoDTO> listarPaginado(PaginacaoParams pp) throws DaoException {

		return tipoSolicitacaoDao.listarPaginado(pp);
	}

	@Override
	public TipoSolicitacaoDTO consultarTipoSolicitacaoSemViculoSolicitacao(Long id) throws DaoException {
		return tipoSolicitacaoDao.consultarCategoriaSemViculoSolicitacao(id);
	}
}
