package br.com.vitae.bus.service;

import java.util.Collection;
import java.util.List;

import br.com.arquitetura.servico.IService;
import br.com.vitae.bus.dto.MenuDTO;
import br.com.vitae.bus.entity.Menu;

public interface IMenuServico extends IService<Menu> {

	/**
	 * 
	 * 
	 * @param login
	 * @param senha
	 * @return
	 */
	List<Menu> listarMenu(Long idPerfilUsuario);

	Collection<MenuDTO> carregarMenu(Long idPerfilUsuario);

	List<Menu> listarPaginaNaoSelecionadas(Long idPerfilUsuario);

	List<Menu> listarPaginaSelecionadas(Long idPerfilUsuario);

}
