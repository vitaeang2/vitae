package br.com.vitae.bus.service.impl;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.servico.ServiceImpl;
import br.com.arquitetura.util.IsNullUtil;
import br.com.arquitetura.vo.PaginacaoDTO;
import br.com.arquitetura.vo.PaginacaoParams;
import br.com.vitae.bus.conv.FornecedorConverter;
import br.com.vitae.bus.dao.FornecedorDAO;
import br.com.vitae.bus.dto.FornecedorDTO;
import br.com.vitae.bus.entity.Fornecedor;
import br.com.vitae.bus.service.IFornecedorServico;

@SuppressWarnings("serial")
@Service
public class FornecedorServicoImpl extends ServiceImpl<Fornecedor> implements IFornecedorServico {

	@Autowired
	private FornecedorDAO fornecedorDao;

	@Override
	@Transactional(readOnly = true)
	public FornecedorDTO consultarIdDTO(Long id) {

		Fornecedor fornecedor = consultar(Fornecedor.class, id);
		return FornecedorConverter.getInstance().entidade2Dto(fornecedor);
	}

	@Override
	@Transactional(readOnly = false)
	public void salvarOuAtualizar(FornecedorDTO fornecedorDTO) {

		Fornecedor fornecedor = FornecedorConverter.getInstance().dto2Entidade(fornecedorDTO);

		if (fornecedor != null && IsNullUtil.isNullOrEmpty(fornecedor.getId())) {

			fornecedor.setAtivo(Boolean.TRUE);

			if (fornecedor.getPessoa() != null) {

				fornecedor.getPessoa().setDataCadastro(LocalDate.now());
			}
		}
		salvarOuAtualizar(fornecedor);
	}

	@Override
	@Transactional(readOnly = true)
	public PaginacaoDTO<FornecedorDTO> listarPaginado(PaginacaoParams pp) throws DaoException {

		return fornecedorDao.listarPaginado(pp);
	}

	@Override
	@Transactional(readOnly = true)
	public List<FornecedorDTO> listarCombo() {

		List<Fornecedor> lista = fornecedorDao.listarCombo();
		return FornecedorConverter.getInstance().listEntidade2ListDto(lista);
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<FornecedorDTO> listarComboPrestadorServico() {
		
		List<Fornecedor> lista = fornecedorDao.listarComboPrestadorServico();
		return FornecedorConverter.getInstance().listEntidade2ListDto(lista);
	}

	@Override
	@Transactional(readOnly = true)
	public List<FornecedorDTO> listarFiltroCotacao(String valor) {
		List<Fornecedor> lista = fornecedorDao.listarFiltroCotacao(valor);
		return FornecedorConverter.getInstance().listEntidade2ListDto(lista);
	}

}
