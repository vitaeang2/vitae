package br.com.vitae.bus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource({ "classpath:application-config.xml", "classpath:mail-config.xml" })
public class Boot  extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(Boot.class, args);
	}

	// @RequestMapping("/")
	// @ResponseBody
	// public String home()
	// {
	// return "home";
	// }
}
