package br.com.vitae.bus;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.core.JsonTokenId;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.DeserializationFeature;

/**
 * @author Felipe Leonhardt
 * {@link http://www.vision-ti.com.br} 
 * A atual versão do jackson nao suporta o formato 2016-04-01T03:00:00.000Z
 * Então tivemos que reescrever essa classe
 */
public class LocalDateTimeDeserializer extends com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer {

	private static final long serialVersionUID = 1L;
	
	public static final LocalDateTimeDeserializer INSTANCE = new LocalDateTimeDeserializer();

	private LocalDateTimeDeserializer() {
		this(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
	}

	public LocalDateTimeDeserializer(DateTimeFormatter formatter) {
		super(formatter);
	}

	@Override
	public LocalDateTime deserialize(JsonParser parser, DeserializationContext context) throws IOException {
		if (parser.hasTokenId(JsonTokenId.ID_STRING)) {
			String string = parser.getText().trim();
			if (string.length() == 0) {
				return null;
			}
			return LocalDateTime.parse(string, _formatter);
		}
		if (parser.isExpectedStartArrayToken()) {
			if (parser.nextToken() == JsonToken.END_ARRAY) {
				return null;
			}
			int year = parser.getIntValue();

			parser.nextToken();
			int month = parser.getIntValue();

			parser.nextToken();
			int day = parser.getIntValue();

			parser.nextToken();
			int hour = parser.getIntValue();

			parser.nextToken();
			int minute = parser.getIntValue();

			if (parser.nextToken() != JsonToken.END_ARRAY) {
				int second = parser.getIntValue();

				if (parser.nextToken() != JsonToken.END_ARRAY) {
					int partialSecond = parser.getIntValue();
					if (partialSecond < 1_000
							&& !context.isEnabled(DeserializationFeature.READ_DATE_TIMESTAMPS_AS_NANOSECONDS))
						partialSecond *= 1_000_000; // value is milliseconds,
													// convert it to nanoseconds

					if (parser.nextToken() != JsonToken.END_ARRAY) {
						throw context.wrongTokenException(parser, JsonToken.END_ARRAY, "Expected array to end.");
					}
					return LocalDateTime.of(year, month, day, hour, minute, second, partialSecond);
				}
				return LocalDateTime.of(year, month, day, hour, minute, second);
			}
			return LocalDateTime.of(year, month, day, hour, minute);
		}
		throw context.wrongTokenException(parser, JsonToken.START_ARRAY, "Expected array or string.");
	}
}
