package br.com.vitae.bus.dto;

import java.math.BigDecimal;

import org.springframework.format.annotation.DateTimeFormat;

import br.com.arquitetura.model.BaseDTO;
import br.com.arquitetura.util.DataUtil;
import br.com.vitae.bus.entity.ManutencaoPatrimonio;

@SuppressWarnings("serial")
public class ManutencaoPatrimonioDTO implements BaseDTO {

	private Long id;

	private String motivo;

	private String descricaoRetorno;

	private FornecedorDTO fornecedor;

	private PatrimonioDTO patrimonio;

	@DateTimeFormat(pattern = "dd/MM/yyyy hh:mm:ss")
	private String dataSaida;

	@DateTimeFormat(pattern = "dd/MM/yyyy hh:mm:ss")
	private String dataRetorno;

	@DateTimeFormat(pattern = "dd/MM/yyyy hh:mm:ss")
	private String dataPrevistaRetorno;

	@DateTimeFormat(pattern = "dd/MM/yyyy hh:mm:ss")
	private String fimGarantia;

	private BigDecimal valor;

	private BigDecimal valorPago;

	private Boolean ativo;

	public ManutencaoPatrimonioDTO() {

	}

	public ManutencaoPatrimonioDTO(Long id,

			String motivo,

			String descricaoRetorno,

			FornecedorDTO fornecedor,

			PatrimonioDTO patrimonio,

			String dataSaida,

			String dataRetorno,

			String dataPrevistaRetorno,

			String fimGarantia,

			BigDecimal valor,

			BigDecimal valorPago,

			Boolean ativo) {

		super();
		this.id = id;

		this.motivo = motivo;

		this.descricaoRetorno = descricaoRetorno;

		this.fornecedor = fornecedor;

		this.patrimonio = patrimonio;

		this.dataSaida = dataSaida;

		this.dataRetorno = dataRetorno;

		this.dataPrevistaRetorno = dataPrevistaRetorno;

		this.fimGarantia = fimGarantia;

		this.valor = valor;

		this.valorPago = valorPago;

		this.ativo = ativo;
	}

	public ManutencaoPatrimonioDTO(ManutencaoPatrimonio manutencaoPatrimonio) {

		this.id = manutencaoPatrimonio.getId();

		this.motivo = manutencaoPatrimonio.getMotivo();

		this.descricaoRetorno = manutencaoPatrimonio.getDescricaoRetorno();

		if (manutencaoPatrimonio.getFornecedor() != null) {

			this.fornecedor = new FornecedorDTO(manutencaoPatrimonio.getFornecedor());
		}

		if (manutencaoPatrimonio.getPatrimonio() != null) {

			this.patrimonio = new PatrimonioDTO(manutencaoPatrimonio.getPatrimonio());
		}

		if (manutencaoPatrimonio.getDataSaida() != null) {
			this.dataSaida = DataUtil.localDateToStr(manutencaoPatrimonio.getDataSaida());
		}

		if (manutencaoPatrimonio.getDataRetorno() != null) {
			this.dataRetorno = DataUtil.localDateToStr(manutencaoPatrimonio.getDataRetorno());
		}

		if (manutencaoPatrimonio.getDataPrevistaRetorno() != null) {
			this.dataPrevistaRetorno = DataUtil.localDateToStr(manutencaoPatrimonio.getDataPrevistaRetorno());
		}

		if (manutencaoPatrimonio.getFimGarantia() != null) {
			this.fimGarantia = DataUtil.localDateToStr(manutencaoPatrimonio.getFimGarantia());
		}

		this.valor = manutencaoPatrimonio.getValor();

		this.valorPago = manutencaoPatrimonio.getValorPago();

		this.ativo = manutencaoPatrimonio.getAtivo();
	}

	/**
	 * Retorna o valor do atributo <code>id</code>
	 *
	 * @return <code>Long</code>
	 */
	public Long getId() {

		return id;
	}

	/**
	 * Define o valor do atributo <code>id</code>.
	 *
	 * @param id
	 */
	public void setId(Long id) {

		this.id = id;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public String getDescricaoRetorno() {
		return descricaoRetorno;
	}

	public void setDescricaoRetorno(String descricaoRetorno) {
		this.descricaoRetorno = descricaoRetorno;
	}

	public FornecedorDTO getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(FornecedorDTO fornecedor) {
		this.fornecedor = fornecedor;
	}

	public PatrimonioDTO getPatrimonio() {
		return patrimonio;
	}

	public void setPatrimonio(PatrimonioDTO patrimonio) {
		this.patrimonio = patrimonio;
	}

	public String getDataSaida() {
		return dataSaida;
	}

	public void setDataSaida(String dataSaida) {
		this.dataSaida = dataSaida;
	}

	public String getDataRetorno() {
		return dataRetorno;
	}

	public void setDataRetorno(String dataRetorno) {
		this.dataRetorno = dataRetorno;
	}

	public String getDataPrevistaRetorno() {
		return dataPrevistaRetorno;
	}

	public void setDataPrevistaRetorno(String dataPrevistaRetorno) {
		this.dataPrevistaRetorno = dataPrevistaRetorno;
	}

	public String getFimGarantia() {
		return fimGarantia;
	}

	public void setFimGarantia(String fimGarantia) {
		this.fimGarantia = fimGarantia;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public BigDecimal getValorPago() {
		return valorPago;
	}

	public void setValorPago(BigDecimal valorPago) {
		this.valorPago = valorPago;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

}
