package br.com.vitae.bus.dto;

import br.com.arquitetura.model.BaseDTO;
import br.com.vitae.bus.entity.PerfilUsuario;
import br.com.vitae.bus.entity.Usuario;

@SuppressWarnings("serial")
public class UsuarioDTO implements BaseDTO {

	private Long id;

	private String nome;

	private String senha;

	private String token;

	private Long idPerfil;

	private boolean primeiroAcesso;

	private ColaboradorDTO colaborador;

	public UsuarioDTO() {
	}

	// public UsuarioDTO( Long id, String nome, String login, String senha ) {
	// super();
	// this.id = id;
	// this.nome = nome;
	// this.login = login;
	// this.senha = senha;
	// }

	public UsuarioDTO( Usuario usuario ) {
		if (usuario == null) {
			return;
		}
		this.id = usuario.getId();
		this.senha = usuario.getSenha();
		this.token = usuario.getToken();
		this.primeiroAcesso = usuario.isPrimeiroAcesso();
		PerfilUsuario perfil = usuario.getPerfil();
		if (perfil != null) {
			this.idPerfil = perfil.getId();
		}
		if (usuario.getColaborador() != null) {
			this.colaborador = new ColaboradorDTO(usuario.getColaborador());
			if (colaborador.getPessoa() != null) {
				this.nome = colaborador.getPessoa().getNome();
			}
		}
	}

	public Long getId() {

		return id;
	}

	public void setId(Long id) {

		this.id = id;
	}

	public String getSenha() {

		return senha;
	}

	public void setSenha(String senha) {

		this.senha = senha;
	}

	public String getToken() {

		return token;
	}

	public void setToken(String token) {

		this.token = token;
	}

	public Long getIdPerfil() {

		return idPerfil;
	}

	public void setIdPerfil(Long idPerfil) {

		this.idPerfil = idPerfil;
	}

	public ColaboradorDTO getColaborador() {

		return colaborador;
	}

	public void setColaborador(ColaboradorDTO colaborador) {

		this.colaborador = colaborador;
	}

	public String getNome() {

		return nome;
	}

	public void setNome(String nome) {

		this.nome = nome;
	}

	public boolean isPrimeiroAcesso() {

		return primeiroAcesso;
	}

	public void setPrimeiroAcesso(boolean primeiroAcesso) {

		this.primeiroAcesso = primeiroAcesso;
	}

}
