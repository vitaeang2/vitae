package br.com.vitae.bus.dto;

import br.com.arquitetura.model.BaseDTO;
import br.com.vitae.bus.entity.PerfilUsuario;

@SuppressWarnings("serial")
public class PerfilUsuarioDTO implements BaseDTO {

	private Long id;

	private String nome;
	
	public PerfilUsuarioDTO() {
	}

	public PerfilUsuarioDTO(PerfilUsuario entidade) {
		this(entidade.getId(), entidade.getNomePerfil());
	}

	public PerfilUsuarioDTO(Long id, String nomePerfil) {
		this.id = id;
		this.nome = nomePerfil;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}