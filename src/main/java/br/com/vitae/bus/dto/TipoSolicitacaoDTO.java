package br.com.vitae.bus.dto;

import br.com.arquitetura.model.BaseDTO;
import br.com.vitae.bus.entity.TipoSolicitacao;

/**
 * 
 * <p>
 * <b>Title:</b> TipoSolicitacaoDTO.java
 * </p>
 * 
 * <p>
 * <b>Description:</b>
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnlogia da Informacao
 * </p>
 * 
 * @author Rogério Ferreira - rogerio0308@homtail.com
 * 
 * @version 1.0.0
 */
@SuppressWarnings("serial")
public class TipoSolicitacaoDTO implements BaseDTO {

	private Long id;
	
	private Boolean ativo;
	
	private String nome;

	public TipoSolicitacaoDTO() {
	}

	public TipoSolicitacaoDTO( Long id, String nome, boolean ativo ) {
		super();
		this.id = id;
		this.nome = nome;
		this.ativo = ativo;
	}

	public TipoSolicitacaoDTO( TipoSolicitacao tipoSolicitacao ) {
		this(tipoSolicitacao.getId(), tipoSolicitacao.getNome(), tipoSolicitacao.getAtivo() == null ? false : tipoSolicitacao.getAtivo());
	}

	public Long getId() {

		return id;
	}

	public void setId(Long id) {

		this.id = id;
	}

	public String getNome() {

		return nome;
	}

	public void setNome(String nome) {

		this.nome = nome;
	}

	
	/**
	 * Retorna o valor do atributo <code>ativo</code>
	 *
	 * @return <code>Boolean</code>
	 */
	public Boolean getAtivo() {
	
		return ativo;
	}

	
	/**
	 * Define o valor do atributo <code>ativo</code>.
	 *
	 * @param ativo 
	 */
	public void setAtivo(Boolean ativo) {
	
		this.ativo = ativo;
	}
	
	

}
