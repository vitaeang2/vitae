package br.com.vitae.bus.dto;

import java.math.BigDecimal;

import org.springframework.format.annotation.DateTimeFormat;

import br.com.arquitetura.model.BaseDTO;
import br.com.arquitetura.util.DataUtil;
import br.com.vitae.bus.entity.MovimentoEstoque;

/**
 * 
 * <p>
 * <b>Title:</b> MovimentoEstoqueDTO.java
 * </p>
 * 
 * <p>
 * <b>Description:</b>
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnlogia da Informacao
 * </p>
 * 
 * @author Joaquim Barros - joaquimbarros.cmp@gmail.com
 * 
 * @version 1.0.0
 */
@SuppressWarnings("serial")
public class MovimentoEstoqueDTO implements BaseDTO {

	private Long id;

	@DateTimeFormat(pattern = "dd/MM/yyyy hh:mm:ss")
	private String data;

	private int tipoMovimentoEstoque;

	private String tipoMovimentoEstoqueStr;

	private ProdutoDTO produto;

	private BigDecimal quantidadeMovimentada;

	private BigDecimal valor;

	private BigDecimal saldo;

	private ColaboradorDTO colaborador;

	private EmpresaDTO empresa;

	private NucleoDTO nucleo;

	private String observacao;

	public MovimentoEstoqueDTO() {
	}

	public MovimentoEstoqueDTO( MovimentoEstoque movimento ) {

		this.id = movimento.getId();

		if (movimento.getEmpresa() != null) {

			this.empresa = new EmpresaDTO(movimento.getEmpresa());
		}

		if (movimento.getColaborador() != null) {

			this.colaborador = new ColaboradorDTO(movimento.getColaborador());
		}

		if (movimento.getNucleo() != null) {

			this.nucleo = new NucleoDTO(movimento.getNucleo());
		}

		if (movimento.getProduto() != null) {

			this.produto = new ProdutoDTO(movimento.getProduto());
		}

		this.data = DataUtil.localDateToStr(movimento.getData());

		this.observacao = movimento.getObservacao();

		this.quantidadeMovimentada = movimento.getQuantidadeMovimentada();

		this.valor = movimento.getValor();

		this.tipoMovimentoEstoque = movimento.getTipoMovimentoEstoque().getCodigo();
		this.tipoMovimentoEstoqueStr = movimento.getTipoMovimentoEstoque().getDescricao();

		this.setSaldo(movimento.getSaldo());

	}

	/**
	 * Retorna o valor do atributo <code>id</code>
	 *
	 * @return <code>Long</code>
	 */
	public Long getId() {

		return id;
	}

	/**
	 * Define o valor do atributo <code>id</code>.
	 *
	 * @param id
	 */
	public void setId(Long id) {

		this.id = id;
	}

	/**
	 * Retorna o valor do atributo <code>data</code>
	 *
	 * @return <code>String</code>
	 */
	public String getData() {

		return data;
	}

	/**
	 * Define o valor do atributo <code>data</code>.
	 *
	 * @param data
	 */
	public void setData(String data) {

		this.data = data;
	}

	/**
	 * Retorna o valor do atributo <code>tipoMovimentoEstoque</code>
	 *
	 * @return <code>int</code>
	 */
	public int getTipoMovimentoEstoque() {

		return tipoMovimentoEstoque;
	}

	/**
	 * Define o valor do atributo <code>tipoMovimentoEstoque</code>.
	 *
	 * @param tipoMovimentoEstoque
	 */
	public void setTipoMovimentoEstoque(int tipoMovimentoEstoque) {

		this.tipoMovimentoEstoque = tipoMovimentoEstoque;
	}

	/**
	 * Retorna o valor do atributo <code>produto</code>
	 *
	 * @return <code>ProdutoDTO</code>
	 */
	public ProdutoDTO getProduto() {

		return produto;
	}

	/**
	 * Define o valor do atributo <code>produto</code>.
	 *
	 * @param produto
	 */
	public void setProduto(ProdutoDTO produto) {

		this.produto = produto;
	}

	/**
	 * Retorna o valor do atributo <code>quantidadeMovimentada</code>
	 *
	 * @return <code>BigDecimal</code>
	 */
	public BigDecimal getQuantidadeMovimentada() {

		return quantidadeMovimentada;
	}

	/**
	 * Define o valor do atributo <code>quantidadeMovimentada</code>.
	 *
	 * @param quantidadeMovimentada
	 */
	public void setQuantidadeMovimentada(BigDecimal quantidadeMovimentada) {

		this.quantidadeMovimentada = quantidadeMovimentada;
	}

	/**
	 * Retorna o valor do atributo <code>valor</code>
	 *
	 * @return <code>BigDecimal</code>
	 */
	public BigDecimal getValor() {

		return valor;
	}

	/**
	 * Define o valor do atributo <code>valor</code>.
	 *
	 * @param valor
	 */
	public void setValor(BigDecimal valor) {

		this.valor = valor;
	}

	/**
	 * Retorna o valor do atributo <code>saldo</code>
	 *
	 * @return <code>BigDecimal</code>
	 */
	public BigDecimal getSaldo() {

		return saldo;
	}

	/**
	 * Define o valor do atributo <code>saldo</code>.
	 *
	 * @param saldo
	 */
	public void setSaldo(BigDecimal saldo) {

		this.saldo = saldo;
	}

	/**
	 * Retorna o valor do atributo <code>colaborador</code>
	 *
	 * @return <code>ColaboradorDTO</code>
	 */
	public ColaboradorDTO getColaborador() {

		return colaborador;
	}

	/**
	 * Define o valor do atributo <code>colaborador</code>.
	 *
	 * @param colaborador
	 */
	public void setColaborador(ColaboradorDTO colaborador) {

		this.colaborador = colaborador;
	}

	/**
	 * Retorna o valor do atributo <code>empresa</code>
	 *
	 * @return <code>EmpresaDTO</code>
	 */
	public EmpresaDTO getEmpresa() {

		return empresa;
	}

	/**
	 * Define o valor do atributo <code>empresa</code>.
	 *
	 * @param empresa
	 */
	public void setEmpresa(EmpresaDTO empresa) {

		this.empresa = empresa;
	}

	/**
	 * Retorna o valor do atributo <code>nucleo</code>
	 *
	 * @return <code>NucleoDTO</code>
	 */
	public NucleoDTO getNucleo() {

		return nucleo;
	}

	/**
	 * Define o valor do atributo <code>nucleo</code>.
	 *
	 * @param nucleo
	 */
	public void setNucleo(NucleoDTO nucleo) {

		this.nucleo = nucleo;
	}

	/**
	 * Retorna o valor do atributo <code>observacao</code>
	 *
	 * @return <code>String</code>
	 */
	public String getObservacao() {

		return observacao;
	}

	/**
	 * Define o valor do atributo <code>observacao</code>.
	 *
	 * @param observacao
	 */
	public void setObservacao(String observacao) {

		this.observacao = observacao;
	}

	public String getTipoMovimentoEstoqueStr() {

		return tipoMovimentoEstoqueStr;
	}

	public void setTipoMovimentoEstoqueStr(String tipoMovimentoEstoqueStr) {

		this.tipoMovimentoEstoqueStr = tipoMovimentoEstoqueStr;
	}

}
