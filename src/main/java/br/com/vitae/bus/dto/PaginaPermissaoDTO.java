package br.com.vitae.bus.dto;

import br.com.arquitetura.model.BaseDTO;

@SuppressWarnings("serial")
public class PaginaPermissaoDTO implements BaseDTO {

	private Long id;

	private Long idPapel;

	private Long idPagina;

	private String nome;

	private String nomePagina;

	private boolean visualizar;

	private boolean inserir;

	private boolean editar;

	private boolean excluir;

	private boolean selecionada;

	public Long getId() {

		return id;
	}

	public void setId(Long id) {

		this.id = id;
	}

	public Long getIdPapel() {

		return idPapel;
	}

	public void setIdPapel(Long idPapel) {

		this.idPapel = idPapel;
	}

	public String getNome() {

		return nome;
	}

	public void setNome(String nome) {

		this.nome = nome;
	}

	public boolean isVisualizar() {

		return visualizar;
	}

	public void setVisualizar(boolean visualizar) {

		this.visualizar = visualizar;
	}

	public boolean isInserir() {

		return inserir;
	}

	public void setInserir(boolean inserir) {

		this.inserir = inserir;
	}

	public boolean isEditar() {

		return editar;
	}

	public void setEditar(boolean editar) {

		this.editar = editar;
	}

	public boolean isExcluir() {

		return excluir;
	}

	public void setExcluir(boolean excluir) {

		this.excluir = excluir;
	}

	public Long getIdPagina() {

		return idPagina;
	}

	public void setIdPagina(Long idPagina) {

		this.idPagina = idPagina;
	}

	public String getNomePagina() {

		return nomePagina;
	}

	public void setNomePagina(String nomePagina) {

		this.nomePagina = nomePagina;
	}

	public boolean isSelecionada() {

		return selecionada;
	}

	public void setSelecionada(boolean selecionada) {

		this.selecionada = selecionada;
	}

}
