package br.com.vitae.bus.dto;

import br.com.arquitetura.model.BaseDTO;
import br.com.vitae.bus.entity.CategoriaGrupo;

/**
 * 
 * <p>
 * <b>Title:</b> CategoriaGrupoDTO.java
 * </p>
 * 
 * <p>
 * <b>Description:</b>
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnlogia da Informacao
 * </p>
 * 
 * @author Rogério Ferreira - rogerio0308@hotmail.com
 * 
 * @version 1.0.0
 */
@SuppressWarnings("serial")
public class CategoriaGrupoDTO implements BaseDTO {

	private Long id;

	private Boolean ativo;

	private String nome;

	private GrupoDTO grupo;

	public CategoriaGrupoDTO() {
	}

	public CategoriaGrupoDTO(Long id, String nome, GrupoDTO grupo, boolean ativo) {
		super();
		this.id = id;
		this.nome = nome;
		this.grupo = grupo;
		this.ativo = ativo;
	}

	public CategoriaGrupoDTO(CategoriaGrupo categoriaGrupo) {
		this.id = categoriaGrupo.getId();
		this.nome = categoriaGrupo.getNome();

		if (categoriaGrupo.getGrupo() != null) {
			this.grupo = new GrupoDTO(categoriaGrupo.getGrupo());
		}
		this.ativo = categoriaGrupo.getAtivo();

	}

	public Long getId() {

		return id;
	}

	public void setId(Long id) {

		this.id = id;
	}

	public String getNome() {

		return nome;
	}

	public void setNome(String nome) {

		this.nome = nome;
	}

	/**
	 * Retorna o valor do atributo <code>ativo</code>
	 *
	 * @return <code>Boolean</code>
	 */
	public Boolean getAtivo() {

		return ativo;
	}

	/**
	 * Define o valor do atributo <code>ativo</code>.
	 *
	 * @param ativo
	 */
	public void setAtivo(Boolean ativo) {

		this.ativo = ativo;
	}

	public GrupoDTO getGrupo() {
		return grupo;
	}

	public void setGrupo(GrupoDTO grupo) {
		this.grupo = grupo;
	}

}
