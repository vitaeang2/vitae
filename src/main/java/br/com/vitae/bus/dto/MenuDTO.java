package br.com.vitae.bus.dto;

import java.util.LinkedHashSet;
import java.util.Set;

import br.com.arquitetura.model.BaseDTO;
import br.com.vitae.bus.entity.Menu;

@SuppressWarnings("serial")
public class MenuDTO implements BaseDTO {

	private Long id;

	private Long idPai;

	private String name;

	private String caminho;

	private String icon;

	private Set<SubMenuDTO> submenus;

	public MenuDTO( Long id, String name, String icon ) {
		this(id, name, null, icon);
	}

	public MenuDTO( Long id, String name, String caminho, String icon ) {
		super();
		this.id = id;
		this.name = name;
		this.caminho = caminho;
		this.icon = icon;
	}

	public MenuDTO( Menu menu ) {
		this.id = menu.getId();
		this.caminho = menu.getNomePagina();
		this.name = menu.getNome();

		if (menu.getMenuPai() != null) {
			this.idPai = menu.getMenuPai().getId();
		}
	}

	public void addSubmenu(Long id, String name, String caminho) {

		if (submenus == null) {
			this.submenus = new LinkedHashSet<>();
		}
		this.submenus.add(new SubMenuDTO(id, name, caminho));
	}

	public String getName() {

		return name;
	}

	public void setName(String name) {

		this.name = name;
	}

	public String getCaminho() {

		return caminho;
	}

	public void setCaminho(String caminho) {

		this.caminho = caminho;
	}

	public String getIcon() {

		return icon;
	}

	public void setIcon(String icon) {

		this.icon = icon;
	}

	public Set<SubMenuDTO> getSubmenus() {

		return submenus;
	}

	public void setSubmenus(Set<SubMenuDTO> submenus) {

		this.submenus = submenus;
	}

	public Long getId() {

		return id;
	}

	public void setId(Long id) {

		this.id = id;
	}

	public Long getIdPai() {

		return idPai;
	}

	public void setIdPai(Long idPai) {

		this.idPai = idPai;
	}

}
