package br.com.vitae.bus.dto;

import br.com.arquitetura.model.BaseDTO;
import br.com.vitae.bus.entity.CategoriaProduto;

/**
 * 
 * <p>
 * <b>Title:</b> CategoriaProdutoDTO.java
 * </p>
 * 
 * <p>
 * <b>Description:</b>
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnlogia da Informacao
 * </p>
 * 
 * @author Joaquim Barros - joaquimbarros.cmp@gmail.com
 * 
 * @version 1.0.0
 */
@SuppressWarnings("serial")
public class CategoriaProdutoDTO implements BaseDTO {

	private Long id;
	
	private Boolean ativo;
	
	private String nome;

	public CategoriaProdutoDTO() {
	}

	public CategoriaProdutoDTO( Long id, String nome, boolean ativo ) {
		super();
		this.id = id;
		this.nome = nome;
		this.ativo = ativo;
	}

	public CategoriaProdutoDTO( CategoriaProduto categoriaProduto ) {
		this(categoriaProduto.getId(), categoriaProduto.getNome(), categoriaProduto.getAtivo() == null ? false : categoriaProduto.getAtivo());
	}

	public Long getId() {

		return id;
	}

	public void setId(Long id) {

		this.id = id;
	}

	public String getNome() {

		return nome;
	}

	public void setNome(String nome) {

		this.nome = nome;
	}

	
	/**
	 * Retorna o valor do atributo <code>ativo</code>
	 *
	 * @return <code>Boolean</code>
	 */
	public Boolean getAtivo() {
	
		return ativo;
	}

	
	/**
	 * Define o valor do atributo <code>ativo</code>.
	 *
	 * @param ativo 
	 */
	public void setAtivo(Boolean ativo) {
	
		this.ativo = ativo;
	}
	
	

}
