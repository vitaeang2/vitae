package br.com.vitae.bus.dto;

import java.math.BigDecimal;

import org.springframework.format.annotation.DateTimeFormat;

import br.com.arquitetura.model.BaseDTO;
import br.com.vitae.bus.entity.Produto;

@SuppressWarnings("serial")
public class ProdutoDTO implements BaseDTO {

	private Long id;

	private String nome;

	@DateTimeFormat(pattern = "dd/MM/yyyy hh:mm:ss")
	private String dataCadastro;

	private String codigo;

	private BigDecimal valorVenda;

	private BigDecimal valorCusto;

	// private ProdutoEstoqueDTO produtoEstoque;

	private BigDecimal minimoEmEstoque;

	private BigDecimal maximoEmEstoque;

	private int unidadeMedida;

	private Boolean ativo;

	private FornecedorDTO fornecedor;

	private FabricanteDTO fabricante;

	private CategoriaProdutoDTO categoria;

	public ProdutoDTO() {

	}

	public ProdutoDTO( Long id, String nome, String dataCadastro, String naturalidade, String codigo, BigDecimal valorVenda, BigDecimal valorCusto, ProdutoEstoqueDTO produtoEstoque, BigDecimal minimoEmEstoque, BigDecimal maximoEmEstoque, int unidadeMedida, FornecedorDTO fornecedor, FabricanteDTO fabricante, CategoriaProdutoDTO categoria, Boolean ativo ) {

		super();
		this.id = id;
		this.nome = nome;
		this.dataCadastro = dataCadastro;
		this.codigo = codigo;
		this.valorVenda = valorVenda;
		this.valorCusto = valorCusto;
		// this.produtoEstoque = produtoEstoque;
		this.minimoEmEstoque = minimoEmEstoque;
		this.maximoEmEstoque = maximoEmEstoque;
		this.unidadeMedida = unidadeMedida;
		this.fornecedor = fornecedor;
		this.fabricante = fabricante;
		this.categoria = categoria;
		this.ativo = ativo;
	}

	public ProdutoDTO( Produto produto ) {

		this.id = produto.getId();

		// this.dataCadastro = produto.getDataCadastro();

		this.nome = produto.getNome();

		this.codigo = produto.getCodigo();

		this.valorVenda = produto.getValorVenda();

		this.valorCusto = produto.getValorCusto();

		this.minimoEmEstoque = produto.getMinimoEmEstoque();

		this.maximoEmEstoque = produto.getMaximoEmEstoque();

		this.ativo = produto.getAtivo();

		if (produto.getUnidadeMedida() != null) {

			this.unidadeMedida = produto.getUnidadeMedida().getCodigo();
		}

		/*
		 * if (produto.getProdutoEstoque() != null) {
		 * 
		 * this.produtoEstoque = new ProdutoEstoqueDTO(produto.getProdutoEstoque()); }
		 */
		if (produto.getFornecedor() != null) {

			this.fornecedor = new FornecedorDTO(produto.getFornecedor());
		}

		if (produto.getFabricante() != null) {

			this.fabricante = new FabricanteDTO(produto.getFabricante());
		}

		if (produto.getCategoria() != null) {

			this.categoria = new CategoriaProdutoDTO(produto.getCategoria());
		}

	}

	/**
	 * Retorna o valor do atributo <code>id</code>
	 *
	 * @return <code>Long</code>
	 */
	public Long getId() {

		return id;
	}

	/**
	 * Define o valor do atributo <code>id</code>.
	 *
	 * @param id
	 */
	public void setId(Long id) {

		this.id = id;
	}

	/**
	 * Retorna o valor do atributo <code>nome</code>
	 *
	 * @return <code>String</code>
	 */
	public String getNome() {

		return nome;
	}

	/**
	 * Define o valor do atributo <code>nome</code>.
	 *
	 * @param nome
	 */
	public void setNome(String nome) {

		this.nome = nome;
	}

	/**
	 * Retorna o valor do atributo <code>dataCadastro</code>
	 *
	 * @return <code>String</code>
	 */
	public String getDataCadastro() {

		return dataCadastro;
	}

	/**
	 * Define o valor do atributo <code>dataCadastro</code>.
	 *
	 * @param dataCadastro
	 */
	public void setDataCadastro(String dataCadastro) {

		this.dataCadastro = dataCadastro;
	}

	/**
	 * Retorna o valor do atributo <code>codigo</code>
	 *
	 * @return <code>String</code>
	 */
	public String getCodigo() {

		return codigo;
	}

	/**
	 * Define o valor do atributo <code>codigo</code>.
	 *
	 * @param codigo
	 */
	public void setCodigo(String codigo) {

		this.codigo = codigo;
	}

	/**
	 * Retorna o valor do atributo <code>valorVenda</code>
	 *
	 * @return <code>BigDecimal</code>
	 */
	public BigDecimal getValorVenda() {

		return valorVenda;
	}

	/**
	 * Define o valor do atributo <code>valorVenda</code>.
	 *
	 * @param valorVenda
	 */
	public void setValorVenda(BigDecimal valorVenda) {

		this.valorVenda = valorVenda;
	}

	/**
	 * Retorna o valor do atributo <code>valorCusto</code>
	 *
	 * @return <code>BigDecimal</code>
	 */
	public BigDecimal getValorCusto() {

		return valorCusto;
	}

	/**
	 * Define o valor do atributo <code>valorCusto</code>.
	 *
	 * @param valorCusto
	 */
	public void setValorCusto(BigDecimal valorCusto) {

		this.valorCusto = valorCusto;
	}

	/**
	 * Retorna o valor do atributo <code>minimoEmEstoque</code>
	 *
	 * @return <code>BigDecimal</code>
	 */
	public BigDecimal getMinimoEmEstoque() {

		return minimoEmEstoque;
	}

	/**
	 * Define o valor do atributo <code>minimoEmEstoque</code>.
	 *
	 * @param minimoEmEstoque
	 */
	public void setMinimoEmEstoque(BigDecimal minimoEmEstoque) {

		this.minimoEmEstoque = minimoEmEstoque;
	}

	/**
	 * Retorna o valor do atributo <code>maximoEmEstoque</code>
	 *
	 * @return <code>BigDecimal</code>
	 */
	public BigDecimal getMaximoEmEstoque() {

		return maximoEmEstoque;
	}

	/**
	 * Define o valor do atributo <code>maximoEmEstoque</code>.
	 *
	 * @param maximoEmEstoque
	 */
	public void setMaximoEmEstoque(BigDecimal maximoEmEstoque) {

		this.maximoEmEstoque = maximoEmEstoque;
	}

	/**
	 * Retorna o valor do atributo <code>unidadeMedida</code>
	 *
	 * @return <code>int</code>
	 */
	public int getUnidadeMedida() {

		return unidadeMedida;
	}

	/**
	 * Define o valor do atributo <code>unidadeMedida</code>.
	 *
	 * @param unidadeMedida
	 */
	public void setUnidadeMedida(int unidadeMedida) {

		this.unidadeMedida = unidadeMedida;
	}

	/**
	 * Retorna o valor do atributo <code>fornecedor</code>
	 *
	 * @return <code>FornecedorDTO</code>
	 */
	public FornecedorDTO getFornecedor() {

		return fornecedor;
	}

	/**
	 * Define o valor do atributo <code>fornecedor</code>.
	 *
	 * @param fornecedor
	 */
	public void setFornecedor(FornecedorDTO fornecedor) {

		this.fornecedor = fornecedor;
	}

	/**
	 * Retorna o valor do atributo <code>fabricante</code>
	 *
	 * @return <code>FabricanteDTO</code>
	 */
	public FabricanteDTO getFabricante() {

		return fabricante;
	}

	/**
	 * Define o valor do atributo <code>fabricante</code>.
	 *
	 * @param fabricante
	 */
	public void setFabricante(FabricanteDTO fabricante) {

		this.fabricante = fabricante;
	}

	/**
	 * Retorna o valor do atributo <code>categoria</code>
	 *
	 * @return <code>CategoriaProdutoDTO</code>
	 */
	public CategoriaProdutoDTO getCategoria() {

		return categoria;
	}

	/**
	 * Define o valor do atributo <code>categoria</code>.
	 *
	 * @param categoria
	 */
	public void setCategoria(CategoriaProdutoDTO categoria) {

		this.categoria = categoria;
	}

	public Boolean getAtivo() {

		return ativo;
	}

	public void setAtivo(Boolean ativo) {

		this.ativo = ativo;
	}

}
