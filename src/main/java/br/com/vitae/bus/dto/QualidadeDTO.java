package br.com.vitae.bus.dto;

import org.springframework.format.annotation.DateTimeFormat;

import br.com.arquitetura.model.BaseDTO;
import br.com.arquitetura.util.DataUtil;
import br.com.vitae.bus.entity.Qualidade;

@SuppressWarnings("serial")
public class QualidadeDTO implements BaseDTO {

	private Long id;

	private Boolean ativo;

	private int registro;

	private int origem;

	private int auditoria;

	@DateTimeFormat(pattern = "dd/MM/yyyy hh:mm:ss")
	private String criacao;

	private UsuarioDTO autor;

	private String identificacao;

	private String analise_causa;

	private String acao_imediata;

	private String acao_preventiva;

	private String avaliacao_eficacia;

	private EmpresaDTO empresa;

	private NucleoDTO nucleo;

	public QualidadeDTO() {

	}

	public QualidadeDTO(Long id,

			Boolean ativo,

			int registro,

			int origem,

			int auditoria,

			String criacao,

			UsuarioDTO autor,

			String identificacao,

			String analise_causa,

			String acao_imediata,

			String acao_preventiva,

			String avaliacao_eficacia,

			EmpresaDTO empresa,

			NucleoDTO nucleo) {
		super();

		this.id = id;

		this.ativo = ativo;

		this.registro = registro;

		this.origem = origem;

		this.auditoria = auditoria;

		this.criacao = criacao;

		this.autor = autor;

		this.identificacao = identificacao;

		this.analise_causa = analise_causa;

		this.acao_imediata = acao_imediata;

		this.acao_preventiva = acao_preventiva;

		this.avaliacao_eficacia = avaliacao_eficacia;

		this.empresa = empresa;

		this.nucleo = nucleo;
	}

	public QualidadeDTO(Qualidade qualidade) {

		this.id = qualidade.getId();
		this.ativo = qualidade.getAtivo();

		if (qualidade.getRegistro() != null) {

			this.registro = qualidade.getRegistro().getCodigo();

		}

		if (qualidade.getOrigem() != null) {

			this.origem = qualidade.getOrigem().getCodigo();

		}

		if (qualidade.getAuditoria() != null) {

			this.auditoria = qualidade.getAuditoria().getCodigo();

		}

		this.criacao = DataUtil.localDateToStr(qualidade.getCriacao());

		if (qualidade.getAutor() != null) {

			this.autor = new UsuarioDTO(qualidade.getAutor());
		}

		this.identificacao = qualidade.getIdentificacao();

		this.analise_causa = qualidade.getAnalise_causa();

		this.acao_imediata = qualidade.getAcao_imediata();

		this.acao_preventiva = qualidade.getAcao_preventiva();

		this.avaliacao_eficacia = qualidade.getAvaliacao_eficacia();

		if (qualidade.getNucleo() != null) {

			this.nucleo = new NucleoDTO(qualidade.getNucleo());
		}

		if (qualidade.getEmpresa() != null) {

			this.empresa = new EmpresaDTO(qualidade.getEmpresa());
		}

	}

	public Long getId() {

		return id;
	}

	public void setId(Long id) {

		this.id = id;
	}

	public NucleoDTO getNucleo() {

		return nucleo;
	}

	public void setNucleo(NucleoDTO nucleo) {

		this.nucleo = nucleo;
	}

	public EmpresaDTO getEmpresa() {

		return empresa;
	}

	public void setEmpresa(EmpresaDTO empresa) {

		this.empresa = empresa;
	}

	public Boolean getAtivo() {

		return ativo;
	}

	public void setAtivo(Boolean ativo) {

		this.ativo = ativo;
	}

	public int getRegistro() {
		return registro;
	}

	public void setRegistro(int registro) {
		this.registro = registro;
	}

	public int getOrigem() {
		return origem;
	}

	public void setOrigem(int origem) {
		this.origem = origem;
	}

	public int getAuditoria() {
		return auditoria;
	}

	public void setAuditoria(int auditoria) {
		this.auditoria = auditoria;
	}

	public String getCriacao() {
		return criacao;
	}

	public void setCriacao(String criacao) {
		this.criacao = criacao;
	}

	public UsuarioDTO getAutor() {
		return autor;
	}

	public void setAutor(UsuarioDTO autor) {
		this.autor = autor;
	}

	public String getIdentificacao() {
		return identificacao;
	}

	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}

	public String getAnalise_causa() {
		return analise_causa;
	}

	public void setAnalise_causa(String analise_causa) {
		this.analise_causa = analise_causa;
	}

	public String getAcao_imediata() {
		return acao_imediata;
	}

	public void setAcao_imediata(String acao_imediata) {
		this.acao_imediata = acao_imediata;
	}

	public String getAcao_preventiva() {
		return acao_preventiva;
	}

	public void setAcao_preventiva(String acao_preventiva) {
		this.acao_preventiva = acao_preventiva;
	}

	public String getAvaliacao_eficacia() {
		return avaliacao_eficacia;
	}

	public void setAvaliacao_eficacia(String avaliacao_eficacia) {
		this.avaliacao_eficacia = avaliacao_eficacia;
	}

}
