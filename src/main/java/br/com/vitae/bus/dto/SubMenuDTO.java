package br.com.vitae.bus.dto;

import br.com.arquitetura.model.BaseDTO;

@SuppressWarnings("serial")
public class SubMenuDTO implements BaseDTO {

	private Long id;

	private String name;

	private String caminho;

	public SubMenuDTO( Long id, String name, String caminho ) {
		super();
		this.id = id;
		this.name = name;
		this.caminho = caminho;
	}

	public String getName() {

		return name;
	}

	public void setName(String name) {

		this.name = name;
	}

	public String getCaminho() {

		return caminho;
	}

	public void setCaminho(String caminho) {

		this.caminho = caminho;
	}

	public Long getId() {

		return id;
	}

	public void setId(Long id) {

		this.id = id;
	}

	@Override
	public int hashCode() {

		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( caminho == null ) ? 0 : caminho.hashCode() );
		result = prime * result + ( ( id == null ) ? 0 : id.hashCode() );
		result = prime * result + ( ( name == null ) ? 0 : name.hashCode() );
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SubMenuDTO other = (SubMenuDTO) obj;
		if (caminho == null) {
			if (other.caminho != null)
				return false;
		} else if (!caminho.equals(other.caminho))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
