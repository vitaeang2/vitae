package br.com.vitae.bus.dto;

import br.com.arquitetura.model.BaseDTO;
import br.com.arquitetura.util.DataUtil;
import br.com.vitae.bus.entity.Cotacao;

/**
 * 
 * <p>
 * <b>Title:</b> CotacaoDTO.java
 * </p>
 * 
 * <p>
 * <b>Description:</b>
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnlogia da Informacao
 * </p>
 * 
 * @author Sergio Filho - sergioadsf@gmail.com
 * 
 * @version 1.0.0
 */
@SuppressWarnings("serial")
public class CotacaoDTO implements BaseDTO {

	private Long id;

	private String descricao;

	private String dataCotacao;

	private String dataValidade;

	private Integer status;

	private EmpresaDTO empresa;

	private NucleoDTO nucleo;

	public CotacaoDTO() {
	}

	public CotacaoDTO( Long id, String descricao, String dataCotacao, String dataValidade, Integer status, EmpresaDTO empresa, NucleoDTO nucleo ) {
		super();
		this.id = id;
		this.descricao = descricao;
		this.dataCotacao = dataCotacao;
		this.dataValidade = dataValidade;
		this.status = status;
		this.empresa = empresa;
		this.nucleo = nucleo;
	}

	public CotacaoDTO( Cotacao c ) {
		this.id = c.getId();
		this.descricao = c.getDescricao();
		this.dataCotacao = DataUtil.localDateToStr(c.getDataCotacao());
		this.dataValidade = DataUtil.localDateToStr(c.getDataValidade());
		this.status = c.getStatus() == null ? 0 : c.getStatus().ordinal();

		if (c.getEmpresa() != null) {
			this.empresa = new EmpresaDTO(c.getEmpresa());
		}

		if (c.getNucleo() != null) {
			this.nucleo = new NucleoDTO(c.getNucleo());
		}
	}

	public Long getId() {

		return id;
	}

	public void setId(Long id) {

		this.id = id;
	}

	public String getDescricao() {

		return descricao;
	}

	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	public String getDataCotacao() {

		return dataCotacao;
	}

	public void setDataCotacao(String dataCotacao) {

		this.dataCotacao = dataCotacao;
	}

	public Integer getStatus() {

		return status;
	}

	public void setStatus(Integer status) {

		this.status = status;
	}

	public String getDataValidade() {

		return dataValidade;
	}

	public void setDataValidade(String dataValidade) {

		this.dataValidade = dataValidade;
	}

	public EmpresaDTO getEmpresa() {

		return empresa;
	}

	public void setEmpresa(EmpresaDTO empresa) {

		this.empresa = empresa;
	}

	public NucleoDTO getNucleo() {

		return nucleo;
	}

	public void setNucleo(NucleoDTO nucleo) {

		this.nucleo = nucleo;
	}

}
