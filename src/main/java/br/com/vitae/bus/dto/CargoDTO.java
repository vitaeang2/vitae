package br.com.vitae.bus.dto;

import br.com.arquitetura.model.BaseDTO;
import br.com.vitae.bus.entity.Cargo;

/**
 * 
 * <p>
 * <b>Title:</b> CargoDTO.java
 * </p>
 * 
 * <p>
 * <b>Description:</b>
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnlogia da Informacao
 * </p>
 * 
 * @author Sergio Filho - sergioadsf@gmail.com
 * 
 * @version 1.0.0
 */
@SuppressWarnings("serial")
public class CargoDTO implements BaseDTO {

	private Long id;
	
	private Boolean ativo;
	
	private String nome;

	public CargoDTO() {
	}

	public CargoDTO( Long id, String nome, boolean ativo ) {
		super();
		this.id = id;
		this.nome = nome;
		this.ativo = ativo;
	}

	public CargoDTO( Cargo cargo ) {
		this(cargo.getId(), cargo.getNome(), cargo.getAtivo() == null ? false : cargo.getAtivo());
	}

	public Long getId() {

		return id;
	}

	public void setId(Long id) {

		this.id = id;
	}

	public String getNome() {

		return nome;
	}

	public void setNome(String nome) {

		this.nome = nome;
	}

	
	/**
	 * Retorna o valor do atributo <code>ativo</code>
	 *
	 * @return <code>Boolean</code>
	 */
	public Boolean getAtivo() {
	
		return ativo;
	}

	
	/**
	 * Define o valor do atributo <code>ativo</code>.
	 *
	 * @param ativo 
	 */
	public void setAtivo(Boolean ativo) {
	
		this.ativo = ativo;
	}
	
	

}
