package br.com.vitae.bus.dto;

import java.math.BigDecimal;

import br.com.arquitetura.model.BaseDTO;
import br.com.vitae.bus.entity.CotacaoFornecedor;

/**
 * 
 * <p>
 * <b>Title:</b> CotacaoFornecedorDTO.java
 * </p>
 * 
 * <p>
 * <b>Description:</b>
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnlogia da Informacao
 * </p>
 * 
 * @author Sergio Filho - sergioadsf@gmail.com
 * 
 * @version 1.0.0
 */
@SuppressWarnings("serial")
public class CotacaoFornecedorDTO implements BaseDTO {

	private Long id;

	private BigDecimal vlrFrete;

	private BigDecimal vlrDesconto;

	private String token;

	private CotacaoDTO cotacao;

	private FornecedorDTO fornecedor;

	private Integer status;

	public CotacaoFornecedorDTO() {
	}

	public CotacaoFornecedorDTO( Long id, BigDecimal vlrFrete, BigDecimal vlrDesconto, String token, CotacaoDTO cotacao, FornecedorDTO fornecedor, Integer status ) {
		super();
		this.id = id;
		this.vlrFrete = vlrFrete;
		this.vlrDesconto = vlrDesconto;
		this.token = token;
		this.cotacao = cotacao;
		this.fornecedor = fornecedor;
		this.status = status;
	}

	public CotacaoFornecedorDTO( CotacaoFornecedor cf ) {
		this.id = cf.getId();
		this.vlrFrete = cf.getVlrFrete();
		this.vlrDesconto = cf.getVlrDesconto();
		this.token = cf.getToken();

		if (cf.getCotacao() != null) {
			this.cotacao = new CotacaoDTO(cf.getCotacao());
		}
		if (cf.getFornecedor() != null) {
			this.fornecedor = new FornecedorDTO(cf.getFornecedor());
		}

		if (cf.getStatus() != null) {
			this.status = cf.getStatus().ordinal();
		}
	}

	public Long getId() {

		return id;
	}

	public void setId(Long id) {

		this.id = id;
	}

	public BigDecimal getVlrFrete() {

		return vlrFrete;
	}

	public void setVlrFrete(BigDecimal vlrFrete) {

		this.vlrFrete = vlrFrete;
	}

	public BigDecimal getVlrDesconto() {

		return vlrDesconto;
	}

	public void setVlrDesconto(BigDecimal vlrDesconto) {

		this.vlrDesconto = vlrDesconto;
	}

	public String getToken() {

		return token;
	}

	public void setToken(String token) {

		this.token = token;
	}

	public CotacaoDTO getCotacao() {

		return cotacao;
	}

	public void setCotacao(CotacaoDTO cotacao) {

		this.cotacao = cotacao;
	}

	public FornecedorDTO getFornecedor() {

		return fornecedor;
	}

	public void setFornecedor(FornecedorDTO fornecedor) {

		this.fornecedor = fornecedor;
	}

	public Integer getStatus() {

		return status;
	}

	public void setStatus(Integer status) {

		this.status = status;
	}

}
