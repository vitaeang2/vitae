package br.com.vitae.bus.dto;

import org.springframework.format.annotation.DateTimeFormat;

import br.com.arquitetura.model.BaseDTO;
import br.com.arquitetura.util.DataUtil;
import br.com.vitae.bus.entity.Pessoa;

@SuppressWarnings("serial")
public class PessoaDTO implements BaseDTO {

	private Long id;

	private String nome;

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private String dataCadastro;

	private String naturalidade;

	private Integer ufNaturalidade;

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private String dataNascimento;

	private int estadoCivil;

	private int sexo;

	private String cpfCnpj;

	private String rgIe;

	private String emissorRgIe;

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private String dataExpedicaoRgIe;

	private String endereco;

	private String complemento;

	private String numeroEndereco;

	private String bairro;

	private String cep;

	private String cidade;

	private int ufCidade;

	private String telefone;

	private String celular;

	private String email;

	private String observacao;

	private BancoDTO banco;

	private String agencia;

	private String contaCorrente;

	private String observacoesBancaria;

	private String numeroPIS;

	private String numeroCTPS;

	private Integer tipoPessoa;

	public PessoaDTO() {
	}

	public PessoaDTO( Long id, String nome, String dataCadastro, String naturalidade, Integer ufNaturalidade, String dataNascimento, int estadoCivil, int sexo, String cpfCnpj, String rgIe, String emissorRgIe, String dataExpedicaoRgIe, String endereco, String complemento, String numeroEndereco, String bairro, String cep, String cidade, int ufCidade, String telefone, String celular, String email, String observacao, BancoDTO banco, String agencia, String contaCorrente, String observacoesBancaria,
			String numeroPIS, String numeroCTPS, Integer tipoPessoa ) {
		super();
		this.id = id;
		this.nome = nome;
		this.dataCadastro = dataCadastro;
		this.naturalidade = naturalidade;
		this.ufNaturalidade = ufNaturalidade;
		this.dataNascimento = dataNascimento;
		this.estadoCivil = estadoCivil;
		this.sexo = sexo;
		this.cpfCnpj = cpfCnpj;
		this.rgIe = rgIe;
		this.tipoPessoa = tipoPessoa;
		this.emissorRgIe = emissorRgIe;
		this.dataExpedicaoRgIe = dataExpedicaoRgIe;
		this.endereco = endereco;
		this.complemento = complemento;
		this.numeroEndereco = numeroEndereco;
		this.bairro = bairro;
		this.cep = cep;
		this.cidade = cidade;
		this.ufCidade = ufCidade;
		this.telefone = telefone;
		this.celular = celular;
		this.email = email;
		this.observacao = observacao;
		this.banco = banco;
		this.agencia = agencia;
		this.contaCorrente = contaCorrente;
		this.observacoesBancaria = observacoesBancaria;
		this.numeroPIS = numeroPIS;
		this.numeroCTPS = numeroCTPS;
	}

	public PessoaDTO( Pessoa pessoa ) {
		super();
		this.id = pessoa.getId();
		this.nome = pessoa.getNome();

		if (pessoa.getDataCadastro() != null) {
			this.dataCadastro = DataUtil.localDateToStr(pessoa.getDataCadastro());
		}

		this.naturalidade = pessoa.getNaturalidade();
		this.ufNaturalidade = pessoa.getUfNaturalidade();

		if (pessoa.getDataNascimento() != null) {
			this.dataNascimento = DataUtil.localDateToStr(pessoa.getDataNascimento());
		}

		if (pessoa.getEstadoCivil() != null) {
			this.estadoCivil = pessoa.getEstadoCivil().getCodigo();
		}

		if (pessoa.getSexo() != null) {
			this.sexo = pessoa.getSexo().getCodigo();
		}

		this.cpfCnpj = pessoa.getCpfCnpj();
		this.rgIe = pessoa.getRgIe();
		this.emissorRgIe = pessoa.getEmissorRgIe();

		if (pessoa.getDataExpedicaoRgIe() != null) {
			this.dataExpedicaoRgIe = DataUtil.localDateToStr(pessoa.getDataExpedicaoRgIe());
		}

		this.endereco = pessoa.getEndereco();
		this.complemento = pessoa.getComplemento();
		this.numeroEndereco = pessoa.getNumeroEndereco();
		this.bairro = pessoa.getBairro();
		this.cep = pessoa.getCep();
		this.cidade = pessoa.getCidade();

		if (pessoa.getUfCidade() != null) {
			this.ufCidade = pessoa.getUfCidade().getCodigo();
		}

		this.telefone = pessoa.getTelefone();
		this.celular = pessoa.getCelular();
		this.email = pessoa.getEmail();
		this.observacao = pessoa.getObservacao();

		if (pessoa.getBanco() != null) {
			this.banco = new BancoDTO(pessoa.getBanco());
		}

		this.agencia = pessoa.getAgencia();
		this.contaCorrente = pessoa.getContaCorrente();
		this.observacoesBancaria = pessoa.getObservacoesBancaria();
		this.numeroPIS = pessoa.getNumeroPIS();
		this.numeroCTPS = pessoa.getNumeroCTPS();

		if (pessoa.getTipoPessoa() != null) {
			this.tipoPessoa = pessoa.getTipoPessoa().getCodigo();
		}
	}

	public Long getId() {

		return id;
	}

	public void setId(Long id) {

		this.id = id;
	}

	public String getNome() {

		return nome;
	}

	public void setNome(String nome) {

		this.nome = nome;
	}

	public String getNaturalidade() {

		return naturalidade;
	}

	public void setNaturalidade(String naturalidade) {

		this.naturalidade = naturalidade;
	}

	public Integer getUfNaturalidade() {

		return ufNaturalidade;
	}

	public void setUfNaturalidade(Integer ufNaturalidade) {

		this.ufNaturalidade = ufNaturalidade;
	}

	public String getCpfCnpj() {

		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {

		this.cpfCnpj = cpfCnpj;
	}

	public String getRgIe() {

		return rgIe;
	}

	public void setRgIe(String rgIe) {

		this.rgIe = rgIe;
	}

	public String getEmissorRgIe() {

		return emissorRgIe;
	}

	public void setEmissorRgIe(String emissorRgIe) {

		this.emissorRgIe = emissorRgIe;
	}

	public String getEndereco() {

		return endereco;
	}

	public void setEndereco(String endereco) {

		this.endereco = endereco;
	}

	public String getComplemento() {

		return complemento;
	}

	public void setComplemento(String complemento) {

		this.complemento = complemento;
	}

	public String getNumeroEndereco() {

		return numeroEndereco;
	}

	public void setNumeroEndereco(String numeroEndereco) {

		this.numeroEndereco = numeroEndereco;
	}

	public String getBairro() {

		return bairro;
	}

	public void setBairro(String bairro) {

		this.bairro = bairro;
	}

	public String getCep() {

		return cep;
	}

	public void setCep(String cep) {

		this.cep = cep;
	}

	public String getCidade() {

		return cidade;
	}

	public void setCidade(String cidade) {

		this.cidade = cidade;
	}

	public String getTelefone() {

		return telefone;
	}

	public void setTelefone(String telefone) {

		this.telefone = telefone;
	}

	public String getCelular() {

		return celular;
	}

	public void setCelular(String celular) {

		this.celular = celular;
	}

	public String getEmail() {

		return email;
	}

	public void setEmail(String email) {

		this.email = email;
	}

	public String getObservacao() {

		return observacao;
	}

	public void setObservacao(String observacao) {

		this.observacao = observacao;
	}

	public BancoDTO getBanco() {

		return banco;
	}

	public void setBanco(BancoDTO banco) {

		this.banco = banco;
	}

	public String getAgencia() {

		return agencia;
	}

	public void setAgencia(String agencia) {

		this.agencia = agencia;
	}

	public String getContaCorrente() {

		return contaCorrente;
	}

	public void setContaCorrente(String contaCorrente) {

		this.contaCorrente = contaCorrente;
	}

	public String getObservacoesBancaria() {

		return observacoesBancaria;
	}

	public void setObservacoesBancaria(String observacoesBancaria) {

		this.observacoesBancaria = observacoesBancaria;
	}

	public String getNumeroPIS() {

		return numeroPIS;
	}

	public void setNumeroPIS(String numeroPIS) {

		this.numeroPIS = numeroPIS;
	}

	public String getNumeroCTPS() {

		return numeroCTPS;
	}

	public void setNumeroCTPS(String numeroCTPS) {

		this.numeroCTPS = numeroCTPS;
	}

	public String getDataCadastro() {

		return dataCadastro;
	}

	public void setDataCadastro(String dataCadastro) {

		this.dataCadastro = dataCadastro;
	}

	public String getDataNascimento() {

		return dataNascimento;
	}

	public void setDataNascimento(String dataNascimento) {

		this.dataNascimento = dataNascimento;
	}

	public String getDataExpedicaoRgIe() {

		return dataExpedicaoRgIe;
	}

	public void setDataExpedicaoRgIe(String dataExpedicaoRgIe) {

		this.dataExpedicaoRgIe = dataExpedicaoRgIe;
	}

	public int getEstadoCivil() {

		return estadoCivil;
	}

	public void setEstadoCivil(int estadoCivil) {

		this.estadoCivil = estadoCivil;
	}

	public int getSexo() {

		return sexo;
	}

	public void setSexo(int sexo) {

		this.sexo = sexo;
	}

	public int getUfCidade() {

		return ufCidade;
	}

	public void setUfCidade(int ufCidade) {

		this.ufCidade = ufCidade;
	}

	public Integer getTipoPessoa() {

		return tipoPessoa;
	}

	public void setTipoPessoa(Integer tipoPessoa) {

		this.tipoPessoa = tipoPessoa;
	}

}
