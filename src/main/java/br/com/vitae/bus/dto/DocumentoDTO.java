package br.com.vitae.bus.dto;

import java.math.BigDecimal;

import br.com.arquitetura.model.BaseDTO;
import br.com.arquitetura.util.DataUtil;
import br.com.vitae.bus.entity.Documento;
import br.com.vitae.bus.enumerator.EnumDocumento;
import br.com.vitae.bus.util.ConversorUtil;

@SuppressWarnings("serial")
public class DocumentoDTO implements BaseDTO {

	private Long id;

	private Boolean ativo;

	private boolean temArquivo;

	private String dataCadastro;

	private String dataEvento;

	private String historico;

	private String nomeArquivo;

	private String tipoArquivo;

	private String caminhoArquivo;

	private Long idColaborador;

	private Long idUsuarioAdicionou;

	private Integer tipoDocumento;

	private String nomeDocumento;

	private String valorRecebido;
//	private BigDecimal valorRecebido;

	public DocumentoDTO() {
	}

	public DocumentoDTO( Long id, Boolean ativo, String dataCadastro, String dataEvento, String historico, String nomeArquivo, String tipoArquivo, String caminhoArquivo, Long idColaborador, Long idUsuarioAdicionou, Integer tipo, String nomeDocumento ) {
		super();
		this.id = id;
		this.ativo = ativo;
		this.dataCadastro = dataCadastro;
		this.dataEvento = dataEvento;
		this.historico = historico;
		this.nomeArquivo = nomeArquivo;
		this.tipoArquivo = tipoArquivo;
		this.caminhoArquivo = caminhoArquivo;
		this.idColaborador = idColaborador;
		this.idUsuarioAdicionou = idUsuarioAdicionou;
		this.tipoDocumento = tipo;
		this.nomeDocumento = nomeDocumento;
		this.temArquivo = tipoArquivo != null && !tipoArquivo.equals("");
	}

	public DocumentoDTO( Long id, Boolean ativo, String dataCadastro, String dataEvento, String historico, String nomeArquivo, String tipoArquivo, String caminhoArquivo, Long idColaborador, Long idUsuarioAdicionou, Integer tipoDocumento, BigDecimal valorRecebido ) {
		super();
		this.id = id;
		this.ativo = ativo;
		this.dataCadastro = dataCadastro;
		this.dataEvento = dataEvento;
		this.historico = historico;
		this.nomeArquivo = nomeArquivo;
		this.tipoArquivo = tipoArquivo;
		this.caminhoArquivo = caminhoArquivo;
		this.idColaborador = idColaborador;
		this.idUsuarioAdicionou = idUsuarioAdicionou;
		this.tipoDocumento = tipoDocumento;
		this.valorRecebido = ConversorUtil.BigDecimalToStr(valorRecebido);
		this.temArquivo = tipoArquivo != null && !tipoArquivo.equals("");
	}

	public DocumentoDTO( Documento documento ) {
		this.id = documento.getId();
		this.tipoArquivo = documento.getTipoArquivo();
		this.caminhoArquivo = documento.getCaminhoArquivo();
		this.nomeArquivo = documento.getNomeArquivo();
		this.ativo = documento.getAtivo();
		this.dataCadastro = DataUtil.localDateToStr(documento.getDataCadastro());
		this.dataEvento = DataUtil.localDateToStr(documento.getDataEvento());
		this.historico = documento.getHistorico();
		this.valorRecebido = ConversorUtil.BigDecimalToStr(documento.getValorRecebido());

		this.tipoDocumento = documento.getTipo() != null ? documento.getTipo().ordinal() : null;
		this.idColaborador = documento.getColaborador() != null ? documento.getColaborador().getId() : null;
		this.idUsuarioAdicionou = documento.getUsuarioAdicionou() != null ? documento.getUsuarioAdicionou().getId() : null;
		this.nomeDocumento = EnumDocumento.getDescription(this.tipoDocumento);
		this.temArquivo = tipoArquivo != null && !tipoArquivo.equals("");
		
	}

	public Long getId() {

		return id;
	}

	public void setId(Long id) {

		this.id = id;
	}

	public Boolean getAtivo() {

		return ativo;
	}

	public void setAtivo(Boolean ativo) {

		this.ativo = ativo;
	}

	public String getDataCadastro() {

		return dataCadastro;
	}

	public void setDataCadastro(String dataCadastro) {

		this.dataCadastro = dataCadastro;
	}

	public String getDataEvento() {

		return dataEvento;
	}

	public void setDataEvento(String dataEvento) {

		this.dataEvento = dataEvento;
	}

	public String getHistorico() {

		return historico;
	}

	public void setHistorico(String historico) {

		this.historico = historico;
	}

	public String getNomeArquivo() {

		return nomeArquivo;
	}

	public void setNomeArquivo(String nomeArquivo) {

		this.nomeArquivo = nomeArquivo;
	}

	public String getTipoArquivo() {

		return tipoArquivo;
	}

	public void setTipoArquivo(String tipoArquivo) {

		this.tipoArquivo = tipoArquivo;
	}

	public String getCaminhoArquivo() {

		return caminhoArquivo;
	}

	public void setCaminhoArquivo(String caminhoArquivo) {

		this.caminhoArquivo = caminhoArquivo;
	}

	public Long getIdColaborador() {

		return idColaborador;
	}

	public void setIdColaborador(Long idColaborador) {

		this.idColaborador = idColaborador;
	}

	public Long getIdUsuarioAdicionou() {

		return idUsuarioAdicionou;
	}

	public void setIdUsuarioAdicionou(Long idUsuarioAdicionou) {

		this.idUsuarioAdicionou = idUsuarioAdicionou;
	}

	public Integer getTipoDocumento() {

		return tipoDocumento;
	}

	public void setTipoDocumento(Integer tipoDocumento) {

		this.tipoDocumento = tipoDocumento;
	}

	public String getValorRecebido() {

		return valorRecebido;
	}

	public void setValorRecebido(String valorRecebido) {

		this.valorRecebido = valorRecebido;
	}

	public String getNomeDocumento() {

		return nomeDocumento;
	}

	public void setNomeDocumento(String nomeDocumento) {

		this.nomeDocumento = nomeDocumento;
	}

	
	public boolean isTemArquivo() {
	
		return temArquivo;
	}

	
	public void setTemArquivo(boolean temArquivo) {
	
		this.temArquivo = temArquivo;
	}
	
	

}
