package br.com.vitae.bus.dto;

import org.springframework.format.annotation.DateTimeFormat;

import br.com.arquitetura.model.BaseDTO;
import br.com.arquitetura.util.DataUtil;
import br.com.vitae.bus.entity.Empresa;

@SuppressWarnings("serial")
public class EmpresaDTO implements BaseDTO {

	private Long id;

	private String nome;

	private Boolean ativo;

	private String email;

	@DateTimeFormat(pattern = "dd/MM/yyyy hh:mm:ss")
	private String dataCadastro;

	private String telefone;

	private String cpfCnpj;

	private String inscricaoEstadual;

	private String inscricaoMunicipal;

	private String endereco;

	private String cidade;

	private String bairro;

	private String cep;

	private int ufCidade;

	private Integer tipoPessoa;

	public EmpresaDTO() {
	}

	public EmpresaDTO( Long id, String nome, Boolean ativo, String email, String dataCadastro, String telefone, String cpfCnpj, String inscricaoEstadual, String inscricaoMunicipal, String endereco, String cidade, String bairro, String cep, int ufCidade, Integer tipoPessoa ) {
		super();
		this.id = id;
		this.nome = nome;
		this.ativo = ativo;
		this.email = email;
		this.dataCadastro = dataCadastro;
		this.telefone = telefone;
		this.cpfCnpj = cpfCnpj;
		this.inscricaoEstadual = inscricaoEstadual;
		this.inscricaoMunicipal = inscricaoMunicipal;
		this.endereco = endereco;
		this.cidade = cidade;
		this.bairro = bairro;
		this.cep = cep;
		this.ufCidade = ufCidade;
		this.tipoPessoa = tipoPessoa;
	}

	public EmpresaDTO( Empresa empresa ) {
		super();
		this.id = empresa.getId();
		this.nome = empresa.getNome();
		this.ativo = empresa.getAtivo();
		this.email = empresa.getEmail();
		if (empresa.getDataCadastro() != null) {
			this.dataCadastro = DataUtil.localDateToStr(empresa.getDataCadastro());
		}
		this.telefone = empresa.getTelefone();
		this.cpfCnpj = empresa.getCpfCnpj();
		this.inscricaoEstadual = empresa.getInscricaoEstadual();
		this.inscricaoMunicipal = empresa.getInscricaoMunicipal();
		this.endereco = empresa.getEndereco();
		this.cidade = empresa.getCidade();
		this.bairro = empresa.getBairro();
		this.cep = empresa.getCep();
		if (empresa.getUfCidade() != null) {
			this.ufCidade = empresa.getUfCidade().getCodigo();
		}
		if (empresa.getTipoPessoa() != null) {
			this.tipoPessoa = empresa.getTipoPessoa().getCodigo();
		}
	}

	public Long getId() {

		return id;
	}

	public void setId(Long id) {

		this.id = id;
	}

	public String getNome() {

		return nome;
	}

	public void setNome(String nome) {

		this.nome = nome;
	}

	public Boolean getAtivo() {

		return ativo;
	}

	public void setAtivo(Boolean ativo) {

		this.ativo = ativo;
	}

	public String getEmail() {

		return email;
	}

	public void setEmail(String email) {

		this.email = email;
	}

	public String getDataCadastro() {

		return dataCadastro;
	}

	public void setDataCadastro(String dataCadastro) {

		this.dataCadastro = dataCadastro;
	}

	public String getTelefone() {

		return telefone;
	}

	public void setTelefone(String telefone) {

		this.telefone = telefone;
	}

	public String getCpfCnpj() {

		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {

		this.cpfCnpj = cpfCnpj;
	}

	public String getInscricaoEstadual() {

		return inscricaoEstadual;
	}

	public void setInscricaoEstadual(String inscricaoEstadual) {

		this.inscricaoEstadual = inscricaoEstadual;
	}

	public String getInscricaoMunicipal() {

		return inscricaoMunicipal;
	}

	public void setInscricaoMunicipal(String inscricaoMunicipal) {

		this.inscricaoMunicipal = inscricaoMunicipal;
	}

	public String getEndereco() {

		return endereco;
	}

	public void setEndereco(String endereco) {

		this.endereco = endereco;
	}

	public String getCidade() {

		return cidade;
	}

	public void setCidade(String cidade) {

		this.cidade = cidade;
	}

	public String getBairro() {

		return bairro;
	}

	public void setBairro(String bairro) {

		this.bairro = bairro;
	}

	public String getCep() {

		return cep;
	}

	public void setCep(String cep) {

		this.cep = cep;
	}

	public int getUfCidade() {

		return ufCidade;
	}

	public void setUfCidade(int ufCidade) {

		this.ufCidade = ufCidade;
	}

	public Integer getTipoPessoa() {

		return tipoPessoa;
	}

	public void setTipoPessoa(Integer tipoPessoa) {

		this.tipoPessoa = tipoPessoa;
	}

}
