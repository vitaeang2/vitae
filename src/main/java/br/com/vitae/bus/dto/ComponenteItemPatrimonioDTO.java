package br.com.vitae.bus.dto;

import org.springframework.format.annotation.DateTimeFormat;

import br.com.arquitetura.model.BaseDTO;
import br.com.arquitetura.util.DataUtil;
import br.com.vitae.bus.entity.ComponenteItemPatrimonio;

/**
 * 
 * <p>
 * <b>Title:</b> PatrimonioDTO.java
 * </p>
 * 
 * <p>
 * <b>Description:</b> DTO responsável de representar o Patrimonio de um Produto
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnlogia da Informacao
 * </p>
 * 
 * @author Rogério Ferreira - rogerio0308@homtail.com
 * 
 * @version 1.0.0
 */
@SuppressWarnings("serial")
public class ComponenteItemPatrimonioDTO implements BaseDTO {

	private Long id;

	private String descricao;

	private String numeroSerie;

	@DateTimeFormat(pattern = "dd/MM/yyyy hh:mm:ss")
	private String fimGarantia;

	private String descricaoGarantia;

	private String observacao;

	private PatrimonioDTO patrimonio;

	private Boolean ativo;

	public ComponenteItemPatrimonioDTO() {

	}

	public ComponenteItemPatrimonioDTO(Long id, String descricao, String numeroSerie, String fimGarantia,
			String descricaoGarantia, String observacao, PatrimonioDTO patrimonio, boolean ativo) {

		super();
		this.id = id;
		this.descricao = descricao;
		this.numeroSerie = numeroSerie;
		this.fimGarantia = fimGarantia;
		this.descricaoGarantia = descricaoGarantia;
		this.observacao = observacao;
		this.patrimonio = patrimonio;
		this.ativo = ativo;
	}

	public ComponenteItemPatrimonioDTO(ComponenteItemPatrimonio componenteItemPatrimonio) {

		this.id = componenteItemPatrimonio.getId();

		this.descricao = componenteItemPatrimonio.getDescricao();

		this.numeroSerie = componenteItemPatrimonio.getNumeroSerie();

		if (componenteItemPatrimonio.getFimGarantia() != null) {
			this.fimGarantia = DataUtil.localDateToStr(componenteItemPatrimonio.getFimGarantia());
		}

		this.descricaoGarantia = componenteItemPatrimonio.getDescricaoGarantia();

		this.observacao = componenteItemPatrimonio.getObservacao();

		if (componenteItemPatrimonio.getPatrimonio() != null) {
			this.patrimonio = new PatrimonioDTO(componenteItemPatrimonio.getPatrimonio());
		}

		this.ativo = componenteItemPatrimonio.getAtivo();

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getNumeroSerie() {
		return numeroSerie;
	}

	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}

	public String getFimGarantia() {
		return fimGarantia;
	}

	public void setFimGarantia(String fimGarantia) {
		this.fimGarantia = fimGarantia;
	}

	public String getDescricaoGarantia() {
		return descricaoGarantia;
	}

	public void setDescricaoGarantia(String descricaoGarantia) {
		this.descricaoGarantia = descricaoGarantia;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public PatrimonioDTO getPatrimonio() {
		return patrimonio;
	}

	public void setPatrimonio(PatrimonioDTO patrimonio) {
		this.patrimonio = patrimonio;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

}
