package br.com.vitae.bus.dto;

import java.util.List;

import br.com.arquitetura.model.BaseDTO;
import br.com.vitae.bus.entity.Grupo;

/**
 * 
 * <p>
 * <b>Title:</b> GrupoDTO.java
 * </p>
 * 
 * <p>
 * <b>Description:</b> DTO responsável de representar o Grupo de um Produto
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnlogia da Informacao
 * </p>
 * 
 * @author Rogério Ferreira - rogerio0308@homtail.com
 * 
 * @version 1.0.0
 */
@SuppressWarnings("serial")
public class GrupoDTO implements BaseDTO {

	private Long id;

	private String nome;

	private Boolean ativo;
	
	private List<CategoriaGrupoDTO> categoriaGrupo;

	public GrupoDTO() {

	}

	public GrupoDTO( Long id, String nome, boolean ativo ) {

		super();
		this.id = id;
		this.nome = nome;
		this.ativo = ativo;		
	}

	public GrupoDTO( Grupo grupo ) {

		this(grupo.getId(), grupo.getNome(), grupo.getAtivo() == null ? false : grupo.getAtivo());

	}

	public Long getId() {

		return id;
	}

	public void setId(Long id) {

		this.id = id;
	}

	public String getNome() {

		return nome;
	}

	public void setNome(String nome) {

		this.nome = nome;
	}

	/**
	 * Retorna o valor do atributo <code>ativo</code>
	 *
	 * @return <code>Boolean</code>
	 */
	public Boolean getAtivo() {
	
		return ativo;
	}

	
	/**
	 * Define o valor do atributo <code>ativo</code>.
	 *
	 * @param ativo 
	 */
	public void setAtivo(Boolean ativo) {
	
		this.ativo = ativo;
	}

	public List<CategoriaGrupoDTO> getCategoriaGrupo() {
		return categoriaGrupo;
	}

	public void setCategoriaGrupo(List<CategoriaGrupoDTO> categoriaGrupo) {
		this.categoriaGrupo = categoriaGrupo;
	}

}
