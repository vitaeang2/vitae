package br.com.vitae.bus.dto;

import org.springframework.format.annotation.DateTimeFormat;

import br.com.arquitetura.model.BaseDTO;
import br.com.arquitetura.util.DataUtil;
import br.com.vitae.bus.entity.Solicitacao;

@SuppressWarnings("serial")
public class SolicitacaoDTO implements BaseDTO {

	private Long id;

	private Boolean ativo;

	private String assunto;

	private String descricao;

	private String solucao;

	private String numeroEtiqueta;

	private EmpresaDTO empresa;

	private NucleoDTO nucleo;

	private UsuarioDTO responsavel;

	private UsuarioDTO criador;

	private TipoSolicitacaoDTO tipoSolicitacao;

	private LocalDTO local;

	private int statusSolicitacao;

	@DateTimeFormat(pattern = "dd/MM/yyyy hh:mm:ss")
	private String abertura;

	@DateTimeFormat(pattern = "dd/MM/yyyy hh:mm:ss")
	private String termino;

	@DateTimeFormat(pattern = "dd/MM/yyyy hh:mm:ss")
	private String previsaoFinalizacao;

	public SolicitacaoDTO() {

	}

	public SolicitacaoDTO(Long id,

			Boolean ativo,

			String assunto,

			String descricao,

			String solucao,

			String numeroEtiqueta,

			EmpresaDTO empresa,

			NucleoDTO nucleo,

			UsuarioDTO responsavel,

			UsuarioDTO criador,

			TipoSolicitacaoDTO tipoSolicitacao,

			int statusSolicitacao,

			String abertura,

			String termino,

			String previsaoFinalizacao,

			LocalDTO local) {
		super();

		this.id = id;

		this.ativo = ativo;

		this.assunto = assunto;

		this.descricao = descricao;

		this.solucao = solucao;

		this.numeroEtiqueta = numeroEtiqueta;

		this.empresa = empresa;

		this.nucleo = nucleo;

		this.responsavel = responsavel;

		this.criador = criador;

		this.tipoSolicitacao = tipoSolicitacao;

		this.statusSolicitacao = statusSolicitacao;

		this.abertura = abertura;

		this.termino = termino;

		this.previsaoFinalizacao = previsaoFinalizacao;

		this.local = local;
	}

	public SolicitacaoDTO(Solicitacao solicitacao) {

		this.id = solicitacao.getId();
		this.ativo = solicitacao.getAtivo();

		this.assunto = solicitacao.getAssunto();

		this.descricao = solicitacao.getDescricao();

		this.solucao = solicitacao.getSolucao();

		this.numeroEtiqueta = solicitacao.getNumeroEtiqueta();

		if (solicitacao.getNucleo() != null) {

			this.nucleo = new NucleoDTO(solicitacao.getNucleo());
		}

		if (solicitacao.getEmpresa() != null) {

			this.empresa = new EmpresaDTO(solicitacao.getEmpresa());
		}

		if (solicitacao.getResponsavel() != null) {

			this.responsavel = new UsuarioDTO(solicitacao.getResponsavel());
		}

		if (solicitacao.getCriador() != null) {

			this.criador = new UsuarioDTO(solicitacao.getCriador());
		}

		if (solicitacao.getTipoSolicitacao() != null) {

			this.tipoSolicitacao = new TipoSolicitacaoDTO(solicitacao.getTipoSolicitacao());
		}

		if (solicitacao.getStatusSolicitacao() != null) {

			this.statusSolicitacao = solicitacao.getStatusSolicitacao().getCodigo();

		}

		if (solicitacao.getLocal() != null) {

			this.local = new LocalDTO(solicitacao.getLocal());
		}

		this.abertura = DataUtil.localDateToStr(solicitacao.getAbertura());
		this.termino = DataUtil.localDateToStr(solicitacao.getTermino());
		this.previsaoFinalizacao = DataUtil.localDateToStr(solicitacao.getPrevisaoFinalizacao());
	}

	public Long getId() {

		return id;
	}

	public void setId(Long id) {

		this.id = id;
	}

	public NucleoDTO getNucleo() {

		return nucleo;
	}

	public void setNucleo(NucleoDTO nucleo) {

		this.nucleo = nucleo;
	}

	public EmpresaDTO getEmpresa() {

		return empresa;
	}

	public void setEmpresa(EmpresaDTO empresa) {

		this.empresa = empresa;
	}

	public Boolean getAtivo() {

		return ativo;
	}

	public void setAtivo(Boolean ativo) {

		this.ativo = ativo;
	}

	public String getAssunto() {
		return assunto;
	}

	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getNumeroEtiqueta() {
		return numeroEtiqueta;
	}

	public void setNumeroEtiqueta(String numeroEtiqueta) {
		this.numeroEtiqueta = numeroEtiqueta;
	}

	public UsuarioDTO getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(UsuarioDTO responsavel) {
		this.responsavel = responsavel;
	}

	public int getStatusSolicitacao() {
		return statusSolicitacao;
	}

	public void setStatusSolicitacao(int statusSolicitacao) {
		this.statusSolicitacao = statusSolicitacao;
	}

	public String getAbertura() {
		return abertura;
	}

	public void setAbertura(String abertura) {
		this.abertura = abertura;
	}

	public String getTermino() {
		return termino;
	}

	public void setTermino(String termino) {
		this.termino = termino;
	}

	public String getPrevisaoFinalizacao() {
		return previsaoFinalizacao;
	}

	public void setPrevisaoFinalizacao(String previsaoFinalizacao) {
		this.previsaoFinalizacao = previsaoFinalizacao;
	}

	public TipoSolicitacaoDTO getTipoSolicitacao() {
		return tipoSolicitacao;
	}

	public void setTipoSolicitacao(TipoSolicitacaoDTO tipoSolicitacao) {
		this.tipoSolicitacao = tipoSolicitacao;
	}

	public UsuarioDTO getCriador() {
		return criador;
	}

	public void setCriador(UsuarioDTO criador) {
		this.criador = criador;
	}

	public String getSolucao() {
		return solucao;
	}

	public void setSolucao(String solucao) {
		this.solucao = solucao;
	}

	public LocalDTO getLocal() {
		return local;
	}

	public void setLocal(LocalDTO local) {
		this.local = local;
	}

}
