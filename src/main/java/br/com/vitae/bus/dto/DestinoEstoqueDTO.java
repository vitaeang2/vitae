package br.com.vitae.bus.dto;

import br.com.arquitetura.model.BaseDTO;
import br.com.vitae.bus.entity.DestinoEstoque;

/**
 * 
 * <p>
 * <b>Title:</b> DestinoEstoqueDTO.java
 * </p>
 * 
 * <p>
 * <b>Description:</b>
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnlogia da Informacao
 * </p>
 * 
 * @author Sergio Filho - sergioadsf@gmail.com
 * 
 * @version 1.0.0
 */
@SuppressWarnings("serial")
public class DestinoEstoqueDTO implements BaseDTO {

	private Long id;

	private String descricao;

	private Boolean ativo;

	public DestinoEstoqueDTO() {
	}

	public DestinoEstoqueDTO( Long id, String descricao, Boolean ativo ) {
		super();
		this.id = id;
		this.descricao = descricao;
		this.ativo = ativo;
	}

	public DestinoEstoqueDTO( DestinoEstoque destinoEstoque ) {
		this(destinoEstoque.getId(), destinoEstoque.getDescricao(), destinoEstoque.getAtivo());
	}

	/**
	 * Retorna o valor do atributo <code>id</code>
	 *
	 * @return <code>Long</code>
	 */
	public Long getId() {

		return id;
	}

	/**
	 * Define o valor do atributo <code>id</code>.
	 *
	 * @param id
	 */
	public void setId(Long id) {

		this.id = id;
	}

	/**
	 * Retorna o valor do atributo <code>descricao</code>
	 *
	 * @return <code>String</code>
	 */
	public String getDescricao() {

		return descricao;
	}

	/**
	 * Define o valor do atributo <code>descricao</code>.
	 *
	 * @param descricao
	 */
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/**
	 * 
	 * Método responsável por
	 *
	 * @author Sergio Filho - sergioadsf@gmail.com
	 *
	 * @return
	 */
	public Boolean getAtivo() {

		return ativo;
	}

	/**
	 * 
	 * Método responsável por
	 *
	 * @author Sergio Filho - sergioadsf@gmail.com
	 *
	 * @param ativo
	 */
	public void setAtivo(Boolean ativo) {

		this.ativo = ativo;
	}

}
