package br.com.vitae.bus.dto;

import org.springframework.format.annotation.DateTimeFormat;

import br.com.arquitetura.model.BaseDTO;
import br.com.arquitetura.util.DataUtil;
import br.com.vitae.bus.entity.Colaborador;

@SuppressWarnings("serial")
public class ColaboradorDTO implements BaseDTO {

	private Long id;

	private boolean temFoto;

	private String nomeArquivo;

	private String tipoArquivo;

	private String caminhoArquivo;

	private PessoaDTO pessoa;

	private CargoDTO cargo;

	private NucleoDTO nucleo;

	private EmpresaDTO empresa;

	private Boolean ativo;

	@DateTimeFormat(pattern = "dd/MM/yyyy hh:mm:ss")
	private String admissao;

	@DateTimeFormat(pattern = "dd/MM/yyyy hh:mm:ss")
	private String demissao;

	public ColaboradorDTO() {

	}

	public ColaboradorDTO( Long id, String nomeArquivo, String tipoArquivo, PessoaDTO pessoa, CargoDTO cargo, NucleoDTO nucleo, EmpresaDTO empresa, String admissao, String demissao, Boolean ativo ) {

		super();
		this.id = id;
		this.nomeArquivo = nomeArquivo;
		this.pessoa = pessoa;
		this.cargo = cargo;
		this.nucleo = nucleo;
		this.empresa = empresa;
		this.admissao = admissao;
		this.demissao = demissao;
		this.ativo = ativo;
	}

	public ColaboradorDTO( Colaborador colaborador ) {

		this.id = colaborador.getId();
		this.nomeArquivo = colaborador.getNomeArquivo();
		this.tipoArquivo = colaborador.getTipoArquivo();
		this.ativo = colaborador.getAtivo();

		if (colaborador.getPessoa() != null) {

			this.pessoa = new PessoaDTO(colaborador.getPessoa());
		}

		if (colaborador.getCargo() != null) {

			this.cargo = new CargoDTO(colaborador.getCargo());
		}

		if (colaborador.getNucleo() != null) {

			this.nucleo = new NucleoDTO(colaborador.getNucleo());
		}

		if (colaborador.getEmpresa() != null) {

			this.empresa = new EmpresaDTO(colaborador.getEmpresa());
		}

		this.admissao = DataUtil.localDateToStr(colaborador.getAdmissao());
		this.demissao = DataUtil.localDateToStr(colaborador.getDemissao());
	}

	public Long getId() {

		return id;
	}

	public void setId(Long id) {

		this.id = id;
	}

	public PessoaDTO getPessoa() {

		return pessoa;
	}

	public void setPessoa(PessoaDTO pessoa) {

		this.pessoa = pessoa;
	}

	public CargoDTO getCargo() {

		return cargo;
	}

	public void setCargo(CargoDTO cargo) {

		this.cargo = cargo;
	}

	public String getAdmissao() {

		return admissao;
	}

	public void setAdmissao(String admissao) {

		this.admissao = admissao;
	}

	public String getDemissao() {

		return demissao;
	}

	public void setDemissao(String demissao) {

		this.demissao = demissao;
	}

	public NucleoDTO getNucleo() {

		return nucleo;
	}

	public void setNucleo(NucleoDTO nucleo) {

		this.nucleo = nucleo;
	}

	public EmpresaDTO getEmpresa() {

		return empresa;
	}

	public void setEmpresa(EmpresaDTO empresa) {

		this.empresa = empresa;
	}

	public String getNomeArquivo() {

		return nomeArquivo;
	}

	public void setNomeArquivo(String nomeArquivo) {

		this.nomeArquivo = nomeArquivo;
	}

	public String getCaminhoArquivo() {

		return caminhoArquivo;
	}

	public void setCaminhoArquivo(String caminhoArquivo) {

		this.caminhoArquivo = caminhoArquivo;
	}

	public String getTipoArquivo() {

		return tipoArquivo;
	}

	public void setTipoArquivo(String tipoArquivo) {

		this.tipoArquivo = tipoArquivo;
	}

	public Boolean getAtivo() {

		return ativo;
	}

	public void setAtivo(Boolean ativo) {

		this.ativo = ativo;
	}

	public boolean isTemFoto() {

		return temFoto;
	}

	public void setTemFoto(boolean temFoto) {

		this.temFoto = temFoto;
	}

}
