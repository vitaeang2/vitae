package br.com.vitae.bus.dto;

import br.com.arquitetura.model.BaseDTO;
import br.com.vitae.bus.entity.Fabricante;

/**
 * 
 * <p>
 * <b>Title:</b> FabricanteDTO.java
 * </p>
 * 
 * <p>
 * <b>Description:</b> DTO responsável de representar o Fabricante de um Produto
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnlogia da Informacao
 * </p>
 * 
 * @author Joaquim Barros - joaquimbarros.cmp@gmail.com
 * 
 * @version 1.0.0
 */
@SuppressWarnings("serial")
public class FabricanteDTO implements BaseDTO {

	private Long id;

	private String nome;

	private Boolean ativo;

	public FabricanteDTO() {

	}

	public FabricanteDTO( Long id, String nome, boolean ativo ) {

		super();
		this.id = id;
		this.nome = nome;
		this.ativo = ativo;		
	}

	public FabricanteDTO( Fabricante fabricante ) {

		this(fabricante.getId(), fabricante.getNome(), fabricante.getAtivo() == null ? false : fabricante.getAtivo());

	}

	public Long getId() {

		return id;
	}

	public void setId(Long id) {

		this.id = id;
	}

	public String getNome() {

		return nome;
	}

	public void setNome(String nome) {

		this.nome = nome;
	}

	/**
	 * Retorna o valor do atributo <code>ativo</code>
	 *
	 * @return <code>Boolean</code>
	 */
	public Boolean getAtivo() {
	
		return ativo;
	}

	
	/**
	 * Define o valor do atributo <code>ativo</code>.
	 *
	 * @param ativo 
	 */
	public void setAtivo(Boolean ativo) {
	
		this.ativo = ativo;
	}
}
