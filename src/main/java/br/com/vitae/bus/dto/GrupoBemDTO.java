package br.com.vitae.bus.dto;

import java.math.BigDecimal;

import br.com.arquitetura.model.BaseDTO;
import br.com.vitae.bus.entity.GrupoBem;

/**
 * 
 * <p>
 * <b>Title:</b> GrupoBemDTO.java
 * </p>
 * 
 * <p>
 * <b>Description:</b> DTO responsável de representar o GrupoBem de um Produto
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnlogia da Informacao
 * </p>
 * 
 * @author Rogério Ferreira - rogerio0308@homtail.com
 * 
 * @version 1.0.0
 */
@SuppressWarnings("serial")
public class GrupoBemDTO implements BaseDTO {

	private Long id;

	private String nome;
	
	private String observacao;

	private Boolean ativo;
	
	private BigDecimal depreciacaoAnual;

	public GrupoBemDTO() {

	}

	public GrupoBemDTO( Long id, String nome, boolean ativo, BigDecimal depreciacaoAnual, String observacao ) {

		super();
		this.id = id;
		this.nome = nome;
		this.ativo = ativo;		
		this.depreciacaoAnual = depreciacaoAnual;		
		this.observacao = observacao;		
	}

	public GrupoBemDTO( GrupoBem grupoBem ) {

		this(grupoBem.getId(), grupoBem.getNome(), grupoBem.getAtivo() == null ? false : grupoBem.getAtivo(), grupoBem.getDepreciacaoAnual(), grupoBem.getObservacao());

	}

	public Long getId() {

		return id;
	}

	public void setId(Long id) {

		this.id = id;
	}

	public String getNome() {

		return nome;
	}

	public void setNome(String nome) {

		this.nome = nome;
	}

	/**
	 * Retorna o valor do atributo <code>ativo</code>
	 *
	 * @return <code>Boolean</code>
	 */
	public Boolean getAtivo() {
	
		return ativo;
	}

	
	/**
	 * Define o valor do atributo <code>ativo</code>.
	 *
	 * @param ativo 
	 */
	public void setAtivo(Boolean ativo) {
	
		this.ativo = ativo;
	}

	public BigDecimal getDepreciacaoAnual() {
		return depreciacaoAnual;
	}

	public void setDepreciacaoAnual(BigDecimal depreciacaoAnual) {
		this.depreciacaoAnual = depreciacaoAnual;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	
}
