package br.com.vitae.bus.dto;

import br.com.arquitetura.model.BaseDTO;
import br.com.arquitetura.util.DataUtil;
import br.com.vitae.bus.entity.Nucleo;

@SuppressWarnings("serial")
public class NucleoDTO implements BaseDTO {

	private Long id;

	private String nome;

	private Boolean ativo;

	private String email;

	private String dataCadastro;

	private String telefone;

	private String endereco;

	private String cidade;

	private String bairro;

	private String cep;

	private int ufCidade;

	private Long idEmpresa;

	private String nomeEmpresa;

	public NucleoDTO() {
	}

	public NucleoDTO(Long id, String nome, Boolean ativo, String email, String dataCadastro, String telefone,
			String cpfCnpj, String inscricaoEstadual, String inscricaoMunicipal, String endereco, String cidade,
			String bairro, String cep, int ufCidade, Long idEmpresa, String nomeEmpresa) {
		super();
		this.id = id;
		this.nome = nome;
		this.ativo = ativo;
		this.email = email;
		this.dataCadastro = dataCadastro;
		this.telefone = telefone;
		this.endereco = endereco;
		this.cidade = cidade;
		this.bairro = bairro;
		this.cep = cep;
		this.ufCidade = ufCidade;
		this.idEmpresa = idEmpresa;
		this.nomeEmpresa = nomeEmpresa;
	}

	public NucleoDTO(Nucleo nucleo) {
		this.id = nucleo.getId();
		this.nome = nucleo.getNome();
		this.ativo = nucleo.getAtivo();
		this.email = nucleo.getEmail();
		this.dataCadastro = DataUtil.localDateToStr(nucleo.getDataCadastro());
		this.telefone = nucleo.getTelefone();
		this.endereco = nucleo.getEndereco();
		this.cidade = nucleo.getCidade();
		this.bairro = nucleo.getBairro();
		this.cep = nucleo.getCep();
		if (nucleo.getUfCidade() != null) {
			this.ufCidade = nucleo.getUfCidade().getCodigo();
		}
		if (nucleo.getEmpresa() != null) {
			this.idEmpresa = nucleo.getEmpresa().getId();
			if (nucleo.getEmpresa().getNome() != null) {
				this.nomeEmpresa = nucleo.getEmpresa().getNome();

			}
		}
	}

	public Long getId() {

		return id;
	}

	public void setId(Long id) {

		this.id = id;
	}

	public String getNome() {

		return nome;
	}

	public void setNome(String nome) {

		this.nome = nome;
	}

	public Boolean getAtivo() {

		return ativo;
	}

	public void setAtivo(Boolean ativo) {

		this.ativo = ativo;
	}

	public String getEmail() {

		return email;
	}

	public void setEmail(String email) {

		this.email = email;
	}

	public String getDataCadastro() {

		return dataCadastro;
	}

	public void setDataCadastro(String dataCadastro) {

		this.dataCadastro = dataCadastro;
	}

	public String getTelefone() {

		return telefone;
	}

	public void setTelefone(String telefone) {

		this.telefone = telefone;
	}

	public String getEndereco() {

		return endereco;
	}

	public void setEndereco(String endereco) {

		this.endereco = endereco;
	}

	public String getCidade() {

		return cidade;
	}

	public void setCidade(String cidade) {

		this.cidade = cidade;
	}

	public String getBairro() {

		return bairro;
	}

	public void setBairro(String bairro) {

		this.bairro = bairro;
	}

	public String getCep() {

		return cep;
	}

	public void setCep(String cep) {

		this.cep = cep;
	}

	public int getUfCidade() {

		return ufCidade;
	}

	public void setUfCidade(int ufCidade) {

		this.ufCidade = ufCidade;
	}

	public Long getIdEmpresa() {

		return idEmpresa;
	}

	public void setIdEmpresa(Long idEmpresa) {

		this.idEmpresa = idEmpresa;
	}

	public String getNomeEmpresa() {
		return nomeEmpresa;
	}

	public void setNomeEmpresa(String nomeEmpresa) {
		this.nomeEmpresa = nomeEmpresa;
	}

}
