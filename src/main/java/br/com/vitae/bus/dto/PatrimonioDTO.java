package br.com.vitae.bus.dto;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;

import br.com.arquitetura.model.BaseDTO;
import br.com.arquitetura.util.DataUtil;
import br.com.vitae.bus.entity.Patrimonio;

/**
 * 
 * <p>
 * <b>Title:</b> PatrimonioDTO.java
 * </p>
 * 
 * <p>
 * <b>Description:</b> DTO responsável de representar o Patrimonio de um Produto
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnlogia da Informacao
 * </p>
 * 
 * @author Rogério Ferreira - rogerio0308@homtail.com
 * 
 * @version 1.0.0
 */
@SuppressWarnings("serial")
public class PatrimonioDTO implements BaseDTO {

	private Long id;

	private String nome;

	private String observacao;

	private Boolean ativo;

	private int unidadeMedida;

	private int situacaoBemPatrimonio;

	private GrupoBemDTO grupoBem;

	private DepartamentoDTO departamento;

	private int tipoIncorporacaoPatrimonio;

	private String numeroNota;

	@DateTimeFormat(pattern = "dd/MM/yyyy hh:mm:ss")
	private String dataIncorporacao;

	private BigDecimal valor;

	private String numeroSerie;

	private String numeroEtiqueta;

	@DateTimeFormat(pattern = "dd/MM/yyyy hh:mm:ss")
	private String dataNota;

	@DateTimeFormat(pattern = "dd/MM/yyyy hh:mm:ss")
	private String fimGarantia;

	private String descricaoGarantia;
	
	private List<ComponenteItemPatrimonioDTO> componenteItemPatrimonio;

	public PatrimonioDTO() {

	}

	public PatrimonioDTO(Long id, String nome, boolean ativo, String observacao) {

		super();
		this.id = id;
		this.nome = nome;
		this.ativo = ativo;
		this.observacao = observacao;
	}

	public PatrimonioDTO(Long id, String nome, String observacao, Boolean ativo, int unidadeMedida,
			int situacaoBemPatrimonio, GrupoBemDTO grupoBem, DepartamentoDTO departamento,
			int tipoIncorporacaoPatrimonio, String numeroNota, String dataIncorporacao, BigDecimal valor,
			String numeroSerie, String numeroEtiqueta, String dataNota, String fimGarantia, String descricaoGarantia) {

		super();
		this.id = id;
		this.nome = nome;
		this.observacao = observacao;
		this.ativo = ativo;
		this.unidadeMedida = unidadeMedida;
		this.situacaoBemPatrimonio = situacaoBemPatrimonio;
		this.grupoBem = grupoBem;
		this.departamento = departamento;
		this.tipoIncorporacaoPatrimonio = tipoIncorporacaoPatrimonio;
		this.numeroNota = numeroNota;
		this.dataIncorporacao = dataIncorporacao;
		this.valor = valor;
		this.numeroSerie = numeroSerie;
		this.numeroEtiqueta = numeroEtiqueta;
		this.dataNota = dataNota;
		this.fimGarantia = fimGarantia;
		this.descricaoGarantia = descricaoGarantia;
	}

	public PatrimonioDTO(Patrimonio patrimonio) {

		this.id = patrimonio.getId();

		this.nome = patrimonio.getNome();

		this.observacao = patrimonio.getObservacao();

		this.ativo = patrimonio.getAtivo();

		if (patrimonio.getUnidadeMedida() != null) {

			this.unidadeMedida = patrimonio.getUnidadeMedida().getCodigo();
		}

		if (patrimonio.getSituacaoBemPatrimonio() != null) {
			this.situacaoBemPatrimonio = patrimonio.getSituacaoBemPatrimonio().getCodigo();
		}

		if (patrimonio.getGrupoBem() != null) {
			this.grupoBem = new GrupoBemDTO(patrimonio.getGrupoBem());
		}

		if (patrimonio.getDepartamento() != null) {
			this.departamento = new DepartamentoDTO(patrimonio.getDepartamento());
		}

		if (patrimonio.getTipoIncorporacaoPatrimonio() != null) {
			this.tipoIncorporacaoPatrimonio = patrimonio.getTipoIncorporacaoPatrimonio().getCodigo();
		}

		this.numeroNota = patrimonio.getNumeroNota();

		if (patrimonio.getDataIncorporacao() != null) {
			this.dataIncorporacao = DataUtil.localDateToStr(patrimonio.getDataIncorporacao());
		}

		this.valor = patrimonio.getValor();

		this.numeroSerie = patrimonio.getNumeroSerie();

		this.numeroEtiqueta = patrimonio.getNumeroEtiqueta();

		if (patrimonio.getDataNota() != null) {
			this.dataNota = DataUtil.localDateToStr(patrimonio.getDataNota());
		}

		if (patrimonio.getFimGarantia() != null) {
			this.fimGarantia = DataUtil.localDateToStr(patrimonio.getFimGarantia());
		}

		this.descricaoGarantia = patrimonio.getDescricaoGarantia();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public int getUnidadeMedida() {
		return unidadeMedida;
	}

	public void setUnidadeMedida(int unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}

	public int getSituacaoBemPatrimonio() {
		return situacaoBemPatrimonio;
	}

	public void setSituacaoBemPatrimonio(int situacaoBemPatrimonio) {
		this.situacaoBemPatrimonio = situacaoBemPatrimonio;
	}

	public GrupoBemDTO getGrupoBem() {
		return grupoBem;
	}

	public void setGrupoBem(GrupoBemDTO grupoBem) {
		this.grupoBem = grupoBem;
	}

	public DepartamentoDTO getDepartamento() {
		return departamento;
	}

	public void setDepartamento(DepartamentoDTO departamento) {
		this.departamento = departamento;
	}

	public int getTipoIncorporacaoPatrimonio() {
		return tipoIncorporacaoPatrimonio;
	}

	public void setTipoIncorporacaoPatrimonio(int tipoIncorporacaoPatrimonio) {
		this.tipoIncorporacaoPatrimonio = tipoIncorporacaoPatrimonio;
	}

	public String getNumeroNota() {
		return numeroNota;
	}

	public void setNumeroNota(String numeroNota) {
		this.numeroNota = numeroNota;
	}

	public String getDataIncorporacao() {
		return dataIncorporacao;
	}

	public void setDataIncorporacao(String dataIncorporacao) {
		this.dataIncorporacao = dataIncorporacao;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public String getNumeroSerie() {
		return numeroSerie;
	}

	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}

	public String getDataNota() {
		return dataNota;
	}

	public void setDataNota(String dataNota) {
		this.dataNota = dataNota;
	}

	public String getFimGarantia() {
		return fimGarantia;
	}

	public void setFimGarantia(String fimGarantia) {
		this.fimGarantia = fimGarantia;
	}

	public String getDescricaoGarantia() {
		return descricaoGarantia;
	}

	public void setDescricaoGarantia(String descricaoGarantia) {
		this.descricaoGarantia = descricaoGarantia;
	}

	public String getNumeroEtiqueta() {
		return numeroEtiqueta;
	}

	public void setNumeroEtiqueta(String numeroEtiqueta) {
		this.numeroEtiqueta = numeroEtiqueta;
	}

	public List<ComponenteItemPatrimonioDTO> getComponenteItemPatrimonio() {
		return componenteItemPatrimonio;
	}

	public void setComponenteItemPatrimonio(List<ComponenteItemPatrimonioDTO> componente) {
		componenteItemPatrimonio = componente;
	}


}
