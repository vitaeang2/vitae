package br.com.vitae.bus.dto;

import br.com.arquitetura.model.BaseDTO;
import br.com.vitae.bus.entity.NumeroBanco;

/**
 * 
 * <p>
 * <b>Title:</b> CargoDTO.java
 * </p>
 * 
 * <p>
 * <b>Description:</b>
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnlogia da Informacao
 * </p>
 * 
 * @author Sergio Filho - sergioadsf@gmail.com
 * 
 * @version 1.0.0
 */
@SuppressWarnings("serial")
public class BancoDTO implements BaseDTO {

	private Long id;

	private String numero;

	private String nome;

	public BancoDTO() {
	}

	public BancoDTO( Long id, String numero, String nome ) {
		super();
		this.id = id;
		this.numero = numero;
		this.nome = nome;
	}

	public BancoDTO( NumeroBanco banco ) {
		this(banco.getId(), banco.getNumero(), banco.getNome());
	}

	public Long getId() {

		return id;
	}

	public void setId(Long id) {

		this.id = id;
	}

	public String getNome() {

		return nome;
	}

	public void setNome(String nome) {

		this.nome = nome;
	}

	public String getNumero() {

		return numero;
	}

	public void setNumero(String numero) {

		this.numero = numero;
	}

}
