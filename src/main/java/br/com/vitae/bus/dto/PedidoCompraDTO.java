package br.com.vitae.bus.dto;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;

import br.com.arquitetura.model.BaseDTO;
import br.com.arquitetura.util.DataUtil;
import br.com.vitae.bus.entity.PedidoCompra;

@SuppressWarnings("serial")
public class PedidoCompraDTO implements BaseDTO {

	private Long id;

	private BigDecimal vlrFrete;

	private BigDecimal vlrDesconto;

	private Integer status;

	private ColaboradorDTO colaborador;

	private NucleoDTO nucleo;

	private EmpresaDTO empresa;

	private FornecedorDTO fornecedor;

	private CotacaoDTO cotacao;

	private List<ItemPedidoCompraDTO> itensPedidoCompra;

	@DateTimeFormat(pattern = "dd/MM/yyyy hh:mm:ss")
	private String dataPedidoCompra;

	public PedidoCompraDTO() {

	}

	public PedidoCompraDTO( Long id, BigDecimal vlrFrete, BigDecimal vlrDesconto, ColaboradorDTO colaborador, NucleoDTO nucleo, EmpresaDTO empresa, FornecedorDTO fornecedor, String dataPedidoCompra, CotacaoDTO cotacao, Integer status ) {
		super();
		this.id = id;
		this.vlrFrete = vlrFrete;
		this.vlrDesconto = vlrDesconto;
		this.colaborador = colaborador;
		this.nucleo = nucleo;
		this.empresa = empresa;
		this.fornecedor = fornecedor;
		this.dataPedidoCompra = dataPedidoCompra;
		this.cotacao = cotacao;
		this.status = status;
	}

	public PedidoCompraDTO( PedidoCompra pedidoCompra ) {

		this.id = pedidoCompra.getId();
		this.vlrDesconto = pedidoCompra.getVlrDesconto();
		this.vlrFrete = pedidoCompra.getVlrFrete();

		if (pedidoCompra.getColaborador() != null) {

			this.colaborador = new ColaboradorDTO(pedidoCompra.getColaborador());
		}

		if (pedidoCompra.getNucleo() != null) {

			this.nucleo = new NucleoDTO(pedidoCompra.getNucleo());
		}

		if (pedidoCompra.getEmpresa() != null) {

			this.empresa = new EmpresaDTO(pedidoCompra.getEmpresa());
		}

		if (pedidoCompra.getFornecedor() != null) {

			this.fornecedor = new FornecedorDTO(pedidoCompra.getFornecedor());
		}

		if (pedidoCompra.getCotacao() != null) {

			this.cotacao = new CotacaoDTO(pedidoCompra.getCotacao());
		}

		this.dataPedidoCompra = DataUtil.localDateToStr(pedidoCompra.getDataPedidoCompra());
		this.status = pedidoCompra.getStatus() != null ? pedidoCompra.getStatus().ordinal() : null;
	}

	public Long getId() {

		return id;
	}

	public void setId(Long id) {

		this.id = id;
	}

	public NucleoDTO getNucleo() {

		return nucleo;
	}

	public void setNucleo(NucleoDTO nucleo) {

		this.nucleo = nucleo;
	}

	public EmpresaDTO getEmpresa() {

		return empresa;
	}

	public void setEmpresa(EmpresaDTO empresa) {

		this.empresa = empresa;
	}

	public BigDecimal getVlrFrete() {

		return vlrFrete;
	}

	public void setVlrFrete(BigDecimal vlrFrete) {

		this.vlrFrete = vlrFrete;
	}

	public BigDecimal getVlrDesconto() {

		return vlrDesconto;
	}

	public void setVlrDesconto(BigDecimal vlrDesconto) {

		this.vlrDesconto = vlrDesconto;
	}

	public String getDataPedidoCompra() {

		return dataPedidoCompra;
	}

	public void setDataPedidoCompra(String dataPedidoCompra) {

		this.dataPedidoCompra = dataPedidoCompra;
	}

	public ColaboradorDTO getColaborador() {

		return colaborador;
	}

	public void setColaborador(ColaboradorDTO colaborador) {

		this.colaborador = colaborador;
	}

	public List<ItemPedidoCompraDTO> getItensPedidoCompra() {

		return itensPedidoCompra;
	}

	public void setItensPedidoCompra(List<ItemPedidoCompraDTO> itensPedidoCompra) {

		this.itensPedidoCompra = itensPedidoCompra;
	}

	public FornecedorDTO getFornecedor() {

		return fornecedor;
	}

	public void setFornecedor(FornecedorDTO fornecedor) {

		this.fornecedor = fornecedor;
	}

	public CotacaoDTO getCotacao() {

		return cotacao;
	}

	public void setCotacao(CotacaoDTO cotacao) {

		this.cotacao = cotacao;
	}

	public Integer getStatus() {

		return status;
	}

	public void setStatus(Integer status) {

		this.status = status;
	}

}
