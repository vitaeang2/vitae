package br.com.vitae.bus.dto;

import java.math.BigDecimal;

import br.com.arquitetura.model.BaseDTO;
import br.com.vitae.bus.entity.ItemCotacao;

/**
 * 
 * <p>
 * <b>Title:</b> ItemCotacaoDTO.java
 * </p>
 * 
 * <p>
 * <b>Description:</b>
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnlogia da Informacao
 * </p>
 * 
 * @author Sergio Filho - sergioadsf@gmail.com
 * 
 * @version 1.0.0
 */
@SuppressWarnings("serial")
public class ItemCotacaoDTO implements BaseDTO {

	private Long id;

	private BigDecimal vlrUnitario;

	private BigDecimal quantidade;

	private CotacaoFornecedorDTO cotacaoFornecedor;

	private ProdutoDTO produto;

	public ItemCotacaoDTO() {
	}

	public ItemCotacaoDTO( Long id, BigDecimal vlrUnitario, BigDecimal quantidade, CotacaoFornecedorDTO cotacaoFornecedor, ProdutoDTO produto ) {
		super();
		this.id = id;
		this.vlrUnitario = vlrUnitario;
		this.quantidade = quantidade;
		this.cotacaoFornecedor = cotacaoFornecedor;
		this.produto = produto;
	}

	public ItemCotacaoDTO( ItemCotacao ic ) {
		this.id = ic.getId();
		this.vlrUnitario = ic.getVlrUnitario();
		this.quantidade = ic.getQuantidade();
		if (ic.getCotacaoFornecedor() != null) {
			this.cotacaoFornecedor = new CotacaoFornecedorDTO(ic.getCotacaoFornecedor());
		}

		if (ic.getProduto() != null) {
			this.produto = new ProdutoDTO(ic.getProduto());
		}
	}

	public Long getId() {

		return id;
	}

	public void setId(Long id) {

		this.id = id;
	}

	public BigDecimal getVlrUnitario() {

		return vlrUnitario;
	}

	public void setVlrUnitario(BigDecimal vlrUnitario) {

		this.vlrUnitario = vlrUnitario;
	}

	public BigDecimal getQuantidade() {

		return quantidade;
	}

	public void setQuantidade(BigDecimal quantidade) {

		this.quantidade = quantidade;
	}

	public CotacaoFornecedorDTO getCotacaoFornecedor() {

		return cotacaoFornecedor;
	}

	public void setCotacaoFornecedor(CotacaoFornecedorDTO cotacaoFornecedor) {

		this.cotacaoFornecedor = cotacaoFornecedor;
	}

	public ProdutoDTO getProduto() {

		return produto;
	}

	public void setProduto(ProdutoDTO produto) {

		this.produto = produto;
	}

}
