package br.com.vitae.bus.dto;

import java.math.BigDecimal;

import br.com.arquitetura.model.BaseDTO;
import br.com.vitae.bus.entity.ProdutoEstoque;

/**
 * 
 * <p>
 * <b>Title:</b> ProdutoEstoqueDTO.java
 * </p>
 * 
 * <p>
 * <b>Description:</b> DTO Responsavél de representar o estoque de um produto
 * </p>
 * 
 * <p>
 * <b>Company: </b> B3 - Tecnlogia da Informacao
 * </p>
 * 
 * @author Joaquim Barros - joaquimbarros.cmp@gmail.com
 * 
 * @version 1.0.0
 */
@SuppressWarnings("serial")
public class ProdutoEstoqueDTO implements BaseDTO {

	private Long id;

	private BigDecimal quantidade;

	private ProdutoDTO produto;

	private EmpresaDTO empresa;

	private NucleoDTO nucleo;

	public ProdutoEstoqueDTO() {

	}

	public ProdutoEstoqueDTO( Long id, BigDecimal quantidade, ProdutoDTO produto, EmpresaDTO empresa, NucleoDTO nucleo ) {

		super();
		this.id = id;
		this.quantidade = quantidade;
		this.produto = produto;
		this.empresa = empresa;
		this.nucleo = nucleo;
	}

	public ProdutoEstoqueDTO( ProdutoEstoque produtoEstoque ) {

		this.id = produtoEstoque.getId();
		this.quantidade = produtoEstoque.getQuantidade();

		if (produtoEstoque.getProduto() != null) {

			this.produto = new ProdutoDTO(produtoEstoque.getProduto());
		}

		if (produtoEstoque.getEmpresa() != null) {

			this.empresa = new EmpresaDTO(produtoEstoque.getEmpresa());
		}

		if (produtoEstoque.getNucleo() != null) {

			this.nucleo = new NucleoDTO(produtoEstoque.getNucleo());
		}
	}

	/**
	 * Retorna o valor do atributo <code>id</code>
	 *
	 * @return <code>Long</code>
	 */
	public Long getId() {

		return id;
	}

	/**
	 * Define o valor do atributo <code>id</code>.
	 *
	 * @param id
	 */
	public void setId(Long id) {

		this.id = id;
	}

	/**
	 * Retorna o valor do atributo <code>quantidade</code>
	 *
	 * @return <code>BigDecimal</code>
	 */
	public BigDecimal getQuantidade() {

		return quantidade;
	}

	/**
	 * Define o valor do atributo <code>quantidade</code>.
	 *
	 * @param quantidade
	 */
	public void setQuantidade(BigDecimal quantidade) {

		this.quantidade = quantidade;
	}

	/**
	 * Retorna o valor do atributo <code>produto</code>
	 *
	 * @return <code>ProdutoDTO</code>
	 */
	public ProdutoDTO getProduto() {

		return produto;
	}

	/**
	 * Define o valor do atributo <code>produto</code>.
	 *
	 * @param produto
	 */
	public void setProduto(ProdutoDTO produto) {

		this.produto = produto;
	}

	/**
	 * Retorna o valor do atributo <code>empresa</code>
	 *
	 * @return <code>EmpresaDTO</code>
	 */
	public EmpresaDTO getEmpresa() {

		return empresa;
	}

	/**
	 * Define o valor do atributo <code>empresa</code>.
	 *
	 * @param empresa
	 */
	public void setEmpresa(EmpresaDTO empresa) {

		this.empresa = empresa;
	}

	/**
	 * Retorna o valor do atributo <code>nucleo</code>
	 *
	 * @return <code>NucleoDTO</code>
	 */
	public NucleoDTO getNucleo() {

		return nucleo;
	}

	/**
	 * Define o valor do atributo <code>nucleo</code>.
	 *
	 * @param nucleo
	 */
	public void setNucleo(NucleoDTO nucleo) {

		this.nucleo = nucleo;
	}

}
