package br.com.vitae.bus.dto;

import br.com.arquitetura.model.BaseDTO;
import br.com.vitae.bus.entity.Fornecedor;

@SuppressWarnings("serial")
public class FornecedorDTO implements BaseDTO {

	private Long id;

	private Boolean ativo;

	private PessoaDTO pessoa;

	private String nomeContato;

	private String celularContato;

	private boolean prestadorServico;

	public FornecedorDTO() {
	}

	public FornecedorDTO( Long id, Boolean ativo, PessoaDTO pessoa, String nomeContato, String celularContato, boolean prestadorServico ) {
		super();
		this.id = id;
		this.ativo = ativo;
		this.pessoa = pessoa;
		this.nomeContato = nomeContato;
		this.celularContato = celularContato;
		this.prestadorServico = prestadorServico;
	}

	public FornecedorDTO( Fornecedor fornecedor ) {
		this.id = fornecedor.getId();
		this.ativo = fornecedor.getAtivo();
		this.nomeContato = fornecedor.getNomeContato();
		this.celularContato = fornecedor.getCelularContato();
		this.prestadorServico = fornecedor.isPrestadorServico();
		if (fornecedor.getPessoa() != null) {
			this.pessoa = new PessoaDTO(fornecedor.getPessoa());
		}
	}

	public Long getId() {

		return id;
	}

	public void setId(Long id) {

		this.id = id;
	}

	/**
	 * Retorna o valor do atributo <code>ativo</code>
	 *
	 * @return <code>Boolean</code>
	 */
	public Boolean getAtivo() {

		return ativo;
	}

	/**
	 * Define o valor do atributo <code>ativo</code>.
	 *
	 * @param ativo
	 */
	public void setAtivo(Boolean ativo) {

		this.ativo = ativo;
	}

	public PessoaDTO getPessoa() {

		return pessoa;
	}

	public void setPessoa(PessoaDTO pessoa) {

		this.pessoa = pessoa;
	}

	public String getNomeContato() {

		return nomeContato;
	}

	public void setNomeContato(String nomeContato) {

		this.nomeContato = nomeContato;
	}

	public String getCelularContato() {

		return celularContato;
	}

	public void setCelularContato(String celularContato) {

		this.celularContato = celularContato;
	}

	public boolean isPrestadorServico() {

		return prestadorServico;
	}

	public void setPrestadorServico(boolean prestadorServico) {

		this.prestadorServico = prestadorServico;
	}

}
