package br.com.vitae.bus.dto;

import java.math.BigDecimal;

import br.com.arquitetura.model.BaseDTO;
import br.com.vitae.bus.entity.ItemPedidoCompra;

@SuppressWarnings("serial")
public class ItemPedidoCompraDTO implements BaseDTO {

	private Long id;

	private BigDecimal vlrUnitario;

	private BigDecimal quantidade;

	private PedidoCompraDTO pedidoCompra;

	private ProdutoDTO produto;

	private Integer status;

	public ItemPedidoCompraDTO() {

	}

	public ItemPedidoCompraDTO( ItemPedidoCompra itemPedidoCompra ) {

		this.id = itemPedidoCompra.getId();
		this.vlrUnitario = itemPedidoCompra.getVlrUnitario();
		this.quantidade = itemPedidoCompra.getQuantidade();
		this.status = itemPedidoCompra.getStatus() != null ? itemPedidoCompra.getStatus().ordinal() : null;

		if (itemPedidoCompra.getPedidoCompra() != null) {

			this.pedidoCompra = new PedidoCompraDTO(itemPedidoCompra.getPedidoCompra());
		}

		if (itemPedidoCompra.getProduto() != null) {

			this.produto = new ProdutoDTO(itemPedidoCompra.getProduto());
		}

	}

	public Long getId() {

		return id;
	}

	public void setId(Long id) {

		this.id = id;
	}

	public BigDecimal getVlrUnitario() {

		return vlrUnitario;
	}

	public void setVlrUnitario(BigDecimal vlrUnitario) {

		this.vlrUnitario = vlrUnitario;
	}

	public BigDecimal getQuantidade() {

		return quantidade;
	}

	public void setQuantidade(BigDecimal quantidade) {

		this.quantidade = quantidade;
	}

	public PedidoCompraDTO getPedidoCompra() {

		return pedidoCompra;
	}

	public void setPedidoCompra(PedidoCompraDTO pedidoCompra) {

		this.pedidoCompra = pedidoCompra;
	}

	public ProdutoDTO getProduto() {

		return produto;
	}

	public void setProduto(ProdutoDTO produto) {

		this.produto = produto;
	}

	public Integer getStatus() {

		return status;
	}

	public void setStatus(Integer status) {

		this.status = status;
	}

}
